\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}

%% pour avoir la place de noter les coups d'archet :
\layout {
  %% plus d'espace entre les paroles et la ligne de l'instrument
  \context {
    \Lyrics
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.minimum-distance = #8
  }
  %% Plus d'espace entre les indications de tempo et la portée
  \context {
    \Score
    \override MetronomeMark.padding = #2.5 % default 0.8
  }
  %% Plus d'espace entre les notes et les nuances
  \context {
    \Voice
    \override DynamicText.Y-offset = #(scale-by-font-size -3) % default -0.6
    \override Hairpin.Y-offset = #(scale-by-font-size -3)
    \override DynamicTextSpanner.Y-offset = #(scale-by-font-size -2.5)
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Armida"
    date = "1771"
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \partBlankPageBreak #'(bassi fagotti viola violino1 violino2)
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO PRIMO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "ATTO PRIMO"
%% 1-1
\pieceToc "Sinfonia"
\includeScore "AAAsinfonia"
\newBookPart#'(corni)
\scene "Scena I" "Scena I"
%% 1-2
\pieceToc\markup\wordwrap {
  Coro: \italic { Sparso di pure brine }
}
\includeScore "AABcoro"
%% 1-3
\pieceToc "Ballo"
\includeScore "AACballo"
%% 1-4
\pieceToc "Gavotta"
\includeScore "AADgavotta"
%% 1-5
\pieceToc\markup\wordwrap {
  Coro: \italic { Donzelle semplici }
}
\includeScore "AAEcoro"
\newBookPart#'(tromboni)

\scene "Scena II" "Scena II"
%% 1-6
\pieceToc\markup\wordwrap {
  Ismene: \italic { Ah disendete, amiche }
}
\includeScore "ABArecit"
%% 1-7
\pieceToc\markup\wordwrap {
  Coro: \italic { Ah fra la nera }
}
\includeScore "ABBcoro"
%% 1-8
\pieceToc\markup\wordwrap {
  Ismene: \italic { In si crudel dubbiezza ah non perdiamo }
}
\includeScore "ABCrecit"
%% 1-9
\pieceToc\markup\wordwrap {
  Coro: \italic { Sparso di pure brine }
}
\includeScore "ABDcoro"

\scene "Scena III" "Scena III"
%% 1-10
\pieceToc\markup\wordwrap {
  Ubaldo, Ismene: \italic { Ecco l’onda infidiosa }
}
\includeScore "ACArecit"
%% 1-11
\pieceToc\markup\wordwrap {
  Coro: \italic { Vieni al fonte del contento }
}
\includeScore "ACBcoro"
\newBookPart#'(corni)
%% 1-12
\pieceToc\markup\wordwrap {
  Ubaldo, Ismene, Coro: \italic { Che periglioso assalto! }
}
\includeScore "ACCrecit"
%% 1-13
\pieceToc\markup\wordwrap {
  Ismene: \italic { Mostri i più crudi, e infesti }
}
\includeScore "ACDaria"
%% 1-14
\pieceToc\markup\wordwrap {
  Coro di Demoni: \italic { Qual è la man che scuote }
}
\includeScore "ACEcoro"
\partNoPageBreak#'(tromboni)
%% 1-15
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Quanto fragili, e vane, immondi spirti }
}
\includeScore "ACFrecit"
%% 1-16
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Tornate al nero abisso }
}
\includeScore "ACGaria"
\newBookPart#'(tromboni)
%% 1-17
\pieceToc\markup\wordwrap {
  Coro di Demoni: \italic { Qual sibilo orrendo }
}
\includeScore "ACHcoro"
%% 1-18
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Eterno Proveder, tu che guidasti }
}
\includeScore "ACIrecit"
%% 1-19
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Finta larva, d’Abisso fra l’ombra }
}
\includeScore "ACJaria"
\actEnd "Fine dell’ Atto primo."
\partBlankPageBreak#'(fagotti)
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO SECONDO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "ATTO SECONDO"
\scene "Scena I" "Scena I"
%% 2-1
\pieceToc\markup\wordwrap {
  Armida, Rinaldo: \italic { Qui’l regno è del contento }
}
\includeScore "BAAduo"
%% 2-2
\pieceToc\markup\wordwrap {
  Armida, Rinaldo: \italic { Addio. Già m'abbandoni? }
}
\includeScore "BABrecit"
\newBookPart#'(oboi)
%% 2-3
\pieceToc\markup\wordwrap {
  Armida: \italic { Tremo, bell' idol mio }
}
\includeScore "BACaria"

\scene "Scena II" "Scena II"
%% 2-4
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { E non deggio seguirla? }
}
\includeScore "BBArecit"
%% 2-5
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Lungi da te, ben mio }
}
\includeScore "BBBaria"
%% 2-6
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Forse, chi sà? Verranno }
}
\includeScore "BBCrecit"
%% 2-7
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Vieni a me sull’ali d’oro }
}
\includeScore "BBDaria"
%% 2-8
\pieceToc "Ballo"
\includeScore "BBEballo"
%% 2-9
\pieceToc "[Sinfonia]"
\includeScore "BBFballo"

\scene "Scena III" "Scena III"
%% 2-10
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Oh come in un momento }
}
\includeScore "BCAaria"
%% 2-11
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Così molle e ridente }
}
\includeScore "BCBrecit"
%% 2-12
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Misero! chi mi scuote? }
}
\includeScore "BCCrecit"
\newBookPart#'(flauti)

\scene "Scena IV" "Scena IV"
%% 2-13
\pieceToc\markup\wordwrap {
  Armida, Rinaldo: \italic { Soccorso, o Dei! }
}
\includeScore "BDAduo"
%% 2-14
\pieceToc\markup\wordwrap {
  Rinaldo, Armida: \italic { Questo arcano funesto }
}
\includeScore "BDBrecit"
%% 2-15
\pieceToc\markup\wordwrap {
  Armida, Rinaldo: \italic { Dilegua il tuo timore }
}
\includeScore "BDCduo"
\newBookPart#'(violino1)
%% 2-16
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Ora si ch’io mi perdo }
}
\includeScore "BDDrecit"

\scene "Scena V" "Scena V"
%% 2-17
\pieceToc\markup\wordwrap {
  Ubaldo, Rinaldo: \italic { Fermati, incauto }
}
\includeScore "BEArecit"
%% 2-18
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Vedo l’abisso orrendo }
}
\includeScore "BEBaria"
\actEnd "Fine dell’ Atto secondo."
\newBookPart#'()

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO TERZO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\act "ATTO TERZO"
\scene "Scena I" "Scena I"
%% 3-1
\pieceToc\markup\wordwrap {
  Coro, Armida: \italic { Chi sorde vi rende }
}
\includeScore "CAAcoro"
\partNoPageTurn#'(bassi)
\newBookPart#'(corni)
%% 3-2
\pieceToc\markup\wordwrap {
  Armida: \italic { Misera! Il Ciel m’opprime }
}
\includeScore "CABrecit"

\scene "Scena II" "Scena II"
%% 3-3
\pieceToc\markup\wordwrap {
  Ismene, Armida: \italic { Corri, Regina }
}
\includeScore "CBArecit"
%% 3-4
\pieceToc\markup\wordwrap {
  Armida, coro: \italic { Ah mi tolga almen la vita }
}
\includeScore "CBBaria"
\newBookPart#'(oboi)

\scene "Scena III" "Scena III"
%% 3-5
\pieceToc\markup\wordwrap {
  Ismene: \italic { Ove corre, infelice! }
}
\includeScore "CCArecit"
\newBookPart#'(corni)
%% 3-6
\pieceToc\markup\wordwrap {
  Coro, Ismene: \italic { Ah misera regina }
}
\includeScore "CCBcoro"
%% 3-7
\pieceToc\markup\wordwrap {
  Ismene, coro: \italic { Schernita, depressa }
}
\includeScore "CCCaria"

\scene "Scena IV" "Scena IV"
%% 3-8
\pieceToc\markup\wordwrap {
  Ubaldo, Rinaldo: \italic { Siam quasi in salvo, eccoci al lido. }
}
\includeScore "CDArecit"
\newBookPart#'(corni)
%% 3-9
\pieceToc\markup\wordwrap {
  Ubaldo: \italic { Torna schiavo infelice }
}
\includeScore "CDBaria"
%% 3-10
\pieceToc\markup\wordwrap {
  Rinaldo: \italic { Ah non lasciarmi no }
}
\includeScore "CDCaria"
\newBookPart #'(oboi)
%% 3-11
\pieceToc\markup\wordwrap {
  Rinaldo, Ubaldo: \italic { Misera! È già vicina }
}
\includeScore "CDDrecit"

\scene "Scena V" "Scena V"
%% 3-12
\pieceToc\markup\wordwrap {
  Armida, Ubaldo, Rinaldo: \italic { Fermatai, aspetta }
}
\includeScore "CEArecit"
%% 3-13
\pieceToc\markup\wordwrap {
  Armida, Rinaldo, Ubaldo: \italic { Strappami il cuor dal seno }
}
\includeScore "CEBterzetto"
\newBookPart#'(oboi)

\scene "Scena VI" "Scena VI"
%% 3-14
\pieceToc\markup\wordwrap {
  Ismene: \italic { Ecco la sventurata }
}
\includeScore "CFArecit"
%% 3-15
\pieceToc\markup\wordwrap {
  Coro delle donne: \italic { Ah barbaro, ah senti }
}
\includeScore "CFBcoro"
%% 3-16
\pieceToc\markup\wordwrap {
  Ismene, Armida: \italic { Infelice Regina! Ancor respira }
}
\includeScore "CFCrecit"
%% 3-17
\pieceToc\markup\wordwrap {
  Armida, coro: \italic { Io con voi la nera face }
}
\includeScore "CFDaria"
\actEnd "[Fine dell Dramma.]"
