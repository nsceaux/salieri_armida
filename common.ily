\header {
  copyrightYear = "2019"
  composer = "Antonio Salieri"
  poet = "Marco Coltellini"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'use-ancient-clef (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((eqv? #t (ly:get-option 'urtext)) 14)
        ((not (symbol? (ly:get-option 'part))) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (cond ((eqv? (ly:get-option 'part) 'reduction) ly:optimal-breaking)
           ((symbol? (ly:get-option 'part)) ly:page-turn-breaking)
           (else ly:optimal-breaking)))
}

\layout {
  reference-incipit-width = #(* 1/2 mm)
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((flauti "Flauti" () (#:notes "flauti"))
   (oboi "Oboi" () (#:notes "oboi"))
   (fagotti "Fagotti" ((bassi "Bassi")) (#:clef "bass" #:notes "fagotti"))
   (corni "Corni" () (#:notes "corni"))
   (tromboni "Tromboni" () (#:notes "tromboni" #:clef "alto"
                                    #:score-template "score-tromboni"))
   (violino1 "Violino I" ()  (#:notes "violini" #:tag-notes primo))
   (violino2 "Violino II" () (#:notes "violini" #:tag-notes secondo))
   (viola "Viola" () (#:notes "viola" #:clef "alto"))
   (bassi "Bassi" () (#:notes "bassi" #:clef "bass")))

\opusTitle "Armida"

\paper {
  % de la place en bas de page pour les annotations
  last-bottom-spacing.padding = #3
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      (* 8 mm))
  ragged-last = ##f
}

\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'inside
    \override DynamicTextSpanner.style = #'none
    \override AccidentalSuggestion.avoid-slur = #'outside
  }
  \context {
    \DrumVoice
    \override DynamicTextSpanner.style = #'none
  }
}

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset.
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
  tagline = \markup\sans\abs-fontsize #8
  \override #'(baseline-skip . 0) \vcenter {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \column {
      \line { \tagline-vspacer \copyright }
      \smaller\line {
        \tagline-vspacer
        Sheet music from
        \with-url #"http://nicolas.sceaux.free.fr"
        http://nicolas.sceaux.free.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
      \smaller\line {
        \tagline-vspacer \license
        — free to download, distribute, modify and perform.
      }
    }
  }

}

corniAInstr = \with {
  instrumentName = "Corni in A"
  shortInstrumentName = "Cor."
}
corniBInstr = \with {
  instrumentName = "Corni in B"
  shortInstrumentName = "Cor."
}
corniCInstr = \with {
  instrumentName = "Corni in C"
  shortInstrumentName = "Cor."
}
corniFInstr = \with {
  instrumentName = "Corni in F"
  shortInstrumentName = "Cor."
}
corniEInstr = \with {
  instrumentName = "Corni in E"
  shortInstrumentName = "Cor."
}
corniEbInstr = \with {
  instrumentName = "Corni in E♭"
  shortInstrumentName = "Cor."
}
corniDInstr = \with {
  instrumentName = "Corni in D"
  shortInstrumentName = "Cor."
}
trombeInstr = \with {
  instrumentName = "Trombe"
  shortInstrumentName = "Tr."
}
tromboniInstr = \with {
  instrumentName = "Tromboni"
  shortInstrumentName = "Trb."
}
flautoInstr = \with {
  instrumentName = "Flauto"
  shortInstrumentName = "Fl."
}
flautiInstr = \with {
  instrumentName = "Flauti"
  shortInstrumentName = "Fl."
}
oboiInstr = \with {
  instrumentName = "Oboe"
  shortInstrumentName = "Ob."
}
clarinettiInstr = \with {
  instrumentName = "Clarinetti"
  shortInstrumentName = "Cl."
}
fagottiInstr = \with {
  instrumentName = "Fagotti"
  shortInstrumentName = "Fg."
}
timpaniInstr = \with {
  instrumentName = "Timpani"
  shortInstrumentName = "Tim."
}

violiniInstr = \with {
  instrumentName = "Violini"
  shortInstrumentName = "Vln."
}
altoInstr = \with {
  instrumentName = "Viola"
  shortInstrumentName = "Vla."
}
violaInstr = \with {
  instrumentName = "Viola"
  shortInstrumentName = "Vla."
}
bassoInstr = \with {
  instrumentName = "Bassi"
  shortInstrumentName = "B."
}
vlcInstr = \with {
  instrumentName = "Violoncelli"
  shortInstrumentName = "Vlc."
}
bassoInstr = \with {
  instrumentName = "Basso"
  shortInstrumentName = "B."
}
vlcBInstr = \with {
  instrumentName = \markup\center-column { Violoncelli Basso }
  shortInstrumentName = \markup\center-column { Vlc B. }
}

trill = #(make-articulation "trill")

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET }
                                             #}))

%% Cues
cueTreble =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuringWithClef $cue-name #CENTER "treble" $music #})

cue =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuring $cue-name #CENTER $music #})

cueTransp =
#(define-music-function (parser location cue-name pitch music)
     (string? ly:pitch? ly:music?)
   #{ \transposedCueDuring $cue-name #CENTER $pitch $music #})


quoteViolinoI =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'primo { \includeNotes "violini" }
#}))

quoteViolinoII =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'secondo { \includeNotes "violini" }
#}))

quoteViola =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'tutti { \includeNotes "viola" }
#}))

quoteBassi =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'tutti { \includeNotes "bassi" }
#}))

quoteBassoI =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'primo { \includeNotes "bassi" }
#}))

quoteBassoII =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'secondo { \includeNotes "bassi" }
#}))

mmRestInvisible = \override Staff.MultiMeasureRest.transparent = ##t
mmRestVisible = \revert Staff.MultiMeasureRest.transparent
restInvisible = \override Staff.Rest.transparent = ##t
restVisible = \revert Staff.Rest.transparent

%% Nuances
rf = #(make-music 'AbsoluteDynamicEvent 'text "rf")

%% finalement, afficher les nuances suggérées en grande taille
sug = #(define-music-function (parser loc arg) (ly:music?) arg)
