\clef "alto" fa'4 r fa' r |
\override AccidentalSuggestion.avoid-slur = #'outside
fa' r r16 fa' mi' re' do' \ficta sib la sol |
fa( sol la sib) do'( sib la sol) fa( sol la sib) do'( sib la sol) |
fa( sol la \ficta sib) do'( sib la sol) fa( sol la sib) do'( sib la sol) |
sol( mi sol la) \ficta sib( la sol fa) mi( fa sol la) sib( la sol fa) |
mi( fa sol la) \ficta sib( la sol fa) mi( fa sol la) sol( mi re do) |
fa( sol la \ficta sib) do'( sib la sol) fa( sol la sib) do'( la sol fa) |
sol( la \ficta sib la) sol( fa mi re) do( re mi fa) mi( do mi do) |
fa( sol la \ficta sib) do'( sib la sol) fa( sol la sib) do'( la sol fa) |
mi( fa sol la) \ficta sib( la sol fa) mi( fa sol la) sol( mi re do) |
fa( sol la \ficta sib) do'( sib la sol) fa( sol la sib) do'( sib la sol) |
fa( sol la \ficta sib) do'( sib la sol) fa( sol la sib) do'( sib la sol) |
fa2 r |
R1 |
r2 sib16( do' re' mib') fa'( mib' re' do') |
\ficta sib( do' re' \ficta mib') fa'( mib' re' do') sib( do' re' mib') \once\slurDashed fa'( mib' re' do') |
\ficta sib( do' re' \ficta mib') \once\slurDashed fa'( mib' re' do') do'4 r |
r2 r4 la |
r4 sib r2 |
mib16( fa sol lab) sib( lab sol fa) mib( fa sol lab) sib( lab sol fa) |
\ficta mib( fa sol \ficta lab) \ficta sib( lab sol fa) mib( fa sol lab) sib( sol fa mib) |
fa( sol \ficta lab sol) fa( \ficta mib re do) re( do re mib) fa( re do re) |
\ficta mib( fa sol \ficta lab) \ficta sib( lab sol fa) mib( fa sol lab) sib( sol fa mib) |
fa( sol \ficta lab sol) fa( \ficta mib re do) re( do re mib) fa( re do re) |
mib4 r r2 |
R1 | \allowPageTurn
mib16 fa sol lab sib sol fa mib fa sol lab! sol sib lab sol fa |
mib4 r re'32 do' re'8. r4 |
r2 do'4 r |
fad' r r2 |
r2 sol8 sol sol sol |
mi4 r r2 |
re4 r r2 |
R1 |
si!4 r r2 |
do'4 r red' r |
r2 mi'4 r |
mid' r r2 |
R1 |
fad'4 r r2 |
fad'4 r r sold' |
