\clef "treble" R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { la'8 s4. do''8 s4. |
    s4 la''2 s4 |
    mi''8 s4. sol''8 s4. |
    s4 re'''2 s4 |
    s4 \grace re'''16 do'''32 si''! do'''8. \ru#2 { \grace re'''16 do'''32 si'' do'''8. } | }
  { fa'8 s4. la'8 s4. |
    s4 do''2 s4 |
    \ficta sib'8 s4. mi''8 s4. |
    s4 sol''2 s4 |
    s4 \grace sib''?16 la''32 sol'' la''8. \ru#2 { \grace sib''16 la''32 sol'' la''8. } | }
  { s8 r r4 s8 r r4 |
    r4 s2 r4 |
    s8 r r4 s8 r r4 |
    r4 s2 r4 |
    r4 s2. | }
>>
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { \grace re'''16 do'''32 si''! do'''8. \ru#2 { \grace re'''16 do'''32 si'' do'''8. } | }
  { \grace sib''?16 la''32 sol'' la''8.  \ru#2 { \grace sib''16 la''32 sol'' la''8. } | }
>>
R1 |
\twoVoices #'(primo secondo tutti) <<
  { s4 \grace re'''16 do'''32 si'' do'''8. s4 \grace sib''16 la''32 sol'' la''8. |
    s4 \grace sib''?16 la''32 sol'' la''16 s8 s4 \grace sol''16 fa''32 mi'' fa''16 s8 |
    do''2 }
  { s4 \grace re''16 do''32 si'! do''8. s4 \grace sol''16 fa''32 mi'' fa''8. |
    s4 \grace sol''16 fa''32 mi'' fa''16 s8 s4 \grace re''16 do''32 si'! do''16 s8 |
    la'2 }
  { r4 s r s | r4 s8 r r4 s | }
>> r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''8 s4. | fa''8 s4. s4 re'''~ | re''' }
  { sib'8 s4. | re''8 s4. s4 fa''4~ | fa'' }
  { s8 r r4 | s8 r r4 r }
>> r4 r2 |
r2 r4 \twoVoices #'(primo secondo tutti) <<
  { \grace sol''16 fa''32 mi''! fa''16 r8 |
    s4 \grace mib''16 re''32 do'' re''16 r8 }
  { do''4 | s fa' }
  { s4 | r s }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { sol'8 s4. sib'8 s4. | s4 sol''2 s4 | }
  { mib'8 s4. sol'8 s4. | s4 sib'2 s4 | }
  { s8 r r4 s8 r r4 | r4 s2 r4 | }
>>
r4 \twoVoices #'(primo secondo tutti) <<
  { \grace sib''?16 lab''32 sol'' lab''8. \ru#2 { \grace sib''16 lab''32 sol'' lab''8. } |
    sol''8 }
  { \grace sol''16 fa''32 mi''! fa''8. \ru#2 { \grace sol''16 fa''32 mi'' fa''8. } |
    \ficta mib''8 }
>> r8 r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { \grace sib''?16 lab''32 sol'' lab''8. \ru#2 { \grace sib''16 lab''32 sol'' lab''8. } |
    sol''4 }
  { \ru#3 { \grace sol''16 fa''32 mi'' fa''8. } |
    mib''4 }
>> r4 r2 | \allowPageTurn
R1 |
\twoVoices #'(primo secondo tutti) <<
  { sol''8 s4. lab''8 s4. |
    sol''4 s \grace la''16 sol''32 fad'' sol''8. s4 |
    s2 \grace la''16 sol''32 fa'' sol''8. s4 | }
  { mib''8 s4. sib'8 s4. |
    \ficta sib'4 s4 \grace do''16 si'!32 la' si'8. s4 |
    s2 do''4 s | }
  { s8 r r4 s8 r r4 | s4 r s r | r2 s4 r | }
>>
R1*12 |
