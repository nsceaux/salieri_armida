\key do \major
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*2 \bar "||" \grace s16
\tempo "Andante" \midiTempo#100 s1*28
\tempo "Adagio" \midiTempo#80 s1*11 \bar "|."
