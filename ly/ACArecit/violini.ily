\clef "treble" \override AccidentalSuggestion.avoid-slur = #'outside
<<
  \tag #'primo {
    fa''16 do''' \ficta sib'' la'' sol'' fa'' mi'' re'' do'' la'' sol'' fa'' mi'' re'' do'' \ficta sib' |
    la' fa'' mi'' re'' do'' \ficta sib' la' sol'
  }
  \tag #'secondo {
    la'16 la'' sol'' fa'' mi'' re'' do'' \ficta sib' la' fa'' mi'' re'' do'' \ficta sib' la' sol' |
    fa' re'' do'' \ficta sib' la' sol' fa' mi'
  }
>> fa'16 fa' mi' re' do' \ficta sib la sol |
la8 r <<
  \tag #'primo {
    \grace sol' fa'32 mi' fa'16 r8 r4 \grace sib'16 la'32 sol' la'16 r8 |
    r8 do''( la''2)\rf sol''8( fa'') |
    r4 \grace fa''16 mi''32 re'' mi''16 r8 r4 \grace la''16 sol''32 fad'' sol''16 r8 |
    r8 sol''( re'''2)\rf do'''8( sib'') |
    la'' r r4 r2 |
    r8 do''( \ficta sib''2)\rf la''8( sol'') |
    la''8 r r4 r2 |
    r8 sol''( re'''2)\rf do'''8( sib'') |
    r4 \grace sib''16 la''32 sol'' la''16 r8 r4 \grace sol''16 fa''32 mi'' fa''16 r8 |
    r4 \grace re''16 do''32 si'! do''16 r8 r4 \grace sib'?16 la'32 sol' la'16 r8 |
    fa'2
  }
  \tag #'secondo {
    r8 do'16 sib la8 r r do'16 sib |
    la8 r r do'16 do' \sug do'8 r r8 do'16 do' |
    do'8 r r do'16 do' do'8 r r \ficta sib16 la |
    sol8 r r \ficta sib16 la sol8 r r do'16 do' |
    do'8 r r do'16 do' do'8 r r do'16 do' |
    do'8 r r do'16 do' do'8 r r do'16 do' |
    do'8 r r do'16 do' do'8 r r \ficta sib16 la |
    sol8 r r \ficta sib16 la sol8 r r do'16 do' |
    do'8 r r do'16 do' do'8 r r do'16 do' |
    do'8 r r do'16 do' do'8 r r do'16 do' |
    do'2
  }
>> r2 |
R1 |
r2 r4 <<
  \tag #'primo {
    \grace do''16 sib'32 la' sib'16 r8 |
    r4 \grace mib''16 re''32 do'' re''16 r8 r fa''( re'''4)\rf~ |
    re'''4( do'''8 sib'') la''4 r |
    r2 r4 \grace sol''16 fa''32 mi''! fa''16 r8 |
    r4 \grace mib''16 re''32 do'' re''16 r8 r2 |
    r4 \grace fa'16 mib'32 re' mib'16 r8 r4 \grace lab'16 sol'32 fa' sol'16 r8 |
    r8 sib' sol''2\rf fa''8 mib'' |
    re''4 r r2 |
    r8 sib'8( sol''2) fa''8 mib'' |
    re''4 r r2 |
    sol''4
  }
  \tag #'secondo {
    r8 fa'16 \ficta mib' |
    re'8 r r fa'16 mib' \sug re'8 r r8 fa'16 fa' |
    fa'8 r r fa'16 fa' fa'4 r |
    r2 r4 <fa' do''>4 |
    r4 fa' r2 |
    r4 r8 sib16 lab sol8 r r sib16 lab |
    sol8 r r sib16 sib sib8 r r sib16 sib |
    \ficta sib8 r r fa'16 mib' re'8 r r sib16 sib |
    \ficta sib8 r r sib16 sib sib8 r r sib16 sib |
    \ficta sib8 r r sib16 sib sib8 r r sib16 sib |
    \ficta sib4
  }
>> r4 r2 |
R1 | \allowPageTurn
<<
  \tag #'primo {
    r4 \grace lab''16 sol''32 fa'' sol''16 r8 r4 \grace mib''16 re''32 do'' re''16 r8 |
    mib''4 r \grace do''16 si'!32 la' si'16 r8 r4 |
    r2 \grace re''16 do''32 si' do''8. r4 |
    la'4
  }
  \tag #'secondo {
    r4 r8 sib16 sol \ficta lab8 r r sib!16 sib |
    \ficta sib4 r fa' r |
    r2 mib'32 re' mib'8. r4 |
    re'
  }
>> r4 r2 |
r2 <<
  \tag #'primo {
    sol'8_\markup\italic dolce sib'16( sol') sol'( mi'!) mi'( re') |
    dod'4 r r2 |
    la'4 r r2 |
    R1 |
    sold'4 r r2 |
    la'4 r si' r |
    r2 si'4 r |
    dod'' r r2 |
    R1 |
    dod''4 r r2 |
    si'4 r r dod'' |
  }
  \tag #'secondo {
    sol'8 sib sib sib |
    la4 r r2 |
    fa'4 r r2 |
    R1 |
    mi'!4 r r2 |
    mi'4 r fad' r |
    r2 sol'4 r |
    sold' r r2 |
    R1 |
    la'4 r r2 |
    fad'4 r r mid' |
  }
>>
