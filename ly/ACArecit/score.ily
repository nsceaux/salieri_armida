\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \flautiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr  } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Corno solo in E♭ }
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ubaldo
      shortInstrumentName = \markup\character Ub.
    } \withLyrics <<
      { s1*27 \set Staff.shortInstrumentName = "Ism." }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*3\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        \grace s16 s1*3\pageBreak
        s1*4\pageBreak
        s1*3\pageBreak
        s1*4\pageBreak
        s1*3\pageBreak
        s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
