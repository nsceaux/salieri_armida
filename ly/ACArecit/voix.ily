\clef "tenor/G_8" R1*12 |
r2 do'8 do' do' do'16 do' |
la8 la fad fad16 sol la8 la r la16 do' |
do'4 mib8 fa re re r4 |
R1 |
r2 r4 do'8 re'16 mib' |
\ficta mib'8 la la la16 la la8 sib do' do' |
r16 do' re' mib' re'8 re' re' re'16 re' re'8 mib'! |
mib' sib r4 r2 |
R1*4 |
r4 r8 \ficta sib16 \ficta mib' mib'8 sib r sib |
\ficta sib8 sib sib sib sol sol r16 sol sol sol |
mib8 mib r4 r2 |
\ffclef "alto/treble" <>^\markup\character-text Ismene Rec[itativo] senza rigor di tempo
mib''8 mib''16 mib'' re''8 do'' si'!8 si' r si'16 re'' |
re''4 fa'8 sol' mib'4 do''8 do''16 do'' |
la'8 la' la'16 la'32 la' la'16 re'' re''8 la' r la'16 sib' |
do''4 do''8 sib' \grace la' sol'4 r |
la'8 la'16 la' la' mi' mi' fa' sol'4 sol'8 la' |
\grace sol'8 fa'4 r la'8 la' la' re'' |
re'' la' r la'16 la' la'4 la'8 si'! |
sold'8 sold' r sold'16 la' si'4 si'8 do'' |
la'4 la'8 si'16 do'' si'8 si' si' fad'16 sol' |
la'8 la' r si' sol' sol' r16 si' dod'' re'' |
dod''8 dod'' r sold' sold' sold' sold' la' |
si' si' r si'16 re'' si'4 si'8 dod'' |
la'8 la' r fad'16 sol' la'4 sol'8 fad' |
si' si' r16 re'' si' la' fad'8 fad' r4 |
