\clef "treble" \transposition mib
R1*2 |
re''1~ |
re'' |
mi''~ |
mi'' |
re''4 r r2 |
mi''1 |
re''4 r r2 |
mi''1 |
re''~ |
re''~ |
re''2 r |
R1 |
r2 re''~ |
re''1~ |
re''2. r4 |
r2 r4 re'' |
r sol' r2 |
do''1~ |
do'' |
re''4 r r2 |
do''1 |
re''4 r r2 |
do''4 r r2 |
R1 |
do''2 re'' |
do''4 r r2 |
R1*13 |
