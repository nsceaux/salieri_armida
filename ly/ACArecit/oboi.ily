\clef "treble" R1*2 |
<>^\markup\concat { 1 \super o }
\grace sol'16 fa'32 mi' fa'16 r8 r4 \grace sib'16 la'32 sol' la'16 r8 r do'' |
la''2.( sol''8) fa'' |
\grace fa''16 mi''32 re'' mi''16 r8 r4 \grace la''16 sol''32 fad'' sol''16 r8 r sol''( |
re'''2.)( do'''8) sib'' |
la'' r r4 r r8 do'' |
sib''2.( la''8) sol'' |
la''4 r r r8 fa'' |
re'''2.( do'''8) \ficta sib'' |
la''32 sol'' la''16 r8 r4 \grace sol''16 fa''32 mi'' fa''16 r8 r4 |
\grace re''16 do''32 si'! do''16 r8 r4 \grace sib'?16 la'32 sol' la'16 r8 r4 |
fa'2 r |
R1 |
r2 \grace do''16 sib'32 la' sib'16 r8 r4 |
\grace mib''16 re''32 do'' re''16 r8 r fa''8( re'''2)~ |
re'''4 do'''8( sib'') la''4 r |
r2 r4 \grace sol''16 fa''32 mi''! fa''16 r8 |
r4 \grace mib''16 re''32 do'' re''16 r8 r2 |
\grace fa'16 mib'32 re' mib'16 r8 r4 \grace la'16 sol'32 fa' sol'8. r8 sib'( |
\once\slurDashed sol''2.)( fa''8 mib'') |
\override AccidentalSuggestion.avoid-slur = #'outside
re''4 r r r8 \ficta sib'( |
\once\slurDashed sol''2.)( fa''8 \ficta mib'') |
re''4 r r r8 \ficta sib'( |
sol''4) r r2 |
R1 |
\grace lab''?16 sol''32 fa'' sol''16 r8 r4 \grace mib''16 re''32 do'' re''16 r8 r4 |
mib''4 r \grace la''16 sol''32 \ficta fad'' sol''8. r4 |
r2 \grace la''16 sol''32 fa'' sol''8. r4 |
R1*12
