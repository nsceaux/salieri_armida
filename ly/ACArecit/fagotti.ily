\clef "bass" R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { do'1~ | do'~ | do' | mi2. do'4~ | do' }
  { la1~ | la | sib~ | sib | la4 }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { do'1~ | do'4 }
  { sib1 | la4 }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { mi2. do'4~ | do'1~ | do'~ | do'2 }
  { sib1 | la~ | la~ | la2 }
>> r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re'2~ | re'1~ | re'2 mib'4 }
  { sib2~ | sib1~ | sib2 do'4 }
>> r4 |
r2 r4 \twoVoices #'(primo secondo tutti) <<
  { do'4 | s re' s2 | sib1~ | sib | \ficta sib4 }
  { la4 | s fa s2 | sol1~ | sol | lab4 }
  { s4 | r s r2 | }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { sib1 | \ficta sib4 s2. | \ficta sib4 s2. | }
  { sol1 | lab4 s2. | sol4 s2. | }
  { s1 | s4 r r2 | s4 r r2 | }
>>
R1 |
\twoVoices #'(primo secondo tutti) <<
  { \ficta sib1~ | \ficta sib4 }
  { sol2 lab | sol4 }
>> r4 r2 | \allowPageTurn
R1*13 |
