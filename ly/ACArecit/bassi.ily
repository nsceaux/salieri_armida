\clef "bass" fa4 r fa r |
fa r fa,16 fa mi re do sib, la, sol, |
fa,8 r r4 fa8_\markup\italic piziccato r r4 |
fa,8 r r4 fa8 r r4 |
sol,8 r r4 sol8 r r4 |
mi,8 r r4 mi8 r r4 |
fa,8 r r4 fa8 r r4 |
sol8 r r4 do8 r r4 |
fa,8 r r4 fa8 r r4 |
mi,8 r r4 mi8 r r4 |
fa,8 r r4 fa8 r r4 |
fa,8 r r4 fa8 r r4 |
fa,2 r |
R1 |
r2 sib,8 r r4 |
sib8 r r4 sib,8 r r4 |
sib8 r r4 do4 r |
r2 r4 la |
r lab r2 |
mib,8 r r4 mib8 r r4 |
mib,8 r r4 mib8 r r4 |
fa8 r r4 sib,8 r r4 |
mib,8 r r4 mib8 r r4 |
fa8 r r4 sib,8 r r4 |
mib4 r r2 |
R1 |
mib8 r r4 fa8 r r4 |
mib4 r re r |
r2 do4 r |
fad4 r r2 |
r2 <>_\markup\italic [arco] sol4 r |
la4 r r2 |
re4 r r2 |
R1 |
re4 r r2 |
do4 r red r |
r2 mi4 r |
mid r r2 |
R1 |
fad4 r r2 |
re4 r r dod |
