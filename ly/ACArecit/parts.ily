\piecePartSpecs
#`((flauti #:score-template "score-deux-voix")
   (oboi #:score-template "score-voix")
   (corni #:score-template "score-voix"
          #:tag-global ()
          #:instrument , #{ \markup\center-column { Corno solo in Eb } #})
   (fagotti #:score-template "score-deux-voix")
   (violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Ubaldo
  \livretVerse#12 { Ecco l'onda insidiosa; ove col riso }
  \livretVerse#12 { Altri beve la morte. Io la ravviso }
  \livretVerse#12 { Alle fiorite sponde, }
  \livretVerse#12 { À lauti cibi, alle Sirene immonde. }
  \livretVerse#12 { Con quant’ arte dispose i neri inganni }
  \livretVerse#12 { L'accorta Maga! }
  \livretPers Ismene
  \livretVerse#12 { Ah dileguate amiche, }
  \livretVerse#12 { L'importuno timor. Guerra non reca }
  \livretVerse#12 { Alla tranquilla sede }
  \livretVerse#12 { Questo prode guerrier. D'ogni diletto }
  \livretVerse#12 { Vieni a parte con noi; vieni a deporre }
  \livretVerse#12 { Quell’ inutile acciaro }
  \livretVerse#12 { Fortunato stranier; dolce ristoro }
  \livretVerse#12 { T'offre la mensa, e il rio. De’ fieri mostri }
  \livretVerse#12 { La perigliosa guardia }
  \livretVerse#12 { Più a temer non ti resta; }
  \livretVerse#12 { Altra specie di pugna Amor t'appresta. }
}
#}))
