Ec -- co l’on -- da in -- si -- dio -- sa; o -- ve col ri -- so
al -- tri be -- ve la mor -- te. Io la rav -- vi -- so
al -- le fio -- ri -- te spon -- de,
a’ lau -- ti ci -- bi, al -- le si -- re -- ne im -- mon -- de.
Con quant’ ar -- te di -- spo -- se i ne -- ri in -- gan -- ni
l’ac -- cor -- ta ma -- ga.

Ah di -- le -- gua -- te a -- mi -- che,
l’im -- por -- tu -- no ti -- mor. Guer -- ra non re -- ca
al -- la tran -- quil -- la se -- de
que -- sto pro -- de guer -- rier. D’o -- gni di -- let -- to
vie -- ni a par -- te con noi; vie -- ni a de -- por -- re
quell’ in -- u -- ti -- le ac -- cia -- ro
for -- tu -- na -- to stra -- nier; dol -- ce ri -- sto -- ro
t’of -- fre la men -- sa, e il ri -- o. De’ fie -- ri mo -- stri
la pe -- ri -- glio -- sa guar -- dia
più a te -- mer non ti re -- sta;
al -- tra spe -- cie di pu -- gna A -- mor t’ap -- pre -- sta.
