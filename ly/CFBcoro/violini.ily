\clef "treble" <>\f <re' si' sol''>8 si'' q si'' |
sol''16 re'' si' re'' sol' si' re'' si' |
sol' si' re'' si' sol' si' re'' si' |
la' re'' fad'' re'' la'' fad'' re'' la' |
sol'' fad'' sol'' la'' sol'' mi'' la'' sol'' |
fad'' la'' fad'' mi'' re''-. re''( mi'' fad'') |
<re' si' sol''>8 si'' q si'' |
sol''16 re'' si' re'' sol' si' re'' si' |
sol' si' re'' si' sol' si' re'' si' |
la' re'' fad'' re'' la'' fad'' re'' la' |
sol'' fad'' sol'' la'' sol'' mi'' la'' sol'' |
fad'' la'' fad'' mi'' re'' sol'' si'' sol'' |
mi'' sold'' la'' si'' do''' mi'' fad'' mi'' |
re'' fad'' sol''! la'' si'' re'' mi'' re'' |
do'' mi'' fad'' sol'' la'' do'' re'' do'' |
si' re'' do'' mi'' re'' mi'' fad'' sol'' |
fad'' sol'' mi'' fad'' re'' mi'' do'' re'' |
si' re'' si' do'' re'' sol'' si'' sol'' |
mi'' sold'' la'' si'' do''' mi'' fad'' mi'' |
re'' fad'' sol''! la'' si'' re'' mi'' re'' |
do'' mi'' fad'' sol'' la'' do'' re'' do'' |
si' re'' mi'' fad'' sol'' si' do'' si' |
la' do'' re'' mi'' fad'' la' si' la' |
sol' si' do'' re'' mi'' sol' la' sol' |
fad' la' si' do'' re'' fad' sol' fad' |
mi' sol' la' si' do'' mi' fad' mi' |
re' fad' sol' la' si' re' mi' re' |
re' si' re'' do'' si' do'' si' la' |
<sold' si> q8 q q q16~ |
q16 q8 q q q16 |
\custosNote q
