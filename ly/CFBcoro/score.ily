\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\center-column { Basso Fagotto }
      shortInstrumentName = \markup\center-column { B. Fg. }
    } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s2*4 s4 \bar "" \pageBreak
        s4 s2*4\pageBreak
        s2*4 s4 \bar "" \pageBreak
        s4 s2*4\pageBreak
        s2*5\pageBreak
        s2*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
