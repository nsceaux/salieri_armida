\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (fagotti #:notes "bassi")

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Coro delle donne
  \livretVerse#10 { Ah barbaro, ah senti, }
  \livretVerse#10 { Rivolgi le vele }
  \livretVerse#10 { Ah mira, crudele, }
  \livretVerse#10 { De tuoi tradimenti }
  \livretVerse#10 { Il frutto qual è. }
} #}))
