<<
  \tag #'voix1 {
    \clef "soprano/treble" R2 |
    r4 r8 re'' |
    re''8. si'16 si'8 re'' |
    re''4 la'8 re'' |
    sol''4 sol''8 sol'' |
    fad'' re'' r4 |
    R2 |
    r4 r8 re'' |
    re''8. si'16 si'8 re'' |
    re''8. la'16 la'8 re'' |
    sol''4 sol''8 sol'' |
    fad'' re'' r re'' |
    mi''4 mi''8 mi'' |
    re''4 re''8 re'' |
    do''4 do''8 do'' |
    si'[ do''] re'' sol'' |
    fad''[ mi''] re'' do'' |
    si'4 r8 re'' |
    mi''4 mi''8 mi'' |
    re''4 re''8 re'' |
    do''4 do''8 do'' |
    si'4 si'8 si' |
    la'4 la'8 la' |
    sol'4 r |
  }
  \tag #'voix2 {
    \clef "alto/treble" R2 |
    r4 r8 si' |
    si'8. sol'16 sol'8 si' |
    la'4 fad'8 re'' |
    dod''4 la'8 la' |
    la' la' r4 |
    R2 |
    r4 r8 si' |
    si'8. sol'16 sol'8 si' |
    la'8. fad'16 fad'8 re'' |
    dod''4 la'8 la' |
    la' la' r sol' |
    sol'4 la'8 la' |
    la'4 sol'8 sol' |
    sol'4 re'8 re' |
    re'[ do'] si si' |
    la'[ fad'] fad' fad' |
    sol'4 r8 sol' |
    sol'4 la'8 la' |
    la'4 sol'8 sol' |
    sol'4 re'8 re' |
    re'4 mi'8 mi' |
    mi'4 re'8 re' |
    sol'4 r |
  }
  \tag #'voix3 {
    \clef "tenor/G_8" R2 |
    r4 r8 re' |
    re'8. re'16 re'8 re' |
    re'4 re'8 re' |
    mi'4 dod'8 dod' |
    re' re r4 |
    R2 |
    r4 r8 re' |
    re'8. re'16 re'8 re' |
    re'8. re'16 re'8 re' |
    mi'4 dod'8 dod' |
    re' re' r re' |
    re'4 do'8 do' |
    do'4 si8 si |
    si4 la8 la |
    sol[ la] si re' |
    re'[ do'] si la |
    \grace la sol4 r8 re' |
    re'4 do'8 do' |
    do'4 si8 si si4 la8 la |
    la4 sol8 sol |
    sol4 fad8 fad8 |
    sol4 r |
  }
  \tag #'voix4 {
    \clef "bass" R2 |
    r4 r8 sol |
    sol8. sol16 sol8 sol |
    fad4 fad8 fad |
    mi4 mi8 mi |
    re re r4 |
    R2 |
    r4 r8 sol |
    sol8. sol16 sol8 sol |
    fad8. fad16 fad8 fad |
    mi4 mi8 mi |
    re re r si |
    do'4 la8 la |
    fad4 sol8 sol |
    mi4 fad8 fad |
    sol4 sol8 sol |
    re'4 re8 re |
    sol4 r8 si |
    do'4 la8 la |
    fad4 sol8 sol |
    mi4 fad8 fad |
    sol4 mi8 mi |
    do4 re8 re |
    sol4 r |
  }
>>
R2*6 |
