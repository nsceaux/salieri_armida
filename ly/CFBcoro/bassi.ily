\clef "bass" sol8\f sol, sol sol, |
sol sol, sol sol |
sol2:8 |
fad: |
mi8 mi mi mi |
re re, re16-. \once\slurDashed re( mi fad) |
sol8 sol, sol sol, |
sol sol, sol sol |
sol2:8 |
fad: |
mi: |
re8 re si si |
do' do' la la |
fad fad sol sol |
mi mi fad fad |
sol sol sol, sol |
re' re' re re |
sol sol, sol si |
do' do' la la |
fad fad sol sol |
mi mi fad fad |
sol sol mi mi |
do do re re |
si, si, do do |
la, la, si, si, |
sol, sol, la, la, |
fad, fad, sol, sol, |
fa, fa, fa, fa, |
mi, fa, mi, fa, |
mi, fa, mi, fa, |
\custosNote mi,16
