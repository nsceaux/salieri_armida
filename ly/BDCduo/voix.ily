<<
  \tag #'(armida basse) {
    <<
      \tag #'basse { s1*16 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { \clef "soprano/treble" R1*16 | }
    >>
    mi''4 mi'\fermata r2\fermata |
    \grace mi''8 re''!4 dod''8 si' si'4. la'16 sold' |
    sold'8.[ la'32 si'] la'4 r la'8[ dod''] |
    \grace dod''8 si'4 la'8[ dod''] \grace dod''8 si'4 \tuplet 3/2 { la'8([ dod'' mi'']) } |
    \grace mi''8 re''4 dod'' r mi''16[ dod'' re'' si'] |
    \grace sold'8 la'4~ la'16 dod''[ mi'' dod'']
    \grace lad'8 si'4. dod''16[ re''] |
    \grace re''16 dod''8[ si'16 la'] la'4 r2 |
    la'8.[ dod''16] dod''8. mi''16 mi''4. fad''16[ sol''] |
    \grace sol''8 fad''4 fad'' r2\fermata |
    re''4-! dod''-! r2\fermata |
    fad''4 mi''16[ re''] dod''[ si'] mi''8.[ fad''16] \grace la'8 sold'4 |
    la' r <<
      \tag #'basse { s2 s4 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 | r4 }
    >> r8 re'' re''8.[ mi''32 fad''] mi''8 re'' |
    re''8 \ficta do''4. r4 r8 la' |
    la'4. la'8 la''4. la''8 |
    \grace la''8 sold''[ fad''16 mi''] mi''4 <<
      \tag #'basse { s2 s1*2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 | R1*2 | }
    >>
    \grace re''8 do''4 si'8 r16 re'' \grace re''8 do''4 r8 si'16[ la'] |
    \grace la'8 sold'4 sold' r r8 mi'' |
    fad''4. fad''8 mi''4. mi''8 |
    re''4 re'' r r8 red'' |
    mi''4. mi''8 re''!4. re''8 |
    dod''4 dod'' r2 |
    R1 |
    r2 r4 re''8[ mi''16 fad''] |
    \grace fad''8 mi''4 dod''8[ re''16 mi''] \grace mi''8 re''4 dod''8[ si'] |
    \grace si'4 dod''2 r |
    R1 |
    r2 r4 re''8[ mi''16 fad''] |
    \grace fad''8 mi''4 dod''8[ re''16 mi''] \grace mi''8 re''4 dod''8[ si'] |
    dod''[ la''] sold''[ fad''] mid''[ fad''] \grace mi''8 re'' dod''16[ si'] |
    mi''4. fad''8 dod''4 si' |
    la'2 r |
    <<
      \tag #'basse { s1 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { R1 | r2 }
    >> r4 la'' |
    fad'' re'' r2 |
    <<
      \tag #'basse { s1 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { R1 | r2 }
    >> r8 mi'' mi'' la'' |
    fad''4 <<
      \tag #'basse { re''8 s s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { re''4 r2 | }
    >>
    fad''2 fad''4. fad''8 |
    re''8. dod''16 si'4 r8 si' si' si' |
    dod''2 red'' |
    mi''4 mi'' r mi''8([ fad'']) |
    sol''4 mi'' dod'' si' |
    fad''4 fad' r2 |
    <<
      \tag #'basse { s1*7 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { R1*7 | r2 }
    >> r4 r8 re'' |
    sol''[ fad''] mi'' re'' do''[ si'] la' sol' |
    re''4 re'' r2 |
    mib''4 mib'' r r8 mib'' |
    re''4 re'' <<
      \tag #'basse { s2 s1*2 \ffclef "soprano/treble" <>^\markup\character Arm. }
      \tag #'armida { r2 | R1*2 | }
    >>
    fa''4 re'' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Arm. }
      \tag#'armida { r2 | r }
    >> r4 mi'' |
    fa'' re'' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag#'armida { r2 | r }
    >> r4 r8 mi'' |
    la''4 mi''8 mi'' re''4 dod''8 dod'' |
    si'4 si' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 | r }
    >> la''2 |
    sold''4 mi''<<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 | r }
    >> la''2 |
    sold''4 mi'' r la'' |
    sold'' mi'' r2 |
    %%
    la''2. sold''4 |
    \grace sold''4 fad''2 fad''4 mi'' |
    \grace mi''4 re''2 re''4 dod'' |
    \grace dod''4 re''2 re'' |
    si''2. la''4 |
    \grace la''4 sold''2 sold''4 fad'' |
    \grace fad'' mi''2 mi''4 re'' |
    \grace re''4 dod''2 dod'' |
    mi''2. mi''4 |
    fad''1 |
    la''2. la''4 |
    la''1~ |
    la''~ |
    la'' |
    la''2. la''4 |
    sold''1 |
    fad''2. fad''4 |
    \grace fad''4 mi''2 mi'' |
    sold''2. sold''4 |
    la''2 fad''4. fad''8 |
    mi''2 sold''4. sold''8 |
    la''2 fad'' |
    mi'' sold''4. sold''8 |
    la''2 fad'' |
    mi''2 mi''4. mi''8 |
    la'2 r |
    R1*3 |
    r2
  }
  \tag #'(rinaldo basse) {
    \clef "soprano/treble" R1*2 |
    r2 r4 \tag #'basse <>^\markup\character Rinaldo r8 la' |
    mi''4. re''16[ dod''] dod''8[ si'] la'[ sold'] |
    la'4 la' r la'8[ dod''] |
    \grace dod''8 si'4 la'8 dod'' \grace dod'' si'4 \tuplet 3/2 { la'8[ dod'' mi''] } |
    \grace mi''8 re''4 dod'' r mi''16[ dod'' re'' si'] |
    \grace sold'8 la'4~ la'16 dod''[ mi'' dod''] \grace lad'8 si'4. dod''16[ re''] |
    \grace re''16 dod''8[ si'16 la'] la'4 r2 |
    la'4 la'8 mi'' red''\fermata red'' r red'' |
    mi''4( si'4.) mi''8 red'' dod'' |
    si'2 la' |
    sold'4 red''8([ mi'']) mi''[ dod''] \grace re''?8 dod''[ si'16 la'] |
    sold'4 r fad' r |
    sold''8([ fad'' mi'' red'']) mi''([ re''? dod'' si']) |
    dod''8.[ red''32 mi''] \grace red''16 dod''8[ si'16 la'] sold'4 fad' |
    <<
      \tag #'basse { s1*11 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo {
        mi'4 r r2\fermata |
        R1*10 |
        r2
      }
    >> r4 r8 mi'' |
    \grace mi''8 re''4 <<
      \tag #'basse { s2. s1*2 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo {
        r4 r2 |
        r2 r4 r8 la' |
        red''4. red''8 red''4. red''8 |
        mi''4 mi'
      }
    >> r4 r8 mi'' |
    re''!4 do'' si'4. do''16[ la'] |
    sold'8.[ la'16] si'4 r2 |
    <<
      \tag #'basse { s1*16 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo {
        R1 |
        r2 r4 r8 la' |
        la'4. la'8 lad'4. lad'8 |
        si'4 si' r r8 si' |
        sold'4. sold'8 si'4. si'8 |
        la'4 la' r2 |
        R1 |
        r2 r4 si'8[ dod''16 re''] |
        \grace re''8 dod''4 la'8[ si'16 dod''] \grace dod''8 si'4 la'8[ sold'] |
        \grace sold'4 la'2 r |
        R1 |
        r2 r4 si'8[ dod''16 re''] |
        \grace re''8 dod''4 la'8[ si'16 dod''] \grace dod''8 si'4 la'8[ sold'] |
        la'[ fad''] mi''[ re''] dod''[ re''] \grace dod''8 si' la'16[ sold'] |
        dod''4. re''8 la'4 sold' |
        %%
        la'2 r |
      }
    >>
    r2 r8 la' si' dod'' |
    re''4 re'' <<
      \tag #'basse { s2 s1 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | R1 | }
    >>
    r2 r4 r8 la' |
    re''4 re'' <<
      \tag #'basse { s2 s4. \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | r4 r8 }
    >> fad''8 fad''4 fad' |
    <<
      \tag #'basse { s1*6 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { R1*6 | }
    >>
    re''2 r |
    fad''4 re'' r8 re'' re'' do'' |
    si'4 si' r mi''8[ sol''] |
    \grace sol''8 fad''4 mi''8 re'' la''4 la' |
    re'' r r2 |
    R1*2 |
    re''4\fermata re' <<
      \tag #'basse { s2 s1*3 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | R1*3 | r2 }
    >> r4 r8 re'' |
    fad''4 fad'' r r8 fad'' |
    sol''4 re'' r2 |
    <<
      \tag #'basse { s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 }
    >> r4 re'' |
    dod''! dod'' <<
      \tag #'basse { s2 s \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | r }
    >> r4 re'' |
    dod''4 dod'' <<
      \tag #'basse { s2 s1 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | R1 | r2 }
    >> r4 r8 si' |
    do''4 do'' <<
      \tag #'basse { s2 s \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | r }
    >> r4 si' |
    do'' do'' <<
      \tag #'basse { s2 s1*2 }
      \tag #'rinaldo { r2 | R1*2 }
    >>
    %%
    \tag #'rinaldo {
      dod''2. mi''4 |
      \grace mi''4 re''2 re''4 dod'' |
      \grace dod''4 si'2 si'4 lad' |
      \grace lad' si'2 si' |
      sold''2. mi''4 |
      \grace fad''4 mi''2 mi''4 re'' |
      \grace re''4 dod''2 dod''4 si' |
      la'2 la' |
      la'2. la'4 |
      la'2( re'') |
      fad''2. fad''4 |
      fad''1~ |
      fad''~ |
      fad'' |
      fad''2. fad''4 |
      mi''1 |
      re''2. re''4 |
      \grace re''4 dod''2 dod'' |
      si'2. si'4 |
      dod''2 re''4. re''8 |
      dod''2 si'4. si'8 |
      dod''2 re'' |
      dod'' si'4. si'8 |
      dod''2 re'' |
      dod''2 si'4. sold'8 |
      la'2 r |
      R1*3 |
      r2
    }
  }
>>
