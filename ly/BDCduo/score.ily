\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new Staff \with { \corniAInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>

      \new Staff \with {
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Arm.
      } \withLyrics <<
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Rin.
      } \withLyrics <<
        \global \keepWithTag #'rinaldo \includeNotes "voix"
      >> \keepWithTag #'rinaldo \includeLyrics "paroles"

      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion {
          s1*49\break
          s1*21\break
          s1*17\break
        }
        \origLayout {
          s1*4\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 5
          \grace s8 s1*4 s2 \bar "" \pageBreak
          \grace s8 s2 s1*3\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 10
          s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 15
          s1*4\pageBreak
          s1*4\pageBreak
          \grace s8 s1*3 s2 \bar "" \pageBreak
          s2 s1*3\pageBreak
          s1*3\pageBreak
          %% 20
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 25
          s1*5\pageBreak
          s1*5\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
