\clef "treble" \transposition la
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { do'' }
>> r4 r2 |
r2 r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { re''4 | re''2( mi''4) }
  { sol'4 | sol'2. }
>> r4 |
R1*5 |
\tag#'tutti <>^"a 2." do''2-\sug\p r |
do''2-\sug\p~ \once\override Script.avoid-slur = #'outside do''4-\sug\f\fermata r4 |
R1*13 | \allowPageTurn
r2 do'' |
do'' r\fermata |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { re''4-! do''-! }
  { sol'-! do''-! }
>> r2\fermata |
R1 |
\tag#'tutti <>^"a 2." do''1-\sug\p |
do''2 sol' |
do''1 |
do''4 r do''2-\sug\f |
sol'4 sol' r2 |
R1*4 |
do''4.-\sug\f do''8-\sug\p mi''4.-\sug\f mi''8-\sug\p |
re''4-\sug\f r r2 |
re''4.-\sug\f re''8-\sug\p re''4.-\sug\f re''8-\sug\p |
do''4-\sug\f r r2 |
R1 |
r4 do''2-\sug\p re''4 |
mi'' r r2 |
R1*2 |
r4 do''2 re''4 |
mi'' r r2 |
do''4\f r do'' r |
\twoVoices #'(primo secondo tutti) <<
  { mi''2 mi''4 re'' | do''4 }
  { do''2 do''4 sol' | mi'4 }
>> r4 r2 | \allowPageTurn
%%
R1*8 |
\tag#'tutti <>^"a 2." re''4-\sug\fp r do''-\sug\fp r |
re''-\sug\fp r r2 |
R1*2 |
do''4.-\sug\f do''8 do''4. do''8 |
do''4 do'' r2 |
R1*6 |
%%
R1*17 |
%%
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''1 | }
  { mi' | }
>>
R1*3 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { re''1 | }
  { sol' | }
>>
R1*6 |
\tag#'tutti <>^"a 2." do''1-\sug\ff~ |
do''~ |
do'' |
re''1~ |
re''2-\sug\p r |
R1*2 |
sol'1-\sug\f |
do''1*1/2 s2-\sug\p |
\twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' |
    mi'' fa'' |
    mi'' do'' |
    mi'' fa'' |
    mi'' re'' | }
  { do''2 sol' |
    do'' sol' |
    do'' do'' |
    do'' sol' |
    do'' sol' | }
  { s2 s-\sug\f |
    s s-\sug\p |
    s2 s-\sug\f |
  }
>>
\tag#'tutti <>^"a 2." do''4 do' mi' sol' |
do'' do'' sol' mi' |
do' do' mi' sol' |
do'' do'' sol' mi' |
do'4 r
