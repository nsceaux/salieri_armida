\tag #'(rinaldo basse) {
  Di -- le -- gua il tuo ti -- mo -- re,
  se -- re -- na i me -- sti ra -- i,
  se -- re -- na i me -- sti ra -- i;
  sai ch’io t’a -- do -- ro, e sai,
  ch’io mo -- ri -- rò per te,
  ch’io mo -- ri -- rò per te, __
  ch’io __ mo -- ri -- rò per \tag #'rinaldo te.
}
\tag #'(armida basse) {
  Ta -- ci, ta -- ci, che ac -- cre -- sci al co -- re
  il suo mor -- ta -- le af -- fan -- no
  il __ suo mor -- ta -- le af -- fan -- no.
  L’i -- ra del Ciel ti -- ran -- no
  tut -- ta
  tut -- ta si sfo -- ghi in me.
}
\tag #'(rinaldo basse) {
  Mio ben.
}
\tag #'(armida basse) {
  Mio dol -- ce a -- mo -- re.
}

Che bar -- ba -- ro mo -- men -- to,

\tag #'(rinaldo basse) {
  Io tre -- mo al tuo ter -- ro -- re
}
\tag #'(armida basse) {
  L’al -- ma man -- car mi sen -- to
}

Né in -- ten -- do il mio spa -- ven -- to,
né in -- ten -- do il mio spa -- ven -- to
né pos -- so dir per -- ché,
né pos -- so dir per -- ché,
né pos -- so,
né pos -- so dir per -- ché.

\tag #'(rinaldo basse) {
  Ma il mio soc -- cor -- so?
}
\tag #'(armida basse) {
  È va -- no.
}
\tag #'(rinaldo basse) {
  L’a -- mo -- re…
}
\tag #'(armida basse) {
  È mio pe -- ri -- glio…
}
\tag #'(rinaldo basse) {
  Il Cie -- lo…
}
\tag #'(armida basse) {
  Ah de’ suoi ful -- mi -- ni
  già ba -- le -- nar sul ci -- glio
  mi ve -- do ac -- ce -- so il lam -- po.
}
\tag #'(rinaldo basse) {
  No, mi -- ra al no -- stro scam -- po
  qual’ ar -- me il Ciel mi dié;
  ve -- di!
}
\tag #'(armida basse) {
  Oh stel -- le! che lu -- ce fu -- ne -- sta!
  Fug -- gi t’as -- con -- di…
}
\tag #'(rinaldo basse) {
  Ma sen -- ti, t’ar -- re -- sta.
}
\tag #'(armida basse) {
  Fug -- gi t’as -- con -- di…
}
\tag #'(rinaldo basse) {
  Ma sen -- ti, t’ar -- re -- sta.
}
\tag #'(armida basse) {
  Che lu -- ce, che lu -- ce, fu -- ne -- sta!
}
\tag #'(rinaldo basse) {
  Ma sen -- ti,
}
\tag #'(armida basse) {
  Ah fug -- gi
}
\tag #'(rinaldo basse) {
  t’ar -- re -- sta.
}
\tag #'(armida basse) {
  t’as -- con -- di…
  Ah fug -- gi.
}

Ah, che stra -- na vi -- cen -- da è mai que -- sta,
ah, che stra -- na vi -- cen -- da è mai que -- sta,
no, più or -- ri -- bil la mor -- te non è,
no, più or -- ri -- bil
no, più or -- ri -- bil la mor -- te non è,
la mor -- te non è,
la mor -- te non è.
