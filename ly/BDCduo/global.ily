\tag #'all \key la \major
\tempo "Andante non troppo" \midiTempo#100
\time 4/4 s1*49 \bar "||"
\tempo "Più andante" s1*21 \bar "||"
\tempo "Allegro assai" \midiTempo#120 s1*17 \bar "||"
\tempo "Presto" \midiTempo#240 \time 2/2 s1*29 s2 \bar "|."
