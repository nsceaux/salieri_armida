\clef "treble" R1 |
\twoVoices #'(primo secondo tutti) <<
  { r4 mi''8(_\markup\italic dol. la'') fad''( re'' si' mi'') |
    \grace re''4 dod''2. }
  { r2 r4 \tag #'secondo <>-\sug\p \once\slurDashed si'( | la'2.) }
>> r4 |
R1*5 |
r4 r8 <>\rf \twoVoices #'(primo secondo tutti) <<
  { la''8 sold''( fad'' mi'' re'') |
    dod''2( \once\override Script.avoid-slur = #'outside red''4)\fermata }
  { fad''8 mi''( re'' dod'' si') |
    la'2 la'4\fermata }
  { s8 s2 | s2\p s4\f }
>> r4 |
R1*13 |
r2 \twoVoices #'(primo secondo tutti) <<
  { sol''2 | fad'' }
  { mi''2 | re'' }
>> r2\fermata |
<>\f \twoVoices #'(primo secondo tutti) <<
  { re''4-! dod''-! }
  { si'-! la'-! }
>> r2\fermata |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { \slurDashed la'8( mi'') do''( mi'') \slurSolid do''( mi'') do''( mi'') |
    si'( re'') si'( re'') si'( re'') si'( re'') |
    la'8( \ficta do'') la'( do'') la'( do'') la'( do'') |
    la'4 }
  { la'8( do'') la'( do'') la'( do'') la'( do'') |
    fa''2 mi'' |
    mi''1 |
    la'4 }
>> r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { la''2 | \grace la''8 sold'' fad''16 mi'' mi''4 }
  { red''2 | mi''4 sold' }
>> r2 | \allowPageTurn
R1*3 |
r2 r4 r8 \twoVoices #'(primo secondo tutti) <<
  { mi''8 | fad''4. fad''8 mi''4. mi''8 | re''4 }
  { la'8 | la'4. la'8 lad'4. lad'8 | si'4 }
  { s8\f | s4. s8\p s4.\f s8\p | s4\f }
>> r4 r r8 \twoVoices #'(primo secondo tutti) <<
  { red''8 | mi''4. mi''8 re''!4. re''8 | dod''4 }
  { si'8 | sold'4. sold'8 si'4. si'8 | la'4 }
  { s8\f | s4. s8\p s4.\f s8\p | s4\f }
>> r4 r2 |
R1 |
r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { fad''16 sold'' la'' fad'' re''4 }
  { la'4 la' }
>> r4 |
R1*3 | \allowPageTurn
r4 \twoVoices #'(primo secondo tutti) <<
  { fad''16 sold'' la'' fad'' re''4 }
  { re''4 re'' }
>> r4 |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { dod''8( la'' sold'' fad'') mid''( fad'' \grace mi''8 re'' dod''16 si') |
    mi''4. fad''8 dod''4 si' |
    la'4 }
  { la'8( fad'' mi'' re'') dod''( re'' \grace dod'' si' la'16 sold') |
    dod''4. re''8 la'4 sold' |
    la'4 }
>> r4 r2 | \allowPageTurn
R1*6 |
\twoVoices #'(primo secondo tutti) <<
  { fad''1 | re''8. dod''16 si'4 }
  { la'8-. si'-. dod''-. re''-. mi''-. re''-. mi''-. dod''-. |
    si'4 r }
  { s1\f | s2-\sug\p }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { dod''4 s \ficta red'' s | mi'' }
  { mi'4 s fad' s | sol' }
  { s4-\sug\fp r s-\sug\fp r | s-\sug\fp }
>> r4 r2 |
R1*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { re''4. re''8 mi''4. mi''8 | fad''4 re'' }
  { fad'4. fad'8 la'4. la'8 | la'4 fad' }
>> r2 |
R1*2 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { re''2 mi'' |
    fad'' sol'' |
    la'' si''16 la'' \ficta sol'' fad'' sol'' la'' si'' dod''' |
    re'''4\fermata re'' }
  { fad'2 la' |
    re'' mi'' |
    fad'' re''4. \ficta sol''8 |
    fad''4\fermata re'' }
  { s1-\sug\p-\sug\cresc |
    s1 |
    s2-\sug\f s-\sug\ff }
>> r2 |
%%
R1*17 |
%%
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { la''1 | }
  { dod'' | }
>>
R1*3 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { si''1 | }
  { sold'' | }
>>
R1*6 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { la''1~ | la''~ | la'' | la''1 | sold''2 }
  { fad''1~ | fad''~ | fad'' | fad'' | mi''2 }
  { s1*4 | s2-\sug\p }
>> r2 |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { sold''2. sold''4 |
    la''2 fad'' |
    mi'' sold'' |
    la'' fad'' |
    mi'' sold'' |
    la'' re''' |
    dod''' si'' |
    la''1 |
    la''~ |
    la''~ |
    la'' |
    la''4 }
  { si'2. si'4 |
    dod''2 re'' |
    dod'' si' |
    dod'' re'' |
    dod'' si' |
    dod'' si'' |
    la'' sold'' |
    la'' dod''~ |
    dod''1~ |
    dod''~ |
    dod'' |
    dod''4 }
  { s1-\sug\f | s2 s-\sug\p | s s-\sug\f | s2 s-\sug\p | s s-\sug\f | }
>> r4
