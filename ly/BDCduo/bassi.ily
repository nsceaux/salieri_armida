\clef "bass" la8.\f la16 sold8.\trill fad16 mi8.\trill re16 dod8.\trill si,16 |
la,4 r r sold(\p |
la2) la,4 r |
la r mi r |
fad4 fad8. mi32 re dod4 dod'(\rf |
re') dod'(\rf re') dod'(\rf |
si) la\p la, r |
la la sold mi |
la la, r2 |
\once\slurDashed la2(-\sug\p \once\override Script.avoid-slur = #'outside fad4)\f\fermata r4 |
r mi\p mi mi |
mi r si, r |
mi sold la la |
si r si, r |
mi2( sold) |
la4\f la si si, |
mi2\fermata r2 |
r re'\p~ |
re'4 dod' dod dod'-\sug\rf( |
re') dod'-\sug\rf( re') dod'-\sug\rf( |
si) la la, r |
la la sold mi |
la la8 la,16\f si,32 dod re8. mi16 fad8. sold16 |
la4 r r la, |
re re, r2\fermata |
mi4-!\f fad-! r2\fermata |
re4\f fad\p mi mi, |
la,\p r la r |
la r sold r |
la r la, r |
fa2:8\cresc fa:\f |
mi4 mi, r2 |
R1 |
mi4\p r r2 |
R1 |
mi2\p r4 r8 dod'\f |
re'4. re'8\p dod'4.\f dod'8\p |
si8\f si,16. si,32 si,8 si, si,4 r |
mi4.\f mi8\p sold4.\f sold8\p |
la8\f la,16. la,32 la,8 la, la,4 r |
R1 |
r2 r4 sold(\p |
la) la, mi mi, |
la, la la, r |
R1 |
r2 r4 sold\p |
la la, mi mi, |
la,\f r re r |
mi8 mi mi mi mi, mi, mi, mi, |
%%
la,4 r r2 | \allowPageTurn
r2 la-\sug\fp |
fad4 r mi r |
re r r2 |
r2 la2\fp |
fad4 r dod r |
re r lad r |
fad8-.\f sold-. lad-. si-. dod'-. si-. dod'-. lad-. |
si2:8\p si:\fp |
si:\fp la:\fp |
sol:\fp sol:\fp |
mi: sol: |
fad:-\sug\f fad4 r |
re4.\f re8 dod4. dod8 |
re4 re fad\p fad |
sol sol sol, la, |
re8-! fad( mi re) la4 la, |
re2:8\p\cresc re: |
re: re: |
re:\f sol8-\sug\ff sol mi mi |
re2\fermata r4 r8 re'\f |
%%
\once\slurDashed sol'( fad') mi'-. re'-. do'-. si-. la-. sol-. |
re'8 r re'\p r re' r re' r |
re' r re' r re' r re' r |
re' r re' r re' r re' r |
do' r do' r do' r do' r |
sib r sib r sib r la r |
sold!\f r la\p r si r sold r |
la r dod'! r mi' r la r |
sold\f r la\p r si r sold r |
la r dod' r mi' r la r |
dod\f dod dod dod re re red red |
mi r mi\p r mi r mi r |
mi r mi r mi2:8\f |
mi8 r mi\p r mi r mi r |
mi r mi r mi2:8\f |
mi8 r mi\p r mi r mi r |
mi4 r r2 |
%%
la,2:8\f la,: |
la,:-\sug\p la,: |
la: la: |
mi: mi: |
mi:-\sug\f mi: |
mi:-\sug\p mi: |
mi: mi: |
la: la,: |
dod': dod': |
re': re': |
red':\f red': |
\ficta red'4-!\ff fad'-! red'-! do'-! |
la-! do'-! la-! fad-! |
red-! fad-! red-! do-! |
si,-! \ficta red-! fad-! si-! |
mi2:8\p mi: |
mi: mi: |
mi: mi: |
mi:\f mi: |
la,: re:\p |
mi: mi,:\f |
la,: re:\p |
mi: mi,:\f |
la,: re: |
mi: mi,: |
la,4 la, dod mi |
la la mi dod |
la, la, dod mi |
la la mi dod |
la,4 r

