\clef "treble" la'8.\f la'16 sold'8.\trill fad'16 mi'8.\trill re'16 dod'8.\trill si16 |
la4 <<
  \tag #'primo {
    mi''8(_\markup\italic dol. la'') fad''8( re'' si' mi'') |
    \grace re''4 dod''2. r4 |
    mi''4. re''16 dod'' dod''8 si' la' sold' |
    la'4 r r la''(\rf |
    si'') la''4(\rf si'') la''(\rf |
    sold'') la'' r mi''8-! re''-! |
    dod''4 dod'' dod'' si'8 dod''16 re'' |
    dod''4( la'8) la''\rf sold''( fad'' mi'' re'') |
    dod''2(\p \once\override Script.avoid-slur = #'outside red''4)\f\fermata r4 |
    r sold''~ sold''8 dod'''(\rf si'' la'') |
    r8 sold''-.( sold''-. sold''-.) r fad''8-.( fad''-. fad''-.) |
    mi''4 red''16(\rf mi''8.) mi''16( dod''8.) dod''8( la') |
    r8 sold'-.( sold'-. sold'-.) r fad'-.( fad'-. fad'-.) |
    sold'2( si') |
    dod''8.(\f red''32 mi'' \grace red''8 dod'' si'16 la') sold'4 fad' |
    mi'2\fermata r |
    r2 si'4.(\p la'8) |
    sold'4( la') r la''(\rf |
    si'') la''(\rf si'') la''(\rf |
    sold'') la'' r4 mi''8-! re''-! |
    dod''4 dod'' si' si' |
    \grace re''16 dod''8 si'16 la' la'8
  }
  \tag #'secondo {
    r4 r mi'-\sug\p~ |
    mi'2. r4 |
    dod''4 r sold' r |
    fad'4 fad'8.\trill mi'32 re' dod'4 mi'-\sug\rf( |
    fad') mi'-\sug\rf( fad') mi'-\sug\rf( |
    mi') mi' r dod''8-! si'-! |
    la'4 mi' mi' sold'8 la'16 si' |
    la'4. fad''8-\sug\rf mi''( re'' dod'' si') |
    la'2-\sug\p la'4-\sug\f\fermata r4 |
    r si'~ si'8 mi''(-\sug\rf red'' dod'') |
    r8 si'-.( si'-. si'-.) r la'-.( la'-. la'-.) |
    sold'4 si'-\sug\rf dod'' mi' |
    r8 mi'-.( mi'-. mi'-.) r red'-.( red'-. red'-.) |
    mi'2~ mi' |
    mi'4-\sug\f la'8 sold'16 fad' mi'4 red' |
    mi'2\fermata r |
    r2 fad'-\sug\p |
    mi'4 mi' r mi'(-\sug\rf |
    fad') mi'-\sug\rf( fad') mi'-\sug\rf( |
    mi') mi' r dod''8-! si'-! |
    la'4 mi' mi' mi' |
    mi'8 re'16 dod' dod'8
  }
>> la16\f si32 dod' re'8. mi'16 fad'8. sold'16 |
<<
  \tag #'primo {
    la'8. dod''16 dod''8. mi''16 mi''4. fad''16 sol'' |
    fad''2 r\fermata |
    re''4\f-! dod''-! r2\fermata |
    <re' la' fad''>4\f mi''16(\p re'' dod'' si') mi''8. fad''16 \grace la'8 sold'4 |
    la'8(_\markup\italic dol. mi'') do''( mi'') do''( mi'') do''( mi'') |
    si'( re'') si'( re'') si'( re'') si'( re'') |
    la'( do'') la'( do'') la'( do'') la'( do'') |
    la'16\cresc red'' red'' red'' red'' red'' red'' red'' red''2:16\f |
    mi''4 mi' r r8 mi''(\p |
    re''4 do'' si'4. do''16 la') |
    \once\slurDashed sold'8.( la'16) si'4. si'8(\rf mi'' re'') |
    do''4(\p si'8.) re''16 \grace re''8 do''4. si'16 la' |
    \grace la'8 sold'4 sold' r r8 mi''\f |
    <la' fad''>4. q8\p mi''4.\f mi''8\p |
    re''8\f
  }
  \tag #'secondo {
    la'4 la'8. dod''16 dod''4. re''16 mi'' |
    la'2 r\fermata |
    sold'!4-!-\sug\f la'-! r2\fermata |
    la'4-\sug\f la'-\sug\p la' \grace la'8 sold'4 |
    \slurDashed la'8(_\markup\italic\small dol. do'') la'( do'') \slurSolid la'( do'') la'( do'') |
    fa'2 mi' |
    do'8 mi' do' mi' do' mi' do' mi' |
    do'16-\sug\cresc la' la' la' la' la' la' la' <la' la''>2:16-\sug\f |
    <sold'' si'>16 mi'-\sug\p mi' mi' mi' mi' mi' mi' red' mi' mi' mi' mi'4:16 |
    mi'2: mi': |
    mi'2: red'16 mi' mi' mi' mi'4: |
    mi'2: mi'4: fad'!16 fad' fad' fad' |
    si2 r4 r8 la'-\sug\f |
    la'4. la'8-\sug\p lad'4.-\sug\f lad'8-\sug\p |
    si'8-\sug\f
  }
>> re'16. re'32 re'8 re' re'4 r8 <<
  \tag #'primo {
    red''8\f |
    mi''4. mi''8\p re''!4.\f re''8\p |
    dod''8\f
  }
  \tag #'secondo {
    si'8-\sug\f |
    sold'4. sold'8-\sug\p si'4.-\sug\f si'8-\sug\p |
    la'8-\sug\f
  }
>> dod'16. dod'32 dod'8 dod' dod'4 <<
  \tag #'primo {
    mi''8._\markup\italic dol. mi''16 |
    fad''8( mi'') la''( sold'') fad''( mi'') la''16( sold'' fad'' mid'') |
    \grace mid''8 fad''4 fad'' r re''8( mi''16 fad'') |
    \grace fad''8 mi''4 \once\slurDashed dod''8( re''16 mi'') \grace mi''8 re''4 dod''8 si' |
    \grace si'4 dod''2 r4 mi''8. mi''16 |
    fad''8( mi'') la''( sold'') fad''( mi'') la''16( sold'' fad'' mid'') |
    \grace mid''8 fad''4 fad'' r \once\slurDashed re''8( mi''16 fad'') |
    \grace fad''8 mi''4 \once\slurDashed dod''8( re''16 mi'') \grace mi''8 re''4 dod''8 si' |
    dod''(\f la'' sold'' fad'') mid''( fad'' \grace mi''8 re'' dod''16 si') |
    mi''4. fad''8 dod''4 si' |
  }
  \tag #'secondo {
    mi'8._\markup\italic\small dol. mi'16 |
    \slurDashed fad'8( mi') la'( sold') fad'( mi') la'16( sold' fad' mid') | \slurSolid
    \grace mid'8 fad'4 fad' r \once\slurDashed si'8( dod''16 re'') |
    \grace re''8 dod''4 \once\slurDashed la'8( si'16 dod'') \grace dod''8 si'4 la'8 sold' |
    la'2 r4 mi'8. mi'16 |
    fad'8( mi') la'( sold') fad'( mi') la'16( sold' fad' mid') |
    \grace mid'8 fad'4 fad' r \once\slurDashed si'8( dod''16 re'') |
    \grace re''8 dod''4 \once\slurDashed la'8( si'16 dod'') \grace dod''8 si'4 la'8 sold' |
    la'(-\sug\f fad'' mi'' re'') dod''( re'' \grace dod'' si'8 la'16 sold') |
    dod''4. re''8 la'4 sold' |
  }
>>
la'4 r
%%
<<
  \tag #'primo {
    re''8.\p re''16 \grace mi''16 re''8 dod''16 re'' |
    mi''8(\rf dod'' la'4) <la' la''>2\fp |
    re''4 r dod'' r |
    re'' r re''8. re''16 \grace mi''16 re''8 dod''16 re'' |
    mi''8(\rf dod'' la'4) <la' la''>2\fp |
    re''4 r mi'' r |
    fad'' r dod'' r |
    fad''2:16\f fad'': |
    re''8.\p dod''16 si'8 si' si'\fp si'16 si' si'4:16 |
    dod''2:\fp red'':\fp
    mi'':\fp mi''4:\fp mi''16 mi'' fad'' fad'' |
    sol''4: mi'': dod'': si': |
    fad''8\f mid''16 red'' dod'' si' lad' sold' fad'4 r |
  }
  \tag #'secondo {
    la'2-\sug\p |
    sol' sol'-\sug\fp |
    la'4 r la' r |
    la' r la'2 |
    sol'2 sol'-\sug\fp |
    la'4 r la' r |
    la' r fad' r |
    la'8-.-\sug\f si'-. dod''-. re''-. mi''-. re''-. mi''-. dod''-. |
    fad'2:16-\sug\p fad':-\sug\fp |
    sol':-\sug\fp la':-\sug\fp |
    si':-\sug\fp si':-\sug\fp |
    si': mid': |
    fad'8-\sug\f sold'16 lad' si' dod'' red'' mid'' fad''4 r |
  }
>>
<fad' re''>4.\f q8 <la'! mi''!>4. q8 |
<fad'' la'>4 <re'' fad'> <<
  \tag #'primo {
    re''4\p re'' |
    re'' re''8(\rf red'') mi''4 mi''8( sol'') |
    \grace sol''8 fad''4 mi''8 re'' la''4. la'8 |
    re''8.\p\cresc re''16 \grace mi''16 re''8 dod''16 re'' mi''8. mi''16 \grace fad''16 mi''8 red''16 mi'' |
    fad''8. fad''16 \grace sold''16 fad''8 mi''16 fad'' sol''8. sol''16 \grace la''16 sol''8 fad''16 sol'' |
    la''8.\f la''16 \grace si'' la''8 sold''!16 la'' si''16\ff la'' sol'' fad'' sol'' la'' si'' dod''' |
    re'''4\fermata re'' r4 r8 re''\f |
  }
  \tag #'secondo {
    la'4-\sug\p la' |
    si' si' si' dod''8 mi'' |
    la' fad' mi' re' la'4. la'8 |
    fad'8-\sug\p -\sug\cresc fad' la' fad' dod''8. dod''16 \grace re''16 dod''8 si'16 dod'' |
    re''8. re''16 \grace mi''16 re''8 dod''16 re'' mi''8. mi''16 \grace fad''16 mi''8 red''16 mi'' |
    fad''8.-\sug\f fad''16 \grace sold''16 fad''8 mi''16 fad'' re''4.-\sug\ff \ficta sol''8 |
    fad''4\fermata re'' r4 r8 re'-\sug\f |
  }
>>
%%
<<
  \tag #'primo {
    sol''8( fad'') mi''-. re''-. do''-. si'-. la'-. sol'-. |
    <re' re''>8 q
    
  }
  \tag #'secondo {
    sol'8( fad') mi'-. re'-. do'-. si-. la-. sol-. |
    re'8 re'
  }
>> r8 re''\p r re'' r re'' |
<<
  \tag #'primo {
    r8 mib'' r mib'' r mib'' r mib'' |
    r re'' r re'' r re'' r re'' |
    r fad'' r fad'' r fad'' r fad'' |
    r \ficta sol'' r sol'' r sol'' r fa'' |
    fa''\f fa'' r fa''\p r fa'' r fa'' |
    mi'' mi'' r mi'' r mi'' r mi'' |
    fa''\f fa'' r fa''\p r fa'' r fa'' |
    mi'' mi'' r mi'' r mi'' r mi'' |
    <la' la''>16\f sold'' fad'' mi'' la'' sold'' fad'' mi'' re''4:16 dod''!: |
    si'8 si' r si'\p r si' r si' |
    r do'' r do'' <do'' la''>2:16\f |
    <si' sold''>8 q r si'\p r si' r si' |
    r do'' r do'' <do'' la''>2:16\f |
    sold''8 sold'' r sold''\p r la'' r la'' |
    sold''4 r r2 |
  }
  \tag #'secondo {
    r8 re'' r re'' r do'' r do'' |
    r \ficta do'' r do'' r sib' r sib' |
    r la' r la' r re'' r re'' |
    r re'' r re'' r re'' r re'' |
    re''-\sug\f re'' r re''-\sug\p r re'' r re'' |
    dod''! dod'' r dod'' r dod'' r dod'' |
    re''-\sug\f re'' r re''-\sug\p r re'' r re'' |
    dod''8 dod'' r dod'' r dod'' r dod'' |
    la'2:16-\sug\f si'4: la': |
    sold'8 sold' r sold'-\sug\p r sold' r sold' |
    r la' r la' <do'' la''>2-\sug\f |
    <si' sold''>8 q r sold'-\sug\p r sold' r sold' |
    r la' r la' <do'' la''>2-\sug\f |
    si'8 si' r si'-\sug\p r do'' r do'' |
    si'4 r r2 |
  }
>>
%%
<<
  \tag #'primo {
    << la''2. \\ dod''\f >> sold''4 |
    \grace sold''4 fad''2\p fad''4 mi'' |
    \grace mi''4 re''2 re''4 dod'' |
    \grace dod''4 re''2 re'' |
    <si' si''>2.\f <la'' la'>4 |
    \grace la''4 <sold'' si'>2\p sold''4 fad'' |
    \grace fad'' mi''2 mi''4 re'' |
    \grace re'' dod''2 dod'' |
  }
  \tag #'secondo {
    << mi''2. \\ la'-\sug\f >> mi''4 |
    \grace mi''4 re''2-\sug\p re''4 dod'' |
    \grace dod'' si'2 si'4 \ficta lad' |
    \grace { \ficta lad'4 } si'2 si' |
    <si' sold''>2.-\sug\f fad''4 |
    \grace fad''4 mi''2-\sug\p mi''4 re'' |
    \grace re'' dod''2 dod''4 si' |
    \grace si' la'2 la' |
  }
>>
<la' mi''>2:8 q: |
<la' fad''>: q: |
<<
  \tag #'primo {
    <la' la''>2:\f q: |
    <do'' la''>2:\ff q: |
    <do''? la''>: q: |
    <do''? la''>: q: |
    <si' la''>2: q: |
    <si' sold''>1\p |
    fad''2. fad''4 |
    \grace fad''4 mi''2 mi'' |
  }
  \tag #'secondo {
    <la' fad''>2:8-\sug\f q: |
    q:-\sug\ff q: |
    q: q: |
    q: q: |
    <si' fad''>2: q: |
    si'1-\sug\p |
    re''2. re''4 |
    \grace re''4 dod''2 dod'' |
  }
>>
<si' sold''>2:8\f q: |
<dod'' la''>: << fad'': \\ re'':\p >> |
<< mi'': \\ dod'': >> <si' sold''>:\f |
<dod'' la''>: << fad'': \\ re'':\p >> |
<< mi'': \\ dod'': >> <si' sold''>:\f |
<dod'' la''>: << fad'': \\ re'': >> |
<< mi'': \\ dod'': >> << re'': \\ si': >> |
<<
  \tag #'primo {
    <la' la''>2:8 q: |
    q: q: |
    q: q: |
    q: q: |
    q4 r
  }
  \tag #'secondo {
    \ru#4 << { mi''2: mi'': } \\ { dod'': dod'': } >> |
    << mi''4 \\ dod''4 >> r
  }
>>
