\clef "alto" la'8.\f la'16 sold'8.\trill fad'16 mi'8.\trill re'16 dod'8.\trill si16 |
la4 r r si'(\p |
la'2) la4 r |
la r mi' r |
fad'4 fad'8. mi'32 re' dod'4 dod'(\rf |
re') dod'(\rf re') dod'(\rf |
si) la-\sug\p la r |
la4 la sold mi |
la la r2 |
\once\slurDashed mi'2(\p \once\override Script.avoid-slur = #'outside fad'4)\f\fermata r4 |
r4 mi'\p mi' mi' |
mi' r si r |
mi' sold la la |
si r si r |
si2( sold) |
la4-\sug\f la si si |
mi'2\fermata r |
r re'\p |
si4( dod') dod' dod'(\rf |
re') dod'(\rf re') dod'(\rf |
si) la la r |
la la sold sold |
la4. la16\f si32 dod' re'8. mi'16 fad'8. sold'16 |
la'4 r r la |
re'2 r\fermata |
si4-!-\sug\f la-! r2\fermata |
re'4-\sug\f fad'-\sug\p mi' mi |
la\p r la r |
fa'2 mi' |
mi'1 |
fa'2:16-\sug\cresc fa':-\sug\f |
mi'4 mi r do'-\sug\p |
si la sold la |
mi8.( fad16) sold4 r sold( |
la sold la red) |
mi2 r4 r8 dod'\f |
la'4. la'8\p lad'4.\f lad'8\p |
si'8\f fad16. fad32 fad8 fad fad4 r |
mi'4.\f mi'8\p mi'4.\f mi'8\p |
mi'8\f mi16. mi32 mi8 mi mi4 dod'8._\markup\italic dol. dod'16 |
\slurDashed re'8( dod') fad'( mi') re'( dod') \slurSolid fad'16( mi' re' dod') |
\grace dod'8 re'4 re' r sold-\sug\p |
la la mi' mi |
la2 r4 dod'8. dod'16 |
re'8( dod') fad'( mi') re'( dod') fad'16( mi' re' dod') |
\grace dod'8 re'4 re' r sold-\sug\p |
la la mi' mi |
la-\sug\f r re' r |
mi'2:8 mi: |
%%
la4 r fad'2\p |
mi' mi'\fp |
fad'4 r mi' r re' r fad'2 |
mi'2 mi'-\sug\fp |
fad'4 r dod' r |
re' r lad r |
fad8-.\f sold-. lad-. si-. dod'-. si-. dod'-. lad-. |
si8.\p dod'16 re'8 re' re'2:16\fp |
mi':\fp fad':\fp |
sol':\fp sol':\fp |
sol': sol': |
fad'2:8 fad4 r |
la'4.\f la'8 la'4. la8 |
re'4 re' fad'\p fad' |
sol' sol' sol la |
re'8 fad' mi' re' la'4. la8 |
fad'\p-\sug\cresc fad' la' fad' la'2:8 |
la': sol': |
fad':-\sug\f sol'8 sol' mi' mi' |
re'2\fermata r4 r8 re'-\sug\f |
%%
sol'8( fad') mi'-. re'-. do'-. si-. la-. sol-. |
re'8 r re'-\sug\p r re' r re' r |
re' r re' r re' r re' r |
re'4-! re'-! re'-! re'-! |
re'-! re'-! re'-! re'-! |
re'-! sib-! sib-! la-! |
sold!8-!-\sug\f r la-\sug\p r si r sold r |
la r la r la r la r |
sold-\sug\f r la-\sug\p r si r sold r |
la r la r la r la r |
dod'2:8-\sug\f re'8 re' red' red' |
mi'4-. mi'-.-\sug\p mi'-. mi'-. |
mi'-. mi'-. mi'2:8-\sug\f |
mi'4-. mi'-.-\sug\p mi'-. mi'-. |
mi'-. mi'-. mi'2:8-\sug\f |
mi'4-. mi'-.-\sug\p mi'-. mi'-. |
mi'-. r r2 |
%%
la2:8-\sug\f la: |
la: la: |
la: la: |
mi: mi: |
mi:-\sug\f mi: |
mi:-\sug\p mi: |
mi: mi: |
la: la: |
dod': dod': |
re': re': |
red':-\sug\f red': |
\ficta red'4-!-\sug\ff fad'-! red'-! do'-! |
la-! do'-! la-! fad-! |
red-! fad-! red-! do-! |
red-! red-! fad-! si-! |
mi2:8-\sug\p mi: |
mi: mi: |
mi: mi: |
mi':-\sug\f mi': |
mi': la:-\sug\p |
la: sold8-\sug\f mi' mi' mi' |
mi'2: la:-\sug\p |
la: sold8-\sug\f mi' mi' mi' |
mi'2: la: |
la: sold: |
la4 la dod' mi' |
la' la' mi' dod' |
la la dod' mi' |
la' la' mi' dod' |
la r
