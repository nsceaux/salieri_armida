\piecePartSpecs
#`((oboi #:score-template "score-deux-voix")
   (corni #:tag-global ()
          #:instrument "Corni in A."
          #:score-template "score-voix")
   (fagotti #:music , #{
\quoteBassi "BDCbassi"
s1*3
<>^\markup\tiny "B."
\cue "BDCbassi" { \mmRestInvisible s1*6 \mmRestVisible }
s1
<>^\markup\tiny "B."
\cue "BDCbassi" { \mmRestInvisible s1*12 \mmRestVisible \restInvisible s4. \restVisible }
s8 s2
s1*10
<>^\markup\tiny "B."
\cue "BDCbassi" { \mmRestInvisible s1 \mmRestVisible }
s1
\cue "BDCbassi" { \restInvisible s2 \restVisible }
s2 s1*5 s2.
<>^\markup\tiny "B."
\cue "BDCbassi" { \restInvisible s4 \restVisible \mmRestInvisible s1*2 \mmRestVisible }
s1 s2.
<>^\markup\tiny "B."
\cue "BDCbassi" { \restInvisible s4 \restVisible \mmRestInvisible s1 \mmRestVisible }
s1*3
<>^\markup\tiny "B."
\cue "BDCbassi" { \mmRestInvisible s1*6 \mmRestVisible }
s1*15 s4
<>^\markup\tiny "B."
\cue "BDCbassi" {
  \ru#3 { \restInvisible s8 \restVisible s }
  \mmRestInvisible s1*10 s2 \mmRestVisible
}
\new CueVoice { mi2:8 }
\cue "BDCbassi" { \mmRestInvisible s1 s2 \mmRestVisible }
\new CueVoice { mi2:8 }
\cue "BDCbassi" { \mmRestInvisible s1*2 \mmRestVisible }

                          #})

   (violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (bassi #:indent 0)

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Rinaldo
    \livretVerse#10 { Dilegua il tuo timore, }
    \livretVerse#10 { Serena i mesti rai; }
    \livretVerse#10 { Sai ch'io t'adoro, e sai, }
    \livretVerse#10 { Ch'io morirò per te. }
    \livretPers Armida
    \livretVerse#10 { Taci, che accresci al core }
    \livretVerse#10 { Il suo mortale affanno. }
    \livretVerse#10 { L'ira del Ciel tiranno }
    \livretVerse#10 { Tutta si sfoghi in me. }
    \livretPers Rinaldo
    \livretVerse#10 { Mio ben. }
    \livretPers Armida
    \livretVerse#10 { Mio dolce amore, }
    \livretPers A 2
    \livretVerse#10 { Che barbaro momento, }
    \livretPers Rinaldo
    \livretVerse#10 { Io tremo al tuo terrore }
    \livretPers Armida
    \livretVerse#10 { L'alma mancar mi sento }
    \livretPers A 2
    \livretVerse#10 { Ne intendo il mio spavento }
    \livretVerse#10 { Ne posso dir perché. }
    \livretPers Rinaldo
    \livretVerse#10 { Ma il mio soccorso? }
  }
  \null
  \column {
    \livretPers Armida
    \livretVerse#10 { E' vano. }
    \livretPers Rinaldo
    \livretVerse#10 { L'amore… }
    \livretPers Armida
    \livretVerse#10 { E' mio periglio. }
    \livretPers Rinaldo
    \livretVerse#10 { Il Cielo… }
    \livretPers Armida
    \livretVerse#10 { Ah de' suoi fulmini }
    \livretVerse#10 { Già balenar sul ciglio }
    \livretVerse#10 { Mi vedo acceso il lampo. }
    \livretPers Rinaldo
    \livretVerse#10 { No mira al nostro scampo }
    \livretVerse#10 { Quall'armi il Ciel mi diè; }
    \livretVerse#10 { Vedi }
    \livretPers Armida
    \livretVerse#10 { Oh stelle! che luce funesta! }
    \livretVerse#10 { Fuggi ascondi… }
    \livretPers Rinaldo
    \livretVerse#10 { Ma senti, t'arresta }
    \livretPers A 2
    \livretVerse#10 { Ah che strana vicenda è mai questa, }
    \livretVerse#10 { Non più orribil la morte non è. }

  }
  \null
} #}))

