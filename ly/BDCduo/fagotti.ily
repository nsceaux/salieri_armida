\clef "bass" la8.\f la16 sold8.\trill fad16 mi8.\trill re16 dod8.\trill si,16 |
la,4 r r sold(\p |
la2) la,4 r |
R1*6 |
la2(-\sug\p \once\override Script.avoid-slur = #'outside fad4-\sug\f)\fermata r |
R1*12 |
r4 r8 la,16\f si,32 dod re8. mi16 fad8. sold16 |
la4 r r la, |
re2 r |
mi4-!\f fad-! r2\fermata |
re4\f fad\p mi mi, |
la,\p r la r |
la r sold r |
do'1 |
fa2:8-\sug\cresc fa:-\sug\f |
mi4 mi, r2 |
R1*3 |
r2 r4 r8 dod'!-\sug\f |
re'4. re'8-\sug\p dod'4.-\sug\f dod'8-\sug\p |
si8\f si,16. si,32 si,8 si, si,4 r |
mi4.\f mi8\p sold4.\f sold8\p |
la8\f la,16. la,32 la,8 la, la,4 r |
R1 |
r4 re-\sug\p re r |
R1*3 |
r4 re re r |
R1 |
la,4-\sug\f r re r |
mi2:8 mi,: |
%%
la,4 r r2 |
R1*6 |
fad8-.\f sold-. lad-. si-. dod'-. si-. dod'-. lad-. |
si2 r |
si4-\sug\fp r la-\sug\fp r |
sol-\sug\fp r r2 | \allowPageTurn
R1*2 |
re4.\f re8 dod4. dod8 |
re4 re, fad\p fad |
sol sol sol, la, |
re8-! fad( mi re) la4 la, |
re2:8\p\cresc re: |
re: re: |
re:\f sol8-\sug\ff sol mi mi |
re2\fermata r4 r8 re'\f |
%%
\once\slurDashed sol'( fad') mi'-. re'-. do'-. si-. la-. sol-. |
re'4 r r2 |
R1*15 |
%%
la,1-\sug\f |
R1*3 |
mi1-\sug\f |
R1*3 |
dod'2.-\sug\p dod'4 |
re'1 |
red'-\sug\f |
\ficta red'4-!-\sug\ff fad'-! red'-! do'-! |
la-! do'-! la-! fad-! |
red-! fad-! red-! do-! |
si,-! \ficta red-! fad-! si-! |
mi1-\sug\p |
mi |
mi |
mi-\sug\f |
mi1*1/2 s2-\sug\p |
mi4 mi mi,-\sug\f mi, |
la, la, re2-\sug\p |
mi mi,-\sug\f |
la, re |
mi mi, |
la,4 la, dod mi |
la la mi dod |
la, la, dod mi |
la la mi dod |
la, r
