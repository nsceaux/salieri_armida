\piecePartSpecs
#`((flauti #:score-template "score")

   (oboi #:score-template "score-deux")

   (corni #:score-template "score-deux"
          #:tag-global ()
          #:instrument "Corni in F")

   (fagotti
    #:music , #{

\quoteBassi "AABbassi"
\cue "AABbassi" {
  <>^\markup\tiny Bassi \mmRestInvisible s2.*5\mmRestVisible
  \restInvisible s2 \restVisible
} s4
s2.*22
\cue "AABbassi" {
  \mmRestInvisible s2 s8 <>^\markup\tiny Bassi s8 s2. \mmRestVisible
  \restInvisible s4 \restVisible
} s2
s2
\cue "AABbassi" {
  <>^\markup\tiny Bassi \restInvisible s2 \restVisible
} s2
s2.*16

\new CueVoice {
  <>^\markup\tiny Bassi \mmRestInvisible
  do'8\p do' do' do' do' do' |
  do'2.:8 |
  do':8 |
  do':8 |
  do':8 |
  do'4 do'8\f sib! la sol |
  fa4 fa fa |
  \mmRestVisible
  \restInvisible fa \restVisible
} s2
s4 \clef "bass"
\cue "AABbassi" {
  <>^\markup\tiny Bassi \restInvisible s2 s4 \restVisible
}

                #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{\markup\tacet #}))
