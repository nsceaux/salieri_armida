\clef "alto" fa4\f fa fa |
fa fa fa |
fa fa fa8 fa' |
re'4 re' r |
do' do do |
fa4 r8 do'' sib' sol' |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la2.:8 |
sib:8 |
do'2:8 do8 do |
fa4 r fa''\p( |
mi'' re'' do'') |
sib' sib8 sib' la' la-\sug\f |
sib4 do' do |
fa r fa''-\sug\p( |
mi'' re'' do'') |
sib' sib8 sib' la' la-\sug\f |
sib4 do' do |
fa fa r |
fa\p fa fa |
fa-\sug\mf fa fa |
fa fa fa8 fa' |
re'4 re' r |
do' do' do' |
fa' fa'8 do' la fa |
r4 r r8 fa'-\sug\p |
do'4 do r |
sol'4 sol'8 sol' sol' sol' |
sol'2. |
sol'4 sol'8 sol' sol' sol' |
sol'2 r4 |
do''4(\fp si'? la') |
\slurDashed sol'4.( fa'8 mi'4) |
fa'( sol' sol) | \slurSolid
do' r do''(-\sug\mf |
si' la' sol') |
fa'-! fa'( mi') |
fa'-\sug\p mi' sol'~ |
sol' r do''-\sug\mf |
si'( la' sol') |
fa'-! fa'( mi') |
fa'\f sol' sol |
do' mi' mi' |
re' si8-! si( do' re') |
mi' fa' sol'4 sol |
do'4 do do' |
do''2.:8\p |
do'':8 |
do'':8 |
do'':8 |
do'':8 |
do''4 do''8\f sib'! la' sol' |
fa'4 fa' fa' |
fa' fa' fa' |
fa' fa'\p mi'8 mi' |
fa'4 fa' r |
<<
  { sib'8 sib'4 sib' sib'8 |
    sib'2.\fermata |
    fa'8 mi' re' do' sib la |
    \grace la8 sol2 } \\
  { sol'8-\sug\p\cresc sol'4 sol' sol'8 |
    sol'2.-\sug\f\fermata |
    re'8 do' sib la sol fa |
    \grace fa8 mi2 }
>> fa4 |
fa8 sib do'4 do' |
fa' r fa''-\sug\mf |
mi'' re'' do'' |
sib' sib' la'8 fa'-\sug\p |
sib4 do' do' |
fa'4 r fa''-\sug\mf |
mi'' re'' do'' |
sib'2 la'8 fa'-\sug\p |
sib4 do' do |
<<
  { fa8 la' sol' fa' mi' re' | do'4 mi' fa' | } \\
  { fa8-\sug\f fa' mi' re' do' sib | la8 do'4 do' do'8 | }
>>
sib8 sib do' do' do do |
fa4 fa fa |
fa-\sug\rf fa fa |
fa fa fa |
sol4 sol8 fa mi re |
do4 do do |
fa fa'8 do'' sib' sol' |
la' sib la sib la mi |
fa mi fa mi fa sol |
la sib la sib la mi |
fa mi fa mi fa sol |
la2.:8 |
sib:8 |
do'2:8 do8 do |
fa4 r fa''-\sug\p |
mi'' re'' do'' |
sib'2 la'4-\sug\f |
sib do' do |
fa r fa'' |
mi'' re'' do'' |
sib'2 la'4 |
sib8 sib do' do' do do |
fa4 r r |
