\clef "treble"
<<
  \tag #'primo {
    do''\f do''8-.( do''-. do''-. do''-.) |
    do''8.(\rf fa''16) fa''4. mi''8 |
    \grace mi''8 re''4. do''8( do'' re'') |
    \grace do''8 sib'4 sib' r |
    \grace la'8 sol'4 sol'4.( la'16 sib') |
    la'8.( sib'16) do''8-! do''8( re'' mi'') |
    fa''16( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
    re''8( mi''16 fa'') sol''( la'' sib'' do''') re'''( sib'' la'' sol'') |
    fa''4~ fa''16 mi'' fa'' sol'' la''( sib'' la'' sol'') |
    fa''4 r8 do'''(\p re''' si'') |
    do'''( la'') sib''( sol'') la''( fa'') |
    \grace fa''16 mi''4.( fa''16 sol'') fa''8. do'''16\f |
    \grace do'''16 sib''8 la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4. \grace re'''16 do'''\p sib''32 do''' re'''8 \grace do'''16 si'' la''32 si'' |
    do'''8 \grace sib''16 la''32 sol'' la''16 sib''8-. \grace la''16 sol''32 fa'' sol''16 la''8-! fa''-! |
    \grace fa''16 mi''4.( fa''16 sol'') fa''8. do'''16\f |
    re'''( sib'' la'' sol'') fa''4. \grace la''16 sol''8 |
    fa''4 fa' r |
    do''4\p do''8 do'' do'' do'' |
    do''8.(\rf fa''16) fa''4. mi''8 |
    \grace mi''8 re''4. do''8 do'' re'' |
    \grace do''8 sib'4 sib' r |
    sol'4 sol'4. la'16 sib' |
    la'8. sib'16 do''4 r |
    \grace { sol''16[ fa'' mi''] } fa''8\f mi'' re''\p do'' sib' la' |
    la'4( sol') r |
    do''4 do''8-! do''( re'' do'') |
    \grace do''8 si'2 r4 |
    re''4 re''8-! re''( mi'' re'') |
    do''2~ do''8( re'') |
    mi''2.\fp~ |
    mi''4.( fa''8 sol''4) |
    \grace sol''8 fa''4( mi'' re'') |
    do''4 r8 sol''\mf( la'' fad'') |
    sol''( mi'') fa''!( re'') mi''( do'') |
    \grace do'' si'4. do''16 re'' do''4 |
    la'8\p fa''16 re'' do''4. re''8 |
    \grace re''8 mi''4. \grace la''16 sol''32 fad'' sol''16 la''8-!\mf \grace sol''16 fad''!32 mi'' fad''16 |
    sol''8-! \grace fa''!16 mi''32 re'' mi''16 fa''8-! \grace mi''16 re''32 dod'' re''16 mi''8-! \ficta do''?-! |
    \grace do''8 si'4. do''16 re'' do''8 sol''\f |
    la''8. fa''16 mi''4 re'' |
  }
  \tag #'secondo {
    la'4-\sug\f la'8-.( la'-. la'-. la'-.) |
    la'8.(-\sug\rf re''16) re''4. do''8 |
    \grace do''8 sib'4. la'8( la' sib') |
    \grace la'8 sol'4 sol' r |
    \grace fa'8 mi'4 mi'4.( fa'16 sol') |
    fa'8. sol'16 la'8 la' sib' do'' |
    \ru#15 { do'' \grace re''16 do''32 si' do''16 } |
    \ru#3 { re''8 \grace mi''16 re''32 dod'' re''16 } |
    do''16 sib' \once\tieDashed la'8~ la'16 sol' la' sib' \once\slurDashed do''( re'' do'' sib') |
    la'4 r la''8-\sug\p sol''~ |
    sol'' fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''8. do''16-\sug\f |
    \grace mi''16 re''8 do''16 sib' la'4. do''8 |
    \grace mi''8 fa''4 r la''8-\sug\p sol'' |
    sol''8 fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''8. do''16-\sug\f |
    fa''( re'' do'' sib') la'4. sib'8 |
    la'4 fa' r |
    la'4-\sug\p la'8 la' la' la' |
    la'8.(-\sug\rf re''16) re''4. do''8 |
    \grace do''8 sib'4. la'8 la' sib' |
    \grace la'8 sol'4 sol' r |
    mi'4 mi'4. fa'16 sol' |
    fa'8. sol'16 la'4 r |
    \grace mi''16 re''32-\sug\f dod'' re''16 do''8 sib'-\sug\p la' sol' fa' |
    \once\slurDashed fa'4( mi') r |
    mi'4 mi'8-! mi'( fa' mi') |
    \grace mi'8 re'2 r4 |
    fa'4 fa'8-! fa'( sol' fa') |
    \grace fa'8 mi'2~ mi'8 sol' |
    do''2.-\sug\fp~ |
    \once\slurDashed do''4.( re''8 mi''4) |
    \grace mi''8 \once\slurDashed re''4( do'' si') |
    do''4 r mi''8-\sug\mf re''~ |
    re'' do''4 si' la'8~ |
    la' sol'4 sol'8 sol'4 |
    la'8.-\sug\p la'16 sol'4. si'8 |
    \grace si'8 do''4 r mi''8-\sug\mf re''~ |
    re'' do''4 si' la'8~ |
    la' sol'4 sol'8 sol'[ do'']-\sug\f |
    do''8. re''16 do''4. si'8 |
  }
>>
do''8( do'''4 si''8 la'' sol'') |
\grace sol''8 fa''4. re''8( mi'' fa'') |
<<
  \tag #'primo { sol''8 la''16 fa'' mi''4. re''8 | }
  \tag #'secondo { sol''8 fa''16 re'' do''4. si'8 | }
>>
do''4 do' r |
<<
  \tag #'primo {
    la''4\p la''8( sol'' la'' sib'') |
    \grace la''8 sol''2 r4 |
    sol''4 la''8( sib'' la'' sol'') |
    \grace sol''8 fa''2 r4 |
    la''4 \grace { sib''16[ do'''] } re'''8 do'''16 sib'' \grace sib''8*1/2 la''8 sol''16 fa'' |
    \grace fa''8 mi''2 r4 |
    do''4\f do''8 do'' do'' do'' |
    do''8.(\rf fa''16) fa''4. mi''8 |
    \grace mi''8 re''4 re''8\p do'' re''8 do''16 sib' |
    \grace sib'8 la'2 r8 do'' |
    mi''4 mi''16(\cresc fa'' sol'' la'') sib''( la'' sol'' fa'') |
    \grace fa''8 mi''2.\f\fermata |
    \grace sol''16 fa''32\p mi'' fa''16 mi''8 re'' do'' sib' la' |
    \grace la'8 sol'2 la'8 sib' |
    do''8 re''16 sib' la'4 sol' |
    fa' r8 do'''(\mf re''' si'') |
    do'''( la'') sib''!( sol'') la''( fa'') |
    \grace fa''8 mi''4.( fa''16 sol'') fa''8 do''\p |
    re'' mi''16( fa'') \grace fa''8 do''4. sib'8 |
    \grace sib'8 la'4 r8 \grace re'''16 do'''\mf si''32 do''' re'''8-! \grace do'''16 si''32 la'' si''16 |
    do'''8-! \grace sib''!16 la'' sol''32 la'' sib''8-! \grace la''16 sol'' fa''32 sol'' la''8-! fa''-! |
    \grace fa''16 mi''4. fa''16 sol'' fa''8 do''\p |
    re'' mi''16( fa'') la'4 sol' |
    la'8\f
  }
  \tag #'secondo {
    do''4-\sug\p do''8( mi'' fa'' re'') |
    \grace do''8 sib'2 r4 |
    sib'4 \once\slurDashed do''8( re'' do'' sib') |
    \grace sib'8 la'2 r4 |
    do''4 \grace { re''16[ mi''] } fa''8 mi''16 re'' \grace re''8*1/2 do''8 sib'16 la' |
    \grace la'8 sol'2 r4 |
    la'4-\sug\f la'8 la' la' la' |
    \once\slurDashed la'8.(-\sug\rf re''16) <re'' re'>4. do''8 |
    \grace do''8 sib'4 sib'8-\sug\p la' sib' la'16 sol' |
    \grace sol'8 fa'2 r4 |
    <mi' do''>8-\sug\cresc q4 q q8 |
    q2.-\sug\f\fermata |
    \grace mi''16 re''32-\sug\p dod'' re''16 do''8 sib' la' sol' fa' |
    \grace fa' mi'2 fa'8 sol' |
    la'8 sib'16 sol' fa'4 mi' |
    fa' r la''8-\sug\mf sol''~ |
    sol'' fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''[ la']-\sug\p |
    sib'8 \once\slurDashed do''16( re'') \grace sib'8 la'4. sol'8 |
    \grace sol'8 fa'4 r la''8-\sug\mf sol''~ |
    sol'' fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''[ la']-\sug\p |
    sib'8 do''16( re'') fa'4 mi' |
    fa'8-\sug\f 
  }
>> la''8( sol'' fa'' mi'' re'') |
<<
  \tag #'primo {
    do''4( mi'' fa'') |
    \grace mi''16 re''8 do''16 sib' la'4 sol'8. do''16 |
    do''4 do''8 do'' do'' do'' |
    do''8.(\rf fa''16) fa''4.( mi''8) |
    \grace mi''8 re''4. do''8 do'' re'' |
    \grace do''8 sib'2 r4 |
    \grace la'16 sol'4 sol'4. la'16 sib' |
    la'8. sib'16 do''8 do'' re'' mi'' |
    fa''16( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
    re''8 mi''16( fa'' sol'' la'' sib'' do''') re'''( sib'' la'' sol'') |
    fa''4~ fa''16 mi'' fa'' sol'' la''( sib'' la'' sol'') |
    fa''4 r8 do'''(\p re''' si'') |
    do'''( la'') sib''!( sol'') la''( fa'') |
    \grace fa''16 mi''4.( fa''16 sol'') fa''8.(\f do'''16) |
    \grace do'''16 sib''8 la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4. do'''8( re''' si'') |
    do'''( la'') sib''!( sol'') la''( fa'') |
    \grace fa''16 mi''4.( fa''16 sol'') fa''8. do'''16 |
    re''' sib'' la'' sol'' fa''4. \grace la''16 sol''8 |
    fa''4 r r |
  }
  \tag #'secondo {
    la'8 do''4 do'' do''8 |
    sib' la'16 sol' fa'4 mi' |
    la' la'8 la' la' la' |
    la'-\sug\rf re'' <re'' re'>4. do''8 |
    \grace do''8 sib'4. la'8 la' sib' |
    \grace la'8 sol'2 r4 |
    \grace fa'16 mi'4 mi'4. fa'16 sol' |
    fa'8. sol'16 la'8 la' sib' do'' |
    \ru#15 { do'' \grace re''16 do''32 si' do''16 } |
    \ru#3 { re''8 \grace mi''16 re''32 dod'' re''16 } |
    do''16 sib' la'8~ la'16 sol' la' sib' \once\slurDashed do''( re'' do'' sib') |
    la'4 r la''8(-\sug\p sol'')~ |
    sol'' fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''8.-\sug\f do''16 |
    \grace mi''16 re''8 do''16 sib' la'4. do''8 |
    \grace mi''8 fa''4 r4 la''8( sol'')~ |
    sol'' fa''4 mi'' re''8~ |
    re'' do''4 do''8 do''8. do''16 |
    fa''16 re'' do'' sib' la'4. sib'8 |
    la'4 r r |
  }
>>
