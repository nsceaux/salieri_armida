\clef "treble" \transposition fa
R2.*6 |
\twoVoices #'(primo secondo tutti) <<
  { do''8 sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do'' sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do''2.:8 |
    do''4 }
  { mi'8 sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi' sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi'8 do'' do''2:8 |
    do''4 }
>> r4 r |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' mi''8. re''16 | do''4 }
  { do''4 do'' do''8. sol'16 | mi'4 }
>> r4 r |
R2.*2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 }
  { do''4. sol'8 | mi'4 }
>> r4 r |
R2.*2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 do' }
  { do''4. sol'8 | mi'4 do' }
>> r4 |
R2. |
r4 <>\fp \twoVoices #'(primo secondo tutti) <<
  { do''2~ |
    do''2. |
    re''4 re''2 | }
  { do'2~ |
    do'2. |
    re''4 re''2 | }
>>
R2. |
r4 \tag #'tutti <>^"a 2." do''8 sol' mi' do' |
R2. |
r4 r8 re''\f re'' re'' |
re''2.\p~ |
re''4. re''8\f re'' re'' |
re''2.\p~ |
re''4. re''8\f re'' re'' |
re''4\fp sol'2~ |
sol'2 r4 |
r re'' re'' |
sol' sol r |
R2.*7 |
re''4 re'' re'' |
re''4. re''8 re'' re'' |
\twoVoices #'(primo secondo tutti) <<
  { re''8 mi'' re''4 re'' | sol'4 }
  { re''8 do'' re''4 re'' | sol'4 }
>> r4 r |
R2.*6 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''2. |
    do''4 do''2~ |
    do''4 do''8 mi'' re''4 |
    do''4 do''8 sol' mi' do' |
    re''4 re'' re'' |
    re''2.\fermata | }
  { do'2. |
    do'4 do'2 |
    do''4 do''8 mi'' re''4 |
    do''4 do''8 sol' mi' do' |
    sol'4 sol' sol' |
    sol'2.\fermata | }
  { s2. | s\f | s4 s2\p | s2. | s-\sug\cresc | s-\sug\f }
>>
R2.*2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' | do'' }
  { do''4 sol' | mi' }
>> r r |
R2.*8 |
\twoVoices #'(primo secondo tutti) <<
  { s4 re'' mi'' | s mi'' re'' | }
  { s sol' do'' | s do'' sol' | }
  { r4 s s | r s s | }
>>
\tag #'tutti <>^"a 2." do''4 r do'' |
do''2. |
do'' |
re''4 re'' r |
R2.*2 |
\twoVoices #'(primo secondo tutti) <<
  { do''8 sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do'' sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do'' do'' do'' do'' do'' do'' |
    do''4 }
  { mi'8 sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi' sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi'8 do'' do''2:8 |
    do''4 }
>> r4 r |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' mi''8. re''16 | do''4 }
  { do''4 do'' do''8. sol'16 | mi'4 }
>> r r |
R2.*2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 }
  { do''4. sol'8 | mi'4 }
>> r r |
R2.*2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 }
  { do''4. sol'8 | mi'4 }
>> r4 r |
