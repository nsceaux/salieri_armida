\clef "bass" fa\f fa fa |
fa\rf fa fa |
fa fa fa |
sol sol8 fa mi re |
do4 do do |
fa4 fa, r8 sib |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la2.:8 |
sib:8 |
do'8 do' do' do' do do |
fa4 fa, r |
R2. |
r4 r r8 la-\sug\f |
sib4 do' do |
fa fa, r |
R2. |
r4 r r8 la-\sug\f |
sib4 do' do |
fa fa, r |
fa\p fa fa |
fa-\sug\rf fa fa |
fa fa fa |
sol4 sol8 fa mi re |
do4 do do |
fa fa8 do la, fa, |
r4 r r8 fa-\sug\p |
do2. |
do'4 r do |
sol r sol,-\sug\f |
sol-\sug\p r sol, |
do' r do-\sug\f |
do'4(\fp si la) |
\once\slurDashed sol4.( fa8 mi4) |
fa sol sol, |
do do, r |
R2. |
r4 r r8 mi\p |
fa4 sol sol, |
do do, r |
R2. |
r4 r r8 mi\f |
fa4 sol sol, |
do do' do' |
re'( si) sol8 fa |
mi fa sol4 sol, |
do do, do |
do'8\p do' do' do' do' do' |
do'2.:8 |
do':8 |
do':8 |
do':8 |
do'4 do'8\f sib! la sol |
fa4 fa fa |
fa fa fa |
fa\p fa mi |
fa4 fa8 do la, fa, |
sol4\cresc sol sol |
sol2.\f\fermata |
R2. |
r4 r fa\p |
fa,8 sib, do do do, do, |
fa4 fa, r |
R2. |
r4 r r8 fa-\sug\p |
sib,4 do do, |
fa,4 fa r |
R2. |
r4 r r8 fa-\sug\p |
sib,4 do do, |
fa,2.\f |
fa,4 sib( la) |
sib8 sib do' do' do do |
fa,4 fa fa |
fa-\sug\rf fa fa |
fa fa fa |
sol sol8 fa mi re |
do4 do do |
fa fa, r8 sib |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la2.:8 |
sib:8 |
do'8 do' do' do' do do |
fa4 fa, r |
R2. |
r4 r r8 la\f |
sib4 do' do |
fa fa, r |
R2. |
r4 r r8 la\f |
sib sib do' do' do do |
fa4 r r |
