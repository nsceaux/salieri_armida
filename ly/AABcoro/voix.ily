<<
  \tag #'voix1 {
    \clef "soprano/treble" R2.*22 |
    do''4 do''8 do'' do'' do'' |
    do''8.([ fa''16]) fa''4. mi''8 |
    \grace mi''8 re''4. do''8 do'' re'' |
    \grace do''8 sib'4 sib' r |
    \grace la'8 sol'4 sol'4. la'16[ sib'] |
    la'8.[ sib'16] do''4 r |
    fa''8[ mi''] re'' do'' sib' la' |
    \grace la'8 sol'2 r4 |
    do''4 do''8 do'' re'' do'' |
    \grace do''8 si'2 r4 |
    re''4 re''8 re'' mi'' re'' |
    \grace re''8 do''2~ do''8 re'' |
    mi''2.~ |
    mi''4.( fa''8) sol''4 |
    \grace sol''8 fa''4( mi'' re'') |
    do''4 r r |
    R2. |
    r4 r r8 do'' |
    la'8 fa''16[ re''] do''4.( re''8) |
    \grace re''8 mi''2 r4 |
    R2. |
    r4 r r8 sol'' |
    la''8. fa''16 mi''4( re'') |
    do''4 r r |
    R2.*3 |
    do''4 do''8 mi'' fa'' re'' |
    \grace re''8 do''2 r4 |
    sib'4 do''8 re'' do'' sib' |
    \grace sib'8 la'2 r4 |
    do''4 fa''8 mi''16[ re''] \grace re''16 do''8 sib'16[ la'] |
    \grace la'8 sol'2 r4 |
    R2.*2 |
    re''4 re''8 do'' re'' do''16[ sib'] |
    \grace sib'8 la'2 r8 do'' |
    mi''4. mi''8 mi'' sol'' |
    \grace fa''8 mi''2.\fermata |
    fa''8[ mi''] re'' do'' sib' la' |
    \grace la' sol'2 la'8[ sib'] |
    do''8 re''16[ sib'] la'4( sol') |
    fa'4 r r |
    R2. |
    r4 r r8 do'' |
    re'' mi''16[ fa''] \grace re''8 do''4. sib'8 |
    \grace sib'8 la'2 r4 |
    R2. |
    r4 r r8 do'' |
    re'' mi''16[ fa''] la'4( sol') |
    la'8 la'' sol'' fa'' mi'' re'' |
    do''4( mi'') fa'' |
    re''8 do''16[ sib'] la'4( sol') |
    fa'4 r r |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R2.*22 |
    la'4 la'8 la' la' la' |
    la'8.([ re''16]) re''4. do''8 |
    \grace do''8 sib'4. la'8 la' sib' |
    \grace la'8 sol'4 sol' r |
    \grace fa'8 mi'4 mi'4. fa'16[ sol'] |
    fa'8.[ sol'16] la'4 r |
    re''8[ do''] sib' la' sol' fa' |
    \grace fa'8 mi'2 r4 |
    sol' sol'8 sol' sol' sol' |
    sol'2 r4 |
    \sugNotes {
      sol'4 sol'8 sol' sol' sol' |
      sol'2~ sol'8
    } re'' |
    do''2.~ |
    do''4.( re''8) mi''4 |
    \grace mi''8 re''4( do'' si') |
    do''4 r r |
    R2. |
    r4 r r8 sol' |
    la'8. la'16 sol'4.( si'8) |
    \grace si'8 do''2 r4 |
    R2. |
    r4 r r8 do'' |
    do''8. re''16 do''4( si') |
    do''4 r r |
    R2.*3 |
    la'4 la'8 sol' la' sib' |
    \grace la'8 sol'2 r4 |
    sol'4 la'8 sib' la' sol' |
    \grace sol'8 fa'2 r4 |
    la'4 re''8 do''16[ sib'] \grace sib'16 la'8 sol'16[ fa'] |
    \grace fa'8 mi'2 r4 |
    R2.*2 |
    sib'4 sib'8 la' sib' la'16[ sol'] |
    \grace sol'8 fa'2 r8 la' |
    sib'4. sib'8 sib' sib' |
    sib'2.\fermata |
    re''8[ do''] sib' la' sol' fa' |
    \grace fa'8 mi'2 fa'8[ sol'] |
    la'8 sib'16[ sol'] fa'4( mi') |
    fa'4 r r |
    R2. |
    r4 r r8 la' |
    sib' do''16[ re''] \grace sib'8 la'4. sol'8 |
    \grace sol'8 fa'2 r4 |
    R2. |
    r4 r r8 la' |
    sib' do''16[ re''] fa'4( mi') |
    fa'8 fa'' mi'' re'' do'' sib' |
    la'4( sol') fa' |
    sib'8 la'16[ sol'] fa'4( mi') |
    fa'4 r r |
  }
  \tag #'voix3 {
    \clef "alto/treble" R2.*22 |
    fa'4 fa'8 fa' fa' fa' |
    fa'4 fa' r8 fa' |
    fa'4. fa'8 fa' fa' |
    re'4 re' r |
    do'4 do'4. do'8 |
    fa'4 fa' r |
    r4 r r8 fa' |
    do'2 r4 |
    mi'4 mi'8 mi' fa' mi' |
    \grace mi'8 re'2 r4 |
    fa'4 fa'8 fa' sol' fa' |
    \grace fa'8 mi'2~ mi'8 sol' |
    do''4(\melisma si' la') |
    sol'4.( fa'8)\melismaEnd mi'4 |
    la'( sol' fa') |
    mi' r r |
    R2. |
    r4 r r8 mi' |
    fa'8. fa'16 mi'4.( sol'8) |
    sol'2 r4 |
    R2. |
    r4 r r8 mi' |
    fa'8. fa'16 sol'2 |
    mi'4 r r |
    R2.*3 |
    do'4 do'8 do' do' do' |
    do'2 r4 |
    do'4 do'8 do' do' do' |
    do'2 r4 |
    do'4 do'8 do' do' do' |
    do'2 r4 |
    R2.*2 |
    fa'4 fa'8 fa' mi' mi' |
    \grace sol'8 fa'2 r8 fa' |
    sol'4. sol'8 sol' sib' |
    \grace la'8 sol'2.\fermata |
    R2. |
    r4 r fa' |
    fa'8 sib do'2 |
    la4 r r |
    R2. |
    r4 r r8 fa' |
    sib sib do'4. do'8 |
    do'2 r4 |
    R2. |
    r4 r r8 fa' |
    sib sib do'2 |
    fa'8 fa' fa' fa' fa' fa' |
    fa'4\melisma do''\melismaEnd do'' |
    fa'8 sib' do''4\melisma do'\melismaEnd |
    fa' r r |
  }
>>
R2.*21 |
