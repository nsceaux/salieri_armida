\clef "treble" R2.*6 |
\tag #'tutti <>^"a 2." do''2.~ |
do''~ |
do''~ |
do'' |
fa''16( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
re''8( mi''16 fa'') sol''( la'' sib'' do''') re'''( sib'' la'' sol'') |
fa''4~ fa''16 mi'' fa'' sol'' la''( sib'' la'' sol'') |
fa''4 r r |
R2. |
r4 r r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    \grace do'''16 sib''8 la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4 }
  { do''8 |
    \grace mi''16 re''8 do''16 sib' la'4. do''8 |
    \grace mi''8 fa''4 }
>> r4 r |
R2. |
r4 r r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    re'''16 sib'' la'' sol'' fa''4. sol''8 |
    fa''4 fa' }
  { do''8 |
    fa''16 re'' do'' sib' la'4. sib'8 |
    \sugNotes la'4 fa' }
>> r4 |
R2. |
r4 \twoVoices #'(primo secondo tutti) <<
  { fa''4. mi''8 |
    \grace mi''8 re''4.( do''8) do'' re'' |
    s4 sib'2 | }
  { re''4. do''8 |
    \grace do''8 sib'4.( la'8) la' sib' |
    s4 sol'2 | }
  { s2 | s2. | r4 s2 | }
>>
R2.*3 |
r4 r8 \twoVoices #'(primo secondo tutti) <<
  { sol''8 sol'' sol'' |
    sol''2.~ |
    sol''4. sol''8 sol'' sol'' |
    sol''2.~ |
    sol''4. sol''8 sol'' fa'' |
    mi''2.~ |
    \slurDashed mi''4.( fa''8 sol''4) |
    \grace sol''8 fa''4( mi'' re'') | \slurSolid
    do'' }
  { sol'8 sol' sol' |
    sol'2.~ |
    sol'4. sol'8 sol' sol' |
    sol'2.~ |
    sol'4. sol'8 sol' sol' |
    do''2.~ |
    \slurDashed do''4.( re''8 mi''4) |
    re''( do'' si') | \slurSolid
    do'' }
  { s4.\f |
    s2.\p |
    s4. s\f |
    s2.\p |
    s4. s\f |
    s2.\fp | }
>> r4 r |
R2.*7 | \allowPageTurn
do''8 do'''4( si''8 la'' sol'') |
\grace sol''8 fa''4. re''8( mi'' fa'') |
\twoVoices #'(primo secondo tutti) <<
  { sol''8 la''16 fa'' mi''4. re''8 | do''4 }
  { sol''8 fa''16 re'' do''4. si'8 | do''4 }
>> r4 r |
<>\p \twoVoices #'(primo secondo tutti) <<
  { la''4 la''8 sol'' la'' sib'' |
    \grace la''8 sol''2 s4 |
    sol''4 la''8 sib'' la'' sol'' |
    \grace sol''8 fa''2 s4 |
    la''4 \grace { sib''16[ do'''] } re'''8 do'''16 sib'' \grace sib''8*1/2 la''8 sol''16 fa'' |
    \grace fa''8 mi''2 }
  { do''4 do''8 mi'' fa'' re'' |
    \grace do''8 sib'2 s4 |
    sib'4 do''8 re'' do'' sib' |
    \grace sib'8 la'2 s4 |
    do''4 \grace { re''16[ mi''] } fa''8 mi''16 re'' \grace re''8*1/2 do''8 sib'16 la' |
    \grace la'8 sol'2 }
  { s2. | s2 r4 |
    s2. | s2 r4 | }
>> r4 |
R2. |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''4. mi''8 | re''4 }
  { re''4. do''8 | sib'4 }
>> r4 r |
R2. |
\twoVoices #'(primo secondo tutti) <<
  { mi''2.~ | mi''\fermata | }
  { sib'2.~ | sib'\fermata | }
  { s2.-\sug\cresc | s-\sug\f | }
>>
R2.*2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { fa''4 mi'' | fa'' }
  { la'4 sol' | la' }
>> r4 r |
R2.*7 |
r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { la''8 sol'' fa'' mi'' re'' |
   do''4 mi'' fa'' |
   re''8 sib'' la''4 sol'' |
   fa''4 }
  { fa''8 mi'' re'' do'' sib' |
    la'8 do''4 do'' do''8 |
    sib' re'' fa''4 mi'' |
    fa'' }
>> r4 r |
R2.*5 |
\tag #'tutti <>^"a 2." do''2.~ |
do''~ |
do''~ |
do'' |
fa''16( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
re''8 mi''16( fa'' sol'' la'' sib'' do''') re'''( sib'' la'' sol'') |
fa''4~ fa''16 mi'' fa'' sol'' la''( sib'' la'' sol'') |
fa''4 r r |
R2. |
r4 r r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    sib'' la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4 }
  { do''8 |
    re'' do''16 sib' la'4. do''8 |
    \grace mi''8 fa''4 }
>> r4 r |
R2. |
r4 r r8 \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    re'''16 sib'' la'' sol'' fa''4. sol''8 |
    fa''4 }
  { do''8 |
    fa''16 re'' do'' sib' la'4. sib'8 |
    la'4
  }
>> r4 r |
