\clef "bass" R2.*5 |
r4 r r8 sib |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la2.:8 |
sib:8 |
do'8 do' do' do' do do |
fa4 fa, r |
R2. |
r4 r r8 la-\sug\f |
sib4 do' do |
fa fa, r |
R2. |
r4 r r8 la-\sug\f |
sib4 do' do |
fa fa, r |
R2. |
\clef "tenor" r4 \twoVoices #'(primo secondo tutti) <<
  { fa'4. mi'8 | re'4. do'8 do' re' | }
  { re'4. do'8 | sib4. la8 la sib | }
>>
r4 \clef "bass" \twoVoices #'(primo secondo tutti) <<
  { sib2 | }
  { sol | }
>>
R2. |
r4 fa8 do la, fa, |
R2.*2 |
r4 r do-\sug\p |
sol r r |
r r sol, |
do r r |
do'4(\fp si la) |
\once\slurDashed sol4.( fa8 mi4) |
fa sol sol, |
do do, r |
R2. |
r4 r r8 mi\p |
fa4 sol sol, |
do do, r |
R2. |
r4 r r8 mi\f |
fa4 sol sol, |
do do' do' |
re'( si) sol8 fa |
mi fa sol4 sol, |
do do, r |
R2.*7 |
r4 \clef "tenor" <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fa'4. mi'8 | re'4 }
  { re'4. do'8 | sib4 }
>> r4 r |
r4 \clef "bass" fa8 do la, fa, |
sol,4-\sug\cresc sol, sol, |
sol,2.-\sug\f\fermata |
\clef "tenor" \grace s16 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { fa'8 mi' re' do' sib la |
    \grace la8 sol2 la8 sib |
    do'8 sib do'4 do' | }
  { re'8 do' sib la sol fa |
    \grace fa mi2 fa8 sol |
    la sib do'4 do' | }
>>
\clef "bass" fa4 fa, r |
R2. |
r4 r r8 fa-\sug\p |
sib,4 do do, |
fa,4 r r |
R2. |
r4 r r8 fa-\sug\p |
sib,4 do do, |
fa,2.\f |
fa,4 sib( la) |
sib8 sib do' do' do do |
fa,4 fa fa |
fa fa fa |
fa fa fa |
sol sol8 fa mi re |
do4 do do |
fa fa, r8 sib |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la-.( sib-. la-. sib-. la-. mi-.) |
fa-.( mi-. fa-. mi-. fa-. sol-.) |
la2.:8 |
sib:8 |
do'8 do' do' do' do do |
fa4 fa, r |
R2. |
r4 r r8 la\f |
sib4 do' do |
fa fa, r |
R2. |
r4 r r8 la\f |
sib sib do' do' do do |
fa4 r r |
