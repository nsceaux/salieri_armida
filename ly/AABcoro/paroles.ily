Spar -- so di pu -- re bri -- ne
all’ au -- re mat -- tu -- ti -- ne
co -- me ver -- mi -- glio
\tag #'(voix1 voix2) { co -- me ver -- mi -- glio un fior }
\tag #'voix3 { un fior }
spun -- ta sul pri -- mo al -- bor
spun -- ta sul pri -- mo al -- bor
poi lan -- gue, e muo -- re,
poi lan -- gue, e muo -- re,
poi lan -- gue, e muo -- re.
Pas -- sa per noi co -- sì
il fior del -- la bel -- tà,
il fior del -- la bel -- tà,
pas -- sa per noi co -- sì
e du -- ra un bre -- ve dì,
\tag #'(voix1 voix2) { se nel -- la fre -- sca e -- tà }
nol’ co -- glie a -- mo -- re,
se nel -- la fre -- sca e -- tà
nol’ co -- glie a -- mo -- re,
se nel -- la fre -- sca e -- tà
nol’ co -- glie a -- mo -- re.
