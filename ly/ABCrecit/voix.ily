\clef "alto/treble" r4 r8 sol' sol' sol' sol' do'' |
do'' sol' sol' sol'16 sol' mi'8 mi' r mi'16 fa' |
sol'4 sol'8 lab' fa' fa' lab' \ficta sib'16 do'' |
\ficta lab'8 lab' r fa'16 sol' lab'!4 lab'8 sol' |
\ficta mib'4 sol'8 sol'16 lab' sib'8 sib' r sib' |
reb''8 reb'' r do'' lab' lab' r4 |
lab'8 lab' lab'16 lab' sib'8 do'' do'' r do'' |
do'' do'' do'' do'' do'' sol' r16 sol' sol' lab' |
sib'8 sib' sib' sib'16 sib' sib'8 reb'' sib' sib' |
r sib' sib' do'' lab' lab' r fa' |
lab'8 lab' lab' do'' si'! si' r16 si' si' do'' |
do''8 sol' r4 r2 |
r r4 la'8 la' |
la' mi' mi' mi' dod' dod' r16 mi' mi' fa' |
sol'8 sol' r sol'16 sib' sol'8 sol' r sol'16 fa' |
re'8 re' r4 r re''8 re''16 re'' |
re''8 la' la' la'16 la' fad'4 r8 re' |
fad'4 r16 fad' la' sib' do''8 do'' r do''16 mib'' |
do''4 do''8 re'' sib' sib' r sol' |
sib'4 r r r8 sol' |
la' la' la' si'!16 do'' do''8 si' r4 |
r4 r8 do'' do'' sol' r4 |
R1 |
