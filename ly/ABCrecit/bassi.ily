\clef "bass" mi!2 r |
R1 |
r2 fa4 r |
re4 r r2 |
mib r |
r do |
R1 |
r2 mi! |
R1 |
r2 fa |
r fa |
r4 sol\f do'( sib) |
la sib16( la sib la) sol2~ |
sol1~ |
sol |
fa4( mi) re2~ |
re r |
R1 |
r2 sol4 r |
sol(\f fa! mi!) r |
fa4 r4 r fa~ |
fa r r sol\f |
do2 r |
