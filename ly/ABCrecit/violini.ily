\clef "treble" mi'!2 r |
R1 |
<<
  \tag #'primo {
    r2 lab'4 r |
    lab' r r2 |
    sol'2 r |
    r2 lab' |
  }
  \tag #'secondo {
    r2 do'4 r |
    fa' r r2 |
    mib'2 r |
    r2 mib' |
  }
>>
R1 |
r2 << \tag #'primo do''2 \tag #'secondo sol' >> |
R1 |
<<
  \tag #'primo {
    r2 do'' |
    r <re' si'!> |
  }
  \tag #'secondo {
    r2 lab' |
    r sol' |
  }
>>
r4 <sol' re''>\f <sol' mib''> <<
  \tag #'primo {
    r16 mi''-.( mi''-. mi''-.) |
    mi''16( la''8 mi'' mi'' mi''16) mi''2~ |
    mi''1~ |
    mi''2 r |
    la''4 r16 sol''-.( sol''-. sol''-.) fad''2~ |
    fad''
  }
  \tag #'secondo {
    r4 |
    dod''4 dod''8 dod'' dod''2~ |
    dod''1~ |
    dod''2 r |
    re''4 r16 re''-.( dod''-. dod''-.) do''2~ |
    do''
  }
>> r2 |
R1 |
<<
  \tag #'primo {
    r2 sol''4 r |
    r8 sib'(-.\f sib'-. sib'-.) sib'4 r |
    la' r r si'!~ |
    si'
  }
  \tag #'secondo {
    r2 sib'4 r |
    r8 \once\slurDashed re'-.(-\sug\f re'-. re'-.) do'4 r |
    do' r r re'~ |
    re'
  }
>> r4 r <sol re' si'>4\f |
<sol mi' do''>2 r |
