\clef "alto" mi'!2 r |
R1 |
r2 fa'4 r |
sib r r2 |
sib2 r |
r2 do' |
R1 |
r2 mi'! |
R1 |
r2 fa' |
r fa' |
r4 sol'-\sug\f do'' r16 \once\slurDashed sol'-.( sol'-. sol'-.) |
la'4 sib'16( la') sib'( la') la'2 |
la'1~ |
la'2 r |
\once\slurDashed fa'4( mi') la2~ |
la r |
R1 |
r2 re4 r |
sol'-\sug\f fa' mi' r |
fa' r r sol'~ |
sol' r r sol'-\sug\f |
do'2 r |
