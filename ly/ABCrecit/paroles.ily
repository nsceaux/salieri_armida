In sì cru -- del dub -- biez -- za ah non per -- dia -- mo
i mo -- men -- ti in que -- re -- le. Un fin -- to ri -- so
cuo -- pra il no -- stro ter -- ror. Tut -- to re -- spi -- ri
le -- ti -- zia, e pa -- ce; e pa -- ro -- let -- te ac -- cor -- te
e lan -- gui -- di so -- spi -- ri, e mol -- li sguar -- di
tut -- to si met -- ta in o -- pra, e tut -- to al -- let -- ti
l’in -- cau -- to vin -- ci -- to -- re a’ sor -- si in -- fet -- ti.
Le di -- fe -- se io pre -- pa -- ro, e non co -- no -- sco
o il pe -- ri -- glio, o il ne -- mi -- co. Ah se lo gui -- da
for -- za mag -- gior, di fé, di ze -- lo al -- me -- no
si com -- pis -- can le par -- ti
e poi… Che veg -- gio? Ec -- co il ne -- mi -- co: All’ ar -- ti.
