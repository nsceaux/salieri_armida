\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ismene
    \livretVerse#12 { In si crudel dubbiezza ah non perdiamo }
    \livretVerse#12 { I momenti in querele. Un finto riso }
    \livretVerse#12 { Cuopra il nostro terror. Tutto respiri }
    \livretVerse#12 { Letizia, e pace; e parolette accorte }
    \livretVerse#12 { E languidi sospiri, e molli sguardi }
    \livretVerse#12 { Tutto si metta in opra, e tutto alletti }
  }
  \null
  \column {
    \livretVerse#12 { L'incauto vincitore à sorsi infetti. }
    \livretVerse#12 { Le difese preparo, e non conosco }
    \livretVerse#12 { O il periglio, o il nemico. Ah se lo guida }
    \livretVerse#12 { Forza maggior, di fé, di zelo almeno }
    \livretVerse#12 { Si compiscan le parti }
    \livretVerse#12 { E poi… Che veggio? Ecco il nemico: All’ arti. }
  }
  \null
}

       #}))
