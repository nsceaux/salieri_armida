<<
  \tag #'(primo primo-secondo) \clef "alto"
  \tag #'secondo \clef "tenor"
  \tag #'terzo \clef "bass"
>>
R1*38 | \allowPageTurn
r2 <>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { lab'4 lab' |
      lab'?4 s sold' sold' |
      sold'? s sold' s | }
    { fa'4 fa' |
      fa' s mi'! mi' |
      mi' s re' s | }
  >>
  \tag #'terzo {
    reb4 reb |
    reb s dod dod |
    \ficta dod s si,! s |
  }
  { s2 | s4 r s2 | s4 r s r | }
>>
r2 <>-\sug\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { la'4 mi' | mi' }
    { dod'4 dod' | dod'? }
  >>
  \tag #'terzo { la,4 la, | la, }
>> r4 r2 |
R1 |
<<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sol'4 s fa'! s |
      fa' s2. |
      fa'4 fa' fa' s |
      sol' sol' sol' s |
      s2. fa'4 | }
    { mi'4 s re' s |
      do' s2. |
      re'4 re' re' s |
      sib sib sib s |
      s2. do'4 | }
  >>
  \tag #'terzo {
    la,4 s re s |
    do' s2. |
    sib,4 sib, sib, s |
    mib mib mib s |
    s2. fa4 |
  }
  { s4 r s r |
    s r r2 |
    s2.-\sug\f r4 |
    s2. r4 |
    r2 r4 s | }
>>
