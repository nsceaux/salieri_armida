\clef "alto/treble" <>^\markup\character Ismene
r4 r8 si'16 si' si'4 la'8 si' |
sold' sold' r16 mi' mi' fad' sold'!8 sold' si' si'16 do'' |
la'8 la' r4 r2 |
r4 r8 mi' mi' mi' mi' fad' |
sold' sold' r sold'16 si' sold'4 sold'8 la' |
la' mi' r4 r2 |
R1 |
r8 sib' sib' sib' sol' sol' r sol'16 la' |
\ficta sib'4 sol'8 la' \grace sol' fa'4 r16 fa' fa' sol' |
la'8 la' r la'16 la' do''4 sib'8 do'' |
la' la' r fa'16 sol' la'4 la'8 sib' |
\ficta sib' fa' r16 sib' sib' sib' la'8 la' la' mi'16 fa' |
sol'8 sol' r la' \grace sol' fa'4 la'8 sib'16 do'' |
sib'8 sib' r sol' sib' sib' sib' do'' |
la' la' r4 do''4. do''8 |
la'4 sol'8 la' fa' fa' r fa' |
si'! si' r do'' do'' sol' r4 |
R1 |
\clef "soprano/treble" <>^\markup\character Armida
r4 r8 sib' mib'' sib' sib' sib' |
sol' sol' r4 reb'' reb''8 reb'' |
sib' sib' r16 sib' sib' sib' sol'8 sol' r sol' |
\ficta sib' sib' sib' do'' lab' lab' r4 |
fa''16 do'' do''8 do'' do'' lab'4 r |
r2 fa''8 fa'' r si' |
do'' do'' r4 r2 |
sol''4 mi''!8 mi'' do'' do'' r4 |
do''8 do''16 do'' do''8 do'' do'' sol' r sol'16 sol' |
do''4 do''8 re'' mi'' mi'' r16 mi'' re'' do'' |
fa''8 fa'' r4 r2 |
r8 do'' fa''16 do'' reb'' mib'' reb''8 reb'' r16 reb'' mib'' fa'' |
mib''8 mib'' mib'' mib''16 sib' do''8 do'' r \ficta lab' |
mib'' mib'' mib'' mib'' mi'' mi'' r4 |
reb''4 reb''8 reb'' do'' do'' r sol' |
sib' sib' do'' sol' lab' lab' r4 |
r2 r4 fa' |
lab''4. fa''8 fa''4. mib''8 |
reb''4 reb'' reb''8 reb''16 reb'' reb''8 reb'' |
do'' do'' r16 lab' do'' reb'' mib''8 mib'' r mib''16 mib'' |
mib''4 solb'8 lab' fa' fa' r4 |
R1 |
r8 dod'' mi''16 mi'' mi'' fad'' re''8 re'' re''16 re''32 re'' dod''16 re'' |
si'4 re''8 re''16 mi'' \ficta dod''8 dod'' r la'' |
la'' mi'' mi'' mi'' dod'' dod'' dod''16 dod''32 dod'' re''16 mi'' |
mi''8 la' la' dod''16 re'' mi''8 mi'' r mi'' |
sol''8 mi'' r16 mi'' mi'' fa''! re''4 fa''8 fa''16 sol'' |
mib''8 mib'' r16 do'' do'' re'' mib''4. re''8 |
sib'8 sib' r4 re''8 re''16 re'' fa''8. lab'16 |
sol'4 r r r8 sol'' |
sol''4 la'8 r16 \ficta sib' sib'8 fa' r4 |
