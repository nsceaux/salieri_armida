\clef "treble" <si sold'>2 r |
R1 |
<<
  \tag #'primo { la'8 la'4 la' la' la'8 | sold'2 }
  \tag #'secondo { mi'8 mi' fa' fa' fa' fa' mi' mi' | re'2 }
>> r2 |
R1 |
<<
  \tag #'primo {
    la'2 r8 la'-.(\p la'-. la'-.) |
    sib'8\fermata sib'8-.([ sib'-. sib'-.]) sib'2 |
  }
  \tag #'secondo {
    mi'2 r8 mi'-.(-\sug\p mi'-. mi'-.) |
    mi'8\fermata mi'8-.([ mi'-. re'-.]) do'2 |
  }
>>
R1*9 |
r2 r4 sol'-\sug\p |
<<
  \tag #'primo {
    lab'8 mib' do'16( reb' mib' fa') fa'8 mib'4 re'8 |
    mib'4 r r2 |
    sib8-.( mib'-. mib'-. mib'-.) mi'2~ |
    mi'1~ |
    mi'2 fa'4 r |
  }
  \tag #'secondo {
    lab'8 do' do'4 sib sib |
    \ficta sib r r2 |
    sib8-.( sib-. sib-. sib-.) sib2~ |
    sib1~ |
    sib2 lab4 r |
  }
>>
R1 |
<<
  \tag #'primo {
    si!1\fp |
    do'4 r r8. <sol' mi''>16\f q8. <do'' sol''>16 |
    q4 r r2 | \allowPageTurn
    r2 do''4 r |
    r2 mi''!4 r |
    fa''4 r r8. fa''16 fa''8. fa''16 |
    fa''4 r fa'' r |
    mib'' r mib'' r |
    r2 mi''!:32 |
    reb'\fp\fermata do'\p |
    r do'4
  }
  \tag #'secondo {
    lab1-\sug\fp |
    sol4 r r8. <sol' mi''>16-\sug\f q8. q16 |
    q4 r r2 | \allowPageTurn
    r2 sol'4 r |
    r2 sol'4 r |
    fa'4 r r8. do''16 do''8. do''16 |
    do''4 r reb'' r |
    \ficta sib'4 r do'' r |
    r2 sib':32 |
    lab-\sug\fp\fermata sol-\sug\p |
    r2 lab4
  }
>> r16 do''\f re'' mi'' |
fa'' sol'' lab'' sol'' fa'' mi'' fa'' sol'' fa'' reb'' do'' sib' lab' sol' fa' mi' |
<<
  \tag #'primo {
    fa'32\fp lab'' lab'' lab'' lab''[ lab'' lab'' lab''] lab''2.:32 |
    \ficta lab''2:32 lab'': |
    solb'': solb'':~ |
    solb''2_\markup\italic ten. fa'':\f |
    fa'': mi''!: |
    mi'':\p re'': |
    re'': dod'':\f |
    \ficta dod'':\fp dod'': |
    \ficta dod'': dod'': |
    \ficta dod'': re'': |
    mib'': mib'': |
  }
  \tag #'secondo {
    fa'32-\sug\fp do'' do'' do'' do''[ do'' do'' do''] do''2.:32 |
    reb''2: reb'': |
    do'': do'':~ |
    do''2_\markup\italic ten. reb'':-\sug\f |
    \ficta reb'': dod'': |
    \ficta dod'':-\sug\p si': |
    si': mi':-\sug\f |
    mi':-\sug\fp mi': |
    mi': mi': |
    mi': fa': |
    << { do'': do'': } \\ { fa': fa': } >> |
  }
>>
sib'16\f do''32 re'' mib'' fa'' sol'' la'' sib''16 la''32 sol'' fa'' mib'' re'' do'' sib'4 r |
mib'16 fa'32 sol' lab' sib' do'' re'' mib''16 re''32 do'' sib' lab' sol' fa' mib'4 r8. << sol''16 \\ la'! >> |
<sol'' la'>2 r4 <fa' do''>4 |
