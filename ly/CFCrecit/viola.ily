\clef "alto" mi2 r |
R1 |
do'8 do' re' re' re' re' do' do' |
si2 r |
R1 |
do'2 r8 do'-.(-\sug\p do'-. do'-.) |
do'8\fermata \once\slurDashed do'8-.([ mi'-. fa'-.]) sol'2 |
R1*9 |
r2 r4 sol'-\sug\p |
lab'4 lab sol lab |
sol r r2 |
sol4 \ficta mib' reb'2~ |
reb'1~ |
reb'2 do'4 r |
R1 |
fa1-\sug\fp |
mi4 r r8. do'16-\sug\f do'8. do'16 |
sib4 r r2 | \allowPageTurn
r mi'4 r |
r2 do''4 r |
do'' r r8. la!16 la8. la16 |
la4 r sib r |
reb'4 r do' r |
r2 sol:32 |
lab-\sug\fp\fermata sib-\sug\p |
r2 lab4 r |
r4 r16 do-\sug\f re mi fa reb' do' sib lab sol fa mi |
fa32-\sug\fp fa' fa' fa' fa'[ fa' fa' fa'] fa'2.:32 |
fa'2:32 fa': |
mib': mib':~ |
mib'2_\markup\italic ten. lab':\f |
\ficta lab': sold': |
\ficta sold': sold': |
\ficta sold': la':\f |
la':\fp la': |
la': la': |
la': la': |
la': la': |
sib16-\sug\f do'32 re' mib' fa' sol' la' sib'16 la'32 sol' fa' mib' re' do' sib4 r |
mib16 fa32 sol lab sib do' re' mib'16 re'32 do' sib lab sol fa mib4 r8. do'16 |
do'2 r4 do' |
