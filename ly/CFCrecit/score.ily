\score {
  <<
    \new StaffGroup \with { \haraKiriFirst } <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff << \global \keepWithTag#'primo-secondo \includeNotes "tromboni" >>
        \new Staff << \global \keepWithTag#'terzo \includeNotes "tromboni" >>
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket \with { \haraKiri } <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \withLyrics <<
        { \set Staff.shortInstrumentName = \markup\character Is. s1*18
          \set Staff.shortInstrumentName = \markup\character Ar. }
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s1 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
          s2 s1*3\break s1*3\pageBreak
          s1*3\break s1*3\pageBreak
          s1*4\break s1*3\pageBreak
          s1*3 s2 \bar "" \break s2 s1*2\pageBreak
          s1*3\break s1*3\pageBreak
          s1*3\pageBreak
          s1*2 s2 \bar "" \pageBreak
          s2 s1*2\pageBreak
          s1*2 s2 \bar "" \pageBreak
          s2 s1*2
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
