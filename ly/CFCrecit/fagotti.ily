\clef "bass" R1*38 |
r2 reb4-\sug\f reb |
reb r dod dod |
\ficta dod r si,! r |
r2 la,4-\sug\f la, |
la, r r2 |
R1 |
la,4 r re r |
do! r r2 |
sib,4-\sug\f sib, sib, r |
mib mib mib r |
r2 r4 fa |
