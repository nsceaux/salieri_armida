In -- fe -- li -- ce Re -- gi -- na! an -- cor re -- spi -- ra,
pal -- pi -- ta an -- co -- ra. A ri -- chia -- mar -- la in vi -- ta
a -- iu -- ta -- te -- mi a -- mi -- che. Al tuo do -- lo -- re
deh non ce -- der co -- sì. D’un tra -- di -- to -- re
non ac -- cre -- sca il tri -- on -- fo
la tua mor -- te in -- fe -- li -- ce: a ven -- di -- car -- ti
ser -- ba -- ti in vi -- ta al -- men del -- la ven -- det -- ta
la spe -- me non t’è tol -- ta.
Deh con -- so -- la -- ti, Ar -- mi -- da, Ar -- mi -- da, a -- scol -- ta.

Per chi ri -- tor -- no in vi -- ta? ah col -- la mor -- te
fi -- ni -- sci al -- me -- no il mio cru -- del mar -- ti -- ro.
Sve -- na -- mi, tra -- di -- tor. Stel -- le! che mi -- ro?
Fug -- ge il cru -- del? Sul -- la de -- ser -- ta ri -- va
mi la -- scia scon -- so -- la -- ta e se -- mi -- vi -- va!
E non l’in -- ghiot -- te il ma -- re? e a in -- ce -- ne -- rir -- lo
ful -- min non piom -- ba? e il duol, che mi di -- vo -- ra
mi -- ra l’a -- bis -- so, e non si scuo -- te an -- co -- ra?
Sor -- ge -- te, al -- fin sor -- ge -- te
al -- la te -- mu -- ta vo -- ce, o fu -- rie in -- fe -- ste,
del -- la not -- te pro -- fon -- da.
Strug -- ge -- te o Dei, strug -- ge -- te
que -- ste del mio ros -- sor ri -- ve fu -- ne -- ste.
I ven -- ti, e le tem -- pe -- ste
tur -- bi -- no il Cie -- lo, è l’on -- da. Ah più non chie -- do
di -- fe -- sa a un fol -- le a -- mor. Vo -- glio ven -- det -- ta
del -- la mia fé tra -- di -- ta.
Que -- sta spe -- ran -- za sol mi ser -- ba in vi -- ta.
