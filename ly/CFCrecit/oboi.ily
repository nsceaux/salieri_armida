\clef "treble" R1*38 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fa''4 fa'' |
    fa''4 s mi''! mi'' |
    mi'' s re'' s | }
  { lab'4 lab' |
    lab'? s sold' sold' |
    sold'? s sold' s | }
  { s2 | s4 r s2 | s4 r s r | }
>> |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { dod''4 la'' | la'' }
  { la'4 dod''! | dod''? }
>> r4 r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 s fa''! s |
    mib'' s2. |
    re''4 re'' re'' s |
    sol'' sol'' sol'' s |
    s2. do''4 | }
  { la'4 s la' s |
    la' s2. |
    fa'4 fa' fa' s |
    sib' sib' sib' s |
    s2. fa'4 | }
  { s4 r s r |
    s r r2 |
    s2.-\sug\f r4 |
    s2. r4 |
    r2 r4 s | }
>>
