\piecePartSpecs
#`((oboi #:score-template "score-voix")
   (fagotti #:score-template "score-voix"
            #:indent 0
            #:music , #{
<>^\markup "Bassi"
\mmRestInvisible mi,2 r |
\mmRestVisible R1 | \mmRestInvisible
do8 do re re re re do do |
si,2 r |
\mmRestVisible R1 | \mmRestInvisible
la,2 r8 la(-.\p la-. la-.) |
sol8\fermata sol-.([ sol-. fa-.]) mi2~ |
mi1~ |
mi2 fa~ |
fa1~ |
fa2 mib |
re2 dod~ |
dod re |
mi1 |
fa2 r |
\mmRestVisible R1 | \mmRestInvisible
r2 r4 sol\p |
lab4 lab( sol lab) |
sol r r2 |
sol4 sol sol2~ |
sol1~ |
sol2 fa4 r |
\mmRestVisible R1 | \mmRestInvisible
reb1\fp |
do4 r r8. do'16\f do'8. do'16 |
sib2 r | \allowPageTurn
r mi4 r |
r2 sib4 r |
lab r r8. la16 la8. la16 |
la4 r sib r |
sol4 r lab r |
r2 sol2:32 |
fa2\fp\fermata mi!\p |
r2 fa4 r |
r4 r16 do\f re mi fa reb' do' sib lab sol fa mi |
fa2-\sug\fp r |
fa2:32\p fa: |
mib: mib: |
mib2 \mmRestVisible <>^"Fagotto"

                        #})
   (tromboni #:score-template "score-tromboni-voix")

   (violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ismene
    \livretVerse#12 { Infelice Regina! Ancor respira, }
    \livretVerse#12 { Palpita ancora. A richiamarla in vita }
    \livretVerse#12 { Ajutatemi amiche. Al tuo dolore }
    \livretVerse#12 { Deh non ceder così. D’un traditore }
    \livretVerse#12 { Non accresca il trionfo }
    \livretVerse#12 { La tua morte infelice. A vendicarti }
    \livretVerse#12 { Serbati in vita almen della vendetta }
    \livretVerse#12 { La speme non t’è tolta. }
    \livretVerse#12 { Deh consolati, Armida, Armida, ascolta. }
    \livretVerse#12 { Perche ritorno in vita? Ah colla morte }
    \livretVerse#12 { Finisci almeno il mio crudel martiro, }
    \livretVerse#12 { Svenami, traditor. Stelle! Che miro? }
    \livretVerse#12 { Fugge il crudel? Sulla deserta riva }
  }
  \null
  \column {
    \livretVerse#12 { Mi lascia sconsolata e semiviva! }
    \livretVerse#12 { E non l’inghiotte il mare? E a incenerirlo }
    \livretVerse#12 { Fulmin non piomba? E il duol, che mi divora }
    \livretVerse#12 { Mira l’abisso, e non si scuote ancora? }
    \livretVerse#12 { Sorgete alfin sorgete }
    \livretVerse#12 { Alla temuta voce, o furie infeste, }
    \livretVerse#12 { Della notte profonda. }
    \livretVerse#12 { Struggete, oh Dei, struggete }
    \livretVerse#12 { Queste del mio rossor rive funeste. }
    \livretVerse#12 { I venti, e le tempeste }
    \livretVerse#12 { Turbino il Cielo, è l’onda. Ah più non chiedo }
    \livretVerse#12 { Difesa a un folle amor. Voglio vendetta }
    \livretVerse#12 { Della mia fè tradita. }
    \livretVerse#12 { Questa speranza sol mi serba in vita. }
  }
  \null
}
#}))
