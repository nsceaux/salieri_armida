\key do \major
\time 4/4 \midiTempo#80 s1*5 s2 \tempo "Adagio" s2
s1*10 s2.
\tempo "Adagio" s4 s1*7 s2
\tempo "Allegro" s2 s1*24 \bar "|."
