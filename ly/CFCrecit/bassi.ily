\clef "bass" mi,2 r |
R1 |
do8 do re re re re do do |
si,2 r |
R1 |
la,2 r8 la(-.\p la-. la-.) |
sol8\fermata sol-.([ sol-. fa-.]) mi2~ |
mi1~ |
mi2 fa~ |
fa1~ |
fa2 mib |
re2 dod~ |
dod re |
mi1 |
fa2 r |
R1 |
r2 r4 sol\p |
lab4 lab( sol lab) |
sol r r2 |
sol4 sol sol2~ |
sol1~ |
sol2 fa4 r |
R1 |
reb1\fp |
do4 r r8. do'16\f do'8. do'16 |
sib2 r | \allowPageTurn
r mi4 r |
r2 sib4 r |
lab r r8. la16 la8. la16 |
la4 r sib r |
sol4 r lab r |
r2 sol2:32 |
fa2\fp\fermata mi!\p |
r2 fa4 r |
r4 r16 do\f re mi fa reb' do' sib lab sol fa mi |
fa2-\sug\fp r |
fa2:32\p fa: |
mib: mib: |
mib2 reb16\f mib32 fa solb lab sib do' reb'16 do'32 sib lab solb fa mib |
reb4 r dod16 red32 mi! fad sold lad sid dod'16 si32 la sold! fad mi red |
dod4 r4 si, r |
r2 la,16\f si,32 dod re mi fad sold la16 sold32 fad mi re dod si, |
la,4 r r2 |
R1 |
la,4 r re r |
do! r r2 |
sib,16-\sug\f do32 re mib fa sol la sib16 la32 sol fa mib re do sib,4 r |
mib16 fa32 sol lab sib do' re' mib'16 re'32 do' sib lab sol fa mib4 r8. mib16 |
\ficta mib2 r4 fa |
