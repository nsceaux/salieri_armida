\clef "alto" do'2_\markup\small\italic dol. re'4 |
mib' mib' do' |
sol'2. |
sol'4 sol' r |
r4 r8 si'8-.( do''-. si'-.) |
r4 r8 do''8-.( re''-. do''-.) |
do''4 do'' fa' |
sol' r r |
sol' r r |
sol8.\rf lab32*2/3( sol fad) sol8-.( sol-. sol-. sol-.) |
sol8.\rf lab32*2/3( sol fad) sol8-. sol-. sol-. sol16( fa) |
fa4 fa fad |
sol4.-\sug\f lab8-! sol-! fa!-! |
mib4_\markup\smaller\italic dolce do' re' |
mib' mib' mib'16-\sug\rf do' do' do' |
fa'4 sol' sol |
do'2 r4 |
