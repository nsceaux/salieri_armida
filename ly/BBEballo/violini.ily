\clef "treble"
<<
  \tag #'primo {
    do''8.(_\markup\italic dol. mib''16) mib''4. fa''8 |
    \grace fa''8 sol''4 sol''4. \once\slurDashed fa''16( mib'') |
    re''8 re''4 mib''16 fa'' sol''8 fa'' |
    \grace fa'' mib'' re''16 do'' do''4 r |
    sol''8.\rf lab''32*2/3( sol'' fad'') sol''8-.( sol''-. sol''-. sol''-.) |
    sol''8.\rf lab''32*2/3( sol'' fad'') sol''8-.( sol''-. sol''-. sol''16 lab'') |
    lab''8(\f lab''4 sol''16 fa'') fa'' mib'' re'' do'' |
    \grace re''8 do''4 si' r |
    \grace re''8 do''4 si' r |
    r4 r8 sol''\rf \grace sol''16 fad''8-! fad''-! |
    r4 r8 fa''!\rf \grace fa''16 mib''8-! mib''-! |
    \grace mib''16 re''8 re''4 re''16 mib'' mib''( re'') re''( do'') |
    si'8. do''16 re''4 r |
    do''8.(_\markup\smaller\italic dolce mib''16) mib''4. fa''8 |
    \grace fa''8 sol''4 sol''~ sol''16\rf sol''( do''' sib'') |
    \grace sib''8 lab'' sol''16( fa'') mib''4. re''8 |
    do''2 r4 |
  }
  \tag #'secondo {
    do''2._\markup\small\italic dol. |
    do''4 do'' mib''8 re''16( do'') |
    \grace do''8 si'4 si' si' |
    do'' mib' r |
    r r8 re''-.( mib''-. re''-.) |
    r4 r8 mib''-.( fa''-. mib''-.) |
    lab'4-\sug\f lab' lab' |
    mib'4 re' r4 |
    mib'4 re' r4 |
    r4 r8 mib''-\sug\rf \grace mib''16 re''8-. re''-. |
    r4 r8 re''-\sug\rf \grace re''16 do''8-. do''-. |
    lab'4 lab' la' |
    re'8. do'16 si4 r |
    do''2._\markup\smaller\italic dolce |
    do''4 do'' do''16-!-\sug\rf mib''( lab'' sol'') |
    \grace sol''8 fa'' mib''16( re'') do''4. si'8 |
    do''2 r4 |
  }
>>