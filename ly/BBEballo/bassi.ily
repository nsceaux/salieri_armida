\clef "bass" do2(_\markup\italic dol. re4) |
mib4 mib do |
sol sol sol, |
do2 do,4 |
R2.*2 |
fa4 fa fa |
sol sol8 fa mib re |
sol,2 r4 |
sol8.\rf lab32*2/3( sol fad) sol8-.( sol-. sol-. sol-.) |
sol8.\rf lab32*2/3( sol fad) sol8-. sol-. sol-. \once\slurDashed sol16( fa) |
fa4 fa fad |
sol4.\f fa!8-! mib-! re-! |
do2_\markup\italic dolce re4 |
mib mib do\rf |
fa sol sol, |
do2 r4 |
