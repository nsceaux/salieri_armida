\score {
  \new StaffGroup <<
    \new GrandStaff \with { \violiniInstr } <<
      \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
      \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
    >>
    \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s2.*4\break s2.*5\pageBreak
        s2.*5\break \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
