\clef "soprano/treble" R1*8 |
fa''2 re''4. do''8 |
sib'2. la'8[ sol'] |
fa'2.\melisma mib'4\melismaEnd |
re'4 r r2 |
\grace re''8 do''4 sib'2 re''4 |
\grace re''8 do''4( sib'2) si'4 |
\grace si'8 do''4 do'' r2 |
fa''2 mib''4. re''8 |
\grace re''8 do''4( sib'2) la'4 |
sib'4 sib' r2 |
R1 |
sib'2 re''4. mib''8 |
fa''2. mib''8[ re''] |
do''4 do'' r2 |
\grace fa''8 mi''4 mi''2 fa''4 |
sol''2~ sol''8[\melisma fa'' \ficta mi'' re''] |
do''2.\melismaEnd sib'4 |
\grace sib'8 la'2 r |
sib'4 la'8 la' sib'4 do'' |
re'' do'' r2 |
fa''4 mi''8 mi'' fa''4 sol'' |
la''4.\melisma sol''8 la''([ sol'' fa'' mib''!]) |
re''2\melismaEnd fa''8[ re''] re''8 sib' |
la'2 sol' |
\once\tieDashed la'4~\melisma la'16[ do'' sib' la'] sib'[ re'' do'' sib'] do''[ mib''! re'' do''] |
re''4 do''16[ mib'' re'' do''] re''[ fa'' mi'' re''] mi''[ sol'' fa'' mi''] |
fa''4 mi''16[ sol'' fa'' mi''] fa''[ la'' sol'' fa''] sol''[ sib'' la'' sol''] |
la''8[ sol''16 fa''] \ficta mi''[ fa'' sol'' mi''] fa''8[ mi''16 re''] do''[ re'' mib'' do''] |
re''4\melismaEnd fa''8[ re''] re''[ sib'] sib'[ sol'] |
la'1 |
sol' |
fa'2 r |
R1 |
do''2 do''4 do'' |
fa''2. fa''4 |
mi''4 do'' r r8 do'' |
fa''2. fa''4 |
mi''4 do''2 do''4 |
fa'' re'' si' sol' |
do'' do'' r2 |
r2 r4 r8 do'' |
do''4 re''8([ dod'']) re''([ do'']) sib'([ la']) |
la'2 sib'4 r8 sib' |
sib'4 do''8([ si']) do''([ sib']) la'([ sol']) |
sol'2 la'4 r8 do'' |
fa''2. mib''4 |
mib''2. mib''4 |
re'' re'' r re'' |
do''2 sib' |
la'2. sib'8[ sol'] |
\grace sol'8 fad'2 r |
r r4 sol' |
la'2 sol' |
do'' sib' |
mib''( re''4) fad'' |
sol''2~ sol''8[\melisma fa''! mib'' re''] |
\grace re'' mib''2~ mib''8[ do'']\melismaEnd mib''[ do''] |
sib'2 la' |
sib'4 r r r8 re'' |
sol''2~ sol''8[\melisma fa'' mib'' re''] |
\grace re''8 mib''2~ mib''8[\melismaEnd do''] mib''[ do''] |
sib'2 la' |
sol' r |
R1 |
sib'2 mib''4 sol'' |
lab''2. lab'4 |
sol' sib' r2 |
R1 |
mib''2 mib''4 sol'' |
\grace fa''8 mib''2. fa''4 |
re''4 re'' r2 |
\grace re''8 do''4 sib'2 re''4 |
\grace re''8 do''4\melisma sib'2\melismaEnd si'4 \grace si'8 do''4 do'' r2 |
fa''2 mib''4. re''8 |
\grace re''8 do''4( sib'2) la'4 |
sib' sib' r2 |
R1 |
sib'2 sib'4. sib'8 |
fa''2. sol''4 |
mi'' mi'' r2 |
lab'2 lab'4. lab'8 |
mib''2. fa''4 |
re'' re'' r2 |
sol'4( sol''4.) fa''8 mib''[ re''] |
do''4 do'' r2 |
do''2 mib''4. re''8 |
do''2. sib'4 |
\grace sib'8 la'2 r4 r8 fa'' |
fa''4 sol''8[ fad''] sol''[ fa''] mib''[ re''] |
re''2 mib''4 r8 mib'' |
mib''4 fa''8[ mi''] fa''[ mib''] re''[ do''] |
do''2 re''4 r |
\once\tieDashed sib'2~\melisma sib'16[ do'' sib' do''] re''[ mib'' re'' mib''] |
fa''2 sib'16[ do'' sib' do''] re''[ mib'' re'' mib''] |
fa''[ sol'' fa'' mib''] re''[ mib'' re'' mib''] fa''[ sol'' lab'' sol''] \grace sib''8 lab''[ sol''16 fad''] |
sol''2\melismaEnd fa''8[ mib''] re''[ do''] |
sib'1 do'' |
sib'4 r8 fa'' fa''8.[ mib''16] re''8.[ do''16] |
sib'4 sib'8. fa''16 fa''8.[ mib''16] re''8.[ do''16] |
sib'4 sib' fa''8.[ mib''16] re''8. do''16 |
sib'4 sib'8. fa''16 fa''8.[ mib''16] re''8.[ do''16] |
\once\tieDashed sib'4~\melisma sib'16[ do'' sib' do''] re''[ mib'' re'' mib''] fa''[ mi'' fa'' mib''] |
\once\tieDashed re''4~ re''16[ mib'' re'' mib''] fa''[ sol'' fa'' sol''] lab''[ sol'' lab'' fa''] |
sol''[ fad'' sol'' fad''] sol''[ fad'' sol'' fad''] sol''[ sib'' la'' sib'']\melismaEnd \grace la''16 sol''8[ \ficta fa''16 mib''] |
re''2 do'' |
fa'' sol'' |
re'' do'' |
fa'' sol'' |
re''1 |
do'' |
sib'2 r |
R1*8 |
