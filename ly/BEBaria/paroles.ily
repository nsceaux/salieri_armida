Ve -- do l’a -- bis -- so or -- ren -- do,
on -- de ri -- tras -- si il pie -- de,
on -- de ri -- tras -- si il pie -- de
sen -- to d’o -- nor, di fe -- de
mil -- le ri -- mor -- si al cor,
sen -- to d’o -- nor, di fe -- de
mil -- le ri -- mor -- si al cor, __
mil -- le ri -- mor -- si al cor, __
mil -- le ri -- mor -- si al cor.

Tut -- to mi fà spa -- ven -- to
mi fà spa -- ven -- to
do -- vun -- que vol -- go il ci -- glio;
ma in fac -- cia al mio pe -- ri -- glio,
ma in fac -- cia al mio pe -- ri -- glio
la fiam -- ma an -- co -- ra io sen -- to
d’un ma -- le e -- stin -- to ar -- dor,
d’un ma -- le e -- stin -- to ar -- dor,
d’un ma -- le e -- stin -- to ar -- dor,
d’un ma -- le e -- stin -- to ar -- dor.

D’un ne -- ro mar cruc -- cio -- so
tut -- te le in -- si -- die ho scor -- to.
Gra -- zia è del Ciel pie -- to -- so
s’io non ri -- ma -- si as -- sor -- to,
ma pur vi -- ci -- no al por -- to
ma pur vi -- ci -- no al por -- to
for -- se mi per -- do
for -- se mi per -- do an -- cor
ma pur vi -- ci -- no al por -- to
ma pur vi -- ci -- no al por -- to
for -- se mi per -- do an -- cor
ma pur vi -- ci -- no vi -- ci -- no al por -- to
for -- se mi per -- do
mi per -- do an -- cor __
mi per -- do an -- cor
mi per -- do an -- cor
mi per -- do an -- cor.
