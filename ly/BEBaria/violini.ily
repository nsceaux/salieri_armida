\clef "treble" \mergeDifferentlyHeadedOn
<<
  \tag #'primo {
    \voiceOne << fad''2 <la' re'>4 >> \oneVoice <la' fad''>8 q <la' mi''> <la' fad''> |
    \grace la''8 <sol'' la'>4( << fad''4.) la'4 >> <la' fad''>8-! <la' mi''>-! <la' fad''>-! |
    \grace la''8 \once\slurDashed <sol'' la'>4( <la' fad''>) <re'' re'>2 |
  }
  \tag #'secondo {
    <la' fad''>16 <la' la''> q q q4:16 q2: |
    q: q: |
    q: la''16 \ficta fad'' mi'' re'' dod'' re'' mi'' re'' |
  }
>>
si'16 re'' sol'' si'' la'' sol'' fad'' mi'' re'' dod'' mi'' re'' fad'' mi'' la''32 sol'' fad'' mi'' |
<<
  \tag #'primo {
    <re'' re'>2~ re''8_\markup\whiteout { \dynamic mf \italic appassionato } fa''!-! mi''-! fa''-! |
    \grace la''8 \once\slurDashed sol''4( fa''4.) fa''8-! mi''-! fa''-! |
    \grace la''8 sol''4( fa'') mib''2\p( |
    re'' do'') |
  }
  \tag #'secondo {
    re''16 la' la' la' la'4:16 la'2: |
    la': la': |
    la'16 la' la' la' la'8 la' la'2(-\sug\p |
    \ficta sib' la) |
  }
>>
%%
<<
  \tag #'primo {
    sib'8\p fa'' fa'' fa'' re'' re'' re'' do'' |
    sib' sib' sib' sib' sib' sib'( la' sol') |
    fa' fa' fa' fa' fa' fa' mib' mib' |
    re'-! fa'-! fa'-! sib'-! sib'-! re''( fa'' re'') |
    \grace re''8 do''4(\rf sib'8) sib' sib' sib' re'' re'' |
    \grace re''8 do''4(\rf sib'8) sib' sib' sib' si' si' |
    \grace si' do''4 do''4. do''8( re'' mib'') |
    \voiceOne << fa''2 <sib' re'>4\p >> \oneVoice mib''4. re''8 |
    \grace re''8 \once\slurDashed do''4( sib'2) la'4 |
    sib'4 sib''8.\f sib''16 fa''8. fa''16 mib''8. mib''16 |
    re''8. re''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'2:8\p re''8 re'' re'' mib'' |
    fa''2:8 fa'': |
    fa''2: fa'': |
    mi''2.: fa''8 fa'' |
    sol''2: sol''8( fa'' mi'' re'') |
    do''4 do''2 sib'4 |
    la'2: la': |
    sib'8 sib' la' la' sib' sib' do'' do'' |
    re'' re'' do'' do'' re'' re'' mi'' mi'' |
    fa'' fa'' mi'' mi'' fa'' fa'' sol'' sol'' |
    <la' la''>4.\f( sol''8) la''8\p r fa'' r |
    fa''4 r fa''8( re'') re''( sib') |
    la'2: sol': |
    la'4-! la'-! sib'-! do''-! |
    re''-! do''-! re''-! mi''-! |
    fa''16\cresc la'' sol'' fa'' mi'' sol'' fa'' mi'' fa'' la'' sol'' fa'' sol'' sib'' la'' sol'' |
    la''8\f sol''16 fa'' mi'' fa'' sol'' mi'' fa''8 mi''16 re'' do'' re'' \ficta mib'' do'' |
    re''4 fa''8\p r re'' r sib' r |
    la'16\ff la'' la'' la'' la''4:16 la''2: |
    sol'': sol'': |
    fa''8\f do'16 do' do'2.:16\p |
    do'2:\f do':\p |
    do':\f do':\p |
    <sol fa'>2:16\ff q: |
    <sol mi'>2:\f q:\p |
    <sol fa'>2:\ff fa'8 fa'' sol''8. la''32 si'' |
    do'''4-! sol''-! mi''-!
  }
  \tag #'secondo {
    sib8-\sug\p re'' re'' re'' sib' fa' fa' mib' |
    re' sol' sol' sol' sol' sol'( fa' mib') |
    re' re' re' re' re' re' do' do' |
    sib-! re'-! re'-! fa'-! re'-! sib'( re'' sib') |
    sol'(-\sug\rf la') sib'-! fa'-! fa' fa' sib' sib' |
    sol'(-\sug\rf la') sib' fa' fa' fa' lab'' lab'' |
    sol''4 sol''4. mib''8( re'' do'') |
    sib'2-\sug\p sol'4. fa'8 |
    \grace fa'8 \once\slurDashed mib'4( re'2) do'4 |
    re'4 <re'' fa''>8.-\sug\f q16 re''8. re''16 do''8. do''16 |
    sib'8. sib'16 re''8. re''16 sib'8. sib'16 fa'8. fa'16 |
    re'2:8-\sug\p sib'8 sib' sib' do'' |
    re''2: re'': |
    do'': do'': |
    sib': sib': |
    sib': sol'8 sol' la' la' |
    sol' sol' \ficta mi' mi' fa' fa' sol' sol' |
    fa'2: fa': |
    fa'2: fa': |
    fa': fa'8 fa' do'' do'' |
    do''2: do'': |
    << do''1 { s2\f s\p } >> |
    re''8 sib' fa' re' re'( fa') fa'( sol') |
    fa'2:8 mi':8 |
    fa'4-! fa'-! sol'-! la'-! |
    sib'-! la'-! sib'-! sol'-! |
    la'8-\sug\cresc do'' do'' do'' do''2:8 |
    do''8\f la' sib' sib' do'' do'' fa' fa' |
    fa'4 re''8-\sug\p r sib' r sol' r |
    fa'16\ff fa'' fa'' fa'' fa''4:16 fa''2: |
    fa''2: mi'': |
    fa''8-\sug\f la16 la la2.:16-\sug\p |
    la2:-\sug\f la:-\sug\p |
    sol:-\sug\f sol:-\sug\p |
    si:-\sug\ff si: |
    do'2:16-\sug\f do':-\sug\p |
    \ficta si:-\sug\ff si: |
    do'4 r r
  }
>> do''8.-! re''32 mi'' |
fa''4-! re''-! si'-! sol'8. la'32 si' |
do''4-! sol'-! mi'-! sol'-! |
do'2 r4 r8 <<
  \tag #'primo {
    do''8\p |
    do''4 re''8( dod'') re''( do'' sib' la') |
    la'2( sib'4) r8 sib' |
    sib'4 do''8( si') do''( sib' la' sol') |
    sol'2 la'4 r |
    fa''2. mib''!8 mib'' |
    mib''2:8 mib'': |
    re'': re'': do'': sib': |
    la'2.: sib'8 sol' |
    fad'8-!\f
  }
  \tag #'secondo {
    la'8-\sug\p |
    la'4 sib'8( la') sib'( la' sol' fad') |
    fad'2( sol'4) r8 sol' |
    sol'4 la'8( sold') la'( sol' fa' mi') |
    mi'2 fa'4 r |
    do''2:8 do'': |
    do'': do'': |
    do''2.: sib'8 sib' |
    sol'2: sol': |
    sol': mi': |
    re'8-!\f
  }
>> fad'( la' dod'') re''( fad'' la'' fad'') |
sol'' sib'' sol'' re'' sib' re'' sib' sol' |
<<
  \tag #'primo {
    la'2:8\p sol': |
    do'': sib': |
    mib'': re''8 re'' fad'' fad'' |
    sol''4\f sol''4.( fa''!8\p mib'' re'') |
    \grace re''8 mib''2 mib''8( do'') mib''( do'') |
    sib'2: la': |
  }
  \tag #'secondo {
    mib'2:8-\sug\p re': |
    re': re': |
    la': la'8 sol' la' re'' |
    sol''4-\sug\f re''2 sol'4-\sug\p |
    sol'2 la': |
    sol': fad': |
  }
>>
sib'8\f sib( re' fad') sol'( sib' re'' fad'') |
<<
  \tag #'primo {
    sol''8-\sug\fp sol'' sol'' sol'' sol''(\p fa''! mib'' re'') |
    \grace re''8 mib''2 mib''8( do'') mib''( do'') |
    sib'2:8 la': |
    sol'16\ff re'' re'' re'' re''2.:16 |
    re''2: re'': |
    mib'':\p mib'': |
    re'': re'': |
    mib'':\f mib'': |
    re'':\ff re'': |
    r8 mib''\p mib'' mib'' mib''2:8 |
    r8 mib'' mib'' mib'' mib'' mib'' fa'' fa'' |
    re''4-\sug\f re'' r8 re''( fa'' re'') |
    \grace re''8 do''4(\rf sib'8) sib' sib' sib' re'' re'' |
    \grace re''8 do''4(\rf sib'8) sib' sib' sib' si' si' |
    \grace si'8 do''4 do''8 do'' do''-! do''( re''\rf mib'') |
    \voiceOne << fa''2 <sib' re'>4\p >> \oneVoice mib''4. re''8 |
    \grace re''8 do''4( sib'2) la'4 |
    sib'4 sib''8.\ff sib''16 fa''8. fa''16 mib''8. mib''16 |
    re''8. re''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'2:8\p sib': |
    sib'2: sib': |
    sib': sib': |
    r8 lab' lab' lab' lab'2: |
    r8 \ficta lab' lab' lab' lab'2: |
    \ficta lab'4 lab'4. lab'8 lab' lab' |
    sol'4 sol''4.\f fa''8\p mib'' re'' |
    do''4. do''8 do''2:8 |
    do'':\p do'': |
    do'': do'': |
    do''4\ff <fa' la!> q r8 fa''\p |
    fa''4 sol''8( fad'') sol''( fa'' mib'' re'') |
    re''2( mib''4) r8 mib'' |
    mib''4 fa''8( mi'') fa''( mib'' re'' do'') |
    do''2 re''4 r |
  }
  \tag #'secondo {
    sol''8-\sug\fp re'' re'' re'' re''4 sol' |
    sol'2: la': |
    sol': fad': |
    sol'16-\sug\ff sib' sib' sib' sib'2.:16 |
    <fa' sib'>2: q: |
    << { sib'2: sib': } \\ { sol':-\sug\p sol': } >> |
    <sib' fa'>2:16 q: |
    <sol' sib'>:-\sug\f q: |
    <fa' sib'>:-\sug\ff q: |
    r8 sol'-\sug\p sol' sol' sol'2:8 |
    r8 sol' sol' sol' sol' sol' la' la' |
    sib'4-\sug\f sib' r8 sib'( re'' sib') |
    sol'(-\sug\rf la') sib' fa' fa' fa' sib' sib' |
    sol'(-\sug\rf la' sib') fa' fa' fa' lab'' lab'' |
    \grace lab''8 sol''4 sol''8 sol'' sol''-! mib''( re''\rf do'') |
    sib'2:8-\sug\p  sol'8 sol' sol' fa' |
    \grace fa'8 mib'4 re'2 do'4 |
    re' << { fa''8. fa''16 } \\ { re''8.-\sug\ff re''16 } >> re''8. re''16 do''8. do''16 |
    sib'8. sib'16 re''8. re''16 sib'8. sib'16 fa'8. fa'16 |
    re'2:8-\sug\p  re'8 re' mib' mib' |
    fa'2:8 fa': |
    mi': mi': |
    r8 \ficta mib'! mib' mib' mib'2:8 |
    r8 mib' mib' mib' mib'2: |
    re'8 fa' fa' fa' fa'2:8 |
    sib4 sib'4.-\sug\f re''8\p do'' si' |
    do''4. sol'8 sol'2:8 |
    sol'2:\p sol': |
    sol': sol': |
    fa'8-\sug\ff do' la do' <fa' la>4 r8 re''-\sug\p |
    re''4 mib''8( re'') mib''( re'' do'' si') |
    si'2( do''4) r8 do'' |
    do''4 re''8( dod'') re''( do'' sib' la') |
    la'2 sib'4 r |
  }
>>
sib'16\p sib' fa' fa' re' re' fa' fa' sib4 r |
sib'16 sib' fa' fa' re' re' fa' fa' sib4 r |
<<
  \tag #'primo {
    fa''8\p fa'' fa'' fa'' fa''2:8 |
    sol'': fa''8 mib'' re'' do'' |
    sib'16\p re'' re'' re'' re''2.:16 |
    do''2: do'': |
    <<
      { sib'16 fa'' fa'' fa'' fa''4:16 fa''2: |
        fa'': fa'': |
        fa'': fa'': |
        fa'': fa'': | } \\
      { sib'16\f re'' re'' re'' re''4: do''2: |
        re'': mib'': |
        re'': mib'': |
        re'': mib'': | }
    >>
    re''2:8\f re''8-.\p mib''-. fa''-. mib''-. |
    re''-.\f re''-.\p re''-. mib''-. fa''-. sol''-. lab''-. fa''-. |
    sol''16\f fad'' sol'' fad'' sol'' fad'' sol'' fad'' sol'' sib'' la'' sib'' \grace la''16 sol''8 \ficta fa''16 mib'' |
    re''2:8\f do'': |
    <sib' fa''>:16-\sug\p <sib' sol''>: |
    re'': do'': |
    <sib' fa''>2:16\ff <sib' sol''>: |
    fa''16 re''' re''' re''' re'''4:16 re'''2: |
    do''': do''': |
    <sib'' sib'>16 fa'' fa'' fa'' fa''4:16 fa''8-. re''-. re''-. re''-. |
    \grace fa''8 mib''4( re''4.) re''8-. do''-. re''-. |
    fa''8( mib'' re'') fa''
  }
  \tag #'secondo {
    re''2:8-\sug\p sib': |
    sib': la'8 sol' fa' mib' |
    re'16-\sug\p sib' sib' sib' sib'2.:16 |
    la'2: la': |
    sib'8-.\f sib-. re'-. sib-. do' la do' la |
    re' sib re' sib do' la do' la |
    re' sib re' sib do' la do' la |
    re' sib re' sib do' la do' la |
    sib-\sug\fp sib' sib' sib' sib' do'' re'' do'' |
    sib'-\sug\f sib'-\sug\p sib'-. do''-. re''-. mib''-. fa''-. re''-. |
    sib'2:8\f sib': |
    sib':\f la': |
    sib':16-\sug\p sib': |
    sib': la': |
    <sib' fa''>2:16-\sug\ff <sib' sol''>: |
    fa''16 <sib' sib''> q q q2.:16 |
    <la' la''>2: q: |
    sib'16 fa'' fa'' fa'' fa''2.:16 |
    fa''2: fa'': |
    fa'':
  }
>> sol''16 fa'' mib'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' la' sib' do'' sib' la' sol' fa' mib' |
re'4 sib'' \grace la''8 sol''4 fa''8 mib'' |
<<
  \tag #'primo {
    re''16 mib'' fa'' sol'' fa''8 re'' \grace sol''16 fa''8 mib''16 re'' \grace sol''16 fa''8 mib''16 re'' |
    do''2:16 do'': |
  }
  \tag #'secondo {
    sib'16 do'' re'' mib'' re''8 sib' \grace mib''16 re''8 do''16 sib' \grace mib''16 re''8 do''16 sib' |
    la'2:16 la': |
  }
>>
sib'4. re'16 mib' fa'8 re' fa' re' |
sib2 r |
