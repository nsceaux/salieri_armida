\clef "bass" re,2~ re,8 re'-! dod'-! re'-! |
mi'4( re'4.) re'8-! dod'-! re'-! |
\once\slurDashed mi'4( re') fad2 |
sol:8 la8 la la, la, |
re,2 r8 re'-!\mf dod'-! re'-! |
\grace fa'!8 mi'4( re'4.) re'8-! dod'-! re'-! |
\grace fa'8 mi'4( re') do'!2(\p |
sib fa!) |
%%
R1*7 |
re2:8\p mib: |
fa: fa,: |
sib,2-\sug\f r |
sib, r |
R1*10 |
fa2\f r |
R1*5 |
fa8-\sug\f fa sol sol la la la, la, |
sib,4 r r2 |
do'2:8-\sug\ff do': |
do: do: |
fa2-\sug\f r |
fa'1\fp |
mi'\fp |
re'\ff |
do' |
re'\ff |
do'4 r r2 |
r2 r4 sol8. la32 sib |
do'4\f sol mi sol |
do2 r4 r8 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { do'8 |
    \slurDashed do'4 re'8( dod') re'( do' sib la) |
    la2( sib4) s8 sib |
    sib4 do'8( si) do'( sib la sol) |
    sol2 la4 }
  { la8 |
    \slurDashed la4 sib8( la) sib( la sol fad) |
    fad2( sol4) s8 sol |
    sol4 la8( sold) la( sol fa mi) |
    mi2 fa4 }
  { s8 | s1 | s2. r8 s | }
>> r4 |
R1*10 |
sol,8\f sib, sib, sib, sib,4 r |
R1*3 |
sib,2:8-\sug\fp sib,4 r |
R1*2 |
sol,2-\sug\ff r4 sol-!\ff |
lab-! fa-! re-! sib,-! |
mib2 r4 mib-\sug\p |
fa-! re-! sib,-! re-! |
mib2-\sug\f r4 sol-\sug\ff |
lab-! fa-! re-! sib,-! |
mib'2\p re' |
do'1 |
sib,8\f re fa re sib,4 r |
R1*3 |
re2:8-\sug\p mib: |
fa: fa,: |
sib,2-\sug\f r |
sib, r |
R1*10 |
fa8\ff do la, do fa,4 r8 \clef "tenor" \twoVoices #'(primo secondo tutti) <<
  { fa'8 |
    \slurDashed fa'4 sol'8( fad') sol'( fa' mib' re') |
    re'2( mib'4) s8 mib' |
    mib'4 fa'8( mi') fa'( mib' re' do') |
    do'2 re'4 }
  { re'8 |
    \slurDashed re'4 mib'8( re') mib'( re' do' si) |
    si2( do'4) s8 do' |
    do'4 re'8( dod') re'( do' sib la) |
    la2 sib4 }
  { s8-\sug\p | s1 | s2 s4 r8 }
>> r4 |
\clef "bass" sib8-!-\sug\p fa-! re-! fa-! sib,4 r |
sib8-! fa-! re-! fa-! sib,4 r |
R1*2 |
fa2:8-\sug\p fa: |
fa,: fa,: |
sib,4\f r \twoVoices #'(primo secondo tutti) <<
  { mib'2 |
    re'4 s mib'2 |
    re'4 s mib'2 |
    re'4 s mib'2 |
    re'4 }
  { la2 |
    sib4 s la2 |
    sib4 s la2 |
    sib4 s la2 |
    sib4 }
  { s2 |
    s4 r s2 |
    s4 r s2 |
    s4 r s2 | }
>> r4 r2 |
R1*2 |
fa2:8\f fa,: |
re4 r r2 |
R1 |
re2:\ff mib: |
fa: fa: |
fa,: fa,: |
sib,2 r8 sib-. la-. sib-. |
\grace re'8 do'4( sib4.) sib8-. la-. sib-. |
\grace re'8 do'4( sib8.) re16 mib2:8 |
re4 r do r |
sib,8 sib, re re mib2:8 |
fa: fa: |
fa,: fa,: |
sib,4. re16 mib fa8-. re-. fa-. re-. |
sib,2 r |
