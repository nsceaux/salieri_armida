\clef "bass" re2~ re8 re-! dod-! re-! |
\once\slurDashed mi4( re4.) re8-! dod-! re-! |
\once\slurDashed mi4( re) fad2:8 |
sol:8 la8 la la, la, |
re2 \clef "tenor" <>^"Violoncelli" r8 re'-!_\markup { \dynamic mf \italic appassionato } dod'-! re'-! |
\grace fa'!8 mi'4( re'4.) re'8-! dod'-! re'-! |
\grace fa'8 mi'4( re'4) \clef "bass" do'!2\p( |
sib fa!) |
%%
sib,2:8\p sib,: |
sib,: sib,: |
fa, r4 fa, |
sib, r r2 |
R1*3 |
re2:8\p mib: |
fa: fa,: |
sib,\f r |
sib r |
sib,:\p sib,: |
sib: sib: |
la: la: |
sol: sol8 sol fa fa |
mi2: mi8 mi fa fa |
mi4 r r do |
fa r r2 |
re'8 r do' r re' r la r |
sib r la r sib r sol r |
la r sol r la r mi r |
fa2:8\f fa8\p fa la la |
sib2: sib: |
do': do: |
fa: fa: |
fa: fa8-. fa-. sib-. sib-. |
la4-.\cresc sib-. la-. mi-. |
fa8\f fa sol sol la la la, la, |
sib,4 r r2 |
do'2:\ff do': |
do: do: |
fa:\f fa:\p |
fa:\f fa:\p |
mi:\f mi:\p |
re:\ff re: |
do:\f do:\p |
re:\ff re: |
do4 r r2 |
r r4 sol8. la32 sib |
do'4\f sol mi sol |
do2 r |
R1*4 | \allowPageTurn
la2: la: |
sol: sol: |
fad2.: sol8 sol |
mib2: re: |
do: dod: |
re:\f do!: |
sib,: sol,: |
do':\p sib: |
fad: sol: |
do': sib8 sib la la |
sol,8\f sib, sib, sib, sib,2:\p |
do: do: |
re: re,: |
sol,4 r r2 |
sib,:\fp sib,: |
do: do: |
re: re,: |
sol,2\ff r4 sol |
lab-! fa-! re-! sib,-! |
mib2 r4 mib\p |
fa4-! re-! sib,-! re-! |
mib2\f r4 sol-!\ff |
lab-! fa-! re-! sib,-! |
mib2:\p re: |
do: do: |
sib,8-!\f re-! fa-! re-! sib,4-! r |
R1*3 | \allowPageTurn
re2:\p mib: |
fa: fa,: |
sib,2\f r |
sib,2 r |
sib,2:8\p sib,8 sib, do do |
reb2: reb: |
do: do: |
do: do': |
dob': dob': |
sib2: sib8 sib, do re |
mib2: mib: |
mib: mib: |
mib:\p mib: |
mib: mi: |
fa8\f do la, do fa,4 r |
R1*4 |
sib8-!\p fa-! re-! fa-! sib,4 r |
sib8-! fa-! re-! fa-! sib4 r |
re'2:\p re: |
mib: mib: |
fa:\p fa: |
fa,: fa,: |
sib8-.-\sug\f fa-. sib-. fa-. la-. fa-. la-. fa-. |
sib fa sib fa la fa la fa |
sib fa sib fa la fa la fa |
sib fa sib fa la fa la fa |
sib2:\fp sib: |
sib:\fp sib,: |
mib:\f mib: |
fa:\f fa,: |
re:\p mib: |
fa: fa,: |
re:16\ff mib: |
fa: fa: |
fa,: fa,: |
sib,2 r8 sib-. la-. sib-. |
\grace re'8 do'4( sib4.) sib8-. la-. sib-. |
\grace re'8 do'4( sib8.) re16 mib2:8 |
re4 r do r |
sib,8 sib, re re mib2:8 |
fa: fa: |
fa,: fa,: |
sib,4. re16 mib fa8-. re-. fa-. re-. |
sib,2 r |
