\piecePartSpecs
#`((violino1 #:indent 0 #:music ,#{ s1*8\break #})
   (violino2 #:indent 0 #:music ,#{ s1*8\break #})
   (viola #:indent 0 #:music ,#{ s1*8\break #})
   (bassi #:indent 0 #:music ,#{ s1*8\break #})
   
   (oboi #:score-template "score-deux-voix")
   (corni #:score-template "score-deux-voix"
          #:tag-global ()
          #:instrument "Corni in B"
          #:music , #{ \noHaraKiri s1*8 \revertNoHaraKiri #})
   (fagotti #:system-count 20
            #:music , #{
\quoteBassi "BEBbassi"
s1*8
<>^\markup\tiny "B." \mmRestInvisible
\new CueVoice { sib,2:8\p sib,: | sib,: sib,: | }
\cue "BEBbassi" { s1*2 \mmRestVisible }
s1*7
<>^\markup\tiny "B." \mmRestInvisible
\new CueVoice { sib,2:8\p sib,: | sib: sib: | la: la: | sol: }
\cue "BEBbassi" { s2 }
\new CueVoice { mi2:8 }
\cue "BEBbassi" { s2 s1*5 \mmRestVisible }
s2
<>^\markup\tiny "B." \mmRestInvisible
\cue "BEBbassi" { \restInvisible s2 \restVisible }
\new CueVoice { sib2:8 sib: | do': do: | fa: fa: | fa: }
\cue "BEBbassi" { s2 s1 \mmRestVisible }
s1*18
<>^\markup\tiny "B." \mmRestInvisible
\new CueVoice { la2:8 la: | sol: sol: | }
\cue "BEBbassi" { s1 }
\new CueVoice {
  mib2:8 re: | do: dod: | re: do!: | sib,: sol,: |
  do':\p sib: | fad: sol: | do':
}
\cue "BEBbassi" { s2 }
s2 \restInvisible
\new CueVoice { \voiceTwo sib,:8\p | \restVisible
  \oneVoice do2: do: |
  re: re,: |
}
\cue "BEBbassi" { s1 } \allowPageTurn
s2 \restInvisible
\new CueVoice { \voiceTwo sib,2:8 | \restVisible
  \oneVoice do:8 do: | re: re,: | } \mmRestVisible
s1*16
<>^\markup\tiny "B." \mmRestInvisible
\new CueVoice { sib,2:8 }
\cue "BEBbassi" { s2 }
\new CueVoice {
  reb2:8 reb: |
  do: do: |
  do: do': |
  dob': dob': |
  sib:
}
\cue "BEBbassi" { s2 }
\new CueVoice {
  mib2:8 mib: |
  mib: mib: |
  mib:\p mib: |
  mib: mi: |
}
s1*7
<>^\markup\tiny "B." \mmRestInvisible
\new CueVoice {
  sib2:8\p re: |
  mib: mib: |
}
s1*6
\new CueVoice {
  \voiceFour sib2:8 \oneVoice sib: |
  sib:\fp sib,: |
  mib:\f mib: |
}
s1
\new CueVoice {
  \voiceOne re2:8 mib: \oneVoice |
  fa: fa,: |
  
}

                        #})

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \force-line-width-ratio#0.3 \column {
    \line { Vedo l'abisso orrendo, }
    \line { Onde ritrassi il piede }
    \line { Sento d'onor, di fede }
    \line { Mille rimorsi al cor. }
  }
  \force-line-width-ratio#0.3 \column {
    \line { Tutto mi fa spavento }
    \line { Dovunque volgo il ciglio; }
    \line { Ma in faccia al mio periglio }
    \line { La fiamma ancora io sento }
    \line { D'un male estinto ardor. }
  }
  \force-line-width-ratio#0.3 \column {
    \line { D'un nero mar cruccioso }
    \line { Tutte le insidie ho scorto. }
    \line { Grazia è del Ciel pietoso }
    \line { S'io non rimasi ascoso, }
    \line { Ma pur vicino al porto }
    \line { Forse mi perdo ancor. }
  }
} #}))
