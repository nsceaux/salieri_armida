\clef "alto" re'2~ re'8 re'-! dod'-! re'-! |
mi'4( re'4.) re'8-! dod'-! re'-! |
\once\slurDashed mi'4( re') la'2:8 |
si': la'8 la' la la |
re2 r8 re'-!_\markup { \dynamic mf \italic appassionato } dod'-! re'-! |
\grace fa'!8 mi'4( re'4.) re'8-! dod'-! re'-! |
\grace fa'8 \once\slurDashed mi'4( re') fa'2-\sug\p~ |
\once\slurDashed fa'( mib') |
%%
sib2:8-\sug\p sib: |
sib4 sol'2 fa'8 sol' |
fa'2: fa': |
fa'4 r r8 re' re' re' |
mib'\rf mib' re' re' re' re' fa' fa' |
mib'\rf mib' re' re' re'2:8 |
mib': mib'8-. sol'( fa' mib') |
re'2:-\sug\p mib': |
fa': fa: |
sib4-\sug\f r r2 |
sib2 r |
fa'2:8-\sug\p fa'8 fa' fa' mib' |
re' sib sib sib sib2: |
la2: la: |
sol: sol8 sol fa fa |
mi2: mi8 mi fa fa |
mi mi do do re re mi mi |
fa4 r r2 |
re'8 r do' r re' r la r |
sib r la r sib r sol r |
la r sol r la r mi r |
<< fa2.:8 { s2\f s4\p } >> la8 la |
sib8 fa' re' sib sib4 r |
do'2:8 do: |
fa: fa: |
fa: fa8 fa sib sib |
la4\cresc sib la mi |
fa8\f fa sol sol la2:8 |
sib4 r r2 |
do'2:8\ff do': |
do: do: |
fa2:16\f fa:\p |
fa2:\f fa:\p |
mi:\f mi:\p |
re:\ff re: |
do:\f do:\p |
re:\ff re: |
do4 r r2 | \allowPageTurn
r2 r4 sol8. la32 si |
do'4-! sol-! mi-! sol-! |
do2 r4 r8 <<
  { \slurDashed do'8 |
    do'4 re'8( dod') re'( do' sib la) |
    la2( sib4) \oneVoice r8 \voiceOne sib |
    sib4 do'8( si) do'( sib la sol) |
    sol2 la4 } \\
  { la8\p |
    la4 sib8 la sib la sol fad |
    fad2 sol4 s8 sol |
    sol4 la8 sol la sol fa mi |
    mi2 fa4 }
>> r4 |
la'2:8 la': |
sol': sol': |
fad'2.: sol'8 sol' |
mib'2: re': |
do': dod': |
re':\f do'!: |
sib: sol: |
do':\p sib: |
la': sol': |
do'': sib'8 sib' la' la' |
sol-\sug\f sib sib sib sib2:\p |
do': do': |
re': re: |
sol4 r r2 |
sib2:-\sug\fp sib: |
do': do': |
re': re: |
sol16\ff sol' sol' sol' sol'2.:16 |
lab'2: lab': |
sol':\p sol': |
lab': lab': |
sol':\f sol': |
lab':\ff lab': |
sol'8\p mib' mib' mib' re'2:8 |
do': do': |
sib8-\sug\f re' fa' re' sib4 r8 re' |
mib'4(\rf re'8) re' re' re' fa' fa' |
mib'\rf mib' re' re' re'2:8 |
mib': mib8 sol'( fa'\rf mib') |
re'2:\p mib': |
fa': fa: |
sib2-\sug\f r |
sib2 r |
sib2:8-\sug\p sib8 sib do' do' |
reb' reb' reb' reb' reb' reb' sol' sol' |
sol'2: sol': |
do': do': |
dob': dob': |
sib2: sib8 sib( do' re') |
mib'2:8 mib': |
mib'4. mib'8 mib'2: |
mib': mib': |
mib': \ficta mi': |
fa'8-\sug\ff do' la do' fa4 r8 <<
  { \slurDashed fa'8 |
    fa'4 sol'8( fad') sol'8( fa' mib' re') |
    re'2( mib'4) \oneVoice r8 \voiceOne mib'8 |
    mib'4 fa'8( mi') fa'( mib' re' do') |
    do'2 re'4 } \\
  { re'8\pp |
    re'4 mib'8 re' mib' re' do' si |
    si2 do'4 s8 do' |
    do'4 re'8 dod' re' do' sib la |
    la2 sib4 }
>> r4 |
sib8-!-\sug\p fa-! re-! fa-! sib4-! r |
sib8-! fa-! re-! fa-! sib4 r |
sib2:8\p re': |
mib': mib': |
fa':-\sug\p fa': fa: fa: |
sib16\f fa' fa' fa' fa'2.:16 |
fa'2: fa': |
fa': fa': |
fa': fa': |
fa'2:8-\sug\fp fa'8 mib' re' mib' |
fa'4-\sug\fp sib2.:8 |
mib'2:-\sug\f mib': |
fa':-\sug\f fa: |
re':-\sug\p mib': |
fa': fa: |
re'2:16\ff mib': |
fa': fa': |
fa: fa: |
sib16 fa' fa' fa' fa'2.:16 |
fa'2: fa': |
fa'4: fa'8 re' mib'2:8 |
re'4 r do' r |
sib8 sib re' re' mib'2: |
fa'2: fa': |
fa':16 fa: |
sib4. re'16 mib' fa'8 re' fa' re' |
sib2 r |
