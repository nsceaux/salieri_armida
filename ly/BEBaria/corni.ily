\clef "treble" \transposition sib
R1*8 |
%%
R1*10 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { do''8. do''16 mi''8. mi''16 re''8. re''16 | do''4 }
  { mi'8. mi'16 do''8. do''16 sol'8. sol'16 | mi'4 }
>> r4 r2 |
R1*9 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol'4. }
  { sol' }
>> r8 r2 |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { re''1 | re''4 }
  { re''1 | sol'4 }
>> r4 r2 |
R1*4 |
\tag#'tutti <>^"a 2." re''1\ff~ |
re''1~ |
re''2\f r8 re''16\p re'' re''8 re'' |
re''2\f r8 re''16\p re'' re''8 re'' |
re''2\f r |
\twoVoices #'(primo secondo tutti) <<
  { mi''1 | }
  { sol'1 | }
  { s1\ff }
>>
\tag#'tutti <>^"a 2." re''2\f r8 re''16-\sug\p re'' re''8 re'' |
\twoVoices #'(primo secondo tutti) <<
  { mi''1 | re''4 }
  { sol'1 | re''4 }
  { s1\ff }
>> r4 r2 |
r2 r4 \tag#'tutti <>^"a 2." la' |
re'' r r2 | \allowPageTurn
R1*15 |
\tag#'tutti <>^"a 2." do''2\f r |
R1*3 |
do''2-\sug\fp r |
R1*3 |
\twoVoices #'(primo secondo tutti) <<
  { do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''~ |
    do''2 }
  { do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'~ |
    do'2 }
  { s1-\sug\ff | s\p | s | s\f | s\ff | s2-\sug\p }
>> \tag#'tutti <>^"a 2." mi''2 |
re''2. re''4 |
do''8\f mi' sol' mi' do'4 r |
R1*6 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { do''8. do''16 mi''8. mi''16 re''8. re''16 | do''4 }
  { mi'8. mi'16 do''8. do''16 sol'8. sol'16 | mi'4 }
>> r4 r2 |
R1 |
r4 <>\p \twoVoices #'(primo secondo tutti) <<
  { re''4 re'' re'' | }
  { do'' do'' do'' | }
>>
R1*2 | \allowPageTurn
r4 \twoVoices #'(primo secondo tutti) <<
  { do''4 do'' do'' | do'' }
  { do'' do'' do'' | do'' }
>> r4 r2 |
R1*3 |
r4 <>\ff \twoVoices #'(primo secondo tutti) <<
  { re''4 re'' }
  { sol' sol' }
>> r4 |
R1*4 |
\tag#'tutti <>^"a 2." do''8-!-\sug\p sol'-! mi'-! sol'-! do'4 r |
do''8-! sol'-! mi'-! sol'-! do'4 r |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''1 |
    re'' |
    do''4 s re''2 |
    mi'' re'' |
    mi'' re'' |
    mi'' re'' |
    mi''4 }
  { do''1 sol' |
    mi'4 s sol'2 |
    do'' sol' |
    do'' sol' |
    do'' sol' |
    do''4 }
  { s1\p s | s4\f r s2 | }
>> r4 r2 |
R1*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' | do''4 }
  { do''2 sol' | mi'4 }
>> r4 r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { do''2 do'' |
    mi''1 |
    re'' |
    do''2 s8 mi''-. re''-. mi''-. |
    \grace sol''8 fa''4( mi''4.) mi''8-. re''-. mi''-. |
    \grace sol''8 fa''4( mi''8) do'' do''2:8 |
    do''4 s re'' s |
    mi'' do'' do'' do'' |
    mi''1 |
    re''1 |
    do''4 }
  { do''2 do'' |
    do''1 |
    sol' |
    mi'2 s8 do''-. sol'-. do''-. |
    re''4( do''4.) do''8-. sol'-. do''-. |
    re''4( do''8) do'' do''2:8 |
    do''4 s sol' s |
    do'' do'' do'' do'' |
    do''1 |
    sol' |
    mi'4 }
  { s1\ff | s1*2 | s2 r8 s4. | s1*2 | s4 r s r | }
>> r4 sol'8 mi' sol' mi' |
do'2 r |
