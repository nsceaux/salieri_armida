\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } << \global \keepWithTag #'tutti \includeNotes "oboi" >>
      \new Staff \with { \corniBInstr  } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Ri.
      } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion { s1*8\break }
        \origLayout {
          s1*2\pageBreak
          \grace s8 s1*4\pageBreak
          \grace s8 s1*2\pageBreak
          s1*4\pageBreak
          % 5
          \grace s8 s1*5\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          % 10
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          % 15
          \grace s8 s1*5\pageBreak
          s1*5\pageBreak
          \grace s8 s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          % 20
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          % 25
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          \grace s8 s1*4\pageBreak
          % 30
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
