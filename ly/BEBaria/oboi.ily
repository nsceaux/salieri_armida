\clef "treble"
\twoVoices #'(primo secondo tutti) <<
  { re'2~ re'8 fad''-! mi''-! fad''-! |
    \grace la''8 sol''4( fad''4.) fad''8-! mi''-! fad''-! |
    \grace la''8 sol''4( fad'') re''2 | }
  { re'2~ re'8 la'-! la'-! la'-! |
    la'4 la'4. la'8-! la'-! la'-! |
    la'4 la' la'2 | }
>>
si'16 re'' sol'' si'' la'' sol'' fad'' mi'' re'' dod'' mi'' re'' fad'' mi'' sol'' mi'' |
re''2 r |
R1*3 |
%%
R1*9 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { sib''8. sib''16 fa''8. fa''16 mib''8. mib''16 |
    re''8. re''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'4 }
  { fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'8. sib'16 re''8. re''16 sib'8. sib'16 fa'8. fa'16 |
    re'4 }
>> r4 r2 |
R1*9 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { la''4. }
  { do'' }
>> r8 r2 |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { la''2 sol'' | fa''4 }
  { fa''2 mi'' | fa''4 }
>> r4 r2 |
R1*4 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { la''1 | sol'' | fa''4 }
  { fa''1~ | fa''2 mi'' | fa''4 }
  { s1\ff | s | s4\f }
>> r4 r8 \twoVoices #'(primo secondo tutti) <<
  { do''16 do'' do''8 do'' | do''2 }
  { la'16 la' la'8 la' | la'2 }
  { s4.\p | s2\f }
>> r8 \twoVoices #'(primo secondo tutti) <<
  { do''16 do'' do''8 do'' | do''2 }
  { la'16 la' la'8 la' | sol'2 }
  { s4.\p | s2\f }
>> r8 \twoVoices #'(primo secondo tutti) <<
  { do''16 do'' do''8 do'' | si'1 | do''2 }
  { sol'16 sol' sol'8 sol' | fa'1 | mi'2 }
  { s4.\p | s1\ff | s2\f }
>> r8 \twoVoices #'(primo secondo tutti) <<
  { do''16 do'' do''8 do'' |
    si'2. sol''8. la''32 si'' |
    do'''4 sol'' mi'' }
  { mi'16 mi' mi'8 mi' |
    fa'1 |
    mi'4 r r }
  { s4.\p | s1\ff }
>> \tag#'tutti <>^"a 2." do''8. re''32 mi'' |
fa''4-! re''-! si'-! sol'8. la'32 si' |
do''4-! sol'-! mi'-! sol'-! |
do'2 r |
R1*14 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { re'' }
>> r2 |
R1*3 |
<>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { re'' }
>> r2 |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { sib''1 |
    lab'' |
    sol'' |
    lab'' sol''2. sib''4 |
    lab''1 |
    sol'' |
    sol''2. la''4 |
    sib''4 sib' }
  { sol''1 |
    fa'' |
    mib'' |
    fa''1 |
    mib'' |
    fa'' |
    mib'' |
    mib''2. mib''4 |
    re''4 sib' }
  { s1-\sug\ff | s | s\p |
    s | s\f | s\ff | s\p | s | s2-\sug\f }
>> r2 |
R1*5 |
r4 <>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''8. sib''16 fa''8. fa''16 mib''8. mib''16 |
    re''8. re''16 fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'4 }
  { fa''8. fa''16 re''8. re''16 do''8. do''16 |
    sib'8. sib'16 re''8. re''16 sib'8. sib'16 la'8. la'16 |
    sib'4 }
>> r4 r2 |
R1 |
r4 <>\p \twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' mi'' | }
  { sol'4 sol' sol' | }
>>
R1*2 | \allowPageTurn
r4 \twoVoices #'(primo secondo tutti) <<
  { re''4 re'' lab'' | sol'' }
  { fa'4 fa' sib' | sib'4 }
>> r4 r2 |
R1*3 |
r4 <>\ff \twoVoices #'(primo secondo tutti) <<
  { la'4 la' }
  { do'4 do' }
>> r4 |
R1*4 | \allowPageTurn
\tag#'tutti <>^"a 2." sib'8-!-\sug\p fa'-! re'-! fa'-! sib'4 r |
sib'8-! fa'-! re'-! fa'-! sib'4 r |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { sib''1 |
    la'' |
    sib''4 s fa''2~ |
    fa''1~ |
    fa''~ |
    fa'' |
    fa''4 }
  { re''1 |
    do'' |
    re''4 s fa'2~ |
    fa'1~ |
    fa'~ |
    fa' |
    fa'4 }
  { s1\p | s | s4\f r s2 | }
>> r4 r2 |
R1*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { re''2 do'' | sib'4 }
  { sib'2 la' | sib'4 }
>> r4 r2 |
R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { fa''2 sol'' | re'''1 | do''' | sib''4 }
  { sib'2 sib' | sib''1 | la'' | sib''4 }
>> r4 r8 \tag#'tutti <>^"a 2." re''8-. do''-. re''8-. |
\grace fa''8 mib''4( re''4.) re''8-. do''-. re''-. |
\grace fa''8 mib''4( re''8) fa'' sol''16 fa'' mib'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' la' sib' do'' sib' la' sol' fa' mib' |
re'4 sib'' \grace la''8 sol''4 fa''8 mib'' |
\twoVoices #'(primo secondo tutti) <<
  { re''16 mib'' fa'' sol'' fa''8 re'' \grace sol''16 fa''8 mib''16 re'' \grace sol''16 fa''8 mib''16 re'' | }
  { sib'16 do'' re'' mib'' re''8 sib' \grace mib''16 re''8 do''16 sib' \grace mib''16 re''8 do''16 sib' | }
>>
\twoVoices #'(primo secondo tutti) <<
  { do''1 | }
  { la' }
>>
sib'4. re'16 mib' fa'8 re' fa' re' |
sib'2 r |
