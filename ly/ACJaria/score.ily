\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr  } <<
        \new Staff << \global \keepWithTag #'tutti \includeNotes "oboi" >>
        %\new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new Staff \with { \corniDInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ubaldo
      shortInstrumentName = \markup\character Ub.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new StaffGroup <<
      \new Staff \with { \vlcInstr } <<
        \global \keepWithTag #'primo \includeNotes "bassi"
      >>
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'secondo \includeNotes "bassi"
        \modVersion { s1*82\break }
        \origLayout {
          s1*3\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          %% 5
          \grace s2 s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 10
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          %% 15
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 20
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 25
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 30
          s1*5\pageBreak
          s1*4\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
