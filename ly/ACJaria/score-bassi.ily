\score {
  \new GrandStaff \with { instrumentName = $(*instrument-name*) } <<
    \new Staff \with { instrumentName = "Violoncelli" } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'primo \includeNotes #(*note-filename*)
      \clef #(*clef*)
      $(or (*score-extra-music*) (make-music 'Music))
    >>
    \new Staff \with { instrumentName = "Basso" \haraKiriFirst } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #'secondo \includeNotes #(*note-filename*)
      \clef #(*clef*)
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = \largeindent
    ragged-last = #(*score-ragged*)
  }
}
