\clef "alto" \override AccidentalSuggestion.avoid-slur = #'outside
re'8-.\p mi'-. fad'-. re'-. dod'-. la-. si-. dod'-. |
re'-. la-. fad-. la-. re-. fad-. mi-. re-. |
la4 r la r |
fad8-. sol-. la-. fad-. mi-. dod-. re-. mi-. |
fad-. sol-. la-. fad-. mi-. dod-. re-. mi-. |
fad-. sol-. la-. fad-. si-. dod'-. re'-. si-. |
la4 r8 la la4 la |
\sug la2 r |
R1*4 |
\ru#4 { fad8-. re-. } |
\ru#8 { sol-. re-. } |
fad-. re-. fad-. re-. fad-. re-. la( sol) |
fad-. mi-. re-. fad-. mi-. dod-. re-. mi-. |
re4 r r2 |
<fad re'>2.\ff re'4\p |
dod' r si r |
la4 la8. la16 la4 r |
la'1 |
sold'4 mi' red'2 |
mi'8\ff <mi' sold>16 q q4:16 q2:16 |
<sold? fa'>2:16\p <mi' la>:16 |
<fad! red'>4-!-\sug\f <sold mi'>-! q2:16 |
<sold? fa'>2:16\p <mi' la>:16 |
<fad! red'>4-!\f <sold mi'>-! mi'8 re'!( dod' re') |
mi'1\p |
fad' |
la'4 dod'' si'2 |
la'4 r r2 |
mi'1 |
fad' |
la'4 dod'' si'2 |
la'8 <mi' dod'>16\f q q4:16 <si mi'>2:16 |
<dod' mi'>2:16 <<
  { mi'2:16 | mi':16 } \\
  { re'2:16 | dod':16 }
>> <si mi'>2:16-\sug\fp |
<do' mi'>:16 <si mi'>:16-\sug\fp |
<do' mi'>:16\f mi':16 |
fa':16 si':16 |
si'4 r4 r2\fermata |
si'4(-\sug\p la' sold' fad') |
mid'4-\sug\rf r \ficta sold-\sug\rf fad |
mid2\rf r |
r4 fad'(-\sug\p mid') r |
r fad'( mid') r |
r fad'( mid' mid') |
fad' fad' fad fad' |
r4 sold sold sold |
la2. re'4 |
mi' r mi r |
la2. \once\slurDashed fad'8( re') |
mi'4 r mi r |
la r sib2\f |
la4-\sug\p fa2-\sug\f sold4-\sug\p |
la8-\sug\p r r4 sib2-\sug\f |
la r |
re'8-.\p mi'-. fad'-. re'-. dod'-. la-. si-. dod'-. |
re'-. la-. fad-. la-. re-. mi-. fad-. re-. |
la4 r la r |
fad8-. fad-. la-. fad-. fad4 r |
mi'8-. fad'-. sol'-. mi'-. red'-. si-. dod'-. red'-. |
mi'-. si-. sol-. si-. mi-. fad-. sol-. mi-. |
si4 r si r |
mi8 sol si mi' mi4 si |
mi'8(\f re') dod'-. re'-. \once\slurDashed dod'( si) lad-. sold-. |
fad8-\sug\ff <lad fad'>16 q q4:16 q2:16 |
<lad? sol'!>2:16-\sug\p <si fad'>:16 |
mid'4\f <lad fad'> q2:16 |
<lad? sol'!>:16\p <si fad'>:16 |
<sold mid'>4\f <lad fad'> fad'8 \ficta mi'( re' mi') |
fad'1\p |
sol'! |
fad'~ |
fad'2 r |
fad'1 |
sol' |
fad' |
re'4 mi'2\f mi'4\p |
fad'2 fad |
si r | \allowPageTurn
%%
fad8.-\sug\p fad16 fad8. fad16 mi8. mi16 mi8. mi16 |
re4 r r2 |
<la fad' re''>4\f r <la mi' dod''> r |
fad8.-\sug\p fad16 fad8. fad16 mi8. mi16 mi8. mi16 |
re4 r r2 |
<la fad' re''>4-\sug\f r <la mi' dod''> r |
<la fad' re''> re' fad'16-\sug\p <fad' la'> q q q4:16 |
q2:16 q:16 |
q:16\f q:16 |
q:16\fp q:16\fp |
mi':16\fp mi':16\fp |
mi':16\fp mi':16\fp |
re'8-\sug\f re-. re-. re-. re mi16 fad sol la si dod' |
re'8-. re'-.\p dod'-. dod'-. si-. si-. la-. la-. |
sol-. sol-. fad-. fad-. sol-. sol-. la-. la-. |
si-. si-. la-. la-. si-. si-. dod'-. dod'-. |
re'2:16\f si:16 |
sol:16 la:16 |
re'8-. la-.\p re'-. la-. dod'-. la-. dod'-. la-. |
re' la re' la mi' la mi' la |
re' la re' la dod' la dod' la |
re' la re' la mi' la mi' la |
re' la re' la mi' la mi' la |
re' la re' la dod' la dod' la |
re'2:8-\sug\p re':8 |
re'2:8 re':8 |
dod'16-\sug\f la la la la4:16 la2:16 |
sib4:16 la:16 re':16 dod':16 |
fa'4:16 mi'16 la la la la2:16 |
sib4:16 la:16 re':16 dod':16 |
fa'4:16 mi' sold2-\sug\f |
la4-\sug\p r sold2-\sug\fp |
<< la2:8 { s8 s-\sug\f } >> la8 la la si16 dod' |
re'8-. re'-.\p dod'-. dod'-. si-. si-. la-. la-. |
sol-. sol-. fad-. fad-. sol-. sol-. la-. la-. |
si-. si-. la-. la-. si-. si-. dod'-. dod'-. |
re'2:8 re':8 |
sol:8 la:8 |
re':16\ff si':16 |
si':16 la':16 |
fad'8-. re'-. fad'-. re'-. mi'-. dod'-. mi'-. dod'-. |
fad'-. re'-. fad'-. re'-. mi'-. dod'-. mi'-. dod'-. |
fad'-. re'-. fad'-. re'-. mi'-. dod'-. mi'-. dod'-. |
re'4 r8 fad sol la si dod' |
re' mi' fad' sol' la' si' dod'' re'' |
la4 la' la la' |
re'4 r <re' fad>4 q |
q2 r |
