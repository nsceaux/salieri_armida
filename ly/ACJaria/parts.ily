\piecePartSpecs
#`((oboi #:score-template "score-deux-voix" #:music ,#{ s1*82\break #})
   (corni #:tag-global () #:instrument "Corni in D"
          #:score-template "score-voix" #:music ,#{ s1*82\break #})
   (fagotti #:tag-notes (tutti part)
            #:music , #{
\quoteBassoI "ACJvlc"
\quoteBassoII "ACJbasso"
<>_\markup\tiny "Vlc."
\cue "ACJvlc" { \mmRestInvisible s1*18 \mmRestVisible }
s2.
<>^\markup\tiny "Vlc."
\cue "ACJvlc" { \restInvisible\mmRestInvisible s4 s1*4 \mmRestVisible\restVisible }
s1*5
<>_\markup\tiny "Vlc."
\cue "ACJvlc" {\mmRestInvisible s1*7 \restInvisible s4 \mmRestVisible\restVisible s4 }
s2 s1*22
<>_\markup\tiny "Vlc."
\cue "ACJvlc" { \mmRestInvisible s1*9 \mmRestVisible }
s1*4 s2
<>_\markup\tiny "Vlc."
\cue "ACJvlc" { \restInvisible\mmRestInvisible s2 s1*3 \mmRestVisible\restVisible }
s1
<>^\markup\tiny "Vlc." \new CueVoice { \stemUp re'1 }  \restInvisible\mmRestInvisible
\cue "ACJvlc" { s1*3 \restVisible s1*2 \mmRestVisible }
s1*13
<>_\markup\tiny "Vlc."
\cue "ACJvlc" { \restInvisible\mmRestInvisible s1*3 \restVisible\mmRestVisible }
s1*2 s2
<>_\markup\tiny "B."
\cue "ACJbasso" { \restInvisible s4 \restVisible s4 \restInvisible s4 \restVisible s4 }
s2 s1 s2
<>_\markup\tiny "B."
\cue "ACJbasso" { \restInvisible s4 \restVisible s4 \restInvisible s4 \restVisible s4 }
s2 s1
<>_\markup\tiny "B."
\restInvisible\mmRestInvisible \new CueVoice { \once\stemDown re'2:8 re':8 | si:8 si:8 }
\restVisible\mmRestVisible
s1*7
<>^\markup\tiny "Vlc."
\restInvisible\mmRestInvisible
\new CueVoice { \stemUp re'8 re' dod' dod' }
\cue "ACJvlc" { s2 s1*2 }
\new CueVoice {
  re'2:8 si:8 sol:8 la:8
} \restVisible\mmRestVisible
    #})

   (violino1)
   (violino2)
   (viola)
   (bassi #:score "score-bassi")

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { Finta larva, d'Abisso frall' ombra, }
  \livretVerse#10 { Il piacere gli scherza d'intorno. }
  \livretVerse#10 { Ah se il sonno di morte l'ingombra, }
  \livretVerse#10 { Se i suoi lumi si chiudono al giorno, }
  \livretVerse#10 { Nell' orrore del carcere indegno }
  \livretVerse#10 { Più che a sdegno ti muova a pietà. }
  \livretVerse#12 { Sciogli, sgombra, la notte funesta, }
  \livretVerse#10 { Dio possente, lo scuoti, lo desta. }
  \livretVerse#10 { Chi può trarlo dall' ombra di morte; }
  \livretVerse#10 { Se i tuoi raggi per scorta non hà? }
  \livretVerse#12 { Il tuo spirto m'infiamma, m'accende }
  \livretVerse#10 { Dio possente lo sento, lo scerno. }
  \livretVerse#10 { Ah le frodi, e le forze d'Averno }
  \livretVerse#10 { Van contrasto saranno al tuo vanto, }
  \livretVerse#10 { E l'incanto di vana beltà. }
} #}))

