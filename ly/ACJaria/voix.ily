\clef "bass/G_8" R1*8 |
re'2 la |
fad4 la r r8 re |
la,2 la4. sol8 |
fad4 re r2 |
la2. re'4 |
si si r sol8. la16 |
si4 si8 dod' re'4 si8 sol |
la4 re8 la re'4 dod'8 si |
\grace la2 sol1 |
fad4 r r2 |
re'2. re4 |
dod4 dod8 dod si,4 si,8 si, |
la,4 la, r la8 si |
do'4 do'8 do' do'4 si8 la |
sold4 si8 si do'4 si8 la |
sold4 mi r2 |
fa2 mi |
red4 mi r mi |
fa2 mi4. mi8 |
red4 mi r2 |
la2. mi'8([ dod']) |
lad4 si2 dod'8([ re']) |
\once\slurDashed fad4.( la8) sold4. si8 |
\grace si8 dod'2 r |
r4 la2 mi'8([ dod']) |
lad4 si2 dod'8[ re'] |
fad4.( la8) sold4. si8 |
la4 r si mi |
dod'4 mi8 dod' re'[ si] sold re |
dod4 mi mi'4. si8 |
do'4 la8 la mi'4 si8 si |
do'4 la r r8 do' |
\ficta do'4 la r r8 red' |
mi'4 mi r si8 dod'! |
re'4 re'8 re' re'4 re'8 re' |
re'[ dod'] si4 r2 |
R1 |
dod'2. re'8([ dod']) |
dod'2. re'8 dod' |
dod'2. si8 dod' |
\grace si8 la2 r |
si2. dod'8[ re'] |
\grace re'8 dod'4( si8[ la sold]) fad re'8[ si] |
la2 si4. re'8 |
\grace re'8 dod'4(\melisma si8[ la]) \ficta sold[ fad]\melismaEnd re'[ si] |
la2 si4. la8 |
la2 r4 mi'8 re' |
dod'4 la8 la fa4 mi8 re |
dod!4 mi r2 |
dod'2 r |
re' la |
fad4 la r r8 re |
la,2 la4. sol8 |
fad4 re r2 |
mi'2 si |
sol4 si r si8 si |
si4 si8 si si4 si8 si |
sol4 mi r si8 si |
mi'[ re'] dod' re' dod'[ si] lad sold |
fad4 fad r2 |
sol!2 fad |
mid4 fad r fad |
sol2 fad4 fad |
mid fad r2 |
r4 si2 dod'8[ re'] |
\grace re'8 dod'4 dod'2 mi'8[ dod'] |
si2 lad4. dod'8 |
\grace dod'8 re'2 r |
r4 si2 dod'8[ re'] |
\grace re'8 dod'4 dod'2 mi'8[ dod'] |
si2 lad4. dod'8 |
si4( sol2) fad8[ mi] |
fad2 fad4. fad8 |
si,2 r |
%%
\tag #'ubldo {
R1 |
r2 la4 la |
re' la8 la mi'[ dod'] la sol |
fad4 re r2 |
r la4 la |
re' la8 la mi'[ dod'] la sol |
fad4 re r re8 re' |
do'4 do'8 do' do'4 do'8 do' |
si4 si r si8 si |
si4 fad8 sol la4 la8 si |
sol4 sol r sol8 si |
la4 mi8 fad sol4 sol8 la |
fad4 fad r2 |
re'4( dod' si) la |
sol( fad) sol la |
si( la) si-. dod'-. |
re'2 si |
sol la4 la |
re r r2 |
r la4 la |
re' la8 la mi'[ dod'] la sol |
fad4 re r2 |
r la4 la |
re' la8 la mi'[ dod'] la sol |
fad4 re r r8 re' |
re'4 sold r r8 re' |
dod'4 la r la8 la |
sib4 la8 la re'4 dod'8 dod' |
\ficta fa'4-. mi'-. r la8 la |
sib4 la8 la re'4 dod'8 dod' |
fa'4 mi' r2 |
la2 si4. re'8 |
dod'4 la r2 |
re'4( dod') si( la) |
sol( fad) sol la |
si( la) si dod' |
re'2 si |
sol2 la4 la |
re'2 si |
sol la4 la |
re2 r |
R1*7
}
