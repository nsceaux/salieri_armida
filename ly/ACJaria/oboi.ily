\clef "treble" R1*18 |
re'''2.-\sug\ff r4 |
R1*5 |
\twoVoices #'(primo secondo tutti) <<
  { re''2 do'' |
    si'4-! mi'-! s si' |
    re''2 do'' |
    si'4-! mi'-! }
  { sold'2 la' |
    fad'4-! sold'-! s sold' |
    sold'?2 la' |
    fad'4 sold' }
  { s1\p | s2\f r4 s | s1-\sug\p | s2\f }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { dod''1 | si' | fad''2 mi''4. re''8 |
    s8 dod''( si' dod'') re''( mi'' fad'' sold'') |
    la''8. sold''32 fad'' mi''8 re'' dod''2 |
    si'1 |
    fad''2 mi''4. sold'8 |
    la'4 s mi'' s |
    dod'' s sold'' s |
    la'' s mi''4.( si'8) |
    do''4 s mi''4.( si'8) |
    do''1 |
    do''?2 red'' |
    mi''4 }
  { la'1 | la' | re''2 sold'?4. si'8 |
    s8 la'( sold' la') si'( dod'' re'' si') |
    la'1~ |
    la' |
    re''2 sold'4. sold'8 |
    la'4 s si' s |
    la'4 s si' s |
    dod'' s si'4.( sold'?8) |
    la'4 s si'4.( sold'8) |
    la'1 |
    la'1 |
    sold'4 }
  { s1-\sug\p | s1*2 | r8 s4. s2 |
    s1*3 |
    s4 r s\f r |
    s r s r |
    s r s4.\f s8\p |
    s4 r s4.\f s8\p |
    s1\f | }
>> r4 r2\fermata |
\twoVoices #'(primo secondo tutti) <<
  { re''1 | re''8( dod'') si'4 }
  { si'1 | si'8( la') sold'4 }
  { s1\p | s2\rf }
>> r2 | \allowPageTurn
R1*10 |
r4 \tag #'tutti <>^"a 2." fa''2(\f mi''8-\sug\p re'') |
dod''4 r r2 |
r4 fa''2(\f mi''8 re'') |
dod''2 r |
R1*10 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' |
    dod''4 fad' s dod'' |
    mi''2 re'' |
    dod''4 fad' s2 |
    re''1 dod'' |
    re''2 dod'' |
    s8 si''( la'' sol'' fad'' mi'' re'' dod'') |
    si'4 re''2. |
    dod''1 |
    re''2 dod'' |
    si'4 sol'2 fad'8\p mi' |
    re'4 re'' dod''2 |
    si' }
  { lad'2 si' |
    sold'4 lad' s lad' |
    lad'2 si' |
    sold'4 lad' s2 |
    si'1~ |
    si'~ |
    si'2 lad' |
    s8 sol''!( fad'' mi'' re'' dod'' si' la') |
    si'1~ |
    si'~ |
    si'2 lad' |
    si'4 mi'2 r4 |
    si'2 lad' |
    si' }
  { s1\p |
    s2\f r4 s |
    s1-\sug\p |
    s2\f r |
    s1\p |
    s1*2 |
    r8 s4. s2 |
    s1*3 |
    s4 s2.\f |
    s1\p | }
>> r2 |
%%
R1 |
r2 <>\f \twoVoices #'(primo secondo tutti) <<
  { la''4 la'' | fad''2 mi'' | re''4 }
  { la'4 la' | re''2 dod'' | re''4 }
>> r4 r2 |
r2 <>\f \twoVoices #'(primo secondo tutti) <<
  { la''4 la'' | fad''2 mi'' | re''4 }
  { la'4 la' | re''2 dod'' | re''4 }
>> r4 \twoVoices #'(primo secondo tutti) <<
  { re''2 |
    do''1 |
    si'1 |
    si'2 si' |
    si' si' |
    la' la' |
    fad'8 re'8-. re'-. re'-. re'8 mi'16 fad' sol' la' si' dod'' |
    re''8-. fad''-. fad''-. fad''-. fad''8-. fad''-. fad''-. fad''-. |
    sol''8-. sol''-. la''-. la''-. si''-. si''-. la''-. la''-. |
    sol''-. sol''-. fad''-. fad''-. sol''-. sol''-. mi''-. mi''-. |
    fad''1 mi'' |
    re''4 }
  { fad'2 |
    fad'1 |
    fad'1 |
    fad'2 fad' |
    mi' mi' |
    mi'2 mi' |
    re'8-. re'-. re'-. re'-. re'8 mi'16 fad' sol' la' si' dod'' |
    re''8 re''-. re'' -. re'' -. re''8-. re''-. dod''-. dod''-. |
    si'-. si'-. la'-. la'-. sol'-. sol'-. fad'-. fad'-. |
    sol'-. sol'-. la'-. la'-. sol'-. sol'-. la'-. la'-. |
    re''1~ |
    re''2 dod'' |
    re''4 }
  { s2\p | s1 | s\f |
    s2\fp s\fp |
    s2\fp s\fp |
    s\fp s-\sug\fp | s1-\sug\f | s8 s4.\p s2 | s1*2 | s1-\sug\f | }
>> r4 r2 |
r2 \tag #'tutti <>^"a 2." la'16\p si' dod'' re'' mi'' fad'' sol'' la'' |
\twoVoices #'(primo secondo tutti) <<
  { re''8 re''16 re'' re''8 re'' mi'' mi''16 mi'' mi''8 mi'' |
    fad''4 }
  { fad'8 fad'16 fad' fad'8 fad' la' la'16 la' la'8 la' |
    re''4 }
>> r4 r2 |
r2 \tag #'tutti <>^"a 2." la'16\p si' dod'' re'' mi'' fad'' sol'' la'' |
\twoVoices #'(primo secondo tutti) <<
  { re''8 re''16 re'' re''8 re'' mi'' mi''16 mi'' mi''8 mi'' |
    fad''4 }
  { fad'8 fad'16 fad' fad'8 fad' la' la'16 la' la'8 la' |
    re''4 }
>> r4 r2 |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { la''2 }
  { la' }
>> r4 \tag #'tutti <>^"a 2." la'-! |
sib'-! la'-! re''-! dod''-! |
fa''-! mi''-! r la'-! |
sib'-! la'-! re''-! dod''-! |
fa''-! mi''-! \twoVoices #'(primo secondo tutti) <<
  { fa''4. mi''16 re'' |
    dod''4 la' fa''4. mi''16 re'' |
    dod''8-. la'-. la'-. la'-. la'8-. la'-. la' si'16 dod'' |
    re''8-. fad''-. fad''-. fad''-. fad''8-. fad''-. fad''-. fad''-. |
    sol''8-. sol''-. la''-. la''-. si''-. si''-. la''-. la''-. |
    sol''-. sol''-. fad''-. fad''-. sol''-. sol''-. mi''-. mi''-. |
    fad''1 mi'' |
    fad''4 re'''2.~ |
    re'''2 dod''' |
    re'''8-. fad''-. la''-. fad''-. }
  { si'2 |
    la'4 r si'2 |
    la'8-. la'-. la'-. la'-. la'8-. la'-. la' si'16 dod'' |
    re''8 re''-. re'' -. re'' -. re''8-. re''-. dod''-. dod''-. |
    si'-. si'-. la'-. la'-. sol'-. sol'-. fad'-. fad'-. |
    sol'-. sol'-. la'-. la'-. sol'-. sol'-. la'-. la'-. |
    \once\tieDashed re''1~ |
    re''2 dod'' |
    re''4 fad''2. |
    mi''1 |
    re''8-. fad''-. la''-. fad''-. }
  { s2\f | s2\p s2\fp | s8 s2..\f | s8 s4.\p s2 | s1*4 | s1\ff | }
>> la''16( sol'' fad'' mi'') la''( sol'' fad'' mi'') |
\ru#2 { re''8-. fad''-. la''-. fad''-. la''16( sol'' fad'' mi'') la''( sol'' fad'' mi'') | }
re''4 r8 fad' sol' la' si' dod'' |
re'' mi'' fad'' sol'' la'' si'' dod''' re''' |
la'4 la'' la' la' |
re'4 r \twoVoices #'(primo secondo tutti) <<
  { fad''4 fad'' | fad''2 }
  { re''4 re'' | re''2 }
>> r2 |
