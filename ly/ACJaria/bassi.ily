\clef "bass"
<<
  \tag #'primo {
    \clef "tenor" re'8-\sug\p-. mi'-. fad'-. re'-. dod'-. la-. si-. dod'-. |
    re'-. la-. fad-. la-. re fad mi re |
    \clef "bass"
  }
  \tag #'secondo { R1*2 | }
>>
la4\p r4 la, r |
re8-. mi-. fad-. re-. dod-. la,-. si,-. dod-. |
re-. mi-. fad-. re-. dod-. la,-. si,-. dod-. |
re-. mi-. fad-. re-. si,-. dod-. re-. si,-. |
la,4 r8 la, la,4 la, |
la,2 r |
<<
  \tag #'primo {
    \clef "tenor" re'8-. mi'-. fad'-. re'-. dod'-. la-. si-. dod'-. |
    re'-. la-. fad-. la-. re-. fad-. mi-. re-. |
    \clef "bass"
  }
  \tag #'secondo { R1*2 | }
>>
la4 r la, r |
re8-. fad-. la-. fad-. re4 r |
\ru#4 { fad8-. re-. } |
\ru#8 { sol8-. re-. } |
fad-. re-. fad-. re-. fad-. re-. la( sol) |
fad-. mi-. re-. fad-. mi-. dod-. si,-. la,-. |
re4 r r2 |
re2.\ff re4\p |
dod r si, r |
la,4 la,8. la,16 la,4 r |
red1 |
mi2 red |
mi16\ff mi mi mi mi4:16 mi2:16 |
mi2:16\p mi:16 |
mi:16\f mi:16 |
mi2:16\p mi:16 |
<<
  \tag #'primo {
    mi4-\sug\f mi, mi'8 re'( dod' re') |
    dod'1\p |
    re' |
    re'2 mi' |
    la4 r r2 |
    dod'1 re' |
    re'2 mi' |
    la4 r <sold, mi si>-\sug\f r |
    <la, mi dod'> r <sold, mi re'> r |
    <la, mi dod'> r
  }
  \tag #'secondo {
    mi16\f mi mi mi mi,4 r2 |
    dod4\p r dod r |
    re r re r |
    re r mi r |
    la, r r2 |
    dod4 r dod r |
    re r re r |
    re r mi r |
    la r sold\f r |
    la r mi r |
    la r
  }
>> sold4-!\f mi-!\p |
la r sold-!\f mi-!\p |
la,2:16\f la:16 |
fa:16 fa:16 |
mi4 r r2\fermata |
R1 | \allowPageTurn
mid2\rf r |
mid\rf r |
r4 fad(\p mid) r |
r fad( mid) r |
r fad( mid dod) |
fad fad fad, fad |
r sold sold, sold |
la2. re4 |
mi r mi, r |
la,2. \once\slurDashed fad8( re) |
mi4 r mi, r |
la, r sib2\f( |
la4)\p r sib,2(\f |
la,4)\p r sib2(\f |
la) r |
<<
  \tag #'primo {
    \clef "tenor" re'8-.-\sug\p mi'-. fad'-. re'-. dod'-. la-. si-. dod'-. |
    re'-. la-. fad-. la-. re-. mi-. fad-. re-. |
    \clef "bass"
  }
  \tag #'secondo { R1*2 }
>>
la4\p r la,\p r |
re8-. fad-. la-. fad-. re4 r |
<<
  \tag #'primo {
    \clef "tenor" mi'8-. fad'-. sol'-. mi'-. red'-. si-. dod'-. red'-. |
    mi'-. si-. sol-. si-. mi-. fad-. sol-. mi-. |
    \clef "bass"
  }
  \tag #'secondo { R1*2 }
>>
si4 r si, r |
mi,8 sol, si, mi mi,4 si |
mi'8\f re' dod' re' dod' si lad sold |
fad2:16\ff fad:16 |
fad2:16\p fad:16 |
fad2:16\f fad:16 |
fad2:16\p fad:16 |
<<
  \tag #'primo {
    fad4-\sug\f fad, \clef "tenor" fad'8 mi'( re' mi') |
    re'1\p |
    mi' |
    fad' |
    si4 r r2 |
    re'1 |
    mi' |
    fad' |
    \clef "bass"
  }
  \tag #'secondo {
    fad16\f fad fad fad fad,4 r2 |
    re4\p r re r |
    mi r mi r |
    fad r fad, r |
    si, r r2 |
    re4 r re r |
    mi r mi r |
    fad r fad, r |
  }
>>
si,4 mi2.\f |
fad4\p r fad, r |
si,2 r |
%%
re4\p re dod dod |
re r r2 |
<<
  \tag #'primo { <la, fad re'>4-\sug\f r <la, mi dod'> r | }
  \tag #'secondo { re4\f r la, r | }
>>
re4\p re dod dod |
re r r2 |
<<
  \tag #'primo { <la, fad re'>4-\sug\f r <la, mi dod'> r | }
  \tag #'secondo { re4\f r la, r | }
>>
re2:8\p re:8 |
re:8 re:8 |
red:8\f red:8 |
red:8\fp red:8\fp |
mi:8\fp re!:8\fp |
dod:8\fp dod:8\fp |
re8-.\f re-. re-. re-. re mi16 fad sol la si dod' |
re'8-. re'-.\p dod'-. dod'-. si-. si-. la-. la-. |
sol-. sol-. fad-. fad-. sol-. sol-. la-. la-. |
si-. si-. la-. la-. si-. si-. dod'-. dod'-. |
re'2:8\f si:8 |
sol:8 la:8 |
<<
  \tag #'primo {
    re'8-. la-.\p re'-. la-. dod'-. la-. dod'-. la-. |
    re' la re' la mi' la mi' la |
    re' la re' la dod' la dod' la |
    re' la re' la mi' la mi' la |
    re' la re' la dod' la dod' la |
    re'4
  }
  \tag #'secondo {
    re'4 r la\p r |
    re r la r |
    re' r la r |
    re r la r |
    re' r la r |
    re
  }
>> r4 la r |
re'2:8\p re':8 |
si:8 si:8 |
la2\f r4 la-! |
sib-! la-! re'-! dod'-! |
fa'-! mi'-! r la-! |
sib-! la-! re'-! dod'-! |
fa'-! mi'-! sold2\f |
la4\p la, sold2\fp |
la8 la,-.\f la-. la-. la-. la-. la-. si16-. dod'-. |
re'8-. re'-.\p dod'-. dod'-. si-. si-. la-. la-. |
sol-. sol-. fad-. fad-. sol-. sol-. la-. la-. |
si-. si-. la-. la-. si-. si-. dod'-. dod'-. |
re'2:8 si:8 |
sol:8 la:8 |
re':8\ff si:8 |
sol:8 la:8 |
re'8-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'4 r8 fad, sol, la, si, dod |
re mi fad sol la si dod' re' |
la,4 la la, la |
re4 r re re |
re2 r |
