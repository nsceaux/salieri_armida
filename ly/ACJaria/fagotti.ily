\clef "bass" R1*18 |
re2.-\sug\ff r4 |
R1*4 |
mi,1-\sug\ff~ |
mi,-\sug\p~ |
mi,-\sug\f~ |
mi,-\sug\p |
mi4\f mi, r2 |
R1*7 |
r2 sold,4-\sug\f r4 |
la, r mi, r |
la, r sold,-!-\f mi,-!-\p |
la, r sold,-!-\f mi,-!-\p |
la,2:8-\sug\f la:8 |
fa:8 fa:8 |
mi4 r4 r2\fermata |
R1 |
mid4\rf mid \twoVoices #'(primo secondo tutti) <<
  { re'2 | re'8 dod' si4 }
  { si2 | si8 la sold4 }
  { s2 | s2\rf }
>> r2 |
r4 fad(-\sug\p mid) r |
r4 fad( mid) r |
r4 fad( mid dod) |
r4 fad fad, fad |
r sold sold, sold |
la2. re4 |
mi r mi, r |
la,2. fad8 re |
mi4 r mi, r |
la, r sib2\f |
la4\p r4 sib,2\f |
la,4\p r4 sib2\f |
la4 r r2 |
R1*9 |
fad,1-\sug\ff |
fad,-\sug\p~ |
fad,4-\sug\f fad, fad, fad, |
fad,1-\sug\p |
fad,4\f fad, r2 |
R1*3 |
si1~ |
si4 r r2 |
R1*5 | \allowPageTurn
%%
re4\p re dod dod |
re r r2 |
re4-\sug\f r la, r |
re-\sug\p re dod dod |
re r r2 |
re4-\sug\f r la, r |
re2:8-\sug\p re:8 |
re:8 re:8 |
red:8\f red:8 |
red:8\fp red:8\fp |
mi:8\fp re!:8\fp |
dod:8\fp dod:8\fp |
re8\f re re re re mi16 fad sol la si dod' |
\tag #'part \once\voiceOne re'4 r r2 |
R1*2 |
re'2:8\f si:8 |
sol:8 la8 la, la, la, |
re4 r r2 |
r la4\p la |
re'8 la16 la la8 la dod' dod'16 dod' dod'8 dod' |
re'4 r r2 |
r2 la4-\sug\p la |
re'8 la16 la la8 la dod' dod'16 dod' dod'8 dod' |
\tag #'part \once\voiceOne re'4 r r2 |
R1 |
la,2-\sug\f r4 la-! |
sib-! la-! re'-! dod'-! |
fa'-! mi'-! r la-! |
sib-! la-! re'-! dod'-! |
fa'-! mi'-! sold2\f |
la4\p la, sold2\fp |
la8 la,\f la, la, la, la, la, si,16 dod |
re4 r r2 |
R1*4 |
re2:8-\sug\ff si,:8 |
sol,:8 la,:8 |
re'8-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'-. la-. re'-. la-. dod'-. la-. dod'-. la-. |
re'4 r8 fad, sol, la, si, dod |
re mi fad sol la si dod' re' |
la,4 la la, la |
re4 r re re |
re2 r |
