\clef "treble" \transposition re
R1*12 |
\tag #'tutti <>^"a 2." do''1~ |
do''~ |
do''~ |
do''2 r |
R1*2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { sol'2. }
  { do' }
>> r4 |
R1*4 |
\tag #'tutti <>^"a 2." re''1\ff~ |
re''\p~ |
re''\f~ |
re''\p |
re''4-!-\sug\f re''-! r2 |
R1*3 |
\twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''4 }
  { sol'1~ | sol'4 }
>> r4 r2 |
R1*2 |
r2 \tag #'tutti <>^"a 2." re''4-\sug\f r |
re'' r re'' r |
re'' r re''4.\f re''8\p |
re''4 r re''4.\f re''8\p |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''1 | re''4 }
  { sol'1~ | sol' | re''4 }
>> r4 r2\fermata |
R1*25 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { mi''1 | mi''~ | mi''~ | mi'' | mi''4 mi'' }
  { mi'1 | mi'~ | mi'~ | mi' | mi''4 mi'' }
  { s1\ff | s\p | s\f | s\p | s2\f }
>> r2 |
R1*10 |
%%
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { sol''4 sol'' | mi''2 re'' | do''4 }
  { sol'4 sol' | do''2 sol' | mi'4 }
  { s2\f | s1\f | }
>> r4 r2 |
r2 \twoVoices #'(primo secondo tutti) <<
  { sol''4 sol'' | mi''2 re'' | do''4 }
  { sol'4 sol' | do''2 sol' | mi'4 }
  { s2\f | s1\f | }
>> r4 \twoVoices #'(primo secondo tutti) <<
  { mi''2 |
    sol''1 |
    sol'' |
    sol'' |
    fa''4 re''2. |
    sol''2 sol'' |
    mi''8 do'' do'' do'' do''4 }
  { do''2 |
    mi''1 |
    mi''1 |
    mi'' |
    re''4 re''2. |
    re''2 re'' |
    mi''8 do'' do'' do'' do''4 }
  { s2\p |
    s1 |
    s\f |
    s\fp |
    s\fp |
    s2\fp s-\sug\fp |
    s2\f }
>> r4 |
R1*3 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''1 | re'' | do''4 }
  { do''1~ | do''2 sol' | mi'4 }
>> r4 r2 |
r2 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' |
    do''8 do''16 do'' do''8 do'' re'' re''16 re'' re''8 re'' |
    mi''4 }
  { sol'4 sol' |
    mi'8 mi'16 mi' mi'8 mi' sol' sol'16 sol' sol'8 sol' |
    do''4 }
>> r4 r2 |
r2 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' |
    do''8 do''16 do'' do''8 do'' re'' re''16 re'' re''8 re'' |
    mi''4 }
  { sol'4 sol' |
    mi'8 mi'16 mi' mi'8 mi' sol' sol'16 sol' sol'8 sol' |
    do''4 }
>> r4 r2 |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { sol' }
>> r2 |
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { sol' }
>> r4 |
R1 |
r2 \tag #'tutti <>^"a 2." do''2\f |
sol'\p do''-\sug\fp |
sol'8 sol'-\sug\f sol' sol' sol'4 r |
R1*5 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''1 |
    re'' |
    mi''8 do'' mi'' do'' re'' sol' re'' sol' |
    mi'' do'' mi'' do'' re'' sol' re'' sol' |
    mi'' do'' mi'' do'' re'' sol' re'' sol' |
    do''4 }
  { do''1 |
    do''2 si' |
    do''8 sol' do'' sol' re'' sol' re'' sol' |
    do'' sol' do'' sol' re'' sol' re'' sol' |
    do'' sol' do'' sol' re'' sol' re'' sol' |
    do''4 }
>> r4 r2 |
\tag #'tutti <>^"a 2." do''8 re'' mi'' fa'' sol'' la'' si'' do''' |
sol'4 sol'' sol' sol'' |
do''4 r \twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' | mi''2 }
  { do''4 do'' | do''2 }
>> r2 |
