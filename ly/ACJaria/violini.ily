\clef "treble" \override AccidentalSuggestion.avoid-slur = #'outside
<<
  \tag #'primo {
    re''2\p la' |
    fad'4 la' r r8 re' |
    la2 la'4. sol'8 |
    fad'4 r8 la'( sol''4) r8 sol''( |
    fad''4) r8 la'( sol''4) r8 sol''( |
    fad''4) r8 re''( sold''4) r8 sold'' |
    la''4
  }
  \tag #'secondo {
    fad'8-.\p sol'-. la'-. fad'-. mi'-. dod'-. re'-. mi'-. |
    fad'-. re'-. la-. re'-. fad'-. la'-. sol'-. fad'-. |
    mi'16 re' mi' fad' sol' fad' mi' re' dod' re' mi' re' dod' si la sol' |
    fad'8-. sol'-. la'-. fad'-. mi'-. dod'-. re'-. mi'-. |
    fad'-. la'-. sol'-. fad'-. mi'-. dod'-. re'-. mi'-. |
    fad'-. sol'-. la'-. fad'-. re'-. dod'-. si-. re'-. |
    dod'4
  }
>> r8 la la4 la |
la2 r |
<<
  \tag #'primo {
    re''2 la' |
    fad'4 la' r r8 re' |
    la2 la'4. sol'8 |
    fad'4 re' r2 |
    la''2_\markup\italic dol. ~ la''8 fad'' mi'' re'' |
    \grace dod''8 si'2 r4 sol''8. la''16 |
    si''4 si''8. dod'''16 re'''4 si''8. sol''16 |
    la''8. fad''16 re''8( la'') re'''4 \slurDashed dod'''8( si'') |
    la''2 sol''\trill |
    fad''4
  }
  \tag #'secondo {
    fad'8-. sol'-. la'-. fad'-. mi'-. dod'-. re'-. mi'-. |
    fad'-. re'-. la-. re'-. fad'-. la'-. sol'-. fad'-. |
    mi'16 re' mi' fad' sol' fad' mi' re' dod' re' mi' re' dod' si la sol' |
    fad'8 la re' la la4 r |
    <fad' la'>1 |
    <sol' si'> |
    q |
    << { la'2 la'4 } \\ { fad'2 fad'4 } >> mi''4 |
    la'8-. sol'-. fad'-. la'-. sol'-. mi'-. re'-. dod'-. |
    re'4
  }
>> r16 re' mi' fad' sol' fad' sol' la' si' la' si' dod'' |
<re'' re'>2.\ff re'4\p |
dod'2 si |
la4 la8. la16 la4 r |
<<
  \tag #'primo {
    do''1 |
    si'2 do''4 si'8 la' |
    sold'8\ff <mi' sold>16 q q4:16 q2:16 |
    <sold? fa'>2:16\p <mi' la>:16 |
    red'4-!\f <mi' sold?>-! <sold? mi'>2:16 |
    <sold? fa'>2:16\p <mi' la>:16 |
    red'4-!\f <mi' sold?>-! r2 |
    r8
  }
  \tag #'secondo {
    fad'1 |
    sold'2 fad' |
    mi'16-\sug\ff si si si si4:16 si2:16 |
    <si re'>2:16-\sug\p <mi' do'>2:16 |
    si4-!-\sug\f si-! si2:16 |
    <si re'>2:16-\sug\p <mi' do'>2:16 |
    si4-!-\sug\f si-! mi'8 re'( dod'! re') |
    dod'8
  }
>> \grace si'16 la'32[-\sug\p sold' la'16] dod''8-. la'-. mi''( dod'') mi''( dod'') |
r8 \grace dod''16 si'32 lad' si'16 re''8-. si'-. fad''( re'') fad''( re'') |
r8 \ficta \grace sold'16 fad'32 mi' fad'16 la'8( fad') r8 sold'!16 la' si'8 sold' |
la'4 r r2 |
r8 \grace si'16 la'32[ sold' la'16] dod''8-. la'-. mi''( dod'') mi''( dod'') |
r8 \grace dod''16 si'32 lad' si'16 re''8-. si'-. fad''( re'') fad''( re'') |
r8 \ficta \grace sold'16 fad'32 mi' fad'16 la'8( fad') r8 <<
  \tag #'primo {
    sold'!16 la' si'8 sold' |
    la'4 r <mi' si' mi''>\f r |
    <la mi' dod''>4 r <mi' si' sold''> r |
    <mi' dod'' la''> r <mi'' mi'>4.\rf( si'8)\p |
    do''4 r <mi'' mi'>4.\rf( si'8)\p |
    do''4 r <do'' mi'>2:16-\sug\f |
    <fa' do''>2:16 red'':16 |
    <mi'' sold'>4 r4 r\fermata si'8\p dod''! |
    re''1 |
    re''8(\rf dod'') si'8. re'16 re'4\rf re'8. re'16 |
    re'8(-\sug\rf dod') si4 r2 |
    dod'''2._\markup\italic dol. re'''8( dod''') |
    dod'''2. re'''8( dod''') |
    dod'''2. si''8.( dod'''16) |
    r8 la''(\mf sold'' la'') la''( dod''' si'' lad'') |
    si''2.\p dod'''8( re''') |
    \grace re'''8 dod'''4 si''8( la'' sold'' fad'') re'''( si'') |
    la''2 si''4.( re'''8) |
    \grace re'''8 dod'''4( si''8 la'' sold'' fad'') re'''( si'') |
    la''2 si''4.( la''8) |
    la'' r fa''2(\f mi''8\p re'') |
    dod''8 r fa'2(\f mi'8\p re') |
    dod'8\p r fa''2(\f mi''8 re'') |
    dod''2 r |
  }
  \tag #'secondo {
    si16 dod' re'8 si |
    dod'16 <dod' mi'>-\sug\f q q q4:16 <si mi'>2:16 |
    <dod' mi'>2:16 <re' mi'>:16 |
    <do' mi'>:16 <si mi'>:16-\sug\fp |
    <do' mi'>:16 <si mi'>:16-\sug\fp |
    <do' mi'>:16-\sug\f la':16 |
    la':16 la':16 |
    sold'4 r r\fermata sold'8-\sug\p la' |
    si'2~ si'4 la' |
    sold'4-\sug\rf sold'8. si16 si4-\sug\rf la8. la16 |
    sold2-\sug\rf r |
    r4 la'(\p sold') r |
    r4 la'( sold') r |
    r la'( \ficta sold'2) |
    r8 la'(-\sug\mf sold' la') la'( dod'' si' lad') |
    si'-\sug\p mi' mi' mi' mi'2:8 |
    mi'4 r r \once\slurDashed fad'8( re') |
    dod'4 la'2 sold'4 |
    \grace sold'8 la'4 re'8( dod' mi' re') re'4 |
    dod'2 \once\slurDashed re'4.( dod'8) |
    dod'8 r fa'2\f sold'4\p |
    la'8 r \ficta fa'2\f sold4\p |
    la8-\sug\p r fa'2-\sug\f sold'4 |
    la'2 r |
  }
>>
<<
  \tag #'primo {
    re''2\p la' |
    fad'4 la' r r8 re' |
    la2 la'4. sol'8 |
    fad'4 re' r2 |
    mi''2 si' |
    sol'4 si' r si''8. si''16 |
    si''4 si'2 si'4 |
    sol' mi' r si'8.\f si'16 |
  }
  \tag #'secondo {
    fad'8-.\p sol'-. la'-. fad'-. mi'-. dod'-. re'-. mi'-. |
    fad'-. re'-. la-. re'-. fad'-. sol'-. la'-. fad'-. |
    mi'16 re' mi' fad' sol' fad' mi' re' dod' re' mi' re' dod' si la sol' |
    fad'8-. la-. re'-. la-. la4 r |
    sol'8-. la'-. si'-. sol'-. fad'-. red'-. mi'-. fad'-. |
    sol'-. mi'-. si-. mi'-. sol-. la-. si-. sol'-. |
    red'16 mi' fad' sol' la' sol' fad' mi' red' mi' fad' mi' red' dod' si la |
    sol4 sol r si'8-\sug\f si' |
  }
>>
sol''8( fad'') mi''-. fad''-. mi''( re'') dod''-. si'-. |
<<
  \tag #'primo {
    lad'16\ff <fad' lad> q q q4:16 q2:16 |
    <lad? sol'!>2:16-\sug\p <fad' si>:16 |
    <sold mid'>4\f <lad fad'> q2:16 |
    <lad? sol'!>2:16\p <fad' si>:16 |
    <sold mid'>4\f <lad fad'> r2 |
    r8
  }
  \tag #'secondo {
    lad'16-\sug\ff fad' dod' dod' dod'4:16 dod'2:16 |
    <dod' mi'>2:16-\sug\p re':16 |
    dod'4-\sug\f dod' dod'2:16 |
    <dod' mi'>2:16-\sug\p re':16 |
    dod'4-\sug\f dod' fad'8 mi'( re' mi') |
    re'
  }
>> \grace dod''16 si'32\p lad' si'16 re''8-. si'-. fad''( re'') fad''( re'') |
r8 \grace re''16 dod''32 si' dod''16 mi''8-. dod''-. sol''( mi'') sol''( mi'') |
<<
  \tag #'primo {
    r8 re''-! fad''( re'') r dod''-! mi''( dod'') |
    \grace dod''8 re''2 r |
  }
  \tag #'secondo {
    r8 si'-. re''( si') r lad'-. dod''( lad') |
    \grace lad'8 si'2 r |
  }
>>
r8 \grace dod''16 si'32\p lad' si'16 re''8-. si'-. fad''( re'') fad''( re'') |
r8 \grace re''16 dod''32 si' dod''16 mi''8-. dod''-. sol''( mi'') sol''( mi'') |
<<
  \tag #'primo {
    r8 re''-! fad''( re'') r dod''-! mi''( dod'') |
    si'4 sol'2\f fad'8 mi' |
    re'2\p dod' |
    si r |
  }
  \tag #'secondo {
    r8 si'-. re''( si') r lad'-. dod''( lad') |
    si'4 si2.-\sug\f |
    si2-\sug\p lad |
    si r |
  }
>> \allowPageTurn
%%
re'8.\p re'16 re'8. re'16 <<
  \tag #'primo { <la mi'>8. q16 q8. q16 | <fad' la>4 }
  \tag #'secondo { dod'8. dod'16 dod'8. dod'16 | re'4 }
>> r4 r2 |
<re' la' fad''>4\f r <la mi' la' mi''>4 r |
re'8.\p re'16 re'8. re'16 <<
  \tag #'primo { <la mi'>8. q16 q8. q16 | <fad' la>4 }
  \tag #'secondo { dod'8. dod'16 dod'8. dod'16 | re'4 }
>> r4 r2 |
<re' la' fad''>4\f r <la mi' la' mi''>4 r |
<fad' re''>4 <fad' la>4 <<
  \tag #'primo {
    la'16\p re'' re'' re'' re''4:16 |
    do''2:16 do'':16 |
    si':16\f si':16 |
    \ru#4 { <si' si''>16\fp si' si' si' si'4:16 } |
    \ru#2 { <la' la''>16\fp la' la' la' la'4:16 } |
  }
  \tag #'secondo {
    <re' fad'>16-\sug\p <fad' la'> q q q4:16 |
    q2:16 q:16 |
    q:16-\sug\f q:16 |
    q:16\fp q:16\fp |
    sol':16-\sug\fp sol':16-\sug\fp |
    sol':16-\sug\fp sol':16-\sug\fp |
  }
>>
fad'8\f re'-. re'-. re'-. re' mi'16 fad' sol' la' si' dod'' |
<<
  \tag #'primo {
    re''8-. fad''-.\p fad''-. fad''-. fad''2:8^\dotFour |
    sol''8-. sol''-. la''-. la''-. si''-. si''-. la''-. la''-. |
    sol''-. sol''-. fad''-. fad''-. sol''-. sol''-. mi''-. mi''-. |
  }
  \tag #'secondo {
    re''2:8^\dotFour-\sug\p re''8-. re''-. dod''-. dod''-. |
    si'-. si'-. la'-. la'-. sol'-. sol'-. fad'-. fad'-. |
    sol'-. sol'-. la'-. la'-. sol'-. sol'-. la'-. la'-. |
  }
>>
<>\f <<
  { fad''2:16 fad'':16 | mi'':16 mi'':16 | } \\
  { re''2:16 re'':16 | re'':16 dod'':16 | }
>>
<<
  \tag #'primo {
    re''8-. fad''-.\p la''-. fad''-. la''16( sol'' fad'' mi'') la''( sol'' fad'' mi'') |
    re''4 r r2 |
    R1 |
    re''8-. fad''-. la''-. fad''-. la''16( sol'' fad'' mi'') la''( sol'' fad'' mi'') |
    re''4 r r2 |
    R1 |
    re''8(\p fad'') la''-. la''-. fad''( mi'') re''-. re''-. |
    re''( dod'') si'-. la'-. sold'( si') dod''-. re''-. |
    dod''16\f
  }
  \tag #'secondo {
    fad'8-. re'-.-\sug\p fad'-. re'-. mi'-. dod'-. mi'-. dod'-. |
    fad' re' fad' re' sol' mi' sol' mi' |
    fad' re' fad' re' mi' dod' mi' dod' |
    fad' re' fad' re' sol' mi' sol' mi' |
    fad' re' fad' re' sol' mi' sol' mi' |
    fad' re' fad' re' mi' dod' mi' dod' |
    fad'2:8-\sug\p fad':8 |
    sold'2:8 sold':8 |
    la'16-\sug\f
  }
>> <la' la''>16 q q q4:16 q16 la' la' la' la'4:16 |
sib':16 la':16 re'':16 dod'':16 |
fa''4:16 mi''16 la' la' la' la'2:16 |
sib'4:16 la':16 re'':16 dod'':16 |
fa''4:16 mi'':16 <<
  \tag #'primo {
    fa''4.\f mi''16 re'' |
    dod''4\p la' fa''4.\fp mi''16 re'' |
    dod''8-.
  }
  \tag #'secondo {
    si'2-\sug\f |
    la'4-\sug\p dod' si'2-\sug\fp |
    la'8
  }
>> la'8-.\f la'-. la'-. la' la' la' si'16 dod'' |
<<
  \tag #'primo {
    re''8 fad''-.\p fad''-. fad''-. fad''2:8^\dotFour |
    sol''8-. sol''-. la''-. la''-. si''-. si''-. la''-. la''-. |
    sol''-. sol''-. fad''-. fad''-. sol''-. sol''-. mi''-. mi''-. |
    \ru#2 fad''2:8^\dotFour |
    mi''2:8 mi'':8 |
    <fad'' re'''>2:16\ff q:16 |
    <mi'' re'''>:16 <mi'' dod'''>:16 |
  }
  \tag #'secondo {
    re''8-. re''-.-\sug\p re''-. re''-. re''-. re'' -. dod''-. dod''-. |
    si'-. si'-. la'-. la'-. sol'-. sol'-. fad'-. fad'-. |
    sol'-. sol'-. la'-. la'-. sol'-. sol'-. la'-. la'-. |
    re''2:8^\dotFour re'':8^\dotFour |
    re'':8 dod'':8 |
    fad'':16-\sug\ff fad'':16 |
    << { mi'':16 mi'':16 } \\ { re'':16 dod'':16 } >> |
  }
>>
\ru#3 { re''8-. fad''-. la''-. fad''-. la''16( sol'' fad'' mi'') la''( sol'' fad'' mi'') | }
re''4 r8 la sol la si dod' |
re' mi' fad' sol' la' si' dod'' re'' |
la'4 <la' la''> la la' |
re'4 r <re' la' fad''> q |
<< fad''2 <la' re'>4 >> r2 |
