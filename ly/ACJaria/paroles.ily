Fin -- ta lar -- va, d’a -- bis -- so fra l’om -- bra,
il pia -- ce -- re,
il pia -- ce -- re gli scher -- za d’in -- tor -- no,
gli scher -- za d’in -- tor -- no.
Ah se il son -- no di mor -- te l’in -- gom -- bra,
se i suoi lu -- mi si chiu -- do -- no al gior -- no,
si chiu -- do -- no al gior -- no,
nell’ or -- ro -- re del car -- ce -- re in -- de -- gno
più che a sde -- gno ti muo -- va a pie -- tà,
più che a sde -- gno ti muo -- va a pie -- tà.

Scio -- gli, sgom -- bra, la not -- te fu -- nes -- ta,
Dio pos -- sen -- te, lo scuo -- ti, lo des -- ta,
lo scuo -- ti, lo des -- ta.
Chi può trar -- lo dall’ om -- bra di mor -- te;
se i tuoi rag -- gi per scor -- ta non ha?
Se i tuoi rag -- gi per scor -- ta non ha __
per scor -- ta non hà?
Chi può trar -- lo dall’ om -- bra di mor -- te;
chi?

Fin -- ta lar -- va, d’a -- bis -- so fra l’om -- bra,
il pia -- ce -- re,
il pia -- ce -- re gli scher -- za d’in -- tor -- no.
Ah se il son -- no di mor -- te l’in -- gom -- bra,
nell’ or -- ro -- re del car -- ce -- re in -- de -- gno
più che a sde -- gno ti muo -- va a pie -- tà,
più che a sde -- gno ti muo -- va a pie -- tà __
ti muo -- va a pie -- tà.
\tag #'ublado {
Il tuo spir -- to m’in -- fiam -- ma, m’ac -- cen -- de
Dio pos -- sen -- te lo sen -- to, lo scer -- no.
Ah le fro -- di, e le for -- ze d’A -- ver -- no
van con -- tra -- sto sa -- ran -- no al tuo van -- to,
van con -- tra -- sto sa -- ran -- no al tuo van -- to,
e __ l’in -- can -- to di va -- na bel -- tà,
di va -- na bel -- tà.

Il tuo spir -- to m’in -- fiam -- ma, m’ac -- cen -- de
Dio pos -- sen -- te lo sen -- to, lo scer -- no,
lo sen -- to, lo scer -- no.
Ah le fro -- di, le for -- ze d’A -- ver -- no
van con -- tra -- sto sa -- ran -- no al tuo van -- to,
si van con -- tra -- sto,
e __ l’in -- can -- to di va -- na bel -- tà,
di va -- na bel -- tà,
di va -- na bel -- tà.
}
