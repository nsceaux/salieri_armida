\clef "treble" dod'!4\fermata r4 |
lad r r2 |
si4 r si-! red'-! |
sid-! r r2 |
R1 |
dod'4\fermata r dod'4-! mi'-! |
red'-! r r2 |
r2 r8 <>\rf <<
  \tag #'primo {
    si''8( la'' sold''!) |
    sold''( fad'' mi'' red'') mi''4 r8. re''16\f |
    re''4 re'' re''2 |
  }
  \tag #'secondo {
    si'8( la' sold'!) |
    sold'( fad' mi' red') mi'4 r8. << si'16 \\ sold'-\sug\f >> |
    << { si'4 si' si'2 } \\ { sold'4 sold' sold'2 } >> |
  }
>>
R1*2 |
<<
  \tag #'primo {
    r2 dod''4 r |
    r2 la'4 r |
    r2 la'4 r |
    r8 si'(-.\p si'-. si'-.) r si'-.( si'-. si'-.) |
    r8 si'-.( si'-. si'-.) r si'8-.( do''-. si'-.) |
    \once\tieDashed lad'1\p~ |
    \ficta lad'2( \once\override Script.avoid-slur = #'outside si'8)\fermata si'8[ si' si'] |
    si'2 do''~ |
    do''1~ |
    do''2 si' |
  }
  \tag #'secondo {
    r2 sold'4 r |
    r2 la'4 r |
    r2 fad'4 r |
    r8 sold'-.(-\sug\p sold'-. sold'-.) r la'-.( la'-. la'-.) |
    r8 sol'!-.( sol'-. sol'-.) r sol'-.( sol'-. sol'-.) |
    \once\tieDashed fad'1-\sug\p~ |
    fad'2~ fad'8\fermata fad'8[ fad' fad'] |
    fad'2~ fad'~ |
    fad'1~ |
    fad' |
  }
>>
