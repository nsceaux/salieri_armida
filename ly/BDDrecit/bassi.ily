\clef "bass" dod!4\fermata r |
lad, r r2 |
si,4 r si,-! red-! |
sid,-! r r2 |
R1 |
dod4\fermata r4 dod-! mi-! |
red-! r r2 |
r mi4(\p sold! |
la si mi) r8. mid16\f |
\ficta mid4 mid mid2 |
R1*2 |
r2 mid4 r |
r2 fad!4 r |
r2 fad4 r |
mi4\p r red r |
mi r mi r |
\once\tieDashed mi1~ |
mi2 re8\fermata re[ re re] |
re2 \once\tieDashed red~ |
\ficta red1~ |
red |
