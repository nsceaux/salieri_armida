\clef "soprano/treble" r2 |
r4 r8 dod''16 fad'' fad''4 dod''8 re''! |
si' si' r4 r2 |
r sold'!4 r16 sold' sold' lad' |
sid'8 sid' r sid'16 dod'' red''4 sid'8 sold'! |
dod''4 r r2 |
r4 si'8 si'16 si' si'8 \ficta fad' r fad'16 sold' |
la'4 la'8 si' sold' sold' r4 |
R1 |
r2 r8 re'' dod'' re'' |
si' si' si' si'16 si' si'8 si' sold' sold' |
r sold' sold' la' si' si' r sold'' |
sold'' si' r16 si' dod'' re'' dod''4 dod'' |
si'16. si'32 si'8 r dod'' la' la' r la' |
la' la' la' dod'' la' la' r16 la' la' sold' |
si'8 si' r4 r2 |
si'8. sol'!16 sol'4 r r8 si' |
\grace si' lad'4 r8 \ficta fad' lad' lad' lad' si' |
dod''4 lad'8 fad' si'\fermata si' r4 |
r8 si' si' si' do'' do'' r16 do'' do'' do'' |
la'8 la' r la' la' la' la' la' |
fad' fad' r fad' si'4 la' |
