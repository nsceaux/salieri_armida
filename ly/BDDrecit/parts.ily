\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Rinaldo
  \livretVerse#12 { Ora si ch'io mi perdo. Ah chi le inspira }
  \livretVerse#12 { Questo nuovo terror? Teme il periglio, }
  \livretVerse#12 { E aborrisce l'ajuto! Il Ciel pietoso }
  \livretVerse#12 { M'arma per sua difesa, e sul suo capo }
  \livretVerse#12 { Il Cielo, se credo a lei, fulmina e tuona! }
  \livretVerse#12 { Di perdermi paventa, e m'abbandona! }
  \livretVerse#12 { Misera! Oh Dio! la rende }
  \livretVerse#12 { Forfennata l'affanno. In questo stato }
  \livretVerse#12 { Lasciarla in preda al suo crudel deliro }
  \livretVerse#12 { Saria… }
} #}))
