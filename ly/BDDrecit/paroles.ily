O -- ra sì ch’io mi per -- do. Ah chi le i -- spi -- ra
que -- sto nuo -- vo ter -- ror? Te -- me il pe -- ri -- glio,
e ab -- bo -- ri -- sce l’a -- ju -- to! Il Ciel pie -- to -- so
m’ar -- ma per sua di -- fe -- sa, e sul suo ca -- po
il Cie -- lo, se cre -- do a le -- i, ful -- mi -- na, e tuo -- na!
Di per -- der -- mi pa -- ven -- ta, e m’ab -- ban -- do -- na!
Mi -- se -- ra! Oh Dio! la ren -- de
for -- sen -- na -- ta l’af -- fan -- no. In que -- sto sta -- to
la -- sciar -- la in pre -- da al suo cru -- del de -- li -- ro
sa -- ri -- a…
