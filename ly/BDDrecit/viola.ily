\clef "alto" dod'!4\fermata r |
lad r r2 |
si4 r si-! red'-! |
sid-! r r2 |
R1 |
dod'4\fermata r dod'-! mi'-! |
red'-! r r2 |
r2 mi'4(-\sug\p sold'! |
la' si' mi') r8. mid'16-\sug\f |
\ficta mid'4 mid' mid'2 |
R1*2 |
r2 \ficta mid'4 r |
r2 dod'4 r |
r2 red'4 r |
r8 mi'-\sug\p -.( mi'-. mi'-.) r \slurDashed fad'8-.( fad'-. fad'-.) |
r8 mi'(-. mi'-. mi')-. r mi'(-. mi'-. mi')-. | \slurSolid
\once\tieDashed dod'1-\sug\p~ |
dod'2( \once\override Script.avoid-slur = #'outside re'8)\fermata re'[ re' re'] |
re'2 la'~ |
la'1~ |
la' |
