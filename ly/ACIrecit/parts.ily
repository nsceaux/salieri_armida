\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretVerse#12 { Eterno Proveder, tu che guidasti }
    \livretVerse#12 { Per si strano camino i passi miei }
    \livretVerse#12 { Scorgigli al fin prescritto. Ecco l'albergo, }
    \livretVerse#12 { Ove, in grembo al piacere, al giogo indegno }
    \livretVerse#12 { Di beltà lusinghiera }
    \livretVerse#12 { Il tuo giovane Eroe piega la fronte, }
    \livretVerse#12 { Tu che percuoti il monte, e dall' estrema }
    \livretVerse#12 { Falda si scuote, e fumo, e fiamma spira, }
  }
  \null
  \column {
    \livretVerse#12 { Tu che nel mezzo all' ira }
    \livretVerse#12 { Il suol rimiri, e il suol vacilla, e trema, }
    \livretVerse#12 { Scuoti, scuoti, gran Dio, dal cupo sonno }
    \livretVerse#12 { Quell' alma incauta, al guardo suo disvela }
    \livretVerse#12 { L'orror de' falli suoi, dell'empia Armida }
    \livretVerse#12 { Sien le frodi schernite, }
    \livretVerse#12 { E trionfi il tuo nome in faccia a Dite. }
  }
  \null
}

       #}))
