E -- ter -- no Pro -- ve -- der, tu che gui -- da -- sti
per sì stra -- no cam -- mi -- no i pas -- si miei
scor -- gi -- gli al fin pre -- scrit -- to. Ec -- co l’al -- ber -- go,
o -- ve, in grem -- bo al pia -- ce -- re, al gio -- go in -- de -- gno
di bel -- tà lu -- sin -- ghie -- ra
il tuo gio -- va -- ne E -- ro -- e pie -- ga la fron -- te,
tu che per -- cuo -- ti il mon -- te, e dall’ es -- tre -- ma
fal -- da si scuo -- te, e fu -- mo e fiam -- ma spi -- ra,
tu che nel mez -- zo all’ i -- ra
il suol ri -- mi -- ri, e il suol va -- cil -- la, e tre -- ma,
scuo -- ti, scuo -- ti, gran Di -- o, dal cu -- po son -- no
quell’ al -- ma in -- cau -- ta, al guar -- do suo dis -- ve -- la
l’or -- ror de’ fal -- li suo -- i, dell’ em -- pia Ar -- mi -- da
sien le fro -- di scher -- ni -- te,
e tri -- on -- fi il tuo no -- me in fac -- cia a Di -- te.
