\clef "tenor/G_8" R1*2 |
r2 r4 r8 re' |
re'2. do'8[ re'] |
mib'4. re'8 do'4 r |
do'4 do'8 do' la la r la16 la |
la4 la8 sib do' do' r16 do' re' mib' |
\grace mib'4 re' re'16 la la8 r la do' sib |
sol sol r4 r2 |
R1 |
r4 re'8 re'16 re' si!8 si r sol16 sol |
sol4 sol8 la si! si r16 si si do' |
re'8 re' r re'16 sol' sol'4 re'8 re' |
si! si r si16 si si4 do'8 re' |
re' fa lab8 lab16 sol mib8 mib r4 |
R1*2 |
do'8 do'16 do' mib'8 do' lab lab r mib |
lab lab lab sib do' do' r do' |
sib sib r sib fa'4 re' |
sib4. lab8 sol sol r4 |
sib8 sib16 sib sib8 mib' mib' sib r16 sib sib sib |
\ficta sib8 sib r sib reb' reb' reb' mib' |
do' do' r4 do'8 lab r4 |
mib'4 do'8 reb' sib sib sib4 |
sol8 fa mi! mi r sib sib do' |
la! la r fa do' do' re' mib' |
re'8 re' r la re4. fad8 |
la do' sib sib r16 re' re' re' sib8 sib |
r8 \ficta sib16 sib sib8 do'16 re' re'8 sol r sol16 la |
sib2 do'4. re'8 |
mib'8 do' r16 do' do' sib sol8 sol r4 |
R1 |
