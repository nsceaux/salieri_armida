\clef "alto" sib4 r8 sib-!-\sug\f do'4-! re'-! |
mib'4-! r8 do'-! la4-! sol-! |
fad-! fad(-\sug\p sol la) |
sol sol sol2~ |
sol mib~ |
mib1~ |
mib |
re2 r |
sol4-\sug\f r sol r |
re'4 re' re' re' |
re' r r2 |
R1*3 |
sol2-\sug\fp sol4 r |
do-\sug\f r sol sol |
sol sol do'2:32-\sug\f |
do':32 do':32 |
do'1:32 |
re':32 |
re'2:32 mib':32 |
\ficta mib'1:32 |
\ficta mib'2:32 sol':32 |
lab':32-\sug\ff lab'4 r8. \ficta lab16 |
\ficta lab4 r \once\tieDashed do'2-\sug\p~ |
do'1 |
do'4 r r2 |
re'4 r do'2-\sug\fp~ |
do'4 sib r2 |
R1 |
re'2 mib'4. fa'8 |
mib'2 r4 re' |
sol2 r |
