\clef "treble" sib4 r8 sib'-!\f do''4-! re''-! |
\override AccidentalSuggestion.avoid-slur = #'outside
mib''-! r8 do''-! la'4-! sol'-! |
fad'8 <<
  \tag #'primo {
    re'4\p re' re' re'8 |
    re'8 re'4 re'8 re'4 do'8 re' |
    mib'4. re'8 do'2~ |
    do'1~ |
    do'~ |
    do'2 r |
    r8 sol'16(\f fad') sol'8-! la'-! sib'8-! sib'16 la' sib'8-! do''-! |
    re''-! sib''(\rf la'' fad'') sol''-! re''4( do''8) |
    si'!4 r r2 |
  }
  \tag #'secondo {
    r8 la4(-\sug\p sib do') |
    sib4 sib sib2~ |
    sib la~ |
    la1~ |
    la~ |
    la2 r |
    \ficta sib4-\sug\f r r8 \once\slurDashed sol'16( fad') sol'8-! la'-! |
    \ficta \once\slurDashed sib'4( do'' re'' la' |
    sol') r r2 |
  }
>>
R1*3 |
<<
  \tag #'primo {
    fa'2\fp r8 do'16(\f si!) do'8-. re'-. |
    mib'8 mib'16( re') mib'8-. fa'-. sol' mib''( re'' si'!) |
    do''8( sol'4 fa'!8) mib'32\f sol' sol' sol' sol' sol' sol' sol' sol'4:32 |
    sol'2:32 lab':32 |
    \ficta lab':32 lab':32 |
    \ficta sib':32 sib':32 |
    \ficta sib':32 sib':32 |
    \ficta sib'1:32 |
    \ficta sib'2:32 reb'':32 |
    do'':32\ff do''4 r8. <do'' mib'>16 |
    <do'' mib'?>4 r sib2\p~ |
    sib1 |
    la!4 r r2 |
    re''4 r re'2\fp~ |
    re' r |
    R1 |
    sib'2 do''4. re''8 |
    mib''2
  }
  \tag #'secondo {
    re'2-\sug\fp mib'4 r |
    r8 \once\slurDashed do'16(-\sug\f si!) do'8-. re'-. \once\slurDashed mib'4( fa' |
    sol' re') mib'2:32-\sug\f |
    mib':32 mib':32 |
    mib':32 mib':32 |
    fa'1:32 |
    fa'2:32 <sol' sib>:32 |
    <sib? sol'>1:32 |
    <sib? sol'>2:32 <mib' sib'?>:32 |
    <mib' do''>:32-\sug\ff q4 r8. <lab' do'>16 |
    <lab'? do'>4 r \once\tieDashed sol'2-\sug\p~ |
    sol'1 |
    fa'4 r r2 |
    la'4 r <re' la>2-\sug\fp~ |
    q4 <sib re'> r2 |
    R1 |
    sol'2 sol'4. fa'8 |
    mib'2
  }
>> r4 <re' la' fad''> |
<re' sib' sol''>2 r |
