\clef "bass" sib,4 r8 sib-!\f do'4-! re'-! |
mib'-! r8 do'-! la4-! sol-! |
fad8 r fad4(\p sol la) |
sol sol sol2~ |
sol1~ |
sol~ |
sol |
fad2 r |
sol4\f r sol r |
sol( la sib fad |
fa!) r r2 |
R1*3 |
si,!2\fp do4 r |
do\f r do( do |
mib si,!) do-\sug\f r |
r2 do |
R1 |
re2 r |
r mib:32 |
\ficta mib1:32 |
\ficta mib:32 |
lab2:32\ff lab4 r8. lab16 |
\ficta lab4 r mi!2\p~ |
mi1 |
fa4 r r2 |
fad4 r \once\tieDashed fad,2\fp~ |
\ficta fad,4 sol, r2 |
R1 |
sol4. fa!8 mib4. re8 |
do2 r4 re |
sol,2 r |
