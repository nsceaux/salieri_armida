\clef "alto" mib2-\sug\f fa4. sol8 |
mib4 r r2 |
fa2 sol4. lab8 |
fa4 r r2 |
sol2 fa4. mib8 |
re2. r8 re |
dod2. r8 sib-! |
lab4-! sol-! fa-! mib-! |
fa2 mib |
sol4 sol sol sol\p |
sol sol sol sol |
sol sol-\sug\f sol <>-\sug\p <<
  { sol'4 |
    fa'4. sol'16 fa' mib'4. fa'16 mib' |
    re'4 } \\
  { mib'4 |
    re'4. mib'16 re' do'4. re'16 do' |
    si?4 }
>> sol4-\sug\f sol sol-\sug\p |
sol sol sol sol |
sol sol-\sug\f sol <>-\sug\p <<
  { sol'4 |
    fa'4. sol'16 fa' mib'4. fa'16 mib' |
    re'4 } \\
  { mib'4 |
    re'4. mib'16 re' do'4. re'16 do' |
    si4 }
>> fa'4-\sug\f fa' fa' |
mib'4. mib'8 re'4. re'8 |
do'4. sol'8 sol'4. sol'8 |
lab'1 |
fa'2 sol'4( mib') |
mib'4. mib'8 mib'4. mib'8 |
re'4 sib\p sib sib |
sib sib sib sol'\f |
sol'2 fa'4 sib\p |
sib4 sib sib sol'\f |
sol'2( fa'4) fa' |
fa' fa' fa' fa' |
sol' r r2 |
fa'4 lab lab fa' |
fa' r r2 |
sol'4. sol'8 re'4. re'8 |
mib'4. mib'8 lab4. lab8 |
sol2 lab |
sib sib |
mib
%%
r4 r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 fa' |
fa'4. fa'8 solb'4. solb'8 |
fa'2 r |
r r4 r8 <<
  { la' | la'?2 la'4 la' | } \\
  { mib'8 | mib'2 mib'4 mib' | }
>>
solb'8\ff solb'4 solb' solb' solb'8 |
fa'2 
%%
r |
R1 |
sib'8.\p sib16 lab8.\trill sol32 lab sol4 r |
R1 |
sib'8. sib16 lab8.\trill sol32 lab sol4 r |
mib-\sug\f r sol'8.\p fa'16 mib'8. re'16 |
mib'4\f r sol'8.-\sug\p fa'16 mib'8. re'16 |
mib'4\f r sol'8.-\sug\p fa'16 mib'8. fa'16 |
sol'8. fa'16 mib'8. re'16 do'4\fermata r |
fa'1\pp |
mib' |
re'4 r mib' r |
fa'2 r |
sib1 |
lab |
sol4 r do r |
re2 r |
re''4:16\f si':\p do''2:\fp |
si'2:\fp si':\fp |
re''4:\f si':\p do''2:\fp |
si':\fp do'': |
do'': si'16\f sol' sol' sol' sol'4: |
sol'8 fad'16 mi' re' do' si lab sol4
%%
r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 fa' |
fa'4. fa'8 solb'4. solb'8 |
fa'2 r |
r r4 r8 <<
  { la' | la'?2 la'4 la' | } \\
  { mib'8 | mib'2 mib'4 mib' | }
>>
solb'8\ff solb'4 solb' solb' solb'8 |
fa'4 r
%%
r2 |
R1*5 | \allowPageTurn
sib2:16-\sug\ff sib: |
mib':-\sug\p mib': |
do':-\sug\ff do': |
fa':-\sug\p fa': |
fa':\ff fa': |
re':\p re': |
re': re': |
mib':-\sug\ff mib': |
mib'4\fermata r r2 |
mib'2:16\ff mib': |
mib'4 r r2 |
mi'4-\sug\p r mi' r |
fa'2:16-\sug\f fa'4: sol': |
fa'16\fp do'' do'' do'' do''4:16 re'': do'': |
re''4\f do''32 do re mib fa sol la sib do'16 do'' do'' do'' do''4: |
do''2:\fp re''4: do'': |
re''4-!\f do''-! r2 |
mib'2:8-\sug\p mib': |
lab': lab': |
re'2
%%
r4 r8 sib-\sug\f |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 fa' |
fa'4. fa'8 solb'4. solb'8 |
fa'2 r |
r r4 r8 <<
  { la' | la'?2 la'4 la' | } \\
  { mib'8 | mib'2 mib'4 mib' | }
>>
solb'8\ff solb'4 solb' solb' solb'8 |
fa'2 r2 |
