\clef "treble" do'2\f re'4. mib'8 |
do'4 r r2 |
re'2 mib'4. fa'8 |
re'4 r r2 |
<<
  \tag #'primo {
    mib'2 fa'4. sol'8 |
    lab'2. r8 lab' |
    sol'2. r8 sol'-! |
    fa'4-! mib'-! re'-! do'-! |
    si2 do' |
    sol2. r4 |
  }
  \tag #'secondo {
    do'2 re'4. mib'8 |
    fa'2. r8 fa' |
    mi'2. r8 dod' |
    do'!4-! sib-! lab-! sol-! |
    sol1 |
    sol2. r4 |
  }
>>
R1 |
r4 sol\f sol r |
R1 |
r4 sol\f sol r |
R1 |
r4 sol\f sol r |
R1 |
<<
  \tag #'primo {
    r4 <>-\sug\f <si' lab''> q q |
    <do'' sol''>4. sol''8 fa''4. fa''8 |
    mib''4. mib''8 mib''4. mib''8 |
    mib''4( lab'' \grace sol''8 fa''4 mib'') |
    re''2 mib''4 sol'' |
    fa''4. fa''8 fa''4. fa''8 |
    fa''2 r4 fa''\p |
    fad''2( sol''8) r <sib' sib''>4\f |
    \once\slurDashed <sib'_~ sib''(>2 <lab'') sib'>4 fa''\p |
    fad''2( sol''8) r <sib' sib''>4\f |
    \once\slurDashed <sib'_~ sib''^(>2 <lab'') sib'>4 lab'' |
    sol''8 re'''4 si'' lab'' sol''16 fa'' |
    mi''4 r r2 |
    fa''8 do'''4 lab'' sol'' fa''16 mib''! |
    re''4 r r2 |
    mib''4. mib''8 lab''4. lab''8 |
    sol''4. sol''8 do'''4. do'''8 |
    sib''2 r4 r16 do''32[ re'' mib'' fa'' sol'' lab''] |
    sol''2 fa''\trill |
    mib''
  }
  \tag #'secondo {
    r4 <>-\sug\f <fa' re''> q q |
    <mib' do''>4. do''8 do''4. si'8 |
    do''4. do''8 reb''4. reb''8 |
    do''1 |
    sib'2 sib'4 sib' |
    do''4. do''8 do''4. do''8 |
    re''4 r r re''-\sug\p |
    re''2( mib''8) r <sol' sib>4-\sug\f |
    \once\slurDashed <sol'^( sib_~>2-\sug\f <sib fa')>4 re''-\sug\p |
    re''2( mib''8) r <sol' sib>4-\sug\f |
    \once\slurDashed <sol'^( sib_~>2 <sib fa')>4 re'' |
    <re'' re'>4 q q q |
    dod'' r r2 |
    do''!4 do'' do'' do'' |
    sib' r r2 |
    sib'4. sib'8 sib'4. sib'8 |
    sib'4. mib''8 mib''4. mib''8 |
    mib''2 do''4. fa''8 |
    mib''2 re'' |
    mib''
  }
>>
%%
r4 r8 sib' |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
<re' sib'>2 r4 r8 <<
  \tag #'primo {
    sib'8 |
    sib'4. sib'8 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    solb''8 solb''4 solb'' solb'' solb''8 |
    la''8\ff la''4 la'' la'' la''8 |
    sib''2
  }
  \tag #'secondo {
    re'8 |
    re'4. re'8 la4. la8 |
    sib4 re' r2 |
    r r4 r8 <<
      { do''8 | do''2 do''4 do'' | } \\
      { la'8 | la'2 la'4 la' | }
    >>
    mib''8-\sug\ff mib''4 mib'' mib'' mib''8 |
    re''2
  }
>>
%%
<<
  \tag #'primo {
    r4 r8 sib' |
    sol''2\fp fa''4. mib''8 |
    re''2 mib''4 r8 sib' |
    sol''2\fp fa''4. mib''8 |
    re''2 mib''4 r8 sib' |
    sol''8.\fp fa''16 mib''8. re''16 do''8. re''16 mib''8. fa''16 |
    sol''8.\fp fa''16 mib''8. re''16 do''8. re''16 mib''8. fa''16 |
    sol''8.\fp fa''16 mib''8. re''16 do''8. re''16 mib''8. re''16 |
    do''8. sib'16 la'8. sol'16 fa'4\fermata r4 |
  }
  \tag #'secondo {
    r2 |
    sib'2-\sug\fp lab'4. sol'8 |
    fa'4 sib' sib' r |
    sib'2-\sug\fp lab'4. sol'8 |
    fa'4 sib' sib' r |
    mib'8.-\sug\fp re'16 sol'8. fa'16 mib'8. fa'16 sol'8. fa'16 |
    mib'8.-\sug\fp re'16 sol'8. fa'16 mib'8. fa'16 sol'8. fa'16 |
    mib'8.-\sug\fp re'16 sol'8. fa'16 mib'8. fa'16 sol'8. fa'16 |
    mib'8. re'16 do'8. sib16 la4\fermata r4 |
  }
>>
fa'1\pp |
mib' |
re'4 r mib' r |
fa'2 r |
sib'1 |
lab' |
sol'4 r <<
  \tag #'primo {
    la'4 r |
    sib'2 r |
    sol''16\f sol'' sol'' sol'' re''4:16\p mib''2:\fp |
    re'':\fp re'':-\sug\fp |
    sol''4:\f re'':\p mib''2:\fp |
    re'':\fp do''4: re'': |
    mib'': do'': sol''8\f fad''16 mi'' re'' do'' si' lab' |
  }
  \tag #'secondo {
    fa'4 r |
    fa'2 r |
    <sol sol'>2:16-\sug\fp q2:-\sug\fp |
    q:-\sug\fp q:-\sug\fp |
    q:-\sug\fp q:-\sug\fp |
    q:-\sug\fp sol': |
    fad': sol': |
  }
>>
sol'8-\sug\f fad'16 mi' re' do' si lab sol4
%%
r8 sib'! |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
sib'2 r4 r8 <<
  \tag #'primo {
    sib'8 |
    sib'4. sib'8 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    \ficta solb''8 solb''4 solb'' solb'' solb''8 |
    la''8\ff la''4 la'' la'' la''8 |
    sib''4
  }
  \tag #'secondo {
    re'8 |
    re'4. re'8 la4. la8 |
    sib4 re' r2 |
    r r4 r8 <<
      { do''8 | do''2 do''4 do'' | } \\
      { la'8 | la'2 la'4 la' | }
    >>
    mib''8-\sug\ff mib''4 mib'' mib'' mib''8 |
    re''4
  }
>> r4 r2 |
R1*2 | \allowPageTurn
%%
<<
  \tag #'primo {
    re'16\pp re' re' re' re'4:16 mib'2: |
    re'4: re': re' sol' |
    sol'2 fa' |
    mib'8\ff fa'16 sol' lab' sib' do'' re'' mib''8 fa''16 sol'' lab'' sib'' do''' re''' |
    mib'''4 r r2 |
    fa'8\ff sol'16 la' sib' do'' re'' mi'' fa''8 sol''16 la'' sib'' do''' re''' mi''' |
    fa'''4 r r2 |
    sib8\ff do'16 re' mib' fa' sol' \ficta la' sib'8 do''16 re'' mib'' fa'' sol'' la'' |
    sib''4 r r2 |
    sib''16-.\p lab''!-. sol''-. fa''-. mib''-. re''-. do''-. sib'-. lab'-. sol'-. fa'-. mib'-. re'-. do'-. sib-. lab-. |
    sol8 <sib' sol''>16\ff q q q q q q2:16 |
    q4\fermata r r2 |
    q16\ff q q q q4:16 q2: |
    sol''4 r r2 |
    do''4\p r do'' r |
    do''2:16\f do'': |
    do''16\fp lab''! lab'' lab'' lab''4:16 lab''2: |
    lab''4-!\f sol''-! <<
      { sol''16 sol'' sol'' sol'' sol''4:16 } \\
      { do''4: do'': }
    >> |
    lab''4:16\fp lab'': lab''2: |
    lab''4-!\f sol''-! r2 |
    do''8\p do'' do'' do'' do''2:8 |
    do'': do'': |
    si'2
  }
  \tag #'secondo {
    si2:16-\sug\pp do': |
    si: si4 sol' |
    sol'2 fa' |
    <mib' sol>2:16-\sug\ff q: |
    <sib sol'>:\p q: |
    <la fa'>:-\sug\ff q: |
    <do' la'>:16\p q: |
    <re' sib'>:-\sug\ff q: |
    <sib fa'>:\p q: |
    q: q: |
    <<
      { s8 sib'16 sib' sib'4:16 sib'2: | sib'4\fermata } \\
      { sol'8 sol'16-\sug\ff sol' sol'4:16 sol'2: | sol'4 }
    >> r4 r2 |
    << { sib'2:16 sib': sib'4 } \\ { sol'2:-\sug\ff sol': | sol'4 } >> r4 r2 |
    << sib'4 \\ sol'-\sug\p >> r << sib' \\ sol' >> r |
    lab'2:16-\sug\f lab'4: sib': |
    lab'16-\sug\fp fa'' fa'' fa'' fa''4:16 fa''2: |
    si'4-\sug\f do''32 do' re' mib' fa' sol' la' si' do''16 mib'' mib'' mib'' mib''4:16 |
    fa''2:16-\sug\fp fa'': |
    si'4-\sug\f do'' r2 |
    sol'2:8-\sug\p sol': |
    sol': fad': |
    sol'2
  }
>>
%%
r4 r8 sib'!\f |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
sib'2 r4 r8 <<
  \tag #'primo {
    sib'8 |
    sib'4. sib'8 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    \ficta solb''8 solb''4 solb'' solb'' solb''8 |
    la''8\ff la''4 la'' la'' la''8 |
    sib''2
  }
  \tag #'secondo {
    re'8 |
    re'4. re'8 la4. la8 |
    sib4 re' r2 |
    r r4 r8 <<
      { do''8 | do''2 do''4 do'' | } \\
      { la'8 | la'2 la'4 la' | }
    >>
    mib''8-\sug\ff mib''4 mib'' mib'' mib''8 |
    re''2
  }
>> r2 |
