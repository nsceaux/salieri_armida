\clef "bass" do4\f do do do |
do r r2 |
do4 do do do |
do r r2 |
do4 do do do |
si,2. r8 si, |
sib,!2. r8 sib-! |
lab4-! sol-! fa-! mib-! |
re2 mib |
sol,4 sol, sol, sol,\p |
sol, sol, sol, sol, |
sol, sol\f sol sol,\p |
sol, sol, sol, sol, |
sol, sol\f sol sol,\p |
sol, sol, sol, sol, |
sol, sol\f sol sol,\p |
sol, sol, sol, sol, |
sol, fa!\f fa fa |
mib4. mib8 re4. re8 |
do4. do'8 sib4. sib8 |
lab1~ |
lab2 sol |
lab4. lab8 la4. la8 |
sib4 sib,\p sib, sib, |
sib, sib, sib, sib,(\f |
sib) sib sib, sib,\p |
sib, sib, sib, sib,(\f |
sib) sib sib, sib |
si si si si |
si r r2 |
lab4 lab lab lab |
lab r r2 |
sol4. sol8 fa4. fa8 |
mib4. mib8 lab4. lab8 |
sol2 lab |
sib sib, |
mib r4
%%
r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
\once\slurDashed lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 |
\clef "tenor" <>^\markup { Violoncelli soli } r4 solb'( mib' do') |
la \clef "bass" <>^"Tutti" solb(-\sug\ff mib do) |
sib,2
%%
r |
R1 |
sib,8.\p sib16 lab!8.\trill sol32 lab sol4 r |
R1 |
sib,8. sib16 lab8.\trill sol32 lab sol4 r |
mib4\f r mib'\p r |
mib4\f r mib'\p r |
mib4\f r mib'\p r |
mib r fa\fermata r |
fa1\pp |
mib |
re4 r mib r |
fa2 r |
sib1 |
lab |
sol4 r mib r |
sib,2 r |
si4\f r do' r |
sol r r2 |
si4 r do' r |
sol r lab16\p lab lab lab lab4:16 |
lab2: sol: |
sol16\f sol fad mi re do si, lab, sol,4
%%
r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 | \allowPageTurn
\clef "tenor" <>^\markup { Violoncelli soli } r4 solb'( mib' do') |
la \clef "bass" <>^"Tutti" solb(\ff mib do) |
%%
sib,16 sib\p sib sib sib4:16 sib2: |
lab: lab: |
\ru#12 { sol16 sol lab lab } |
sol sol lab lab sol sol lab lab fa fa sol sol fa fa sol sol |
mib2:16-\sug\ff mib: |
mib8-\sug\p fa16 sol lab sib do' re' mib' re' do' sib lab sol fa mib |
fa2:16-\sug\ff fa: |
fa8\p sol16 la sib do' re' mib' fa' mib' re' do' sib la sol fa |
re2:16\ff re: |
re:\p re: |
re: re: |
mib:\ff mib: |
mib4\fermata r4 r2 |
mib2:\ff mib: |
mib4\fermata r r2 |
mi4\p r mi r |
fa8.\f fa16 lab8. lab16 do'8. do'16 mi'8. mi'16 |
fa'4 r r2 |
fa4-!\f mib-! r2 |
fa4 r r2 |
fa4-!\f mib-! r2 |
mib2:8\p mib: |
lab: lab: |
sol2 r4
%%
r8 sib\f |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
\once\slurDashed lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 |
\clef "tenor" <>^\markup { Violoncelli soli } r4 solb'( mib' do') |
la \clef "bass" <>^"Tutti" solb(\ff mib do) |
sib,2 r |
