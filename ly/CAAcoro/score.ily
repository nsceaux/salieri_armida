\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \oboiInstr  } <<
        { \noHaraKiri s1*47
          \revertNoHaraKiri s1*22
          \noHaraKiri s1*10 }
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniEbInstr  } <<
        { \noHaraKiri s1*47
          \revertNoHaraKiri s1*22
          \noHaraKiri s1*10 }
        \keepWithTag #'corni \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        { \noHaraKiri s1*47
          \revertNoHaraKiri s1*22
          \noHaraKiri s1*10 }
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff <<
          \global \keepWithTag #'primo-secondo \includeNotes "tromboni"
        >>
        \new Staff <<
          \global \keepWithTag #'terzo \includeNotes "tromboni"
        >>
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiriFirst
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with {
        \haraKiriFirst
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Ar.
      } \withLyrics <<
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion {
          s1*36\break
          % coro
          s1*11\break
          % armida
          s1*21\break
          % coro
          s1*11\break
          % armida
          s1*24\break
          % coro
        }
        \origLayout {
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          % 5
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          % 10
          s1*5\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          % 15
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          % 20
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
