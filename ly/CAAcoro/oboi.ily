\clef "treble" R1 |
\tag#'tutti <>^"a 2." r4 do'\f do' do' |
R1 |
r4 re' re' re' |
\twoVoices #'(primo secondo tutti) <<
  { mib'2 fa'4. sol'8 | lab'2. s8 lab' | sol'2. }
  { do'2 re'4. mib'8 | fa'2. s8 fa' | mi'2. }
  { s1 | s2. r8 s | }
>> r8 \tag#'tutti <>^"a 2." sol'8-! |
fa'4-! mib'-! re'-! do'-! |
fa'2 mib' |
sol'2. <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { re''4 |
    mib''4. re''16 mib'' fa''4. mib''16 fa'' |
    sol''4 }
  { si'4 |
    do''4. si'16 do'' re''4. do''16 re'' |
    mib''4 }
>> r4 r2 |
R1 |
r2 r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { re''4 |
    mib''4. re''16 mib'' fa''4. mib''16 fa'' |
    sol''4 }
  { si'?4 |
    do''4. si'16 do'' re''4. do''16 re'' |
    mib''4 }
>> r4 r2 |
R1 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { lab''4 lab'' lab'' |
    sol''4. sol''8 fa''4. fa''8 |
    mib''4. mib''8 mib''4. mib''8 |
    mib''4 }
  { si'4 si' si' |
    do''4. do''8 do''4. si'8 |
    do''4. do''8 reb''4. reb''8 |
    do''4 }
>> r4 r2 |
r2 r4 \twoVoices #'(primo secondo tutti) <<
  { sol''4 |
    fa''4. fa''8 fa''4. fa''8 |
    fa''2 }
  { sib'4 |
    do''4. do''8 do''4. do''8 |
    re''2 }
>> r2 |
r2 r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { sib''4 | \once\slurDashed sib''2( lab''4) }
  { sol'4 | \once\slurDashed sol'2( fa'4) }
>> r4 |
r2 r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { sib''4 |
    \once\slurDashed sib''2( lab''4) lab'' |
    sol''8 re'''4 si'' lab'' sol''16 fa'' |
    mi''4 mi'4 mi' mi' |
    fa'8 do'''4 lab'' sol'' fa''16 mib''! |
    re''4 re' re' re' |
    mib'4. mib''8 lab''4. lab''8 |
    sol''4. sol''8 do'''4. do'''8 |
    sib''2 \tag #'tutti { \voiceTwo <>_\markup\concat { 1 \super o } }
    r4 r16 do''32[ re'' mib'' fa'' sol'' lab''] |
    \tag #'tutti { \voiceOne <>^\markup\concat { 1 \super o } }
    sol''2 fa''\trill |
    mib''
  }
  { sol'4 |
    \once\slurDashed sol'2( fa'4) re'' |
    re'1 |
    dod''4 mi'4 mi' mi' |
    fa'4 do''4 do'' do'' |
    sib' re' re' re' |
    mib'4. sib'8 sib'4. sib'8 |
    sib'4. mib''8 mib''4. mib''8 |
    mib''2  \tag #'tutti { \voiceOne <>^\markup\concat { 2 \super o } }
    do''4. fa''8 |
    \tag #'tutti { \voiceTwo <>_\markup\concat { 2 \super o } }
    mib''2 re'' |
    mib''
  }
>>
%%
r4 \tag#'tutti <>^"a 2." r8 sib' |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
\twoVoices #'(primo secondo tutti) <<
  { sib'2 s4 s8 sib' |
    sib'4. sib'8 mib''4. mib''8 |
    re''4 sib' }
  { re'2 s4 s8 re' |
    re'4. re'8 la'4. la'8 |
    sib'4 re'' }
  { s2 r4 r8 s | }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { solb''1 |
    solb'' |
    la'' |
    sib''2 }
  { R1 |
    do''1 |
    mib''1 |
    re''2 }
  { s1*2 | s1-\sug\ff }
>>
%%
r2 | \allowPageTurn
R1*21 |
r2 r4
%%
\tag#'tutti <>^"a 2." r8 sib' |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
\twoVoices #'(primo secondo tutti) <<
  { sib'2 s4 s8 sib' |
    sib'4. sib'8 mib''4. mib''8 |
    re''4 sib' }
  { re'2 s4 s8 re' |
    re'4. re'8 la'4. la'8 |
    sib'2 }
  { s2 r4 r8 s | }
>>
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 sib' |
    solb''1 |
    solb'' |
    la'' |
    sib''4 }
  { r2 |
    R1 |
    do''1 |
    mib''1 |
    re''4 }
  { s2 | s1*2 | s1-\sug\ff }
>> r4 r2 |
%%
R1*5 | \allowPageTurn
\tag#'tutti <>^"a 2." mib'1-\sug\ff |
sol'4 r r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { la'1 | la'4 }
  { do'1 | do'4 }
>> r4 r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib'1 | sib'4 }
  { re'1 | re'4 }
>> r4 r2 |
R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed sol''1~ | sol''4\fermata }
  { \once\tieDashed sib'1~ | sib'4\fermata }
>> r4 r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed sol''1~ | sol''4 }
  { \once\tieDashed sib'1~ | sib'4 }
>> r4 r2 |
R1 |
\tag#'tutti <>^"a 2." fa'8.\mf fa'16 lab'8. lab'16 do'8. do'16 mi'8. mi'16 |
fa'4 r r2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { lab''4 sol'' sol''2 | lab''4 }
  { si'4 do'' do''2 | do''4 }
>> r4 r2 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { lab''4-! sol''-! }
  { si'4-! do''-! }
>> r2 | \allowPageTurn
R1*2 |
r2 %%
r4 \tag#'tutti <>^"a 2." r8 sib'\f |
mib''2 sib'4. sib'8 |
do''2 sol'4. sol'8 |
lab'4( sol' fa' mib') |
sib'2 r4 r8 \twoVoices #'(primo secondo tutti) <<
  { sib'8 | sib'4. sib'8 mib''4. mib''8 | re''4 sib' }
  { re'8 | re'4. re'8 la'4. la'8 | sib'2 }
>>
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 sib' |
    solb''1 |
    solb'' |
    la'' |
    sib''2 }
  { r2 |
    r2 r4 r8 do'' |
    do''2 do''4 do'' |
    mib''1 |
    re''2 }
  { s2 s1*2 s1-\sug\ff }
>> r2 |
