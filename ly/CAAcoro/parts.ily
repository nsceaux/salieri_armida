\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (oboi #:score-template "score-deux-voix")
   (corni #:score-template "score-deux-voix"
          #:tag-global ()
          #:instrument "Corni in E♭")
   (tromboni #:score-template "score-tromboni-voix")
   (fagotti #:music , #{
   \quoteBassi "CAAbassi"
   s1*48
   <>^\markup\tiny "B."
   \new CueVoice {
     \mmRestInvisible
     sib,8.\p sib16 lab!8.\trill sol32 lab sol4 r |
     \mmRestVisible R1 | \mmRestInvisible
     sib,8. sib16 lab8.\trill sol32 lab sol4 r |
     mib4\f r mib'\p r |
     mib4\f r mib'\p r |
     mib4\f r mib'\p r |
     mib r fa\fermata r |
     fa1\pp |
     mib |
     re4 r mib r |
     fa2 r |
     sib1 |
     lab |
     sol4 r mib r |
     sib,2 r |
     si4\f r do' r |
     sol r r2 |
     si4 r do' r |
     sol r lab16\p lab lab lab lab4:16 |
     lab2: sol: |
     \restInvisible sol16\f sol fad mi re do si, lab, sol,4
     \mmRestVisible\restVisible
   }
   s4 s1*9
   \new CueVoice {
     \restInvisible\mmRestInvisible
     sib,16\p sib sib sib sib4:16 sib2: |
     \restVisible lab: lab: |
     \ru#12 { sol16 sol lab lab } |
     sol sol lab lab sol sol lab lab fa fa sol sol fa fa sol sol |
     mib2:16-\sug\ff mib: | \mmRestVisible
   }
   s1*3
   \new CueVoice {
     \restInvisible\mmRestInvisible
     \voiceOne re2:16\ff re: | \restVisible\oneVoice
     re:\p re: |
     re: re: | \mmRestVisible
   }
   s1*4
   \cue "CAAbassi" { \mmRestInvisible s1 \mmRestVisible }
   s1*5
   \new CueVoice {
     \mmRestInvisible\restInvisible mib2:8\p mib: |
     lab: lab: |
     sol2 \mmRestVisible \restVisible
   }
   #})

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \force-line-width-ratio#0.3 \column {
    \livretPers Coro
    \livretVerse#12 { Chi sorde vi rende }
    \livretVerse#12 { Al magico incanto, }
    \livretVerse#12 { Potenze tremende }
    \livretVerse#12 { De' regni del pianto? }
    \livretPers Armida
    \livretVerse#12 { Son questi, son questi }
    \livretVerse#12 { I carmi possenti, }
    \livretVerse#12 { Per cui di Cocito }
    \livretVerse#12 { Sull' orride sponde }
    \livretVerse#12 { Raddoppio il mugito }
    \livretVerse#12 { De' gridi funesti; }
    \livretVerse#12 { Nè ancor sanguigna, e pallida }
    \livretVerse#12 { Luce coperse il dì }
    \livretVerse#12 { Nè l'arva informa, e squalida }
    \livretVerse#12 { Al suon temuto uscì. }
    \livretVerse#12 { Nè ancor si scuote il Tripode }
    \livretVerse#12 { Nè sull' offerte vittime }
    \livretVerse#12 { La nera fiamma scende! }
  }
  \force-line-width-ratio#0.3 \column {
    \livretPers Coro
    \livretVerse#12 { Chi sorde vi rende }
    \livretVerse#12 { Al magico incanto, }
    \livretVerse#12 { Potenze tremende }
    \livretVerse#12 { De' regni del pianto? }
    \livretPers Armida
    \livretVerse#12 { Gemer dell' antro io sento }
    \livretVerse#12 { Le cupe ampie caverne; }
    \livretVerse#12 { Vedo il baglior de folgori }
    \livretVerse#12 { Odo il muggir del vento, }
    \livretVerse#12 { Par ch' Ecate precipiti }
    \livretVerse#12 { Dalle ragion superne. }
    \livretVerse#12 { Scuoton la reggia a Pluto, }
    \livretVerse#12 { Turbano a stige il corso }
    \livretVerse#12 { Queste mie note orrende. }
    \livretVerse#12 { Freme dall' ombre eterne }
    \livretVerse#12 { L'abitator temuto, }
    \livretVerse#12 { E al mio soccorso intanto }
    \livretVerse#12 { Qual nuovo cenno attende? }
  }
  \force-line-width-ratio#0.3 \column {
    \livretPers Coro
    \livretVerse#12 { Chi sorde vi rende }
    \livretVerse#12 { Al magico incanto, }
    \livretVerse#12 { Potenze tremende }
    \livretVerse#12 { De' regni del pianto? }
  }
} #}))
