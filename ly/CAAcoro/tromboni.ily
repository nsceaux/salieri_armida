<<
  \tag #'(primo primo-secondo) \clef "alto"
  \tag #'secondo \clef "tenor"
  \tag #'terzo \clef "bass"
>>
R1 |
r4 <>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { do'4 do' do' | }
    { sol4 sol sol | }
  >>
  \tag #'terzo { mib4 mib mib | }
>>
R1 |
r4 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { re'4 re' re' |
      mib'2 fa'4. sol'8 |
      lab'2. s8 lab' |
      sol'2. s8 sol'-! |
      fa'4-! mib'-! re'-! do'-! |
      si2 do' |
      sol' }
    { do'4 do' do' |
      mib'2 fa'4. sol'8 |
      fa'2. s8 fa' |
      mi'2. s8 dod'-! |
      do'!4-! sib-! lab-! sol-! |
      sol1 |
      sol2 }
  >>
  \tag #'terzo {
    fa4 fa fa |
    do1 |
    si,2. s8 si, |
    sib,!2. s8 sib-! |
    lab4-! sol-! fa-! mib-! |
    re2 mib |
    sol,
  }
  { s2. | s1 | s2. r8 s | s2. r8 s | }
>> r2 |
R1 |
r4 <>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sol'4 sol' }
    { sol4 sol }
  >>
  \tag #'terzo { sol,4 sol, }
>> r4 |
R1 |
r4 <>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sol'4 sol' }
    { sol4 sol }
  >>
  \tag #'terzo { sol,4 sol, }
>> r4 |
R1 |
r4 <>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sol'4 sol' }
    { sol4 sol }
  >>
  \tag #'terzo { sol,4 sol, }
>> r4 |
R1 |
r4 <>-\sug\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { lab'4 lab' lab' |
      sol'4. sol'8 fa'4. fa'8 |
      mib'4. mib'8 mib'4. mib'8 |
      mib'4 }
    { si4 si si |
      do'4. do'8 do'4. si8 |
      do'4. do'8 reb'4. reb'8 |
      do'4 }
  >>
  \tag #'terzo {
    fa!4 fa fa |
    mib4. mib8 re4. re8 |
    do4. do'8 sib!4. sib8 |
    lab4
  }
>> r4 r2 |
R1*6 |
r2 r4 <>-\sug\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { lab'! |
      sol' sol' lab' fa' |
      s mi' mi' mi' |
      fa'4 }
    { re'4 |
      re' re' re' re' |
      s dod' dod' dod' |
      do'! }
  >>
  \tag #'terzo {
    sib4 |
    si si si si |
    s sol sol sol |
    lab
  }
  { s4 | s1 | r4 s2. | }
>> r4 r2 |
r4 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { re'4 re' re' |
      mib'4. mib'8 lab'4. lab'8 |
      sol'4. sol'8 do''4. do''8 |
      sib'2 lab' |
      sol'2 fa' |
      mib' }
    { sib!4 sib sib |
      sib4. sib8 sib4. sib8 |
      sib4. mib'8 mib'4. mib'8 |
      mib'2 do'4. fa'8 |
      mib'2 re' |
      \sug sib }
  >>
  \tag #'terzo {
    fa4 fa fa |
    sol4. sol8 fa4. fa8 |
    mib4. mib8 lab4. lab8 |
    sol2 lab |
    sib sib, |
    mib
  }
>>
%%
r2 |
R1*6 |
r2 r4 r8 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { mib'8 |
      mib'2 mib'4 mib' |
      solb'1 |
      fa'2 }
    { do'8 |
      do'2 do'4 do' |
      mib'1 |
      re'2 }
  >>
  \tag #'terzo {
    la8 |
    la2 la4 la |
    do'1 |
    sib2
  }
  { s8 | s1 | s-\sug\ff }
>>
%%
r2 | \allowPageTurn
R1*21 |
%%
R1*7 |
r2 r4 r8 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { mib'8 |
      mib'2 mib'4 mib' |
      solb'1 |
      fa'4 }
    { do'8 |
      do'2 do'4 do' |
      mib'1 |
      re'4 }
  >>
  \tag #'terzo {
    la8 |
    la2 la4 la |
    do'1 |
    sib4
  }
  { s8 | s1 | s-\sug\ff }
>> r4 r2 |
%%
R1*24 |
%%
R1*7 |
r2 r4 r8 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { mib'8 |
      mib'2 mib'4 mib' |
      solb'1 |
      fa'2 }
    { do'8 |
      do'2 do'4 do' |
      mib'1 |
      re'2 }
  >>
  \tag #'terzo {
    la8 |
    la2 la4 la |
    do'1 |
    sib2
  }
  { s8 s1 s-\sug\ff }
>> r2 |
