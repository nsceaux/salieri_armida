<<
  \tag #'(voix1 basse) \clef "soprano/treble"
  \tag #'voix2 \clef "alto/treble"
  \tag #'voix3 \clef "tenor/G_8"
  \tag #'voix4 \clef "bass"
  \tag #'armida \clef "soprano/treble"
  R1*36
>>
<<
  \tag #'(voix1 basse) {
    \tag #'basse <>^\markup\character Coro
    r2 r4 r8 sib' |
    mib''2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 sib' |
    sib'2 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    solb''1\melisma |
    la'1\melismaEnd |
    sib'2
  }
  \tag #'voix2 {
    r2 r4 r8 sib' |
    mib'2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 fa' |
    fa'2 solb'4. solb'8 |
    fa'4 fa' r2 |
    r r4 r8 mib' |
    mib'2 mib'4 mib' |
    solb'1 |
    fa'2
  }
  \tag #'voix3 {
    r2 r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 re' |
    re'2 la4. la8 |
    sib4 re' r2 |
    r r4 r8 do' |
    do'2 do'4 do' |
    mib'1 |
    re'2
  }
  \tag #'voix4 {
    r2 r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 sib |
    sib2 dob'4. dob'8 |
    sib4 sib r2 |
    r r4 r8 la |
    la2 la4 la |
    do'1 |
    sib2
  }
  \tag #'armida { R1*10 r2 }
>>
<<
  \tag #'(voix1 voix2 voix3 voix4) { r2 R1*21 r2 }
  \tag #'(armida basse) {
    \tag #'basse { \ffclef "soprano/treble" <>^\markup\character Armida }
    <>^\markup\character Armida r4 r8 sib' |
    sol''!2 fa''4. mib''8 |
    re''2 mib''4 r8 sib' |
    sol''2 fa''4. mib''8 |
    re''2 mib''4 r8 sib' |
    sol''8.[ fa''16] mib''8. re''16 do''8.[ re''16] mib''8. fa''16 |
    sol''8.[ fa''16] mib''8. re''16 do''8.[ re''16] mib''8. fa''16 |
    sol''8.[ fa''16] mib''8. re''16 do''8.[ re''16] mib''8. re''16 |
    do''8.[ sib'16] la'8. sol'16 fa'8 fa'\fermata r8 fa' |
    fa'4. fa'8 fa'4. sol'8 \grace fa'8 mib'2. mib'8 mib' |
    re'4 re'8 re'' do''4 sib' |
    fa'2 r4 r8 fa' |
    sib'4. sib'8 sib'4. do''8 |
    lab' lab' lab'4 r r8 lab' |
    sol'4. mib''8 do''4. la'8 |
    sib'2 r4 sol' |
    sol'' re'' mib'' do'' |
    re''8. si'16 sol'4 r r8 sol' |
    sol''4 re'' mib'' do'' |
    re''8. si'16 sol'8 sol' do''4 re'' |
    mib'' do'' sol''2 |
    sol'2
  }
>>
<<
  \tag #'(voix1 basse) {
    \tag #'basse <>^\markup\character Coro
    r4 r8 sib' |
    mib''2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 sib' |
    sib'2 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    solb''1\melisma |
    la'1\melismaEnd |
    sib'2 r |
    R1*2 |
  }
  \tag #'voix2 {
    r4 r8 sib' |
    mib'2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 fa' |
    fa'2 solb'4. solb'8 |
    fa'4 fa' r2 |
    r r4 r8 mib' |
    mib'2 mib'4 mib' |
    solb'1 |
    fa'2 r |
    R1*2 |
  }
  \tag #'voix3 {
    r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 re' |
    re'2 la4. la8 |
    sib4 re' r2 |
    r r4 r8 do' |
    do'2 do'4 do' |
    mib'1 |
    re'2 r |
    R1*2 |
  }
  \tag #'voix4 {
    r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 sib |
    sib2 dob'4. dob'8 |
    sib4 sib r2 |
    r r4 r8 la |
    la2 la4 la |
    do'1 |
    sib2 r |
    R1*2 |
  }
  \tag #'armida { r2 R1*12 }
>>
<<
  \tag #'(voix1 voix2 voix3 voix4) { R1*22 r2 }
  \tag #'(armida basse) {
    \tag #'basse { \ffclef "soprano/treble" <>^\markup\character Armida }
    re''4 re''8 re'' mib''4 do'' |
    sol''2 sol'4 r8 sol' |
    sol'4 sol' fa' fa'8 fa' |
    mib'4 mib' r2 |
    mib''4 mib''8 mib'' mib''4. do''8 |
    la'8. la'16 la'4 r2 |
    fa''4 fa''8 fa'' fa''4. re''8 |
    sib'!4 sib' r r8 sib' |
    lab''!4. fa''8 fa''4. re''8 |
    sib'8. sib16 sib8 sib'8 sib' re'' fa'' lab'' |
    sol''4 mib'' r2 |
    reb''4\fermata reb''8 reb'' reb''4. sib'8 |
    sol'4 sol' r2 |
    reb''4\fermata reb''8 reb'' reb''4. reb''8 |
    do''4 do''8 reb'' do''8. reb''16 do''8. sib'16 |
    lab'4 fa' r2 |
    lab''4 fa''8. fa''16 re''4 do'' |
    si' do'' r2 |
    lab''4 fa''8. fa''16 re''4. do''8 |
    si'4 do'' r r8 sol' |
    do''4. do''8 do''4. mib''8 |
    do''4 do''8 do'' do'' re'' mib'' do'' |
    si'4 re''
  }
>>
<<
  \tag #'(voix1 basse) {
    \tag #'basse <>^\markup\character Coro
    r4 r8 sib' |
    mib''2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 sib' |
    sib'2 mib''4. mib''8 |
    re''4 sib' r r8 sib' |
    solb''2 solb''4 solb'' |
    solb''1\melisma |
    la'1\melismaEnd |
    sib'2 r |
  }
  \tag #'voix2 {
    r4 r8 sib' |
    mib'2 sib'4. sib'8 |
    do''2 sol'4. sol'8 |
    lab'4\melisma sol'\melismaEnd fa' mib' |
    sib'4 sib' r r8 fa' |
    fa'2 solb'4. solb'8 |
    fa'4 fa' r2 |
    r r4 r8 mib' |
    mib'2 mib'4 mib' |
    solb'1 |
    fa'2 r |
  }
  \tag #'voix3 {
    r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 re' |
    re'2 la4. la8 |
    sib4 re' r2 |
    r r4 r8 do' |
    do'2 do'4 do' |
    mib'1 |
    re'2 r |
  }
  \tag #'voix4 {
    r4 r8 sib |
    mib'2 sib4. sib8 |
    do'2 sol4. sol8 |
    lab4\melisma sol\melismaEnd fa mib |
    sib4 sib r r8 sib |
    sib2 dob'4. dob'8 |
    sib4 sib r2 |
    r r4 r8 la |
    la2 la4 la |
    do'1 |
    sib2 r |
  }
  \tag #'armida { r2 R1*10 }
>>
