\tag #'(voix1 voix2 voix3 voix4 basse) {
  Chi sor -- de vi ren -- de
  al ma -- gi -- co in -- can -- to,
  po -- ten -- ze tre -- men -- de
  de’ re -- gni del pian -- to?
}
\tag #'(armida basse) {
  Son que -- sti, son que -- sti
  i car -- mi pos -- sen -- ti,
  per cui di Co -- ci -- to
  sull’ or -- ri -- de spon -- de
  rad -- dop -- pio il mu -- sgi -- to
  de’ gri -- di fu -- ne -- sti;
  né an -- cor san -- gui -- gna, e pal -- li -- da
  lu -- ce co -- per -- se il dì
  ne lar -- va in -- for -- me, e squal -- li -- da
  al suon te -- mu -- to us -- cì,
  né an -- cor si scuo -- te il tri -- po -- de,
  né sull’ of -- fer -- te vit -- ti -- me
  la ne -- ra fiam -- ma scen -- de!
}
\tag #'(voix1 voix2 voix3 voix4 basse) {
  Chi sor -- de vi ren -- de
  al ma -- gi -- co in -- can -- to,
  po -- ten -- ze tre -- men -- de
  de’ re -- gni del pian -- to?
}
\tag #'(armida basse) {
  Ge -- mer dell’ an -- tro io sen -- to
  le cu -- pe am -- pie ca -- ver -- ne;
  ve -- do il ba -- glior de’ fol -- go -- ri
  o -- do il mu -- gir del ven -- to,
  par che E -- ca -- te pre -- ci -- pi -- ti
  dal -- le re -- gion su -- per -- ne.
  Scuo -- ton la reg -- gia a Plu -- to,
  tur -- ba -- no a sti -- ge il cor -- so
  que -- ste mie no -- te or -- ren -- de
  fre -- me dell’ om -- bre e -- ter -- ne
  l’a -- bi -- ta -- tor te -- mu -- to,
  e al mio soc -- cor -- so in -- tan -- to
  qual nuo -- vo cen -- no at -- ten -- de?
}
\tag #'(voix1 voix2 voix3 voix4 basse) {
  Chi sor -- de vi ren -- de
  al ma -- gi -- co in -- can -- to,
  po -- ten -- ze tre -- men -- de
  de’ re -- gni del pian -- to?
}
