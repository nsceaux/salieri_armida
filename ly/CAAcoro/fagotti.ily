\clef "bass" mib2\f fa4. sol8 |
mib4 r r2 |
fa2 sol4. lab8 |
fa4 r r2 |
sol2 fa4. mib8 |
re2 r4 r8 re |
dod2. r8 sib-! |
lab4-! sol-! fa-! mib-! |
re2 mib |
sol,4 sol, sol, r |
R1 |
\clef "tenor" r2 r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sol'4 |
    fa'4. sol'16 fa' mib'4. fa'16 mib' |
    re'4 }
  { mib'4 |
    re'4. mib'16 re' do'4. re'16 do' |
    si4 }
>> r4 r2 |
R1 |
r2 r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sol'4 |
    fa'4. sol'16 fa' mib'4. fa'16 mib' |
    re'4 }
  { mib'4 |
    re'4. mib'16 re' do'4. re'16 do' |
    si4 }
>> \clef "bass" fa4-\sug\f fa fa |
mib4. mib8 re4. re8 |
do4. do'8 sib!4. sib8 |
lab4 r r2 |
R1*2 |
sib,2 r |
r r4 sib,(-\sug\f |
sib) sib sib, r |
r2 r4 \once\slurDashed sib,(-\sug\f |
sib) sib sib, r |
si si si si |
r dod' dod' dod' |
do'! r r2 |
r4 lab! lab lab |
sol4.sol8 fa4. fa8 |
mib4. mib8 lab4. lab8 |
sol2 lab |
sib sib, |
mib r4
%%
r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
\once\slurDashed lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 |
r4 solb'-.( mib'-. do'-.) |
la-! solb(-\sug\ff mib do) |
sib,2
%%
r2 |
R1*21 |
r2 r4
%%
r8 sib |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 |
r4 solb'-.( mib'-. do'-.) |
la-! solb(-\sug\ff mib do) |
sib, r r2 |
%%
R1*6 |
mib8\p fa16 sol lab sib do' re' mib' re' do' sib lab sol fa mib |
fa4 r r2 |
fa8\p sol16 la sib do' re' mib' fa' mib' re' do' sib la sol fa |
re4 r r2 |
R1*2 |
\once\tieDashed mib1-\sug\ff~ |
mib4\fermata r r2 |
\once\tieDashed mib1-\sug\ff~ |
mib4 r r2 |
R1 |
fa8.\f fa16 lab8. lab16 do'8. do'16 mi'8. mi'16 |
fa'4 r r2 |
fa4-!\f mib-! r2 |
fa4 r r2 |
fa4-!-\sug\f mib-! r2 |
R1*2 |
r2 r4
%%
r8 sib-\sug\f |
mib'2 sib4. sib8 |
do'2 sol4. sol8 |
lab4( sol fa mib) |
sib2 r4 r8 sib |
sib4. sib8 dob'4. dob'8 |
sib2 r |
R1 |
r4 solb'-.( mib'-. do'-.) |
la-! solb(-\sug\ff mib do) |
sib,2 r2 |
