\clef "treble" \transposition mib
R1 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' mi'' | }
  { do''4 do'' do'' | }
>>
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { fa''4 fa'' fa'' |
    mi''2 re''4. do''8 |
    re''2 }
  { re''4 re'' re'' |
    do''2 re''4. do''8 |
    re''2 }
>> r2 |
\tag#'tutti <>^"a 2." sol'2 r |
R1*2 |
mi'4 mi' mi' mi'\p |
mi' mi' mi' mi' |
mi' mi'\f mi' mi'-\sug\p |
mi' mi' mi' mi' |
mi' mi'\f mi' mi'\p |
mi' mi' mi' mi' |
mi' mi'\f mi' mi'\p |
mi' mi' mi' mi' |
mi' re''-\sug\f  re'' re'' |
\twoVoices #'(primo secondo tutti) <<
  { mi''4. mi''8 re''4. re''8 |
    do''4. do''8 do''4. do''8 |
    do''4 }
  { do''4. do''8 re''4. re''8 |
    do''4. do''8 do''4. do''8 |
    do''4 }
>> r4 r2 |
R1*2 |
r4 \tag#'tutti <>^"a 2." sol'\p sol' sol' |
sol' sol' sol' \twoVoices #'(primo secondo tutti) <<
  { mi''4 |
    \once\slurDashed mi''2( re''4) sol' |
    sol' sol' sol' mi'' |
    \once\slurDashed mi''2( re''4) re'' |
    re'' re'' re'' re'' | }
  { sol'4 |
    sol' sol' sol' sol' |
    sol' sol' sol' sol' |
    sol' sol' sol' re'' |
    re'' re'' re'' re'' | }
  { s4\f | s2. s4\p | s2. s4-\sug\f | }
>>
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { sol''4 sol'' sol'' | fa'' }
  { mi''4 mi'' mi'' | re''4 }
>> r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { re''4 re'' re'' | }
  { sol'4 sol' sol' | }
>>
\tag#'tutti <>^"a 2." do''4. sol'8 sol'4. sol'8 |
sol'4. do''8 do''4. do''8 |
do''2 do'' |
\twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' | do'' }
  { do''2 sol' | mi' }
>>
%%
r2 |
R1*6 |
r2 r4 r8 do'' |
do''1~ |
do''-\sug\ff |
\twoVoices #'(primo secondo tutti) <<
  { re''2 }
  { sol' }
>>
%%
r2 | \allowPageTurn
R1*21 |
%%
R1*7
r2 r4 r8 do'' |
do''1~ |
do''\ff |
\twoVoices #'(primo secondo tutti) <<
  { re''4 }
  { sol'4 }
>> r4 r2 |
%%
R1*5 | \allowPageTurn
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sol''1 | sol''4 }
  { mi''1 | do''4 }
>> r4 r2 |
\tag#'tutti <>^"a 2." \once\tieDashed re''1-\sug\ff~ |
re''4 r r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { re''1~ | re''4 }
  { sol'1~ | sol'4 }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed mi''1~ | mi''4\fermata }
  { \once\tieDashed do''1~ | do''4\fermata }
>> r4 r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed mi''1~ | mi''4 }
  { \once\tieDashed do''1~ | do''4 }
>> r4 r2 |
R1 |
\tag#'tutti <>^"a 2." re''2.-\sug\f mi''4 |
re'' r r2 |
re''4-\sug\f do'' \twoVoices #'(primo secondo tutti) <<
  { mi''2 | re''4 }
  { do''2 | re''4 }
>> r4 r2 |
\tag#'tutti <>^"a 2." re''4-!-\sug\f do''-! r2 |
R1*9 |
r2 r4 r8 do'' |
do''1~ |
do''-\sug\ff |
\twoVoices #'(primo secondo tutti) <<
  { re''2 }
  { sol' }
>> r2 |
