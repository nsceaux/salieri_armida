\clef "alto/treble" \tag#'basse <>^\markup\character Ismene
sib'16 sol' sol'8 fa' sol' |
mib'8 mib' r16 mib' sol' fa' sol'8 sol' r sol' |
sib' sib' do'' reb'' reb'' sol' r16 sib' sib' do'' |
lab'8 lab' r do'' do'' do'' do'' fad' |
sol' sol' r4 r2 |
