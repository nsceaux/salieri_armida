\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ismene
      shortInstrumentName = \markup\character Is.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout { s2 s1 s2 \bar "" \break }
    >>
  >>
  \layout { }
  \midi { }
}
