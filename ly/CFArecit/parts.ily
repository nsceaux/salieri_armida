\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Ismene
  \livretVerse#12 { Eccola sventurata. In questo stato }
  \livretVerse#12 { Quell’ empio traditore }
  \livretVerse#12 { Potè lasciarla, e gli el sofferse il cuore? }
} #}))
