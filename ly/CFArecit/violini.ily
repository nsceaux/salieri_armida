\clef "treble" r2 |
R1*2 |
<<
  \tag #'primo {
    lab'4 r do''2 |
    sol' r |
  }
  \tag #'secondo {
    do'4 r fad'2 |
    sol' r |
  }
>>
