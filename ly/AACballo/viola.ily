\clef "alto" sib4\f sib' fa' |
fa'2~ fa'8 r |
sol'4_\markup\italic [dol.] mib' mib' |
fa' mib' re' |
do'2.:8 |
do'4 do' r8 do'-\sug\p |
do' re' do'4 do' |
fa' do' fa |
fa-\sug\pp fa fa |
sol sol sol |
fa' fa' fa' |
fa' fa' r |
mib'-\sug\f re' r |
do' fa' fa'8 r |
mib'4-\sug\p fa'4. mib'8 |
\once\slurDashed mib'2( re'4) |
re'4 r r |
