\clef "treble"
<<
  \tag #'primo {
    <re' sib' fa''>4\f~ fa''8. sol''32 la'' sib''8. re''16 |
    fa''2( mib''8) r |
    re''4_\markup\italic dol. do''8( re'' mib'' fa''16 sol'') |
    la'4.( sib'16 do'' sib'4) |
    do''4.( la''8 \grace sib''16 la''8 sol''16 fa'') |
    mi''8 mi''32( fa'' sol'' la'') sib''4 r8 sol''\p |
    la''16( fa'' re'' sib') la'4. sol'8 |
    sol'2( fa'4) |
    fa''4.\p fa''8( la'' fa'') |
    fa''2( mi''4) |
    mib''!8 do'''8. sib''16([ la'' sib''] la'' sol'' fa'' mib'') |
    \grace fa''8 mib''4( re'') r |
    <mib' sib' sol''>4\f fa''4~ fa''16( sib'' la'' sol'') |
    fa''4.~ fa''32 sol'' fa'' mib'' re''8[ r16 fa'']\p |
    sol''8.\trill la''32 sib'' re''4. do''8 |
    \once\slurDashed do''2( sib'4) |
    sib' r r |
  }
  \tag #'secondo {
    <fa' re''>2-\sug\f re''8. re''16 |
    re''2( do''8) r |
    sib'4_\markup\italic [dol.] sol' sol' |
    do'8. fa'16 fa'4 fa' |
    la'4. do''8 \grace re''16 do''8 sib'16 la' |
    sib'4 <mi' sol>4 r8 do''-\sug\p |
    do''16( la') sib'( sol') fa'4. mi'8 |
    mi'2( fa'4) |
    la4-\sug\pp la la |
    sib sib sib |
    do' do' do' |
    \grace re'8 \once\slurDashed do'4( sib) r |
    sib'-\sug\f sib'~ sib'16 re'' do'' sib' |
    la'4 do'' fa'8[ r16 re'']-\sug\p |
    sib'4 sib'4. la'8 |
    la'2( sib'4) |
    sib'4 r r |
  }
>>
