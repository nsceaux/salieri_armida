\piecePartSpecs
#`((fagotti #:notes "bassi")

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{\markup\tacet #}))
