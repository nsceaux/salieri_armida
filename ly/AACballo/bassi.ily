\clef "bass" sib4-\sug\f sib, sib |
do' la fa |
sol_\markup\italic [dol.] mib do |
fa mib re |
fa8 fa fa fa fa fa |
sol4 do r8 mi\p |
fa sib do'4 do |
fa do fa, |
fa\pp fa fa |
sol sol sol |
la la la |
sib sib, r |
mib4(\f re) r |
do( la,) sib,8 r |
mib4\p fa fa, |
sib, fa sib |
sib, r r |
