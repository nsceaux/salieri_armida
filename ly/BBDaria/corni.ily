\clef "treble" \transposition mib
R1*8 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { mi''1 | re''4 }
  { do''1 | sol'4 }
>> r4 r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { mi''1 | re''4 }
  { do''1 | sol'4 }
>> r4 r2 |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' | do''4 }
  { do''2 sol' | mi'4 }
  { s2-\sug\fp s-\sug\f }
>> r4 r2 |
R1*2 |
\tag#'tutti <>^"a 2." do''1\p |
R1 |
do''1\p~ |
do''2 do''4 r |
R1*2 | \allowPageTurn
do''1-\sug\f |
\twoVoices #'(primo secondo tutti) <<
  { do''4 do'' do'' }
  { do''4 mi' mi' }
>> r4 |
R1*4 |
\tag#'tutti <>^"a 2." re''1-\sug\f~ |
\once\tieDashed re''-\sug\p~ |
re''~ |
re''-\sug\f~ |
re''-\sug\p~ |
re''~ |
re''4 re'' re'' r |
R1*6 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''4 }
  { sol'1~ | sol'4 }
>> r4 r2 |
R1 | \allowPageTurn
r2 \twoVoices #'(primo secondo tutti) <<
  { re''2 |
    sol'' sol'' |
    sol''4 mi'' re''2 | }
  { re''2 |
    sol' sol' |
    sol''4 mi'' re''2 | }
  { s2\p | s1\f | }
>>
R1*2 |
r2 \tag#'tutti <>^"a 2." re''2\f |
sol'~ sol'4 r |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { sol''2 sol' |
    do''2 do''4 do'' |
    do'' }
  { sol'2 sol |
    do'2 do'4 do' |
    do' }
  { s1\p | s\f | }
>> r4 r2 |
R1*9 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { mi''1 | re''4 }
  { do''1 | sol'4 }
>> r4 r2 |
R1 |
\tag#'tutti <>^"a 2." mi''1 |
R1*5 |
\twoVoices #'(primo secondo tutti) <<
  { do''1\p~ |
    do'' |
    do''2 mi'' |
    re'' re'' | }
  { R1*2 |
    r2 do'' |
    do'' sol' | }
>>
\tag#'tutti <>^"a 2." do''1~ |
do''~ |
do''4 r r2 |
R1 |
