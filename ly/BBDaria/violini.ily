\clef "treble"
<<
  \tag #'primo {
    sol'8(\p sib') sol'( sib') sol'( sib') sol'( sib') |
    lab'( mib') sol'( mib') lab'( mib') fa'( lab') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    fa'( sib') fa'( sib') fa'( sib') fa'( sib') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    do'' r do'' r do'' r do'' r |
    sib'8-! sib''(\f lab'' sol'') lab'' fa''( mib'' re'') |
    mib''\p sol''( fa'' mib'') fa'' re''( do'' sib') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    sib'8\f sib''( lab'' sol'') lab'' fa''( mib'' re'') |
    mib''\p sol''( fa'' mib'') fa'' re''( do'' sib') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    r sib'-.(\pp sib'-. sib'-.) r sib'-.( sib'-. sib'-.) |
    r sib'-.( sib'-. sib'-.) r do''8-.( do''-. do''-.) |
    fa'4 r r do''\f |
    sib'8\fp sol'' sol'' sol'' fa''2:8\f |
    mib''4 sib'8. lab'16 \grace lab'4 sol'2 |
  }
  \tag #'secondo {
    R1*2 |
    r8. sib''16\mf sib''8.\trill la''32 sib'' sol''8-! mib''-! sib'-! sib''-! |
    sib''4\p~ sib''( sib') r |
    r8. sib''16\mf sib''8.\trill la''32 sib'' sol''8-! mib''-! sib'-! sol''-! |
    fa''8 r fa'' r fa'' r fa'' r |
    fa'4\f r4 lab'2( |
    sol'8)-\sug\p sib'( lab' sol') lab' fa'( mib' re') |
    mib'( sol') mib'( sol') mib'( sol') mib'( sol') |
    fa'4\f r lab'2 |
    sol'8-\sug\p sib'( lab' sol') lab' fa'( mib' re') |
    mib'( sol') mib'( sol') mib'( sol') mib'( sol') |
    r8 fa'-.(-\sug\pp fa'-. fa'-.) r fa'-.( fa'-. fa'-.) |
    r8 sol'-.( sol'-. sol'-.) r mib'-.( mib'-. mib'-.) fa'4 r r fa'\f |
    sol'8-\sug\fp mib'' mib'' mib'' re''2:8\f |
    mib''4 sol'8. fa'16 \grace fa'4 mib'2 |
  }
>>
<<
  \tag #'primo {
    R1*2 |
    r8. sib''16\mf sib''8.\trill la''32 sib'' sol''8-! mib''-! sib'-! sib''-! |
    sib''2(\p sib'4) r |
    r8. sib''16\mf sib''8.\trill la''32( sib'') sol''8-! mib''-! sib'-! mib''-! |
    do''8\p do'' do'' do'' do''4 r |
    fa''4-! mi''-! fa''-! fa''-! |
    sol''4 r fa' r |
    mib'4.\f re'8 do'( sib do' re') |
    mib'4 sol8. lab16 sol4 r |
    r2 r8 sol''-.( sol''-. sol''-.) |
    lab''4(\rf sol''4.) sol''8-.( sol''-. sol''-.) |
    sol''4\rf( fa''16) sol''( la'' sib'') fa''8 fa'' r4 |
    R1*2 |
    fa'''2.(\p fa''4) |
    sib''2.~ sib''8. la''32 sol'' |
    fa''4 r r2 |
    fa'''2.( fa''4) |
    sib''2.~ sib''8. la''32 sol'' |
    fa''4 \grace sol''16 fa'' mi'' fa''8 \grace sol''16 fa''16 mi'' fa''8 r4 |
    sib'4\p r re'' r |
    mib''2(\rf fa') |
    fa'4 sol' fa' la' |
    la'2( sib'4) r |
    sib'4\p r re'' r |
    mib''2(\rf fa') |
    fa'4 r fa'' r |
    sol''-! do''-! do''-! do''-! |
    mib''8-!\cresc fa''-! sol''-! fad''-! \grace lab''16 sol''(\f fad'') \grace lab'' sol''( fad'') sol'' mib'' re'' do'' |
    sib' re''\p re'' re'' re'' re'' re'' re'' do''2:16 |
  }
  \tag #'secondo {
    sol'8(\p sib') sol'( sib') sol'( sib') sol'( sib') |
    lab'( mib') sol'( mib') lab'( mib') fa'( lab') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    fa'( sib') fa'( sib') fa'( sib') fa'( sib') |
    sol'( sib') sol'( sib') sol'( sib') sol'( sib') |
    mib' mib' mib' mib' mib'4 r |
    do''4-! do''-! do''-! do''8( re'') |
    mib''4 r re' r |
    <sol mib'>4.-\sug\f sol8 \once\slurDashed lab( sol lab lab) |
    sol4 sol8. lab16 sol4 r |
    sol'8-\sug\p( do'') sol'( do'') sol'4 r |
    si'4\rf do''8( mib'') do''4 r |
    la'4\rf sib'8( re'') sib'( re'') sib'( re'') |
    sib'( do'') sib'( do'') sib'( do'') sib'( do'') |
    la'4\f r mib'2 |
    re'8\p fa'( mib' re') mib' do'( sib la) |
    sib8( re') sib( re') sib( re') sib( re') |
    do'4 r mib'!2 |
    re'8 fa'( mib' re') mib' \once\slurDashed do'( sib la) |
    sib( re') sib( re') sib( re') sib( re') |
    do'4 \grace sib'16 la' sol' la'8 \grace sib'16 la' sol' la'8 r4 |
    fa'-\sug\p r fa' r |
    sib'2(-\sug\rf do'') |
    re''4 mib'' re'' do'' |
    do''2( re''4) r |
    fa'4-\sug\p r fa' r |
    sib'2-\sug\rf( do'') |
    re''4 r sib' r |
    sib'-! sol'-! sol'-! sol'-! |
    do''8-\sug\cresc do'' do'' do'' do''16-\sug\f do'' do'' do'' do'' do'' do'' do'' |
    re'' sib'\p sib' sib' sib'4:16 sib': la': |
  }
>>
sib'4\f fa''~ fa''16 mib''( re'' mib'') fa'' sol'' la'' sib'' |
<<
  \tag #'primo {
    fad''16 sol'' re'' mib'' si'32([ do'' re'' mib'']) fa''([ sol'' fa'' mib'']) re''4 do'' |
  }
  \tag #'secondo {
    sib'4 sol'8. la'16 sib'4 la' |
  }
>>
sib'1\p~ |
sib'4 r r2 |
<<
  \tag #'primo {
    re''8-\sug\p re'' re'' re'' do''2:8\f |
    sib'4 fa'8. mib'16 re'4 r |
    do''4\p r do'' r |
    re'' r re'' r |
    fa'' r lab'! r |
  }
  \tag #'secondo {
    sib'2:8-\sug\p la':-\sug\f |
    sib'4 re'8. do'16 sib4 r |
    mib'8(-\sug\p sol') mib'( sol') mib'( sol') mib'( sol') |
    fa'( lab'!) fa'( lab') fa'( lab') fa'( lab') |
    fa'( lab') fa'( lab') fa'( lab') fa'( lab') |
  }
>>
sol'4.\f sol'8 lab'( sol' lab' fa') |
<<
  \tag #'primo {
    sol'4-\sug\p r r2 |
    R1 |
    r8. sib''16\mf sib''8.\trill la''32 sib'' sol''8-! mib''-! sib'-! sol'-! |
    do'''1\p~ |
    do''' |
    r8. do'''16 do'''8.\trill si''32 do''' lab''8-! fa''-! do''-! lab'-! |
    reb''4 r mib'' r |
    mib'' r fa'' r |
    fa''-\sug\f r r2 |
    sib''1\p |
    mib'''2. mib'''8. re'''32 do''' |
    sib''4 r r2 |
    sib'1 |
    r8. sib''16 sib''8.\trill la''32 sib'' sol''8 mib'' sib' sol' |
    r4 do'' r re'' |
    r mib'' r fa'' |
    sol''1 |
    do''4\mf do'' do''8( lab') lab'( fa') |
    sol'4 r fa' r |
    r r8. sib''16\p \grace do''' sib''32 la'' sib''8. r4 |
    r r8. lab''!16 \grace sib''16 lab''32 sol'' lab''8. r4 |
    r r8. sol''16 \grace lab'' sol''32 fad'' sol''8. r4 |
    r r8. fa''16 \grace sol'' fa''32 mi'' fa''8. r4 |
    mib''4 r r2 |
    R1*3 |
  }
  \tag #'secondo {
    sol'8(-\sug\p sib') sol'( sib') sol'( sib') sol'( sib') |
    lab'8( mib') sol'( mib') lab'( mib') fa'( lab') |
    sol'( sib') sol'( sib') sol' sib' sol' sib' |
    sol'16.( lab'32 sol'16. lab'32 sol'16. lab'32 sol'16. lab'32) sol'2 |
    sol'16.( lab'32 sol'16. lab'32 sol'16. lab'32 sol'16. lab'32) sol'2 |
    lab'8( do'') lab'( do'') lab'( do'') lab'( do'') |
    lab'( reb'') lab'( reb'') sib'( reb'') sib'( reb'') |
    lab'( do'') do''( mib'') do'' mib'' do'' mib'' |
    re''4-\sug\f r lab'!2 |
    sol'8\p sib'( lab' sol') lab' fa'( mib' re') |
    sol' sib' sol' sib' sol' sib' sol' sib' |
    fa'4 r lab'2 |
    sol'8 sib'( lab' sol') fa' lab'( sol' fa') |
    mib'4 r sib' r |
    r mib' r fa' |
    r sol' r sib' |
    sib' r r2 |
    mib'4-\sug\mf mib' fa' fa'8( re') |
    mib'4 r re' r |
    mib'8-\sug\p( sol') mib'( sol') mib'( sol') mib'( sol') |
    mib'( lab') mib'( lab') fa'( sib') fa'( sib') |
    sol'( sib') sol'( sib') mib'( sol') mib'( sol') |
    mib' fa' mib' fa' re' fa' re' fa' |
    mib'( sol) sib( sol) lab( do') lab( sib) |
    sol4 r r2 |
    R1*2 |
  }
>>
