Vie -- ni a me sull’ a -- li d’o -- ro
lu -- sin -- ghier so -- gno a -- mo -- ro -- so,
lu -- sin -- ghier __ so -- gno a -- mo -- ro -- so,
in -- gan -- nan -- do il mio ri -- po -- so
il mio ri -- po -- so
in sem -- bian -- za del mio ben.
Vie -- ni a me sull’ a -- li d’o -- ro
lu -- sin -- ghier so -- gno a -- mo -- ro -- so,
in -- gan -- nan -- do il mio ri -- po -- so
in sem -- bian -- za del mio ben,
in -- gan -- nan -- do il mio ri -- po -- so
in sem -- bian -- za del mio ben.

Tro -- vi in te per po -- chi is -- tan -- ti
il mio cor qual -- che ri -- sto -- ro,
fin -- che a -- mor del mio te -- so -- ro
fac -- cia poi sve -- gliar -- mi in sen,
fin -- che a -- mor del mio __ te -- so -- ro
del mio __ te -- so -- ro
fac -- cia poi sve -- gliar -- mi in sen,
fac -- cia poi sve -- gliar -- mi in sen,
sve -- gliar -- mi in sen,
sve -- gliar -- mi in sen.
