\clef "soprano/treble" R1*17 |
mib''2. re''4 |
do'' sib' do'' re'' |
\grace fa''16 mib''8[ re''16 do''] sib'4 r sib'8 sol'' |
fa''2~ \override TupletBracket.direction = #UP \tuplet 3/2 { fa''8 mib''([ re'']) } \tuplet 3/2 { do''([ sib']) lab' } |
sol'8.[ lab'16] sib'4 r sib'8 mib'' |
\grace re''4 do''2~ do''16[\melisma re'' mib'' re''] mib''[ mi'' fa'' mi''] |
fa''8 do''4 mi''8 fa''\turn[ lab'' sol'' fa'']\melismaEnd |
mib''8 sib'4.~ sib'8[ do''16 sib'] \grace sib'16 lab'8[ sol'16 fa'] |
mib'4 mib' r2 |
r r4 mib''8. re''16 |
do''4 do''4. mib''8 re'' do'' |
si'4 do''4. mib''8 \grace re''16 do''8 \ficta sib' |
la'4 sib' r sib'8 re'' |
do''4 do'' do''8[ re''16 do''] \grace do''16 sib'8[ \ficta la'16 sib'] |
la'4 r4 r2 |
fa''2. fa'4 |
sib'4 sib' sib'4. do''16[ re''] |
do''4 do'' r2 |
fa''2. fa'4 |
sib'4 sib' sib'4. do''16[ re''] |
do''4 do'' r fa'8 fa' |
\tuplet 3/2 { sib'8[ re'' do''] } \tuplet 3/2 sib'4.~ \tuplet 3/2 { sib'8 sib'[ do''] } \tuplet 3/2 { re''[ mib''] fa'' } |
\tuplet 3/2 { fa''8[ re'' mib''] } mib''4 r \tuplet 3/2 { do''8[ re''] mib'' } |
\grace mib'' \tuplet 3/2 { re''[ do'' re''] } \grace fa'' \tuplet 3/2 { mib''8[ re'' do''] } sib'4. do''8 |
re''2 r4 fa'8 fa' |
\tuplet 3/2 { sib'8[ re'' do''] } \tuplet 3/2 sib'4.~ \tuplet 3/2 { sib'8 sib'[ do''] } \tuplet 3/2 { re''[ mib''] fa'' } |
\tuplet 3/2 { fa''[ re'' mib''] } mib''4 r \tuplet 3/2 { do''8[ re''] mib'' } |
re''4\melisma fa''~ fa''16([ mib'' re'' mib'']) fa''([ sol'' lab''! lab']) |
sol'[ la' sib' si'] do''[ si' do'' re''] mib''[ si' do'' re''] mib''[ si' do'' re''] |
mib''[ re'' mib'' fa''] sol''[ fad'' sol'' fad''] sol''[ fad'' sol'' fad'']\melismaEnd sol''[ mib'' re'' do''] |
sib'2 do'' |
sib'4 r r2 |
R1*4 |
r2 r4 sib'8 sib' |
do''4 do'' do''4. re''16[ mib''] |
\grace mib''8 re''4 re'' r re''8 mib'' |
\omit TupletBracket
fa''2~ \tuplet 3/2 { fa''8 re''[ do''] } \tuplet 3/2 { do''[ sib'] lab'! } |
sol'4 sol' r2 |
mib''2. re''4 |
do''4 sib' do'' re'' |
\grace fa''8 mib''[ re''16 do''] sib'4 r sib'8 reb'' |
do''2. reb''16.([ do''32 reb''16. do''32]) |
sib'2. do''16.[ sib'32 do''16. sib'32] |
lab'!2 r4 do''8 do'' |
reb''4 reb''4. mib''16[ fa'']\melisma mib''8\melismaEnd reb'' |
reb'' do''4 mib''8 mib''[\melisma fa''16 sol''] fa''8\melismaEnd mib'' |
mib'' re''4. r2 |
sib'2. sib'4 |
mib''4 mib'' mib''8[ sol''16 fa''] \grace lab'' sol''8[ fa''16 mib''] |
\grace mib''8 re''4 r r2 |
sib'2. do''8[ re''] |
\grace fa'' mib''4 r r reb'' |
do''4~ do''16[\melisma mib'' re'' mib''] re''4~ re''16[ fa'' mib'' fa''] |
mib''4~ mib''16[ sol'' fa'' sol''] fa''4~ fa''16[ lab'' sol'' lab''] |
sol''8[ fa'' mib'' re''] do''[ sib' lab' sol'] |
do''8 mib''4 do'' lab'8 \grace sib' lab'[ sol'16 fa'] |
sol'2\melismaEnd fa' |
mib' r4 sib' |
sib'8( lab'4.) r4 lab' |
lab'8( sol'4.) r4 sol' |
sol'8( fa'4.) r4 fa' |
mib'2 r |
R1*3 |
