\clef "alto" mib'8\p( sol') mib'( sol') mib'( sol') mib'( sol') |
mib'1 |
mib'8( sol') mib'( sol') mib'( sol') mib'( sol') |
re'( fa') re'( fa') re'( fa') re'( fa') |
mib'( sol') mib'( sol') mib'( sol') mib'( sol') |
lab8 r lab r lab r lab r |
sib2\f lab'( |
sol'8)-\sug\p sib'( lab' sol') lab' fa'( mib' re') |
mib'4 <<
  { sol'2. | fa'4 } \\
  { mib'2. | re'4-\sug\f }
>> r4 lab'2 |
\slurDashed sol'8-\sug\p sib'( lab' sol') lab' fa'( mib' re') | \slurSolid
mib'4 <<
  { sol'2. | fa'8 \slurDashed re'-.( re'-. re'-.) } \\
  { mib'2. | re'8\pp \slurDashed sib-.( sib-. sib-.) }
>> \slurDashed r8 re'(-. re'-. re'-.) |
r mib'(-. mib'-. mib')-. r lab(-. lab-. lab)-. | \slurSolid
lab4 r r lab\f |
sib4\fp sib sib\f sib |
mib' r mib r |
mib'8(-\sug\p sol') mib'( sol') mib'( sol') mib'( sol') |
mib'1 |
mib'8( sol') mib'( sol') mib'( sol') mib'( sol') |
re'( fa') re'( fa') re'( fa') re'( fa') |
mib'( sol') mib'( sol') mib'( sol') mib'( sol') |
lab' lab' lab' lab' lab'4 r |
lab4-! lab-! lab-! lab-! |
sib4 r sib r |
sol2-\sug\f lab8( sol lab sib) |
sib4 sib sib r |
mib'8\p( sol') mib'( sol') mib'4 r8 mib'( |
re'4)\rf mib'8( sol') mib'4 r8 re'( |
do'4) re'8( fa') re'( fa') re'( fa') |
sol'4 r sol' r |
fa'2\f mib'! |
re'8-\sug\p fa'( mib' re') mib' do'( sib la) |
re8( fa) re( fa) re( fa) re( fa) |
fa2 mib'! |
re'8 fa'( mib' re') mib' \once\slurDashed do'( sib la) |
re( fa) re( fa) re( fa) re( fa) |
la4 r r2 |
re'4-\sug\p r sib r |
sol2(-\sug\rf la) |
sib4 mib' fa' fa |
sib2 r |
re'4-\sug\p r sib r |
\once\slurDashed sol2-\sug\rf( la) |
sib4 r re' r |
mib'-! mib'-! mib'-! mib'-! |
mib'8\cresc mib' mib' mib' mib'2:8\f |
fa':\p fa: |
sib4\f re' re' re' |
mib' mib' fa' fa |
r re'\p re' re' |
mib' r r2 |
fa'4-\sug\p fa' fa-\sug\f fa |
sib sib sib r |
sib'1-\sug\p |
lab' |
re' |
mib'4.-\sug\f re'8 do' sib do' re' |
\slurDashed mib'(-\sug\p sol') mib'( sol') mib'( sol') mib'( sol') | \slurSolid
mib'1 |
mib'8( sol') mib'( sol') mib'( sol') mib'( sol') |
\ru#2 { mi'16.( fa'32 mi'16. fa'32 mi'16. fa'32 mi'16. fa'32) mi'2 | }
fa'8( lab') fa'( lab') fa'( lab') fa'( lab') |
fa'( lab') fa'( lab') sol'( sib') sol'( sib') |
mib' lab' lab' do'' la' do'' la' do'' |
sib'2\f lab'! |
sol'8\p sib'( lab' sol') lab' fa'( mib' re') |
mib' sol' mib' sol' mib' sol' mib' sol' |
fa'4 r lab'2 |
\slurDashed sol'8 sib'( lab' sol') fa' lab'( sol' fa') | \slurSolid
mib'4 r mib' r |
r lab r sib' |
r sib' r re' |
mib'1 |
lab4-\sug\mf lab lab lab |
sib r sib r |
sol8(-\sug\p sib) sol( sib) sol( sib) sol( sib) |
do' mib' do' mib' re' fa' re' fa' |
mib' sol' mib' sol' do'( mib') do'( mib') |
do'-. do'-. do'-. do'-. sib re' sib re' |
mib'4 r r2 |
r8 mib sol( mib) fa( lab) fa( re) |
mib2 r |
R1 |
