\clef "treble"
<<
  \tag #'(primo tutti) {
    \tag #'tutti <>^\markup\concat { 1 \super o }
    <>_\markup\italic Dolce mib''2. re''4 |
    do''( sib' do'' re'') |
    \grace fa''8 mib'' re''16 do'' sib'4 r sib'8 sol'' |
    fa''2~ fa''8*2/3[ mib'' re''] do''[ sib' lab'] |
    sol'8. lab'16 sib'4 r mib''8-. sol''-. |
    fa''16-! fa''( sol'' mi'') fa''-! fa''( sol'' mi'') fa''( sol'' lab'' sol'') lab'' sib'' do''' \ficta mib'' |
  }
  \tag #'secondo R1*6
>>
\twoVoices #'(primo secondo tutti) <<
  { re''4 r r2 |
    sib''2. sib'4 |
    mib'' mib''~ mib''16( fa'' sol'' fa'') \grace lab''16 sol''8 fa''16 mib'' |
    re''4 r r2 |
    sib''2. sib'4 |
    mib'' mib''~ mib''16 fa'' sol'' fa'' \grace lab'' sol''8 fa''16 mib'' |
    re''2 lab'' |
    sol'' do''' |
    fa''4 lab''8 sol'' do''' lab'' sol'' fa'' |
    mib''8 sib''4 lab''32 sol'' fa'' mib'' fa''2\trill |
    mib''4 }
  { sib'1-\sug\f~ |
    sib'-\sug\p~ |
    sib'~ |
    sib'\f~ |
    sib'\p~ |
    sib' |
    sib'4-\sug\pp r r2 |
    R1*2 |
    mib''2-\sug\fp re''-\sug\f |
    mib''4 }
>> r4 r2 |
R1*14 |
\twoVoices #'(primo secondo tutti) <<
  { r8 fa''( mib''! re'') mib'' do''( sib' la'!) |
    sib' re''( do'' sib') do'' la'( sol' fa') |
    sib'4 r r2 |
    r8 fa''( mib'' re'') mib'' do''( sib' la') |
    sib' re''( do'' sib') do'' la' sol' fa' |
    sib'4 r r2 |
    s4 fa''4^\markup\concat { \musicglyph "scripts.turn" \natural }
    fa''^\markup\concat { \musicglyph "scripts.turn" \natural } s | }
  { fa''1~ |
    fa''~ |
    fa''~ |
    fa''~ |
    fa''~ |
    fa'' |
    s4 la' la' s | }
  { s1\f | s\p | s |
    s1\f | s\p | s |
    r4 s s r | }
>> \allowPageTurn
R1*9
\twoVoices #'(primo secondo tutti) <<
  { re''2 do'' | sib'4 }
  { sib'2 la' | sib'4 }
  { s1\p | s4-\sug\f }
>> r4 r2 |
R1 |
\override Staff.AccidentalSuggestion.avoid-slur = #'outside
\twoVoices #'(primo secondo tutti) <<
  { <>_\markup\italic Dolce lab''!2 sol''4 fad'' |
    fad''32( sol''16.) la''32( sib''16.) la''32( sol''16.) fad''!32( sol''16.) re''32( mib''16.) si'32( do''16.) re''32( mib''16.) \ficta fa''32( sol''16.) |
    fa''8 sib''~ sib''16 fa''( mib'' re'') do''2 |
    sib'4 }
  { sib'1\p~ |
    sib'4 r r2 |
    sib'2\p la'\f |
    sib'4 }
>> r4 r2 |
R1*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mib''4. re''8 do''( sib' do'' re'') | mib''4 }
  { sol'4. sol'8 lab'( sol' lab' sib') | sib'4 }
>> r4 r2 | \allowPageTurn
R1*7 |
\twoVoices #'(primo secondo tutti) <<
  { r8 sib''( lab'' sol'') lab'' fa''( mib'' re'') |
    mib'' sol''( fa'' mib'') fa'' re''( do'' sib') |
    mib''4 r r2 |
    r8 sib''( lab'' sol'') lab'' fa''( mib'' re'') |
    mib'' sol''( fa'' mib'') re'' fa''( mib'' re'') |
    mib''1 | }
  { \tieDashed sib''1~ |
    sib''~ |
    \tieSolid sib''~ |
    sib'' |
    sib'~ |
    sib' }
  { s1\f | s\p | }
>>
R1*5 |
r2 <>\p \twoVoices #'(primo secondo tutti) <<
  { sol'2( | lab' sib') |
    sol' sol' |
    fa' fa' |
    mib'1~ |
    mib'4 }
  { mib'2( |
    mib' fa') |
    mib' mib' |
    mib' re' |
    mib'1~ |
    mib'4 }
>> r4 r2 |
R1*2 |
