\piecePartSpecs
#`((oboi #:score-template "score-deux-voix"
         #:music , #{ <>^\markup\concat { 1 \super o } #})
   (corni #:score-template "score-voix"
          #:tag-global ()
          #:instrument "Corni in Eb")
   (fagotti #:music , #{
\quoteBassi "BBBbassi"
<>^\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s1*6 \mmRestVisible }
s1*6 s2
<>^\markup\tiny "B."
\cue "BBBbassi" { \restInvisible s2 \restVisible\mmRestInvisible s1*2 \mmRestVisible }
s1 s2
<>^\markup\tiny "B."
\cue "BBBbassi" { \restInvisible s2 \restVisible\mmRestInvisible s1*8 \mmRestVisible }
s1*2
<>^\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s1*6 \mmRestVisible }
s1*5
<>^\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s1*9 \mmRestVisible }
s1*3
<>^\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s1*2 \mmRestVisible }
s1*2
<>^\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s1*3 \mmRestVisible }
s1 s2
<>^\markup\tiny "B."
\cue "BBBbassi" { \restInvisible s2 \restVisible\mmRestInvisible s1*7 \mmRestVisible }
s1*5 s2
<>^\markup\tiny "B."
\cue "BBBbassi" { \restInvisible\mmRestInvisible s2 s1*5 s2 \mmRestVisible\restVisible }

    #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#10 { Vieni à me sull' ali d'oro }
  \livretVerse#10 { Lusinghier sogno amoroso, }
  \livretVerse#10 { Ingannando il mio riposo }
  \livretVerse#10 { In sembianza del mio ben. }
  \livretVerse#10 { Trovi in te per pochi istanti }
  \livretVerse#10 { Il mio cor qualche ristoro, }
  \livretVerse#10 { Finche amor del mio tesoro }
  \livretVerse#10 { Faccia poi svegliarmi in sen. }
} #}))

