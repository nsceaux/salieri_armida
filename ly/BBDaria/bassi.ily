\clef "bass" mib4-\sug\p r mib r |
mib r mib r |
mib r mib, r |
re r sib, r |
mib r mib, r |
lab lab lab lab |
\once\tieDashed sib1\f~ |
sib\p~ |
sib~ |
sib\f~ |
sib\p~ |
sib |
sib4\pp r re r |
mib r lab r |
lab, r r lab\f |
sib\fp sib sib,\f sib, |
mib r mib, r |
mib\p r mib r |
mib r mib r |
mib r mib, r |
re r sib, r |
mib r sol r |
lab r lab, r |
lab-! lab-! lab,-! lab,-! |
sib r sib, r |
mib1-\sug\f |
mib4 mib mib, r |
mib-\sug\p r mib r |
fa\rf mib mib, r |
mib(\rf re) re, r |
mib r mi r |
fa1\f~ |
fa\p~ |
fa~ |
fa~ |
fa~ |
\once\tieDashed fa~ |
fa4 fa fa, r |
re\p r sib, r |
sol2\rf la |
sib4 mib fa fa, |
sib,2 r |
re4-\sug\p r sib, r |
sol2(\rf la) |
sib4 r re r |
mib-! mib-! mib-! mib-! |
mib\cresc mib mib8\f mib mib mib |
fa2:8\fp fa,: |
sib,4\f sib sib re |
mib mib fa fa, |
r re-!(\p re-! re-!) |
mib4 r r2 |
fa4\p fa fa\f fa, |
sib, sib sib, r |
sib\p r sib, r |
sib r sib, r |
sib r sib, r |
mib1\f |
mib4\p r mib r |
mib r mib r |
mib r mib r |
mi r r2 |
mi4 r r2 |
fa4 r fa, r |
fa r sol r |
lab! r la r |
<<
  { sib1^"Violoncel." ~ |
    sib~ |
    sib~ |
    sib~ |
    sib2 } \\
  { sib,1_"Bassi"\f~ |
    sib,\p~ |
    sib,~ |
    sib,~ |
    sib,2 }
>> lab!2 |
sol4 r sol, r |
lab, r lab r |
sol r re r |
mib1 |
lab4\mf lab lab lab |
sib r sib, r |
mib1\p~ |
mib |
mib2 do |
lab, sib, |
mib1~ |
mib~ |
mib2_\markup\italic calando mib~ |
mib r |
