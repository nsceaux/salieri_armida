\score {
  <<
    \new ChoirStaff <<
      \new GrandStaff \with { \oboiInstr  } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new Staff \with { \corniEbInstr  } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Rinaldo
      shortInstrumentName = \markup\character Rin.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*4\pageBreak
        s1*5\pageBreak
        s1*6\pageBreak
        s1*5\pageBreak
        %% 5
        s1*4\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        %% 10
        s1*4\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        %% 15
        s1*4\pageBreak
        s1*4\pageBreak
        \grace s8 s1*4\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
