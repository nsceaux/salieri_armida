\clef "bass" R1*6 |
sib1\f~ |
sib-\sug\p~ |
sib~ |
sib\f~ |
sib\p~ |
sib~ |
sib4-\sug\pp r r2 |
R1*2 |
sib4\fp sib sib,\f sib, |
mib4 r r2 |
R1*8 |
mib1-\sug\f |
mib4 mib mib, r |
R1*6 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { re'1 | do'4 }
  { sib1 | la4 }
>> r4 r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { re'1 | do'4 do' do' }
  { sib1 | la4 la la }
>> r4 |
R1*9 |
fa2:8-\sug\p fa,: |
sib,4\f sib sib sib |
mib mib fa fa, |
R1*2 |
fa4\p fa fa\f fa, |
sib, sib sib, r |
R1*3 |
mib'4.-\sug\f re'8 do' sib do' re' |
mib'4 r r2 |
R1*7 |
\once\tieDashed sib1-\sug\f~ |
sib-\sug\p~ |
sib~ |
\once\tieDashed sib~ |
sib2 lab! |
sol4 r r2 |
R1*5 |
r2 sib-\sug\p |
do' re' |
mib' do' |
lab sib |
mib1~ |
\once\tieDashed mib~ |
mib2 r |
R1 |
