\ffclef "soprano/treble" <>^\markup\character Armida
R1*4 |
r4 do''16 la' la'8 r4 r8 do'' |
do'' fa' r4 r2 |
R1*2 |
r8 mib'' do''4 r8 do'' sib' do'' |
lab'8 lab' r4 r2 |
R1 |
r4 r8 mib' lab' lab' lab' sib' |
do''8 do'' r16 do'' sib' do'' lab'4 r |
r2 r4 r8 do'' |
do'' sol' r sol' sol' sol' r sol' |
mi'! mi' r4 r8 sol' sol' fa' |
sol' sol' r16 sol' sol' lab' sib'8 sib' r sib' |
\ficta sib'4 do''8 reb'' do'' do'' r4 |
do'' r8 do'' lab' lab' r4 |
r r8 reb'' sib' sib' sib' lab' |
fa' fa' r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
la8 la16 la sol8 la fa fa |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
do''4 do''8 do'' la' la' r la'16 sib' |
do''4 do''8 re'' sib' sib' r4 |
\ffclef "soprano/treble" <>^\markup\character Armida
fa''16 re'' re''8 r re'' sib'8 sib' r sib' sib' sib' sib' do'' |
re'' re'' r re''16 fa'' re''4 do''8 sib' |
mib''4 mib''8 re''16 do'' si'!8 si' r si'16 si' |
si'4 si'8 do'' \grace do'' re''4 fa''8 fa'' |
r sol''16 re'' mib''8 mib'' r4
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
do'8 sol16 sol |
\grace fa8 mib4 r
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r8 la' do'' sib' |
sol'8 sol' r4 r r8 sib' |
sol' sol' r16 sib' sib' mib'' mib''8 sib' r do'' |
la' la' r4 do''8 do''16 do'' do''8 re'' |
sib'4 r8 sib' re'' re'' r fa'' |
re'' re'' r re''16 re'' re''4 do''8 re'' |
sib' sib' r4 lab' lab'8 lab' |
sol' sol' r re''16 mib'' fa''4 re''8 mib'' |
do''4 do'' r8 do'' do'' do'' |
reb''8 reb'' r reb'' reb'' sib' sib' sib' |
sol'8 sol' r reb''16 fa'' reb''4 reb''8 do'' |
lab'4 r lab'8 lab' lab' sib' |
do'' do'' r do'' do'' do'' sib' do'' |
lab' lab' r sib'16 do'' do''8 sol' r sol'16 lab' |
sib'8 sib' r16 sib' sib' do'' lab'8 lab' r4 |
do''4 r16 do'' do'' fa'' fa''8 do'' r re'' |
si'! si' re'' re''16 mib'' fa''8 re'' r mib''! |
do''4 sol'8 sol'16 sol' do''8 do'' re'' mib'' |
re'' re'' r la' la' la' la' la' |
fad' fad' r la'16 sib' do''8 la' r la'16 sib' |
sol'4 sol'8 sol'16 la' sib'8 sib' sib' sib'16 do'' |
re''8 re'' r16 re'' re'' mi''! dod''8 dod'' r la' |
dod'' dod'' r16 dod'' dod'' re'' mi''8 mi'' r mi'' |
mi''8 mi'' fa'' sol'' sol'' dod'' r dod''16 re'' |
mi''8 mi'' r \ficta dod''16 la' re''4 r |
\ffclef "soprano/treble" <>^\markup\character Armida
fa''16 re'' re''8 r re'' sib' sib' r4 |
\ficta sib'8 sib' r do'' re'' re'' mib'' fa'' |
fa'' sib' r16 fa' sib' la' sib'8 sib' r sib'16 do'' |
re''4 do''8 sib' la' la' r16 la' la' sib' |
\grace re''8 do''4 mib''8. mib''16 do''8 do'' r do'' |
do'' do'' do'' do'' la' la' r la' |
la' la' la' sib' do'' do'' r16 la' do'' sib' |
sol'8 sol' r sol'' sol'' re'' re'' re'' |
si'! si' r si' si' si' la' si' |
sol' sol' r16 sol' si' la' si'8 si' r16 si' si' do'' |
re''8 re'' do'' re'' si'4 r8 sol' |
re''8 re'' r re''16 re'' re''4 re''8 mib'' |
fa'' re'' r16 re'' fa'' mib'' do''8 do'' r mib''16 do'' |
fad''4 fad''8 sol'' sol'' re'' r4 |
r2 r8 mi'' mi'' mi'' |
mi'' si' r si'16 si' si'4 si'8 do'' |
re''8 re'' r16 re'' re'' mi'' do''8 do'' r la' |
do'' do'' si' la' red''4 fad''8 red''16 mi'' |
mi''8 si' r4 r2 |
fa''4 re''8 re'' \grace do''8 si'4. la'8 |
sold'8 sold' r si'16 do'' re''4 si'8 do'' |
la' la' r4 r2 |
R1 |
re''4 mi''8 fa'' \grace mi'' re''4 r8 re''16 re'' |
re''4 do''8 re'' si' si' si' si'16 do'' |
\grace mi''8 re''4 re''8 re''16 re'' mi''8 fa'' \grace do''8 si'4 |
r8 re'' si' sol' do'' do'' r4 |
R1 |
r2 mi''4 re''8 mi'' |
do''4 r8 sol' do'' do'' do'' si' |
do'' do'' r16 do'' do'' re'' mi''8 mi'' r sol'' |
sol'' sib' r re'' sib' sib' sib' do'' |
la'4 r16 do'' do'' fa'' fa''8 do'' r do''16 do'' |
do''4 sib'8 do'' la' la' r16 la' do'' sib' |
do''8 do'' r do'' mib'' mib'' mib'' fa'' |
re'' re'' r16 re'' re'' mi''! dod''8 dod'' r la'16 si' |
\ficta dod''4 dod''8 re'' re'' la' r4 |
R1 |
re''4 mi''!8 fa'' fa'' si' r si'16 do'' |
re''4 re''8 mib'' fa'' fa'' r16 re'' fa'' mib'' |
do''8 do'' r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r2 r8 sol do' si |
do' do' r4 r2 |
\ffclef "soprano/treble" <>^\markup\character Armida
r2 do''4 do''8 mib'' |
do'' do''
\ffclef "soprano/treble" <>^\markup\character Rinaldo
re''4 si'!8 si' r si'16 do'' |
re''8 re'' r sol' do'' do'' r16 do'' mi'' sol'' |
sol''8 do'' r sol'16 la' sib'4 do''8 sol' |
la'8 la' r4 do'' do''8 fa'' |
fa'' do'' r16 do'' do'' sib' do''8 do'' r do'' |

la'8 la' r la'16 la' la'4 sol'8 la' |
fa'4 r16 fa' la' sib' do''8 do''16 do'' do''8 do''16 re'' |
mib''8 mib' mib' fa' re' re' r4 |
r r16 re'' mib'' fa'' mib''8 mib'' r4 |
r r8. mib''16 \grace mib''8 re''4 r |
r2 fa''16 re'' re''8 r do'' |
si'!8 si' r re''16 mib'' fa''4 re''8 mib'' |
do'' do'' r4 r2 |
R1 |
r4 mib'' do''8 do'' r16 do'' re'' mib'' |
re''8 re'' la' sol'16 la' fad'4 r16 la' la' sib' |
do''4 do''8 re'' sib' sib' r4 |
r2 r4 sol'' |
mib''8 mib'' r4 r mib''8 mib''16 mib'' |
re''8 re'' r sib' re'' re'' re'' mib'' |
fa'' re'' re'' do''16 re'' si'!8 si' r re''16 mib'' |
fa''4 re''8 mib'' do'' do''
\ffclef "soprano/treble" <>^\markup\character Armida
fad''8 fad''16 sol'' |
sol''8 re'' r4 r2 |
