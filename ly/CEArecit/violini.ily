\clef "treble"
<<
  \tag #'primo {
    r8 do''4\f do'' do'' do''8~ |
    do'' fa''4 la''8 sib''( sol'' mi'' sib') |
    la'8 do''4 do'' do'' do''8~ |
    do'' fa''4 la''8 sib''( sol'' mi'' sib') |
    la'4 r r2 |
    r8 do''4\p do'' do'' do''8~ |
    do'' fa''4 lab''8 sib'' sol'' mi'' sib' |
    lab' do''4 do''8\f reb'' reb''4 reb''8 |
    do''2 r |
    r8 mib'4\p mib' mib' mib'8~ |
    mib' mib'4 mib' mib' mib'8 |
    mib'4 r r2 |
    r r8 mib'4\p mib'8~ |
    mib' mib'4 mib'8 mib'4 r |
    do''-! r r2 |
  }
  \tag #'secondo {
    la'4(\f fa' mi' sol') |
    fa'8 la'4 do''8 do'' do''4 do''8 |
    do''4( fa' mi' sol') |
    fa'8 la'4 do'' do'' do''8 |
    do''4 r r2 |
    lab'4(\p fa' mi' sol') |
    fa'8 lab'4 do'' do'' do''8 |
    do'' lab'4 lab'8\f << { sib'8 sib'4 sib'8 } \\ { sol'8 sol'4 sol'8 } >> |
    lab'2 r |
    lab8\p lab4 do'8 reb' reb'4 reb'8 |
    do' do'4 lab8 sol sol4 sib8 |
    lab4 r r2 |
    r r8 do'4\p do'8~ |
    do' do'4 do'8 do'4 r |
    sol'-! r4 r2 |
  }
>>
R1*3 |
r2 r8 <<
  \tag #'primo { do''4( fa''8) | \grace mib''8 reb''4 }
  \tag #'secondo { do'4( fa'8) | \grace mib'8 reb'4 }
>> r4 r2 |
r4 do'-\sug\p \tieDashed mib'2~ |
mib'1*3/4~ |
mib'2 \tieSolid <<
  \tag #'primo { do''2~ | do'' sib' | }
  \tag #'secondo { la'2~ | la' sib'2 | }
>>
R1. |
<<
  \tag #'primo {
    re''2 r |
    mib''4 r si'! r |
    R1 |
    r4 mib'' r2 |
  }
  \tag #'secondo {
    sib'2 r |
    \ficta sib'4 r fa' r |
    R1 |
    r4 sol' r2 |
  }
>>
r8 do'( re'\rf mib') re'2 |
sol'4 <<
  \tag #'primo { <lab' sib>4 <sib sol'>2 | }
  \tag #'secondo { re'4 \ficta mib'2 | }
>> \allowPageTurn
R1*41 | \allowPageTurn
<<
  \tag #'primo {
    r4 red''\f mi'' r16 mi'' \grace fa'' mi'' \ficta re''32 mi'' |
    fa''!1\p~ |
    fa'' |
    mi''8 la'' sold''8.(\trill fad''32 sold'') la''8( \grace sol''8 fa''16. mi''32) mi''8( \grace sol''16 fa''16. mi''32) |
    mi''8( \grace sol''16 fa''16. mi''32) mi''8( mi''16. fa''32) \grace mi''8 \once\tieDashed re''2~ |
    re''1~ |
    re'' |
    re'' |
    r2 sol''8( do''' si''8.\trill la''32 si'') |
    \slurDashed do'''8( \grace si'' la''16. sol''32) sol''8( \grace si''16 la''16. sol''32) sol''8( \grace si''16 la''16. sol''32) sol''8 si' | \slurSolid
    do''2 r |
  }
  \tag #'secondo {
    r4 fad'-\sug\f sol' r8 sol' |
    sold'1-\sug\p~ |
    sold' |
    la'4( re'' do'' si') |
    \once\slurDashed do''( si') la'2~ |
    la'1~ |
    la'2 si'!~ |
    si'1 |
    r2 sol'4( fa') |
    \once\slurDashed mi'( re' mi' fa') |
    sol'2 r |
  }
>>
R1*3 |
<<
  \tag #'primo { do''2 }
  \tag #'secondo { la' }
>> r2 |
R1*2 |
<<
  \tag #'primo {
    re''4 r dod'' r |
    r2 la''8 re''' \grace re''' \slurDashed dod'''8.(\trill si''32 dod''') |
    re'''8( \grace do''' sib''16. la''32) la''8( \grace do'''16 sib''16. la''32) la''8( \grace do'''16 sib''16. la''32) la''8 dod'' | \slurSolid
    re''2 \once\tieDashed re''~ |
    re''1 |
  }
  \tag #'secondo {
    fa'4 r mi' r |
    r2 \once\slurDashed la'4( sol') |
    fa'( mi' fa' sol') |
    la'2 \once\tieDashed si'!~ |
    si'1 |
  }
>>
do''4 r8 sol-\sug\p do'4-! re'-! |
mib'4 r r2 | \allowPageTurn
r4 r8 sol' mib'4 re' |
do' r r2 |
<<
  \tag #'primo {
    do''2\fp si'!4 r |
    re'' r do'' r |
    R1 |
    do''1~ |
    \once\tieDashed do''~ |
    do''1~ |
    do''2 r |
    r re''16.(-\sug\p mib''32) re''16-. r mib''16.( fa''32) mib''16-. r |
    r fa''( sib''\rf la'') sol''( fa'' \ficta mib'' re'') do''16.( re''32) do''16-. r re''16.( mib''!32) re''16-. r |
    r16 \ficta mib''( do'''\rf  sib'') la''( sol'' fa'' mib'') re''16.( mib''32) re''16-. r mib''!16.( fa''32) mib''16-. r |
    r fa''( lab''\rf sol'') fa''( \ficta mib'' re'' do'') \once\tieDashed si'!2~ |
    si'1 |
    mib''16.( fa''32) mib''16-. r fa''16.( sol''32) fa''16-. r r sol''( do'''\rf sib'') lab''( sol'' fa'' mib'') |
    re''16.( mib''32) re''16-. r mib''16.( fa''32) mib''16-. r r fa''( re'''\rf do''') si''!( lab'' sol'' fa'') |
    mib''4 r r2 |
    re''4 r r2 |
    r2 re''16.( mib''32) re''16-. r fad''16.( sol''32) fad''16-. r |
    r16 sol''( sib'' \ficta lab'') sol''( fa'' mib'' re'') mib''2 |
    \ficta mib''16.( fa''32) mib''16-. r sol''16.( la''32) sol''16-. r sib''2 |
    \once\tieDashed lab''1~ |
    \ficta lab''~ |
    lab''2 sol''4 fad''-\sug\f |
  }
  \tag #'secondo {
    fad'2-\sug\fp fa'4 r |
    sol' r sol' r |
    R1 |
    \once\tieDashed la'1~ |
    \once\tieDashed la'~ |
    la'~ |
    la'2 r |
    r2 sib'8-\sug\p r sib' r |
    r16 re''( sol''-\sug\rf fa'') mib''( re'' do'' \ficta sib') fa'8 r fa' r |
    r16 do''( la''-\sug\rf sol'') fa''( mib'' re'' do'') sib'8 r sib' r |
    r16 re''( fa''-\sug\rf \ficta mib'') re''( do'' si'! lab') \once\tieDashed sol'2~ |
    sol'1 |
    do''8 r do'' r r16 \ficta mib''( lab''-\sug\rf sol'') fa''( mib'' re'' do'') |
    sol'8 r sol' r r16 re''( si''!-\sug\rf \ficta lab'') sol''( fa'' mib'' re'') |
    do''4 r r2 |
    la'4 r r2 |
    r re''8 r mib'' r |
    r16 sib'( re'' do'') sib'( lab' sol' fa') <mib' sib'>2 |
    sib'8 r mib'' r mib''2 |
    \once\tieDashed re''1~ |
    re''~ |
    re''2 \ficta mib''4 mib''-\sug\f |
  }
>>
r4 <re' la' fad''> <re' sib' sol''>2 |
