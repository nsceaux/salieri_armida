\clef "alto" \slurDashed fa4(\f la sol sib) |
la( fa sol do) |
fa( la sol sib) |
la( fa sol do) | \slurSolid
fa r r2 |
r8 do'4-\sug\p do'4 do' do'8 |
do' fa'4 lab'8 sib' sol' mi' sib |
\ficta lab4 fa' mib'-\sug\f mib' |
mib'2 r |
do'4(\p lab sib sol) |
lab( do' sib reb') |
do'4 r r2 |
r r8 lab4\p lab8~ |
lab lab4 lab8 lab4 r |
mi'!4-! r r2 |
R1*3 |
r2 lab4( do' |
fa'4) r r2 |
r4 do'-\sug\p \once\tieDashed mib'2~ |
mib'1*3/4~ |
mib'2 fa'~ |
fa'1 |
R1. |
fa'2 r |
sol'4 r sol' r |
R1 |
r4 do' r2 |
r8 do'( re'-\sug\rf mib') re'2 |
sol'4( fa') mib'2 | \allowPageTurn
R1*41 | \allowPageTurn
r4 si'-\sug\f si' r8 si' |
si'1\p~ |
si' |
do''4( si' la' sold') |
\once\slurDashed la'( sol'!) fa'2~ |
fa'1~ |
fa'2 sol'~ |
sol'1 |
r2 mi'4( re') |
\once\slurDashed do'( si do' re') |
mi'2 r |
R1*3 |
fa'2 r |
R1*2 |
sib4 r la r |
r2 \once\slurDashed fa'4( mi') |
re'( dod' re' mi') |
fa'2 \once\tieDashed sol'~ |
sol'1 |
sol'4 r8 sol-\sug\p do'4-! re'-! |
mib' r r2 |
r4 r8 sol' mib'4 re' |
do' r r2 |
lab'2-\sug\fp sol'4 r |
si'!4 r do'' r | \allowPageTurn
R1 |
\once\tieDashed fa'1~ |
\once\tieDashed fa'~ |
\once\tieDashed fa'~ |
fa'2 r |
r sib8-\sug\p r do' r |
r16 fa'( sib'-\sug\rf la') sol'( fa' \ficta mib' re') do'8 r sib r |
r16 mib'( do''-\sug\rf \ficta sib') la'( sol' fa' mib') re'8 r do' r |
r16 fa'( lab'-\sug\rf sol') fa'( \ficta mib' re' do') \once\tieDashed re'2~ |
re'1 |
do'8 r re' r r16 sol'( do''-\sug\rf sib') lab'( sol' fa' \ficta mib') |
re'8 r do' r r16 fa'( re''\rf do'') si'!( lab' sol' fa') |
sol'4 r r2 |
fad'4 r r2 |
r2 sib'8 r do'' r |
r16 \once\slurDashed sol'( sib' \ficta lab') \once\slurDashed sol'( fa' mib' re') mib'2 |
sol'8 r sib' r sib'2~ |
\once\tieDashed sib'1~ |
\ficta sib'2 si'~ |
si' do''4 la'!-\sug\f |
r la' sol'2 |
