\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Armida
    \livretVerse#12 { Fermati, aspetta. Ahime! respiro appena. }
    \livretVerse#12 { Un palpito affannoso }
    \livretVerse#12 { Mi serra il cuor, mi toglie }
    \livretVerse#12 { E moto, e voce, e m’abbandona in questi }
    \livretVerse#12 { Brevi istanti fatali }
    \livretVerse#12 { Fin la forza di piangere i miei mali. }
    \livretPers Ubaldo
    \livretVerse#12 { (Che periglioso assalto!) }
    \livretPers Rinaldo
    \livretVerse#12 { (Ah sconsigliato! }
    \livretVerse#12 { Qual cimento aspettai!) }
    \livretPers Armida
    \livretVerse#12 { Mirami ingrato, }
    \livretVerse#12 { Ah que ti feci mai }
    \livretVerse#12 { Per ridurmi cosi? Come a tal segno }
    \livretVerse#12 { Io da te meritai l’odio, e lo sdegno? }
    \livretPers Ubaldo
    \livretVerse#12 { (Tremo per lui.) }
    \livretPers Rinaldo
    \livretVerse#12 { (Resisto appena.) Ascolta }
    \livretVerse#12 { E almen per poco Armida, }
    \livretVerse#12 { Modera il tuo dolor. Pur troppo io sento }
    \livretVerse#12 { I rimproveri tuoi, gemo al tuo pianto, }
    \livretVerse#12 { Mi dispera il tuo affanno, e al solo aspetto }
    \livretVerse#12 { Del tuo presente stato }
    \livretVerse#12 { Io mi sento morir. Vani argomenti }
    \livretVerse#12 { Sariano alla mia fuga }
    \livretVerse#12 { L’arti tue, l’odio antico, il mio periglio; }
    \livretVerse#12 { Ma a un Cittadino, a un figlio }
    \livretVerse#12 { Parla la Patria, e il Ciel. Tutto condanna }
    \livretVerse#12 { Il nostro amore, e mi richiama altrove }
    \livretVerse#12 { La giustizia, il dover. Vuoi ch’io tradisca }
    \livretVerse#12 { Tante speranze, e tanti voti, e manchi, }
    \livretVerse#12 { Guerrier codardo, e cittadin ribelle }
    \livretVerse#12 { Alla Patria, all’ onor? }
    \livretPers Armida
    \livretVerse#12 { Perfido! Oh stelle! }
    \livretVerse#12 { Patria, e regno ebbi anch’io. Di mille voti }
    \livretVerse#12 { E di dolci speranze l’oggetto }
  }
  \null
  \column {
    \livretVerse#12 { Fui già gran tempo, e grand’invidia porsi }
    \livretVerse#12 { Dell’ emule regine al sasto altero; }
    \livretVerse#12 { Anch’io del vostro impero }
    \livretVerse#12 { Distruggere i principi, e col tuo sangue }
    \livretVerse#12 { La Patria oppressa vendicar, stimai }
    \livretVerse#12 { Un consiglio de Numi, e ti salvai. }
    \livretVerse#12 { Ma son vane memorie. In questo stato }
    \livretVerse#12 { Sol le frodi rammenti, e l’odio antico; }
    \livretVerse#12 { L’amante mi tradì; parlo al nemico. }
    \livretVerse#12 { Tuo prigionier, tua preda, }
    \livretVerse#12 { Chiedo sol di seguirti. Ah dove andrei }
    \livretVerse#12 { Senza te sventurata, ove non sia }
    \livretVerse#12 { Segno agli scherni altrui la sorte mia? }
    \livretVerse#12 { Teco verrò. La mia beltà negletta }
    \livretVerse#12 { Del tuo trionfo al campo }
    \livretVerse#12 { La gloria accrescerà; Fedele ancella }
    \livretVerse#12 { Nel fervor della pugna a giorni tuoi }
    \livretVerse#12 { Io veglierò gelosa, e al ferro ostile }
    \livretVerse#12 { Del mio sen farò scudo. Ah non si nieghi }
    \livretVerse#12 { L’innocente richiesta al pianto mio. }
    \livretPers Ubaldo
    \livretVerse#12 { Vacilli forse? }
    \livretPers Armida
    \livretVerse#12 { Ah non rispondi? }
    \livretPers Rinaldo
    \livretVerse#12 { Oh Dio! }
    \livretVerse#12 { Tu nemica? Tu serva? Io te del campo }
    \livretVerse#12 { Al ludibrio esporrei? Deh qui si il fine }
    \livretVerse#12 { Del fallir nostro, Armida, }
    \livretVerse#12 { E del nostro rossor. Nel lido ignoto }
    \livretVerse#12 { Nè copra la memoria eterno obliò. }
    \livretVerse#12 { Rimanti in pace. Addio. Gl’impeti accheta }
    \livretVerse#12 { D’un amore infelice; }
    \livretVerse#12 { Io parto, a te non lice }
    \livretVerse#12 { Meco venir, la gloria tua lo vieta. }
    \livretVerse#12 { Addio. Più che non credi }
    \livretVerse#12 { Attroce nel lasciarti è la mia pena, }
    \livretVerse#12 { Ma seguirmi non dei. }
    \livretPers Armida
    \livretVerse#12 { Dunque mi svena. }
  }
  \null
} #}))
