\key do \major
\tempo "Allegro con agitazzione" \midiTempo#100
\time 4/4 s1*18 s2 \tempo "Più adagio" s2 s1*2
\measure 3/4 s2.
\measure 4/4 s1*2
\measure 6/4 s1.
\measure 4/4 s1*47 s2.
\tempo "Adagio" s4 s1*32 s2
\tempo "Più adagio" s2 s1*15 \bar "|."
\endMark "Seg. Terzetto"