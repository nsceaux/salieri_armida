\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Ar. s1*21
        \set Staff.shortInstrumentName = \markup\character Ub. s2.
        \set Staff.shortInstrumentName = \markup\character Ri. s1*2
        \set Staff.shortInstrumentName = \markup\character Ar. s1. s1*3 s2.
        \set Staff.shortInstrumentName = \markup\character Ub. s4 s2
        \set Staff.shortInstrumentName = \markup\character Ri. s2 s1*24
        \set Staff.shortInstrumentName = \markup\character Ar. s1*41
        \set Staff.shortInstrumentName = \markup\character Ub. s1*2
        \set Staff.shortInstrumentName = \markup\character Ar. s1 s4
        \set Staff.shortInstrumentName = \markup\character Ri. s2. s1*20 s2.
        \set Staff.shortInstrumentName = \markup\character Ar. }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s2. s1*2 s2 \bar "" \pageBreak
        s1*3\break s1*3\pageBreak
        %% 5
        s1*3\break s1*3\break s1*3\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \break
        s2 s1*2\break s1*2\pageBreak
        s1*3\break s1*2 s2 \bar "" \break s2 s1*2\break
        s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*3\break s1*3\pageBreak
        s1*4\break s1*3\pageBreak
        s1*4\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*5\break s1*4\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
