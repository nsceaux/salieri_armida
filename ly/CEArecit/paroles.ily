Fer -- ma -- ti, a -- spet -- ta. Ahi -- me! re -- spi -- ro ap -- pe -- na.
Un pal -- pi -- to af -- fan -- no -- so
mi ser -- ra il cuor, mi to -- glie
e mo -- to, e vo -- ce, e m’ab -- ban -- do -- na in que -- sti
bre -- vi i -- stan -- ti fa -- ta -- li
fin la for -- za di pian -- ge -- re i miei ma -- li.

(Che pe -- ri -- glio -- so as -- sal -- to!)

(Ah scon -- si -- glia -- to!
qual ci -- men -- to a -- spet -- ta -- i!)

Mi -- ra -- mi in -- gra -- to,
ah che ti fe -- ci ma -- i
per ri -- dur -- mi co -- sì, co -- me a tal se -- gno
io da te me -- ri -- tai l’o -- dio, e lo sde -- gno?

(Tre -- mo per lui.)

(Re -- si -- sto ap -- pe -- na.) A -- scol -- ta,
e al -- men per po -- co Ar -- mi -- da,
mo -- de -- ra il tuo do -- lor. Pur trop -- po io sen -- to
i rim -- pro -- ve -- ri tuo -- i, ge -- mo al tuo pian -- to,
mi di -- spe -- ra il tuo af -- fan -- no, e al so -- lo a -- spet -- to
del tuo pre -- sen -- te sta -- to
io mi sen -- to mo -- rir. Va -- ni ar -- go -- men -- ti
sa -- ria -- no al -- la mia fu -- ga
l’ar -- ti tu -- e, l’o -- dio an -- ti -- co, il mio pe -- ri -- glio;
ma a un Cit -- ta -- di -- no, a un fi -- glio
par -- la la Pa -- tria, e il Ciel tut -- to con -- dan -- na il no -- stro a -- mo -- re, e mi ri -- chia -- ma al -- tro -- ve
la giu -- sti -- zia, il do -- ver. Vuoi ch’io tra -- di -- sca
tan -- te spe -- ran -- ze, e tan -- ti vo -- ti, e man -- chi,
guer -- rier co -- dar -- do, e cit -- ta -- din ri -- bel -- le
al -- la Pa -- tria, all’ o -- nor?

Per -- fi -- do! Oh stel -- le!
Pa -- tria, e re -- gno eb -- bi anch’ i -- o, di mil -- le vo -- ti,
e di dol -- ci spe -- ran -- ze l’og -- get -- to
fui, già gran tem -- po, e grand’ in -- vi -- dia por -- si
dell’ e -- mu -- le re -- gi -- ne al fa -- sto al -- te -- ro;
anch’ io del vo -- stro im -- pe -- ro
di -- strug -- ge -- re i prin -- ci -- pi, e col tuo san -- gue
la pa -- tria op -- pres -- sa ven -- di -- car, sti -- ma -- i
un con -- si -- glio de’ nu -- mi, e ti sal -- va -- i,
ma son va -- ne me -- mo -- rie. In que -- sto sta -- to
sol le fro -- di ram -- men -- ti, e l’o -- dio an -- ti -- co;
l’a -- man -- te mi tra -- dì; par -- lo al ne -- mi -- co.
Tuo pri -- gio -- nier, tua pre -- da,
chie -- do sol di se -- guir -- ti. Ah do -- ve an -- drei
sen -- za te sven -- tu -- ra -- ta, o -- ve non sia
se -- gno a -- gli scher -- ni al -- trui la sor -- te mi -- a?
Te -- co ver -- rò. La mia bel -- tà ne -- glet -- ta
del tuo tri -- on -- fo al cam -- po
la glo -- ria ac -- cre -- sce -- rà; Fe -- del an -- cel -- la
nel fer -- vor del -- la pu -- gna a’ gior -- ni tuo -- i
io ve -- glie -- rò ge -- lo -- sa, e al fer -- ro o -- sti -- le
del mio sen fa -- rò scu -- do. Ah non si nie -- ghi
l’in -- no -- cen -- te ri -- chie -- sta al pian -- to mi -- o.

Va -- cil -- li for -- se?

Ah non ri -- spon -- di?

Oh Di -- o!
tu ne -- mi -- ca? tu ser -- va? Io te del cam -- po
al lu -- di -- brio e -- spor -- re -- i? Deh qui sia il fi -- ne
del fal -- lir no -- stro, Ar -- mi -- da,
e del no -- stro ros -- sor. Nel li -- do i -- gno -- to
ne co -- pra la me -- mo -- ria e -- ter -- no o -- blio.
Ri -- man -- ti in pa -- ce. Ad -- dio. Gl’im -- pe -- ti ac -- che -- ta
d’un a -- mo -- re in -- fe -- li -- ce;
io par -- to, a te non li -- ce
me -- co ve -- nir, la glo -- ria tu -- a lo vie -- ta.
Ad -- di -- o. Più che non cre -- di
a -- tro -- ce nel la -- sciar -- ti è la mia pe -- na,
ma se -- guir -- mi non de -- i.

Dun -- que mi sve -- na.
