\clef "bass" fa4(\f la sol sib) |
la( fa sol do) |
fa( la sol sib) |
la( fa sol do) |
fa r r2 |
fa4(\p lab sol sib) |
lab( fa sol do) |
fa fa mib\f mib |
lab2 r |
do4(\p lab, sib, sol,) |
lab,( do sib, reb) |
do r r2 |
r lab,4(\p do |
mib lab) lab, r |
mi!-! r r2 |
R1*3 |
r2 fa4( lab |
sib) r r2 |
r4 do\p mib2~ |
mib1*3/4~ |
mib1~ |
mib2 re |
R1. |
lab2 r |
sol4 r re r |
R1 |
r4 do r2 |
r8 do( re\rf mib) re2 |
sol4 fa mib2 |
R1*2 |
re2\p r |
R1 |
r2 do |
si,! r |
do r |
mib r |
R1 |
lab2 r |
R1 | \allowPageTurn
r2 mi! |
r fa |
R1*2 |
mib2 r |
fad r |
R1 |
sol2 r |
R1*3 |
r2 fa4 fa |
sib,2 r |
R1*2 |
r2 fad |
R1*3 |
sol2 r |
fa! r |
R1*3 | \allowPageTurn
r2 mib |
do' r4 re' |
sib la sold2 |
R1 |
r2 la |
R1 |
r4 si\f mi r8 mi |
re1\p~ |
re |
do'4( si la sold) |
\once\slurDashed la( sol!) fa2~ |
fa1~ |
fa~ |
fa |
r2 mi'4( re') |
do'( si do' re') |
mi'2 r |
R1*3 |
fa2 r |
R1*2 | \allowPageTurn
sib4 r sol r |
r2 fa4( mi) |
re( dod re mi) |
\once\tieDashed fa1~ |
fa |
mib4 r8 sol,\p do4 re |
mib r r2 |
r4 r8 sol mib4 re |
do r r2 |
lab2\fp sol4 r |
fa! r mi r |
R1 |
fa1~ |
\once\tieDashed fa~ |
fa~ |
fa2 r |
r sib8\p r do' r |
re' r r4 la8 r sib r |
do' r r4 sib8 r do' r |
re' r r4 \once\tieDashed fa2~ |
fa1 |
do'8 r re' r mib' r r4 |
si!8 r do' r re' r r4 |
do4 r r2 |
fad4 r r2 |
r sol8 r la r |
sib r r4 sol2 |
sol8 r sol r sol2 |
\once\tieDashed fa1~ |
fa~ |
fa2 mib4 do'\f |
r re' sol2 |
