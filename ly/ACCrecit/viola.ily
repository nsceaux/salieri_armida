\clef "alto" mid r r2 |
fad'8 fad fad' |
sold' sold sold' |
la'4 r r2 |
r re'4-\sug\p r |
R1 |
sol'4 r sol' r |
sol' r r2 |
R1*2 |
fa'4 r r2 |
r4 sol'-\sug\f fad' r |
R1*2 |
sol'4 r r2 |
mib'2\fp re' |
re'4-\sug\f do' sib2~ |
\once\tieDashed sib1-\sug\p~ |
\ficta sib2 mib'4 r |
R1 |
r4 fa\f sib8. do'16 re'8. do'16 |
\ficta sib8. la16 sib8. sol16 fad2 |
R1*2 |
r2 sol'4 r |
dod' r r2 |
r2 re'4 r |
r2 mi'4 r |
r fad' si r |
