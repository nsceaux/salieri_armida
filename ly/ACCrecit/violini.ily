\clef "treble" mid'4 r r2 |
<<
  \tag #'primo {
    r8. \ficta dod''16 \grace { re''32[ dod'' si'] } dod''16( re'') |
    \grace { \ficta dod''8 } si'8. re''16 \grace { mi''32[ re'' dod''] } re''16( mi'') |
    \ficta dod''4 r r2 |
    r fad'4\p r |
    R1 |
    sol'4 r si' r |
    do'' r4 r2 |
    R1*2 |
    la'4 r r2 |
    r4 <re' si'>\f <re' la'> r |
    R1*2 |
    <re' sib'>4 r r2 |
    do''\fp <re'' fad'> |
  }
  \tag #'secondo {
    fad'4 la'8 |
    mi'4 si'8 |
    la'4 r r2 |
    r2 la4-\sug\p r |
    R1 |
    si4 r re' r |
    <sol mi'> r r2 |
    R1*2 |
    do'4 r r2 |
    r4 <si sol'>-\sug\f <la' re'> r |
    R1*2 |
    <sib sol'>4 r r2 |
    sol'2-\sug\fp <la' re'> |
  }
>>
re'4\f do'8. re'32 do' sib2~ |
\once\tieDashed sib1\p~ |
\ficta sib2 mib'4 r |
R1 |
r4 la'\f sib'8. do''16 re''8. do''16 |
\ficta sib'8. la'16 sib'8. sol'16 fad'2 |
R1*2 |
<<
  \tag #'primo {
    r2 si'!4 r |
    lad' r r2 |
    r si'4 r |
    r2 si'4 r |
  }
  \tag #'secondo {
    r2 re'4 r |
    fad' r r2 |
    r \ficta fad'4 r |
    r2 sol'4 r |
  }
>>
r4 <fad'? dod''? lad''>4\f <fad' re'' si''>4 r |
