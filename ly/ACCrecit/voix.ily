\clef "tenor/G_8" <>^\markup\character Ubaldo
r2 dod'8 sold16 la si8 la |
fad16 fad r8 r |
R4. |
\ffclef "alto/treble" <>^\markup\character Ismene
r8 la' la' la' la' mi' r mi' |
sol' sol' la' mi' fad'4 r16 fad' fad' re'' |
re''8 la' r la' do''! do'' do'' si' |
\grace la'8 sol'4 r16 sol' sol' la' si'8 si' re'' si'16 do'' |
do''8 sol' r sol' do'' do'' r sol' |
sol' sol' sol' la' sib' sib' r sib' |
\ficta sib' sib' la' sib' sol' sol' r16 sol' la' sib'! |
la'8 la' r fa' si'! si' si' do'' |
sol'4 r
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
re'8. fad16 fad8 r16 la |
la8 re r re re' re' re' mib' |
do'8 do' r16 la la sib do'8 do' do'16 do'32 do' do'16 sib! |
sol8 sol r sol16 la sib4 la8 sol |
do'8 do' r16 sol sol fad \sugNotes { la8 la } r4 |
r r8 fa sib8 sib sib sib |
\ficta sib fa r fa16 fa fa4 fa8 sol |
lab8 lab lab lab16 sib sol8 sol r sib16 sib |
sol4 lab8 sib sib fa
\ffclef "soprano/treble" <>2^\markup\character Coro
mib''8 la'16 sib' |
sib'8 fa' r4 r2 |
\ffclef "alto/treble" <>^\markup\character Ismene
r2 r4 r8 la'16 re'' |
re''8 la' r la' la' la'16 la' la' la' la' la' |
fad'4 r16 fad' fad' sol' la'8 la' r la'16 si' |
do''4 la'8 si' sol'4 r16 si' si' dod'' |
lad'8 lad' r fad'16 fad' fad'4 fad'8 \ficta sold' |
lad' lad' r16 dod'' lad' fad' si'8 si' re'' si'16 si' |
si'4 la'8 si' sol' sol' r16 si' lad' si' |
si'8 fad' r4 r2 |
