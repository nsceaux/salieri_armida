\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (fagotti #:score-template "score-voix" #:notes "bassi" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPersDidas Ubaldo rispingendola con severità.
  \livretVerse#12 { Scostati, indegna }
  \livretVerse#12 { Riserba a miglior uso }
  \livretVerse#12 { I finti vezzi, e le lusinghe accorte. }
  \livretVerse#12 { Puoi col riso sul labro offrir la morte? }
  \livretVerse#12 { So qual tosco s'asconde }
  \livretVerse#12 { In que' cibi, in quell'onde, e le temute }
  \livretVerse#12 { Vane frodi d'Armida… }
  \livretPersDidas Coro delle Donzelle
  \livretDidasP\justify {
    (tutte impaurite scostancdosi da Ubaldo.)
  }
  \livretVerse#12 { Ah siam perdute. }
  \livretPers Ismene
  \livretVerse#12 { In mal punto ricusi, }
  \livretVerse#12 { Malnato Cavalier, l'offerta pace, }
  \livretVerse#12 { E l'offerta amistà; se à molli preghi }
  \livretVerse#12 { Altro ch' onte, e disprezzi offrir non sai }
  \livretVerse#12 { Guerra apporti, infelice, e guerra avrai. }
}

       #}))
