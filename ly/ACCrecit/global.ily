\key mi \minor \midiTempo#80 \time 4/4 s1 \bar "||"
\time 3/8 \midiTempo#40 s4.*2 \bar "||"
\time 4/4 \midiTempo#80 s1*3
\key do \major \grace s8 s1*14 s4 \tempo "Allegro" s2. s1*8 \bar "|."
