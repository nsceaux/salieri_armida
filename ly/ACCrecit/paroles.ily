(Che pe -- ri -- glio -- so as -- sal -- to!)

Al dol -- ce in -- vi -- to
per -- chè re -- si -- sti mai? Non pre -- ga in -- va -- no
don -- zel -- la in fre -- sca e -- tà. Trar -- rot -- ti io stes -- sa
l’el -- mo lu -- cen -- te, e que -- sta,
con cui di -- stin -- gue Ar -- mi -- da
i fi -- di ser -- vi su -- oi, fio -- ri -- ta in -- se -- gna
al crin ti cin -- ge -- rò.

Sco -- sta -- ti, in -- de -- gna
ri -- ser -- ba a mi -- glior u -- so
i fin -- ti vez -- zi, e le lu -- sin -- ghe ac -- cor -- te:
puoi col ri -- so sul la -- bbro of -- frir la mor -- te?
So qual to -- sco s’as -- con -- de
in que’ ci -- bi, in quell’ on -- de, e le te -- mu -- te
va -- ne fro -- di d’Ar -- mi -- da…

Ah siam per -- du -- te!

In mal pun -- to ri -- cu -- si,
mal -- na -- to ca -- va -- lier, l’of -- fer -- ta pa -- ce,
e l’of -- fer -- ta a -- mi -- stà; se a’ mol -- li pre -- ghi
al -- tro ch’on -- te, e di -- sprez -- zi of -- frir non sa -- i
guer -- ra ap -- por -- ti, in -- fe -- li -- ce, e guer -- ra a -- vrai.
