\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Ub.
        s1 s4.*2
        \set Staff.shortInstrumentName = \markup\character Ism.
        s1*8 s2
        \set Staff.shortInstrumentName = \markup\character Ub.
        s2 s1*7 s2
        \set Staff.shortInstrumentName = \markup\character Cor.
        s2 s1
        \set Staff.shortInstrumentName = \markup\character Ism.
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1 s4.*2\pageBreak
        s1*2\break s1*2 s4 \bar "" \pageBreak
        s2. s1*2\break s1*3\pageBreak
        s1*2\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
