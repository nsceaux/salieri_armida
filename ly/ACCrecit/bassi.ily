\clef "bass" mid4 r r2 |
fad8 fad, fad |
sold sold, sold |
la4 r r2 |
r re4\p r |
r1 |
sol4 r \sug fa r |
\sug mi r r2 |
R1*2 |
\sug fa4 r r2 |
r4 sol\f fad r |
r1*2 |
sol4 r r2 |
mib\fp re |
re4-\sug\f do sib,2~ |
sib,1\p~ |
sib,2 mib4 r |
r1 |
r4 fa-\sug\f sib8. do'16 re'8. do'16 |
sib?8. la16 sib8. sol16 fad2 |
R1*2 |
r2 sol4 r |
mi4 r r2 |
r re4 r |
r2 mi4 r |
r4 fad si, r |
