\clef "tenor/G_8" R1*13 |
r2 r4 r8 sib |
mib'4. mib'8 mib'4. sib8 |
do'4 do' r r8 sol |
lab4. lab8 la4. la8 |
sib2( do'4) re' |
mib' do'8 do' lab4 sib |
mib4 r r2 |
R1 |
r2 r4 sib |
do' mib' do' lab |
sib8[ sol] mib4 r2 |
R1 |
r2 r4 sib |
do'4 mib' do' lab |
sib8[ sol] mib4 r r8 sib |
mib'2. sib4 |
do' r r r8 sol |
lab4. lab8 sib4. sib8 |
do'2 r4 r8 sib |
mib'2. sib4 |
do'2 r4 r8 sol |
lab4. lab8 sib4. sib8 |
mib4 r r2 |
R1 |
