\clef "bass" mib8-!\pp mib16( re) mib8-! mib-! mib16 fa sol fa mib re do si, |
do8 do16( si,) do8-! do-! do16 re mib re do sib,! lab, sol, |
lab,8 lab,16 sol, lab,8 lab, lab,16 lab, lab, lab, la,4:16 |
sib,2:\ff sib,: |
sib,: sib,: |
sib,: sib,: |
sib,: sib,: |
si,4: si: do': la: |
sib!: sol: lab!: fa: |
sol: mib: fa16 fa mib mib re re do do |
sib,2: sib,: |
sib,:-\sug\mf sib,: |
sib,4\p r sib, r |
sib,2 r |
mib4 r r4 r8 sib |
do'4 r r r8 sol |
lab4 r r r8 la |
sib2 do'4 re' |
mib'4 do' lab sib |
<< \ru#4 { mib8 mib' } { s2 s\f } >> |
<>_"Simili" \rt#2 { mib8 mib' } \rt#2 { mib8 mib' } |
\rt#2 { mib8 mib' } \rt#2 { mib8 mib'\p } |
\rt#2 { mib8 mib' } \rt#2 { mib8 mib' } |
\rt#2 { mib8 mib' } \rt#2 { mib8\f mib' } |
\rt#2 { mib8 mib' } \rt#2 { mib8 mib' } |
\rt#2 { mib8 mib' } \rt#2 { mib8 mib'\p } |
mib8 mib' mib mib' mib mib' mib mib' |
mib8 mib' mib mib' mib mib' mib mib' |
mib4 r r r8 sib |
do'4 r r r8 sol |
lab4 r sib r |
do'2 r4 r8 sib,\f |
mib8-! mib16( re) mib8-! mib-! mib16 fa sol fa mib re do sib, |
do8-! do16( si,) do8 do do16 re mib re do \ficta sib, lab, sol, |
lab,2:16 sib,: |
mib4 r8 sib mib' sol16 lab sib8 sol |
mib sol sib mib' mi2*7/8~ \hideNotes mi16 |
