\clef "alto" \sug do'4 r r2 |
R1*2 |
sib16-\sug\ff sib' sib' sib' sib'4:16 sib'2: |
sib'16 sib sib sib sib4: sib2: |
sib16 sib' sib' sib' sib'4: sib'2: |
sib'16 sib sib sib sib4: sib2: |
si4 si'16 si' si' si' do''4: la': |
sib'!: sol': lab'!: fa': |
sol': mib': fa'16 fa' mib' mib' re' re' do' do' |
sib sib' la' sol' fa' sol' fa' mib' re' sol' fa' mib' re' mib' re' do' |
sib2:16-\sug\mf sib: |
sib4-\sug\p r sib r |
sib2 r |
mib4\p r4 r4 r8 sib |
do'4 r r r8 sol |
lab4 r la r |
sib2 do'4 re' |
mib'4 do' lab sib |
<<
  \ru#36 { mib8 mib' }
  { s2 s\f |
    s1 |
    s2. s4\p |
    s1 |
    s2 s-\sug\f |
    s1 |
    s2. s4\p | }
>>
mib4 r r r8 sib |
do'4 r r r8 sol |
lab4 r sib r |
do'2 r4 sib\f |
mib'8-! mib'16( re') mib'8-! mib'-! mib'16 fa' sol' fa' mib' re' do' sib |
do'8-! do'16( si) do'8 do' do'16 re' mib' re' do' \ficta sib lab sol |
lab2:16 sib: |
mib4 r8 sib mib' sol16 lab sib8 sol |
mib sol sib mib' mi2 |
