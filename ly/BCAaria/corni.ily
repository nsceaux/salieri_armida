\clef "treble" \transposition mib
R1*4 |
r8 <>\ff \twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' sol' sol'8 | sol'4 }
  { sol4 sol sol sol8 | sol4 }
>> r4 r2 |
r8 \twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' sol' sol'8 | }
  { sol4 sol sol sol8 | }
>>
R1*3 |
\twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''2 }
  { sol'1~ | sol'2 }
>> r2 |
R1*7 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' |
    do''2 re'' |
    \grace fa''4 mi''2 }
  { do''4 do'' |
    do''2 sol' |
    \grace re''4 do''2 }
>> r2 |
R1 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' |
    do''2 re'' |
    \grace fa''4 mi''2 }
  { do''4 do'' |
    do''2 sol' |
    \grace re''4 do''2 }
>> r2 |
R1*11 |
