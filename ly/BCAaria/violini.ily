\clef "treble"
<<
  \tag #'primo { do''4 r r2 | R1*2 | }
  \tag #'secondo { R1*3 | }
>>
r8 <sib' sib''>4\ff q q q8 |
q sib4 sib sib sib8 |
sib <sib' sib''>4 q q q8 |
q sib4 sib sib sib8 |
si4 <<
  \tag #'primo {
    si''16 si'' si'' si'' do''' do''' do''' do''' la'' la'' la'' la'' |
    sib''!4:16 sol'': lab''!: fa'': |
    sol'': mib'': fa''16 fa'' mib'' mib'' re'' re'' do'' do'' |
    sib' sib'' la'' sol'' fa'' sol'' fa'' mib'' re'' sol'' fa'' mib'' re'' mib'' re'' do'' |
    sib'
  }
  \tag #'secondo {
    si'4:16 do'': la': |
    sib'!: sol': lab'!: fa': |
    sol': mib': fa'16 fa' mib' mib' re' re' do' do' |
    sib sib' la' sol' fa' sol' fa' mib' re' sol' fa' mib' re' mib' re' do' |
    sib
  }
>> sib'16\mf \ficta la' sol' fa' sol' fa' mib' re' sol' fa' mib' re' mib' re' do' |
<<
  \tag #'primo { sib2:16\p sib: | }
  \tag #'secondo { sib4-\sug\p r sib r | }
>>
sib2 r4 r8 sib'\p |
mib''8-! mib''16( re'') mib''8-! mib''-! mib''16 fa'' sol'' fa'' mib'' re'' do'' sib' |
do''8-! do''16( si') do''8-! do''-! do''16 re'' mib'' re'' do'' sib' lab' sol' |
lab'2:16 la': |
sib': do''4: re'': |
mib'': do'': lab': sib': |
mib'4 r4*3/4 s16-\sug\f <sol mib' sib' sol''>4 <sib' sib''> |
<lab'' do''>4-! <<
  \tag #'primo {
    fa''4-! re''-! lab'-! |
    \grace lab'4 sol'2 r4 sib'\p |
    do''4( mib'' do'' lab') |
    sib'8( sol') mib'4*3/4
  }
  \tag #'secondo {
    lab'4-! fa'-! re'-! |
    \grace re'4 mib'2 r4 sol'-\sug\p |
    lab'( do'' lab' fa') |
    sol'8( mib') sol4*3/4
  }
>> s16\f <sol mib' sib' sol''>4 <sib' sib''> |
<lab'' do''>4-! <<
  \tag #'primo {
    fa''4-! re''-! lab'-! |
    \grace lab'4 sol'2 r4 sib'\p |
    do''4( mib'' do'' lab') |
    sib'8( sol') mib'4
  }
  \tag #'secondo {
    lab'4-! fa'-! re'-! |
    \grace re'4 mib'2 r4 sol'-\sug\p |
    lab'( do'' lab' fa') |
    sol'8( mib') sol4
  }
>> r4 r8 sib' |
mib''-! mib''16( re'') mib''8-! mib''-! mib''16 fa'' sol'' fa'' mib'' re'' do'' sib' |
do''8 do''16( si') do''8-! do''-! do''16 re'' mib'' re'' do'' sib' lab' sol' |
lab'2:16 sib': do''2 r4 r8 sib'\f |
mib''8-! mib''16( re'') mib''8-! mib''-! mib''16 fa'' sol'' fa'' mib'' re'' do'' sib' |
do''8-! do''16( si') do''8 do'' do''16 re'' mib'' re'' do'' \ficta sib' lab' sol' |
lab'2:16 sib': |
mib'8 mib''16 fa'' sol''8 re'' mib'' sol'16 lab' sib'8 sol' |
mib'8 sol' sib' mib'' mi'2 |
