\clef "bass" R1*3 |
sib,1\ff~ |
sib,~ |
\once\tieDashed sib,~ |
sib, |
si,4 si do' la |
sib! sol lab! fa |
sol mib fa8 mib re do |
\once\tieDashed sib,1~ |
sib, |
sib,4-\sug\p r sib, r |
sib,2 r |
R1*5 |
r2 mib8-\sug\f mib' mib mib' |
mib mib' mib mib' mib mib' mib mib' |
mib2 r |
R1 |
r2 mib8-\sug\f mib' mib mib' |
mib mib' mib mib' mib mib' mib mib' |
mib2 r |
R1*11 |
