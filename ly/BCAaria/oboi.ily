\clef "treble"
<<
  \tag #'(primo tutti) {
    \tag #'tutti <>^\markup\concat { 1 \super o }
    \sug do''4 r r2 |
    R1*2 |
  }
  \tag #'secondo R1*3
>>
r8 <>\ff \twoVoices #'(primo secondo tutti) <<
  { sib''4 sib'' sib'' sib''8 | sib''4 }
  { sib'4 sib' sib' sib'8 | sib'4 }
>> r4 r2 |
r8 \twoVoices #'(primo secondo tutti) <<
  { sib''4 sib'' sib'' sib''8 | sib''4 }
  { sib'4 sib' sib' sib'8 | sib'4 }
>> r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { si''4 do''' la'' |
    sib''! sol'' lab''! fa'' |
    sol'' }
  { si'4 do'' la' |
    sib'! sol' lab'! fa' |
    sol' }
>> mib''4 fa''8 mib'' re'' do'' |
sib'1~ |
sib'2 r |
R1*7 |
r2 <>\f \twoVoices #'(primo secondo tutti) <<
  { sol''4-! sib''-! |
    lab''-! fa''-! re''-! lab'-! |
    \grace lab'4 sol'2 }
  { sib'4-! sib'-! |
    do''-! lab'-! fa'-! re'-! |
    \grace re'4 mib'2 }
>> r2 |
R1 |
r2 <>\f \twoVoices #'(primo secondo tutti) <<
  { sol''4-! sib''-! |
    lab''-! fa''-! re''-! lab'-! |
    \grace lab'4 sol'2 }
  { sib'4-! sib'-! |
    do''-! lab'-! fa'-! re'-! |
    \grace re'4 mib'2 }
>> r2 |
R1*11 |
