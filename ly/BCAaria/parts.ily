\piecePartSpecs
#`((oboi #:score-template "score-deux-voix"
         #:indent 0)
   (corni #:score-template "score-deux-voix"
          #:tag-global ()
          #:instrument "Corni in Eb")
   (fagotti #:music , #{ <>
\quoteBassi "BCAbassi"
<>^\markup\tiny "B."
\cue "BCAbassi" { \mmRestInvisible s1*2 s2. \mmRestVisible }
\new CueVoice { la,16 la, la, la, }
s1*11
<>^\markup\tiny "B."
\cue "BCAbassi" { \mmRestInvisible s1*5 \mmRestVisible \restInvisible s2 \restVisible }
s2 s1 s2
<>^\markup\tiny "B."
\cue "BCAbassi" { \restInvisible\mmRestInvisible s2 s1 s2 \mmRestVisible\restVisible }
s2 s1 s2
<>^\markup\tiny "B."
\cue "BCAbassi" { \restInvisible\mmRestInvisible s2\restVisible s1*11 \mmRestVisible }


    #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { Oh come in un momento }
  \livretVerse#10 { Dall' incantata mole }
  \livretVerse#10 { Tutto l'orror spari, }
  \livretVerse#12 { Qual nube in faccia al vento, }
  \livretVerse#10 { Qual fosca nebbia suole }
  \livretVerse#10 { A' caldi rai del dì. }
} #}))

