\clef "treble" <>_\markup\italic dolce
\override AccidentalSuggestion.avoid-slur = #'outside
<<
  \tag #'primo {
    fa''2. mib''8. re''16 |
    do''4 re''2 mib''4 |
    \grace fa''8 mib''4 re'' re''8-! sib'(\rf sol'' fa'') |
    fa''2 mib''\fermata |
    re'' r4^\markup\italic Con la parte \slurDashed re''( |
    \override Script.avoid-slur = #'outside 
    do'') re''( do'') re''( |
     mib''2)\fermata \slurSolid r8 do'' re''( do'') |
    sib'4.\fermata do''8 \grace do'' re''4 re''( |
    do'') \slurDashed re''( do'') re''( |
    mib''2)\fermata \slurSolid r8 do'' re''( mib''16 do'') |
    sib'4.\fermata do''8 sib'4 r |
  }
  \tag #'secondo {
    fa'8(-. fa'-. fa'-. fa'-. fa'-. fa'-. mib'-. re'-.) |
    do' do' re' re' re' re' mib' mib' |
    \grace fa' mib' mib' re' re' re'-! sib(-\sug\rf sol' fa') |
    fa'2 mib'\fermata |
    re' r4 \once\tieDashed fa'~ |
    fa'4 fa'2 fa'4( |
    \override Script.avoid-slur = #'outside 
    sib'2)\fermata r8 fa' \once\slurDashed fa'( sol'16 mib') |
    re'4.\fermata la'8 sib'4 fa'~ |
    fa' fa'2 \once\slurDashed fa'4( |
    sib'2)\fermata r8 fa' fa'( sol'16 mib') |
    re'4.\fermata mib'8 re'4 r |
  }
>>
<<
  \tag #'primo {
    re''2~ re''8 mib'' |
    \grace re'' do''2 re''8( fa'') |
    \grace fa''8 mib''4 re''4.( do''8) |
    sib'4 r8 \grace sol''16 fa''\mf mi''32 fa'' sol''8-. la''-. sib''-. la''-. sol''-. fa''-. mib''-. re''-. |
    \grace fa''8 mib''4(\rf re''4) r |
    re''2\p~ re''8 mib'' |
    \grace re''8 do''2 re''8( fa'') |
    \grace fa''8 mib''4 re''4.( do''8) |
    sib'4 r8 \grace { sol''16[ fa''32 mib''] } fa''8-. sol''-. la''-. |
    sib''-. la''-. sol''-. fa''-. mib''-. re''-. |
    \grace fa''8 \once\slurDashed mib''4(\rf re'') r4 |
    fa''8(\p re'') fa''( re'') fa''( re'') |
    \grace sol''8 fa''4\rf mib''8( re'') do''( si') |
    do''(\p fa') do''( fa') do''( fa'') |
    fa'' fa'' mi'' mi'' mi'' fa'' |
    sol''-. sol''-. sib''-. sol''-. mi''-. sib'-. |
    \grace sib'8 la'4 la'8.\f sib'16 sib'8.\trill la'32 sib' |
    do''8\p r fa''4.(\f mib''!8)\p |
    re''8 re'' re'' dod'' re'' mi'' |
    fa'' fa'' fa'' mi'' fa'' sol'' |
    <la' la''>4\f \once\slurDashed la''16(\p sib'' la'' sol'') fa'' mi'' fa'' sol'' |
    la''16-.\f sib''-. do'''-. sib''-. la''-. sib''-. la''-. sol''-. fa''-. mib''!-. re''-. do''-. |
    re''( mi'' re'' mi'') re''8-!\f sib''(\p la'' sol'') |
    do''\f la'' sol'' sol'' sol'' sol'' |
    fa''4 r8 \grace sol''16 fa'' mib''32 fa'' la''8-. do'''-. |
    \grace do'''8 sib''4(\rf la''4.) sib''8 |
    \grace la''8 sol''(\p fa'') \grace sol''8 fa''\f mi''16 fa'' la''8-! do'''-! |
    \grace do'''8 sib''4(\f la''4.) sib''8\p |
    \grace la''8 sol''4( fa''4.) mib''!8 |
    re'' re'' re'' dod'' re'' mi'' |
    fa'' fa'' fa'' mi'' fa'' sol'' |
    la'' la'' la''( sol'' fa'' mib''!) |
    re''8 mi''16 fa'' sol'' la'' sib'' do''' re'''8 re'' |
    do''\f la''16 la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    la''\p la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol''16\f sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    fa''4. \grace sol''8 fa''16 \ficta mi''32 fa'' la''8 do''' |
    \grace do'''8 sib''4( la''4.) sib''8 |
    \grace la''8 sol''4( fa''4.) fa''8 |
    mi''16 re'' sib'' sol'' fa''4. \grace la''8 sol'' |
    fa''4 fa' r |
  }
  \tag #'secondo {
    sib'8( fa') sib'( fa') sib'( do'') |
    sib' fa' la' fa' sib' sib' |
    sib'8 sib'4 sib' la'8 |
    sib'4 r8 re'-.-\sug\mf mib'-. fa'-. |
    sol'-. fa'-. mib'-. re'-. do'-. sib-. |
    <la fa'>4-\sug\rf <sib fa'> r |
    sib'8(\p fa') sib'( fa') sib'( do'') |
    sib'( fa') la'( fa') sib' sib' |
    sib' sib'4 sib' la'8 |
    sib'4 r8 re'-. mib'-. fa'-. |
    sol'-. fa'-. mib'-. re'-. do'-. sib-. |
    la4(-\sug\rf sib) r |
    re''8(-\sug\p sib') re''( sib') re''( sib') |
    re'' sib' sib'( fa') sib'( fa') |
    la'( fa') la'( fa') fa'( do'') |
    sib'2.~ |
    sib'2 sol'4 |
    do'4 fa'8.-\sug\f sol'16 sol'8.\trill fa'32 sol' |
    fa'8-\sug\p r8 do''-\sug\f r fa'-\sug\p r |
    fa'8 fa' fa' mi' fa' la' |
    do'' do'' do'' do'' do'' do'' |
    << do''4 \\ fa'\f >> do''16(\p re'' do'' sib') la' sol' la' sib' |
    do''2-\sug\f~ do''8 fa'-\sug\p |
    fa'16 sol' fa' sol' fa'8-!-\sug\f re''(-\sug\p do'' sib') |
    la'-\sug\f fa'' fa'' fa'' mi'' mi'' |
    fa''4 r8 fa'' fa'' fa'' |
    fa''8 fa''4 fa'' fa''8 |
    \grace fa''8 \once\slurDashed mi''(-\sug\p fa'') fa'' fa'' fa'' fa'' |
    fa'' fa''4 fa'' fa''8 |
    \once\slurDashed mi''4(\p fa'') do'' |
    fa'8 fa' fa' mi' fa' do'' |
    do'' do'' do'' do'' do'' do'' |
    do''8 do'' do'' do'' fa' fa' |
    sib'4 r r8 sib' |
    la'8\f fa''16 fa'' fa''4:16 fa'': |
    fa''2.:\p |
    fa'': |
    mi''16\f mi'' mi'' mi'' mi''4:16 mi'': |
    fa''16 do'' do'' do'' do''8 do'' fa'' fa'' |
    fa''16 fa'' fa'' fa'' fa''4:16 fa''16 fa'' fa'' fa'' |
    fa'' fa'' \ficta mi'' mi'' fa''8 do'' do'' do'' |
    sib' re''16 sib' la'4. sib'8 |
    la'4 fa' r |
  }
>>
\override Script.avoid-slur = #'inside
<<
  \tag #'primo {
    \allowPageTurn R2.*2 |
    sol''2.\p~ |
    sol''4( fa'' mib'') |
    re''8 mib'' fa''4 re'' |
    \grace fa''8 mib''4. mib''8-.( mib''-. mib''-.) |
    \grace mib''8 re''4 re''4. re''8 |
    mib''4 r8 mib''( fa'' sol'') |
    lab''2.\f |
    sol'' |
    fa''4(\p mib'' re'') |
    do'' r sib'! |
    mib''2 mib''4 |
    mib''4.\rf ( fa''16 sol'' lab''8) fa''-! |
    mib''4 r sib' |
    mib''4.(\rf fa''16 sol'' lab''8) fa''-! |
    mib''4 sol''8( fa'' mib'' re'') |
    \grace re''8 do''4( sib') r |
    mib'2\f reb''4\p |
    do''8( lab'' sol'' fa'') mi''16( fa'' lab'' fa'') |
    mib''!2( fa''4) |
    \grace fa''8 sol''-. sib''-. lab''-. sol''-. fa''-. mib''-. |
    re''-.-\sug\f do''-. sib'-. lab'-. sol'-.-\sug\p sib'-. |
    do''8 do'' do'' re''16 mib'' re'' do'' sib' lab' |
    sol'8\f mib' re' re' re' re' |
    mib'4 r r |
    R2. |
    fa''2.-\sug\p~ |
    fa''4( mib'' re'') |
    do''2 re''4 |
    re''( do''8) fa''-.\f sol''-. la''-. |
    \grace do'''8 sib'' la''16-. sib''-. \grace do'''8 sib'' la''16-. sib''-. \grace do'''8 sib''( la''16 sol'') |
    fa''4 r8 fa''-.\p sol''-. la''-. |
    \grace do'''8 sib'' la''16-. sib''-. \grace do'''8 sib'' la''16 sib'' \grace do'''8 sib'' la''16 sol'' |
    fa''2 r4 |
    << sol''2. <sib' mib'>4-\sug\f >> |
    fa''2\fermata r4 |
    mib''2. |
    re''2\fermata re''4( |
    do'') re''( do'') re''( |
    mib''2) r8 do'' re''( mib''16 do'') |
    sib'4.( do''8 re''4)\fermata re''( |
    do'') re''( do'') re''( |
    mib''2) r8 do'' re''( mib''16 do'') |
    sib'2 do''4 |
    sib'8.\f do''32 la' sib'8. do''32 la' sib'8-.\p lab'-. |
    sol'-. sol'-. sol'16( \ficta lab' sol' fad') sol'( sib') la'( do'') |
    sib'8-. sib'-. sib'16( do'' sib' la') sib'( re'') do''( mib'') |
    re''8\cresc re'' re''16 mib'' re'' do'' re'' fa'' mib'' sol'' |
    fa''8\f fa'' sol''16 fa'' mib'' re'' mib'' fa'' sol'' la'' |
    sib''4 fa'' r |
    <sib'' sib'>2.:16\ff |
    <sol'' sib'>8\p q sol'' la''!16 sib'' la'' sol'' fa'' mib'' |
    re''8 re'' re'' re'' do'' do'' |
    re''\f mib''16-. fa''-. sol''-. la''-. sib''-. do'''-. re'''-. re'''-. re'''-. re'''-. |
    sib''2.:16\ff |
    sol''8. la''32 fad'' sol''8 la''16 sib'' la'' sol'' fa'' mib'' |
    re''2.:8 |
    re''16-\sug\p fa'' mib'' re'' mib'' fa'' sol'' la'' sib'' la'' sib'' la'' |
    sib''8 fa''4 mib'' re''8 |
    do''2.:16 |
    <do'' la''>:-\sug\f |
    sib'4. \grace do''8 sib'16 la'32 sib' re''8-. fa''-. |
    \grace fa''8 mib''4 re''4.( mib''8) |
    \grace re''8 do''4 sib' r |
    re'''16 do''' sib'' la'' sib''2:16 |
    \grace la''8 sol''4. la''16 sib'' la'' sol'' fa'' mib'' |
    re''8. mib''16 fa''4 la' |
    sib'2 r4 |
  }
  \tag #'secondo {
    fa''4(_\markup\tiny\italic dol. sol'' lab'') |
    re''( mib'' fa'') |
    si'( do'' re'') |
    sol'4 r r | \allowPageTurn
    sol''2.~ |
    sol''4. do''8-.( do''-. do''-.) |
    do''2 si'4 |
    do'' r8 mib''( fa'' sol'') |
    lab''\rf fa'' \grace mib'' re''2 |
    re''8\rf mib'' si'4 do''8. do''16 |
    re''4(\p do'' si') |
    do''8-. do''-. sib'!-. do''-. sib'-. \ficta lab'-. |
    sol'-. lab'-. sib'-. do''-. sib'-. do''-. |
    sib'-. do''-. sib'-. mib''-. mib''-. re''-. |
    mib''4 r8 do''-. sib'-. lab'-. |
    sib'-. do''-. sib'-. mib''-. mib''-. re''-. |
    mib''4 mib'' sib' |
    lab' sol'8-. sol'-. lab'-. lab'-. |
    << sib'2.:8 { s2-\sug\f s4-\sug\p } >> |
    mib'8 do'' do'' do'' do'' do''16 \ficta lab' |
    sol'2:8 sib'8 re'' |
    mib''2 r4 |
    mib'2\f mib'4\p |
    mib'8 mib' mib' do'' sib'16 lab' sol' fa' |
    mib'8\f sol' fa' fa' fa' fa'' |
    mib''4(-\sug\p fa'' sol'') |
    do''( re'' mib'') |
    la'!( sib' do'') |
    fa' r r | \allowPageTurn
    fa''2.~ |
    fa''4 r8 la-.-\sug\f sib-. do'-. |
    \grace mi'8 re' do'16-. re'-. \grace mi'8 re' do'16-. re'-. \grace mi'8 re'( do'16 sib) |
    la4 r8 la-.-\sug\p sib-. do'-. |
    \grace mi'8 re' do'16 re' \grace mi'8 re' do'16 re' \grace mi'8 re' do'16 sib |
    do'2 r4 |
    sib'2.-\sug\f |
    sib'2\fermata r4 |
    la'2. |
    sib'2\fermata fa'4~ |
    fa'4 fa'2 fa'4( |
    sib'2) r8 fa' fa'( sol'16 mib') |
    re'4.( la'8 sib'4)\fermata fa'~ |
    fa' fa'2 \once\slurDashed fa'4( |
    sib'2) r8 fa' \once\slurDashed fa'( sol'16 mib') |
    re'2 mib'4 |
    re'8.\f mib'32 do' re'8. mib'32 do' re'8\p sib |
    sib sib r4 r8 fa' |
    fa' fa' r4 r8 fa' |
    fa'-\sug\cresc fa' r4 r8 mib'' |
    sib'-\sug\f sib' sib'16 sib' sib' sib' mib''4:16 |
    re''4 re'' r |
    <sib' fa''>2.:16\ff <sib' sol''>8\p q q4 r |
    r8 sib' sib' sib' la' la |
    sib8\f do'16-. re'-. mib'-. fa'-. sol'-. la'-. sib'-. sib'-. sib'-. sib'-. |
    <sib' fa''>2.:16\ff |
    <sib' sol''>2: fa''16 mib'' re'' do'' |
    sib'2.:8 |
    sib'16-\sug\p re'' do'' sib' do'' re'' mib'' do'' re'' do'' re'' do'' |
    re''8 re''4 do'' sib'8 |
    sib'2.:16 |
    la'16-\sug\f <do'' la''> q q q2:16 |
    sib'2.:8 |
    sib': |
    la'8 la' sib'4 r |
    <sib' fa''>2.:16 |
    <sib' sol''>16 q q q q4:16 fa''16 mib'' re'' do'' |
    sib'8. do''16 re''4 do'' |
    sib'2 r4 |
  }
>>
