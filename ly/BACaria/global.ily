\key sib \major
\tempo "Andantino" \midiTempo#100
\time 4/4 s1*11 \bar "||"
\tempo "Andantino Grazioso"
\time 3/4 s2.*82 \bar "||"
\time 4/4 s1*5 \bar "||"
\tempo "Andantino"
\time 3/4 s2.*25 \bar "|."
