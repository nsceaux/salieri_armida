\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#10 { Tremo, bell' idol mio, }
  \livretVerse#10 { Ma questo mio timor }
  \livretVerse#10 { Non è tormento. }
  \livretVerse#10 { E' vita dell' amor }
  \livretVerse#10 { E stimolo a goder, }
  \livretVerse#10 { Per lui tutto il’ piacer }
  \livretVerse#10 { Di possederti; oh Dio, }
  \livretVerse#10 { Tutto risento. }
  \livretVerse#10 { Langue nel sen l'ardor, }
  \livretVerse#10 { Langue il desio, }
  \livretVerse#10 { Quando a temer non s'hà, }
  \livretVerse#10 { E troppa sicurtà }
  \livretVerse#10 { Non è contento. }
} #}))
