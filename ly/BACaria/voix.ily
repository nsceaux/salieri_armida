\clef "soprano/treble" \override AccidentalSuggestion.avoid-slur = #'outside
fa''2. mib''8. re''16 |
do''4( re''2) mib''4 |
\grace fa''8 mib''4 re'' r8 sib' sol'' fa'' |
fa''4.(\melisma re''16[ mib'']) mib''2\fermata\trill\melismaEnd |
re''2 r4 <>^\markup\italic ad libitum re''8.[ mib''32 fa''] |
do''4 re''8.[ mib''32 fa''] do''4 re''8.[ mib''32 fa''] |
\grace fa''8 mib''4 r r8 do''16([ mib'']) re''([ sol'']) mib''([ do'']) |
\once\override Script.avoid-slur = #'outside
sib'4.\fermata( do''8) \grace do'' re''4 re''8.[ mib''32 fa''] |
do''4 re''8.[ mib''32 fa''] do''4 re''8.[ mib''32 fa''] |
\grace fa''8 mib''4\fermata r4 r8 do''16([ mib'']) re''([ sol'']) mib''([ do'']) |
sib'4.\fermata( do''8) sib'4 r |
%%
re''2~ re''8[ mib''] |
do''2 re''8[ fa''] |
\grace fa''8 mib''4( re''4.) do''8 |
sib'4 r r |
R2.*2 |
re''2~ re''8[ mib''] |
\grace re''8 do''2 re''8([ fa'']) |
\grace fa''8 mib''4 re''( do'') |
sib' r r |
R2. |
r4 r r8 sib' |
\grace sol''8 fa''2 r4 |
\grace sol''8 fa''4 mib''8[ re''] do''[ si'] |
\grace si'8 do''2~ do''8 fa'' |
\grace fa''8 \ficta mi''2~ mi''8 fa'' |
sol''16[\melisma la'' sol'' la''] sib''8[ sol'']\melismaEnd mi'' sib' |
\grace sib'8 la'2 r4 |
do''8([ fa'']) fa''4. mib''!8 |
\once\tieDashed re''4~\melisma re''16[ mi'' re'' dod''] re''[ fa'' mi'' sol''] |
\once\tieDashed fa''4~ fa''16[ sol'' fa'' mi''] fa''[ la'' sol'' sib''] |
la''4~ la''16[ sib'' la'' sol''] fa''[ mi'' fa'' sol''] |
la''[ sib'' do''' sib''] la''[ sib'' la'' sol''] fa''[ mib''! re'' do''] |
re''4. sib''8[ la'' sol''] |
do''2 sol''4\melismaEnd |
fa''4 r r |
R2. |
\grace la''8 sol''4 fa'' r |
r r do''8 fa'' |
mi''([\melisma fa''16 sol'']) fa''4.\melismaEnd mib''8 |
re''4\melisma re''16[ mi'' re'' dod''] re''[ fa'' mi'' sol''] |
fa''4 fa''16[ sol'' fa'' mi''] fa''[ la'' sol'' sib''] |
la''4. sol''8[ fa'' mib''!] |
re''[ mi''16 fa''] sol''[ la'' sib'' do''' re'''8] re'' |
do''2.~ |
do'' |
sol''\trill~ |
sol''\melismaEnd |
fa''4 r r |
R2.*6 |
sol''2.~ |
sol''4 fa'' mib''! |
re''8[\melisma mib''] fa''4\melismaEnd sol'' |
\grace fa''8 mib''2 r4 |
\grace mib''8 re''4 re''4. fa''8 |
mib''8.([ fa''16] sol''4) r |
R2. |
sol''8[ mib''] \grace re''8 do''4. mib''8 |
re''([ mib''16 fa''] mib''4) re'' |
do'' r sib' |
mib''2 mib''4 |
mib''4.( fa''16[ sol'' \ficta lab''8]) sol''16[ fa''] |
mib''4 r r |
R2. |
mib''4 sol''8[ fa''] mib''[ re''] |
\grace re''8 do''4 sib' r |
mib'2 reb''4 |
do''8[ lab'' sol'' fa''] mi''16[ fa'' lab'' fa''] |
mib''!2( fa''4) |
sol''2 r4 |
mib'2 reb''4 |
do''4.( re''!16[ mib'' re'' do'']) sib'[ lab'] |
sol'4( fa'2) |
mib' r4 |
R2. |
fa''2.~ |
fa''4 mib'' re'' |
do''8.([ re''16] mib''4) re'' |
re'' do'' r |
R2. |
fa''2.~ |
fa''\melisma |
<>^\markup\italic ad libitum sol''16([ fa'' mi'' fa''])\melismaEnd fa''4.( fad''8) |
sol''4.( la''16[ sib'']) la''8 sol'' |
\grace sol''8 fa''2 r4 |
mib''!4.( fa''16[ sol'']) fa''8 mib'' |
mib'' re''4.\fermata re''8.[ mib''32 fa''] |
do''4 re''8.[ mib''32 fa''] do''4 re''8.[ mib''32 fa''] |
\grace fa''8 mib''4 r r8 do''16[ mib''] re''[ sol''] mib''[ do''] |
sib'4.( do''8) \grace do'' re''4\fermata re''8.[ mib''32 fa''] |
do''4 re''8.[ mib''32 fa''] do''4 re''8.[ mib''32 fa''] |
\grace fa''8 mib''4 r r8 do''16[ mib''] re''[ sol''] mib''[ do''] |
sib'2( do''4) |
sib'4 r lab' |
sol'4~ sol'16[\melisma \ficta lab' sol' fad'] sol'[ sib' la'! do''] |
\once\tieDashed sib'4~ sib'16[ do'' sib' la'] sib'[ re'' do'' mib''] |
\once\tieDashed re''4~ re''16[ mib'' re'' do''] re''[ fa'' mib'' sol''] |
fa''8.[ fa''16] sol''[ fa'' mib'' re'']\melismaEnd mib''[ fa'' sol'' la''] |
sib''4 fa'' r |
sib''2. |
sol''4.( la''16[ sib'']) la''[ sol'' fa'' mib''] |
re''2( do''4) |
\grace do''8 re''2 r4 |
sib''2. |
sol''4.\melisma la''16[ sib'']\melismaEnd la''[ sol'' fa'' mib''] |
\once\tieDashed re''2.~\melisma |
re''16[ fa'' mib'' re''] mib''[ fa'' sol'' la''] sib''[ la'' sib'' la''] |
sib''8 fa''4 mib'' re''8 |
do''2.~ |
do''\melismaEnd |
sib'2 r4 |
R2.*6 |
