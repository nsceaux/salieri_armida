Tre -- mo, bell’ i -- dol mi -- o,
bell’ i -- dol mi -- o,
ma ques -- to mio ti -- mor
non è tor -- men -- to,
ma ques -- to mio ti -- mor
non è tor -- men -- to.
É __ vi -- ta dell’ __ a -- mor
e __ sti -- mo -- lo a go -- der,
per lui tut -- to il pia -- cer __
di pos -- se -- der -- ti, oh Dio,
tut -- to ri -- sen -- to,
tut -- to il pia -- cer __ ri -- sen -- to.

Lan -- gue nel sen l’ar -- dor,
lan -- gue il de -- sio, __
quan -- do a te -- mer non s’hà,
e trop -- pa si -- cur -- tà
non è con -- ten -- to,
nò non è __ con -- ten -- to,
nò non è __ con -- ten -- to.

Tre -- mo, bell’ i -- dol mi -- o,
tre -- mo, bell’ i -- dol mio,
bell’ i -- dol mi -- o,
ma ques -- to mio ti -- mor
non è tor -- men -- to,
ma ques -- to mio ti -- mor
non è tor -- men -- to,
non è __ tor -- men -- to,
non è __ tor -- men -- to,
non è __ tor -- men -- to.
