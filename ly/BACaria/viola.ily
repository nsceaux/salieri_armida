\clef "alto" r8 <>_\markup\italic dolce re'-.( re'-. re'-. re'-. re'-. do'-. sib-.) |
la la sib sib sib sib do' do' |
\grace re' do' do' sib sib sib-! re'\rf( mib' re') |
re'2 do'\fermata |
sib r4 \slurDashed sib( |
la4) sib( la) sib( |
\override Script.avoid-slur = #'outside
sol2)\fermata r8 la sib( mib) |
fa2\fermata sib4 \slurSolid sib( |
la) \slurDashed sib( la) sib( |
sol2)\fermata \slurSolid r8 la sib mib' |
fa'4.\fermata fa8 sib4 r |
%%
fa'8( re') fa'( re') fa'4 |
fa'2 fa'4 |
sol'8( mib') fa'4 fa |
sib r r |
R2. |
do'4(\rf sib) r |
fa'8(\p re') fa'( re') fa'4 |
fa'2 fa'4 |
sol'8( mib') fa'4 fa |
sib r r |
R2. |
do'4(\rf sib) r |
re'8(-\sug\p fa') re'( fa') re'( fa') |
fa'( re') re'( sib) re' re' |
fa'4 la la |
sol sol fa |
mi mi mi |
fa r r |
la\p la\f la\p |
sib8 sib' sib' sib' sib' sib' |
la' la' la' la' la' \ficta mi' |
fa'\f fa r4 r8 do' |
fa'4-\sug\f r r |
sib8-\sug\p sib sib sib sib sib |
do'-\sug\f do' do' do' do' do' |
fa4 r fa' |
re'(-\sug\rf do') r |
sib\rf( la) r |
re'(-\sug\rf do') r |
sib la2:8 |
sib8 sib' sib' sib' sib' sol' |
la' la' la' la' la' mi' |
fa' fa' fa' fa' la la |
sib4 r r |
do'2.:8\f |
do':\p |
do: |
do8\f do do do do do |
fa la la la la la |
re' re' do' do' do' do' |
sib sib la la la la |
sib sib do' do' do do |
fa4 fa' r |
R2. | \allowPageTurn
fa'4(_\markup\italic dol. sol' lab') |
re' mib' fa' |
mib' re' do' |
si( re' si) |
do'2 sol'4 |
lab'2 sol'4 |
sol' r8 do'( re' mib') |
fa'2.(-\sug\rf |
mib') |
fa'4\p( sol' sol) |
do'8-. do'-. sib-. lab-. sol-. fa-. |
mib-. fa-. sol-. lab-. sol-. lab-. |
sol-. lab-. sol-. sib-. do'-. lab-. |
sol-. do'-. sib-. lab-. sol-. fa-. |
sol-. \ficta lab-. sol-. sib-. do'-. lab-. |
sol4 r sol |
mib'4 mib'8 mib' fa' fa' |
sol'-\sug\f sol' sol' sol' sol-\sug\p sol |
lab2.:8 |
sib: |
mib'8-. sol'-. fa'-. mib'-. re'-. do'-. |
sib-.-\sug\f \ficta lab-. sol-. fa-. mib-.-\sug\p sol-. |
lab lab lab lab lab lab |
sib2.:8-\sug\f |
mib'4 r r |
mib'(-\sug\p fa' sol') |
\once\slurDashed do'( re' mib') |
\once\slurDashed re'( do' sib) |
la2 sib4 |
fa2 r4 |
R2. |
fa2\f r4 |
R2. |
fa2\p r4 |
mib'2.-\sug\f |
re'2\fermata r4 |
do'2. |
sib2\fermata sib4( |
la) sib( la) sib( |
sol2) r8 la \once\slurDashed sib( mib) |
fa2 sib4\fermata sib( |
la) sib( la) sib( |
sol2) r8 la \once\slurDashed sib( mib') |
fa'2 fa4 |
sib2:8\f re'8\p re' |
mib' mib' r4 r8 mib' |
re' re' r4 r8 la |
sib-\sug\cresc sib r4 r8 do' |
re'-\sug\f re' re' re' do' do' |
sib2 r4 |
re'2.:16\ff |
mib':8\p |
fa'2: fa8 fa |
sib4 r r |
re'2.:16\ff |
mib':8 |
fa'2.:8 |
fa':\p |
fa': |
fa': |
fa:\f |
sib2.:8 |
sol'8 sol' fa' fa' fa' fa' |
mib' mib' re' mib' re' do' |
sib16 do' re' re' re'2:16 |
mib'2.:8 |
fa'4 fa' fa |
sib2 r4 |
