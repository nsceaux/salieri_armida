\clef "bass" sib4\p r r2 |
fa4 r fa, r |
sib r sib, r |
fa2 fa,\fermata |
sib, r4_\markup\italic Con la parte \slurDashed sib( |
la) sib( la) \slurSolid sib( |
\once\override Script.avoid-slur = #'outside sol2)\fermata r8 la sib( mib) |
fa4 fa,\fermata sib,4 sib( |
la) sib( la) \once\slurDashed sib( |
\once\override Script.avoid-slur = #'outside sol2)\fermata r8 la sib( mib) |
fa4.\fermata fa,8 sib,4 r |
%%
sib4 sib sib |
fa mib re |
sol8 mib fa4 fa, |
sib, r r |
R2.*2 |
sib4-\sug\p sib sib |
fa( mib re) |
sol8 mib fa4 fa, |
sib, r r |
R2.*2 |
sib4-\sug\p sib sib |
sib sib sib |
la la la |
sol sol fa |
mi mi mi |
fa4 fa8.\f sol16 sol8.\trill fa32 sol |
la4\p la\f la\p |
sib r r8 sib-! |
la4-! r r8 mi-! |
fa4-!\f r4 r8 do-! |
fa4-!\f r r8 la\p |
sib2.:8 |
do'2:\f do8 do |
fa4 r fa |
re'(-\sug\rf do') r |
sib(\p la) r |
re'(\rf do') r |
sib\p( la8) la la la |
sib4 r r8 sib-! |
la4-! r4 r8 mi-! |
fa fa fa fa la la |
sib4 r sib\f |
do'2.:8 |
do':\p |
do: |
do8\f do do do do do |
fa fa fa fa fa fa |
re' re' do' do' do' do' |
sib sib la la la la |
sib sib do' do' do do |
fa4 fa, r | \allowPageTurn
R2.*3 |
fa2.\p |
fa, |
do2 mib!4 |
fa2 sol4 |
do'4 \clef "tenor" <>^"Violonc. solo" r8 do'( re' mib') |
fa'2.\rf( |
mib') |
fa'4\p sol' sol |
\clef "bass" r8 \grace re' do'-! sib-! lab-! sol-! fa-! |
mib-! fa-! sol-! lab-! sol-! lab-! |
sol-! \ficta lab-! sol-! sib-! do'-! lab-! |
sol-! do'-! sib-! lab-! sol-! fa-! |
sol-!\mf \ficta lab-! sol-! sib-! do'-! lab-! |
sol4 r sol4 |
\ficta lab mib8 mib fa fa |
sol-\sug\f sol sol sol sol,-\sug\p sol, |
lab,2.:8 |
sib,8 sib, sib sib sib sib |
mib' sol' fa' mib' re' do' |
sib-\sug\f lab sol fa mib-\sug\p sol |
lab lab lab lab, lab, lab, |
sib,\f sib, sib, sib, sib sib |
mib4 r r |
R2.*2 |
fa2.\p |
fa,2 sib,4 |
fa,2 r4 |
R2. |
fa,2\f r4 |
R2. | \allowPageTurn
fa,2\p r4 |
mib2.\f |
re2\fermata r4 |
do2. |
sib,2\fermata sib4( |
la)_\markup\italic Con la parte sib( la) sib( |
sol2)\rf r8 la sib( mib) |
fa4. fa,8 sib,4\fermata sib( |
la) sib( la) sib( |
sol2)\rf r8 la( sib mib) |
fa2 fa,4 |
sib,8\f sib, sib, sib, re\p re |
mib mib mib mib mib mib |
re re re re re la, |
sib,\cresc sib, sib, sib, sib, do |
re\f re re re do do |
sib,2 r4 |
re8\ff re re re re re |
mib2.:8\p |
fa8 fa fa fa fa, fa, |
sib,4 r r |
re8\ff re re re re re |
mib2.:8 |
fa: |
fa:\p |
fa8 fa fa fa fa fa |
fa2.:8 |
fa,\f |
sib8 sib sib sib sib sib |
sol sol fa fa fa fa |
mib8 mib re mib re do |
sib,16 do re re re2:16 |
mib2.:8 |
fa4 fa fa, |
sib,2 r4 |
