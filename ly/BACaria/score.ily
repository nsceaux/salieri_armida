\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Armida
      shortInstrumentName = \markup\character Arm.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*5\pageBreak
        s1*4\pageBreak
        \grace s8 s1*2 s2.*2\pageBreak
        \grace s8 s2.*5\pageBreak
        \grace s8 s2.*5\pageBreak
        \grace s8 s2.*5\pageBreak
        \grace s8 s2.*5\pageBreak
        s2.*5\pageBreak
        \grace s8 s2.*5\pageBreak
        s2.*5\pageBreak
        s2.*5\pageBreak
        s2.*7\pageBreak
        \grace s8 s2.*6\pageBreak
        s2.*6\pageBreak
        s2.*5\pageBreak
        s2.*6\pageBreak
        s2.*5\pageBreak
        s2.*5 s1\pageBreak
        \grace s8 s1*4 s2.\pageBreak
        s2.*5\pageBreak
        s2.*5\pageBreak
        s2.*9\pageBreak \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
