\score {
  <<
    \new StaffGroup <<
      \new Staff \with {
        instrumentName = "Oboe solo"
        shortInstrumentName = "Ob."
      } << \global \includeNotes "oboi" >>
      \new Staff \with { \fagottiInstr } << \global { \includeNotes "fagotti" s8 } >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global { \keepWithTag #'secondo \includeNotes "violini" s8 } >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \vlcInstr } << \global \includeNotes "viola" \clef "tenor" >>
      \new Staff \with { \bassoInstr } <<
        \global { \keepWithTag #'tutti \includeNotes "bassi" s8 }
        \origLayout {
          s4 s2*4\break s2*6\pageBreak
          s2*6\break s2*6\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
