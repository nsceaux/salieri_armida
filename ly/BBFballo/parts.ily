\piecePartSpecs
#`((oboi #:instrument "solo")
   (fagotti #:clef "tenor"
            #:music , #{
\quoteViola "BBFbassi"
s8 <>^\markup\tiny "Vcl."
\cue "BBFbassi" { \mmRestInvisible s8 s2*7 \mmRestVisible\restInvisible s8 \restVisible s }
s4 s2*3 s4
s8 <>^\markup\tiny "Vcl."
\cue "BBFbassi" { \mmRestInvisible s8 s2*3 \mmRestVisible\restInvisible s8 \restVisible s }
s4 s2*3 s4
s8 <>^\markup\tiny "Vcl."
\cue "BBFbassi" { \mmRestInvisible s8 s2*7 \mmRestVisible }
#})
   (violino1)
   (violino2)
   (viola)
   (bassi #:score "score-bassi")
   (silence #:on-the-fly-markup ,#{ \markup\tacet #}))
