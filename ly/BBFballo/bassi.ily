\clef "bass" r4 |
R2*7 |
r4 r8 sol\p |
r sol r sol |
r sol r sol |
r sol r sol |
r sol r4 |
R2*3 |
r4 r8 sol |
r sol r sol |
r sol r sol |
r sol r sol |
sol4 r |
R2*7 |
