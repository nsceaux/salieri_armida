\clef "treble" \override AccidentalSuggestion.avoid-slur = #'outside
<<
  \tag #'primo {
    r4 |
    <>_"pizz." sol''8-! sol''-! r4 |
    sol''8-! sol''-! r4 |
    <>_"coll’arco" do''8-! do''( la' si') |
    do''4 r |
    <>_"pizz." sol''8-! sol''-! r4 |
    sol''8-! sol''-! r4 |
    <>_"arco" do''8-! do''( la' si') |
    do'' r sol''4\rf~ |
    sol''2\rf~ |
    << sol''2~ { s4 <>\rf } >> |
    \once\tieDashed sol''2~ |
    sol''4 r |
    <>_"[pizz.]" sol''8-! sol''-! r4 |
    sol''8-! sol''-! r4 |
    <>_"[arco]" do''8-! do''( la' si') |
    do'' r sol''4-\sug\rf~ |
    sol''2~ |
    sol''~ |
    sol''~ |
    sol''4 r |
    <>_"[pizz.]" sol''8-! sol''-! r4 |
    sol''8-! sol''-! r4 |
    <>_"[arco]" do''8-! do''( la' si') |
    do''4 r |
    <>_"[pizz.]" sol''8-! sol''-! r4 |
    sol''8-! sol''-! r4 |
    <>_"[arco]" do''8-! do''( la' si') |
    \custosNote do''8
  }
  \tag #'secondo {
    r4 |
    <>_"pizz." re''8-! do''-! r4 |
    fa''8-! mi''-! r4 |
    R2*2 |
    re''8-! do''-! r4 |
    fa''8-! mi''-! r4 |
    R2 |
    r4 <>^"[arco]" dod'16(-\sug\rf re') re'8 |
    red'16(\rf mi') mi'( sol') fa'( \ficta re' do' si) |
    do' re'32 mi' re'8 dod'16\rf re' re'8 |
    red'16( mi') mi'( sol') fa'( \ficta re' do' si) |
    do' re'32 mi' re'8 r4 |
    <>_"[pizz.]" re''8-! do''-! r4 |
    fa''8-! mi''-! r4 |
    R2 |
    r4 <>^"[arco]" dod'16(\rf re') re'8 |
    red'16\rf( mi') mi'( sol') fa'( re' do' si) |
    do' re'32 mi' re'8 dod'16 re' re'8 |
    red'16( mi') mi'( sol') fa' re' do' si |
    do'16 re'32 mi' re'8 r4 |
    <>_"[pizz.]" re''8-! do''-! r4 |
    fa''8-! mi''-! r4 |
    R2*2 |
    <>_"[pizz.]" re''8-! do''-! r4 |
    fa''8-! mi''-! r4 |
    R2 |
  }
>>
