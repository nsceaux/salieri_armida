\clef "tenor" r4 |
R2*7 |
r4 si16 sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol si sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol r4 |
R2*3 |
r4 si16 sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol si sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol r4 |
R2*7 |
