\clef "alto" r8 do'\mf |
sol'16-. fa'-. mi'8-. r8 do' |
si16-! sol-! do'8-! mi'8.(\rf fa'32 sol') |
la'8( sol' fa' sol') |
mi'-! do'-! r do' |
sol'16-! fa'-! mi'8-! r8 do' |
si16-! sol-! do'8-! mi'-!\rf mi'16.-! fa'32*1/2( sol') |
la'8( sol' fa' sol') |
do'8 r8 si16 sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol si sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol r8 do' |
sol'16 fa' mi'8 r do' |
si16 sol do'8 mi'8.\rf fa'32 sol' |
la'8 sol' fa' sol' |
do' r si16 sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol si sol si sol |
do' sol do' sol re' sol re' sol |
mi' sol si sol r8 do' |
sol'16-! fa'-! mi'8 r do' |
si16 sol do'8 mi'8.(\rf fa'32 sol') |
la'8( sol' fa' sol') |
mi'8-! do'-! r do' |
sol'16 fa' mi'8 r do' |
si16 sol do'8 mi'8.(\rf fa'32 sol') |
la'8( sol' fa' sol') |
\custosNote do'8
