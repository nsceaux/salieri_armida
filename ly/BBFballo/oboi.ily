\clef "treble" \override AccidentalSuggestion.avoid-slur = #'outside
mi''8.(\mf fa''32 re'') |
si'8( do'') sol''8. la''32 fa'' |
re''8( mi'') do''8.-\sug\rf re''32 mi'' |
fa''8-. mi''( \grace mi''16 re''8 do''16 si') |
do''16( sol') sol'8 mi''8.( fa''32 re'') |
si'8 do'' sol''8. la''32 fa'' |
re''8 mi'' do''8-! do''16.-! re''32*1/2( mi'') |
fa''16( fa''8 mi''16) \grace mi''8 re'' do''16 si' |
do''8 r dod''16(-\sug\rf re'') re''8 |
red''16(-\sug\rf mi'') mi''( sol'') fa''( \ficta re'' do'' si') |
do'' re''32 mi'' re''8 dod''16-\sug\rf re'' re''8 |
red''16( mi'') mi''( sol'') fa''( \ficta re'' do'' si') |
do'' re''32 mi'' re''8 \once\slurDashed mi''8.( fa''32 re'') |
si'8( do'') sol''8. la''32 fa'' |
re''8( mi'') \once\slurDashed do''8.(-\sug\rf re''32 mi'') |
fa''8 mi'' \grace mi''16 re''8 do''16 si' |
do''8 r dod''16(\rf re'') re''8 |
red''16(-\sug\rf mi'') mi''( sol'') fa''( \ficta re'' do'' si') |
do''16 re''32 mi'' re''8 dod''16( re'') re''8 |
red''16( mi'') mi''( sol'') fa'' \ficta re''( do'' si') |
do''16 re''32 mi'' re''8 \once\slurDashed mi''8.( fa''32 re'') |
si'8 do'' sol''8. la''32 fa'' |
re''8 mi'' \once\slurDashed do''8.(-\sug\rf re''32 mi'') |
fa''8 mi'' \grace mi''16 re''8 do''16 si' |
do'' sol' sol'8 \once\slurDashed mi''8.( fa''32 re'') |
si'8 do'' \once\slurDashed sol''8.( la''32 fa'') |
re''8 mi'' do''-\sug\rf do''16. re''32*1/2 mi'' |
fa''16 fa''8 mi''16 \grace mi'' re''8 do''16 si' |
\custosNote do''8
