\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Violoncelli" } <<
      \global \includeNotes "viola" \clef "tenor"
    >>
    \new Staff \with { instrumentName = "Basso" } <<
      \global { \keepWithTag #'tutti \includeNotes "bassi" s8 }
    >>
  >>
  \layout { indent = \largeindent }
}
