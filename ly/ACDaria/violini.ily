\clef "treble"
<<
  \tag #'primo {
    si'2\f si'\p |
    si' si'\f |
    si'\p lad' |
    si' si\f |
    do'!\p si4 red'' |
    mi''8\f re''! do'' si' la' sol' fad' mi' |
    do''2
  }
  \tag #'secondo {
    sol'2-\sug\f fad'-\sug\p |
    sol' \ficta fad'-\sug\f |
    mi'-\sug\p mi' |
    red' red'-\sug\f |
    \ficta red'-\sug\p mi'4 fad' |
    sol'-\sug\f r8 si'-\sug\f la' sol' fad' mi' |
    mi'2
  }
>> r4 la8( lad) |
si2 <si'' si'> |
sol''4 r
