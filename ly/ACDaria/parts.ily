\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Ismene
  \livretVerse#10 { Mostri i più crudi, e infesti }
  \livretVerse#10 { Della magion di Dite }
  \livretVerse#10 { Da' cupi antri funesti, }
  \livretVerse#10 { Al cenno mio, venite. }
}

       #}))
