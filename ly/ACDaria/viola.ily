\clef "alto" mi'2\f red'\p |
mi' re'!\f |
do'\p fad' |
\ficta fad' si\f |
la\p sol4 \ficta fad |
mi-\sug\f r8 si'-\sug\f la' sol' fad' mi' |
la'2 r4 \once\slurDashed la8( lad) |
si2 si' |
mi'4 r
