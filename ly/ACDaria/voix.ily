\clef "alto/treble" mi''4 si'8. si'16 si'4. si'8 |
sol'4 si' r2 |
si'4 sol'8 fad' mi'4. fad'8 |
red'4 red' r r8 si |
do'4. do'8 si4 red''8 red'' |
mi''[ re''!] do'' si' la' sol' fad' mi' |
do''4 la' r2 |
r r4 r8 si' |
sol'4 mi'
