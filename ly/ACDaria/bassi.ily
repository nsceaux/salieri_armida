\clef "bass" mi2\f red-\sug\p |
mi re!\f |
do\p do' |
si si,\f |
la,2\p sol,4 fad, |
mi,\f r8 si\f la sol fad mi |
la2 r4 la,8( lad,) |
si,2 si |
mi4 r
