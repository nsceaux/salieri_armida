\clef "treble" r4 |
<<
  \tag #'primo {
    r4 re'' do'' r |
    re''2 r
    r la' |
    R1 |
    la'4 r si'! r |
    R1 |
    r2 do'' |
    r4 fad''\f fad'' r |
    re''2 r |
    dod''2 r |
    R1 |
    r2 la'4 r |
    fad''16\fp fad'' fad'' fad'' fad''4:16 fad''2:16 |
    \ficta fad'': fad'': |
    \ficta fad'': red'': |
    red''4 r mi''
  }
  \tag #'secondo {
    r4 fa' fa' r |
    fa'2 r |
    r \sug mi' |
    R1 |
    fa'4 r mi' r |
    R1 |
    r2 mi' |
    r4 re''-\sug\f dod'' r |
    fad'2 r |
    sold'2 r |
    R1 |
    r2 dod'4 r |
    \ficta dod''2:16 dod'': |
    \ficta dod'': \sug dod'': |
    \ficta dod'': si': |
    la'4 r si'
  }
>> r8. <sold'? mi''>16\f |
q8. <si' sold''>16 q8. <mi'' si''>16 q4 r |
r2 r4 r8. <sold'? mi''>16 |
q8. <si' sold''>16 q8. <mi'' si''>16 q8. <si' sold''>16 <si' mi''>8. <mi' si'>16 |
sold'4 r <<
  \tag #'primo {
    re'!2\p~ |
    re'1 |
    do'!4 r r mi''~ |
    mi''1 |
    fa''2 r |
    R1*2 |
    fa''4 r re'' r |
    R1*2 |
    re''4 r re'' r |
    fa''2 r |
    mib'' r |
  }
  \tag #'secondo {
    si2-\sug\p~ |
    si1 |
    la4 r r sib'~ |
    sib'1 |
    do''2 r |
    R1*2 |
    sib'4 r la' r |
    R1*2 |
    sib'4 r si' r |
    re''2 r |
    sol' r |
  }
>>
r2 r4 <re' la' fad''>4 |
<re' sib' sol''>2 r |
