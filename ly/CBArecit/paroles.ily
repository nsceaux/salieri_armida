Cor -- ri, Re -- gi -- na,

Mi -- se -- ra me! che av -- ven -- ne?

Il tuo Ri -- nal -- do…
fuor del chiu -- so re -- cin -- to

io l’in -- con -- trai
coll’ i -- gno -- to guer -- rier…

de tuoi cu -- sto -- di
la trup -- pa sbi -- go -- ti -- ta.

Ah ta -- ce -- te, v’in -- ten -- do. Io son tra -- di -- ta.
Fug -- ge l’in -- de -- gno? oh stel -- le! e i giu -- ra -- men -- ti…
le pro -- mes -- se… la fe -- de… in que -- sto sta -- to!…
sen -- za pur dir -- mi ad -- di -- o!… Nu -- mi! e che fan -- no,
a que -- ste di per -- fi -- dia i -- ni -- que pro -- ve,
i ful -- mi -- ni im -- po -- ten -- ti in man di Gio -- ve?
Ven -- det -- ta, o Dei, ven -- det -- ta… A chi la chie -- do?
da chi la spe -- ro ohi -- mè? No non mi re -- sta
fuor, che ne’ miei so -- spi -- ri al -- tra spe -- ran -- za.
Que -- sto che sol m’a -- van -- za
in -- fe -- li -- ce soc -- cor -- so
ah non si per -- da al -- men. Mi veg -- ga il per -- fi -- do
sup -- pli -- ce a’ suoi pie -- di chie -- der mer -- ce -- de,
in -- non -- dar -- li di pian -- to, e se non sen -- te
qual -- che pie -- tà per l’in -- fe -- li -- ce Ar -- mi -- da,
m’ab -- ban -- do -- ni il cru -- del, ma pria m’uc -- ci -- da.
