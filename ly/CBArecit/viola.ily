\clef "alto" r4 |
r sib la! r |
sib2 r |
r dod' |
R1 |
re'4 r sold' r |
R1 |
r2 la' |
r4 si-\sug\f lad r |
si2 r |
mid'2 r |
R1 |
r2 fad'4 r |
la'2:16-\sug\fp la': |
la': la': |
la': la': |
la'4 r sold' r8. mi'16-\sug\f |
mi'8. mi'16 mi'8. sold'16 sold'4 r |
r2 r4 r8. si'16 |
si'8. si'16 si'8. sold'16 sold'8. mi'16 sold'8. sold'16 |
mi'4 r mi2-\sug\p~ |
mi1 |
mi4 r r \once\tieDashed do''~ |
do''1 |
do''2 r |
R1*2 |
re''4 r fad' r |
R1*2 |
\sug sol'4 r sol' r |
sol'2 r |
do'' r |
r r4 re' |
sol'2 r |
