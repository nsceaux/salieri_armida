\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ismene
    \livretVerse#12 { Corri, Regina, }
    \livretPers Armida
    \livretVerse#12 { Misera me! che avvenne? }
    \livretPers Ismene
    \livretVerse#12 { Il tuo Rinaldo… }
    \livretVerse#12 { Fuor del chiuso recinto }
    \livretPers Una delle seguaci
    \livretVerse#12 { Io l’incontrai }
    \livretVerse#12 { Coll’ ignoto guerrier… }
    \livretPers Un altra delle seguaci
    \livretVerse#12 { De tuoi custodi }
    \livretVerse#12 { La truppa sbigottita. }
  }
  \null
  \column {
    \livretPers Armida
    \livretVerse#12 { Ah tacete, v’intendo. Io son tradita. }
    \livretVerse#12 { Fugge l’indegno? Oh stelle! e i giuramenti… }
    \livretVerse#12 { Le promesse… la fede… in questo stato!… }
    \livretVerse#12 { Senza pur dirmi addio!… Numi! e che fanno, }
    \livretVerse#12 { A queste di perfidia inique prove, }
    \livretVerse#12 { I fulmini impotenti in man di Giove? }
    \livretVerse#12 { Vendetta, o Dei, vendetta… A chi la chiedo? }
    \livretVerse#12 { Da chi la spero ohimé? No non mi resta }
    \livretVerse#12 { Fuor, che ne’ miei sospiri altra speranza. }
    \livretVerse#12 { Questo che sol m’avanza }
    \livretVerse#12 { Infelice soccorso }
    \livretVerse#12 { Ah non si perda almen. Mi vegga il perfido }
    \livretVerse#12 { Supplice à piedi suoi chieder mercede; }
    \livretVerse#12 { Inondarli di pianto, e se non sente }
    \livretVerse#12 { Qualche pietà dell’ infelice Armida, }
    \livretVerse#12 { M’abbandoni il crudel, ma pria m’uccida. }
  }
  \null
} #}))
