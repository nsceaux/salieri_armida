\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character I. s4 s2
        \set Staff.shortInstrumentName = \markup\character A. s2 s4
        \set Staff.shortInstrumentName = \markup\character I. s2. s1*3 s2.
        \set Staff.shortInstrumentName = \markup\character A. }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s4 s1*3\pageBreak
        \grace s8 s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
