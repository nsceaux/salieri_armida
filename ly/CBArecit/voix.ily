\ffclef "alto/treble" <>^\markup\character Ismene
re''8 re''16 mib'' |
\ficta mib''8 sib' r4
\ffclef "soprano/treble" <>^\markup\character Armida
do''8 do''16 do'' fa''8 do'' |
re'' re''
\ffclef "alto/treble" <>^\markup\character Ismene
sib'16 sib' sib' sib' fa'8 r fa' fa' |
\ficta sib'4 sib'8 sib' la' la' la' la'16 sib' |
\grace la'8 sol'4 r8 mi'16 fa' sol'4 sol'8 la' |
fa'4 r16 la' la' re'' si'!8 si' r si' |
si' si' si' si' sold' sold'
\ffclef "soprano/treble" <>^\markup\character Armida
si'8. mi''16 |
mi''8 si' r do'' la' la' mi'' red''16 mi'' |
mi''8 si' r4 fad'' dod''8 dod'' |
re'' re'' r \ficta fad'' fad'' si' r16 si' dod'' re'' |
dod''8 dod'' r dod''16 dod'' dod''8 sold' r sold' |
si' si' r16 si' si' dod'' re''8 re'' r4 |
si'8 si'16 si' dod''8. sold'16 la'8 la' r4 |
fad''8 dod'' r dod''16 dod'' la'8 la' r la' |
la' la' la' si' \ficta dod'' dod'' r16 dod'' si' dod'' |
la'8 la' r la' red'' red'' mi'' fad'' |
\ficta fad'' la' r16 la' la' sold' si'8 si' r4 |
r2 r4 r8 si' |
mi'' mi'' r16 si' mi'' sold'' mi''8 mi'' r4 |
R1 |
r2 r4 r16 si' si' si' |
sold'8 sold' r16 si' si' do''! re''8 re'' r si' |
do''!4 r do''8 do''16 re'' mi''8 mi'' |
mi'' mi''16 mi'' fa''8 sol'' sol'' sib' sib' sib'16 do'' |
la'8 la' r4 la'8 la'16 la' do''8 sib' |
do''8 do'' r do''16 do'' fa''4 fa''8 sol'' |
mib''8 mib'' do'' do''16 re'' mib''4 do''8 re'' |
sib'4 r4 r16 sib' do'' re'' re'' la' la'8 |
la'16 la' la'8 la' sib' do'' do'' mib'' re''16 mib'' |
do''8 do'' r la'16 sib' do''4 la'8 sib' |
sol'8 sol' r16 sol' si'! do'' re''8 re'' re'' re''16 mib'' |
fa''4 r8 fa'' fa'' fa'' sol'' re'' |
mib'' mib'' r do''16 do'' sol''4 mib''8 re'' |
do''4 r16 do'' do'' sib' sol'8 sol' r4 |
R1 |

