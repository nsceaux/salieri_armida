\clef "bass" r4 |
r sib la! r |
sib2 r |
r dod |
R1 |
re2 sold |
R1 |
r2 la |
r4 si-\sug\f lad r |
si2 r |
mid2 r |
R1 |
r2 fad4 r |
fad?2:16\fp fad: |
fad?: fad: |
fad?: fad: |
fad?4 r mi r8. mi16-\sug\f |
mi4 mi mi r |
R1 |
mi4 mi mi mi |
mi r \once\tieDashed sold,2\p~ |
sold,1 |
la,4 r r sol~ |
sol1 |
fa2 r | \allowPageTurn
R1*2 |
re4 r fad r |
R1*2 |
\sug sol4 r fa r |
si,!2 r |
do r |
r r4 re |
sol2 r |
