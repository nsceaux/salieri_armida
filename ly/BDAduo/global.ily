\key do \major
\midiTempo#92
\once\override Score.TimeSignature.stencil = ##f \time 4/4
s1 \bar "||" \key sol \major s2.
\tempo "Allegro agitato" \midiTempo#120
s4 s1*45 \bar "||"
\endMark "Segue subito"
