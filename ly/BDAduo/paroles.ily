\tag #'(rinaldo basse) {
  - sì?
}
\tag #'(armida basse) {
  Soc -- cor -- so, o De -- i!
  Ahi -- mè! son tra -- di -- ta,
  mi pal -- pi -- ta il co -- re,
  soc -- cor -- so, pie -- tà,
  soc -- cor -- so, pie -- tà.
}
\tag #'(rinaldo basse) {
  Che di -- ci? ah mia vi -- ta,
  qual nuo -- vo ter -- ro -- re
  tre -- ma -- re ti fá?
}
\tag #'(armida basse) {
  M’op -- pri -- me l’af -- fan -- no.
}
\tag #'(rinaldo basse) {
  Ah pal -- pi -- to anch’ i -- o.
}
\tag #'(armida basse) {
  Che dub -- bio ti -- ran -- no!
}
\tag #'(rinaldo basse) {
  Ma spie -- ga -- ti.
}
\tag #'(armida basse) {
  Oh Di -- o!
}
\tag #'(rinaldo basse) {
  Ma par -- la?
}
\tag #'(armida basse) {
  Non só.
  M’op -- pri -- me l’af -- fan -- no.
}
\tag #'(rinaldo basse) {
  Ma spie -- ga -- ti.
}
\tag #'(armida basse) {
  Oh Di -- o!
}
\tag #'(rinaldo basse) {
  Ma par -- la?
}
\tag #'(armida basse) {
  Non só.
}
{
  In tan -- to pe -- ri -- glio
  tal ve -- lo ho sul ci -- glio,
  che ben non com -- pren -- do
  che par -- lo, che fó,
  che ben non com -- pren -- do
  che par -- lo, che fó,
  che ben non com -- pren -- do
  che par -- lo, che fó,
  che par -- lo, che fó.
}
