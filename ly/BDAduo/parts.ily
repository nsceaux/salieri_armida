\piecePartSpecs
#`((oboi #:score-template "score-deux-voix" #:indent 0)
   (fagotti #:music , #{ <>
\quoteBassi "BDAbassi"
<>^\markup\tiny "B."
\cue "BDAbassi" { \mmRestInvisible s1*16 \mmRestVisible }
s1*3
<>^\markup\tiny "B."
\cue "BDAbassi" { \mmRestInvisible s1*9 \mmRestVisible }
                          #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Armida
    \livretVerse#12 { Soccorso, o Dei! }
    \livretVerse#10 { Ahime! son tradita, }
    \livretVerse#10 { Mi palpita il core, }
    \livretVerse#10 { Soccorso, pietà. }
    \livretPers Rinaldo
    \livretVerse#10 { Che dici? ah mia vita, }
    \livretVerse#10 { Qual nuovo terrore }
    \livretVerse#10 { Tremare ti fà? }
    \livretPers Armida
    \livretVerse#10 { M'opprime l'affano. }
    \livretPers Rinaldo
    \livretVerse#10 { Ah palpito anch'io. }
    \livretPers Armida
    \livretVerse#10 { Che dubbio tiranno! }
  }
  \null
  \column {
    \livretPers Rinaldo
    \livretVerse#10 { Ma spiegati. }
    \livretPers Armida
    \livretVerse#10 { Oh Dio! }
    \livretPers Rinaldo
    \livretVerse#10 { Ma parla. }
    \livretPers Armida
    \livretVerse#10 { Non sò. }
    \livretPers A 2
    \livretVerse#10 { In tanto periglio }
    \livretVerse#10 { Tal velo ho sul ciglio, }
    \livretVerse#10 { Che ben non comprendo }
    \livretVerse#10 { Che parlo, che fò. }
  }
  \null
} #}))

