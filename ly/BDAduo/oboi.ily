\clef "treble" R1*16 |
\twoVoices #'(primo secondo tutti) <<
  { sib''1 |
    la''4 s sol''2 |
    fad''4 la'' re'''\fermata }
  { sol''1 |
    fad''4 s dod''2 |
    re''4 fad'' fad''\fermata }
  { s1\p | s4 r s2\f | }
>> r4 |
R1*3 |
<>-\sug\pp \twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''2 fad'' | sol'' }
  { sib'1 | la' | sib'2 }
>> r2 |
R1*5 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''2 |
    mi'' fad'' |
    sol''4 r8 re'' \grace la''16 sol''8 fad''16( sol'') la''8-. sol''-. |
    fad''4 fad'' }
  { si'2 |
    do'' la' |
    si'4 r mi''2 |
    la'4 la' }
  { s2\mf | s1\f | s4. s8\f }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 re'' \grace la''16 sol''8 fad''16( sol'') la''8-. sol''-. |
    fad''4 fad'' }
  { r2 mi''2 | la'4 la' }
  { s4. s8\f }
>> r2 |
R1*2 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''16( mi''8.) mi''4 }
  { si'16( do''8.) do''4 }
>> r2 |
R1*3 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''16( mi''8.) mi''4 }
  { si'16( do''8.) do''4 }
>> r2 |
R1 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { sol' }
>> r2 |
R1*2 |
