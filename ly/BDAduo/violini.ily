\clef "treble"
<<
  \tag #'primo { sib'2 r | }
  \tag #'secondo { re'2 r | }
>>
r4 <la' dod'' mi''>\f <re' la' fad''> r |
<<
  \tag #'primo {
    r8 sol''\p r sol'' r sol'' r sol'' |
    r fad'' r fad'' r sol'' r re'' |
    r mi'' r mi'' r re'' r re'' |
    r do'' r si' r re'' r re'' |
    r mi'' r mi'' r fad'' r fad'' |
    r sol'' r sol'' r mi'' r mi'' |
    r re'' r re'' r re'' r re'' |
    r re'' r re'' r re'' r re'' |
    r re'' r re'' r re'' r re'' |
    r re'' r re'' r mib'' r re'' |
    r do'' r sib' r la' r sib' |
    r la' la' la' la' la' la' la' |
    la'[ la' la'] la'\f la'2:8\p |
    la'8[ la' la'] la'\f r la'\p r la' |
    r sib' r sib' r sib' r sib' |
    r la' r re'' sol''2:16\f |
    <la' fad''>4 <la' la''> re'''\fermata r |
    r mi''!-!\p mi''-! r |
    r re''-! re''-! r |
    r do''-! do''-! r |
    r8 sib'-\sug\pp r sib' r sib' r sib' |
    r la' r la' r la' r la' |
    r \ficta sib' r sib' r re'' r re'' |
    r mib'' r mib'' r mib'' r sol''\f |
    r fad''\f r sol'' r re''\p r re'' |
    r mib'' r mib'' r re'' r re'' |
    sol'2:8\cresc sol': |
    la':16 si': |
    do'':\mf re'': |
    mi'':\f <la' fad''>: |
    <si' sol''>4
  }
  \tag #'secondo {
    r8 si'-\sug\p r si' r mi'' r mi'' |
    r la' r la' r sol' r sol' |
    r sol' r sol' r sol' r sol' |
    r fad' r sol' r sol' r sol' |
    r sol' r la' r la' r la' |
    r sol' r si' r sol' r sol' |
    r si' r si' r la' r la' |
    r sib' r sib' r sib' r sib' |
    r la' r sib' r sib' r sib' |
    r la' r \ficta sib' r sol' r sol' |
    r fad' r sol' r re' r re' |
    r fad' fad' fad' fad'2:8 |
    fad'8[ fad' fad'] fad'-\sug\f fad'2:-\sug\p |
    fad'8[ fad' fad'] fad'-\sug\f r fad'-\sug\p r fad' |
    r sol' r sol' r sol' r sol' |
    r fad' r fad' dod''2:16-\sug\f |
    <fad' re''>: q4\fermata r |
    r sol'-!-\sug\p sol'-! r |
    r sol'-! sol'-! r |
    r sol'-! fad'-! r |
    r8 sol'-\sug\pp r sol' r sol' r sol' |
    r sol' r sol' r fad' r fad' |
    r sol' r sol' r sol' r sol' |
    r sol' r sol' r sol' r mib''-\sug\f |
    r re''-\sug\f r re'' r sol'\p r sol' |
    r sol' r sol' r fad' r fad' |
    sol'2:8\cresc sol': |
    fad':16 fa': |
    mi':-\sug\mf si': |
    do'':-\sug\f <re' do''>: |
    <re' si'>4
  }
>> r8 re''\f \grace la''16 sol''8 fad''16( sol'') la''8-. sol''-. |
\grace sol''8 fad''4 fad'' <<
  \tag #'primo {
    r8 do''\p r do'' |
    r si'(-. si'-.)
  }
  \tag #'secondo {
    r8 la'-\sug\p r la' |
    r sol'(-. sol'-.)
  }
>> re''8\f\noBeam \grace la''16 sol''8 fad''16( sol'') la''8 sol'' |
\grace sol''8 fad''4 fad'' <<
  \tag #'primo {
    r8 do''\p r do'' |
    r si' r si' r re'' r re'' |
    r mi'' r mi'' r fad'' r fad'' |
    sol''16(\f mi''8.) mi''8 mi'' r mi''\p r mi'' |
    r re'' r re'' r do'' r do'' |
    r si' r si' r re'' r re'' |
    r mi'' r mi'' r fad'' r fad'' |
    sol''16(\f mi''8.) mi''8 mi'' r mi''\p r mi'' |
    r re'' r re'' r do'' r do'' |
    << { sol''[ sol''] } \\ { si'\f[ si'] } >> r sol''\p r mi'' r mi'' |
    r re'' r re'' r la' r la' |
  }
  \tag #'secondo {
    r8 la'-\sug\p r la' |
    r sol' r sol' r si' r si' |
    r do'' r do'' r la' r la' |
    si'16(-\sug\f do''8.) do''8 do'' r do''-\sug\p r do'' |
    r8 si' r si' r la' r la' |
    r sol' r sol' r si' r si' |
    r do'' r do'' r la' r la' |
    si'16(-\sug\f do''8.) do''8 do'' r do''-\sug\p r do'' |
    r si' r si' r la' r la' |
    <sol' sol>8-\sug\f q r re''-\sug\p r do'' r do'' |
    r si' r si' r fad' r fad' |
  }
>>
sol'16\f sol'' fad'' mi'' re'' do'' si' la' sol' sol' fad' mi' re' do' si la |
