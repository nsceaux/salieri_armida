\clef "bass" sol2 r |
r4 la\f re r |
sol8\p r sol r dod' r dod' r |
re' r re' r si r si r |
do' r do' r si r si r |
la r sol r si r si r |
do' r do' r re' r re' r |
mi' r mi' r do' r do' r |
re' r re' r re r re r |
sol r sol r sol r sol r |
do' r sib r sib r sib r |
do' r sib r do' r sib r |
la r sol r fad r sol r |
re r re r re, r <>^"Violoncelli" re'4 |
mib'4(\rf re') r4 re' |
mib'(\rf re') re'8 r re' r |
re' r re' r re' r re' r |
re' r re r <>^"Bassi" mib8\f mib mib mib |
re2:8 re4\fermata r4 |
r do'-!\p do-! r |
r si!-! si,-! r |
r la-! la,-! r |
sol8\pp r sol r mib r mib r |
do r do r re r re r |
sol r sol r sib r sib r |
do' r do' r do' r do'\f r |
do'\f r sib r sib\p r sib r |
do' r do' r re' r re r |
sol8\cresc sol sol sol sol2:8 |
sol: sol: |
sol:\mf sol: |
sol:\f sol: |
sol4 sol, dod'2\f |
re'4 re fad8\p r re r |
sol r sol, r dod'2\f |
re'4 re fad8\p r re r |
sol r sol r sol r sol r |
do' r do' r re' r re' r |
sol\f r do' r do'\p r do' r |
re' r re' r re r re r |
sol r sol r sol r sol r |
do' r do' r re' r re' r |
sol\f r do' r do'\p r do' r |
re' r re' r re r re r |
si\f r si r do'\p r do' r |
re' r re' r re r re r |
sol4 r sol r |
