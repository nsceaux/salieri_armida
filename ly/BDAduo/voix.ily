<<
  \tag #'(armida basse) {
    <<
      \tag #'basse { s4 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { \clef "soprano/treble" r4 }
    >> r8 sol'' sol'' dod'' r re'' |
    re'' la' r4 r r8 re'' |
    sol''2 r4 la''8 sol'' |
    fad''[ mi''] re''4 r r8 re'' |
    mi''4 r re'' r8 re'' |
    \grace re''8 do''4 si' r4 r8 re'' |
    \grace re''8 mi''4 mi'' r r8 fad'' |
    \grace fad''8 sol''2 r4 r8 mi'' |
    \grace mi''8 re''4 re'' r r8 fad' |
    sol'4 r <<
      \tag #'basse { s2 s1*3 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 R1*3 r2 }
    >> r4 r8 re'' |
    \grace fa''8 mib''4 re'' r r8 re'' |
    \grace fa''4 mib'' re'' <<
      \tag #'basse { s2 s1 s4. \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 R1 r4 r8 }
    >> re''8 sol''4 sol''8 sol'' |
    fad''4 re'' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2^\fermata r2 }
    >> r4 mi'' |
    dod'' re'' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 r2 }
    >> r4 re'' |
    sol'4 r r r8 sib' |
    \grace sib' la'4 la' r r8 re'' |
    sib'4 sol' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 r2 }
    >> r4 sol'' |
    fad'' sol'' <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 r2 }
    >> r4 re'' |
    sol' r4 r4 r8 sol' |
    la'2 si'4. si'8 |
    do''2 re''4. re''8 |
    mi''2 fad''4. fad''8 |
    sol''4 re'' r2 |
    r4 r8 re'' \grace re'' do''4 do''8 do'' |
    \grace do''8 si'4 si' r2 |
    r4 r8 re'' \grace re'' do''4 do''8 do'' |
    \grace do''8 si'2 r4 r8 re'' |
    mi''2 fad''4. fad''8 |
    sol''16[ mi''8.] mi''4 r r8 mi'' |
    \grace mi''8 re''4 re'' r4 r8 do'' |
    \grace do''4 si'2 r4 r8 re'' |
    \grace re''4 mi''2 fad''4. fad''8 |
    sol''16[ mi''8.] mi''4 r r8 mi'' |
    \grace mi''8 re''4 re'' r r8 la' |
    sol''2 r4 r8 mi'' |
    \grace mi'' re''4 re'' r4 r8 fad' |
    sol'4 r r2 |
  }
  \tag #'(rinaldo basse) {
    \clef "soprano/treble"
    \tag #'basse <>^\markup\character Rin.
    sib'4
    <<
      \tag #'basse { s2. s1*8 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r4 r2 | R1*8 | r2 }
    >> r4 r8 re'' |
    fad'4 sol' r4 re''8 sib' |
    fad'4 sol'8 re'' mib''4 re''8 re'' |
    do''4 sib'8 sib' la'4 sib'8 sol' |
    re''4 r <<
      \tag #'basse { s2 s1 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 R1 r2 }
    >> r4 r8 re'' |
    re''2 sib'4. sol'8 |
    fad'4 <<
      \tag #'basse { re'8 s8 s2 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo r4 }
      \tag #'rinaldo { re'4 r2 r2 r4\fermata }
    >> r8 re'' |
    mi''!8. do''16 do''4 <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 r2 }
    >> r4 re'' |
    do'' la' <<
      \tag #'basse { s2 s1*2 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 R1*2 r2 }
    >> r4 re'' |
    mib''8. do''16 do''4 <<
      \tag #'basse { s2 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 r2 }
    >> r4 re'' |
    mib'' do''
    \tag #'rinaldo {
      r2 |
      r r4 r8 sol' |
      fad'2 fa'4. fa'8 |
      mi'2 si'4. si'8 |
      do''2 la'4. la'8 |
      si'4 si' r2 |
      r4 r8 si' \grace si'8 la'4 la'8 la' |
      \grace la'8 sol'4 sol' r2 |
      r4 r8 si' \grace si' la'4 la'8 la' |
      \grace la'8 sol'2 r4 r8 si' |
      do''2 la'4. la'8 |
      si'16[ do''8.] do''4 r r8 do'' |
      si'4 si' r r8 la' |
      \grace la'4 sol'2 r4 r8 si' |
      \grace si'4 do''2 la'4. la'8 |
      si'16[ do''8.] do''4 r r8 do'' |
      \grace do''8 si'4 si' r r8 fad' |
      sol'2 r4 r8 do'' |
      \grace do''8 si'4 si' r r8 la' |
      sol'4 r r2 |
    }
  }
>>
