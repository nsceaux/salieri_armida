\clef "bass" R1*16 |
re'1-\sug\p |
re'4 r mib2:8-\sug\f |
re: re4\fermata r4 |
R1*9 |
sol1-\sug\cresc~ |
sol~ |
sol-\sug\mf~ |
sol-\sug\f |
sol4 r dod'2\f |
re'4 re r2 |
r dod'\f |
re'4 re r2 |
sol8 r sol r sol r sol r |
do' r do' r re' r re' r |
sol-\sug\f r do' r do'-\sug\p r do' r |
re' r re' r re r re r |
sol r sol r sol r sol r |
do' r do' r re' r re' r |
sol\f r do' r do'\p r do' r |
re' r re' r re r re r |
si\f r si r do'\p r do' r |
re' r re' r re r re r |
sol4 r sol r |
