\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with {
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Arm.
      } \withLyrics <<
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Rin.
      } \withLyrics <<
        \global \keepWithTag #'rinaldo \includeNotes "voix"
      >> \keepWithTag #'rinaldo \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s1*4\pageBreak
          s1*3\pageBreak
          \grace s8 s1*3 s2 \bar "" \pageBreak
          s2 s1*2\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*6\pageBreak
          s1*4\pageBreak
          \grace s8 s1*3\pageBreak
          \grace s8 s1*5\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
