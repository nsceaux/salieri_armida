\clef "alto" sol'2 r |
r4 la'\f re' r |
r8 re''\p r re'' r la' r la' |
r la' r re'' r re'' r si' |
r do'' r do'' r si' r si' |
r la' r sol' r si' r si' |
r do'' r do'' r re'' r re'' |
r mi'' r mi'' r do'' r do'' |
r sol' r sol' r fad' r fad' |
r sol' r sol' r sol' r sol' |
r fad' r sol' r sol' r sol' |
r fad' r sol' r do'' r sib' |
r la' r re' r fad' r sol' |
r re' re' re' re'4 r |
r r8 re'\f re'8\p re' re' r |
r4 r8 re'\f r8 re'\p r re' |
r re' r re' r re' r re' |
r re' r re' mib'2:8\f |
re'16 la' la' la' la'4:16 la'4\fermata r4 |
r do''-!-\sug\p do''-! r |
r si'!-! si'-! r |
r la'-! la'-! r |
r8 re'-\sug\pp r re' r mib' r mib' |
r \ficta mib' r mib' r re' r re' |
r re' r re' r sib r sib |
r do' r do' r do'' r do''\f |
r la'-\sug\f r sib' r sib'-\sug\p r sib' |
r do'' r do'' r la' r la' |
sol'2:8\cresc sol': |
sol': sol': |
sol':\mf sol': |
sol':16\f sol': |
sol'4 r mi'!2-\sug\f |
la4 la r8 re'\p r re' |
r re' re' r mi'2-\sug\f |
la4 la r8 re'\p r re' |
r re' r re' r sol' r sol' |
r sol' r sol' r la' r re' |
sol'4\f sol'8 sol' r sol'\p r sol' |
r sol' r sol' r fad' r fad' |
r sol' r sol' r sol' r sol' |
r sol' r sol' r la' r re' |
sol'4(-\sug\f do'') r8 sol'-\sug\p r sol' |
r sol' r sol' r fad' r fad' |
sol'8\f si' r si'-\sug\p r sol' r sol' |
r sol' r sol' r do' r do' |
sol'4 r sol r |
