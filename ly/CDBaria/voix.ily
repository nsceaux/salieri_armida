\clef "bass/G_8" R1*2 |
sib2\fermata sib,4 r |
R1 |
re'8[ do'] sib2 la4 |
sib8 sib r4 r2 |
R1 |
re'8[ do'] sib2 la4 |
sib8 sib r4 re' sib |
sol8[ sib] sib mib' mib'4. sol8 |
sol4 fa r r8 fa |
sol4. sol8 la4. la8 |
sib4 sib r2 |
do'2 sib8[ la] sol[ fa] |
re'2\melisma sib, |
r4 re'2 do'16[ sib la sol] |
do'2 la, |
r4 do'2 sib16[ la sol fa] |
sib1 |
sib8[\fermata la16 sib]\melismaEnd do4 r2 |
r4 r8 do' re'8.[ do'16] sib8. la16 |
sol4( re'2) fa4 |
mi r fa' la8. si16 |
do'4 do' r la8. si16 |
do'4 do' r r8 do' |
re'8.[ do'16] sib8.[ la16] sib8.[ do'16] re'8.[ mi'16] |
fa'8[ mi'] re'[ do'] sib[ la] sol[ fa] |
sol2 sib4. re'8 |
do'([\melisma sib]) la2\melismaEnd sol4 |
fa re'8.[ sib16] sol4 do' |
la4 re'8.([ sib16]) sol4 do' |
fa2 r |
R1*3 |
fa'4 re'8. re'16 re'4. do'8 |
si4 si r2 |
mib'4. do'8 do'4. sib!8 |
la4 la r fa |
sib fa do' fa |
re'4 sib r2 |
fa2 sol4. la8 |
sib2 sib |
sib1~ |
sib\melisma |
lab\melismaEnd |
solb2 r4 reb' |
mib'4. mib'8 reb'4. dob'8 |
sib2 r |
sib \grace reb'8 dob'4 sib8[ lab] |
solb4(\melisma mib'4.) reb'8[ do'!]\melismaEnd sib |
\grace sib la!4 la r do' |
reb'4 fa' reb' sib |
fa2 r |
sib2\fermata sib,4 r |
R1 |
re'!8([ do']) sib2 la4 |
sib8 sib r4 r2 |
R1 |
re'8[ do'] sib2 la4 |
sib8 sib r4 re' sib |
lab4 sol8 lab sib4. lab8 |
sol4 sol r2 |
sib4 la!8 sib do'4. sib8 |
la4 la r r8 fa |
la4 sib do'4. re'16[ sib] |
do'8[ la] fa4 r2 |
la4 sib8. sib16 do'4 re'8[ sib] |
fa2 r4 r8 fa |
sol4 la sib do' |
re' do' re' mib' |
fa'2 re4. re8 |
mib1 |
fa |
sol2 r4 r8 re' |
do'2 <fa' fa>2 |
<re' sib>2 r4 re' |
do'2 <fa' fa> |
sib r |
