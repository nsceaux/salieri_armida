\clef "treble" R1*2 |
r2\fermata r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fa''8 | fa''4. mib''8 mib''4. re''8 | re''4 }
  { re''8 | re''4. do''8 do''4. sib'8 | sib'4 }
>> r4 r2 |
r2 r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fa''8 | fa''4. mib''8 mib''4. re''8 | re''4 }
  { re''8 | re''4. do''8 do''4. sib'8 | sib'4 }
>> r4 r2 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { re''4 sib' | sol''1 | sol''4 fa'' sib'' }
  { fa'4 re' | mib''1 | mib''4 re'' re'' }
  { s2 | s1\p | s2 s4\f }
>> r4 |
R1*4 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { fa''2 }
  { re''2 }
>> r4 |
R1 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { fa''2 }
  { do''2 }
>> r4 |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { sib''2\fermata }
  { sol''\fermata }
>> r2 |
R1*2 |
r2 <>\f \twoVoices #'(primo secondo tutti) <<
  { la''2 |
    sol''2 la'' |
    sol''4 sol'' sol'' }
  { fa''2 |
    mi'' fa'' |
    mi''4 mi'' mi'' }
>> r4 |
R1 |
\tag#'tutti <>^"a 2." fa''8-\sug\f mi'' re'' do'' sib' la' sol' fa' |
R1 |
r4 <>\fp \twoVoices #'(primo secondo tutti) <<
  { la''2( sol''4) |
    fa''4 s s mi''? |
    fa'' s s mi'' |
    fa'' s re''2 |
    mi''4 s sol''2 |
    la''4 }
  { fa''2( mi''4) |
    fa''4 s s sib' |
    la' s s sib' |
    la' s sol'2 |
    sol'4 s do''2 |
    do''4 }
  { s2. |
    s4 r r s\f |
    s4 r r s |
    s r s2 |
    s4 r s2 | }
>> \tag#'tutti <>_"a 2." la'8.\trill sol'32 fa' sib'8.\trill la'32 sol' do''8. do'16 |
fa'4 do' fa' r |
R1 | \allowPageTurn
sol''4.\f lab''8 sol''16 lab'' sol'' fa'' mib'' fa'' mib'' re'' |
do''4 r r2 |
fa''4. sol''8 fa''16 sol'' fa'' mib'' re'' mib'' re'' do'' |
sib'4-! fa'-! do''-! fa'-! |
re''-! sib'-! mib''-! do''-! |
<>\fp \twoVoices #'(primo secondo tutti) <<
  { fa''2\( mib'' |
    re'' mib'' |
    reb'' re'' |
    mib'' reb''\) |
    dob''1 |
    sib'2 reb'' |
    mib''4. mib''8 reb''4 dob'' |
    sib'2 }
  { fa''4 fa'( sol' la') |
    sib'1~ |
    sib'~ |
    sib' |
    lab' |
    solb' |
    solb'?4 lab' sib' lab' |
    fa'2 }
  { s1*4 | s1-\sug\ff | }
>> r2 |
R1*2 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''2 do''4 | reb''2. sib'4 | fa'2 }
  { la'2 la'4 | sib'2. sib'4 | fa'2 }
>> r2 |
r\fermata r4 r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''8 |
    fa''4. mib''8 mib''4. re''8 |
    re''4 }
  { re''8 |
    re''4. do''8 do''4. sib'8 |
    sib'4 }
>> r4 r2 |
r r4 r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''8 |
    fa''4. mib''8 mib''4. re''8 |
    re''4 }
  { re''8 |
    re''4. do''8 do''4. sib'8 |
    sib'4 }
>> r4 r2 |
R1 |
<>\fp \twoVoices #'(primo secondo tutti) <<
  { lab''1 | sol''4 }
  { re''1 | mib''4 }
>> r4 r2 |
<>\fp \twoVoices #'(primo secondo tutti) <<
  { sib''1 | la''4 }
  { mi''1 | fa''4 }
>> r4 r2 |
R1 |
r4 r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''8-! mib''-! re''-! do''-! sib'-! | la'4 }
  { re''8-! do''-! sib'-! la'-! sol'-! | fa'4 }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { fa'8 la'16 do'' fa''8 fa'' fa''4 }
  { fa'8 la'16 do'' la'8 la' la'4 }
>> r4 |
R1*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { fa''2 }
  { fa'2 }
>> re'2\fp |
mib'1 |
fa' |
sol'2 r |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { re'''1 | do''' | sib''4 }
  { sib''1~ | sib''2 la'' | sib''4 }
>> r4 r2 |
