\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (oboi #:score-template "score-deux-voix")
   (corni #:score-template "score-deux-voix"
          #:tag-global ()
          #:instrument "Corni in B")
   (fagotti #:music , #{

\quoteBassi "CDBbassi"
s1*13
<>^\markup\tiny "B." \new CueVoice { \mmRestInvisible fa8\fp la la la la2:8 | }
\cue "CDBbassi" { s1 \mmRestVisible }
   #})

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Ubaldo
  \livretVerse#10 { Torna schiavo infelice, }
  \livretVerse#10 { Alla prigione antica; }
  \livretVerse#10 { D’un empia ingannatrice }
  \livretVerse#10 { Torna frà lacci ancor. }
  \livretVerse#10 { Vanne, ma pensa intanto, }
  \livretVerse#10 { Che sciogli un di vorrai, }
  \livretVerse#10 { Quando fia vano il pianto }
  \livretVerse#10 { Inutile il dolor. }
}
       #}))
