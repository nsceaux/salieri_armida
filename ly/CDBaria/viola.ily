\clef "alto" sib4.\f sib16 do'32 re' mib'8. fa'16 sol'8. la'16 |
sib'4. sib16 do'32 re' mib'8. fa'16 sol'8. la'16 |
sib'2\fermata r2 |
R1 |
fa'2-\sug\f fa |
sib r |
R1 |
fa'2-\sug\fp fa |
sib2:8 sib:-\sug\f |
sib:-\sug\p sib: |
sib8-\sug\f sib' fa' re' sib4 r8 sib16 do'32 re' |
mib'4. do'16 re'32 mib' fa'4. re'16 mib'32 fa' |
sol'4. la'16 sol'32 fa' mi'2 |
fa'8\fp la la la la2:8 |
sib2 r |
r4 sib8\fp sib sib2: |
la2 r |
r4 la8\fp la la2: |
sol4 fa \ficta mi re |
do2\p\fermata r4 r8 do16\f re32 mi |
fa4 r fa-\sug\p r |
sib8-\sug\fp sib sib sib sol'2:8 |
sol'8-\sug\f do16 do do8 do fa do fa do |
mi do mi do fa do fa do |
mi16 do re mi fa sol la si do'8. sol16 mi8. do16 |
re'8.\p do'16 sib8. la16 sib8. do'16 re'8. \ficta mi'16 |
fa'8\f \ficta mi' re' do' sib la sol fa |
re'2:8\fp sol'8 sol' sol' sib |
do'2:8 do: |
fa4 re'8.\f sib16 sol4 do' |
la4 re'8. sib16 sol4 <do' do> |
fa r sol'2 |
sol'16 fa' mi'8 r4 do'2 |
do'4 la8.\trill sol32 fa sib8.\trill la32 sol do'8. do16 |
fa4 do fa r |
R1 |
sol'4.\f lab'8 sol'16 lab' sol' fa' mib' fa' mib' re' |
do'4 r r2 |
fa'4. sol'8 fa'16 sol' fa' mib' re' mib' re' do' |
sib4 r r fa |
sib fa do' fa |
re'\fp re( mib fa) |
sol2 solb |
fa1 |
solb2 fa |
reb'2:16\ff reb': |
\ficta reb': sib: |
dob': reb': |
re'!2 r |
re'1-\sug\p |
mib'2. mi'4 |
fa'2:8-\sug\f fa': |
fa': fa': |
fa r |
sib\fermata r |
R1 |
fa'2\fp fa |
sib r |
R1 |
fa'2-\sug\fp fa |
sib2:8 sib4-\sug\f re' |
fa'1-\sug\fp  |
sib4 r sib-\sug\f mib' |
sol'1-\sug\fp |
do'16-\sug\f fa' fa' fa' fa'2.:16 |
fa'2: fa': |
fa': fa': |
fa': fa': |
fa'16 fa la do' fa'8 fa' fa'4 r8 re'-\sug\p |
mib' mib' fa' fa' sol' sol' la' la' |
sib' sib' la' la' sib' sib' do'' do'' |
re''8\f re'' re'' re'' re2:8\fp |
mib:16 mib: |
fa: fa: |
sol2: fa':-\sug\p |
sol':-\sug\cresc fa': |
fa':-\sug\ff sol': |
sol': fa': |
sib'4 sol'-! mi'-! do'-! |
