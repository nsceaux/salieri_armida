\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniBInstr  } <<
        \keepWithTag #'corni \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with {
        instrumentName = \markup\character Ubaldo
        shortInstrumentName = \markup\character Ub.
      } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s1*3\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          % 5
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*3\pageBreak
          s1*3\pageBreak
          % 10
          s1*3 s2 \bar ""\pageBreak
          s2 s1*3\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          % 15
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*3 s2 \bar "" \pageBreak
          s2 s1*3\pageBreak
          % 20
          s1*4\pageBreak
          s1*4
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
