\clef "treble" sib4.\f sib16 do'32 re' mib'8. fa'16 sol'8. la'16 |
sib'4. sib'16 do''32 re'' mib''8. fa''16 sol''8. la''16 |
sib''2\fermata r4 r8 <<
  \tag #'primo {
    fa''8\f |
    fa''4. mib''8 mib''4. re''8 |
    re''8( do'' sib'2\rf) la'4\p |
    sib'2 r4 r8 fa''\f |
    fa''4. mib''8 mib''4. re''8 |
    re''8\fp do'' sib'2 la'4 |
    sib' r <fa' re''>4\f <sib' re'> |
    sol'8.(\p sib'16) sib'8.( mib''16) mib''4.( sol''8 |
    sol''4 fa'') <sib fa' re'' sib''>\f
  }
  \tag #'secondo {
    re''8-\sug\f |
    re''4. do''8 do''4. sib'8 |
    fa'( mib' re'2)-\sug\rf do'4-\sug\p |
    re'2 r4 r8 re''-\sug\f |
    re''4. do''8 do''4. sib'8 |
    fa'8-\sug\fp mib' re'2 do'4 |
    re' r <re' sib'>4-\sug\f <fa' sib> |
    <sib sol'>2:8\p q: |
    mib'4 re' <re' sib'>4-\sug\f
  }
>> r8 re'16 mib'32 fa' |
sol'4. mib'16 fa'32 sol' la'4. fa'16 sol'32 la' |
sib'4. <<
  \tag #'primo {
    fa'32 sol' la' sib' do''4. sib'8 |
    la'8\fp do'' do'' do'' do''2:8 |
    re''4 r r16 re''16-.\f mib''-. fa''-. sol''-. la''-. sib''-. do'''-. |
    re'''8 r <re'' re'>2\fp re''4 |
    do''2 r16 do''\f re'' mi'' fa'' sol'' la'' sib'' |
    do'''8 r8 do''2\fp do''4 |
    \ru#4 { sib'8 sib''16 sib' } |
    sib'2\p\fermata r4 r8. sib'16\f |
    la'8. sib'16 do''8. do'''16\p re'''8. do'''16 sib''8. la''16 |
    sol''4 re'''2\rf fa''4 |
    mi''2\f fa''16 mi'' fa'' sol'' la'' sol'' la'' si'' |
    do'''4 do''8. do''16 fa''16 \ficta mi'' fa'' sol'' la'' sol'' la'' si'' |
    do'''4 do'' r r8 do''\p |
    re''8. do''16 sib'8. la'16 sib'8. do''16 re''8. mi''16 |
  }
  \tag #'secondo {
    do''16 sib'32 la' sol'2 |
    do'8\fp fa' fa' fa' fa'2:8 |
    fa'4 r r16 sib16-.-\sug\f do'-. re'-. mib'-. fa'-. sol'-. la'-. |
    sib'8 r <<
      { fa''8 fa'' fa''2:8 | fa''2 } \\
      { re''8\fp re'' re''2:8 | do'' }
    >> r16 la-\sug\f sib do' re' \ficta mi' fa' sol' |
    la'8 r << { fa''8 fa'' fa''2: } \\ { do''8-\sug\fp do'' do''2: } >> |
    sib'8 sib''16 sib' la'8 la''16 la' sol'8 sol''16 sol' fa'8 fa''16 fa' |
    mi'2\p\fermata r4 r8. sol'16-\sug\f |
    fa'8. sol'16 la'8. la''16\p sib''8. la''16 sol''8. fa''16 |
    fa''2:8-\sug\fp fa''8 fa'' re'' re'' |
    do''8\f do'16 do' do'8 do' la'\f fa' la' fa' |
    sol'8 mi' sol' mi' la' fa' la' fa' |
    mi'16 do' re' mi' fa' sol' la' si' do''8. sol'16 mi'8. do'16 |
    re'8.\p do'16 sib8. la16 sib8. do'16 re'8. mi'16 |
  }
>>
fa''16\f sol'' mi'' fa'' re'' mi'' do'' re'' sib' do'' la' sib' sol' la' fa' la' |
<<
  \tag #'primo {
    sol'2:8\fp sib'8 sib' sib' re'' |
    do''8.( sib'16) la'2:8\rf sol'8 sol' |
  }
  \tag #'secondo {
    fa'2:8\fp re'8 re' re' sib' |
    \once\slurDashed la'8.( sol'16) fa'2:8-\sug\rf mi'8 mi' |
  }
>>
la'16\f fa'' mi'' fa'' re'' do'' re'' sib' <sol' sol> sol' fad' sol' <sol mi' do'' mi''> do'' si' do'' |
fa'' fa'' \ficta mi'' fa'' re''' do''' re''' sib'' sol'' sol'' fad'' sol'' do''' si'' do''' la'' |
<fa'' la'>4 r16 do'' re'' mi'' fa''8.\trill mi''32 fa'' sol''8 fa'' |
mi''16 re'' do''8 r16 fa'' sol'' la'' sib''8.\trill la''32 sib'' do'''8 sib'' |
la''4 la'8.\trill sol'32 fa' sib'8.\trill la'32 sol' do''8. do'16 |
fa'4 do' fa' r |
R1 |
<sol re' si' sol''>4.\f lab''8 sol''16 lab'' sol'' fa'' mib'' fa'' mib'' re'' |
do''4 r r2 |
<la' fa''>4. sol''8 fa''16 sol'' fa'' mib'' re'' mib'' re'' do'' |
sib'4-! fa'-! do''-! fa'-! |
re''-! sib'-! mib''-! do''-! |
<<
  \tag #'primo {
    fa''2\(\fp mib'' |
    re'' mib'' |
    reb'' re'' |
    mib'' reb''\) |
    dob''2:16\ff dob'': |
    sib': reb'': |
    mib'': reb''4: dob'': |
    sib'2 r |
    sib'\p dob''4 sib'8 lab' |
    solb'4 mib''4. reb''8 do''! sib' |
    la'!4 << fa''2 \\ la'\f >> do''4 |
    reb''4:16 fa'': reb'': sib': |
    fa'2 r |
    <re' sib'>2\fermata r4 r8 fa''\f |
    fa''4. mib''8 mib''4. re''8 |
    re''8(\fp do'') sib'2 la'4 |
    sib'2 r4 r8 fa''\f |
    fa''4. mib''8 mib''4. re''8 |
    re''8(\fp do'') sib'2 la'4
    sib'4 r <fa' re''>-\sug\f <sib' re'> |
    <sib' lab''>1\fp |
    <sol'' sib'>4 r <mib' sib' sol''>\f <sol' mib''> |
    <do'' sib''>1\fp |
    <la''! do''>4 r r r8 fa'\f |
    la'4-! sib'-! do''4.-! re''16 sib' |
    do''8 la' fa' fa'' mib''16( fa'') re''( mib'') do''( re'') sib'( do'') |
    la'4-! sib'-! do''-! re''8 sib' |
  }
  \tag #'secondo {
    fa''4\fp fa'( sol' la') |
    sib'1~ |
    sib'~ |
    sib' |
    lab'2:16\ff lab': |
    solb': solb': |
    \ficta solb'4: lab': sib': lab': |
    fa'2 r |
    fa''1\p |
    sib'2. sol'!4 |
    do' <fa' do''>2-\sug\f la'!4 |
    sib'4:16 reb'': sib': reb': |
    do'2 r |
    <sib fa'>2\fermata r4 r8 re''-\sug\f |
    re''4. do''8 do''4. sib'8 |
    fa'8(\fp mib') re'2 do'4 |
    re'2 r4 r8 re''-\sug\f |
    re''4. do''8 do''4. sib'8 |
    fa'(-\sug\fp mib') re'2 do'4 |
    re'4 r <re' sib'>-\sug\f <fa' sib> |
    <fa' re''>1-\sug\fp |
    <mib' mib''>4 r <sol' mib''>\f <sib' mib' sol> |
    <sib' mi''>1-\sug\fp |
    fa''16\f fa' fa' fa' fa'4:16 fa'2: |
    fa': fa': |
    fa': fa': |
    fa': fa': |
  }
>>
fa'8 la'16 do'' <la' fa''>8 q fa'' do'' la' fa' |
sol'4-!\p la'-! sib'-! do''-! |
re''-! do''-! re''-! mib''-! |
fa''2:16\f re':\fp |
mib': mib': |
fa': fa': |
sol': <<
  \tag #'primo {
    re''2:\p |
    do'':\cresc do'': |
    re''16\ff re''' re''' re''' re'''4:16 re'''2: |
    do''': do''': |
  }
  \tag #'secondo {
    sib'2:-\sug\p |
    sib':-\sug\cresc la': |
    sib'16-\sug\ff sib'' sib'' sib'' sib''4:16 sib''2: |
    sib'': la'': |
  }
>>
sib''4 sol'-! mi'-! do'-! |
