Tor -- na schia -- vo in -- fe -- li -- ce,
schia -- vo in -- fe -- li -- ce,
tor -- na al -- la pri -- gio -- ne an -- ti -- ca
d’un’ em -- pia in -- gan -- na -- tri -- ce
tor -- na fra’ lac -- ci
ah tor -- na fra’ lac -- ci an -- cor.
Schia -- vo in -- fe -- li -- ce,
in -- fe -- li -- ce,
d’un em -- pia in -- gan -- na -- tri -- ce
tor -- na 
tor -- na frà lac -- ci an -- cor
frà lac -- ci an -- cor
frà lac -- ci an -- cor.

Van -- ne, ma pen -- sa in -- tan -- to,
si ma pen -- sa in -- tan -- to,
che scior -- li un dì vor -- ra -- i,
quan -- do fia va -- no il pian -- to
in -- u -- ti -- le il do -- lor,
quan -- do fia va -- no il pian -- to
in -- u -- ti -- le il do -- lor.

Tor -- na schia -- vo in -- fe -- li -- ce,
schia -- vo in -- fe -- li -- ce,
tor -- na al -- la pri -- gio -- ne an -- ti -- ca
al -- la pri -- gio -- ne an -- ti -- ca
d’un em -- pia in -- gan -- na -- tri -- ce
tor -- na frà lac -- ci an -- cor
d’un em -- pia in -- gan -- na -- tri -- ce
tor -- na 
tor -- na frà lac -- ci an -- cor
frà lac -- ci an -- cor
frà lac -- ci an -- cor.
