\clef "treble" \transposition sib
R1*2 |
r2\fermata r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''8 | do''4. re''8 re''4. mi''8 | mi''4 }
  { mi'8 | mi'4. sol'8 sol'4. do''8 | do''4 }
>> r4 r2 |
r2 r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''8 | do''4. re''8 re''4. mi''8 | mi''4 }
  { mi'8 | mi'4. sol'8 sol'4. do''8 | do''4 }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { do''1~ | do'' | do''8 do'' sol' mi' do'4 }
  { do'1~ | do' | do'8 do'' sol' mi' do'4 }
  { s1 | s\p | s2\f }
>> r4 |
R1*4 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { do''2 }
  { do' }
>> r4 |
R1 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { sol'2 }
  { sol }
>> r4 |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { re''2\fermata }
  { do''2\fermata }
>> r2 |
R1*2 |
r2 \tag #'tutti <>^"a 2." re''2-\sug\f |
re''1 |
re''4 re'' re'' r |
R1*3 |
r4 re''2\fp re''4 |
sol' r r re''-\sug\f |
re'' r r re'' |
re'' r mi''2 |
re''4 r re''2 |
re''4 r r re'' |
sol' re'' sol' r | \allowPageTurn
R1*4 |
r2 r4 sol' |
do'' sol' re'' sol' |
mi'' r r2 |
R1*6 |
\twoVoices #'(primo secondo tutti) <<
  { do''2 }
  { mi' }
>> r2 |
R1*2 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { re''2 re''4 | }
  { sol'2 sol'4 | }
>>
sol'2. sol'4 |
sol'2 r |
r2\fermata r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''8 | do''4. re''8 re''4. mi''8 | mi''4 }
  { mi'8 | mi'4. sol'8 sol'4. do''8 | do''4 }
>> r4 r2 |
r2 r4 r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''8 | do''4. re''8 re''4. mi''8 | mi''4 }
  { mi'8 | mi'4. sol'8 sol'4. do''8 | do''4 }
>> r4 r2 |
R1 |
sol'1\fp |
\twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { do' }
>> r4 r2 |
re''1\fp |
re''8\f sol'16. sol'32 sol'8 sol' sol'2:8 |
\twoVoices #'(primo secondo tutti) <<
  { re''4-! do''-! re''4.-! mi''8-! |
    re''8 sol'16. sol'32 sol'8 sol' sol'2: |
    re''4 do'' re'' mi''8 do'' |
    sol'8 re''16. re''32 re''8 re'' re''4 }
  { sol'4-! sol'-! sol'4.-! do''8-! |
    sol'8 sol'16. sol'32 sol'8 sol' sol'2: |
    sol'4 sol' sol'4 do'' |
    sol'8 sol'16. sol'32 sol'8 sol' sol'4 }
>> r4 |
R1*2 |
\tag#'tutti <>^"a 2." sol'2\f r |
R1*2 |
r2 <>\p \twoVoices #'(primo secondo tutti) <<
  { mi''2 |
    re''1 |
    mi'' |
    re'' |
    do''4 }
  { do''2 |
    do'' sol' |
    do''1~ |
    do''2 sol' |
    mi'4 }
  { s2 | s1-\sug\cresc | s\f }
>> r4 r2 |
