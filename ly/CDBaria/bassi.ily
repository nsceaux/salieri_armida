\clef "bass" sib,4.-\sug\f sib,16 do32 re mib8. fa16 sol8. la16 |
sib4. sib,16 do32 re mib8. fa16 sol8. la16 |
sib2\fermata r2 |
R1 |
fa2\f fa, |
sib, r |
R1 |
fa2\fp fa, |
sib,:8 sib,:-\sug\f |
sib,:\p sib,: |
sib,8\f sib fa re sib,4 r8 sib,16 do32 re |
mib4. do16 re32 mib fa4. re16 mib32 fa |
sol4. la16 sol32 fa mi4 r |
fa8\fp la la la la2:8 |
sib8 fa re fa sib,2 |
r4 sib8\fp sib sib sib sib sib |
la fa do fa la,2 |
r4 la8\fp la la la la la |
sol4-! fa-! mi-! re-! |
do2\fermata r4 r8 do16\f re32 mi |
fa4 r fa,\p r |
sib,2:8\fp si,: |
do:\f fa8-! do-! fa-! do-! |
mi8 do mi do fa do fa do |
mi16 do re mi fa sol la si do'8. sol16 mi8. do16 |
re'8.\p do'16 sib8. la16 sib8. do'16 re'8. mi'16 |
fa'8\f mi' re' do' sib la sol fa |
sib,2:8\fp sib,: |
do: do,: |
fa,4 re'8.\f sib16 sol4 do' |
la4 re'8. sib16 sol4 do' |
fa r si2 |
do'4 r \ficta mi2 |
fa4 la8.\trill sol32 fa sib8.\trill la32 sol do'8. do16 |
fa4 do fa, r |
R1 |
sol4.\f lab8 sol16\f lab sol fa mib fa mib re |
do4 r r2 |
fa4. sol8 fa16 sol fa mib re mib re do |
sib,4 r r fa-! |
sib-! fa-! do'-! fa-! |
re'-!\fp re( mib fa) |
sol2 solb |
fa1 |
solb2 fa |
fa:8\ff fa: |
solb: sib,: |
dob: reb: |
re!2 r |
re1\p |
mib2 r4 mi |
fa2:8\f fa: |
fa: fa: |
fa, r |
sib,\fermata r |
R1 |
fa2\fp fa, |
sib, r |
R1 |
fa2\fp fa, |
sib,:8 sib,:\f |
sib,:\fp sib,: |
mib: mib:\f |
do:\fp do: |
fa\f r |
mib'4-! re'-! la-! sib-! |
fa2 r |
mib'4 re' la sib |
fa16 fa, la, do fa8 fa fa4 r8 re-\sug\p |
mib mib fa fa sol sol la la |
sib sib la la sib sib do' do' |
re'2:8\f re:\fp |
mib:16 mib: |
fa: fa: |
sol: re:\p |
mib:-\sug\cresc fa: |
sib:\f sol: |
mib: fa: |
sib4-! sol-! mi-! do-! |
