\tag #'(ismene basse) {
  Scher -- ni -- ta, de -- pres -- sa
  da un’ al -- ma sper -- giu -- ra,
  da un’ al -- ma sper -- giu -- ra,
  la vi -- ta non cu -- ra,
  scher -- ni -- ta, de -- pres -- sa
  da un’ al -- ma sper -- giu -- ra,
  al duol di se stes -- sa
  già ce -- de il go -- ver -- no,
  ne’il Ciel, ne’ l’A -- ver -- no
  più spe -- me non ha,
  ne’il Ciel, ne’ l’A -- ver -- no
  più spe -- me non ha,
  più spe -- me non ha,
  più spe -- me non ha,
  più spe -- me
  più spe -- me non ha.

  Da noi so -- lo as -- pet -- ta
  in tan -- to pe -- ri -- glio,
  in tan -- to pe -- ri -- glio.
  Tra -- di -- ta ven -- det -- ta,
  smar -- ri -- ta, con -- si -- glio,
  op -- pres -- sa,
  op -- pres -- sa, pie -- tà,
  op -- pres -- sa, pie -- tà. __
  Tra -- di -- ta,
  smar -- ri -- ta,
  op -- pres -- sa, pie -- tà,
  op -- pres -- sa, pie -- tà.

  Scher -- ni -- ta, de -- pres -- sa
  da un’ al -- ma sper -- giu -- ra,
  da un’ al -- ma sper -- giu -- ra,
  la vi -- ta non cu -- ra,
  scher -- ni -- ta, de -- pres -- sa
  da un’ al -- ma sper -- giu -- ra,
  al duol di se stes -- sa
  già ce -- de il go -- ver -- no,
  ne’il Ciel, ne’ l’A -- ver -- no
  più spe -- me non hà,
  nò, nò,
  da noi so -- lo as -- pet -- ta
  in tan -- to pe -- ri -- glio,
  tra -- di -- ta ven -- det -- ta,
  smar -- ri -- ta, con -- si -- glio,
  op -- pres -- sa, pie -- tà, __
  op -- pres -- sa, pie -- tà.
  Op -- pres -- sa, pie -- tà,
  op -- pres -- sa,
  op -- pres -- sa, pie -- tà,
  op -- pres -- sa, pie -- tà,
  op -- pres -- sa, pie -- tà.

}
\tag #'(voix1 voix2 voix3) {
  Ah tro -- vi in -- fe -- li -- ce
  in sor -- te sì fie -- ra,
  se a -- i -- ta non spe -- ra,
  al -- men fe -- del -- tà,
  se a -- i -- ta non spe -- ra, al -- men fe -- del -- tà,
  al -- men fe -- del -- tà,
  al -- men fe -- del -- tà,
  al -- men fe -- del -- tà.
}
