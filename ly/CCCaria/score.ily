\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \corniEInstr  } <<
        \keepWithTag #'corni \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiriFirst
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with {
        \haraKiri
        instrumentName = \markup\character Ismene
        shortInstrumentName = \markup\character Is.
      } \withLyrics <<
        { \noHaraKiri s8 s2*133 \revertNoHaraKiri }
        \global \keepWithTag #'ismene \includeNotes "voix"
      >> \keepWithTag #'ismene \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s8 s2*3\pageBreak
          \grace s8 s2*4\pageBreak
          s2*3\pageBreak
          s2*5\pageBreak
          % 5
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          % 10
          s2*4\pageBreak
          s2*4\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          s2*4\pageBreak
          % 15
          s2*5\pageBreak
          s2*4\pageBreak
          s2*4\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          % 20
          s2*4\pageBreak
          s2*5\pageBreak
          s2*4\pageBreak
          s2*4\pageBreak
          s2*5\pageBreak
          % 25
          s2*5\pageBreak
          s2*6\pageBreak
          s2*4\pageBreak
          s2*4\pageBreak
          s2*5\pageBreak
          % 30
          s2*5\pageBreak
          s2*5\pageBreak
          s2*6\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
