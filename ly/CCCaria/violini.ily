\clef "treble" si'8(\mf |
sold') mi'-! r si'( |
la') fad'-! r fad' |
sold'4\f la'8. dod''16 |
\grace dod''8 si'4 la'16( sold' fad' mi') |
<<
  \tag #'primo { fad'8. sold'32 la' sold'8 fad' | }
  \tag #'secondo { mi'16 mi'8 mi'16 mi'8 red' | }
>>
\grace fad'16 mi'8 mi' r si'(\mf |
sold') mi'-! r si'( |
la') fad'-! r fad'\f |
sold'16.[ sold'32] \grace la'16 sold'32[ fad' sold'16] red''16.[ red''32] \grace mi''16 red''32[ dod'' red''16] |
mi''16.[ mi''32] \grace fad''16 mi''32[ red'' mi''16] fad''16*2/3[ sold'' la''] la''[ sold'' fad''] |
sold''[ fad'' mi''] red''[ mi'' fad''] mi''[ red'' dod''] si'[ la' sold'] |
fad'8 <<
  \tag #'primo {
    dod''8-! red''-! mi''-! |
    red''32( dod'' si'16) la'8( sold' lad') |
    si' la'!( sold' lad') |
    si'4 r8 si''(\p |
    sold'') mi''-! r si''( |
    la'') fad''-! r8. fad''16 |
    sold''4 la''8. dod'''16 |
    \grace dod'''8 si''4\f la''16( sold'' fad'' mi'') |
    fad''8. sold''32 la'' sold''8 fad'' |
    mi''8 mi'' r si''(\p |
    sold'') mi''-! r si''( |
    la'') fad''-! r8. fad''16 |
    sold''4 la''8. dod'''16 |
    \grace dod'''8 si''4\f la''16( sold'' fad'' mi'') |
    fad''8.\p sold''32 la'' sold''8 fad'' |
    mi''16 mi'' si' sold' mi'8 fad'' |
    si''4\f lad''16(\p si'') dod'''( si'') |
    fad''4\f mid''16(\p fad'') sold''( fad'') |
    red''8.\f fad''16 mi''! red'' dod'' si' |
    fad''8 fad' r fad''\p |
    sold''8.( mi''16 si'8) sold''-! |
    fad''32 mi'' red''16 red''8 r fad'' |
    sold''8.( mi''16 si'8) sold''-! |
    fad''32 mi'' red''16 red''8 r si' |
    <<
      { mi''16 mi'' mi'' mi'' mi'' mi'' mi'' mi'' | } \\
      { dod''\f dod''\p dod'' dod'' dod'' dod'' dod'' dod'' }
    >>
    red''2:16 |
    << mi'': \\ dod'':\fp >> |
    red''8[ r16 fad''] sold''(\rf fad'' mid'' fad'') |
    fad''8[ r16 mi''!] fad''(\rf mi'' red'' mi'') |
    mi''4 dod''\p |
    red''16*2/3[ dod'' si'] lad'[ si' dod''] si'[ lad' sold'] fad'[ mi' red'] |
    dod'8 mi''16 dod'' si'8 lad' |
    si'[ r16 fad'']-! sold''(\rf fad'' mid'' fad'') |
    fad''8[ r16 mi''!] fad''(\rf mi'' red'' mi'') |
    mi''4 dod''\p |
    red''16*2/3[ dod'' si'] lad'[ si' dod''] si'[ lad' sold'] fad'[ mi' red'] |
    dod'8 mi''16 dod'' si'8 lad' |
    si'2:16 |
    si'16 si' dod'' dod'' dod'' dod'' mi'' mi'' |
    \grace mi''8 red''( dod''16 si') lad'16( sold') mi''( dod'') |
    <red'' si''>2:16\f |
  }
  \tag #'secondo {
    dod'8-! red'-! mi'-! |
    red'16 si dod' red' mi' dod' fad' mi' |
    red' si dod' red' mi' dod' fad' mi' |
    red'4 r |
    r8 sold'(\p si') sold'-! |
    r red'( fad') red'-! |
    mi'16-! si-! mi'-! si-! fad'-! si-! fad'-! si-! |
    sold'-\sug\f si sold' si si4:16 |
    dod'16 mi'' mi'' mi'' mi''8 red'' |
    mi''16 mi'' si' sold' mi'8 r |
    r sold'(-\sug\p si') sold'-! |
    r red'( fad') red'-! |
    mi'16 si mi' si fad' si fad' si |
    sold'-\sug\f si sold' si si4:16 |
    dod'16-\sug\p mi'' mi'' mi'' mi''8 red'' |
    mi''16 mi'' si' sold' mi'8 fad' |
    si'4-\sug\f lad'16(-\sug\p si') dod''( si') |
    fad'4-\sug\f mid'16(-\sug\p fad') sold'( fad') |
    red'8.-\sug\f fad'16 mi'! red' dod' si |
    fad'8 fad' r4 |
    r8 sold'(-\sug\p si') sold'-! |
    r red'( fad') red'-! |
    r sold'8( si') sold'-! |
    r red'( fad') si'-! |
    lad'2:16-\sug\fp |
    si'2: |
    lad':-\sug\fp |
    si'8 r red''4(\rf |
    dod'') si'-\sug\rf~ |
    si'8( lad') lad'4-\sug\p |
    si'8 mi' red' si |
    si sold'16 mi' red'8 dod' |
    red' r red''4-\sug\rf( |
    dod'') si'-\sug\rf~ |
    si'8( lad') lad'4\p |
    si'8 mi' red' si~ |
    si sold'16 mi' red'8 dod' |
    red'2:16 |
    mi'4: fad': |
    fad': si16 si sold' sold' |
    fad'-\sug\f <red'' si''> q q q4:16 |
  }
>>
<dod'' lad''?>2:16 |
<si' si''>8[ r16 fad''] sold''( fad'' mid'' fad'') |
fad''8[ r16 mi''!] \once\slurDashed fad''( mi'' red'' mi'') |
mi''4 mi'' |
mi'' dod'' |
red''16*2/3[ dod'' si'] lad'[ si' dod''] si'[ lad' sold'] fad'[ mi' red'] |
dod'[ mi' sold'] dod''[ mi'' sold''] fad''[ red'' si'] lad'[ si' dod''] |
si'8 si <<
  \tag #'primo {
    r8 fad'\p |
    si'8 si'4 si'8~ |
    si' si'4 si'8 |
    si'4 la'8 sold' |
    \grace sold'8 la' la'4 fad'8 |
    sold'8. la'32 si' la'8 sold' |
    la'16 sold' fad'8 r mi'\f |
    <dod' la'>4 <si mi' si'> |
    dod''8-! la'-! r fad'\p |
    si'4( dod'') |
    re''16 dod'' si'8 r si'16(\mf do''!) |
    re''4(\f do''8\p si') |
    la'8. si'32 do'' si'8 la' |
    re''4\f do''8\p si' |
    la'8. si'32 do'' si'8 la' |
    si'8\mf( do''4) si'16 la' |
    \ficta sol'(\p si' mi'' \ficta sol'' la'' fad'' red''! la') |
    sol' si' mi'' sol'' \grace fad''16 mi''8\f red''!16 mi'' |
    do''2:16\fp |
    si'16 si' si' si' si' si' la' la' |
    la' la' sol' sol' sol'4: |
    mi''2:16 |
    do'': |
    si'16 sol' sol' sol' la' fad' fad' fad' |
    mi'4 r8 si''(\p |
    sold'') mi''-! r si''( |
    la'') fad''-! r fad'' |
    sold''4 la''8. dod'''16 |
    \grace dod'''8 si''4\f la''16( sold'' fad'' mi'') |
    fad''8. sold''32 la'' sold''8 fad'' |
    mi''4 r8 si''(\p |
    sold'') mi''-! r si''( |
    la'') fad''-! r fad'' |
    sold''4 la''8. dod'''16 |
    \grace dod'''8 si''4\f \once\slurDashed la''16( sold'' fad'' mi'') |
    fad''8.\p sold''32 la'' sold''8 fad'' |
    mi''16
  }
  \tag #'secondo {
    r4 |
    red'8-\sug\p red'4 red'8~ |
    red' red'4 red'8 |
    dod'4 fad'8 mid' |
    \grace mid'8 fad' dod' dod' fad' |
    fad'4 fad'8 mid' |
    fad'16 mid' fad'8 r4 |
    <mi' dod'>4-\sug\f <si sold'> |
    la'8-! dod'-! r4 |
    re'4\p fad' |
    fad'16 mi' re'8 r sol'16-\sug\mf la' |
    si'4-\sug\f la'8-\sug\p sol' |
    sol'8. la'16 sol'8 fad' |
    si'4-\sug\f la'8-\sug\p sol' |
    sol'8. la'16 sol'8 fad' |
    fad'2-\sug\mf |
    sol'8-\sug\p r fad' r |
    \ficta sol' r si'4:16-\sug\f |
    la'2:-\sug\fp |
    sol'16 sol' sol' sol' sol' sol' fad' fad' |
    fad' fad' mi' mi' mi' mi' mi' mi' |
    si'2:16 |
    la': |
    sol'16 mi' mi' mi' fad' red'! red' red' |
    mi'4 r8 si'(-\sug\p |
    sold') mi'-! r si'( |
    la') fad'-! r fad' |
    mi'16 si mi' si fad' si fad' si |
    sold'-\sug\f si sold' si mi' si si si |
    mi'8 mi'4 red'8 |
    mi'16 mi'' si' sold' mi'8 si'-\sug\p( |
    sold') mi'-! r si'( |
    la') fad'-! r fad' |
    mi'16 si mi' si fad' si fad' si |
    sold'-\sug\f si sold' si mi' si si si |
    mi'8-\sug\p mi'4 red'8 |
    mi'16
  }
>> mi''16 si' sold' mi'8 mi' |
re''4\f si'16(\p dod'' re'' si') |
sold'4\f si'16(\p la' sold' fad') |
mid'16 dod' sold'' fad'' mid'' fad'' sold'' si' |
la'8 la' r la' |
<<
  \tag #'primo {
    red''!32\f red'' red'' red'' red''8:32 mi''4: |
    fad''16\f fad'' red'' red'' si'\p si' la' la' |
    sold'4:16 lad': |
    si'8-!\f la'!( sold' lad') |
    si'8-! la'!( sold' lad') |
    si'4 r8 si'\p |
    dod''8.( red''16 mi''8) mi''-! |
    sold''32 fad'' mi''16 mi''8 r si' |
    dod''8.( red''16 mi''8) mi''-! |
    sold''32 fad'' mi''16 mi''8 r si''16 sold'' |
    fad''8(-\sug\p fad''4 sold''8 |
    <la' la''>8\f la''4\p sold''8) |
    fad''8( fad''4 sold''8) |
    <la'' la'>8\f la''4\p sold''8 |
    fad''8 fad''4 sold''8 |
    la''2 |
    lad'' |
    si''8.( dod'''16 si'' la''! sold'' fad'') |
    si''8\f dod'''16 la'' sold''8 fad'' |
    mi''8[ r16 si'] dod''16(\mf si' lad' si') |
    si'8[ r16 la'!] si'(\mf la' sold' la') |
    la'4 dod''\p |
    si'16*2/3[ dod'' red''] mi''[ red'' dod''] si'[ la' sold'] fad'[ mi' red'] |
    dod'8 dod''16 la' sold'8 fad' |
    \grace fad' sold' r16 si'' lad''(\mf si'' dod''' si'') |
    si''8[ r16 la''] sold''(\mf la'' si'' la'') |
    la''4 dod''\p |
    si': mi'': |
    mi'': red'': |
    red''8(\rf mi'') mi''4:16\p |
    mi'': red'': |
    red''8(\mf mi'') sold''4:16\f |
    fad'': fad'': |
    mi''4 r8
  }
  \tag #'secondo {
    la'32-\sug\f la' la' la' la'8:32 la'4: |
    << la'2 { s4-\sug\f s-\sug\p } >> |
    sold'16 si mi' sold' mi'4:16 |
    red'16-!-\sug\f si-! dod'-! red'-! mi'-! dod'-! fad'-! mi'-! |
    red' si dod' red' mi' dod' fad' mi' |
    red'4 r |
    r8 dod'(-\sug\p mi') dod'-! |
    r8 sold( si) sold-! |
    r dod'( mi') dod'-! |
    r sold( si) mi'-! |
    mi''8-\sug\p mi''4 mi''8 |
    mi''-\sug\f mi''4-\sug\p mi''8 |
    mi''8 mi''4 mi''8 |
    mi''-\sug\f mi''4-\sug\p mi''8 |
    mi'' mi''4 mi''8 |
    r8 mi'' mi'' mi'' |
    r sol''(\mf fad'' mi'') |
    red''2 |
    mi''8-\sug\f mi''16 fad'' mi''8 red'' |
    mi'' r sold'4\mf |
    fad' mi'-\sug\mf~ |
    mi'8 red' mi'4-\sug\p |
    mi'4 r |
    r8 la'16 fad' mi'8 red' |
    \grace red' mi' r sold''4\mf( |
    fad'') mi''-\sug\mf~ |
    mi''8 red'' mi'4-\sug\p |
    mi'4:16 sold': |
    fad': fad': |
    fad'8(\mf mi') sold'4:16\p |
    fad': fad': |
    fad'8(-\sug\mf mi') mi''4:16-\sug\f |
    mi'': red'': |
    mi''16 mi'' si' sold' mi'8
  }
>> si'8 |
mi''4 red''16( mi'' fad'' mi'') |
si'4 lad'16( si' dod'' si') |
sold'8. si'16 la'! sold' fad' mi' |
si'8 si r si' |
<<
  \tag #'primo {
    dod''8.( red''16 mi''8) mi''-! |
    sold''32 fad'' mi''16 mi''8 r si' |
    dod''8.( red''16 mi''8) mi''-! |
    sold''32 fad'' mi''16 mi''8 r re''\p |
    \ficta re''( dod'') dod''( sid') |
    dod''4 red''! |
    mi'' fad'' |
    sold'':16\f mi'': |
    mi'': red'': |
    red''8(\mf mi'') mi''4:16 |
    mi'': red'': |
    red''8(\mf mi'') sold''4: |
    fad'': fad'': |
    \ru#4 { <si' mi''>8 <si' sold''> } |
  }
  \tag #'secondo {
    dod''8.( la'16 mi'8) dod''-! |
    si'32 la' sold'16 sold'8 r si' |
    dod''8.( la'16 mi'8) dod''8-! |
    si'32 la' sold'16 sold'8 r si'\p |
    si'( la') la'( sold') |
    la'4 la'8 sold' |
    sold' la' la' si' |
    si'4:16-\sug\f sold': |
    fad'2: |
    fad'8(-\sug\mf mi') sold'4:16 |
    fad'2: |
    fad'8(-\sug\mf mi') mi''4: |
    mi'': red'': |
    \ru#4 { <sold' mi''>8 sold'' } |
  }
>>
\custosNote <mi'' sold'>8 \stopStaff
