\clef "bass" r8 |
r mi(\mf sold) mi-! |
r \once\slurDashed si,( red) si,-! |
mi16-!\f si,-! mi-! si,-! fad-! si,-! fad-! si,-! |
sold-! si,-! sold-! si,-! mi-! sold,-! mi-! sold,-! |
la, dod la, dod si, si, si si |
mi mi si, sold, mi,8 r |
r mi(\mf sold) mi-! |
r si,( red) si,-! |
mi16\f mi, sold, si, la, fad, si, la, |
sold, sold fad mi red8 si, |
mi fad sold sold, |
la,4 r |
si, r |
si, r |
si, r |
R2*12 |
si,4\f r |
si,\f r |
si,8\f si si, sold |
fad4 fad, |
R2*4 |
fad,8\f r fad\p r |
si,4 r |
fad,8\f r fad\p r |
si,4 r |
R2*3 |
r4 fad8 fad, |
si,4 r |
R2*3 |
r4 fad8 fad, |
r8 si,( red) si,-! |
r mi8( dod) lad,-! |
si, dod16 red mi8 mi |
fad2:16\f |
fad4: fad,: |
si,8 si si si |
lad lad sold sold |
sol( fad) sol( fad) |
sol( fad) lad fad |
si, dod red red |
mi mi fad fad, |
si,4 r |
R2*9 |
r4 r8 \twoVoices #'(primo secondo tutti) <<
  { si16 do' |
    re'4 do'8 si |
    la8. si32 do' si8 la |
    re'4 do'8 si |
    la8. si32 do'! si8 la |
    si( do'4) }
  { sol16 la |
    si4 la8 sol |
    sol!8. la16 sol8 fad |
    si4 la8 sol |
    sol!8. la16 sol8 fad |
    fad4. }
  { s8-\sug\mf | s4-\sug\f s-\sug\p | s2 | s4\f s\p | s2 | s4-\sug\mf }
>> r8 |
R2*11 |
mi16 si, mi si, fad si, fad si, |
sold\f si, sold si, mi sold, mi sold, |
la, dod la, dod si, si, si, si, |
mi, mi si, sold, mi,8 r |
R2*2 |
mi16 si, mi si, fad si, fad si, |
sold\f si, sold si, mi sold, mi sold, |
la,\p dod la, dod si, si, si, si, |
mi, mi si, sold, mi,8 mi |
re'4\f si16(\p dod' re' si) |
sold4\f si16(\p la sold fad) |
mid4 dod |
fad16 fad fad fad mi!4:16 |
red!:\f dod: |
si,:\f red:-\sug\p |
mi: dod: |
si,4\f r |
si, r |
si, r |
R2*4 |
\slurDashed la4(-\sug\p si) |
dod'(-\sug\f si)-\sug\p |
la( si) |
dod'(-\sug\f si)-\sug\p |
la( si) | \slurSolid
dod'2 |
do' |
si4 r8 la, |
sold,\f la, si, si |
mi4 r |
R2*9 |
sid8(-\sug\mf dod') r4 |
R2 |
sid8(\mf dod') sold16\f sold sold sold |
la4: si: |
mi16 mi' si sold mi8 si, |
mi4 red16 mi fad mi |
si4 lad16 si dod' si |
sold8. si16 la! sold fad mi |
si,8 si si16 la sold fad |
mi2:16 |
mi: |
mi: |
mi8 mi mi, r |
mi4\p mi |
la4:16 si: |
dod': red |
mi:\f dod': |
la: si: |
sid8\mf dod' sold4: |
la: si: |
sid8\mf dod' sold4: |
la: si: |
mi2:8 |
mi: |
mi4
