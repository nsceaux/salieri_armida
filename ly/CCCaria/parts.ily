\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (corni #:score-template "score-deux-voix" #:tag-global ()
          #:instrument "Corni in E")
   (fagotti #:music , #{
\quoteBassi "CCCbassi"
s8 s2*15
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*12 \mmRestVisible }
s2*4
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*4 \mmRestVisible }
s2*5
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*2 s4 \mmRestVisible }
s4 s2*2
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*2 s4 \mmRestVisible }
s4 s2*12
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*9 s4 \mmRestVisible }
s4 s2*5
<>^\markup\tiny "B." \new CueVoice {
  \mmRestInvisible
  mi8\p r red! r |
  mi4 sol8\f sol |
  la2:8\fp |
  si: |
  do': |
  sol: |
  la: |
  si8 si si, si, |
  mi4 r | \allowPageTurn
  r8 mi-\sug\p( sold!) mi-! |
  r si,( red) si,-! |
  \mmRestVisible
}
s2*4
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*2 \mmRestVisible }
s2*14
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*4 \mmRestVisible }
s2*11
<>^\markup\tiny "B." \cue "CCCbassi" { \mmRestInvisible s2*4 \mmRestVisible }
s2
<>^\markup\tiny "B." \new CueVoice {
  \mmRestInvisible
  r4 la,8\p la |
  sold4:16 dod': |
  la: si: |
  s4 \restInvisible sold4: | \restVisible
  la: si: |
  \mmRestVisible
}


   #})

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ismene
    \livretVerse#10 { Schernita, depressa }
    \livretVerse#10 { Da un’ alma spergiura }
    \livretVerse#10 { La vita non cura, }
    \livretVerse#10 { Al duol di se stessa }
    \livretVerse#10 { Già cede il governo, }
    \livretVerse#10 { Nel Ciel, nell’ Averno }
    \livretVerse#10 { Più speme non hà. }
    \livretVerse#10 { Da noi solo aspetta }
  }
  \null
  \column {
    \livretVerse#10 { In tanto periglio }
    \livretVerse#10 { Tradita vendetta, }
    \livretVerse#10 { Smarrita, consiglio, }
    \livretVerse#10 { Oppressa, pietà. }
    \livretPers Coro
    \livretVerse#10 { Ah trovi infelice }
    \livretVerse#10 { In sorte si fiera, }
    \livretVerse#10 { Se aita non spera, }
    \livretVerse#10 { Almen fedeltà. }
  }
  \null
} #}))
