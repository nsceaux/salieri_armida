<<
  \tag #'(ismene basse) {
    \clef "alto/treble" r8 |
    R2*14 |
    r4 r8 si' |
    sold' mi' r si' |
    la' fad' r8. fad'16 |
    sold'4 la'8. dod''16 |
    \grace dod''8 si'4 la'16[ sold'] fad'[ mi'] |
    fad'8.[ sold'32 la'] sold'8 fad' |
    mi' mi' r si' |
    sold' mi' r si' |
    la' fad' r8. fad'16 |
    sold'4 la'8. dod''16 |
    si'4 la'16[ sold'] fad'[ mi'] |
    fad'8.[ sold'32 la'] sold'8 fad' |
    mi' mi' r fad' |
    si'4 lad'16[ si'] dod''[ si'] |
    fad'4 mid'16[ fad'] sold'[ fad'] |
    red'8.[ fad'16] mi'![ red'] dod'[ si] |
    fad'8 fad' r fad' |
    sold'8.[ lad'16] si'8 si' |
    red''32[ dod'' si'16] si'8 r fad' |
    sold'8.[ lad'16] si'8 si' |
    red''32[ dod'' si'16] r8 r si' |
    mi''4 mi'8. mi'16 |
    red'8[ fad'] si' red'' |
    mi''[ dod''] lad' mi' |
    \grace mi'8 red'4 r |
    R2 |
    r4 dod'' |
    red''16*2/3[\melisma dod'' si'] \ficta lad'[ si' dod''] si'[ lad' sold'] fad'[ mi' red'] |
    dod'8[ mi''16 dod'']\melismaEnd si'8 lad' |
    si'4 r |
    R2 |
    r4 dod'' |
    red''16*2/3[\melisma dod'' si'] lad'[ si' dod''] si'[ lad' sold'] fad'[ mi' red'] |
    dod'8[ mi''16 dod'']\melismaEnd si'8 lad' |
    si'4 r8 si' |
    sold' sold' r dod'' |
    red''[\melisma dod''16 si'] lad'[ sold' mi'' dod'']\melismaEnd |
    fad'2 |
    dod''\trill |
    si'4 r |
    R2*5 |
    r4 r8 fad' |
    si'4 si'8. si'16 |
    si'16([ dod'!]) dod'4 si'8 |
    si'4 la'8 sold' |
    \grace sold'8 la'8 la'4 fad'8 |
    sold'8.[ la'32 si'] la'8 sold' |
    la'16[ sold'] fad'8 r mi' |
    la'4 si'8. si'16 |
    dod''16[ si'] la'8 r fad' |
    si'4 dod''8. dod''16 |
    \ficta re''16[ dod''] si'8 r8 si'16[ do''] |
    re''4 do''8 si' |
    la'8.[ si'32 do''] si'8 la' |
    \ficta re''4 r8 si' |
    la'8.[ si'32 do''] si'8 la' |
    si'( do''4) si'16[ la'] |
    sol'8 si' r si'16[ la'] |
    \ficta sol'8 si' mi''4 |
    do''2 |
    si'4. la'8 |
    \grace la'8 \ficta sol'2 |
    mi'' |
    do'' |
    si'4. si'8 |
    mi'4. si'8 |
    sold'!8 mi' r si' |
    la' fad' r8. fad'16 |
    sold'4 la'8. dod''16 |
    \grace dod''8 si'4 la'16[ sold'] fad'[ mi'] |
    fad'8.[ sold'32 la'] sold'8 fad' |
    mi'8 mi' r si' |
    sold' mi' r si' |
    la' fad' r8. fad'16 |
    sold'4 la'8. dod''16 |
    \grace dod''8 si'4 la'16[ sold'] fad'[ mi'] |
    fad'8.[ sold'32 la'] sold'8 fad' |
    mi' mi' r mi' |
    re''4 si'16[ dod''] re''[ si'] |
    sold'4 si'16[ la'] sold'[ fad'] |
    mid'8[ dod'] dod''8. si'16 |
    la'8 la' r la' |
    red''!4 mi''8. mi''16 |
    fad''8[ red''] si' la' |
    sold'4 lad'8. lad'16 |
    si'4 r |
    si' r |
    si' r8 si' |
    dod''8.[ la'16] mi'8 dod'' |
    si'32[ la' sold'16] sold'8 r si' |
    dod''8.[ la'16] mi'8 dod'' |
    si'32[ la' sold'16] sold'8 r si'16[ sold'] |
    fad'8 fad'4 sold'8 |
    la' la' r si'16[ sold'] |
    fad'8 fad'4 sold'8 |
    la'8 la' r si'16[ sold'] |
    fad'8 fad'4 sold'8 |
    la'2\melisma |
    lad' |
    si'8.[ dod''16] si'16[ la' sold']\melismaEnd fad' |
    si'8[ dod''16 la'] sold'8 fad' |
    mi'4 r |
    R2 |
    r4 dod'' |
    si'16*2/3[\melisma dod'' red''] mi''[ red'' dod''] si'[ la' sold'] fad'[ mi' red'] |
    dod'8[ dod''16 la']\melismaEnd sold'8 fad' |
    \grace fad' sold'4 r |
    R2 |
    r4 dod'' |
    si' mi''8. mi''16 |
    la'4 si'8 si' |
    sid'([ dod'']) mi''4 |
    la'4 si'8 si' |
    sid'([ dod'']) mi''4 |
    la' si'8 si' |
    mi'4 r |
    R2*19 |
    r4
  }
  \tag #'voix1 {
    \clef "soprano/treble" r8 |
    R2*133 |
    r4 r8 si' |
    mi''4 red''16[ mi''] fad''[ mi''] |
    si'4 lad'16[ si'] dod''[ si'] |
    sold'8.[ si'16] la'!16[ sold'] fad'[ mi'] |
    si'8 si' r si'8 |
    dod''8.[ red''16] mi''8 mi'' |
    mi'' si' r si' |
    dod''8.[ red''16] mi''8 mi'' |
    mi''8[ si'] r re'' |
    \ficta re''[ dod''] dod'' \ficta sid' |
    dod''4 red''! |
    mi'' fad''8 fad'' |
    sold''4 mi'' |
    mi'' red''8 red'' |
    red''([ mi'']) mi''4 |
    mi''4 red''8 red'' |
    red''([ mi'']) mi''4 |
    mi''4 red''8 red'' |
    mi''4 r |
    R2 |
    r4
  }
  \tag #'voix2 {
    \clef "soprano/treble" r8 |
    R2*133 |
    r4 r8 si' |
    mi''4 red''16[ mi''] fad''[ mi''] |
    si'4 lad'16[ si'] dod''[ si'] |
    sold'8.[ si'16] la'!16[ sold'] fad'[ mi'] |
    si'8 si' r sold' |
    la'8.[ si'16] dod''8 dod'' |
    si' sold' r sold' |
    la'8.[ si'16] dod''8 dod'' |
    si'[ sold'] r si' |
    si'[ la'] la' sold' |
    la'4 la'8[ sold'] |
    sold'[ la'] la' si' |
    si'4 dod'' |
    dod'' si'8 si' |
    sid'([ dod'']) dod''4 |
    dod''4 si'8 si' |
    sid'([ dod'']) dod''4 |
    dod''4 si'8 si' |
    si'4 r |
    R2 |
    r4
  }
  \tag #'voix3 {
    \clef "alto/treble" r8 |
    R2*133 |
    r4 r8 si8 |
    mi'4 red'16[ mi'] fad'[ mi'] |
    si'4 lad'16[ si'] dod''[ si'] |
    sold'8.[ si'16] la'!16[ sold'] fad'[ mi'] |
    si'8 si r mi' |
    dod''8.[ si'16] la'8 la' |
    sold' mi' r mi' |
    dod''8.[ si'16] la'8 la' |
    sold'8[ mi'] r mi' |
    mi'4 mi'8 mi' |
    mi'[ fad'] fad'[ sold'] |
    sold'[ la'] la'8. la'16 |
    sold'4 sold' |
    fad' fad'8 fad' |
    fad'([ mi']) sold'4 |
    fad' fad'8 fad' |
    fad'([ mi']) sold'4 |
    fad' fad'8 fad' |
    mi'4 r |
    R2 |
    r4
  }
>>
