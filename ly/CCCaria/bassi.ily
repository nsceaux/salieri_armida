\clef "bass" r8 |
r mi(\mf sold) mi-! |
r \once\slurDashed si,( red) si,-! |
mi16-!\f si,-! mi-! si,-! fad-! si,-! fad-! si,-! |
sold-! si,-! sold-! si,-! mi-! sold,-! mi-! sold,-! |
la, dod la, dod si, si, si si |
mi mi si, sold, mi,8 r |
r mi(\mf sold) mi-! |
r si,( red) si,-! |
mi16\f mi, sold, si, la, fad, si, la, |
sold, sold fad mi red8 si, |
mi fad sold sold, |
la,4 r |
si, r |
si, r |
si, r |
r8 mi(\p sold) mi-! |
r8 si,( red) si,-! |
mi16-! si,-! mi-! si,-! fad-! si,-! fad-! si,-! |
sold\f si, sold si, mi sold, mi sold, |
la, dod la, dod si, si, si, si, |
mi mi si, sold, mi,8 r |
r mi(\p sold) mi-! |
r si,( red) si,-! |
mi16 si, mi si, fad si, fad si, |
sold\f si, sold si, mi sold, mi sold, |
la,\p dod la, dod si, si, si si |
mi mi si, sold, mi,8 r |
si,4\f r |
si,\f r |
si,8\f si si, sold |
fad4 fad, |
r8 mi(\p sold) mi-! |
r si,( red) si,-! |
r mi( sold) mi-! |
r si,( red) si,-! |
fad,8\f r fad\p r |
si,4 r |
fad,8\f r fad\p r |
si,4 r |
R2 |
r4 fad8\p fad, |
si, dod red red |
mi mi fad fad, |
si,4 r |
R2 |
r4 fad8\p fad, |
si, dod red red |
mi mi fad fad, |
r8 si,( red) si,-! |
r mi8( dod) lad,-! |
si, dod16 red mi8 mi |
fad2:16\f |
fad4: fad,: |
si,8 si si si |
lad lad sold sold |
sol( fad) sol( fad) |
sol( fad) lad fad |
si, dod red red |
mi mi fad fad, |
si,4 r |
si\p la sold fad |
mid dod |
r8 fad la la |
si si dod' dod |
r fad fad, r |
la4\f mi |
la, r |
si\p fad |
si, r |
sol\f r |
do8\p r re r |
sol,4\f r |
do8\p r re r |
red!2\mf |
mi8\p r red! r |
mi4 sol8\f sol |
la2:8\fp |
si: |
do': |
sol: |
la: |
si8 si si, si, |
mi4 r |
r8 mi-\sug\p( sold!) mi-! |
r si,( red) si,-! |
mi16 si, mi si, fad si, fad si, |
sold\f si, sold si, mi sold, mi sold, |
la, dod la, dod si, si, si, si, |
mi, mi si, sold, mi,8 r |
r mi(\p sold) mi-! |
r si,( red) si,-! |
mi16 si, mi si, fad si, fad si, |
sold\f si, sold si, mi sold, mi sold, |
la,\p dod la, dod si, si, si, si, |
mi, mi si, sold, mi,8 mi |
re'4\f si16(\p dod' re' si) |
sold4\f si16(\p la sold fad) |
mid4 dod |
fad16 fad fad fad mi!4:16 |
red!:\f dod: |
si,:\f red:-\sug\p |
mi: dod: |
si,4\f r |
si, r |
si, r |
r8 la(\p dod') la-! |
r mi8( sold) mi-! |
r la( dod') la-! |
r mi sold sold, |
la,4(\p si,) |
dod(\f si,)\p |
la,( si,) |
dod(\f si,)\p |
la,( si,) |
dod8 dod dod dod |
do do do do |
si,4 r8 la, |
sold,\f la, si, si |
mi4 r |
R2 |
r4 la,8\p la |
sold4 r8 sold, |
la, la si si, |
mi, mi r4 |
R2 |
r4 la,8\p la |
sold4:16 dod': |
la: si: |
sid8(\mf dod') sold4:\p |
la: si: |
sid8(\mf dod') sold4:\f |
la: si: |
mi16 mi' si sold mi8 si, |
mi4 \once\slurDashed red16( mi fad mi) |
si4 \once\slurDashed lad16( si dod' si) |
sold8. si16 la! sold fad mi |
si,8 si si16 la sold fad |
mi2:16 |
mi: |
mi: |
mi8 mi mi, r |
mi4\p mi |
la4:16 si: |
dod': red: |
mi:\f dod': |
la: si: |
sid8\mf dod' sold4: |
la: si: |
sid8\mf dod' sold4: |
la: si: |
mi2:8 |
mi: |
\custosNote mi8 \stopStaff
