\clef "alto" r8 |
r sold(\mf si) sold-! |
r8 red( fad) red-! |
mi'16-!\f si-! mi'-! si-! fad'-! si-! fad'-! si-! |
sold' si sold' si si4:16 |
dod': si: |
mi16 mi' si sold mi8 r |
r sold(\mf si) sold-! |
r red( fad) red-! |
mi16\f mi sold si la fad si la |
sold sold' fad' mi' red'8 si |
mi' fad' sold' sold |
dod' dod'-! red'-! mi'-! |
red'16 si dod' red' mi' dod' fad' mi' |
red' si dod' red' mi' dod' fad' mi' |
red'4 r |
r8 mi'(\p sold') mi'-! |
r8 si( red') si-! |
mi'16-! si-! mi'-! si-! fad'-! si-! fad'-! si-! |
sold'\f si sold' si mi' sold mi' sold |
la dod' la dod' si si si si |
mi' mi' si sold mi8 r |
r mi'(\p sold') mi'-! |
r si( red') si-! |
mi'16 si mi' si fad' si fad' si |
sold'\f si sold' si mi' sold mi' sold |
la\p dod' la dod' si si si' si' |
mi' mi' si sold mi8 r |
si4-\sug\f r |
si-\sug\f r |
si8-\sug\f si si sold |
fad4 fad |
r8 mi'(\p sold') mi'-! |
r si( red') si-! |
r mi'( sold') mi'-! |
r si( red') si-! |
fad'16-\sug\f fad'-\sug\p fad' fad' fad'4:16 |
fad'2: |
fad':-\sug\fp |
fad'8 r \once\slurDashed si'4\rf( |
lad') sold'(-\sug\rf |
\ficta sol'8)( fad') fad'-\sug\p fad |
fad' fad' fad' fad |
sold mi fad fad |
si r si'4-\sug\rf( |
lad') sold'\rf |
sol'8( fad') fad'-\sug\p fad |
fad' fad' fad' fad |
sold mi fad fad' |
fad'2:16 |
sold'4: lad'16 lad' dod'' dod'' |
si' si dod' red' mi'8 mi' |
fad'2:16\f fad': |
si8 red' red' red' |
dod' dod' si si |
si lad si lad |
si \ficta lad lad fad |
si dod' red' red' |
mi' mi' fad' fad |
\sug si4 r |
fad'4\p fad' |
mid' fad' |
sold' dod' |
r8 fad' fad' la |
si si dod' dod |
r fad fad' r |
la4-\sug\f sold |
la r |
si'\p lad' |
si' r8 <<
  { si16 do'? |
    re'4 do'8 si |
    la8. si32 do'? si8 la |
    re'4 do'8 si |
    la8. si32 do'? si8 la |
    si do'4 do'8 | } \\
  { sol16\mf la |
    si4-\sug\f la8-\sug\p sol |
    sol?8. la16 sol8 fad |
    si4\f la8-\sug\p sol |
    sol?8. la16 sol8 fad |
    fad4.-\sug\mf do'8 | }
>>
si8-\sug\p r si r |
si r \ficta sol\f sol |
la2:8\fp |
si: |
do': |
sol': |
la': |
si'8 si' si si |
mi'4 r |
r8 sold-\sug\p( si) sold-! |
r red( fad) red-! |
mi'16 si mi' si fad' si fad' si |
sold'\f si sold' si mi' sold mi' sold |
la dod' la dod' si si si si |
mi mi' si sold mi8 r |
r8 sold(-\sug\p si) sold-! |
r red( fad) red-! |
mi'16 si mi' si fad' si fad' si |
sold'\f si sold' si mi' sold mi' sold |
la\p dod' la dod' si si si si |
mi mi' si sold mi8 mi' |
re''4\f si'16(\p dod'' re'' si') |
sold'4\f si'16(\p la' sold' fad') |
mid'4 dod' |
fad'2:16 |
fad'4:-\sug\f mi': |
<< red'2 { s4-\sug\f s-\sug\p } >> |
mi'4: dod': |
si16-!-\sug\f si-! dod'-! red'-! mi'-! dod'-! fad'-! mi'-! |
red' si dod' red' mi' dod' fad' mi' |
red'4 r |
r8 la(\p dod') la-! |
r mi8( sold) mi-! |
r la( dod') la-! |
r mi sold sold |
la4(-\sug\p si) |
dod'(-\sug\f si)-\sug\p |
la( si) |
dod'(\f si)\p |
la( si) |
r8 dod' dod' dod' |
r sol'(\mf fad' mi') |
red'4 r8 la |
sold\f la si si |
mi r mi'4(-\sug\mf |
red') dod'-\sug\mf( |
do'8) si la-\sug\p la |
sold4 r8 sold |
la la si si |
mi r mi'4(\mf |
red') \once\slurDashed dod'-\sug\mf( |
do'8) si la\p la |
sold4:16 dod': |
dod': si: |
sid8(\mf dod') sold4:16\p |
la: si: |
sid8(\mf dod') sold4:16\p |
la: si: |
mi16 mi' si sold mi8 si |
mi'4 \once\slurDashed red'16( mi' fad' mi') |
si'4 \once\slurDashed lad'16( si' dod'' si') |
sold'8. si'16 la'! sold' fad' mi' |
si8 si' si'16 la' sold' fad' |
la'2:16 |
sold': |
la': |
sold'8 mi' mi r |
mi'4\p mi'8 mi' |
mi' fad' fad' sold' |
sold' la' la' si' |
sold'4:16\f dod'': |
la': si': |
sid'8(\mf dod'') sold4: |
la: si: |
sid8(-\sug\mf dod') sold4: |
la: si: |
mi'2:8 |
mi': |
\custosNote sold8 \stopStaff
