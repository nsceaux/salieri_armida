\clef "treble" \transposition mi
r8 |
R2*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mi''4 fa'' |
    sol''4 fa''16 mi'' re'' do'' |
    re''4 mi''8 re'' |
    do'' do'' do'' }
  { do''4 re'' |
    mi''4 re''16 do'' sol' mi' |
    do''4 do''8 sol' |
    mi' mi' mi' }
>> r8 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 sol' | }
  { do'' sol' | }
>>
\tag#'tutti <>^"a 2." sol'4 sol'8 sol' |
sol'2:8 |
do''4 r |
sol' r |
sol' r |
sol' r | \allowPageTurn
R2*12 |
sol'4 r |
re'' r |
sol' r |
r8 re'' re'' r | \allowPageTurn
R2*4 |
re''2-\sug\fp~ |
re''~ |
re''-\sug\fp |
sol'4 r | \allowPageTurn
R2*3 |
r4 re''8 re'' |
re''4 r | \allowPageTurn
R2*3 |
r4 re''8 re'' |
re''4 r | \allowPageTurn
R2*2 |
re''2-\sug\f |
re'' |
sol'4 r | \allowPageTurn
R2*2 |
r4 re'' |
re''8 re'' re'' r |
mi''8 mi'' re'' re'' |
sol' sol r4 | \allowPageTurn
R2*25 | \allowPageTurn
r4 r8 <>\p \twoVoices #'(primo secondo tutti) <<
  { re''8 |
    mi''4 fa'' |
    sol'' fa''16 mi'' re'' do'' |
    re''8. mi''32 fa'' mi''8 re'' |
    do''4 }
  { sol'8 |
    do''4 re'' |
    mi'' re''16 do'' sol' mi' |
    do''4 do''8 sol' |
    mi'4 }
  { s8 | s2 | s-\sug\f }
>> r4 |
R2 |
r4 r8 \twoVoices #'(primo secondo tutti) <<
  { re''8 |
    mi''4 fa'' |
    sol'' fa''16 mi'' re'' do'' |
    re''8. mi''32 fa'' mi''8 re'' |
    do'' do'' }
  { sol'8 |
    do''4 re'' |
    mi'' re''16 do'' sol' mi' |
    do''4 do''8 sol' |
    mi' mi' }
  { s8-\sug\p | s2 | s-\sug\f | s-\sug\p }
>> r4 | \allowPageTurn
R2*7 | \allowPageTurn
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol'4 s |
    sol' s |
    sol' s | }
  { sol s |
    sol s |
    sol s | }
  { s r |
    s r |
    s r | }
>> \allowPageTurn
R2*4 |
\tag#'tutti <>^"a 2." do''2\p~ |
do''\fp~ |
do''~ |
do''-\sug\fp~ |
do''~ |
do'' |
do''8-! do''(-\sug\mf re'' do'') |
sol'4 r | \allowPageTurn
R2*8 |
r4 do''-\sug\p |
do'' do'' |
do'' re'' |
re''8(-\sug\mf do'') do''4-\sug\p |
re'' re'' |
re''8(-\sug\mf do'') do''4-\sug\p |
re'' re'' |
do''8 sol'16 mi' do'8 sol' |
do''4 r |
sol' r |
mi' r |
r8 sol' sol' r |
\twoVoices #'(primo secondo tutti) <<
  { do''2 | do''~ | do''~ | do''4 }
  { do'2 | do'~ | do'~ | do'4 }
>> r4 | \allowPageTurn
R2*3 |
\tag#'tutti <>^"a 2." do''4-\sug\f do'' |
re'' re'' |
re''8(-\sug\mf do'') do''4 |
re'' re'' |
re''8-\sug\mf do'' \twoVoices #'(primo secondo tutti) <<
  { mi''4 |
    re''2 |
    do''8 mi'' do'' mi'' |
    do'' mi'' do'' mi'' |
    do''4 }
  { do''4 |
    do'' sol' |
    mi'8 sol' mi' sol' |
    mi' sol' mi' sol' |
    mi'4 }
>>
