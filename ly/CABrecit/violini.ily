\clef "treble" R1 |
<<
  \tag #'primo {
    si'!4\p r re'' r |
    sol' r r2 |
    re''2\f r |
    r re''\p |
    r re'' |
    mib''4 r8. <sib' sib''>16\f q4 r |
    R1*2 |
    mib''2 r |
    r2 r4
  }
  \tag #'secondo {
    sol'4-\sug\p r sol' r |
    \ficta mib' r r2 |
    la'2-\sug\f r2 |
    r \ficta sib'-\sug\p |
    r sib' |
    \ficta sib'4 r8. <sib' sol''>16-\sug\f q4 r |
    R1*2 |
    do''2 r |
    r2 r4
  }
>>
