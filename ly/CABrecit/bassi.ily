\clef "bass" R1 |
fa4\p r si, r |
do r r2 |
fad2\f r |
r sol-\sug\p |
r fa |
mib4 r8. mib16\f mib4 r |
R1*2 |
lab2 r |
r r4
