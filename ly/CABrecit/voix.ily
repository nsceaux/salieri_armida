\clef "soprano/treble" fa''8. re''16 re''8 r8 r re'' re'' do'' |
si'!8 si' r si'16 re'' re''4 fa'8 sol' |
mib'8 mib' r sol'16 sol' do''8 do'' mib''8. mib''16 |
re''4 r re''8 la'16 la' la'8 sib' |
do'' do'' r16 do'' do'' re'' sib'4 sib' |
\ficta sib' sib'8 do'' re'' re'' fa'' re''16 sib' |
mib''8 mib'' r mib'' mib'' sib' r16 sib' sib' sib' |
sol'8 sol' r sol'16 lab' sib'4 sib'8 do'' |
reb''8 reb'' r fa'' reb'' reb'' reb'' do'' |
lab'4 lab'8 lab'16 sib' do''8 do'' r do''16 do'' |
do''4 re''8 mib'' re''4
