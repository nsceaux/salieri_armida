Mi -- se -- ra! Il Ciel m’op -- pri -- me,
m’ab -- ban -- do -- na l’a -- bis -- so.
E Ri -- nal -- do… Ah cru -- del! for -- se con -- giu -- ra anch’ e -- gli a’ dan -- ni mie -- i… l’ar -- me fa -- ta -- le
co -- me in sua ma -- no? Oh Di -- o! mi veg -- go an -- co -- ra
quel -- la lu -- ce fu -- ne -- sta su -- gli oc -- chi ba -- le -- nar; mi sen -- to an -- co -- ra
%l’i -- stes -- so ge -- lo al cuor, qua -- si vi -- ci -- na
un pre -- sa -- gio cru -- del…
