\clef "alto" R1 |
re'4-\sug\p r re' r |
do' r r2 |
fad'-\sug\f r |
r sol'\p |
r lab' |
sol'4 r8. mib'16-\sug\f mib'4 r |
R1*2 |
lab2 r |
r2 r4
