\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Armida
  \livretVerse#12 { Misera! Il Ciel m'opprime, }
  \livretVerse#12 { M'abbandona l'Abisso. }
  \livretVerse#12 { E Rinaldo… Ah crudel! forse congiura }
  \livretVerse#12 { Anch'egli a'danni miei… l'arme fatale }
  \livretVerse#12 { Come in sua mano? Oh Dio! mi veggo ancora }
  \livretVerse#12 { Quella luce funesta sugli occhi balenar; mi sento ancora }
  \livretVerse#12 { Un presagio crudel… }
} #}))
