\clef "alto" sold4 r r2 |
R1*6 |
r4 la\f sib2 |
R1*3 |
dod'2 r |
R1 |
re'4 r mi' r |
R1*4 |
fa'4 r re' r |
r2 r4 sol'\f |
fad'2 r |
R1*2 |
r2 sol'4\p r |
sol'2-\sug\f~ sol'-\sug\p~ |
sol'2 fa'4 r |
la'2-\sug\f r |
R1 | \allowPageTurn
r2 sib'4 sib'16-\sug\p sib' sib' sib' |
\ficta sib'2:32 sib': |
\ficta sib': sib': |
reb'':\f do'': |
do'':\p do'': |
reb'': reb'': |
\ficta reb''4 r r2 |
r2 do''4-\sug\p r |
do'2~ do'4 r |
R1*2 |
r2 fad'\p |
sol'8[\fermata r16 sol'\f sol'8. sol'16] fad'2~ |
fad' sol' r |
R1*2 |
sol'2 r |
mi'8.-\sug\f mi'16 mi'8. mi'16 red'2 |
r2 mi' |
R1*2 |
mi'4\f r la r |
r2 re'4 r |
r mi'
