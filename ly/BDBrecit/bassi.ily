\clef "bass" sold4 r r2 |
r2 la4 r |
r2 fad4 r |
r2 sol4 r |
R1*3 |
r4 la\f sib2 |
R1*3 |
dod2 r |
R1 |
re4 r mi r |
R1*4 |
fa4 r fa r |
r2 r4 sol\f |
fad2 r |
R1*2 |
r2 sol4\p r |
sol2\f~ sol\p~ |
sol2 fa4 r |
fa2\f r |
R1 | \allowPageTurn
r2 sib4 sib16\p sib sib sib |
\ficta sib2:16 sib: |
\ficta sib: sib: |
\ficta sib:\f do': |
do':\p mi!: |
mi: mi: |
mi4 r r2 |
r2 fa4\p r |
mib2~ mib4 r |
R1*2 |
r2 fad\p |
sol8\fermata[ r16 sol\f sol8. sol16] fad2~ |
fad sol r |
R1*2 |
fa!2 r |
mi8.\f mi16 mi8. mi16 red2 |
r2 mi |
R1*2 |
mi4\f r la r |
r2 re4 r |
r4 mi
