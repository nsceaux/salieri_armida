\clef "soprano/treble" <>^\markup\character Rinaldo
r8 mi''16 mi'' mi''8 si'16 si' sold'8 sold' r4 |
re''8 re''16 re'' re''8 mi'' do''4 r |
r8 la' do'' mi'' re'' re'' r4 |
r8 la'16 si' do''8 la'16 si' sol'4 r |
r4 r8 re'' re'' re'' re'' re'' |
si'8 si' r16 si' si' la' si'8 si' r4 |
si'8 si'16 si' do''8 re'' re'' sol'
\ffclef "soprano/treble" <>^\markup\character Armida
mi''8 dod''16 re'' |
re''8 la' r4 r2 |
sib'8 sib'16 sib' sib'8 do'' re'' re'' r fa'' |
re'' re'' do'' re'' \ficta sib'4 r16 sib' sib' la' |
\ficta sib'8 sib' r sib' sib' sib' sib' sib' |
la'4 r16 la' la' si' dod''8 dod'' r dod'' |
\ficta dod'' dod'' re'' mi'' mi'' sol' r16 sol' \ficta sib' la' |
fa'8 fa' r16 re'' re'' re'' do''8 do'' r sol' |
do'' do'' r do''16 do'' mi''4 re''8 mi'' |
do'' do'' r16 do'' do'' re'' sib'8 sib' r sib'16 sib' |
\ficta sib'4 la'8 sib' sol'8 sol' r16 sol' sol' la' |
sib'8 sib' r sib'16 sib' sib'4 sib'8 do'' |
la'8 la' r16 do'' do'' re'' si'!8 si' r16 fa'' fa'' re'' |
si'8 si' r do'' do'' sol' r4 |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r4 r8 la' la' la' la' re'' |
re'' la' la' la'16 la' fad'8 fad' r sol' |
la' la' r la'16 sib' do''8 do'' r mib'' |
do'' do'' do'' re''16 la' sib'8 sib' r4 |
\ffclef "soprano/treble" <>^\markup\character Armida
sol''8 mib''16 mib'' mib''8 re'' dod'' dod'' r dod''16 re'' |
mi''!4 mi''8 fa'' re''4 r16 re'' re'' re'' |
mib''8 mib'' r sol'' mib'' mib'' mib'' re'' |
do'' do'' r16 do'' do'' re'' mib''8 mib'' r4 |
\ficta mib''8 do''16 do'' do''8 re'' sib' sib' r4 |
\ficta sib'4 sib'8 do'' reb'' reb'' r reb''16 reb'' |
\ficta reb''4 do''8 reb'' sib' sib' r sib'16 sib' |
\ficta sib'4 sib'8 sol'' mi''!2 |
r8 mi'' mi'' sol'' sol''4 sib' |
reb''8 reb'' r reb'' sib' sib' r sib' |
sol' sol' r sol'16 sol' sol'4 sol'8 lab' |
sib'4 r16 sol' sib' lab' fa'8 fa'
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r8 fa' |
la'!8 la'
\ffclef "soprano/treble" <>^\markup\character Armida
r4 r16 do'' do'' fa'' fa''8 do'' |
do'' do''16 do'' sib'8 do'' la' la' r la'16 sib'! |
do''4 do''8 re'' mib''4 mib''8 re''16 mib'' |
do''8 do'' r do''16 re'' re'' la' la'8 sib'4 |
sol'4 r
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r4 r8 re'' |
re'' la' r si'! sol' sol' r re' sol' sol' sol' la' |
si' si' si' si'16 do'' re''8 re'' r re''16 re'' |
re''4 do''8 re'' si'4 r16 re'' mi'' fa'' |
fa''8 si' r4 re''8 re''16 re'' re''8 mi'' |
do''4 r r8 si' si' do'' |
la'8 la' r16 la' do'' si' sol'8 sol' r16 sol' si' la' |
si'4. mi''8 mi'' si' r si'16 si' |
si'4 la'8 si' sol' sol' r sol' |
si' si' do'' re'' do'' do'' r do'' |
do'' do'' re'' mi'' fa''! fa'' r16 re'' re'' do'' |
la'8 la' r4
