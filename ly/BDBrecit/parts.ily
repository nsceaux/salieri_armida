\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Rinaldo
    \livretVerse#12 { Questo arcano funesto }
    \livretVerse#12 { Spiegami per pietà. Del tuo spavento }
    \livretVerse#12 { Dimmi almen la cagion; }
    \livretVerse#12 { Determina, se puoi }
    \livretVerse#12 { I miei sospetti in rivelando i tuoi. }
    \livretPers Armida
    \livretVerse#12 { Ah siam perduti. Uno straniero ignoto }
    \livretVerse#12 { Nell' isola approdò. Lo spazio immenso }
    \livretVerse#12 { Del periglioso mar, che ci divide }
    \livretVerse#12 { Dal resto de' viventi, }
    \livretVerse#12 { Varcò sicuro, e i fieri mostri, e il giogo }
    \livretVerse#12 { Dirupato del colle, e il dolce incanto }
    \livretVerse#12 { Delle ninfe lascive, e fin del chiuso }
    \livretVerse#12 { Intricato recinto }
    \livretVerse#12 { Il fier custode in un sol giorno ha vinto. }
    \livretPers Rinaldo
    \livretVerse#12 { E non sospetti ancora }
    \livretVerse#12 { Come qui giunse, e d'onde, }
    \livretVerse#12 { A che venne, chi sia, dove s'asconde? }
    \livretPers Armida
    \livretVerse#12 { Questa è de'miei spaventi }
  }
  \null
  \column {
    \livretVerse#12 { La più fiera cagion. Poter maggiore }
    \livretVerse#12 { Del mio poter lo guida, e rende vane }
    \livretVerse#12 { Tutte le mie ricerche. Ah già col piede }
    \livretVerse#12 { Premo l'ampia ruvina }
    \livretVerse#12 { Dell'incendio crudel, che tutto intorno }
    \livretVerse#12 { Strugge, abbatte divora; }
    \livretVerse#12 { E la fiamma crudel non scopro ancora. }
    \livretPers Rinaldo
    \livretVerse#12 { E temi?… }
    \livretPers Armida
    \livretVerse#12 { E che potrei }
    \livretVerse#12 { Altro temer, ben mio, }
    \livretVerse#12 { Dall' irata del Ciel vindice mano, }
    \livretVerse#12 { Che di perderti, oh Dio? }
    \livretPers Rinaldo
    \livretVerse#12 { Paventi in vano. }
    \livretVerse#12 { Ah questi molli fregi, onde ti piacque }
    \livretVerse#12 { Avvilirmi così, non han sopita }
    \livretVerse#12 { Tutta la mia virtù, mi prende al fianco }
    \livretVerse#12 { Non vil l'acciaro, e inerme ancor saprei, }
    \livretVerse#12 { Non che ignoto guerriero, }
    \livretVerse#12 { Sfidare in tua difesa il mondo intero. }
  }
  \null
} #}))
