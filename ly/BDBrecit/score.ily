\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Rin. s1*6 s2.
        \set Staff.shortInstrumentName = \markup\character Arm. s4 s1*13
        \set Staff.shortInstrumentName = \markup\character Rin. s1*4
        \set Staff.shortInstrumentName = \markup\character Arm. s1*11 s2.
        \set Staff.shortInstrumentName = \markup\character Rin. s4 s4
        \set Staff.shortInstrumentName = \markup\character Arm. s2. s1*3 s2
        \set Staff.shortInstrumentName = \markup\character Rin. }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*2\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3\pageBreak
        s1*3\break s1*4\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1 s1. s1\break s1*3\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
