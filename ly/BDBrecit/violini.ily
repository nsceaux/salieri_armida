\clef "treble" sold4 r r2 |
R1*6 |
r4 la'4\f <<
  \tag #'primo { <sib' re'>2 | }
  \tag #'secondo { <fa'! sib>2 | }
>>
R1*3 |
<<
  \tag #'primo { la'2 }
  \tag #'secondo { mi'! }
>> r2 |
R1 |
<<
  \tag #'primo { la'4 r do''! r | }
  \tag #'secondo { fa'4 r sol' r | }
>>
R1*4 |
<<
  \tag #'primo { do''4 r si'! r | }
  \tag #'secondo { la'4 r sol' r | }
>>
r2 r4 <si' re'>4\f |
<re' la'>2 r |
R1*2 |
r2 <<
  \tag #'primo {
    sib'4\p r |
    mib''2(\f mi'')\p~ |
    mi'' re''4 r |
    mib''2\f
  }
  \tag #'secondo {
    re'4-\sug\p r |
    sib''2\f~ sib''\p~ |
    \ficta sib''2 la''4 r |
    do''2\f
  }
>> r2 |
R1 | \allowPageTurn
r2 <<
  \tag #'primo {
    re''4 reb''16\p reb'' reb'' reb'' |
    \ficta reb''2:32 reb'': |
    \ficta reb'': reb'': |
    sol'':\f mi''!: |
    mi'':\p sol'': |
    sol'': sol'': |
    sol''4
  }
  \tag #'secondo {
    fa'4 fa'16-\sug\p fa' fa' fa' |
    fa'2:32 fa': |
    fa': fa': |
    sib':-\sug\f << { sib': \ficta sib': } \\ { sol': sol':-\sug\p } >> sib': |
    \ficta sib': sib': |
    \ficta sib'4
  }
>> r4 r2 |
r2 <<
  \tag #'primo {
    lab'4\p r |
    r8. fa''16_\markup\italic dolce do''( fa'') la'( do'') fa'4 r |
  }
  \tag #'secondo {
    do'4-\sug\p r |
    << { la'2~ la'4 } \\ { fa'2~ fa'4 } >> r4 |
  }
>>
R1*2 |
r2 <<
  \tag #'primo {
    la''2\p |
    sib''8\fermata[ r16 re''\f re''8. re''16] re''2~ |
    re'' re''
  }
  \tag #'secondo {
    re''2-\sug\p |
    re''8\fermata[ r16 sib'-\sug\f sib'8. sib'16] <la' re'>2~ |
    q <re' si'!>
  }
>> r2 |
R1*2 |
<<
  \tag #'primo { re''2 }
  \tag #'secondo { si' }
>> r2 |
<<
  \tag #'primo {
    do''8.\f do'''16 do'''8. do'''16 si''2 |
  }
  \tag #'secondo {
    do''8.-\sug\f sol''16 sol''8. sol''16 fad''2 |
  }
>>
r2 <<
  \tag #'primo { si'2 | }
  \tag #'secondo { sol''2 | }
>>
R1*2 |
<si sold' mi''>4\f r <do'' mi' la> r |
r2 <re' la' fa''!>4 r |
r <sold' si' mi''>4
