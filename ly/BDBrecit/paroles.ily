Que -- sto ar -- ca -- no fu -- ne -- sto
spie -- ga -- mi per pie -- tà, del tuo spa -- ven -- to
dim -- mi al -- men la ca -- gion;
de -- ter -- mi -- na, se puo -- i
i miei so -- spet -- ti in ri -- ve -- lan -- do i tuo -- i.

Ah siam per -- du -- ti. U -- no stra -- nie -- ro i -- gno -- to
nell’ i -- so -- la ap -- pro -- dò. Lo spa -- zio im -- men -- so
del pe -- ri -- glio -- so mar, che ci di -- vi -- de
dal re -- sto de’ vi -- ven -- ti,
var -- cò si -- cu -- ro, e i fie -- ri mo -- stri, e il gio -- go
di -- ru -- pa -- to del col -- le, e il dol -- ce in -- can -- to
del -- le nin -- fe las -- ci -- ve, e fin del chiu -- so
in -- tri -- ca -- to re -- cin -- to
il fier cu -- sto -- de in un sol gior -- no ha vin -- to.

E non sa -- pe -- sti an -- co -- ra
co -- me qui giun -- se, e don -- de,
a che ven -- ne, chi si -- a, do -- ve s’as -- con -- de?

Que -- sta è de’ miei spa -- ven -- ti
la più fie -- ra ca -- gion: po -- ter mag -- gio -- re
del mio po -- ter lo gui -- da, e ren -- de va -- ne
tut -- te le mie ri -- cer -- che. Ah, già col pie -- de
pre -- mo l’am -- pia ru -- i -- na
dell’ in -- cen -- dio cru -- del, che tut -- to in -- tor -- no
strug -- ge, ab -- bat -- te, di -- vo -- ra;
e la fiam -- ma cru -- del, non sco -- pro an -- co -- ra.

E te -- mi?…

E che po -- tre -- i
al -- tro te -- mer, ben mi -- o,
dall’ i -- ra -- ta del Ciel vin -- di -- ce ma -- no,
che di per -- der -- ti, oh Dio?

Pa -- ven -- ti in -- va -- no.
Ah que -- sti mol -- li fre -- gi, on -- de ti piac -- que
av -- vi -- lir -- mi co -- sì, non han so -- pi -- ta
tut -- ta la mia vir -- tù, mi pen -- de al fian -- co
non vil l’ac -- cia -- ro, e i -- ner -- me an -- cor sa -- pre -- i,
non -- ché i -- gno -- to guer -- rie -- ro,
sfi -- da -- re in tua di -- fe -- sa,
sfi -- da -- re in tua di -- fe -- sa il mon -- do in -- te -- ro.
