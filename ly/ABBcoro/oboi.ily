\clef "treble"
\twoVoices #'(primo secondo tutti) <<
  { mib'2\p sol' |
    lab' sib' |
    sol' sib'4 s |
    sib' s sib' s |
    sib' }
  { r2 \tag #'secondo <>\p mib' |
    fa' fa' |
    mib' sol'4 s |
    fa' s lab' s |
    sol' }
  { s1*2 | s2. r4 | s r s r | }
>> r4 r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { sib'4( lab' sol' fa') |
    mib'2 sib'4 s |
    sib' s sib' s |
    sib'2 }
  { fa'4( fa' mib' re') |
    mib'2 sol'4 s |
    lab' s fa' s |
    sol'2 }
  { s1 | s2. r4 | s r s r | }
>> r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { sib'2 | do'' re'' | mib''4 }
  { re'2 | mib'2 fa' | sol'4 }
>> r8 sib'\f mib'' re'' do'' sib' |
lab' sol' fa' mib' lab'4 sib' |
do''\fermata r8 sib'\ff \twoVoices #'(primo secondo tutti) <<
  { sol''2~ | sol'' fa'' | mib''4 }
  { mib''2~ | mib''2. re''4 | mib''4 }
>> r4 r2 |
R1 |
