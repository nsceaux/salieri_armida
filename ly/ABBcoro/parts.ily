\piecePartSpecs
#`((oboi #:score-template "score-deux")
   (corni #:score-template "score-deux"
          #:tag-global ()
          #:instrument "Corni in Eb")
   (violino1)
   (violino2)
   (viola)
   (bassi)
   (silence #:on-the-fly-markup , #{\markup\tacet #}))
