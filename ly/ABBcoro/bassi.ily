\clef "bass" mib8\p mib mib mib mib2:8 |
mib:8 re8 sib, do re |
mib2:8 mib:8 |
fa:8 re:8 |
mib:8 mib8 mib re re |
do2:8 do:8 |
sib,:8 sib,:8 |
mib:8 mib:8 |
mib:8 re8 sib, do re |
mib2:8 mib8 mib re re |
do2:8 do:8 |
sib,:8 sib,:8 |
sib,:8 sib,:8 |
mib8 mib mib16\f fa32 sol lab sib do' re' mib'8 re' do' sib |
lab sol fa mib lab4 sib |
do'\fermata r8 sib\ff mib'8 re' do' sib |
lab sol fa mib lab16 lab lab lab sib4:16 |
mib8 mib mib mib mib2:8 |
mib4 sib8 r sol r fa r |
