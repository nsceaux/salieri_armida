<<
  \tag #'voix1 {
    \clef "soprano/treble" R1*2 |
    r2 sib'4 sib'8 sib' |
    sib'4 sib' sib' sib'8 sib' |
    sib'8. sib'16 sib'4 r8 sib' sib' sib' |
    mib''8. mib''16 mib''8 mib'' do''4 la' |
    sib' r r2 |
    R1*2 |
    sib'2 r8 sib' sib' sib' |
    mib''8. mib''16 mib''8 mib'' do''4 la' |
    sib' r sib' sib'8 sib' |
    do''4 do''8 do'' re''4. re''8 |
    mib''8. sib'16 sib'8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    do''\fermata r8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    mib' r r2 |
    R1 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R1*2 |
    r2 sol'4 sol'8 sol' |
    lab'4 lab' lab' lab'8 lab' |
    sol'8. sol'16 sol'4 r8 sol' fa' fa' |
    mib'8. mib'16 mib'8 mib' mib'4 mib' |
    re' r r2 |
    R1*2 |
    sol'2 r8 sol' fa' fa' |
    mib'8. mib'16 mib'8 mib' mib'4 mib' |
    re' r sib' sib'8 sib' |
    la'4 la'8 la' lab'4. lab'8 |
    sol'8. sol'16 sol'8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    do''\fermata r8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    mib' r r2 |
    R1 |
  }
  \tag #'voix3 {
    \clef "alto/treble" R1*2 |
    r2 mib'4 mib'8 mib' |
    fa'4 fa' fa' fa'8 fa' |
    mib'8. mib'16 mib'4 r8 mib' re' re' |
    do'8. do'16 do'8 do' do'4 fa' |
    fa' r r2 |
    R1*2 |
    mib'2 r8 mib' re' re' |
    do'8. do'16 do'8 do' do'4 fa' |
    fa' r re' re'8 re' |
    mib'4 mib'8 mib' fa'4. fa'8 |
    sol'8. mib'16 mib'8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    do''\fermata r8 sib' mib''[ re''] do''[ sib'] |
    lab' sol' fa' mib' lab'4 sib' |
    mib' r r2 |
    R1 |
  }
>>
