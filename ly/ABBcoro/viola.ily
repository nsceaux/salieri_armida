\clef "alto" mib'8\p mib' mib' mib' mib'2:8 |
mib':8 re'8 sib do' re' |
mib'2:8 mib':8 |
fa':8 re':8 |
mib':8 mib'8 mib' re' re' |
do'2:8 do':8 |
sib:8 sib:8 |
mib':8 mib':8 |
mib':8 re'8 sib do' re' |
mib'2:8 mib'8 mib' re' re' |
do'2:8 do':8 |
sib:8 sib:8 |
sib:8 sib:8 |
mib'8 mib' mib'16\f fa'32 sol' lab' sib' do'' re'' mib''8 re'' do'' sib' |
lab' sol' fa' mib' lab'4 sib' |
do''\fermata r8 sib'\ff mib''8 re'' do'' sib' |
lab' sol' fa' mib' lab'16 lab' lab' lab' sib'4:16 |
mib'8 mib' mib' mib' mib'2:8 |
mib'4 sib'8 r sol' r fa' r |
