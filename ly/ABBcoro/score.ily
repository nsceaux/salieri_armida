\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniEbInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 1 \super mo } }
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 2 \super do } }
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Alto }
      } \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
    >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*2 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*3\pageBreak
        s1*3 s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
