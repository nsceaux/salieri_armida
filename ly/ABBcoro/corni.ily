\clef "treble" \transposition mib
<>\p \twoVoices #'(primo secondo tutti) <<
  { do''1 | do''2 sol' | do' }
  { do'1 | do'2 sol | do' }
>> r2 |
R1*3 |
\twoVoices #'(primo secondo tutti) <<
  { sol'1 | do''2 }
  { sol1 | do'2 }
>> r2 |
R1*3 |
r2 \twoVoices #'(primo secondo tutti) <<
  { sol'2 | sol'2 sol' | sol'4 }
  { sol2 | sol sol | mi'4 }
>> r4 r2 |
R1 |
r4\fermata r8 sol'-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed mi''2~ | mi''2 re'' | do''4 }
  { do''2~ | do''2. re''4 | do''4 }
>> r4 r2 |
R1 |
