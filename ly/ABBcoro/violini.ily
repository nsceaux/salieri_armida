\clef "treble" mib'16(\p sib sol sib) mib'( sib sol sib) sol'( mib' sib mib') sol'( mib' sib mib') |
lab'( fa' do' fa') lab'( fa' do' fa') sib'( fa' re' fa') sib'( fa' re' fa') |
sol'( mib' sib mib') sol'( mib' sib mib') sib'( sol' mib' sol') sib'( sol' mib' sol') |
sib'( lab' fa' lab') sib'( lab' fa' lab') sib'( lab' fa' lab') sib'( lab' fa' lab') |
sib'( sol' mib' sol') sib'( sol' mib' sol') sib'( sol' mib' sol') sib'( fa' re' fa') |
<<
  \tag #'primo {
    sib'16( \ficta la' sib' do'') sib'( do'' re'' mib'') do'' do'' do'' do'' \ficta la'! la' la' la' |
    sib'16( fa' re' fa')
  }
  \tag #'secondo {
    mib'16 mib' mib' mib' mib'4:16 mib'16 re' mib' fa' mib'4:16 |
    re'16 fa' re' fa'
  }
>> lab'16( fa' sib lab') sol'( mib' sib sol') fa'( re' sib re') |
mib'( sib sol sib) sol'( mib' sib mib') sib'( sol' mib' sol') sib'( sol' mib' sol') |
\ru#4 { sib'( lab' fa' lab') } |
sib'16( sol' mib' sol') sib'( sol' mib' sol') sib'( sol' mib' sol') sib'( fa' re' fa') |
<<
  \tag #'primo {
    sib'16( \ficta la' sib' do'') sib'( do'' re'' mib'') do''4:16 \ficta la'!:16 |
    sib'16( fa' re' fa')
  }
  \tag #'secondo {
    mib'2:16 mib'16 re' mib' fa' mib'4:16 |
    re'16 fa' re' fa'
  }
>> \ru#3 { sib'16( fa' re' fa') } |
do''16( la' mib' la') do''( la' mib' la') re''( sib' fa' sib') re''( sib' fa' sib') |
mib''( sib' sol' sib') mib'16\f fa'32 sol' lab' sib' do'' re'' mib''16 mib'' re'' re'' do'' do'' sib' sib' |
lab' lab' sol' sol' fa' fa' mib' mib' lab'4:16 sib':16 |
do''4\fermata r8 sib'\ff <<
  { sol''16 sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    sol''2:16 fa''4:16 fa'':16 | } \\
  { mib''4:16 mib'':16 |
    mib''2:16 mib''4:16 re'':16 }
>>
mib''16 sib' mib'' sol'' sib'' sol'' mib'' sib' sol'' mib'' sib' sol' mib'' sib' sol' sib' |
mib'4-! sib'-! sol'-! fa'-! |
