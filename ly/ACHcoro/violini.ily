\clef "treble" R1 |
<re' re'' re'''>1\ff |
R1 |
<mib'' mib'''>1-\sug\ff |
re'''4 r <<
  \tag #'primo { 
    re'2:16\p |
    mib':16 fa':16 |
    re':16 re':16 |
    mib':16 fa':16 |
    re':16 re':16\fp |
    do':16\fp do':16\fp |
    re':16\fp re':16\fp |
    do':16\fp do':16\fp |
  }
  \tag #'secondo {
    sib2:16-\sug\p |
    sib4:16 do':16 do':16 do':16 |
    sib2:16 sib:16 |
    sib4:16 do':16 do'2:16 |
    sib2:16 sib:16-\sug\fp |
    sib:16-\sug\fp la:16-\sug\fp |
    sib:16-\sug\fp sib:16-\sug\fp |
    sib:16-\sug\fp la:16-\sug\fp |
  }
>>
sib4 r8 sib\ff si2:16 |
\ficta si:16 <<
  \tag #'primo {
    << sol''2 <si' re'>4 >> |
    mib''4 do'' dod'2:16\ff |
    \ficta dod':16_"ten." <la' la''> |
    fad''4 re'' sol'2:16\fp |
    sol':16\fp fa'!:16\fp |
    re':16\p re':16 |
    do':16 do':16 |
    re':16 re':16\pp |
    do':16 do':16 |
    sib'16 sib'' la'' sol'' fa'' mib'' re'' do'' sib' sib' la' sol' fa' mib' re' do' |
  }
  \tag #'secondo {
    si2:16 |
    do':16 dod':16-\sug\ff |
    \ficta dod':16 dod':16 |
    re':16 re':16-\sug\fp |
    mib':16-\sug\fp do':16-\sug\fp |
    sib:16-\sug\p sib:16 |
    sib:16 la:16 |
    sib:16 sib:16-\sug\pp |
    sib:16 la:16 |
    sib4 r sib'8 la'16 sol' fa' mib' re' do' |
  }
>>
sib2:16 sib:16 |
