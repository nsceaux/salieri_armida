\clef "bass" re2:16\p re:16 |
re:16\ff re:16 |
do:16\p do:16 |
do:16\ff do:16 |
sib,2:16\p sib,:16\p |
sib,:16 la,:16 |
sib,:16 sib:16 |
sib:16 la:16 |
sib:16 re:16\fp |
mib:16\fp fa:16\fp |
sib:16\fp re:16\fp |
mib:16\fp fa:16\fp |
sib4 r8 sib,\ff si,2:16 |
\ficta si,:16 si,:16 |
do:16 dod:16\ff |
\ficta dod:16 dod:16 |
re:16 si,:16\fp |
do!:16\fp la,:16\fp |
sib,:16\p re:16 |
mib:16 fa:16 |
sib:16 re:16\pp |
mib:16 fa:16 |
sib,:16 sib,:16 |
sib,16 sib la sol fa mib re do sib, sib la sol fa mib re do |
