\clef "treble" \transposition sib
R1 |
\tag #'tutti <>^"a 2." mi''1\ff |
R1 |
re''-\sug\ff |
do''4 r r2 |
R1 |
r2 sol'8 r sol' r |
do''4 r r2 |
r2 \twoVoices #'(primo secondo tutti) <<
  { mi''4 s |
    re'' s re'' s |
    mi'' s mi'' s |
    re'' s re'' s |
    do'' }
  { do''4 s |
    do'' s sol' s |
    do'' s do'' s |
    do'' s sol' s |
    do'' }
  { s4 r |
    s r s r |
    s r s r |
    s r s r | }
>> r4 r2 |
R1*11 |
