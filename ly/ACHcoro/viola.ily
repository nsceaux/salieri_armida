\clef "alto" re'2:16\p re':16 |
re':16-\sug\ff re':16 |
mib':16-\sug\p mib':16 |
mib':16-\sug\ff mib':16 |
re':16-\sug\p fa:16-\sug\p |
sol:16 fa:16 |
fa:16 fa:16 |
sol:16 la:16 |
sib4:16 fa:16 fa2:16-\sug\fp |
sol:16-\sug\fp fa:16-\sug\fp |
fa:16-\sug\fp fa:16-\sug\fp |
sol:16-\sug\fp fa:16-\sug\fp |
sib4 r8 sib-\sug\ff si2:16 |
\ficta si:16 si:16 |
do':16 dod':16-\sug\ff |
\ficta dod':16 dod':16 |
re':16 si:16-\sug\fp |
do'!:16-\sug\fp la:16-\sug\fp |
sib16-\sug\p fa fa fa fa4:16 fa2:16 |
sol:16 fa:16 |
fa:16 fa:16-\sug\pp |
sol:16 fa:16 |
sib:16 sib:16 |
sib:16 sib:16 |
