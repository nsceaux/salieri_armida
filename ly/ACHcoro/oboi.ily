\clef "treble" R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { re'''1 | }
  { re'' | }
>>
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { mib'''1 | re'''4 }
  { mib''1 | re''4 }
>> r4 r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''8 s re'' s | mib''4 }
  { sib'8 s sib' s | do''4 }
  { s8 r s r | }
>> r4 r2 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''4 s |
    do'' s do'' s |
    re'' s re'' s |
    do'' s do'' s |
    sib' s si'2~ |
    si' }
  { sib'4 s |
    sib' s la' s |
    sib' s sib' s |
    sib'4 s la' s |
    sib' s si'2~ |
    si' }
  { s4 r |
    s r s r |
    s r s r |
    s r s r |
    s r <>-\sug\ff }
>> r2 |
r <>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { dod''2~ | dod'' }
  { dod''2~ | dod'' }
>> r2 |
R1*8 |
