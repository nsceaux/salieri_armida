\tag #'(primo tutti) \clef "alto"
\tag #'secondo \clef "tenor"
R1 |
re'1\ff |
R1 |
mib'1-\sug\ff |
re'4 r r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { s2 re'8 s re' s | mib'4 }
  { s2 sib8 s sib s | do'4 }
  { r2 s8 r s r | }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { s2 re'4 s |
    do' s do' s |
    re' s re' s |
    do' s do' s |
    sib s si2~ |
    si s |
    s dod'~ |
    dod'
  }
  { s2 sib4 s |
    sib s la s |
    sib s sib s |
    sib s la s |
    sib s si2~ |
    si s |
    s dod'~ |
    dod' }
  { r2 s4 r |
    s r s r |
    s r s r |
    s r s r |
    s r s2-\sug\ff |
    s r |
    r s-\sug\ff | }
>> r2 |
R1*8 |
