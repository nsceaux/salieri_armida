\piecePartSpecs
#`((oboi #:score-template "score-deux-voix")
   (corni #:tag-global () #:instrument "Corni in B"
          #:score-template "score-deux-voix")
   (tromboni #:score-template "score-deux-voix")
   (fagotti #:score "score-fagotti")

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\column {

\livretVerse#12 { Qual Sibilo orrendo }
\livretVerse#10 { Per l'aer rimbomba? }
\livretVerse#10 { Qual braccio tremendo }
\livretVerse#10 { Ci opprime cosi? }

} #}))

