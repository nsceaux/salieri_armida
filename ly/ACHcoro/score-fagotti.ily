\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Fagotti" \haraKiri } <<
      \global \keepWithTag #'tutti \includeNotes "fagotti"
    >>
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
    >>
  >>
  \layout { indent = \largeindent }
}
