<<
  \tag #'(voix1 basse) {
    \clef "soprano/treble" R1*4 |
    r4 r8 sib' re''4 re''8 re'' |
    mib''4 mib''8 mib'' fa''4 fa''8 do'' |
    re''4 re'' r2 |
    r4 r8 mib'' fa''4 fa''8 do'' |
    re''4 re''8 sib' fa''[ re''] sib' fa' |
    sol'[ sib'] mib'' sol'' fa''[ re''] do'' fa'' |
    re''4 r8 sib' fa''[ re''] sib' fa' |
    sol'[ sib'] mib'' sol'' fa''[ re''] do'' fa'' |
    sib'4 r r2 |
    r4 r8 re'' sol''4 fa''8 fa'' |
    mib''4 do'' r2 |
    r4 r8 mi'' la''4 sol''8 sol'' |
    fad''4 re''8 re'' sol''4 sol''8 sol'' |
    mib''!4 do''8 do'' fa''!4 fa''8 fa'' |
    re''4 r8 re''^\p re''4 re''8 re'' |
    do''4 do''8 do'' do''4 do''8 do'' |
    re''4 r8 re''^\pp re''4 re''8 re'' |
    do''4 do''8 do'' do''4 do''8 do'' |
    sib'4 r r2 |
    R1 |
  }
  \tag #'voix2 {
    \clef "alto/treble" R1*4 |
    r4 r8 fa' fa'4 fa'8 fa' |
    sol'4 sol'8 sol' fa'4 fa'8 fa' |
    fa'4 fa' r2 |
    r4 r8 sol' fa'4 fa'8 fa' |
    fa'4 fa'8 fa' fa'4 fa'8 fa' |
    mib'4 sol'8 sib' sib'4 fa'8 fa' |
    fa'4 r8 fa' fa'4 fa'8 fa' |
    mib'4 sol'8 sib' sib'4 fa'8 fa' |
    sib'4 r r2 |
    r4 r8 sol' sol'4 sol'8 sol' |
    sol'4 sol' r2 |
    r4 r8 la' la'4 la'8 la' |
    la'4 fad'8 la' sol'4 sol'8 sol' |
    sol'4 mib'8 sol' fa'!4 fa'8 fa' |
    fa'4 r8 fa'^\sug\p fa'4 fa'8 fa' |
    sol'4 sol'8 sol' fa'4 fa'8 fa' |
    fa'4 r8 fa'^\sug\pp fa'4 fa'8 fa' |
    sol'4 sol'8 sol' fa'4 fa'8 fa' |
    fa'4 r r2 |
    R1 |
  }
  \tag #'voix3 {
    \clef "tenor/G_8" R1*4 |
    r4 r8 sib sib4 sib8 sib |
    sib4 do'8 do' do'4 do'8 do' |
    re'4 re' r2 |
    r4 r8 do' do'4 do'8 do' |
    sib4 sib8 sib sib4 re'8 sib |
    sib4 sib8 mib' re'[ sib] la la |
    sib4 r8 sib sib4 re'8 sib |
    sib4 sib8 mib' re'[ sib] la la |
    sib4 r r2 |
    r4 r8 re' re'4 re'8 re' |
    mib'4 sol' r2 |
    r4 r8 \ficta mi' mi'4 mi'8 mi' |
    fad'4 la'8 re' re'4 re'8 re' |
    do'4 do'8 do' do'4 do'8 do' |
    sib4 r8 sib^\sug\p sib4 sib8 sib |
    sib4 sib8 sib la4 la8 la |
    sib4 r8 sib^\sug\pp sib4 sib8 sib |
    sib4 sib8 sib la4 la8 la |
    sib4 r r2 |
    R1 |
  }
  \tag #'voix4 {
    \clef "bass" R1*4 |
    r4 r8 sib sib4 sib8 sib |
    sib4 sib8 sib la4 la8 la |
    sib4 sib r2 |
    r4 r8 sib la4 la8 la |
    sib4 sib8 sib re4 re8 re |
    mib4 mib8 mib fa4 fa8 fa |
    sib4 r8 re re4 re8 re |
    mib4 mib8 mib fa4 fa8 fa |
    sib4 r r2 |
    r4 r8 si si4 si8 si |
    do'4 do' r2 |
    r4 r8 dod' dod'4 dod'8 dod' |
    re'4 re8 re si4 si8 si |
    do'!4 do'8 do' la4 la8 la |
    sib4 r8 re^\sug\p re4 re8 re |
    mib4 mib8 mib fa4 fa8 fa |
    sib4 r8 re^\sug\pp re4 re8 re |
    mib4 mib8 mib fa4 fa8 fa |
    sib,4 r r2 |
    R1 |
  }
>>
