\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr  } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniBInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr \haraKiri } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
      \new Staff \with { \tromboniInstr } <<
        \global \keepWithTag #'tutti \includeNotes "tromboni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*4 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*3\pageBreak
        s1*4\pageBreak
        s1*3\pageBreak
        s1*3\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
