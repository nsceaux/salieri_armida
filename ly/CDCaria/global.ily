\key fa \major
\tempo "Andante con moto" \midiTempo#60
\time 2/4 s2*63 \bar "||"
\time 4/4 \grace s8 s1
\time 2/4 \grace s8 s2 \bar "||"
\tempo "Più andante" s2*4
\time 4/4 s4 \bar "|."
