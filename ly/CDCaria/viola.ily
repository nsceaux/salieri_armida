\clef "alto" fa'8 r fa' r |
r16 fa'-\sug\p fa' fa' r la' la' la' |
r sol' sol' sol' r fa' fa' fa' |
r mi' mi' mi' r mi' mi' mi' |
fa'8 r fa' r |
<< fa'2:32 { s4\p s\f } >> |
mi'4: mi'8 r |
r fa'8-.(\p fa'-.) r |
r8 fa'-.( fa'-. fad'-.) |
sol'16 sol' sol' sol' sol' sol' sol' sol' |
sol'2:16 |
sol': |
sol'16 sol' sol' sol' sol4 |
r16 do'32\f re' mi'16 fa' sol'-! mi'-! re'-! do'-! |
fa'2:16\fp |
sol'4: sol: |
lab8 r r4 |
re'4( do'8) r |
sol'2:16-\sug\p |
sol': |
sol'16 sol' sol' sol' sol4 |
r16 mi'32\fp fa' sol'16 sol' mi'4:16 |
fa'16 fa' fa' fa' fa'4: |
fa'16\f la' la' la' la'4: |
sol'4: sol: |
do'8 r la\p r |
sib r sol r |
mi r fa r |
re r mi r |
fa r r mi' |
re'2( |
reb') |
do'16 sol' sol' sol' r sol' sol' sol' |
si'4-\sug\fp\fermata sol' |
sol'\fermata r |
fa'8\mf r fa' r |
r16 fa'-\sug\p fa' fa' r la' la' la' |
r sol' sol' sol' r fa' fa' fa' |
mi'2\f |
fa'4 r |
<>_\markup\small\italic dolce sib8 r sol r |
fa4 r |
sib8 r sol r |
fa4\fermata r |
fa8.\p do'32 re' mib'16-!-\sug\f do'-! mib'-! do'-! |
sib8.\p sib32 do' re'16-! sib-! re'-! sib-! |
sol8.-\sug\fp re32 mi fa16-!-\sug\f re-! fa-! re-! |
do\p do'' do'' do'' do''4:16 |
do''2: |
do'': |
do'': |
do'': |
do'4:16\p\cresc sib: |
la:\mf mi:\f |
fa8-\sug\fp la sib sib |
do'4: do': |
reb'8 r r4 |
sol4( fa8) r |
r do' do' do' |
r re' re' re' |
r re' re' re' |
r la-\sug\cresc la la |
r16 la'-\sug\f la' la' la' la' la' la' |
sib'4 r r2 |
R2 |
r16 do''\p do'' do'' do''4:16 |
r16 do'' do'' do'' do''4: |
sib2\fp |
do' |
fa'4\fermata
