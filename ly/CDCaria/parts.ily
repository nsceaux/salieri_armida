\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (oboi #:score-template "score-deux-voix")
   (fagotti #:music , #{

\quoteBassi "CDCbassi"
<>^\markup\tiny B. \cue "CDCbassi" { \mmRestInvisible s2*5 \mmRestVisible }
s2*21
<>^\markup\tiny B. \cue "CDCbassi" { \mmRestInvisible s2*7 \mmRestVisible }
s2*2
<>^\markup\tiny B. \cue "CDCbassi" { \mmRestInvisible s2*3 \mmRestVisible }
s2*2
<>^\markup\tiny B. \cue "CDCbassi" { \mmRestInvisible s2*4 \mmRestVisible }
s2*3
<>^\markup\tiny B. \new CueVoice {
  do16\p do' do' do' do'4:16 |
  \mmRestInvisible do'2: |
  do': |
  do': |
  do': | \mmRestVisible
}
s2*2
\new CueVoice { \voiceOne fa8\fp^"B." la sib sib, | }
s2*3
<>^\markup\tiny B. \cue "CDCbassi" { \mmRestInvisible s2*4 \mmRestVisible }
            #})

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Rinaldo
    \livretVerse#10 { Ah non lasciarmi no. }
    \livretVerse#10 { (Che barbaro rigor!) }
    \livretVerse#10 { Quel che vorrai farò. }
    \livretVerse#10 { Perdona a un folle amor }
    \livretVerse#10 { L’affanno mio. }
    \livretVerse#10 { Vorrei partir… vorrei }
  }
  \null
  \column {
    \livretVerse#10 { Darle l’estremo addio… }
    \livretVerse#10 { Poveri affetti miei! }
    \livretPers Armida
    \livretVerse#10 { Aspetta traditor }
    \livretPers Rinaldo
    \livretVerse#10 { Eccola, oh Dio! }
  }
  \null
}
       #}))
