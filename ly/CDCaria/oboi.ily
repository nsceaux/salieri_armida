\clef "treble" R2*9
<>\p \twoVoices #'(primo secondo tutti) <<
  { sol''2~ | sol''~ | sol''~ | sol'' | }
  { sol'2~ | sol'~ | sol'~ | sol' | }
>>
R2*5 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { sol''2~ | sol''~ | sol'' | }
  { sol'2~ | sol'~ | sol' | }
>>
R2*2 |
r4 r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''8 | mi''4. re''8 | do''4 }
  { re''8 | do''4. si'8 | do''4 }
>> r4 |
R2*7 |
<>\fp \twoVoices #'(primo secondo tutti) <<
  { si'2\fermata | do''4\fermata }
  { fa'2\fermata | mi'4\fermata }
>> r4 | \allowPageTurn
R2*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol''2( | fa''4) }
  { \once\slurDashed sib'2( | la'4) }
>> r4 |
R2*4 |
\twoVoices #'(primo secondo tutti) <<
  { do''2 | re'' | re'' | mi''4 }
  { la'2 | sib' | si' | do''4 }
  { s4\p s\f }
>> r4 |
R2*4 |
\twoVoices #'(primo secondo tutti) <<
  { sib'8. sib'16 mi''8. mi''16 |
    \sug fa''4 sol'' |
    la'' s |
    la''4. sol''8 |
    fa''4 }
  { sol'8. sol'16 do''8. do''16 |
    do''4 do'' |
    do'' s |
    fa''4. mi''8 |
    fa''4 }
  { s2\p\cresc |
    s4\mf s\f |
    s4 r |
    s2\p | }
>> r4 |
R2*4 |
\twoVoices #'(primo secondo tutti) <<
  { fa''2~ | fa'' | fa''4 }
  { fa'2~ | fa' | fa'4 }
  { s2\p\cresc | s\f | }
>> r4 r2 |
R2 |
r8 <>\p \twoVoices #'(primo secondo tutti) <<
  { mi''8 mi'' mi'' | }
  { do'' do'' do'' | }
>>
r8 \twoVoices #'(primo secondo tutti) <<
  { fa''8 fa'' fa'' | fa''2 | la'4. sol'8 | fa'4\fermata }
  { do''8 do'' do'' | re''2 | fa'4. mi'8 | fa'4\fermata }
  { s4. | s2\fp | }
>>
