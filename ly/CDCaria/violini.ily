\clef "treble"
<<
  \tag #'primo {
    r16 do'''(\fp la'' fa'') r la''(\fp fa'' do'') |
    r do''\p do'' do'' r fa'' fa'' fa'' |
    r mi''! mi'' mi'' r fa'' fa'' fa'' |
    r sol'' sol'' sol'' r do'' do'' do'' |
    r16 do'' do'' do'' r do'' do'' do'' |
    << si'2:32 { s4\p s\f } >> |
    do''4:32 do''8 r |
    r re''-.(\p re''-.) r |
    r re''-.( re''-.) do''16( la') |
    \grace la'8 sol'4 r8 do'' |
    do''4\rf( si'8) re'' |
    re''4(\mf do''8) mi'' |
    \grace mi''8 re''2 |
    sol''\f~ |
    sol''4 fa''16\p re'' fa'' re'' |
    r mi'' mi'' mi'' mi'' mi'' re'' re'' |
  }
  \tag #'secondo {
    r16 do''(-\sug\fp la' fa') r la'(-\sug\fp fa' do') |
    r fa'\p fa' fa' r do'' do'' do'' |
    r sib' sib' sib' r la' la' la' |
    r sol' do'' do'' r sol' sol' sol' |
    r la' la' la' r la' la' la' |
    << sol'2:32 { s4\p s\f } >> |
    sol'4: sol'8 r |
    r8 la'-.(-\sug\p la'-.) r |
    r8 la'-.( la'-. la'-.) |
    \grace la'8 sol'4 r8 mi' |
    mi'4-\sug\rf( re'8) fa' |
    fa'4(-\sug\mf mi'8) do' |
    \grace do'8 si2 |
    sol'\f |
    la'4 la'8-\sug\p la' |
    sol'16 do'' do'' do'' do'' do'' si' si' |
  }
>>
do''4.( re''8) |
la'8( si' do'') r |
<<
  \tag #'primo {
    do''4(\mf si'8) re'' |
    re''4(\mf do''8) mi''8 |
    \grace mi''4 re''2 |
    sol''4\fp~ sol''16 fa'' mi'' dod'' |
    re'' re'' fa'' fa'' la''32[ sold'' la'' sold''] la''[ sol'' fa'' mi''] |
    re''16\f fa'' fa'' fa'' fa''4:16 |
    mi''4: mi''16 mi'' re'' re'' |
    do''4 r |
    r16 re''\p re'' re'' r re'' re'' re'' |
    r do'' do'' do'' r do'' do'' do'' |
    r16 sib' sib' sib' r sib' sib' sib' |
    r la' la' la' do''4 |
    si'2~ |
    si' |
    r16 do''\p do'' do'' r do'' do'' do'' |
    lab''4-\sug\fp\fermata si' |
    do''\fermata r |
    r16 do'''(\mf la'' fa'') r la''(\mf fa'' do'') |
    r16 do''\p do'' do'' r fa'' fa'' fa'' |
    r mi'' mi'' mi'' r fa'' fa'' fa'' |
    <sib' sol''>2\f |
    la'4
  }
  \tag #'secondo {
    mi'4(-\sug\mf re'8) fa' |
    fa'4(-\sug\mf mi'8) do' |
    \grace do'4 si2 |
    sol'4-\sug\fp~ sol'16 sol' sol' sol' |
    la' la' re'' re'' fa''32[ mi'' fa'' mi''] fa''[ mi'' re'' dod''] |
    re''16-\sug\f re'' re'' re'' re''4:16 |
    do''4: do''16 do'' si' si' |
    do''4 r16 do''\p do'' do'' |
    r do'' do'' do'' r sib'! sib' sib' |
    r sib' sib' sib' r la' la' la' |
    r la' la' la' r sol' sol' sol' |
    r sol' fa' fa' r8 sol' |
    \once\tieDashed fa'2~ |
    fa' |
    r16 mi'-\sug\p mi' mi' r mi' mi' mi' |
    fa'2-\sug\fp\fermata |
    mi'4\fermata r |
    r16 do''(\mf la' fa') r la'(\mf fa' do') |
    r16 la'-\sug\p la' la' r do'' do'' do'' |
    r sib' sib' sib' r la' la' la' |
    << do''2 \\ sol'-\sug\f >> |
    <fa' do''>4
  }
>> <>_\markup\italic dolce do''16( re'' mi'' fa'') |
fa''( mi'' sol'' fa'' mi'' re'' do'' sib') |
\grace sol'16 la'8 la' do''16( re'' mi'' fa'') |
fa''( mi'' sol'' fa'' mi'' re'' do'' sib') |
\grace sib'8 la'4\fermata r |
<<
  \tag #'primo {
    << do''2:16 { s4\p s\f } >> |
    re''2:32 |
    re'':\fp |
    mi''4 r8 la'' |
    la''(\mf sol'') r sol'' |
    sol''(\mf fa'') r fa'' |
    mi''8.( fa''16 sol''8. fa''16) |
    \grace fa''8 mi''4 r8 do'' |
    sib'8.\p\cresc sib'16 mi''8. mi''16 |
    <fa'' la'>4:16\mf <do'' sol''>:\f |
    la''8\fp( fa'' re'' sib') |
    la'16 la' la' la' la' la' sol' sol' |
  }
  \tag #'secondo {
    << << la'2:16 \\ fa': >> { s4\p s\f } >> |
    << sib'2:32 \\ fa':-\sug\p >> |
    << si': \\ sol':-\sug\fp >> |
    <do'' mi'>4 r8 do'' |
    do''8(-\sug\mf sib'!) r8 sib' |
    sib'(-\sug\mf la') r la' |
    sol'8.( la'16 sib'8. la'16) |
    \grace la'8 sol'4 r8 la' |
    sol'8.\p -\sug\cresc sol'16 sol'8. do''16 |
    do''4:16\mf do'':\f |
    \once\slurDashed do''8(\fp do'' fa' sol') |
    fa'16 fa' fa' fa' fa' fa' mi' mi' |
  }
>>
fa'4.( sol'8) |
re'8( mi' fa') r |
<<
  \tag #'primo {
    r8 la' la' la' |
    r sib' sib' sib' |
    r sib' sib' sib' |
    r16 do''\cresc do'' do'' do'' do'' do'' do'' |
    r do''\f do'' do'' do'' do'' do'' dod'' |
    re''4 r r2 |
  }
  \tag #'secondo {
    r8 fa' fa' fa' |
    r fa' fa' fa' |
    r fa' fa' fa' |
    r16 fa'-\sug\cresc fa' fa' fa' fa' fa' fa' |
    r fa'-\sug\f fa' fa' fa'4:16 |
    fa'4 r r2 |
  }
>>
R2 |
<<
  \tag #'primo {
    r16 mi''\p mi'' mi'' mi''4:16 |
    r16 fa'' fa'' fa'' fa''4: |
    <re' sib' fa''>2\fp |
    la'4.( sol'8) |
    fa'4\fermata
  }
  \tag #'secondo {
    r16 sol'\p sol' sol' sol'4:16 |
    r16 la' la' la' la'4: |
    <fa' re''>2\fp |
    fa'4.( mi'8) |
    fa'4\fermata
  }
>>
