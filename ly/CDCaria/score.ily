\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroupNoBar <<
      \new StaffGroupNoBracket <<
        \new GrandStaff \with { \violiniInstr } <<
          \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
          \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
        \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      >>
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Ri.
      } \withLyrics <<
        \global \includeNotes "voix"
      >> \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s2*4\pageBreak
          \grace s8 s2*4\pageBreak
          s2*4\pageBreak
          \grace s8 s2*4\pageBreak
          % 5
          s2*4\pageBreak
          \grace s4 s2*4\pageBreak
          s2*4\pageBreak
          s2*4\pageBreak
          \grace s8 s2*4\pageBreak
          % 10
          s2*4\pageBreak
          s2*4\pageBreak
          s2*4\pageBreak
          \grace s8 s2*4\pageBreak
          s2*4\pageBreak
          % 15
          s2*4\pageBreak
          s2*3 s1\pageBreak
          \grace s8 s2*4\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
