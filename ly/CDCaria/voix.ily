\clef "soprano/treble" R2 |
do''4 r8 do''16 fa'' |
\grace fa''8 mi'' mi'' r4 |
sol''8 mi''16 re'' \grace re'' do''8. sib'16 |
\grace sib'8 la'4 r8. do''16 |
si'8. sol''16 sol''8. si'16 |
\grace re''8 do''4 r8 mi'' |
dod''[ re''] r fa'' |
dod''8\melisma re''4\melismaEnd \ficta do''16[ la'] |
\grace la'8 sol'4 r8 sol'16[ do''] |
do''4( si'8) re'' |
re''4( do''8) mi'' |
\grace mi''8 re''2 |
sol''~ |
sol''4 fa''16[ re''] fa''[ re''] |
do''4.( re''8) |
do''4 r |
r r8 do'' |
do''4( si'8) re'' |
re''4( do''8) mi'' |
\grace mi''4 re''2 |
sol''4~ sol''16[ fa'' mi'' dod''] |
re''32[\melisma mi'' re'' mi''] fa''[ sol'' fa'' sol''] la''[ sold'' la'' sold''] la''[ sol'' fa'' mi''] |
re''16[ fa''8 la'' fa'']\melismaEnd re''16 |
do''4.( re''8) |
do''4 r |
r8. re''16 re''8. re''16 |
\grace re''8 do''4 r |
r8. sib'16 \grace do''8 sib'4 |
\grace sib'8 la' la' do'' do'' |
\grace do'' si'4 si'8 fa'' |
fa''4. si'8 |
\grace re''8 do''4 r8 do'' |
lab''4.\fermata si'8 |
\grace re'' do'' do'' r4 |
R2 |
do''4 r8 do''16 fa'' |
\grace fa''8 mi'' mi'' r4 |
sol''8 mi''16 re'' do''8. sib'16 |
\grace sib'8 la'4 r |
r r16 re'' do'' sib' |
\grace sol'16 la'8 la' r4 |
R2 |
\grace sib'8 la'4\fermata r8. fa'16 |
mib''8. re''32[ do''] \grace do''8 sib'8. la'16 |
\grace la'8 sib'4 r8. sol'16 |
fa''8. mi''!32[ re''] do''8 si' |
\grace si'8 do''4 r8 do'' |
\grace do''8 sib'!4 r8 sib' |
\grace sib'8 la'4 r8 la' |
sol'8. la'16 sib'8. la'16 |
\grace la'8 sol'4 r8 do'' |
sib'8. sib'16 mi''8. mi''16 |
fa''4 sol'' |
la''16[( mi'') fa''( dod'')] re''[( la') sib'( sol')] |
fa'4.( sol'8) |
fa'4 r |
R2 |
la'4~ la'16[ sib'32 do''] sib'16 la' |
\grace la'8 sib'4 sib' |
sib'4~ sib'16[ do''32 re''] do''16 sib' |
\grace si'8 do''4 r |
do''4~ do''16[ re''32 mib''] re''16 dod'' |
\grace dod''8 re''4
\ffclef "soprano/treble" <>^\markup\character Armida
r8 fa'' sib'' fa'' fa'' fa'' |
\ficta \grace mib''8 re''4 r |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
mi''!16 mi'' mi''8 r4 |
fa''16 fa'' fa''8 r4 |
fa''4~ fa''16[ re'' sib' sol'] |
fa'4.( sol'8) |
fa'4\fermata
