\clef "bass" fa8 r fa r |
fa-\sug\p r la r |
sol r fa r |
mi r mi r |
fa r fa r |
re8.\f re32 mi fa16-! re-! fa-! re-! |
do-! mi-! sol-! mi-! do4 |
R2*2 |
sol2\p~ |
sol~ |
sol~ |
sol4 sol, |
r16 do32\f re mi16 fa sol-! mi-! re-! do-! |
fa2:16\fp |
sol4: sol,16 sol, sol sol |
lab4( sol8 fad) |
fa!4( mi8) r |
sol2\p~ |
sol~ |
sol4 sol, |
r16 do32\fp re mi16 mi mi mi mi mi |
fa2:16 |
fa:16\f |
sol4: sol,: |
do8 r la\p r |
sib r sol r |
mi r fa r |
re r mi r |
fa r r mi |
re2 |
reb4 reb' |
do'8 r do' r |
reb2\fp\fermata |
do4\fermata r4 |
fa8\mf r fa r |
fa\p r la r |
sol r fa r |
mi2\f |
fa4 r |
sol8_\markup\italic dolce r do r |
fa4 r |
sol8 r do r |
fa4\fermata r |
fa,8.\p do32 re mib16\f-! do-! mib-! do-! |
sib,8.\p sib,32 do re16-! sib,-! re-! sib,-! |
sol,8.\fp re32 mi fa16-!\f re-! fa-! re-! |
do16\p do' do' do' do'4:16 |
do'2: |
do': |
do': |
do': |
do'4:\p\cresc sib: |
la:\mf mi:\f |
fa8\fp la sib sib, |
do16 do' do' do' do'4:16 |
reb'4( do'8 si) |
sib!4( la8) r |
r8 mib' mib' mib' |
r re' re' re' |
r re' re' re' |
r la\cresc la la |
r la\f la la |
sib4 r r2 |
R2 |
sib8\p sib sib sib |
la la la la |
sib,2\fp |
do2 |
fa4\fermata
