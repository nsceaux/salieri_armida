\clef "bass" R2*5 |
re8.-\sug\f re32 mi fa16-! re-! fa-! re-! |
do4 r |
R2*2 |
sol'4.\p mi'8 |
mi'4\mf( re'8) fa' |
fa'4(\mf mi'8) do' |
\grace do'8 si2 |
r16 do32\f re mi16 fa sol-! mi-! re-! do-! |
fa4 r |
r r8 sol |
lab4( sol8 fad) |
fa!4( mi8) r |
mi'4(\mf re'8) fa' |
fa'4(\mf mi'8) do' |
\grace do'4 si2 |
r16 do32-\sug\fp re mi16 mi mi mi mi mi |
fa4 r |
fa2:16-\sug\f |
sol4: sol,: |
do8 r r4 |
R2*7 |
reb2-\sug\fp\fermata |
do4\fermata r |
R2*3 |
do'2-\sug\f~ |
do'4 r |
R2*4 |
fa,8.-\sug\p do32 re mib16-!-\sug\f do-! mib-! do-! |
sib,8.\p sib,32 do re16-! sib,-! re-! sib,-! |
sol,8.-\sug\fp re32 mi fa16-!-\sug\f re-! fa-! re-! |
do4 r |
R2*4 |
do'16\p\cresc do' do' do' sib sib sib sib |
la4:16-\sug\mf mi:-\sug\f |
fa8 r r4 |
do16\p do' do' do' do'4: |
reb'4 do'8 si |
sib!4( la8) r |
R2*4 |
r8 la-\sug\f la la |
sib4 r r2 |
R2 |
r8 sol'-\sug\p sol' sol' |
r fa' fa' fa' |
sib,2-\sug\fp |
do2 |
fa4\fermata
