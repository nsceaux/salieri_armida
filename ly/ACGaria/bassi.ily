\clef "bass" r4 |
re2\fermata r |
r r4 r8 la, |
re4 do sib,4. la,8 |
sol,2 sib,! |
la, r |
fa fad |
sol sold |
sol!4 r8. sol16 fa8. fa16 sol8. sol16 |
la4 la, re r |
