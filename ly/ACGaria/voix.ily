\clef "tenor/G_8" r8 la |
re'2\fermata re4 r8 la |
re'4 la fa4. mi8 |
re4 re r2 |
sib4 sib8. sib16 la4. sold8 |
la4 la r r8 la |
re'4. re'8 re'4. re'8 |
sib!4. sib8 mi'4. re'8 |
\grace re'8 dod'4 r r r8. sib!16 |
la2 re4 r |
