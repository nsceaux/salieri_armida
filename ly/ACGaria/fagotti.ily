\clef "bass" r4 |
re2\fermata r |
r r4 r8 la, |
re4 do sib,4. la,8 |
sol,2 sib,! |
la, r |
R1*4 |
