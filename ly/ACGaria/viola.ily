\clef "alto" r4 |
re'2\fermata r2 |
r r4 r8 la |
re'4 do' sib4. la8 |
sol2 sib! |
la r |
fa' fad' |
sol' sold' |
la'4 r8. sol'!16 fa'8. fa'16 sol'8. sol'16 |
la'4 la re' r |
