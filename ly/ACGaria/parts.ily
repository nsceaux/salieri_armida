\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (fagotti #:score-template "score-voix" #:indent 0
    #:music , #{

\quoteBassi "ACGbassi"
s4 s1*5
\cue "ACGbassi" { <>^\markup\tiny Bassi \mmRestInvisible s1*4 \mmRestVisible }

                #})
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { Tornate al nero Abisso, }
  \livretVerse#10 { Onde l'orror v'alberga, }
  \livretVerse#10 { E d'una sola verga }
  \livretVerse#10 { All'agitar sparite. }
}

       #}))
