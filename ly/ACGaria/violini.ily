\clef "treble" r4 |
<<
  \tag #'primo <re'' fa'!>2\fermata
  \tag #'secondo <la' re'>2\fermata
>> r4 r8 la' |
re''4 la' fa'4. mi'8 |
re'4 re'2 r4 |
<<
  \tag #'primo {
    sib'2 la'4. sold'8 |
    la'2 r |
    <re'' re'>2 <re' do''> |
    <re' sib'>4. sib'8 mi''!4. re''8 |
    dod''4
  }
  \tag #'secondo {
    re'2 re' |
    dod' r |
    <re' la'>2 la' |
    sib' si' |
    mi'!4
  }
>> r8 la'16 \ficta si'32 dod'' re''8 mi'32 fa' sol' la' \ficta sib'8[ r16 sib'] |
la'4 la re' r |
