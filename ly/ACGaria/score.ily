\score {
  \new StaffGroupNoBar <<
    \new Staff \with { \fagottiInstr } <<
      \global \keepWithTag #'tutti \includeNotes "fagotti"
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\smallCaps Ubaldo
      shortInstrumentName = \markup\smallCaps Ub.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout { s4 s1*3\pageBreak }
    >>
  >>
  \layout { }
  \midi { }
}
