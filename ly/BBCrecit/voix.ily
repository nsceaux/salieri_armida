\clef "soprano/treble" mi''8 mi'' r si' do''4 r8 la' |
do'' do'' r do'' do'' do'' do'' re'' |
sib' sib' r sib'16 sib' sib'4 la'8 sib' |
sol'8 sol' r16 sol' fa' sol' mi'8 mi'16 \ficta sib'16 sib' sib' do'' re'' |
do''8 do'' r16 do'' do'' do'' do''8 sol' r sol' |
sib' sib' sib' la' fa' fa' r4 |
la'4. re''8 re''8 do'' sib' la' |
sib'8 sib' sib'16*4/5 sib' sib' sib' do'' lab'8 lab' lab' lab'16 sib' |
sol'8 sol' r \ficta sib'16 sib' mib''4 do''8 sib' |
la'!4 r16 la' la' sib' sib'8 fa' r4 |
R1 |
