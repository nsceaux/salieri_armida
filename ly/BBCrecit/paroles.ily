For -- se, chi sa? Ver -- ran -- no
con un leg -- gia -- dro in -- gan -- no
in sem -- bian -- za d’Ar -- mi -- da i lie -- ti so -- gni
a lu -- sin -- gar mia sor -- te
in que -- sta dol -- ce im -- ma -- gi -- ne di mor -- te.
Oh in -- gan -- no for -- tu -- na -- to,
che le più ca -- re i -- de -- e fin -- ga al pen -- sie -- ro,
e da un fin -- to pia -- cer lo chia -- mi al ve -- ro!
