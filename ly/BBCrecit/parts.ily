\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { Forse, chi sà? Verranno }
  \livretVerse#12 { Con' un leggiadro inganno }
  \livretVerse#12 { In sembianza d'Armida i lieti sogni }
  \livretVerse#12 { A lusingar mia sorte }
  \livretVerse#12 { In questo dolce immagine di morte. }
  \livretVerse#12 { Oh inganno fortunato, }
  \livretVerse#12 { Che le più care idèe finga il pensiero, }
  \livretVerse#12 { E da un finto piacer lo chiami al vero! }
}

       #}))
