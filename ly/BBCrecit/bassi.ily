\clef "bass" sold4-\sug\f r la2-\sug\p~ |
la1 |
sol~ |
sol |
mi~ |
mi2 fa4( mi) |
re1~ |
re |
mib~ |
mib2 r4 fa |
sib,2 r |
