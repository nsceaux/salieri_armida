\clef "alto" sold'4-\sug\f r \once\tieDashed la'2-\sug\p~ |
la'1 |
sib'~ |
sib' |
\ficta sib'~ |
sib'2 la'4( sol') |
fa'1~ |
fa' |
mib' |
do'4 r r fa' |
sib2 r |
