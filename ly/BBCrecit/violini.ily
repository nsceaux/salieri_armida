\clef "treble"
<<
  \tag #'primo {
    sold'\f r \once\tieDashed do''2~\p |
    do''1 |
    do''~ |
    do'' |
    do''~ |
    do''2 \ru#2 { r16 la''-.( la''-. la''-.) } |
    la''1 |
    sib''1 |
    \ficta sib'' |
    la''!4 r r <do'' fa'> |
    <fa' re''>2 r |
  }
  \tag #'secondo {
    << si'4 \\ mi'-\sug\f >> r << do''2 \\ { mi'-\sug\p~ | mi'1 | } >>
    mi'1~ |
    mi' |
    sol'~ |
    sol'2 la'16-.( do''-. do''-. do''-.) r dod''-.( dod''-. dod''-.) |
    re''1~ |
    re'' |
    sol' |
    fa'4 r r la' |
    sib'2 r |
  }
>>
