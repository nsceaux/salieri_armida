\piecePartSpecs
#`((oboi #:score-template "score-deux-voix")
   (corni #:score-template "score-deux-voix" #:tag-global ())
   (tromboni #:score-template "score-tromboni-voix")

   (violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Armida
    \livretVerse#10 { Io con voi la nera face }
    \livretVerse#10 { A turbargli i rai del giorno, }
    \livretVerse#10 { Al crudel sempre d’intorno }
    \livretVerse#10 { Nuova Furia agiterò. }
    \livretVerse#10 { Io nel sen tutto d’Aletto }
    \livretVerse#10 { Verserògli il rio veleno; }
    \livretVerse#10 { A quel perfido dal petto }
  }
  \null
  \column {
    \livretVerse#10 { L’empio cuore io strapperò. }
    \livretVerse#10 { E agli ingrati eterno esempio }
    \livretVerse#10 { Nel suo scempio io lascerò. }
    \livretVerse#10 { E agli ingrati eterno esempio }
    \livretVerse#10 { Nel suo scempio io lascerò. }
    \livretPers Coro
    \livretVerse#10 { E agli ingrati eterno esempio }
    \livretVerse#10 { Nel suo scempio io lascerò. }
  }
  \null
}
#}))
