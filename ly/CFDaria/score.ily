\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniBInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff << \global \keepWithTag#'primo-secondo \includeNotes "tromboni" >>
        \new Staff << \global \keepWithTag#'terzo \includeNotes "tromboni" >>
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
        >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiriFirst
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix4 \includeNotes "voix"
      >> \keepWithTag #'voix4 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with {
        \haraKiri
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Ar.
      } \withLyrics <<
        { \noHaraKiri s1*130 \revertNoHaraKiri }
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s1*3\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          %% 5
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 10
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 15
          s1*6\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 20
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 25
          s1*7\pageBreak
          s1*7\pageBreak
          s1*6\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 30
          s1*6\pageBreak
          s1*7\pageBreak
          s1*5 s2 \bar "" \pageBreak
          s2
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
