\clef "treble"
\twoVoices #'(primo secondo tutti) <<
  { re''4 s re'' s |
    fa'' s  fa'' sol''8 la'' | }
  { fa'4 s fa' s |
    do'' s do''8 fa'' sol'' la'' | }
  { s4 r s r | s r s2 | }
>>
\tag#'tutti <>^"a 2." sib''8 re'' mib'' fa'' sol'' do'' re'' mib'' |
fa'' sib' do'' re'' mib'' la' sib' do'' |
re'' sol' la' sib' do'' fa' sol' la' |
sib' do'' re'' mib'' fa'' sol'' la'' sib'' |
la'' fa'' mi'' re'' do'' sib' la' sol' |
fa'4 \twoVoices #'(primo secondo tutti) <<
  { fa'4 fa' fa' | fa'2 }
  { do'4 do' do' | do'2 }
>> r2 |
R1*2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { fa'1 | sol'2 la' | sib' }
  { re'1 | mib'2 fa' | re' }
>> r2 |
R1*2 |
r2 r8 \tag#'tutti <>^"a 2." re''-!-\sug\ff mib''-! fa''-! |
sol''-! do''-! re''-! mib''-! fa''-! fa'-! sol'-! la'-! |
sib'4 r r2 |
R1*6 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''2. }
  { mi' }
>> \tag#'tutti <>^"a 2." do''4\ff |
re''8 mi'' fa'' sol'' la'' sib'' do''' re''' |
do'''32 sib'' la''8. la''4 r2 |
R1*2 |
r2 r4 do''-\sug\ff |
re''8 mi'' fa'' sol'' la'' sib'' do''' re''' |
do'''32 sib'' la''8. la''4 r2 |
R1*3 | \allowPageTurn
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { la''4 }
  { do'' }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''4 }
  { mi'' }
>> r4 r2 |
R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { la''4 }
  { do'' }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''4 }
  { mi'' }
>> r4 r2 |
R1*3 |
\tag#'tutti <>^"a 2." sib'4\ff r do'' r |
re'' r r2 |
R1*5 |
sib'4\ff r do'' r |
re'' r r2 |
R1*5 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { la''1 | la'' | sol'' | sol'' | }
  { fa''1 | fa'' | fa'' | mi'' | }
>>
\tag#'tutti <>^"a 2." fa''8 la' do'' do'' do'' la' fa'' fa'' |
la'' sol'' fa'' mi'' re'' do'' sib' la' |
sib' sol' mi'' mi'' mi'' do'' sol'' sol'' |
sib'' la'' sol'' fa'' mi'' re'' do'' sib' |
la'2 \twoVoices #'(primo secondo tutti) <<
  { fa''2 | fa'' mi'' | fa''4 }
  { la'2 | sol' sol' | la'4 }
>> r4 r2 |
R1*6 |
\tag#'tutti <>^"a 2." mib''2. mib''4 |
sol''2 mib''4 sib' |
mib''2. mib''4 |
sol''2 mib'' |
R1 |
fa''1\p~ |
fa''~ |
fa''~ |
fa''~ |
fa''2 fa' |
sib'2.\f do''4 |
re''2. mib''4 |
fa''2 sol''4 la'' |
sib''\p r r2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { re'''4 }
  { fa'' }
>> r4 r2 |
R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mib''4 }
  { la' }
>> r r2 |
R1 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { re'''4 }
  { re'' }
>> r4 r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { mib''4 }
  { la' }
>> r r2 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { re''4 }
  { sib' }
>> r r2 | \allowPageTurn
R1*2 |
\tag#'tutti <>^"a 2." sol'4\ff r fa' r8 sol'32 fa' mib' re' |
do'4 r r2 |
R1*4 |
\tag#'tutti <>^"a 2." sol'4\ff r fa' r8 sol'32 fa' mib' re' |
do'4 r r2 |
R1*5 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''1 | sib'' | sib'' | la'' | sib''2 }
  { re''1 | re'' | do'' | do'' | sib'2 }
>> r2 |
R1*12 |
r2 <>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { re''4. re''8 | re''2 sib' | }
  { fa'4. fa'8 | fa'2 re' | }
>>
r2 \twoVoices #'(primo secondo tutti) <<
  { fa''4. fa''8 | fa''2 re'' | }
  { re''4. re''8 | re''2 sib' | }
>>
r4 r8 \tag#'tutti <>^"a 2." fa''8 sol''4. la''8 |
\tieDashed sib''1~ |
sib''~ | \tieSolid
sib''2 sib' |
\twoVoices #'(primo secondo tutti) <<
  { mib''2 re'' | sol'' fa'' | }
  { sib'2 sib' | sib' sib' | }
>>
r4 \tag#'tutti <>^"a 2." re''4 mib'' fa'' |
\twoVoices #'(primo secondo tutti) <<
  { sib''1 | sib'' | sib'' | la'' |
    sib'' | re''' | do''' | do''' | }
  { re''1 | re'' | do'' | do'' |
    re'' | sib'' | sib'' | la'' | }
>>
\tag#'tutti <>^"a 2." sib''8 sib' do'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
la'2 \twoVoices #'(primo secondo tutti) <<
  { fa''2 | fa'' fa'' | }
  { la'2 | la' la' | }
>>
\tag#'tutti <>^"a 2." sib''8 sib' do'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
la'2 \twoVoices #'(primo secondo tutti) <<
  { fa''2 | fa'' fa'' | sib' }
  { la'2 | la' la' | sib' }
>> r2 |

