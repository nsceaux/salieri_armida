\tag #'(armida basse) {
  Io con voi la ne -- ra fa -- ce
  a tur -- bar -- gli i rai del gior -- no,
  a tur -- bar -- gli i rai del gior -- no,
  al cru -- del sem -- pre d’in -- tor -- no
  sem -- pre d’in -- tor -- no
  nuo -- va fu -- ria a -- gi -- te -- rò.
  Io nel sen tut -- to d’A -- let -- to
  ver -- se -- ro -- gli il rio ve -- le -- no;
  a quel per -- fi -- do dal pet -- to
  l’em -- pio co -- re io strap -- pe -- rò
  l’em -- pio co -- re io strap -- pe -- rò
  l’em -- pio co -- re io strap -- pe -- rò
  io strap -- pe -- rò
  io strap -- pe -- rò.
  
  Io con vo -- i la ne -- ra fa -- ce
  la ne -- ra fa -- ce
  a tur -- bar -- gli i rai del gior -- no,
  al cru -- del sem -- pre d’in -- tor -- no
  sem -- pre d’in -- tor -- no
  nuo -- va fu -- ria a -- gi -- te -- rò.
  A quel per -- fi -- do dal pet -- to
  l’em -- pio co -- re io strug -- ge -- rò
  l’em -- pio co -- re io strug -- ge -- rò
  l’em -- pio co -- re io strap -- pe -- rò
  io strap -- pe -- rò
  io strap -- pe -- rò.
  
  E a -- gli in -- gra -- ti e -- ter -- no e -- sem -- pio
  e -- ter -- no e -- sem -- pio
  nel suo scem -- pio io la -- scie -- rò.
}
\tag #'(voix1 voix2 voix3 voix4) {
  E a -- gli in -- gra -- ti e -- ter -- no e -- sem -- pio
  e -- ter -- no e -- sem -- pio
  nel suo scem -- pio io la -- scie -- rò
  io la -- scie -- rò
  io la -- scie -- rò.
}
