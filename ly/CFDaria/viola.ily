\clef "alto" <fa re'>4 r q r |
<fa do'>4 r la8 fa sol la |
sib re' mib' fa' sol' do' re' mib' |
fa' sib do' re' mib' la sib do' |
re' sol la sib do' fa sol la |
sib do' re' mib' fa' sol' la' sib' |
la' fa' mi' re' do' sib la sol |
fa4 <fa do'> q q |
q2 r |
sib\p re' |
fa'4. fa8 sol4 la |
sib2:8 sib: |
sib: sib: |
sib: sib: |
mib': mib': |
fa': fa: |
sib4 sib r8 re'-!\ff mib'-! fa'-! |
sol'-! do'-! re'-! mib'-! fa'-! fa-! sol-! la-! |
<>-\sug\p \ru#2 { sib8-. re'-. fa'-. re'-. } |
la do' fa' do' la do' fa' do' |
do' mi' sol' mi' do' mi' sol' mi' |
do' fa' la' fa' do' fa' la' fa' |
do' mi' sol' mi' do' mi' sol' mi' |
do' fa' la' fa' do' \ficta mi' sol' sol' |
la'2:8 fa': |
mi'\f r4 la\ff |
sib8 do' re' mi' fa' sol' la' sib' |
la'4 fa' sib'8-.\p la'-. sib'-. sol'-. |
la' do'' la' fa' la' do'' la' fa' |
sol' do'' sol' mi' sol' do'' sol' mi' |
fa' la' fa' do' la4 la-\sug\ff |
sib8 do' re' mi' fa' sol' la' sib' |
la'4 fa' sib'8-.\p la'-. sib'-. sol'-. |
la' do'' la' fa' la' do'' la' fa' |
sol' do'' sol' mi' sol' do'' sol' mi' |
fa'4-\sug\f r fa' r |
<fa' la>4\ff r r2 |
mi'4\p r do'-\sug\f r |
<do sol mi'? do''>4\ff r r2 |
fa'4\p r fa' r |
<la fa'>4-\sug\ff r r2 |
mi'4-\sug\p r do'-\sug\f r |
<do sol mi'? do''>4-\sug\ff r r2 |
fa8-\sug\p fa'-. mi'-. re'-. do'-.-\sug\cresc fa'-. mi'-. re'-. |
do' re' do' sib la sib la sol |
fa2 r4 r8 fa16-\sug\ff sol32 la |
sib4 r8 fa32 sol la sib do'4 r8 fa64*8/5 sol la sib do' |
re'4 r la8-\sug\p la la la |
sib2:8 do': |
re' r | \allowPageTurn
fa8-\sug\f fa'-. mi'-. re'-. do'-.-\sug\p -\sug\cresc fa'-. mi'-. re'-. |
do' re' do' sib la sib la sol |
fa2 r4 r8 fa16-\sug\ff sol32 la |
sib4 r8 fa32 sol la sib do'4 r8 fa64*8/5 sol la sib do' |
re'4 r la2:8-\sug\p |
sib: do': |
fa'4 fa' fa' fa' |
re' re' re' re' |
sib sib sib sib |
do' do' do' do' |
fa'2:8-\sug\ff fa': |
re': re': |
sib: sib: |
do': do: |
fa4 r <la fa' do''> r |
q r r2 |
<do sol mi' do''>4 r q r |
q r r2 |
fa2:8 re': |
sib: do': |
fa4 r r2 |
R1 |
sol8-!-\sug\fp sol( lab sol) fa-!-\sug\fp fa( sol fa) |
mib!4 do-.-\sug\p mib-. do-. |
sol8-!-\sug\fp sol( lab sol) fa-!-\sug\fp fa( sol fa) |
mib!4 do-.-\sug\p mib-. do-. |
sol2 r |
mib8-.\f mib-. fa-. sol-. lab-. lab-. sol-. fa-. |
mib mib fa sol \ficta lab lab sol fa |
mib mib fa sol \ficta lab lab sol fa |
mib mib fa sol \ficta lab sol fa mib |
fa4 r r2 |
fa4-\sug\p r r2 |
fa4 r r2 |
fa4 r r2 |
fa4 r r2 |
fa4 r fa' r |
sib8-.\f la-. sib-. do'-. sib-. re'-. do'-. mib'-. |
re' do' re' mib' re' fa' mib' sol' |
fa' mi' fa' la' sol' sib' la' do'' |
sib'4-\sug\p r sib-\sug\f r |
<sib fa' re''>4-\sug\ff r r2 |
fa4-\sug\p r fa-\sug\f r |
<do fa do'>4-\sug\ff r r2 |
sib4\p r sib-\sug\f r |
<sib fa' re''>4\ff r r2 |
fa4\p r fa-\sug\f r |
<do fa do'>4\ff r r2 | \allowPageTurn
sib8\p sib'-. la'-. sol'-. fa'-\sug\cresc sol' fa' mib' |
re' mib' re' do' sib do' re' sib |
mib'4 r r r8 do'32\ff re' mib' fa' |
sol'4 r8 sib32 do' re' mib' fa'4 r8 sol'32 fa' mib' re' |
do'4 r mib-\sug\f r |
fa r fa r |
sib8\fp sib'-. la'-. sol'-. fa'-.-\sug\cresc sol'-. fa'-. mib'-. |
re' mib' re' do' sib do' sib la |
mib'2 r4 r8 do'32(-\sug\ff re' mib' fa') |
sol'4 r8 sib32 do' re' mib' fa'4 r8 sol'32 fa' mib' re' |
do'4 r mib-\sug\f r |
fa4 r fa r |
sib\p sib sib sib |
sol sol sol sol |
mib mib mib mib |
fa fa fa fa |
sib2:8\ff sib: |
sol': sol': |
mib': mib': |
fa': fa': |
sib2 r |
r sib4. sib8 |
sib2 sib |
r sib4. sib8 |
sib2 sib |
R1 |
sib8-.-\sug\mf -\sug\cresc sib-. do'-. re'-. mib'-. mib'-. re'-. do'-. |
sib sib do' re' mib' mib' re' do' |
sib2 sib' |
sol'-!-\sug\f fa'-! |
mib'-! re'-! |
r4 re mib fa |
sib8\ff sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r <re sib>4. q8 |
q sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r q4. q8 |
q sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r8 fa sol4. la8 |
sib sib do' re' mib' mib' re' do' |
sib sib do' re' mib' mib' re' do' |
sib2 sib' |
sol' fa' |
mib' re' |
r4 re' mib' fa' |
\tuplet 6/4 sib2.:8 sib2.*2/3: |
sol': sol': |
sol': sol': fa': fa': |
fa': fa': |
sol': sol': |
sol': sol': |
fa': fa': |
sib8 sib do' re' mib' fa' sol' la' |
sib' la' sol' fa' mib' re' do' sib |
la4 r fa'2 |
fa fa' |
sib8 sib do' re' mib' fa' sol' la' |
sib' la' sol' fa' mib' re' do' sib |
la4 r fa'2 |
fa fa' |
sib r |
