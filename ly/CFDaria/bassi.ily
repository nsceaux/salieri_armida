\clef "bass" sib,4 r sib r |
la r la8 fa sol la |
sib re mib fa sol do re mib |
fa sib, do re mib la, sib, do |
re sol, la, sib, do fa, sol, la, |
sib, do re mib fa sol la sib |
la fa mi re do sib, la, sol, |
fa,4 fa, fa, fa, |
fa,2 r |
sib,\p re |
fa4. fa,8 sol,4 la, |
sib,2:8 sib,: |
sib,: sib,: |
sib,: sib,: |
mib: mib: |
fa: fa,: |
sib,4 sib, r8 re-!\ff mib-! fa-! |
sol-! do-! re-! mib-! fa-! fa,-! sol,-! la,-! |
sib,4 r sib\p r |
la r la r |
mi r mi r |
fa r fa, r |
mi r mi r |
fa2:8 mi: |
re: re: |
do\f r4 la,\ff |
sib,8-. do-. re-. \ficta mi-. fa-. sol-. la-. sib-. |
la4 fa r2 |
fa4\p r fa r |
do r do r |
fa r r la,\ff |
sib,8-. do-. re-. \ficta mi-. fa-. sol-. la-. sib-. |
la4 fa r2 |
fa4\p r fa r |
do r do r |
fa\f r fa r |
fa\ff r r2 |
do4\p r do\f r |
do\ff r r2 |
fa4\p r fa r |
fa\ff r r2 |
do4\p r do\f r |
do\ff r r2 |
fa,8\p fa-. mi-. re-. do-.-\sug\cresc fa-. mi-. re-. |
do re do sib, la, sib, la, sol, |
fa,2 r4 r8 fa16\ff sol32 la |
sib4 r8 fa32 sol la sib do'4 r8 fa64*8/5 sol la sib do' |
re'2 la,8\p la, la, la, |
sib,2:8 do: |
re r | \allowPageTurn
fa,8-.\f fa-. mi-. re-. do-.\p -\sug\cresc fa-. mi-. re-. |
do re do sib, la, sib, la, sol, |
fa,2 r4 r8 fa16\ff sol32 la |
sib4 r8 fa32 sol la sib do'4 r8 fa64*8/5 sol la sib do' |
re'4 r la,8\p la, la, la, |
sib,2:8 do: |
fa4 fa fa fa |
re re re re |
sib, sib, sib, sib, |
do do do do |
fa2:8\ff fa: |
re: re: |
sib,: sib,: |
do: do: |
fa4 r fa r |
fa r r2 |
do4 r do r |
do r r2 |
fa2:8 re': |
sib: do': |
fa4 r r2 |
R1 | \allowPageTurn
sol8-!\fp sol( lab sol) fa-!\fp fa( sol fa) |
mib!4 do-.\p mib-. do-. |
sol8-!\fp sol( lab sol) fa-!\fp fa( sol fa) |
mib!4 do-.\p mib-. do-. |
sol2 sol, |
mib8\f mib-. fa-. sol-. lab-. lab-. sol-. fa-. |
mib mib fa sol lab lab sol fa |
mib mib fa sol \ficta lab lab sol fa |
mib mib fa sol \ficta lab sol fa mib |
fa4 r r2 |
fa4-\sug\p r r2 |
fa4 r r2 |
fa4 r r2 |
fa4 r r2 |
fa4 r fa, r |
sib,8-.\f la,-. sib,-. do-. sib,-. re-. do-. mib-. |
re do re mib re fa mib sol |
fa mi fa la sol sib la do' |
sib4\p r sib\f r |
sib\ff r r2 |
fa4\p r fa\f r |
fa\ff r r2 |
sib4\p r sib\f r |
sib\ff r r2 |
fa4\p r fa\f r |
fa\ff r r2 |
sib,8-.\p sib-. la-. sol-. fa-.-\sug\cresc sol-. fa-. mib-. |
re mib re do sib, do re sib, |
mib4 r r2 |
mib4\ff r re r |
mib r mib-\sug\f r |
fa r fa, r |
sib,8-.\fp sib-. la-. sol-. fa-.-\sug\cresc sol-. fa-. mib-. |
re mib re do sib, do re sib, |
mib2 r |
mib4\ff r re r |
mib r mib-\sug\f r |
fa r fa, r |
sib,4\p sib, sib, sib, |
sol sol sol sol |
mib mib mib mib |
fa fa fa fa |
sib8\ff sib sib sib sib2:8 |
sol: sol: |
mib: mib: |
fa: fa: |
sib,2 r\fermata |
r sib4. sib8 |
sib2 sib, |
r sib4. sib8 |
sib2 sib, |
R1*4 |
sol2-!\f fa-! |
mib-! re-! |
r4 re mib fa |
sib,2\ff sib, |
sib,4 r
%%
sib4. sib8 |
sib2 sib, |
sib,4 r sib4. sib8 |
sib2 sib, |
sib,4. fa8 sol4. la8 |
sib8 sib, do re mib mib re do |
sib, sib, do re mib mib re do |
sib,2 sib |
sol fa |
mib re |
r4 re mib fa |
\tuplet 6/4 sib2.:8 sib2.*2/3: |
sol2.*2/3: sol: |
mib: mib: |
fa: fa: |
sib: sib: |
sol: sol: |
mib: mib: |
fa: fa: |
sib,8 sib, do re mib fa sol la |
sib la sol fa mib re do sib, |
la,4 r fa2 |
fa, fa |
sib,8 sib, do re mib fa sol la |
sib la sol fa mib re do sib, |
la,4 r fa2 |
fa, fa |
sib, r |
