<<
  \tag #'(armida basse) {
    \clef "soprano/treble" R1*9 |
    sib'2 re'' |
    fa''4. fa'8 sol'4 la' |
    sib'4 sib' r sib'8 re'' |
    do''4. do''8 do''4. mib''8 |
    re''4 re'' r fa''8 re'' |
    sol''2 mib'' |
    do''2. fa''4 |
    re''4 sib' r2 |
    R1 |
    sib'2. sib'4 |
    fa''2 r |
    sol''4( mi'') do'' sib' |
    la' do'' r2 |
    sol''4\melisma mi''\melismaEnd do'' sib' |
    la' do'' do''4. do''8 |
    fa''4 re'' la' si' |
    do''2 r |
    R1 |
    r2 do''4 do'' |
    fa''1 |
    sol''4( mi'') do'' sib' |
    la' do'' r2 |
    R1 |
    r2 do''4 do'' |
    fa''2. fa''4 |
    sol''( \ficta mi'') do''\melisma sib'\melismaEnd |
    la' do'' r2 |
    r do''4 re'' |
    sib'8. sib'16 sib'4 r2 |
    r r4 mi'' |
    fa'' do'' r2 |
    r2 do''4 re'' |
    sib' sib' r2 |
    r4 sib' sib' do'' |
    la' r fa''2~ |
    fa'' fa'' |
    la'' fa''4 r |
    R1 |
    r2 fa'' |
    sib' do'' |
    re'' r |
    r fa''~ |
    fa'' fa'' |
    la'' fa''4 r |
    R1 |
    r2 fa'' |
    sib' do'' |
    la''1 |
    fa'' |
    sib' |
    do'' |
    la'' |
    fa'' |
    sib' |
    do'' |
    fa'2 r |
    R1*6 |
    fa''2 re'' |
    si'4 si' r2 |
    r4 do'' mib'' do'' |
    sol''2 sol' |
    r4 do'' mib'' do'' |
    sol''2 sol' |
    mib''2. mib''4 |
    sol''2 mib''4\melisma sib'\melismaEnd |
    mib''2. mib''4 |
    sol''2 mib'' |
    mib''4( re'') do''( sib') |
    la'( sib') do''( re'') |
    mib''( re'') do''( sib') |
    la'( sib') do''( re'') |
    mib''( re'') do'' sib' |
    fa''2 fa' |
    sib'2. do''4 |
    re''2. mib''4 |
    fa''2 sol''4( la'') |
    sib''2 r |
    r fa''4 sol'' |
    mib''8. mib''16 mib''4 r2 |
    r r4 fa'' |
    re''4 re'' r2 |
    r fa''4 sol'' |
    mib'' mib'' r2 |
    r4 mib'' mib'' fa'' |
    re'' r lab''2~ |
    lab''\melisma sol''4\melismaEnd fad'' |
    sol''4.( mib''8) do''4 r |
    R1 |
    r2 mib'' |
    re'' do'' |
    re''2 lab''~ |
    lab''\melisma sol''4\melismaEnd fad'' |
    sol''4.( mib''8) do''4 r |
    R1 |
    r2 mib'' |
    re''2 do'' |
    sib'1 |
    sol'' |
    mib'' |
    fa'' |
    re'' |
    sol'' |
    mib'' |
    fa'' |
    sib'2 r |
    r re''4. re''8 |
    re''2 sib' |
    r4 r8 fa'' fa''4. fa''8 |
    fa''2 re'' |
    r4 r8 fa'' sol''4. la''8 |
    sib''1~ |
    sib''~ |
    sib''2 sib' |
    mib'' re'' |
    sol'' fa'' |
    r4 re'' mib'' fa'' |
    sib'2 r |
  }
  \tag #'voix1 { \clef "soprano/treble" R1*131 }
  \tag #'voix2 { \clef "alto/treble" R1*131 }
  \tag #'voix3 { \clef "tenor/G_8" R1*131 }
  \tag #'voix4 { \clef "bass" R1*131 }
>>
%%
<<
  \tag #'(armida basse) R1*28
  \tag #'voix1 {
    r2 re''4. re''8 |
    re''2 sib' |
    r4 r8 fa'' fa''4. fa''8 |
    fa''2 re'' |
    r4 r8 fa' sol'4. la'8 |
    sib'1~ |
    \once\tieDashed sib'~ |
    sib'2 sib' |
    mib'' re'' |
    sol'' fa'' |
    r4 re'' mib'' fa'' |
    sib'1 |
    re''1 |
    do'' |
    do'' |
    re'' |
    re'' |
    do'' |
    do'' |
    sib'2 r |
    R1*8 |
  }
  \tag #'voix2 {
    r2 fa'4. fa'8 |
    fa'2 fa' |
    r4 r8 fa' fa'4. fa'8 |
    fa'2 fa' |
    r4 r8 fa' sol'4. la'8 |
    sib'1~ |
    \once\tieDashed sib'~ |
    sib'2 sib |
    sib' sib' |
    sib' sib' |
    r4 re' mib' fa' |
    fa'1 |
    sol'1 |
    sol' |
    fa' |
    fa' |
    sol' |
    sol' |
    fa' |
    fa'2 r |
    R1*8 |
  }
  \tag #'voix3 {
    r2 sib4. sib8 |
    sib2 re' |
    r4 r8 re' re'4. re'8 |
    re'2 fa' |
    r4 r8 fa8 sol4. la8 |
    sib1~ |
    \once\tieDashed sib~ |
    sib2 sib |
    mib' re' |
    sol' fa' |
    r4 re' mib' fa' |
    sib1 |
    sib |
    sib |
    la |
    sib |
    sib |
    sib |
    la |
    sib2 r |
    R1*8 |
  }
  \tag #'voix4 {
    r2 sib4. sib8 |
    sib2 sib, |
    r4 r8 sib sib4. sib8 |
    sib2 sib, |
    r4 r8 fa sol4. la8 |
    sib1~ |
    \once\tieDashed sib~ |
    sib2 sib, |
    sol fa |
    mib re |
    r4 re mib fa |
    sib1 |
    sol |
    mib |
    fa |
    sib |
    sol |
    mib |
    fa |
    sib2 r |
    R1*8 |
  }
>>
