<<
  \tag #'(primo primo-secondo) \clef "alto"
  \tag #'secondo \clef "tenor"
  \tag #'terzo \clef "bass"
>>
<<
  \twoVoices #'(primo secondo primo-secondo) <<
    { fa'4 s fa' s |
      fa' s fa' s |
      sib' }
    { re'4 s re' s |
      do' s do' s |
      sib }
    { s4 r s r |
      s r s r | }
  >>
  \tag #'terzo {
    sib4 r sib r |
    la r la8 fa sol la |
    sib4
  }
>> r4 r2 |
R1*4 |
r4 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { do'4 do' do' | do'2 }
    { la4 la la | la2 }
  >>
  \tag #'terzo { fa,4 fa, fa, | fa,2 }
>> r2 |
R1*2 |
r2 <>\p <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sib'2 | do''1 | re''4 re'' }
    { re'2 | mib' fa' | fa'4 fa' }
  >>
  \tag #'terzo {
    sib,2~ |
    sib,1 |
    sib,4 sib,
  }
>> r2 |
R1*11 |
<>\f <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sol'2. }
    { mi'2. }
  >>
  \tag #'terzo { do2. }
>> r4 |
R1*38 | \allowPageTurn
<>-\sug\ff <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { la'4 s la' s |
      la' s2. |
      sol'4 s sol' s |
      sol' s2. |
      la'2 la' |
      sol'2 sol' |
      la'4 }
    { do'4 s do' s |
      do' s2. |
      mi'4 s mi' s |
      mi' s2. |
      do'2 fa'~ |
      fa' mi' |
      fa'4 }
    { s4 r s r |
      s r r2 |
      s4 r s r |
      s r r2 | }
  >>
  \tag #'terzo {
    fa4 r fa r |
    fa r r2 |
    do4 r do r |
    do r r2 |
    fa2 re' |
    sib2 do' |
    fa4
  }
>> r4 r2 | \allowPageTurn
R1*60 |
r2 <>-\sug\ff <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { fa'4. fa'8 |
      fa'2 fa' |
      s4. fa'8 fa'4. fa'8 |
      fa'2 fa' |
      s4. fa'8 sol'4. la'8 |
      sib'1~ |
      \once\tieDashed sib'~ |
      sib'2 sib |
      sib' sib' |
      sib' sib' |
      s4 re' mib' fa' |
      fa'1 |
      sol' |
      sol' |
      fa' |
      fa' |
      sol' |
      sol' |
      fa' |
      fa'2 }
    { sib4. sib8 |
      sib2 re' |
      s4. re'8 re'4. re'8 |
      re'2 fa' |
      s4. fa8 sol4. la8 |
      sib1~ |
      \once\tieDashed sib~ |
      sib2 sib |
      mib' re' |
      sol' fa' |
      s4 re' mib' fa' |
      sib1 |
      sib |
      sib |
      la |
      sib |
      sib |
      sib |
      la |
      sib2 }
    { s2 |
      s1 |
      r4 r8 s s2 |
      s1 |
      r4 r8 s s2 |
      s1*5 |
      r4 s2. | }
  >>
  \tag #'terzo {
    sib4. sib8 |
    sib2 sib, |
    r4 r8 sib sib4. sib8 |
    sib2 sib, |
    r4 r8 fa sol4. la8 |
    sib1~ |
    \once\tieDashed sib~ |
    sib2 sib, |
    sol fa |
    mib re |
    r4 re mib fa |
    sib1 |
    sol |
    mib |
    fa |
    sib |
    sol |
    mib |
    fa |
    sib2
  }
>> r2 |
R1 |
r2 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { fa'2 | fa' fa' | fa' }
    { do'2 | do' do' | sib }
  >>
  \tag #'terzo { la2 | la la | sib }
>> r2 |
R1 |
r2 <<
  \twoVoices #'(primo secondo primo-secondo) <<
    { fa'2 | fa' fa' | re' }
    { do'2 | do' do' | sib }
  >>
  \tag #'terzo { la2 | la la | sib }
>> r2 |
