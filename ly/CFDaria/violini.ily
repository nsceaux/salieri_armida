\clef "treble" re''32 do'' sib'8. sib'8 sib' re''32 do'' sib'8. sib'8 sib' |
la''32 sol'' fa''8. fa''8 fa'' la''32 sol'' fa''8. sol''8 la'' |
sib''-. re''-. mib''-. fa''-. sol''-. do''-. re''-. mib''-. |
fa''-. sib'-. do''-. re''-. mib''-. la'-. sib'-. do''-. |
re''-. sol'-. la'-. sib'-. do''-. fa'-. sol'-. la'-. |
sib'-. do''-. re''-. mib''-. fa''-. sol''-. la''-. sib''-. |
la''-. fa''-. mi''-. re''-. do''-. sib'-. la'-. sol'-. |
fa'4 <fa' la> q q |
q2 r |
sib'\p re'' |
fa''4. fa'8 sol'4 la' |
<<
  \tag #'primo {
    <re' sib'>2:16 q: |
    <mib' do''>2: <fa' do''>: |
    <fa' re''>: q8 re'' fa'' re'' |
    sol''2:16 mib'': |
    do'': do'': |
    re''4-! sib'-!
  }
  \tag #'secondo {
    sib'16 fa' fa' fa' fa'2.:16 |
    sol'2: la': |
    sib': sib'8 sib' re'' sib' |
    sib'2: sol': |
    la': la': |
    sib'4-! re'-!
  }
>> r8 re''-!\ff mib''-! fa''-! |
sol''-! do''-! re''-! mib''-! fa''-! fa'-! sol'-! la'-! |
<<
  \tag #'primo {
    re''32\fp do'' sib'8. sib'8 sib' re''32\fp do'' sib'8. sib'8 sib' |
    la''32\fp sol'' fa''8. fa''8 fa'' la''32\fp sol'' fa''8. fa''8 fa'' |
    sib''32\fp la'' sol''8. sol''8 sol'' sib''32\fp la'' sol''8. sol''8 sol'' |
    la''32\fp sol'' fa''8. fa''8 fa'' la''32\fp sol'' fa''8. fa''8 fa'' |
    sib''32\fp la'' sol''8. sol''8 sol'' sib''32\fp la'' sol''8. sol''8 sol'' |
    la''32\fp sol'' fa''8. fa''8 fa'' sol'' mi'' do'' do'' |
    fa'' fa'' re'' re'' la' la' si' si' |
    <do'' mi' sol>2\f r4 do''\ff |
    re''8 mi'' fa'' sol'' la'' sib'' do''' re''' |
    do'''32 sib'' la''8. la''4 do''\p do'' |
    fa''2 la''32\fp sol'' fa''8. fa''8 fa'' |
    sib''32\fp la'' sol''8. sol''8 sol'' mi''32\fp re'' do''8. sib'8 do'' |
    la'32\fp sib' do''8. do''8 do'' do''4 do''\ff |
    re''8 mi'' fa'' sol'' la'' sib'' do''' re''' |
    do'''32 sib'' la''8. la''4 do''\p do'' |
    fa''2 la''32\fp sol'' fa''8. fa''8 fa'' |
    sib''32\fp la'' sol''8. sol''8 sol'' mi''32\fp re'' do''8. sib'8 do'' |
    do''(-\sug\f la') fa''-. fa''-. fa''( do'') la''-. la''-. |
    la''\ff sol'' fa'' mi'' re'' do'' sib' la' |
    sib'(\p sol') mi''-. mi''-. mi''(-\sug\f do'') sol''-. sol''-. |
    sib''\ff la'' sol'' fa'' mi'' re'' do'' sib' |
    la'(\p do'') fa''-. fa''-. fa''( do'') la''-. la''-. |
    la''\ff sol'' fa'' mi'' re'' do'' sib' la' |
    sib'(\p sol') mi''-. mi''-. mi''(-\sug\f do'') sol''-. sol''-. |
    sib''\ff la'' sol'' fa'' mi'' re'' do'' sib' |
    la'4 r fa''2\p\cresc~ |
    fa''2 fa'' |
    la''-. fa''4-.
  }
  \tag #'secondo {
    <>-\sug\p \ru#2 { sib8-. re'-. fa'-. re'-. } |
    \ru#2 { la do' fa' do' } |
    do' mi' sol' mi' do' mi' sol' mi' |
    do' fa' la' fa' do' fa' la' fa' |
    do' mi' sol' mi' do' mi' sol' mi' |
    do' fa' la' fa' do' mi' sol' sol' |
    la'2:8 fa': |
    <mi' sol>2\f r4 la\ff |
    sib8 do' re' mi' fa' sol' la' sib' |
    la'32 sol' fa'8. fa'4 sib'8-.\p sol'-. la'-. fa'-. |
    la' do'' la' fa' la' do'' la' fa' |
    sol' do'' sol' mi' sol' do'' sol' mi' |
    fa' la' fa' do' la4 la-\sug\ff |
    sib8 do' re' mi' fa' sol' la' sib' |
    la'32 sol' fa'8. fa'4 sib'8-.-\sug\p la'-. sib'-. sol'-. |
    la' do'' la' fa' la' do'' la' fa' |
    sol' do'' sol' mi' sol' do'' sol' mi' |
    <fa' la>4-\sug\f r q r |
    fa'8-\sug\ff mi' re' do' sib la sol la |
    sol4-\sug\p r <sol mi'>-\sug\f r |
    sol'8-\sug\ff fa' mi' re' do' sib la sol |
    la4-\sug\p r <la fa'> r |
    fa'8-\sug\ff mi' re' do' sib la sol la |
    sol4-\sug\p r <sol mi'>-\sug\f r |
    sol'8-\sug\ff fa' mi' re' do' sib la sol |
    la\p fa'-. mi'-. re'-. do'-.-\sug\cresc la'-. sol'-. fa'-. |
    mi' fa' mi' re' do' re' do' sib |
    la2 r4
  }
>> r8 fa'16\ff sol'32 la' |
sib'4 r8 fa'32 sol' la' sib' do''4 r8 fa'64*8/5 sol' la' sib' do'' |
re''4 r fa''8\p fa'' fa'' fa'' |
sib'2:8 do'': |
re'' r | \allowPageTurn
<<
  \tag #'primo {
    fa'2\f fa''\p\cresc~ |
    fa'' fa'' |
    la'' fa''4
  }
  \tag #'secondo {
    fa'8-.-\sug\f fa'-. mi'-. re'-. do'-.-\sug\p -\sug\cresc la'-. sol'-. fa'-. |
    \ficta mi' fa' mi' re' do' re' do' sib |
    la2 r4
  }
>> r8 fa'16\ff sol'32 la' |
sib'4 r8 fa'32 sol' la' sib' do''4 r8 fa'64*8/5 sol' la' sib' do'' |
re''4 r fa''2\p |
sib' do'' |
<<
  \tag #'primo {
    la''8 la'' la'' la'' la''2:8 |
    la'': la'': |
    sol'': sol'': |
    sol'': sol'': |
    <la' la''>2:16\ff q: |
    q: q: |
    sol'': sol'': |
    sol'': sol'': |
    fa''8( la') do''-. do''-. do''( la') fa''-. fa''-. |
    la'' sol'' fa'' mi'' re'' do'' sib' la' |
    sib'( sol') mi''-. mi''-. mi'' do'' sol'' sol'' |
    sib'' la'' sol'' fa'' mi'' re'' do'' sib' |
    la'4~ la'16 sib'32 do'' re'' mi'' fa'' sol'' la''8-. fa''( mi'' fa'') |
  }
  \tag #'secondo {
    fa''8 fa'' fa'' fa'' fa''2:8 |
    fa'': fa'': |
    fa'': fa'': |
    mi'': mi'': |
    fa'':16-\sug\ff fa'': |
    fa'': fa'': |
    fa'': fa'': |
    mi'': mi'': |
    <la' fa''>4 r <do'' fa' la> r |
    fa'8 mi' re' do' sib la sol la |
    sol4 r <sol mi' do''> r |
    sol'8 fa' mi' re' do' sib la sol |
    la4~ la16 sib32 do' re' mi' fa' sol' la'8-. \once\slurDashed fa''( mi'' fa'') |
  }
>>
<re'' re'>8-. re''( dod'' re'') mi''-. mi''( re'' mi'') |
fa'' fa'-. sol'-. la'-. sib'-. do''-. re''-. mi''-. |
<<
  \tag #'primo {
    fa''2 re'' |
    si' r | \allowPageTurn
    r4 do''-.\p mib''!-. do''-. |
    <sol re' si' sol''>2\f sol'\p |
    r4 do''-.\p mib''-. do''-. |
    <sol re' si' sol''>2\f sol' |
    mib''2\f mib'' |
    mib'' mib'' |
    mib'' mib'' |
    mib'' mib'' |
    mib''16\fp mib'' mib'' mib'' re''4:16 do'': sib': |
    la':\fp sib': do'': re'': |
    mib'':\fp re'': do'': sib': |
    la':\fp sib': do'': re'': |
    mib'':\fp re'': do'': sib': |
    fa''8\f la'' fa'' do''
  }
  \tag #'secondo {
    fa''4 r r2 | \allowPageTurn
    sol'8-!\fp sol'( lab' sol') fa'-!\fp fa'( sol' fa') |
    mib'!4 do'-.-\sug\p mib'-. do'-. |
    sol'8-!\fp sol'( lab' sol') fa'-!\fp fa'( sol' fa') |
    mib'!4 do'-.-\sug\p mib'-. do'-. |
    sol'2-\sug\f sol |
    sol8-.\f sol-. \ficta lab-. sib-. do'-. do'-. sib-. lab |
    sol sol \ficta lab sib do' do' sib lab |
    sol sol \ficta lab sib do' do' sib lab |
    sol sol \ficta lab sib do' sib lab sol |
    la4:16-\sug\fp sib: do': re': |
    mib':-\sug\fp re': do': sib: |
    la:-\sug\fp sib: do': re': |
    mib':-\sug\fp re': do': sib: |
    la:-\sug\fp sib: do': re': |
    do'4 r <>-\sug\f
  }
>> la'8 do'' la' fa' |
sib'-. la'-. sib'-. do''-. sib'-. re''-. do''-. mib''-. |
re'' do'' re'' mib'' re'' fa'' mib'' sol'' |
fa'' mi'' fa'' la'' sol'' sib'' la'' fa'' |
<<
  \tag #'primo {
    re''8(\p sib') fa''-. fa''-. fa''(-\sug\f re'') sib''-. sib''-. |
    re'''\ff do''' sib'' la'' sol'' fa'' mib'' re'' |
    mib''(\p do'') la''-. la''-. la''(-\sug\f fa'') do'''-. do'''-. |
    mib'''\ff re''' do''' sib'' la'' sol'' fa'' mib'' |
    re''(\p sib') fa''-. fa''-. fa''(-\sug\f re'') sib''-. sib''-. |
    re'''8\ff do''' sib'' la'' sol'' fa'' mib'' re'' |
    mib''(\p do'') la''-. la''-. la''(-\sug\f fa'') do'''-. do'''-. |
    mib'''\ff re''' do''' sib'' la'' sol'' fa'' mib'' |
    re''4 r \once\tieDashed lab''2\p\cresc~ |
    lab''( sol''4 fad'') |
    sol''4.( mib''8) do''4
  }
  \tag #'secondo {
    sib''4-\sug\p r <re'' fa' sib>-\sug\f r |
    sib'8-\sug\ff la' sol' fa' mib' re' do' sib |
    la4-\sug\p r <la fa'>-\sug\f r |
    do''8-\sug\ff sib' la' sol' fa' mib' re' do' |
    sib4-\sug\p r <sib fa' re''>4-\sug\f r |
    sib'8-\sug\ff la' sol' fa' mib' re' do' sib |
    la4-\sug\p r <la fa'>-\sug\f r |
    do''8-\sug\ff sib' la' sol' fa' mib' re' do' |
    sib8-.\p sib'-. la'-. sol'-. fa'-\sug\cresc sol' fa' mib' |
    fa'8 sol' fa' mib' re' mib' fa' re' |
    do'2.
  }
>> r8 do'32\ff re' mib' fa' |
sol'4 r8 sib32 do' re' mib' fa'4 r8 sol'32 fa' mib' re' |
do'4 r <<
  \tag #'primo {
    mib''4\f r |
    re'' r do'' r |
    sib'4 r lab''2\p\cresc~ |
    lab''( sol''4 fad'') |
    sol''4.( mib''8) do''4
  }
  \tag #'secondo {
    do''4-\sug\f r |
    sib' r la' r |
    sib'8-.\fp sib'-. la'-. sol'-. fa'-.-\sug\cresc sol'-. fa'-. mib'-. |
    fa' sol' fa' mib' re' mib' fa' re' |
    do'2.
  }
>> r8 do'32\ff re' mib' fa' |
sol'4 r8 sib32 do' re' mib' fa'4 r8 sol'32 fa' mib' re' |
do'4 r <<
  \tag #'primo {
    mib''4\f r |
    re'' r do'' r |
    re''2:8\p re'': |
    re'': re'': |
    do'': do'': |
    do'': do'': |
  }
  \tag #'secondo {
    do''4-\sug\f r |
    sib' r la' r |
    sib'2:8-\sug\p sib': |
    sib': sib': |
    sib': sib': |
    la': la': |
  }
>>
re''16\ff <re'' sib''> q q q2.:16 |
q2: q: |
<do'' sib''>: q: |
<do'' la''>: q: |
<sib' sib''>2 r | \allowPageTurn
r <<
  \tag #'primo {
    re''4. re''8 |
    re''2 sib' |
    r fa''4. fa''8 |
    fa''2 re'' |
    r4 r8 fa''\p sol''4. la''8 |
    sib''1-\sug\cresc~ |
    sib''~ |
    sib''2 sib' |
    mib''-!\f re''-! |
    sol''-! fa''-! |
    r4 re'' mib'' fa'' |
    sib'8\ff
  }
  \tag #'secondo {
    fa'4. fa'8 |
    fa'2 re' |
    r re''4. re''8 |
    re''2 fa' |
    R1 |
    re''8-.\mf\cresc re''-. mib''-. fa''-. sol''-. sol''-. fa''-. mib''-. |
    re'' re'' mib'' fa'' sol'' sol'' fa'' mib'' |
    re''2 sib' |
    sib'-!\f sib'-! |
    sib'-! sib'-! |
    r4 re' mib' fa' |
    sib8-\sug\ff
  }
>> sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r <sib fa' re''>4. q8 |
q sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r <re' sib' fa''>4. q8 |
q sib16 la sib la sib la sib la sib la sib la sib la |
sib4 r8 <<
  \tag #'primo {
    fa''8 sol''4. la''8 |
    <sib'' sib'>2:8 q: |
    q: q: |
    sib'' sib' |
    mib'' re'' |
    sol'' fa'' |
    r4 re'' mib'' fa'' |
  }
  \tag #'secondo {
    fa'8 sol'4. la'8 |
    sib' re' mib' fa' sol' sol' fa' mib' |
    re' re' mib' fa' sol' sol' fa' mib' |
    re'2 sib |
    sib' sib' |
    sib' sib' |
    r4 re' mib' fa' |
  }
>>
sib'8*2/3 <re'' sib''> q q q q \tuplet 6/4 q2.:8 |
q2.*2/3: q: |
<do'' sib''>: q: |
<do'' la''>: q: |
<re'' sib''>: q: |
q: q: |
<do'' sib''>: q: |
<do'' la''>: q: |
sib'8 sib' do'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
la'2 <fa' la' fa''> |
q q |
sib'8 sib' do'' re'' mib'' fa'' sol'' la'' |
sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
la'2 <fa' la' fa''> |
q q |
<sib' re'>2 r |
