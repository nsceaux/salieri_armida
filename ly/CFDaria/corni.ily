\clef "treble" \transposition sib
\twoVoices #'(primo secondo tutti) <<
  { do''4 s do'' s |
    re'' s re'' s |
    mi'' }
  { mi'4 s mi' s |
    sol' s sol' s |
    do'' }
  { s4 r s r | s r s r | }
>> r4 r2 |
R1*4 |
r4 \twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' sol' | sol'2 }
  { sol4 sol sol | sol2 }
>> r2 |
R1*2 |
\tag #'tutti <>^"a 2." do'1\p~ |
do'~ |
do'2 r |
R1*11 |
re''2.-\sug\f r4 |
R1*4 |
r2 r4 <>\ff \twoVoices #'(primo secondo tutti) <<
  { re''4 | mi'' }
  { sol'4 | do'' }
>> r4 r2 |
R1*4 |
\tag #'tutti <>^"a 2." sol'4-\sug\ff r r2 |
R1 |
re''4-\sug\ff r r2 |
R1 |
sol'4-\sug\ff r r2 |
R1 |
re''4-\sug\ff r r2 |
R1*3 | \allowPageTurn
do''4-\sug\ff r re'' r |
mi'' r r2 |
R1*5 |
do''4-\sug\ff r re'' r |
mi'' r r2 |
R1*5 | \allowPageTurn
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { re''1 |
    mi'' |
    mi'' |
    re'' |
    re''4 s re'' s |
    re'' }
  { sol'1 |
    sol' |
    sol' |
    re'' |
    sol'4 s sol' s |
    sol' }
  { s1*4 | s4 r s r | }
>> r4 r2 |
\tag #'tutti <>^"a 2." re''4 r re'' r |
re'' r r2 |
sol'2 sol' |
sol'2 re'' |
\twoVoices #'(primo secondo tutti) <<
  { re''4 }
  { sol' }
>> r r2 |
R1*11 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'2 sol | }
  { sol1~ |
    sol~ |
    sol~ |
    sol~ |
    sol2 sol | }
>>
R1*4 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { mi' }
>> r r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { re''4 }
  { sol' }
>> r r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { mi' }
>> r r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { re''4 }
  { sol' }
>> r r2 |
\twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { mi' }
>> r r2 | \allowPageTurn
R1*15 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''1 | mi'' | re'' | re'' | do''2 }
  { do''1 | do'' | do'' | sol' | mi'2 }
>> r2 |
R1*12 |
r2 <>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''4. mi''8 | mi''2 do'' | }
  { do''4. do''8 | do''2 mi' | }
>>
r2 \twoVoices #'(primo secondo tutti) <<
  { mi''4. mi''8 | mi''2 do'' | }
  { do''4. do''8 | do''2 mi' | }
>>
R1 |
\tag #'tutti <>^"a 2." do''1~ |
\tieDashed do''~ |
do''2 do' |
do'' do'' |
do'' si' |
r4 mi'' fa'' sol'' |
\twoVoices #'(primo secondo tutti) <<
  { mi''1 |
    mi'' |
    re'' |
    re'' |
    mi'' |
    mi'' |
    re'' |
    re'' |
    do''2 }
  { do''1 |
    do'' |
    do'' |
    sol' |
    do'' |
    do'' |
    do'' |
    sol' |
    do''2 }
>> r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''2 | re'' re'' | do'' }
  { sol'2 | sol' sol' | do'' }
>> r2 |
R1 |
r2 \twoVoices #'(primo secondo tutti) <<
  { re''2 | re'' re'' | do'' }
  { sol'2 | sol' sol' | mi' }
>> r2 |
