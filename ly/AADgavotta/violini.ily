\clef "treble"
<<
  \tag #'primo {
    \grace re''8 do''(\p sib' la' sib') |
    do''4-! fa''( sol'' la'') |
    \grace la''8 sol''4( fa'') do''( dod'') |
    re'' sol''8( la'' sib'' la'' sol'' fa'') |
    mi''16 fa'' sol''8 do''4 \grace re''8 do''8\f( sib' la' sib') |
    do''4-! fa''( sol'' la'') |
    \grace fad''8 sol''4 sol'' \grace sol''8 fa''4 fa'' |
    \grace fa''8 mi''4 mi'' \grace mi''8 re''4 do''8 re'' |
    do''2 \grace re''16 do''8(\p si' do'' re'') |
    do''4-! do'''(\f sib''! la'') |
    \grace la''8 sib''2 \grace re''16 do''8(\p si' do'' re'') |
    do''4-! sib''4(\f la'' sold'') |
    \grace sold''8 la''2 \grace sol''16 fa''8_\markup\italic dol. mi''( fa'' sol'') |
    fa''4 mib''( re'' dod'') |
    \grace dod''8 re''2 sol''16\f la'' sib''8 la''4 |
    sol''4 fa'' \grace la''8 sol''4 fa''8 mi'' |
    fa''2
    %%
    do''8(\mf reb'' do'' reb'') |
    do''( fa'') mi''( sol'') fa''( lab'') sol''( mi'') |
    fa''( sol'') fa''( sol'') lab'' sib'' lab'' sib'' |
    do''' si'' do''' si'' do''' lab'' sol'' fa'' |
    fa'' mi'' sol'' mi'' do'' reb'' do'' reb'' |
    mib''! fa'' mib'' fa'' mib'' do''' sib'' lab'' |
    lab'' sol'' sib'' lab'' sol'' fa'' mib'' reb'' |
    do'' fa'' mib'' reb'' do'' sib' lab' sol' |
    lab'2
    %%
    lab'8 sib' lab' sib' |
    do'' reb'' do'' reb'' mib'' lab'' sol'' fa'' |
    mi'' fa'' mi'' fa'' sol'' lab'' sol'' lab'' |
    sib'' lab'' sol'' fa'' mi'' reb''! do'' sib' |
    lab' sol' fa' mi' fa' sol' lab' sib' |
    do'' fa'' mi'' sol'' fa'' lab'' sol'' mi'' |
    fa'' mi'' fa'' do'' reb'' fa'' mib'' reb'' |
    do'' reb'' do'' sib' lab' sol' fa' mi' |
    fa'2
  }
  \tag #'secondo {
    \grace s8
    \once\slurDashed la'8-\sug\p( sol' fa' sol') |
    la'4-! do''( sib' la') |
    \once\slurDashed mi'( fa') fa'2 |
    fa'4 r re'' re'' |
    sol' mi' la'8-\sug\f sol' fa' sol' |
    la'4-! do'' do'' do'' |
    do''2 do''4( si') |
    \grace si'8 do''4 do''2 si'!4 |
    do''2 \grace fa'16 mi'8(-\sug\p re' mi' fa') |
    mi'4-! la'(-\sug\f sol' fad') |
    \grace fad'8 sol'2 \grace fa'16 \once\slurDashed mi'8(-\sug\p re' mi' fa') |
    mi'4-! sol'(-\sug\f fa' mi') |
    \grace mi'8 fa'2 \grace sib'16 la'8_\markup\italic [dol.] \once\slurDashed sol'( la' sib') |
    la'2 fa' |
    fa' do''4-\sug\f do'' |
    mi''4 fa'' \grace do''8 sib'4 la'8 sol' |
    la'2
    %%
    do'8(-\sug\mf reb' do' reb') |
    \slurDashed do'( fa') mi'( sol') fa'( lab') sol'( mi') |
    fa'( sol') fa'( sol') lab' sib' lab' sib' | \slurSolid
    do'' si' do'' si' do'' lab' sol' fa' |
    fa' mi' sol' mi' do' reb' do' reb' |
    mib'! fa' mib' fa' mib' do'' sib' lab' |
    lab' sol' sib' lab' sol' fa' mib' reb' |
    do' fa' mib' reb' do' sib lab sol |
    lab2
    %%
    lab8 sib lab sib |
    do' reb' do' reb' mib' lab' sol' fa' |
    mi' fa' mi' fa' sol' lab' sol' lab' |
    sib' lab' sol' fa' mi' reb'! do' sib |
    lab sol lab sol lab sol lab sib |
    do' fa' mi' sol' fa' lab' sol' mi' |
    fa' mi' fa' do' reb' fa' mib' reb' |
    do' reb' do' sib lab sol do' sol |
    lab2
  }
>>
<<
  \tag #'primo {

  }
  \tag #'secondo {

  }
>>
<<
  \tag #'primo {

  }
  \tag #'secondo {

  }
>>
<<
  \tag #'primo {

  }
  \tag #'secondo {

  }
>>
