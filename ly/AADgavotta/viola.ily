\clef "alto" \grace s8
r2 |
fa'4\p la'( sib' do'') |
do'' do'' la2 |
sib4 r sib si |
do' do r2 |
fa'4-!\f do''( sib' la') |
do''2 re'' |
mi''4 la la sol |
mi2
%%
r2 |
r4 la'(\f sol' fad') |
\grace fad'8 sol'2 r |
r4 sol'(-\sug\f fa'! mi') |
\grace mi'8 fa'2 r |
fa_\markup\italic [dol.] la |
sib mi'4-\sug\f fa' |
sib la sib do' |
fa2
%%
r2 |
lab4(\mf sib do' reb') |
do' r do' r |
do' r si r |
do' r r2 |
do'4 r do' r |
reb' r sib r |
lab lab reb' do'8 sib |
do'2
%%
r2 |
mib'4 r do' r |
sib r r2 |
sol4 r do' r |
do' r r2 |
\once\slurDashed lab4( sib do' reb') |
do' r fa' r |
fa' fa' fa'8 \ficta mi' fa' sol' |
fa'2
