\tag #'all \key fa \major
\midiTempo#160
\time 2/2 \partial 2
\grace s8 s2
s1*7 s2 \bar ":|.|:"
\grace s8 s2 s1*7 s2 \bar ":|.|:"
\key fa \minor s2 s1*7 s2 \bar ":|.|:"
s2 s1*7 s2 \bar ":|."
\endMark "Maggiore da Capo"
