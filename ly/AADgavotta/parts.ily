\piecePartSpecs
#`((flauti #:score-template "score-deux" #:notes "oboi")

   (oboi #:score-template "score-deux")

   (corni #:score-template "score-deux"
          #:tag-global ()
          #:instrument "Corni in F"
          #:system-count 3)

   (fagotti
    #:music , #{

\quoteBassi "AABbassi"
s2 s1 s2
\cue "AABbassi" {
  <>^\markup\tiny Bassi
  \restInvisible\mmRestInvisible s2 s1 s2 \restVisible s2 \mmRestVisible
}
s1*8
\cue "AABbassi" {
  <>^\markup\tiny Bassi
  \restInvisible\mmRestInvisible s1 s2 \restVisible \mmRestVisible
}

                #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{\markup\tacet #}))
