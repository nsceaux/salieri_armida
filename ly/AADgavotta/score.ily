\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new GrandStaff \with {
        instrumentName = \markup\center-column { Flauti Oboi }
        shortInstrumentName = \markup\center-column { Fl. Ob. }
      } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new Staff \with { \corniFInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
        { s2 s1*15 s2 \startHaraKiri }
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \fagottiInstr \haraKiriFirst } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
        { \startHaraKiri s2 s1*15 s2 \stopHaraKiri }
      >>
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion { s2 s1*15 s2\break }
        \origLayout {
          s2 s1*3\pageBreak
          s1*5\pageBreak
          s1*6\pageBreak
          \grace s8 s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
