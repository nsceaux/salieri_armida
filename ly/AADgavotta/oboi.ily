\clef "treble" \grace s8
r2 |
r4 <>\p \twoVoices #'(primo secondo tutti) <<
  { fa''4( sol'' la'') |
    \grace la''8 sol''4( fa'') }
  { \once\slurDashed do''4( sib' la') |
    do'' do'' }
>> r2 |
R1*2 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''4( sol'' la'') |
    \grace fad''8 sol''4 sol'' \grace sol''8 fa''4 fa'' |
    \grace fa''8 mi''4 mi'' \grace mi''8 re''4 do''8 re'' |
    do''2 }
  { do''2 do''4 |
    do''2 do''4( si') |
    \grace si'8 do''4 do''2 si'!4 |
    do''2 }
>>
%%
r2 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { do'''4( sib'' la'') |
    \grace la''8 sib''2 }
  { la''4( sol'' fad'') |
    \grace fad''8 sol''2 }
>> r2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sib''4( la'' sold'') |
    \grace sold''8 la''2 }
  { sol''4( fa'' mi'') |
    \grace mi''8 fa''2 }
>> r2 |
R1 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''16 la'' sib''8 la''4 |
    \grace la''8 sol''4 fa'' \grace la''8 sol''4 fa''8 mi'' |
    fa''2 }
  { do''4 do'' |
    mi''4 fa'' \grace do''8 sib'4 la'8 sol' |
    la'2 }
>>
%%
r2 | R1*7 | r2
r2 | R1*7 | r2
