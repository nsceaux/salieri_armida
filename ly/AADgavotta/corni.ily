\clef "treble" \transposition fa
\grace s8
r2 |
r4 <>\p \twoVoices #'(primo secondo tutti) <<
  { \slurDashed do''4( re'' mi'') |
    \grace mi''8 re''4( do'') \slurNeutral }
  { mi'4 sol' do'' |
    sol' mi' }
>> r2 |
R1*2 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { do''4 re'' mi'' | s re'' s do'' | }
  { mi''4 re'' do'' | s sol' s do'' | }
  { s2. | r4 s r s | }
>>
sol'4 sol'2 re''4 |
sol'2
%%
r2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol'2 sol'4 | sol'2 }
  { sol2 sol4 | sol2 }
>> r2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol'2 sol'4 | do''2 }
  { sol2 sol4 | do'2 }
>> r2 |
R1 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fa''4 mi'' |
    \grace mi''8 re''4 do'' \grace mi''8 re''4 do''8 re'' |
    do''2 }
  { re''4 do'' |
    sol'4 sol'8 do'' do''4 sol' |
    mi'2 }
>>
%%
r2 R1*7 r2
r2 R1*7 r2
