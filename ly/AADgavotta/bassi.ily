\clef "bass" \grace s8
r2 |
fa4-!\p la( sib do') |
sib la la,2 |
sib,4 r sib si |
do' do r2 |
fa4-!\f la( sol fa) |
mi2 re |
do4 la fa sol |
do2
%%
r |
do1\f~ |
do2 r |
do1\f |
fa2 r |
fa2_\markup\italic dol. la |
sib mi4\f fa |
sib4 la sib do' |
fa2
%%
r |
fa4-\sug\mf( sol lab sib) |
lab r fa r |
mib r reb r |
do r r2 |
lab4 r lab r |
sib r mib r |
lab fa reb mib |
lab,2
%%
r
lab4 r lab r |
sol r r2 |
do4 r mi r |
fa r r2 |
\once\slurDashed fa4( sol lab sib) |
lab r sib r |
lab sib do' do |
fa2
