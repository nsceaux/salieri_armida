\clef "bass" \grace s8
r2 |
r4 la(-\sug\p sib do') |
\grace do'8 \once\slurDashed sib4( la) r2 |
R1*2 |
r4 la-\sug\f sol fa |
mi mi' re'2 |
do'4 la fa sol |
do2
%%
r2 |
r4 do'2\f do'4 |
do2 r |
r4 do'2-\sug\f do4 |
fa2 r |
R1 |
r2 mi4-\sug\f fa |
sib la sib do' |
fa2
%%
r2 |
lab4(\mf sib do' reb') |
do' r do' r |
do' r si r |
do' r r2 |
do'4 r do' r |
reb' r sib r |
lab lab reb' do'8 sib |
lab2
%%
r2 |
mib'4 r do' r |
sib r r2 |
sol4 r do' r |
do' r r2 |
\once\slurDashed lab4( sib do' reb') |
do' r reb' r |
fa' fa' fa'8 \ficta mi' fa' sol' |
fa'2
