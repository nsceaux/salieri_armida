\clef "alto" fad'8_\markup\italic dolce fad fad' |
sold' sold sold' |
la' la dod' |
re'4. |
mi'4 mi8 |
la'4-\sug\f fad'8 |
fad'4 mi'8 |
mi'4 re'8 |
mi'4.~ |
mi'~ |
mi'~ |
mi' |
mi'8 mi mi' |
r re'-\sug\p re' |
dod'(-\sug\rf re') r |
dod'4-\sug\p dod'8 |
si-\sug\rf dod' r |
la16-\sug\rf( la' sold' fad' mi' re') |
dod'8 mi' mi' |
fad' mi' mi' |
mi'4 r8 |
la16( la' sold' fad' mi' re') |
dod'8 mi' mi' |
fad' mi' mi' |
mi'4 dod'8 |
si4 mi'8 |
mi' re' mi' |
la' la r |
re'16\p re' re' re' re' re' |
re'4.:16 |
sol16 sol la la la la |
re'4 r8 |
dod'16-. la'-. dod'-. la'-. re'-. la'-. |
mi'-. la'-. sol'-. la'-. fad'-. la'-. |
re'8 re' la |
re'4 r8 |
dod'16 la' dod' la' re' la' |
mi' la' sol' la' fad' la' |
re'8-\sug\f re' la |
re'4 re'8 |
si sol la |
re' re r |
R4.*7 |
mi'16 mi' mi' mi' mi' mi' |
mi'4.:16-\sug\f |
mi':16 |
mi':16 |
mi':16 |
dod'4.-\sug\p |
re' |
mi' |
fad'16\f fad' fad' fad \grace sold'16 fad'[ mi'32 re'] |
dod'8 r r |
dod r r |
re r r |
re r r |
mi16-\sug\f mi-\sug\p[ mi mi mi mi] |
mi mi mi4:16 |
mi8 r r |
mi16\f mi mi mi mi mi |
la8 la' dod' |
si4 la8 |
sold4 fad'8 |
mi'4 r8 |
re' mi' mi |
la4 r8 |
r8 la-\sug\p la' |
la'4 re'8 |
dod' dod' dod' |
re' re r |
dod'16-. la'-. dod'-. la'-. re'-. la'-. |
mi'-. la'-. sol'-. la'-. fad'-. la'-. |
re'8 re' la |
fad16. mi32 re8 r |
dod'16 la' dod' la' re' la' |
mi' la' sol' la' fad' la' |
re'8\f re' la |
la16.( sol32 fad8) re' |
si sol la |
re' re r |
re''4.~ |
re''~ |
re''~ |
re''8 sol16-. la-. si-. la-. |
sold8 mi''4~ |
mi''4.~ |
mi'' |
mi''4 la8 |
si si si |
la4 la8\f |
si si re' |
dod'4 r8 |
dod'4 re'8 |
mi'16.( fad'32 sol'8 fad') |
mi'16.( re'32 dod'8) re' |
mi'16.( fad'32 sol'8 fad') |
mi'16.( re'32 dod'8) re' |
mi'16.( fad'32 sol'8 fad') |
fad'4.\p |
sol' |
fad' |
mi' |
re' |
sol'8 la' la |
re'4. |
re''16-\sug\f re''-\sug\p re'' re'' re'' re'' |
re''4.:16-\sug\fp |
sol'8 r r |
R4.*2 |
la'8 r r |
la' r r |
la'4.:16-\sug\p |
la:16-\sug\ff |
re'8 re r |
sol'-\sug\p la' la |
re'4 r8 |
sol'\f fad'8. mi'16 |
re'8. la16 re' fad' |
fad'8. mi'16 re'8 |
re'16. sol'32 fad'8 mi' |
re'8[ r16 si dod' re'] |
sold8[ r16 sold la si] |
\custosNote mid8
