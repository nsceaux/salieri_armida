\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \flautiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr  } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniDInstr } <<
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiri
      shortInstrumentName = \markup\character Co.
    } <<
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 1 \super mo } }
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 2 \super do } }
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Alto }
      } \withLyrics <<
        { s4.*97 s4 \noHaraKiri s8 s4.*6 \revertNoHaraKiri }
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
    >>
    \new Staff \with {
      instrumentName = \markup\character Ismene
      shortInstrumentName = \markup\character Ism.
    } \withLyrics <<
      \global \keepWithTag #'ismene \includeNotes "voix"
    >> \keepWithTag #'ismene \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \modVersion {
        s4.*28\break
      }
      \origLayout {
        s4.*5\pageBreak
        s4.*6\pageBreak
        s4.*6\pageBreak
        s4.*6\pageBreak
        %% 5
        s4.*6\pageBreak
        \grace s8 s4.*6\pageBreak
        s4.*6\pageBreak
        \grace s8 s4.*7\pageBreak
        s4.*6\pageBreak
        %% 10
        s4.*6\pageBreak
        s4.*5\pageBreak
        s4.*5\pageBreak
        s4.*6\pageBreak
        s4.*5\pageBreak
        %% 15
        \grace s8 s4.*6\pageBreak
        s4.*5\pageBreak
        s4.*5\pageBreak
        s4.*5\pageBreak
        s4.*5\pageBreak
        %% 20
        s4.*5\pageBreak
        s4.*5\pageBreak
        s4.*5\pageBreak
        \grace s8 s4.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
