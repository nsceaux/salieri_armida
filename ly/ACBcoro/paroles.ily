\tag #'(voix1 voix2 voix3) {
  Vie -- ni al fon -- te del con -- ten -- to,
  for -- tu -- na -- to pas -- seg -- gier.
  E per -- du -- to o -- gni mo -- men -- to,
  che __ si per -- de pel pia -- cer,
  che __ si per -- de pel pia -- cer.
}
\tag #'ismene {
  Dell’ A -- mor la reg -- gia è que -- sta,
  la de -- li -- zia de’ __ mor -- ta -- li,
  la de -- li -- zia de’ __ mor -- ta -- li;
  nell’ o -- bli -- o di tut -- ti i ma -- li
  qui si vie -- ne a ri -- po -- sar __
  qui si vie -- ne a ri -- po -- sar
  qui si vie -- ne a ri -- po -- sar.
  Qui non v’è __ cu -- ra mo -- le -- sta,
  che il pia -- cer di to -- sco in -- fet -- ti,
  che il pia -- cer di to -- sco in -- fet -- ti,
  e il più dol -- ce de’ di -- let -- ti
  e il più dol -- ce de’ di -- let -- ti
  non ti co -- sta, che il bra -- mar,
  nò
  e il più dol -- ce de’ di -- let -- ti
  non ti co -- sta, che il bra -- mar,
  non ti co -- sta, che il __ bra -- mar,
  che il bra -- mar,
  che il bra -- mar.
}
\tag #'(voix1 voix2 voix3) {
  Vie -- ni al fon -- te del con -- ten -- to,
  for -- tu -- na -- to pas -- seg -- gier.
  Vie -- ni al fon -- te del con -- ten -- to,
  for -- tu -- na -- to pas -- seg -- gier.
}
