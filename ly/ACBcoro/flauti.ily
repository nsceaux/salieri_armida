\clef "treble" R4.*5 | \mergeDifferentlyDottedOn
\twoVoices #'(primo secondo tutti) <<
  { mi''4 fad''8 |
    \grace mi''8 re''4 mi''8 |
    \grace re''8 dod''4 re''8 |
    \grace dod''8 si'4 la'8 |
    si''16.( dod'''32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' |
    si''16.( dod'''32 re'''8) dod''' |
    \grace dod'''8 si''4 s8 |
    si''8.( re'''16 dod''' si'') |
    lad''8( si''8) s |
    la''!8.( dod'''16 si'' la'') |
    sold''!8 la'' s | }
  { dod''4 dod''8 |
    \grace dod''8 si'4 si'8 |
    \grace si'8 la'4 la'8 |
    \grace la'8 sold'4 la'8 |
    si''4 la''8 |
    sold''16.( la''32 si''8) la'' |
    sold''16.( la''32 si''8) la'' |
    \grace la''8 sold''4 s8 |
    fad''4 fad''8 |
    sol''( fad'') s |
    mi''4 mi''8 |
    mi'' mi'' s | }
  { s4.\f | s4.*6 | s4 r8 |
    s4.\p | s4\rf r8 |
    s4.\p | s4\rf r8 | }
>>
R4.*2 |
\twoVoices #'(primo secondo tutti) <<
  { re'''8 dod''' si'' | \grace si''8 dod'''4 }
  { si''8 la'' sold'' | \grace sold''8 la''4 }
>> r8 |
R4.*2 |
\twoVoices #'(primo secondo tutti) <<
  { re'''8 dod''' si'' | }
  { si'' la'' sold'' | }
>>
la''8 sold''16( fad'' mi'' red'') |
mi'' fad'' mi'' re''! dod'' si' |
dod'' mi'' fad'' la'' sold'' si'' |
la''8 la' r |
R4.*4 |
\twoVoices #'(primo secondo tutti) <<
  { \once\tieDashed la''4.~ |
    la'' |
    si''8 la'' sol'' |
    fad''16.[ sol''32 la''8] }
  { \once\tieDashed la'4.~ |
    la' |
    sol''8 fad'' mi'' |
    re''16.[ mi''32 fad''8] }
>> r8 |
\twoVoices #'(primo secondo tutti) <<
  { la''4.~ | la'' | si''8 la'' sol'' | fad''8. }
  { la'4.~ | la' | sol''8 fad'' mi'' | re''8. }
  { s4. | s | s\f }
>> re''16( fad'' la'') |
\grace la''8 sol'' fad''16 mi'' re'' dod'' |
re''4 r8 |
R4.*8 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { si''16.( dod'''32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' |
    si''16.( dod'''32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' | }
  { si''4 la''8 |
    \ru#3 { sold''16.( la''32 si''8) la'' | } }
>>
R4.*4 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { mi''8 s s |
    mi'' s s |
    fad'' s s |
    fad'' s s |
    dod'''4.~ |
    dod''' |
    si''8 s s |
    si''4. | }
  { la'8 s s |
    la' s s |
    la' s s |
    si' s s |
    la''4.~ |
    la'' |
    sold''8 s s |
    sold''4. | }
  { s8 r r |
    s r r |
    s r r |
    s r r |
    s4.-\sug\fp |
    s |
    s8 r r |
    s4.-\sug\f }
>>
la''16 sold'' la'' la' fad'' red'' |
red''16( mi'') mi''( la' re'' sid') |
\grace sid'8 dod''8 dod''16( fad' si' sold') |
la'8 la'32( si' dod'' re'' mi'' fad'' sold'' la'') |
fad''16. re''32 \grace dod''8 si'4\trill |
la'4 r8 |
R4.*4
\twoVoices #'(primo secondo tutti) <<
  { la''4.~ |
    la'' |
    si''8 la'' sol'' |
    fad''16. sol''32 la''8 s |
    la''4.~ |
    la'' |
    si''8 la'' sol'' |
    fad''8. }
  { la'4.~ |
    la' |
    sol''8 fad'' mi'' |
    re''16. mi''32 fad''8 s |
    la'4.~ |
    la' |
    sol''8 fad'' mi'' |
    re''8. }
  { s4.\p | s4.*2 |
    s4 r8 |
    s4.*2 |
    s4.-\sug\f | }
>> re''16 fad'' la'' |
\grace la'' sol''8 fad''16 mi'' re'' dod'' |
re''4 r8 |
R4.*2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { la''4. | si''4 }
  { re''4. | re''4 }
>> r8 |
R4.*2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { si''4. | dod'''4 }
  { mi''4. | mi''4 }
>> r8 |
R4.*3 |
r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { la''8 la'' |
    la''4.~ la''~ |
    la''~ |
    la''~ |
    la''~ |
    la'' | }
  { la'8 la' |
    la'4.~ |
    la'~ |
    la'~ |
    la'~ |
    la'~ |
    la' | }
>>
R4.*7 |
\twoVoices #'(primo secondo tutti) <<
  { re'''8 s s |
    re''' s s |
    si'' s s | }
  { la'' s s |
   la'' s s |
   re'' s s | }
  { s8 r r |
    s8 r r |
    s8 r r | }
>>
R4.*4 |
\twoVoices #'(primo secondo tutti) <<
  { re'''4. | dod''' | re'''8[ re'] }
  { mi''4. | mi'' | re''8[ re'] }
  { s4.-\sug\p | s\f | }
>> r8 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { si''8 la'' sol'' |
    \grace sol''8 fad''4 s8 |
    si''8 la'' sol'' |
    fad''8. }
  { sol''8 fad'' mi'' |
    \grace mi''8 re''4 s8 |
    sol''8 fad'' mi'' |
    re''8. }
  { s4. | s4 r8 | s4.\f }
>> \grace { mi''32[ re'' dod''] } re''16 fad'' la'' |
la''8. sol''16 fad''8 |
si'16. mi''32 re''8 dod'' |
re''4 r8 |
R4. |
