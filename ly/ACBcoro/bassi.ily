\clef "bass" fad8_\markup\italic dolce fad, fad |
sold sold, sold |
la la, dod |
re4. |
mi4 mi,8 |
r8 la\f lad |
si4 sold8 |
la!4 fad8 |
mi re dod |
sold4 la8 |
si16.( dod'32 re'8) dod' |
si16.( la32 sold8) la |
mi mi, mi |
r re\p re |
mi(\rf re) r |
r dod\p dod |
re\rf dod r |
re4.\f |
mi8( re dod) |
re mi mi, |
la,16 la la, dod mi la |
re4. |
mi8 re dod |
re mi mi, |
la,4 la8 |
sold4 mi8 |
la, re mi |
la la, r |
re16\p re re re re re |
re4.:16 |
sol16 sol la la la, la, |
re re la, fad, re,8 |
R4.*2 |
sol16 sol la la la, la, |
re re la, fad, re,8 |
R4.*2 |
sol16\f sol la la la, la, |
re4 re8 |
si\f sol la |
re re, r |
re\p re re |
dod4.:8 |
sold,:8 |
la,:8 |
dod:8 |
re8 re, r |
re8 re red |
mi4 r8 |
sold4\f la8 |
si16. dod'32 re'8 dod' |
si16. la32 sold8 la |
si16. dod'32 re'8 dod' |
dod16-\sug\p dod dod dod dod dod |
re4.:16 |
mi:16 |
fad16[\f fad fad fad,] \grace sold16 fad mi32 re |
dod16\f dod[\p dod dod dod dod] |
dod16\f dod[\p dod dod dod dod] |
re\f re[\p re re re re] |
re\f re[\p re re re re] |
mi\f mi[\p mi mi mi mi] |
mi\f mi[\p mi mi mi mi] |
mi4.:16 |
mi,:16\f |
la,8 la la |
sold r fad |
mi r re |
dod4 r8 |
re8 mi mi, |
la, la r |
r la(-\sug\p sol!) |
fad4 re8 |
dod dod dod |
re re, r |
R4.*2 |
sol8 la la, |
re re, r |
R4.*2 |
sol16-\sug\f sol la la la, la, |
re4 re8\f |
si sol la |
re re, r |
<>^"Violoncelli"_"solo" fad4 sol8 |
\once\slurDashed la16.( si32 do'8) si16 sol |
fad8 fad fad |
sol sol16-. la-. si-. la-. |
sold4 la8 |
\once\slurDashed si16.( dod'!32 re'8) dod'16 la |
sold8 sold sold |
la la, la |
si4. |
la4 <>^"Bassi" la8\f |
si si si |
la4 r8 |
\clef "tenor" <>2^"[Violoncelli]" dod'4 re'8 |
mi'16.( fad'32 sol'8) fad' |
mi'16.( re'32 dod'8) re' |
\slurDashed mi'16.( fad'32 sol'8) fad' |
mi'16.( re'32 dod'8) re' |
mi'16.( fad'32 sol'8) fad' | \slurNeutral
\clef "bass" <>^"Bassi" fad8-\sug\p r r |
sol r r |
fad r r |
mi r la, |
re r re |
sol la la, |
re4 r8 |
fad16\f fad[\p fad fad fad fad] |
fad16\f fad[\p fad fad fad fad] |
sol\f sol[\p sol sol sol sol] |
sol4.:16 |
sol:16 |
la16\fp la[ la la la la] |
la4.:16 |
la4.:16-\sug\p |
la,4.:16\f |
re8 re' r |
sol\p la la, |
 re4 r8 |
 sol\f la la, |
 re4.:16 |
 re:16 |
 sol16 sol la la la, la, |
 re8[ r16 si dod' re'] |
 sold8[ r16 sold la si] |
 \custosNote mid8
