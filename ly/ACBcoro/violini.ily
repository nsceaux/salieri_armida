\clef "treble" <>_\markup\italic dolce
<<
  \tag #'primo {
    fad'8.[ dod''16]-! \grace re'' dod''32( si' dod'' re'') |
    \grace dod''8 si'8. re''16-! \grace { mi''32[ re'' dod''] } re''16( mi'') |
    \grace re''8 dod''8. mi''16-! mi''( la'') |
    \grace sold''8 fad'' mi''16( re'' dod'' si') |
    \grace la'8 sold'4. |
    mi''4\f fad''8 |
    \grace mi''8 re''4 mi''8 |
    \grace re''8 dod''4 re''8 |
    \grace dod''8 si'4 la'8 |
    si'16.( dod''32 re''8) dod'' |
    si'16.( la'32 sold'8) la' |
    si'16.( dod''32 re''8) dod'' |
    \grace dod''8 si'4 r8 |
    si'8.\p( re''16 dod'' si') |
    lad'8(\rf si'8) r |
    la'!8.\p( dod''16 si' la') |
    sold'!8\rf la' r |
    fad'16\rf( fad'' mi'' re'' dod'' si') |
    \grace la'8 sold'4 la'8 |
    si'16( dod''32 re'' dod''8) si' |
    \grace si'8 dod''4 r8 |
    fad'16( fad'' mi'' re'' dod'' si') |
    \grace la'8 sold'4 la'8 |
    si'16( dod''32 re'' dod''8) si' |
    la'16(
  }
  \tag #'secondo {
    fad'4 la'8 |
    mi'4 si'8 |
    mi'4 la'8 |
    la'4 fad'8 |
    \grace dod'8 si4. |
    dod''4-\sug\f dod''8 |
    \grace dod''8 si'4 si'8 |
    \grace si'8 la'4 la'8 |
    \grace la'8 sold'4 la'8 |
    si'4 la'8 |
    sold'16.( la'32 si'8) la' |
    sold'16.( la'32 si'8) la' |
    \grace la'8 sold'4 r8 |
    fad'4-\sug\p fad'8 |
    sol'(-\sug\rf fad') r |
    mi'4-\sug\p mi'8 |
    mi'-\sug\rf mi' r |
    la16-\sug\rf( la' sold' fad' mi' re') |
    dod'8 si la |
    la' la' sold' |
    \grace sold'8 la'4 r8 |
    la16( la' sold' fad' mi' re') |
    dod'8 si la |
    la'8 la' sold' |
    la'16(
  }
>> la''16 sold'' fad'' mi'' red'') |
mi''( fad'' mi'' re''! dod'' si') |
dod''( mi'') fad'( la') sold'( si') |
la'8 la r |
<<
  \tag #'primo {
    re''4-\sug\p dod''16. re''32 |
    \grace dod''8 si'4 la'8 |
    \grace la' sol' fad' mi' |
    re' re' r |
    mi''4 fad''16( sol''32 la'') |
    \grace la''8( dod''4)\rf re''8 |
    si''( la''8. si''32 sol'') |
    fad''16. sol''32 la''8 r |
    mi''4( fad''16 sol''32 la'') |
    \grace la''8( dod''4)\rf re''8 |
    si''8\f la''8. si''32 sol'' |
    fad''16. mi''32 re''16-!
  }
  \tag #'secondo {
    fad'4-\sug\p la'8 |
    \grace la'8 sol'4 fad'8 |
    \grace fad' mi' re' dod' |
    re'4 r8 |
    dod'16-. la'-. dod'-. la'-. re'-. la'-. |
    mi'-. la'-. sol'-. la'-. fad'-. la'-. |
    sol'8 fad'8. sol'32 mi' |
    re'16. mi'32 fad'8 r |
    dod'16 la' dod' la' re' la' |
    mi' la' sol' la' fad' la' |
    sol'8-\sug\f fad'8. sol'32 mi' |
    re'8 r16
  }
>> re''16(\f fad'' la'') |
\grace la''8 sol'' fad''16( mi'' re'' dod'') |
\grace dod''8 re''4. |
<<
  \tag #'primo {
    <>\p \ru#3 { fad'16( re') } |
    mi'( la') mi'( la') la'( lad') |
    si'( sold') mi'( fad') re'( mi') |
    dod''( la') mi'( fad') re'( mi') |
    dod'( la) dod'( mi') dod''( si') |
    la'( sold') la'( sold') la'( lad') |
    si'32 dod'' re'' dod'' si'16-.( la'-. sold'-. fad'-.) |
    mi'4 r8 |
    si''16.(_\markup { \dynamic f \italic dolce } dod'''32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' |
    si''16.( dod'''32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' |
    la''4.\p~ |
    la'' |
    dod'''8.( si''16 la'' si'') |
    \grace si''8 la''4 r8 |
  }
  \tag #'secondo {
    <>-\sug\p \ru#3 { re'16( la) } |
    la( mi') la( mi') mi' mi' |
    mi' mi' si si si si |
    la4 mi'8 |
    mi'16 mi' mi' mi' mi' mi' |
    re' re' re' re' fad' fad' |
    fad' fad' fad' fad' fad' fad' |
    si4 r8 |
    si'4\f la'8 |
    \ru#3 { sold'16.( la'32 si'8) la' | }
    mi''4.(-\sug\p |
    fad'') |
    mi''8.( re''16 dod'' re'') |
    \grace re''8 dod''4 r8 |
  }
>>
\ru#2 { la'16-.\f si'-.\p dod''-. re''-. red''(\rf mi'') | }
\ru#2 { si'-.\f dod''-.\p re''-. mi''-. mid''(\rf fad'') | }
<<
  \tag #'primo {
    \ru#2 { mi''16-.\f sold''-.\p la''-. si''-. sid''(\rf dod''') | }
    mi''16-.\p fad''-. sold''-. la''-. lad''(\rf si'') |
    mi''16-.\f fad''-. sold''-. la''-. lad''( si'') |
    la'' sold'' la'' la' fad'' red'' |
  }
  \tag #'secondo {
    la'16-.-\sug\f si'-.-\sug\p dod''-. re''-. red''(-\sug\rf mi'') |
    dod''-.-\sug\f si'-.-\sug\p dod''-. re''-. red''(-\sug\rf mi'') |
    sold'-.-\sug\p la'-. si'-. dod''-. dod''(-\sug\rf re'') |
    sold'-.-\sug\f la'-. si'-. dod''-. re''( mi'') |
    dod''8. la'16 fad'' red'' |
  }
>>
red''16( mi'') mi''( la' re'' sid') |
\grace sid'8 dod''8 dod''16( fad' si' sold') |
la'8 la'32( si' dod'' re'' mi'' fad'' sold'' la'') |
<<
  \tag #'primo {
    fad''16. re''32 \grace dod''8 si'4\trill |
    la'8 la r |
  }
  \tag #'secondo {
    re''16. si'32 \grace dod''8 si'4\trill |
    la'8 dod' r |
  }
>>
<<
  \tag #'primo {
    la'4\p( si'16 dod'') |
    \grace dod''8 re''4( dod''16 si') |
    la'16.([\rf si'32]) si'8. la'32 sol'! |
    \grace sol'8 fad' fad' r |
    mi''4 fad''16( sol''32 la'') |
    \grace la''8( dod''4)\rf re''8 |
    si''( la''8. si''32 sol'') |
    fad''16. sol''32 la''8 r |
    mi''4 fad''16( sol''32 la'') |
    \grace la''8( dod''4)\rf( re''8) |
    si''\f la''8. si''32 sol'' |
    fad''16. mi''32 re''16
  }
  \tag #'secondo {
    dod'4-\sug\p( re'16 mi') |
    re'( fad' la') fad'( fad' fad') |
    mi'8 mi' mi' |
    la la r |
    dod'16-. la'-. dod'-. la'-. re'-. la'-. |
    mi'-. la'-. sol'-. la'-. fad'-. la'-. |
    sol'8 fad'8. sol'32 mi' |
    re'16. mi'32 fad'8 r |
    dod'16 la' dod' la' re' la' |
    mi' la' sol' la' fad' la' |
    sol'8-\sug\f fad'8. sol'32 mi' |
    re'8.
  }
>> re''16( fad'' la'') |
\grace la''8 sol'' fad''16( mi'' re'' dod'') |
re''8 re' r |
<<
  \tag #'primo {
    la''16. si''32 do'''8 si'' |
    la''16.( sol''32 fad''8) sol'' |
    <re'' la''>4. |
    <re'' si''>4 r8 |
    si''16.( dod'''!32 re'''8) dod''' |
    si''16.( la''32 sold''8) la'' |
    <mi'' si''>4. |
    <mi'' dod'''>4 la'8 |
    re'' re'' re'' |
    dod''8.
  }
  \tag #'secondo {
    R4. |
    fad'16.( sol'32 la'8) sol' |
    <re' la'>4. |
    <re' si'>4 r8 |
    R4. |
    sold'16.( la'32 si'8) la' |
    <mi' si'>4. |
    <mi' dod''>4 la'8 |
    sold' sold' sold' |
    la'8.
  }
>> la'16\f si' dod'' |
re''32[( mi'') mi''( fad'')] fad''[( re'') re''( dod'')] si'[( la'') la''( sold'')] |
la''4 r8 |
<<
  \tag #'primo {
    mi''16.( fad''32 sol''8) fad'' |
    mi''16.( re''32 dod''8) re'' |
    mi''16.( fad''32 sol''8) fad'' |
    mi''16.( re''32 dod''8) re'' |
    mi''16.( fad''32 sol''8) fad'' |
    mi''16.( re''32 dod''8) re'' |
  }
  \tag #'secondo {
    R4.*2 |
    dod'''16.( re'''32 mi'''8) re''' |
    dod'''16.( si''32 la''8) la'' |
    dod'''16.( re'''32 mi'''8) re''' |
    dod'''16.( si''32 la''8) la'' |
  }
>>
<la' re'>4.\p |
<re' si'>4. |
<re' la'>4. |
<<
  \tag #'primo {
    sol'4. |
    fad'8( sol') la' |
    si'16( sol' fad'8) mi' |
    \grace mi'8 fad'4. |
  }
  \tag #'secondo {
    re'4 dod'8 |
    re'( mi') fad' |
    sol'16( mi' re'8) dod' |
    \grace dod'8 re'4. |
  }
>>
\ru#2 { re''16-.\f mi''-.\p fad''-. sol''-. sold''(\rf la'') | }
<<
  \tag #'primo {
    si''32[(-\rf lad'') si''( lad'')] si''16[(\p lad'') si''( lad'')] |
    si''16.[ dod'''32] re'''32[( dod''') re'''( dod''')] re'''[( dod''') re'''( dod''')] |
    re'''[ dod''' re''' dod'''] re'''16[ si'' la'' sol''] |
    fad''\fp mi'' fad'' sol'' sold''( la'') |
    fad''16-. mi''-. fad''-. sol''-. sold''(\rf la'') |
    la''\p re''' re''' re''' re''' re''' |
    dod'''32\ff dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' dod''' |
    re'''8 re' r |
    si'(\p la' dod'') |
    \grace dod''8 re''4 r8 |
  }
  \tag #'secondo {
    re''16-\sug\f re'' re''4:16-\sug\p |
    re''8 si'16 si' si' si' |
    si'16 si' si' sol'' fad'' mi'' |
    re''-\sug\fp dod'' re'' mi'' mid'' fad'' |
    re''-. dod''-. re''-. mi''-. mid''(-\sug\rf fad'') |
    mi''!4.:16-\sug\p |
    mi'':32-\sug\ff |
    re''8 re' r |
    sol'(-\sug\p fad' mi') |
    \grace mi'8 fad'4 r8 |
  }
>>
<re' re'' si''>8\f <la' la''>8.( dod''16) |
re''8. \grace { mi''32[ re'' dod''] } re''16 fad'' la'' |
la''8. sol''16 fad''8 |
si'16. mi''32 re''8 dod'' |
re''[ r16 si' dod'' re''] |
sold'8[ r16 sold' la' si'] |
\custosNote mid'8
