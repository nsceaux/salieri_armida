\clef "treble" R4.*5 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mi''4 fad''8 |
    \grace mi''8 re''4 mi''8 |
    \grace re''8 dod''4 re''8 |
    \grace dod''8 si'4 la'8 |
    mi''4.~ |
    mi''~ |
    mi''~ |
    mi''4 }
  { dod''4 dod''8 |
    \grace dod''8 si'4 si'8 |
    \grace si'8 la'4 la'8 |
    \grace la'8 sold'4 la'8 |
    mi''4.~ |
    mi''~ |
    mi''~ |
    mi''4 }
>> r8 |
R4. |
<>\rf \twoVoices #'(primo secondo tutti) <<
  { lad''8([ si'']) }
  { sol'([ fad']) }
>> r8 |
R4. |
<>\rf \twoVoices #'(primo secondo tutti) <<
  { sold''8[ la''] }
  { mi'[ mi'] }
>> r8 |
R4.*2 |
\twoVoices #'(primo secondo tutti) <<
  { si''8 la'' sold'' | \grace sold''8 la''4 }
  { re''8 dod'' si' | \grace si'8 dod''4 }
>> r8 |
R4.*2 |
\twoVoices #'(primo secondo tutti) <<
  { si''8 la'' sold'' | }
  { re''8 dod'' si' | }
>>
la''8 sold''16( fad'' mi'' red'') |
mi'' fad'' mi'' re''! dod'' si' |
dod'' mi'' fad'' la'' sold'' si'' |
la''8 la' r |
R4.*11 |
r8. re''16(-\sug\f fad'' la'') |
\grace la''8 sol'' fad''16 mi'' re'' dod'' |
re''4 r8 |
R4.*8 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4.~ | mi''~ | mi''~ | mi'' | }
  { mi''4.~ | mi''~ | mi''~ | mi'' | }
>>
R4.*4 |
\twoVoices #'(primo secondo tutti) <<
  { mi''8 s s |
    mi'' s s |
    fad'' s s |
    fad'' s s |
    la''4.~ |
    la'' |
    sold''8 s s |
    sold''4. | }
  { la'8 s s |
    la' s s |
    la' s s |
    si' s s |
    dod''4.~ |
    dod'' |
    si'8 s s |
    si'4. | }
  { s8 r r |
    s r r |
    s r r |
    s r r |
    s4.-\sug\fp |
    s |
    s8 r r |
    s4.-\sug\f }
>>
\twoVoices #'(primo secondo tutti) <<
  { la''16 sold'' la'' la' fad'' red'' | }
  { dod''8. la'16 fad'' red'' | }
>>
red''16( mi'') mi''( la' re'' sid') |
\grace sid'8 dod''8 dod''16( fad' si' sold') |
la'16*2 la'32( si' dod'' re'' mi'' fad'' sold'' la'') |
\twoVoices #'(primo secondo tutti) <<
  { fad''16.[ re''32] }
  { re''16.[ si'32] }
>> \grace dod''8 si'4\trill |
la'4 r8 |
R4.*11 | \allowPageTurn
r8. re''16-\sug\f fad'' la'' |
\grace la''16 sol''8 fad''16 mi'' re'' dod'' |
re''4 r8 |
R4.*2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { la''4. | si''4 }
  { re''4. | re''4 }
>> r8 |
R4.*2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { si''4. | dod'''4 }
  { mi''4. | mi''4 }
>> r8 |
R4.*4 | \allowPageTurn
<>^"Soli" mi''16.( fad''32 sol''8) fad'' |
mi''16.( re''32 dod''8) re'' |
mi''16.( fad''32 sol''8) fad'' |
mi''16.( re''32 dod''8) re'' |
mi''16.( fad''32 sol''8) fad'' |
mi''16.( re''32 dod''8) re'' |
R4.*7 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { re''8 s s |
    re'' s s |
    re'' s s | }
  { la'8 s s |
    la' s s |
    si' s s | }
  { s8 r r |
    s8 r r |
    s8 r r | }
>>
R4.*5 | \allowPageTurn
<>\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. | }
  { dod'' | }
>>
re''8 re' r |
<>\p \twoVoices #'(primo secondo tutti) <<
  { si''8 la'' sol'' |
    \grace sol''8 fad''4 s8 |
    si''8 la'' sol'' |
    fad''8. }
  { sol''8 fad'' mi'' |
    \grace mi''8 re''4 s8 |
    sol''8 fad'' mi'' |
    re''8. }
  { s4. | s4 r8 | s4.\f }
>> \grace { mi''32[ re'' dod''] } re''16 fad'' la'' |
la''8. sol''16 fad''8 |
si'16. mi''32 re''8 dod'' |
re''4 r8 |
R4. |

\twoVoices #'(primo secondo tutti) <<
  {
  }
  {
  }
>>
<<
  \tag #'(primo tutti) {

  }
  \tag #'(secondo tutti) {

  }
>>
