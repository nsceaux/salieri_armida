<<
  \tag #'voix1 {
    \clef "soprano/treble" R4.*5 |
    mi''4 fad''8 |
    \grace mi''8 re''4 mi''8 |
    \grace re''8 dod''4 re''8 |
    \grace dod''8 si'4 la'8 |
    si'16.([ dod''32 re''8]) dod''8 |
    si'16.[ la'32 sold'8] la'8 |
    si'16.[ dod''32 re''8] dod''8 |
    \grace dod'' si'4 r8 |
    si'8.[ re''16] dod''[ si'] |
    lad'8 si' r |
    la'!8.[ dod''16] si' la' |
    sold'8 la' r |
    fad'16([ fad'' mi'' re'']) dod''[ si'] |
    \grace la'8 sold'4 la'8 |
    si'16[ dod''32 re'' dod''8] si' |
    \grace si' dod''4 r8 |
    fad'16([ fad'' mi'' re'']) dod''[ si'] |
    \grace la'8 sold'4 la'8 |
    si'16[ dod''32 re'' dod''8] si' |
    la'4 r8 |
    R4.*3 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R4.*5 |
    dod''4 dod''8 |
    \grace dod''8 si'4 si'8 |
    \grace si'8 la'4 la'8 |
    \grace la'8 sold'4 la'8 |
    si'4 la'8 |
    sold'16.[ la'32 si'8] la' |
    sold'16.[ la'32 si'8] la' |
    \grace la'8 sold'4 r8 |
    fad'4 fad'8 |
    sol' fad' r |
    mi'4 mi'16 mi' |
    mi'8 mi' r |
    fad'16([ fad'' mi'' re'']) dod''[ si'] |
    \grace la'8 sold'4 la'8 |
    la'4 sold'8 |
    \grace sold'8 la'4 r8 |
    fad'16([ fad'' mi'' re'']) dod''[ si'] |
    \grace la'8 sold'4 la'8 |
    la'4 sold'8 |
    la'4 r8 |
    R4.*3 |
  }
  \tag #'voix3 {
    \clef "alto/treble" R4.*5 |
    la'4 fad'8 |
    fad'4 mi'8 |
    mi'4 re'8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    mi'4 r8 |
    mi'4 re'8 |
    dod' re' r |
    dod'4 dod'16 dod' |
    si8 dod' r |
    la16([ la' sold' fad']) mi'([ re']) |
    dod'8[ si] la |
    fad'[ mi'] mi' |
    mi'4 r8 |
    la16([ la' sold' fad']) mi'[ re'] |
    dod'8[ si] la |
    fad'[ mi'] mi' |
    la4 r8 |
    R4.*3 |
  }
  \tag #'ismene { \clef "alto/treble" R4.*28 | }
>>
<<
  \tag #'voix1 {
    R4.*22 |
    si'16.[ dod''32 re''8] dod'' |
    si'16.[ la'32 sold'8] la' |
    si'16.[ dod''32 re''8] dod'' |
    si'16.[ la'32 sold'8] la' |
    la'4 la'8 |
    la'4 la'8 |
    dod''8.[ si'16] la'[ si'] |
    \grace si'8 la'4 r8 |
    R4.*39 |
    r8 la' la' |
    la'4. |
    la' |
    la' |
    la' |
    la' |
    la' |
    la'4 la'8 |
    si'4.\melisma |
    la' |
    sol' |
    fad'8[ sol']\melismaEnd la' |
    si'[ la'] la' |
    fad'4 r8 |
    R4.*18 |
  }
  \tag #'voix2 {
    R4.*22 |
    si'4 la'8 |
    sold'16.[ la'32 si'8] la' |
    sold'16.[ la'32 si'8] la' |
    sold'16.[ la'32 si'8] la' |
    mi'4 mi'8 |
    fad'4 fad'8 |
    mi'4 mi'8 |
    \grace re'8 dod'4 r8 |
    R4.*39 |
    r8 la' la' |
    la'4. |
    la' |
    la' |
    la' |
    la' |
    la' |
    re'4 re'8 |
    re'4.\melisma |
    re' |
    re'4 dod'8 |
    re'[ mi']\melismaEnd fad' |
    sol'[ re'] dod' |
    re'4 r8 |
    R4.*18 |
  }
  \tag #'voix3 {
    R4.*22 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    mi'4 mi'8 |
    dod'4 dod'8 |
    re'4 re'8 |
    mi'4 mi'8 |
    fad'4 r8 |
    R4.*39 |
    r8 la' la' |
    la'4. |
    la' |
    la' |
    la' |
    la' |
    la' |
    fad'4 fad'8 |
    sol'4.\melisma |
    fad' |
    mi' |
    re'4\melismaEnd re'8 |
    sol'[ la'] la |
    re'4 r8 |
    R4.*18 |
  }
  \tag #'ismene {
    <>^\markup\character Ismene
    re''4 dod''16.[ re''32] |
    \grace dod''8 si'4 la'8 |
    \grace la'8 sol'[ fad'] mi' |
    \grace mi'8 re'16[ dod'] re'8 r |
    mi'4 fad'16[ sol'32 la'] |
    \grace la'8( dod'4) re'8 |
    si'8[ la'8.] si'32[ sol'] |
    fad'16.[ sol'32] la'8 r |
    mi'4 fad'16[ sol'32 la'] |
    \grace la'8( dod'4) re'8 |
    si'8[ la'8.] si'32[ sol'] |
    \grace sol'16 fad'[ mi'32 re'] re'8 r |
    R4.*2 |
    re'4 fad'16.[ re'32] |
    \grace si'8 la' la'8.[( lad'16)] |
    si'16[ sold'] mi'8. fad'32[ re'] |
    dod'16.[ re'32] mi'8 r |
    la'4 sol'!8 |
    fad'8[~ fad'32\melisma la' sold' si'] la'[ dod'' lad' dod''] |
    si'[ dod'' re'' dod'' si'16]\melismaEnd la' sold' fad' |
    mi'4.~ |
    mi'~ |
    mi'~ |
    mi'~ |
    mi' |
    \clef "soprano/treble" la'4 dod''8 |
    fad'4 fad''8 |
    mi''8.[ re''16] dod''[ re''] |
    dod''4 r8 |
    la'4. |
    mi'' |
    fad'4. |
    fad'' |
    mi''~ |
    mi''32[\melisma red'' mi'' red''] mi''16[ dod'' si' la']\melismaEnd |
    si'4.\startTrillSpan~ |
    si'4.*2/3 s8\stopTrillSpan |
    la'4 r8 |
    R4.*5 |
    \clef "alto/treble" la'4 si'16[ dod''] |
    \grace mi''8 re''4( dod''16[ si']) |
    la'16.[ si'32] si'8. la'32[ sol'] |
    \grace sol'8 fad' fad' r |
    mi'4 fad'16[ sol'32 la'] |
    \grace la'8 dod'4 re'8 |
    si'8[ la'8.] si'32[ sol'] |
    fad'16.[ sol'32] la'8 r |
    mi'4 fad'16[ sol'32 la'] |
    \grace la'8 dod'4 re'8 |
    si'[ la'8.] si'32[ sol'] |
    fad'16.[ mi'32] re'8 r |
    R4.*2 |
    la'16.[ si'32 do''8] si' |
    la'16.[ sol'32 fad'8] sol' |
    la'8.[ do''32 si'] \grace re''16 do''[ si'32 la'] |
    sol'8 sol' r |
    si'16.[ dod''!32 re''8] dod'' |
    si'16.[ la'32 sold'8] la' |
    si'8.[ re''32 dod''] \grace mi''16 re''[ dod''32 si'] |
    \grace si'16 la'8 la' la'16 la' |
    re''8. dod''32[ si'] la'16 sold' |
    la'4 r8 |
    R4. |
    la'4 r8 |
    R4.*2 |
    mi'16.[ fad'32 sol'!8] fad' |
    mi'16.[ re'32 dod'8] re' |
    mi'16.[ fad'32 sol'!8] fad' |
    mi'16.[ re'32 dod'8] re' |
    \clef "soprano/treble" la'4 re''8 |
    si'[~ si'32\melisma re'' dod'' re''] \grace mi''16 re''[ dod''32 re''] |
    la'8[~ la'32 re'' dod'' re''] mi''[ re'' dod'' re''] |
    sol'16[ mi''] mi''32([ re'' dod'' re'']) dod''([ si' la' sol']) |
    fad'16[ la' sol' si']\melismaEnd la'16[ re''] |
    si'[ sol' fad'8] mi' |
    \grace mi'8 fad'4 r8 |
    la'4. |
    re'' |
    si'8.[\melisma lad'16 si' lad'] |
    si'16.[ dod''32] re''[ dod'' re'' dod''] re''[ dod'' re'' dod''] |
    re''[ dod'' re'' dod''] re''16[ si']\melismaEnd la'[ sol'] |
    fad'8( la'4)\melisma |
    la'32[ sol' la' si'] la'16[ fad' mi' re']\melismaEnd |
    mi'4.\trill~ |
    mi' |
    re'4 r8 |
    si'([ la']) dod'' |
    \grace dod''8 re''4 r8 |
    si'[ la'] dod' |
    re'4 r8 |
    R4.*4
  }
>>