\clef "treble" \transposition re <>
R4.*9 |
\twoVoices #'(primo secondo tutti) <<
  { re''4.~ | re''~ | re''~ | re''4 }
  { re''4.~ | re''~ | re''~ | re''4 }
>> r8 |
R4.*15 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { do''4. | do''4. | s8 sol' sol' | do''4 }
  { do'4. | do' | s8 sol' sol' | do'4 }
  { s4.*2 | r8 s s | }
>> r8 |
R4.*2 |
r8 \twoVoices #'(primo secondo tutti) <<
  { sol'4 | do''4 }
  { sol'4 | do'' }
>> r8 |
R4.*2 |
r8 <>\f \twoVoices #'(primo secondo tutti) <<
  { sol'8 sol' | do''4 do''8 | do'' do'' sol' | do''[ do'] }
  { sol'8 sol' | do'4 do'8 | do'' do'' sol' | do''8[ do'] }
>> r8 |
R4.*7 |
<>^"[a 2.]" re''4.~ |
re''-\sug\f ~ |
re''~ |
re''~ |
re'' |
R4.*4 |
re''8 r r |
re'' r r |
mi'' r r |
mi'' r r |
re''4.~ |
re'' |
R4. |
re''4.-\sug\f |
re''8 sol' r |
re'' r r |
re''4 do''8 |
sol'4 r8 |
r8 re'' re'' |
sol'4 r8 |
R4.*11 |
do''4-\sug\f do''8 |
do'' re'' re'' |
do''4 r8 |
R4.*2 |
do''4.-\sug\p~ |
do''4 r8 |
R4.*2 |
re''4.-\sug\p~ |
re''4 r8 |
R4.*3 |
r8 \twoVoices #'(primo secondo tutti) <<
  { sol''8 sol'' |
    sol''4.~ |
    sol''~ |
    sol''~ |
    sol'' |
    sol''4. |
    sol'' | }
  { sol'8 sol' |
    sol'4.~ |
    sol'~ |
    sol'~ |
    sol' |
    sol'4. |
    sol' | }
>>
R4.*7 |
<>^"[a 2.]" do''8 r r |
do'' r r |
do'' r r |
R4.*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4. | mi'' | re'' | re'' | do''4 }
  { do''4. | do'' | do'' | sol' | mi'4 }
  { s4.-\sug\fp | s | s-\sug\p | s\f | }
>> r8 |
R4.*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4. | do'' | do''8 mi'' re'' | do''4 }
  { do'4. | do' | do''8 do'' sol' | do''4 }
>> r8 |
R4. |
