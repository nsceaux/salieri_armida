\piecePartSpecs
#`((flauti #:score-template "score-deux")
   (oboi)
   (corni #:tag-global ()
          #:instrument "Corni in D")
   (fagotti
    #:music , #{

\quoteBassi "ACBbassi"
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*5\mmRestVisible }
s4.*23
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*4\mmRestVisible }
s4.*2
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*2\mmRestVisible }
s4.*2
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.\mmRestVisible }
s4.*3
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*8\mmRestVisible }
s4.*4
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*3\mmRestVisible }
s4.*15
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*4\mmRestVisible }
s4.*2 \allowPageTurn
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*2\mmRestVisible }
s4.*2
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.\mmRestVisible }
s4.*13
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*2\mmRestVisible }
s4.*11 \clef "bass"
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*2\mmRestVisible }
s4.*3
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*5\mmRestVisible }
s4.*2
\cue "ACBbassi" { <>^\markup\tiny Bassi \mmRestInvisible s4.*3\mmRestVisible }


                #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\column {

  \livretPersDidas Tutto il Coro con Ismene
  \livretVerse#12 { Vieni al fonte del contento, }
  \livretVerse#10 { Fortunato passeggier. }
  \livretVerse#10 { E' perduto ogni momento, }
  \livretVerse#10 { Che si perde pel' piacer. }
  \livretPers Ismene
  \livretVerse#12 { Dell' Amor la reggia è questa, }
  \livretVerse#10 { La delizia de mortali; }
  \livretVerse#10 { Nell' oblio di tutti i mali }
  \livretVerse#10 { Qui si viene a riposar. }
  \livretVerse#10 { Qui non v'è cura molesta, }
  \livretVerse#10 { Che il piacer di tosco infetti, }
  \livretVerse#10 { E il più dolce de diletti }
  \livretVerse#10 { Non ti costa, che il bramar. }
  \livretPers Tutto il Coro
  \livretVerse#12 { Vieni al fonte del contento, }
  \livretVerse#10 { Fortunato passeggier. }
  \livretVerse#10 { E' perduto ogni momento, }
  \livretVerse#10 { Che si perde pel' piacer. }

} #}))
