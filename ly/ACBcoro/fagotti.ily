\clef "bass" R4.*5 |
r8 la\f lad |
si4 sold8 |
la!4 fad8 |
mi re dod |
sold4 la8 |
si16.( dod'32 re'8) dod' |
si16.( la32 sold8) la |
mi mi, mi |
r re\p re |
mi(\rf re) r |
r dod\p dod |
re\rf dod r |
re4.\f |
mi8( re dod) |
re mi mi, |
la,16 la la, dod mi la |
re4. |
mi8 re dod |
re mi mi, |
la,4 la8 |
sold4 mi8 |
la, re mi |
la la, r |
R4.*11 |
re4\f re8 |
si sol la |
re re, r |
R4.*8 |
sold4\f la8 |
si16. dod'32 re'8 dod' |
si16. la32 sold8 la |
si16. dod'32 re'8 dod' |
R4.*3 |
fad16[-\sug\f fad fad fad,] \grace sold16 fad mi32 re |
dod8 r r |
dod r r |
re r r |
re r r |
mi r r |
mi16-\sug\f mi[\p mi mi mi mi] |
mi8 r r
mi16\f mi mi mi mi mi |
la,8 la la |
sold r fad |
mi r re |
dod4 r8 |
re8 mi mi, |
la,4 r8 |
R4.*11 |
re4-\sug\f re8 |
si sol la |
re re, r |
fad4 sol8 |
\once\slurDashed la16.( si32 do'8) si16 sol |
fad4. |
sol8 sol16-. la-. si-. la-. |
sold4 la8 |
si16.( dod'!32 re'8) dod'16 la |
sold4. la8 la, la |
si4. |
la8 r la\f |
R4.*2 |
\clef "tenor" dod'4 re'8 |
mi'16.( fad'32 sol'8) fad' |
mi'16.( re'32 dod'8) re' |
mi'16.( fad'32 sol'8) fad' |
mi'16.( re'32 dod'8) re' |
mi'16.( fad'32 sol'8) fad' |
fad'4.\p |
sol' |
fad' |
mi' |
re'8 r r |
R4.*2 |
\clef "bass" fad8 r r |
fad r r |
sol r r |
R4.*5 |
la,16-\sug\f la, la, la, la, la, |
re8 re' r |
R4.*3 |
re16-\sug\f re re re re re |
re4.:16 |
 sol16 sol la la la, la, |
 re8[ r16 si dod' re'] |
 sold8[ r16 sold la si] |
 \custosNote mid8
