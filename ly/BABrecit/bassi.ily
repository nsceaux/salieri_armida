\clef "bass" R1 |
sib,2. fad4~ |
fad1~ |
fad2 sol |
fa!1~ |
fa |
mi~ |
mi2 la~ |
la2 sol |
fa1~ |
fa2 mib~ |
mib1~ |
mib |
re2 mib~ |
mib1~ |
mib~ |
mib2 mi~ |
mi fa |
mib1~ |
mib~ |
mib2 reb~ |
reb1~ |
reb~ |
reb~ |
reb2 si,!~ |
si,1~ |
\once\tieDashed si,~ |
si,2 \once\tieDashed mi~ |
mi1~ |
\once\tieDashed mi~ |
mi~ |
mi~ |
mi~ |
mi |
\tieDashed do~ |
do~ |
\tieSolid do~ |
\once\tieDashed do~ |
do2 mi |
fa1~ |
fa~ |
fa |
\once\tieDashed re~ |
re |
fad |
sol |
fa!~ |
fa2 mib1~ |
\once\tieDashed mib~ |
\ficta mib2 re~ |
re1~ |
\once\tieDashed re~ |
re2 sol~ |
sol4 sol_\markup\italic dol. sol sol |
sol4 r r4 la\f |
re r sib4(\p |
do'4) r la( |
sib2.)\fermata |
