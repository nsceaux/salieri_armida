\tag #'basse {
Ad -- di -- o.

Già m’ab -- ban -- do -- ni?

Ah ques -- ta è l’o -- ra
che da te lun -- gi, o ca -- ro,
mi ri -- chia -- ma o -- gni dì.

Ma qual ti sfor -- za,
a ra -- pi -- re o -- gni gior -- no
tan -- ti dol -- ci mo -- men -- ti al nos -- tro a -- mo -- re
cru -- da, bar -- ba -- ra leg -- ge?

Il mio ti -- mo -- re.

Ti -- mor? Di che?

Del tu -- o, del mio ri -- po -- so.

E chi po -- tria tur -- bar -- lo, in ques -- ta, o ca -- ra,
se -- pa -- ra -- ta dal mon -- do i -- gno -- ta spon -- da?

Nu -- mi! Il guar -- do del so -- le, i ven -- ti, l’on -- da…
Ah tut -- to a chi ben a -- ma,
è ca -- gion di ti -- mor: per nos -- tro a -- si -- lo
quest’ i -- so -- la fe -- li -- ce io scel -- si in -- va -- no
in grem -- bo all’ O -- ce -- a -- no. In -- van le cin -- si
di fos -- ca neb -- bia il pie -- de, e i fian -- chi, e il ter -- go
di di -- ru -- pa -- te or -- ri -- de bal -- ze, ag -- giun -- si
fol -- ta guar -- dia di mos -- tri, e d’in -- si -- dio -- se
si -- re -- ne al -- let -- ta -- tri -- ci! I frut -- ti, i fon -- ti
di tos -- co as -- per -- si, e quan -- to mi -- ri in le -- i
e gli au -- gel -- li, e le pian -- te, e l’on -- da, e il ven -- to
tut -- to è in nos -- tra di -- fe -- sa, ep -- pur pa -- ven -- to.
Ah dal dì ch’io can -- gia -- i
per -- te, bell’ i -- dol mi -- o, gli af -- fet -- ti mie -- i;
pa -- tria, re -- gno, te -- sor, tut -- to per -- dei;
pen -- sa, che s’io ti per -- do,
fuor che il ros -- sor del -- la mia fé tra -- di -- ta,
nul -- la mi res -- ta.

Ah non te -- mer, mia vi -- ta.
Sai che la mia tu se -- i,
co -- me io son l’al -- ma tu -- a; che non poss’ i -- o
vi -- ver da te di -- vi -- so un sol mo -- men -- to,
sa -- i che o -- gni mi -- o con -- ten -- to,
o -- gni mia spe -- me è in te, ch’al -- tro non bra -- mo,
che col tuo dol -- ce no -- me il fia -- to es -- tre -- mo
spi -- rar frà lab -- bri tu -- oi.
}
Lo so: ma tre -- mo.
