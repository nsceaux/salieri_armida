\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \violiniInstr \haraKiriFirst } <<
      \new Staff <<
        \keepWithTag #'complet \global
        \keepWithTag #'(complet primo) \includeNotes "violini" >>
      \new Staff <<
        \keepWithTag #'complet \global
        \keepWithTag #'(complet secondo) \includeNotes "violini" >>
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      { s1 s4
        \set Staff.shortInstrumentName = \markup\character Ar. s2. s1 s2.
        \set Staff.shortInstrumentName = \markup\character Ri. s4 s1*3 s2.
        \set Staff.shortInstrumentName = \markup\character Ar. s4 s
        \set Staff.shortInstrumentName = \markup\character Ri. s2. s4
        \set Staff.shortInstrumentName = \markup\character Ar. s2. s2.
        \set Staff.shortInstrumentName = \markup\character Ri. s4 s1*2 s2.
        \set Staff.shortInstrumentName = \markup\character Ar. s4 s1*28 s2
        \set Staff.shortInstrumentName = \markup\character Ri. s2 s1*4 s1. s1*4 s2.
        \set Staff.shortInstrumentName = \markup\character Ar. s4 s1*2 }
      \keepWithTag #'complet \global
      \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { shortInstrumentName = "B." } <<
      \keepWithTag #'complet \global
      \includeNotes "bassi"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*2\break s1*2\break s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2\break s1*2\break s1*2\break s1*2\pageBreak
        s1*2\break s1*2\break s1*2 s2 \bar "" \break s2 s1*2\break s1*2\pageBreak
        s1*2\break s1*3\break s1*2\break s1*2\break s1*2\pageBreak
        s1*3\break s1 s2 \bar "" \break s1*3\break s1*2\break s1\pageBreak
        s1*2 s2.\break \grace s8
      }
    >>
  >>
  \layout {
    indent = \smallindent
  }
  \midi { }
}
