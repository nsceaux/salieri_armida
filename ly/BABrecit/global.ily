\key do \major
\midiTempo#100
\tag #'complet {
  \time 4/4 s1*47
  \measure 3/2 s1.
  \measure 4/4 s1*5
}
\time 4/4 \tag #'part { \partial 4 s4 } s1*2 \bar "|."
\time 3/4 s2.*3 \bar "|."
