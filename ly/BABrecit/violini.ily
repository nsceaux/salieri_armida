\clef "treble"
\tag #'complet { R1*47 R1. R1*5 | }
\tag #'part r4
<<
  \tag #'primo {
    r4 sib''8(_\markup\italic dol. sol'') sol''( mib'') mib''( re'') |
    dod''4 r r la'8.\f( si'!32 dod'') |
    re''4 r re''8.(\p mib''32 fa'') |
    \grace fa''8 mib''4 r do''8.( re''32 mib'') |
    \grace mib''8 re''2.\fermata |
  }
  \tag #'secondo {
    r4 sib'8(_\markup\italic dol. sol') sol'( mib') mib'( re') |
    dod'4 r r dod'8.(\f re'32 mi') |
    fa'4 r \ficta sib'\p |
    \ficta sib' r fa' |
    fa'2. |
  }
>>
