\piecePartSpecs
#`((violino1 #:score "score-violini" #:system-count 1)
   (violino2 #:score "score-violini" #:system-count 1)
   (bassi #:score "score-bassi" #:tag-global complet)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Armida
    \livretVerse#12 { Addio. }
    \livretPers Rinaldo
    \livretVerse#12 { Già m'abbandoni? }
    \livretPers Armida
    \livretVerse#12 { Ah questà è l'ora }
    \livretVerse#12 { Che da te lungi, o caro, }
    \livretVerse#12 { Mi richiama ogni dì. }
    \livretPers Rinaldo
    \livretVerse#12 { Mà qual ti sforza }
    \livretVerse#12 { A rapire ogni giorno }
    \livretVerse#12 { Tanti dolci momenti al nostro amore }
    \livretVerse#12 { Cruda barbara legge? }
    \livretPers Armida
    \livretVerse#12 { Il mio timore. }
    \livretPers Rinaldo
    \livretVerse#12 { Timor? Di che? }
    \livretPers Armida
    \livretVerse#12 { Del tuo, del mio riposo. }
    \livretPers Rinaldo
    \livretVerse#12 { E chi potrai turbarlo in questa, o cara, }
    \livretVerse#12 { Separata dal mondo ignota sponda? }
    \livretPers Armida
    \livretVerse#12 { Numi! Il guardo del sole, i venti, l'onda… }
    \livretVerse#12 { Ah tutto a chi ben ama }
    \livretVerse#12 { È cagion di timor. Per nostro asilo }
  }
  \null
  \column {
    \livretVerse#12 { Quest' isola felice io scelsi in vano }
    \livretVerse#12 { In grembo all' Oceano. In van le cinsi }
    \livretVerse#12 { Di fosca nebbia il piede, e i fianchi, e il tergo }
    \livretVerse#12 { Di dirupate orride balze, aggiunsi }
    \livretVerse#12 { Folta guardia di mostri, e d'insidiose }
    \livretVerse#12 { Sirene allettatrici! I frutti, i fonti }
    \livretVerse#12 { Di tosco aspersi, e quanto miri in lei }
    \livretVerse#12 { E gli augelli, e le piante, e l'onda, e il vento }
    \livretVerse#12 { Tutto è in nostra difesa, eppur pavento. }
    \livretVerse#12 { Ah dal dì ch'io cangiai }
    \livretVerse#12 { Perte, bell'idol mio, gli affetti miei; }
    \livretVerse#12 { Patria, regno, tesor, tutto perdei; }
    \livretVerse#12 { Pensa, che s'io ti perdo, }
    \livretVerse#12 { Fuor che il rossor della mia fè tradita, }
    \livretVerse#12 { Nulla mi resta. }
    \livretPers Rinaldo
    \livretVerse#12 { Ah non temer, mia vita. }
    \livretVerse#12 { Sai che la mia tu sei, }
    \livretVerse#12 { Come io son l'alma tua; che non poss'io }
    \livretVerse#12 { Viver da te diviso un sol momento; }
    \livretVerse#12 { Sai che ogni mio contento, }
    \livretVerse#12 { Ogni mia speme è in te, ch' altro non bramo, }
    \livretVerse#12 { Che col tuo dolce nome il fiato estremo }
    \livretVerse#12 { Spirar frà labbri tuoi. }
    \livretPers Armida
    \livretVerse#12 { Lo so: ma tremo. }
  }
  \null
}

       #}))
