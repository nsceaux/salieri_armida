\tag #'(basse) {
\clef "soprano/treble" <>^\markup\character Armida
r4 r8 do'' do'' fa'
\ffclef "soprano/treble" <>^\markup\character Rinaldo
do''8 fa''16 do'' |
re''8 re''
\ffclef "soprano/treble" <>^\markup\character Armida
sib'4 do''8 re'' re'' la' |
la' la'16 la' la'8 sib' do'' do'' r do''16 mib'' |
do''4 do''8 sib' \grace la' sol'4
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r16 re'' re'' re'' |
si'!8 si' r sol'16 sol' sol'4 sol'8 la' |
si'! si' r si'16 do'' re''4 do''8 si' |
sold'8 sold' r16 sold' sold' la' si'8 si' r4 |
mi''8 si' re'' re''16 si' do''8 do''
\ffclef "soprano/treble" <>^\markup\character Armida
r16 do'' si' do'' |
la'8 la'
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r8 la' dod''4 r8 la' |
re''4
\ffclef "soprano/treble" <>^\markup\character Armida
r8 fa'' fa''4 si'! |
r8 re'' fa'' mib'' do'' do''
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r8 do'' |
do'' do'' do'' re'' mib'' mib'' r16 mib'' re'' mib'' |
do''8 do'' r do''16 re'' mib''4 do''8 sib' |
lab' lab' r16 lab' sib' fa' sol'8 sol'
\ffclef "soprano/treble" <>^\markup\character Armida
sol''8 mib'' |
r sib' \tuplet 3/2 { sib' sib' sib' } sol' sol' r sol' |
sib' sib' \ficta mib'' sib' mib''!2 |
reb''8 do'' sib' lab' sol' sol' r sol'16 lab'! |
sib'4 sol'8 lab' fa'4 r16 do'' do'' do'' |
la'!8 la' r fa' la' la' la' sib' |
do'' do'' r16 do'' do'' fa'' fa''8 do'' r do'' |
mib'' mib'' \sug mib'' reb'' sib' sib' r16 sib' sib' do'' |
reb''8 reb'' r reb'' reb'' sib' sib' lab' |
sol' sol' r sol' sib' sib' r mib'' |
mib'' sib' r16 sib' sib' do'' reb''8 reb'' r4 |
reb''4 reb''8 dob'' sold'8 sold' r sold' |
si' si' r si'16 si' si'4 si'8 do'' |
la'8 la' r16 la' la' la' fad'4 fad'8 sold' |
la' la' r16 la' la' si' sold'8 sold' r sold' |
si'! si' r mi'' mi'' si' r16 si' si' si' |
sold'8 sold' r sold' si' si' si' dod'' |
re'' re'' r re''16 re'' si'8 si' r si'16 si' |
sold'8 sold' r sold' si' si' r mi'' |
mi''8 si' r si'16 si' si'4 si'8 do''! |
re'' re'' r4 r8 re'' re'' do'' |
la' la' r4 la' r8 la' |
la'4 la'8 si' do'' do'' r mi'' |
do'' do'' do'' si' do'' do'' r16 do'' si' do'' |
la'8 la' r4 do''8 do'' r4 |
mi''8 mi'' r sol'' \grace sol''8 sib'4 do''8 sol'16 la' |
fa'4 r do''8 do''16 do'' do''8 fa'' |
fa'' do'' do'' do''16 do'' la'4 r8 la' |
la' la' la' sib' do'' do'' fa'' do''16 re'' |
sib'8 sib' r4
\ffclef "soprano/treble" <>^\markup\character Rinaldo
re''8 re''16 re'' do''8 re'' |
sib'8 sib' r4 sib'8 sib'16 sib' do''8 re'' |
re'' la' la' la'16 sib' do''4 do''8 re'' |
sib' sib' r4 r8 re'' re'' re'' |
si'! si' si' si'16 si' si'8 do'' re'' re'' |
r re'' fa'' mib'' do'' do'' r4 do''8 do'' r do'' |
do'' do'' do'' re'' mib'' mib'' mib'' re''16 mib'' |
do''8 do'' r do'' \ficta \grace sib' la'4 la'8 la'16 re'' |
re''8 la' r la' la' la' la' sib' |
do'' do'' r16 do'' do'' sib' do''8 do'' mib''4 |
do'' r16 do'' do'' sib' sol'8 sol'
}
\ffclef "soprano/treble" <>^\markup\character Armida
r8 sol' |
dod''4 r r2 |
r4 r8 re'' re'' la' r4 |
R2.*3 |
