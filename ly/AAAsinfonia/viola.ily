\clef "alto" do2.(\pp do'4) |
sib( lab sol fa) |
mib2 re4.\trill mib16 re |
do8 do4 re mib fa8 |
sol1~ |
sol~ |
sol |
sol4 sol sol fa |
mib do do2~ |
do2. sol4~ |
sol2 sib |
lab sol |
lab sol |
fad4 sol lab fad |
\ru#4 { sol8.[( lab16) sol8.( lab16)] } |
sol4 r r si |
do'4.( mib8) \once\slurDashed re4.( mib8) |
re4 r mib2 |
re2 mib |
re\fermata r4 r8 sol16\ff la32 si |
<do do'>4\fermata r8 sol16 la32 si <do do'>4\fermata r8 sol16 la32 si |
do'8 do32 re mi fa sol8 sol16 la32 si do'8 do'16 re'32 mi' fa'8 sol'32 sol' la' si' |
do''2 sib |
la r4 r8 la16 si32 dod' |
re'4\fermata r8 re32 mi fa sol la4\fermata r8 la16 si32 dod' |
re'8 re'32 mi' fa' sol' la'8 la'16 si'32 dod'' re''2( |
do'!) si\fermata |
r4 r8 si16 dod'32 red' mi'4\fermata r8 mi32 fad sol la |
si4\fermata r8 si16 dod'32 red' mi'8 mi'32 fad' sol' la' si'8 si'16 dod''32 red'' |
mi''2 mi' |
re'1 |
<do sol mi' do''>4-\sug\ff r8 do'-\sug\p \grace re'16 do'8 si16 do' \grace re' do'8 si16 do' |
si la sol la si do' re' mi' fa' mi' re' mi' re' mi' fa' sol' |
la' fa' mi' fa' fa' re' dod' re' re' si la si si sol la si |
<do sol mi' do''>4\ff r8 do'\p \grace re'16 do'8 si16 do' \grace re' do'8 si16 do' |
si la sol la si do' re' mi' fa' mi' re' mi' re' mi' fa' sol' |
la' fa' mi' fa' fa' re' dod' re' re' si la si si sol la si |
do'-\sug\ff \ru#7 sol' sol' \ru#7 <do' do> |
si2:16 si:16 |
<la re>4 r <la re' la'>4 r |
<sol re' si'>4 r q q |
<la re' la'> r q r |
<sol re' si'> r r2 |
re'4-!-\sug\ff si-! re'-! si-! |
dod'4 r r2 |
dod'4-!-\sug\ff sib-! dod'-! sib-! |
dod'4 r r2 |
dod'4-!-\sug\ff la-! dod'-! la-! |
si! r r2 |
si!4-\sug\ff lab si lab |
si r r2 |
si4-!-\sug\ff sol-! si-! sol-! |
la! r r2 |
la4-!-\sug\ff fad-! la-! fad-! |
sol r r2 |
sol'2:16-\sug\ff sol':16 |
<re sib fa'>4 q q q |
fa'2:16 fa':16 |
<mib sib sol'>4 q q q |
mib2:16 mib:16 |
re8 re mib\p mib re re mib mib |
re re sol\f sol fa fa mib mib |
re re mib\p mib re re mib mib |
re re sol\f sol fa fa mib mib |
re re sol sol fa fa mib mib |
re re sol sol fa fa mib mib |
re4 r r2 |
r solb4\p r |
fa4 r r2 |
r fa4 r |
mib r r2 |
r mib4 r |
re r r2 |
r fa4 r |
solb solb solb solb |
fad-- fad-- fad-- fad-- |
\ru#2 { fad?-- \ru#3 fad-- | }
mi!-- \ru#7 mi-- |
mi mi mi mi |
\ru#12 fad |
\ru#3 { fad\fermata r } r2 |
mi'2:16\p\cresc mi':16 |
fad':16\f fad':16 |
<sol' si>4\ff r <si sol'>4 r |
sol' r r sol' |
fad' r r2 |
red'4-.-\sug\ff do'!-. red'-. do'-. |
red'-. r r2 |
red'4-.-\sug\ff si-. red'-. si-. |
si r r2 |
sol'4-\sug\ff mi' sol' mi' |
sol' r r2 |
fa'!4-\sug\ff re' fa' re' |
do'4 r r8 do do4( |
re) r r8. re16 re8.\trill do32 re |
mi4 r r8. mi16 mi8. re32 mi |
fa8. fa16 fa8.\trill mi32 fa sol8. sol16 sol8.\trill fa32 sol |
la8. la16 la8.\trill sol32 la si8. si16 si8.\trill la32 si |
do'4 do' do' dod' |
re'2 red' |
mi' mi' |
fa' fad' |
sol'1~ |
sol' |
sol~ |
sol~ |
sol~ |
sol~ |
sol2\fermata r |
do'4_\markup { \dynamic p \italic pizzicato } r r |
r do' do |
R2. |
r4 sol' sol |
do' r r |
r do' do |
r r sol |
<>_\markup\italic {  coll‘ arco } <<
  { mi'2. |
    fa'4( re'2) |
    mi'2. |
    fa'4( re'2) |
    mi'2 la'4 |
    sol'2 fa'4 |
    mi'2 la'4 |
    sol'2 fa'4 |
    mi'\trill mi'\trill mi'\trill |
    mi'2 } \\
  { do'2. |
    re'4( si2) |
    do'2. |
    re'4( si2) |
    do'2 fa'4 |
    mi'2 re'4 |
    do'2 fa'4 |
    mi'2 re'4 |
    do' do' do' |
    do'2 }
>> r4 |
