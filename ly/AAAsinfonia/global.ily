\tag #'all \key do \minor
\tempo "Adagio con un poco di moto" \midiTempo#120
\time 4/4 s1*21 \bar "||"
\tempo "Allegro" \midiTempo#120
\key do \major
s1*11
\tempo "Allegro assai" s1*12
\tempo "più presto" s1*71 \bar "||"
\tempo "Andantino grazioso" \midiTempo#80
\time 3/4 s2.*17 \bar "|."
\endMark "Segue subito il coro"
