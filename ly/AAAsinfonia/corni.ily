\clef "treble" R1*32 |
<>-\sug\ff <<
  \tag #'(primo tutti) do''2
  \tag #'(secondo tutti) mi'
>> r2 |
R1*2 |
<>-\sug\ff <<
  \tag #'(primo tutti) do''2
  \tag #'(secondo tutti) mi'
>> r2 |
R1*2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''2 s |
    re'' s |
    do''4 s re''4 s |
    re'' s re'' re'' |
    do'' s re'' s |
    re'' s2. |
    re''4-! re''-! re''-! re''-! |
    mi'' s2. |
    mi''4 mi'' mi'' mi'' |
    mi'' s2. |
    mi''4 mi'' mi'' mi'' |
    re'' s2. |
    re''4 re'' re'' re'' |
    re'' s2. |
    re''4 re'' re'' re'' |
    do'' s2. |
    do''4 do'' do'' do'' |
    sol' s2. | }
  { do''2 s |
    sol' s |
    sol'4 s re'' s |
    sol' s sol' sol' |
    sol' s re'' s |
    sol' s2. |
    re''4-! re''-! re''-! re''-! |
    mi'' s2. |
    mi''4 mi'' mi'' mi'' |
    mi'' s2. |
    mi''4 mi'' mi'' mi'' |
    re'' s2. |
    re''4 re'' re'' re'' |
    sol' s2. |
    sol'4 sol' sol' sol' |
    do'' s2. |
    do''4 do'' do'' do'' |
    sol' s2. | }
  { s2 r | s r | s4 r s r |
    s4 r s2 |
    s4 r s r |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 | }
>>
R1*33 |
\twoVoices #'(primo secondo tutti) <<
  { re''4 s re'' s |
    re'' s sol' s |
    re'' }
  { sol'4 s sol' s |
    sol' s sol' s |
    re'' }
  { s4-\sug\ff r s r |
    s r s r | }
>> r4 r2 |
R1*4 |
\twoVoices #'(primo secondo tutti) <<
  { sol'4 sol' sol' sol' |
    sol' s2. |
    sol'4 sol' sol' sol' |
    do''4 s2. |
    re''4 s2. |
    mi''4 s2. | }
  { sol'4 sol' sol' sol' |
    sol' s2. |
    sol'4 sol' sol' sol' |
    mi'4 s2. |
    re''4 s2. |
    mi''4 s2. | }
  { s1-\sug\ff |
    s4 r r2 |
    s1-\sug\ff |
    s4 r r2 |
    s4 r r2 |
    s4 r r2 | }
>>
R1*12 R1^\fermataMarkup |
R2.*7 |
\twoVoices #'(primo secondo tutti) <<
  { mi''8 s mi'' s mi'' s |
    re'' s re'' s re'' s |
    mi'' s mi'' s mi'' s |
    re'' s re'' s re'' s |
    do''2. |
    do''2 re''4 |
    mi''2 do''4 |
    do''2 re''4 |
    mi'' mi'' mi'' |
    mi''2 }
  { do''8 s do'' s do'' s |
    sol' s sol' s sol' s |
    do'' s do'' s do'' s |
    sol' s sol' s sol' s |
    do''2. |
    do''2 sol'4 |
    do''2. |
    do''2 sol'4 |
    sol' sol' sol' |
    sol'2 }
  { s8 r s r s r |
    s8 r s r s r |
    s8 r s r s r |
    s8 r s r s r | }
>> r4 |
