<<
  \tag #'(primo primo-secondo) \clef "alto"
  \tag #'secondo \clef "tenor"
  \tag #'terzo \clef "bass"
>>
R1*32 |
<>-\sug\ff <<
  \tag #'primo sol'2
  \tag #'primo-secondo << sol'2 \\ mi' >>
  \tag #'secondo mi'2
  \tag #'terzo do'2
>> r2 |
R1*2 |
<>-\sug\ff <<
  \tag #'primo sol'2
  \tag #'primo-secondo << sol'2 \\ mi' >>
  \tag #'secondo mi'2
  \tag #'terzo do2
>> r2 |
R1*2 |
<>-\sug\ff <<
  \tag #'(primo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceOne
    sol'2 s |
    sol' s |
    sol'4 s la' s |
    si'4 s si' si' |
    la' s la' s |
    si' s2. |
    fa'4 fa' fa' fa' |
    mi' s2. |
    mi'4 mi' mi' mi' |
    mi' s2. |
    mi'4 mi' mi' mi' |
    re' s2. |
    re'4 re' re' re' |
    re' s2. |
    re'4 re' re' re' |
    mib' s2. |
    mib'4 mib' mib' mib' |
    re' s2. |
    sib'1 |
    lab'4 s2. |
    lab'1 |
    sol'4 s2. |
    sol'1 |
    fa'4 s2. |
    s4 sib'( lab' sol') |
    fa'4 s2. |
    s4 sib'( lab' sol') |
    fa'8 s sib'4( lab' sol') |
    fa'8 s sib'4( lab' sol') |
    fa'4 s2. |
  }
  \tag #'(secondo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceTwo
    mi'2 s |
    re' s |
    do'4 s re' s |
    re'4 s re' re' |
    re' s re' s |
    re' s2. |
    re'4 re' re' re' |
    dod' s2. |
    dod'4 dod' dod' dod' |
    dod' s2. |
    dod'4 dod' dod' dod' |
    si s2. |
    si4 si si si |
    si s2. |
    si4 si si si |
    do' s2. |
    do'4 do' do' do' |
    si s2. |
    sib1 |
    sib?4 s2. |
    sib1 |
    sib?4 s2. |
    sib1 |
    sib?4 s2. |
    s4 sib2 sib4 |
    sib s2. |
    s4 sib2 sib4 |
    sib8 s sib2 sib4 |
    sib8 s sib2 sib4 |
    sib4 s2. |
  }
  \tag #'terzo {
    do'2 s |
    si s |
    la4 s re s |
    sol4 s si sol |
    la s re s |
    sol s2. |
    si,4 re si, re |
    sib, s2. |
    sib,4 dod sib, dod |
    la, s2. |
    la,4 dod la, dod |
    lab, s2. |
    lab,4 si,! lab, si, |
    sol, s2. |
    sol,4 si, sol, si, |
    fad, s2. |
    fad4 la fad la |
    sol s2. |
    sol1 |
    fa4 s2. |
    re1 |
    mib4 s2. |
    mib1 |
    re4 s2. |
    s4 sol4( fa mib) |
    re4 s2. |
    s4 sol( fa mib) |
    re8 s sol4( fa mib) |
    re8 s sol4( fa mib) |
    re4 s2. |
  }
  { s2 r |
    s r |
    s4 r s r |
    s4 r s2 |
    s4 r s r |
    s r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1 | s4 r r2 |
    s1 |
    s4 r r2 |
    r4 s2.-\sug\ff |
    s4 r r2 |
    r4 s2.-\sug\ff |
    s8 r s2. |
    s8 r s2. |
    s4 r r2 | }
>>
R1*24
<<
  \tag #'(primo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceOne
    fad'4 fad' fad' fad' |
    fad' s2. |
    fad'4 fad' fad' fad' |
    sol' s2. |
    si'4 si' si' si' |
    si' s2. |
    si'4 si' si' si' |
  }
  \tag #'(secondo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceTwo
    red'4 red' red' red' |
    red' s2. |
    red'4 red' red' red' |
    mi' s2. |
    sol'4 sol' sol' sol' |
    sol'4 s2. |
    sol'4 sol' sol' sol' |
  }
  \tag #'terzo {
    do!4 red do red |
    si, s2. |
    si,4 red si, red |
    mi s2. |
    mi4 sol mi sol |
    re s2. |
    re4 fa! re fa |
  }
  { s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | s4 r r2 |
    s1-\sug\ff | }
>>
<<
  \tag #'(primo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceOne
    do''1~ |
    do''~ |
    do''~ |
    do'' |
    do'2 re' |
    mi'4
  }
  \tag #'(secondo primo-secondo) \new Voice {
    \tag #'primo-secondo \voiceTwo
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'2 re' |
    mi'4
  }
  \tag #'terzo {
    do4 r r2 |
    re4 r r2 |
    mi4 r r2 |
    fa4 r sol r |
    la r si r |
    do'4
  }
>> r4 r2 |
R1*9 R1^\fermata |
R2.*17 |
