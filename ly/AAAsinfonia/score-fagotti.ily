\score {
  \new StaffGroup \with { \haraKiriFirst } <<
    \new Staff \with { shortInstrumentName = "Fg." } <<
      { s1*32\break <>^"Fagotti"
        s1*56\break <>^"Fagotti"
        s1*27 s2.*7\break \grace s8 <>^"Fagotti"
      }
      \global \keepWithTag #'tutti \includeNotes "fagotti"
    >>
    \new Staff \with { shortInstrumentName = "B." } <<
      { <>^"Bassi." s1*32
        \startHaraKiri s1*6 s1*30\break
        \stopHaraKiri <>^"Bassi" s1*20 \startHaraKiri s1*16
        \stopHaraKiri\break <>^"Bassi"
      }
      \global \keepWithTag #'primo \includeNotes "bassi"
    >>
    \new Staff \with { shortInstrumentName = "B." } <<
      { \startHaraKiri s1*32
        \stopHaraKiri <>^"Bassi" s1*6\break
        \startHaraKiri }
      \global \keepWithTag #'secondo \includeNotes "bassi"
    >>
  >>
  \layout { short-indent = 5\mm }
}
