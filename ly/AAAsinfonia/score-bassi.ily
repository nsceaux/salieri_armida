\score {
  \new StaffGroup \with { \haraKiriFirst } <<
    \new Staff \with {
      instrumentName = \markup\center-column { Violoncelli Basso }
    } <<
      \global \keepWithTag #'primo \includeNotes "bassi"
      { s1*18 <>^"Vlc." }
    >>
    \new Staff <<
      \global \keepWithTag #'secondo \includeNotes "bassi"
      { \startHaraKiri s1*18
        \stopHaraKiri <>^"Basso" s1*3 \break
        \startHaraKiri s1*11 \break
        \stopHaraKiri s1*12 \break
        \startHaraKiri s1*13 \break
        \stopHaraKiri s1*4
        \startHaraKiri s1*54 \break
        s2.*6 \stopHaraKiri }
    >>
  >>
  \layout { indent = \largeindent }
}
