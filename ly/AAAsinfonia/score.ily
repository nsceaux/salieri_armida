\score {
  <<
    \new StaffGroup \with { \haraKiri } <<
      \new Staff \with { \flautiInstr } <<
        { s1*115 s2.\noHaraKiri }
        \global \keepWithTag #'tutti \includeNotes "flauti"
      >>
      \new Staff \with { \oboiInstr  } <<
        { s1*100\noHaraKiri }
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniCInstr } <<
        { s1*100\noHaraKiri }
        \keepWithTag #'() \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        { s1*100\noHaraKiri }
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff <<
          { s1*100\noHaraKiri s1*14\revertNoHaraKiri }
          \global \keepWithTag #'primo-secondo \includeNotes "tromboni"
        >>
        \new Staff \with { \haraKiriFirst } <<
          { s1*100\noHaraKiri s1*14\revertNoHaraKiri }
          \global \keepWithTag #'terzo \includeNotes "tromboni"
        >>
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
      \new Staff \with { \vlcBInstr \haraKiri } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        { s1*32
          \startHaraKiri s1*12
          \stopHaraKiri s1*12
          \startHaraKiri s1*6
          \stopHaraKiri s1*53
          \startHaraKiri
        }
      >>
      \new Staff \with { \vlcInstr \haraKiriFirst } <<
        \global \keepWithTag #'primo \includeNotes "bassi"
        \modVersion {
          \startHaraKiri s1*32
          \stopHaraKiri s1*12
          \startHaraKiri s1*12
          \stopHaraKiri s1*6
          \startHaraKiri s1*53
          \stopHaraKiri
        }
      >>
      \new Staff \with { \bassoInstr \haraKiriFirst } <<
        \global \keepWithTag #'secondo \includeNotes "bassi"
        \modVersion {
          \startHaraKiri s1*32
          \stopHaraKiri s1*12
          \startHaraKiri s1*12
          \stopHaraKiri s1*6
          \startHaraKiri s1*53
          \stopHaraKiri
        }
        \modVersion {
          s1*32\break
          %% Allegro assai
          s1*12\pageBreak
          \ru#11 { s1\noPageBreak } s1\pageBreak
          s1*11\pageBreak
          s1*17\pageBreak
          s1*11\pageBreak
          \ru#9 { s1\noPageBreak } s1\pageBreak
          s1*10\pageBreak
          %% Andantino grazioso
          \ru#9 { s2.\noPageBreak } s2.\pageBreak
          \grace s8
        }
        \origLayout {
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 5
          s1*3 s2\bar "" \pageBreak
          s2 s1*3\pageBreak
          s1*3\pageBreak
          s1*3\pageBreak
          s1*3\pageBreak
          %% 10
          s1*3\pageBreak
          s1*3\pageBreak
          s1*3\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 15
          s1*4\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 20
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4 s2 \bar "" \pageBreak
          s2 s1*4\pageBreak
          %% 25
          s1*5\pageBreak
          s1*5\pageBreak
          s1*6\pageBreak
          s1*5 s2.*2\pageBreak
          s2.*6\pageBreak
          %% 30
          \grace s8 s2.*4\pageBreak
          s2.*5
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
