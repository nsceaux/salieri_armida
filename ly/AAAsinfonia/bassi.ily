\clef "bass" do2.(\pp do'4) |
sib( lab sol fa) |
mib2 re4.\trill mib16 re |
do8 do4 re mib fa8 |
sol4 sol, sol,2~ |
sol,1~ |
sol, |
sol,4 fa( mib re) |
do do, do,2~ |
do,1~ |
do,2 sol, |
lab,! sol, |
lab, sol, |
fad,4( sol, lab,! fad,) |
sol,4 r r2 |
R1 |
sol,2 r |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { sol,4 r do2 | si, do | si,\fermata }
  { sol,1~ | sol,~ | sol,2\fermata }
>> r4 r8 sol,16\ff la,32 si, |
do4\fermata r8 sol,16 la,32 si, do4\fermata r8 sol,16 la,32 si, |
do8 do32 re mi fa sol8 sol16 la32 si do'8 do'32 re' mi' fa' sol'8 sol16 la32 si |
do'2 sib |
la r4 r8 la,16 si,32 dod |
re4\fermata r8 re32 mi fa sol la4\fermata r8 la16 si32 dod' |
re'8 re32 mi fa sol la8 la16 si32 dod' re'2 |
do'! si\fermata |
r4 r8 si,16 dod32 red mi4\fermata r8 mi32 fad sol la |
si4\fermata r8 si16 dod'32 red' mi'8 mi32 fad sol la si8 si16 dod'32 red' |
mi'2 mi |
re1 |
\twoVoices #'(primo secondo tutti) <<
  { <do, sol, mi do'>4\ff r do\p do | }
  { do16\ff do do do do4:16-\sug\p do:16 do:16 | }
>>
si,16 la, sol, la, si, do re mi fa mi re mi re mi fa sol |
la4 fa re si,16 sol, la, si, |
\twoVoices #'(primo secondo tutti) <<
  { <do, sol, mi do'>4\ff r8 do-\sug\p do4 do | }
  { do16\ff do do do do4:16-\sug\p do:16 do:16 | }
>>
si,16 la, sol, la, si, do re mi fa mi re mi re mi fa sol |
la4 fa re si,16 sol, la, si, |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi16 do' do' do' }
  { do16 do' do' do' }
>> do'4:16 do'2:16 |
si:16 si:16 |
\twoVoices #'(primo secondo tutti) <<
  { <la re>4 r <la, re la> r |
    <sol, re si> r q q |
    <la, re la> r q r |
    <sol, re si> r r2 | }
  { la4 r re r |
    sol r si sol |
    la r re r |
    sol r r2 | }
>>
si4-!\ff re'-! si-! re'-! |
sib r r2 |
sib4-!-\sug\ff dod'-! sib-! dod'-! |
la-! r r2 |
la4-!-\sug\ff dod'-! la-! dod'-! |
lab r r2 |
lab4-! si!-! lab-! si-! |
sol-! r r2 |
sol4-!-\sug\ff si-! sol-! si-! |
fad4 r r2 |
fad?4-!-\sug\ff la-! fad-! la-! |
sol r r2 |
sol2:16\ff sol:16 |
\twoVoices #'(primo secondo tutti) <<
  { fa4 <fa sib, re,>4 q q |
    re2:16 re:16 |
    mib4 <sol sib, mib,>4 q q |
    mib2:16 mib:16 | }
  { fa8 fa fa fa fa2:8 |
    re16 re re re re4:16 re2:16 |
    mib8 mib mib mib mib2:8 |
    mib16 mib mib mib mib4:16 mib2:16 | }
>>
re8 re mib\p mib re re mib mib |
re re sol\ff sol fa fa mib mib |
re re mib\p mib re re mib mib |
re re sol\ff sol fa fa mib mib |
\ru#2 { re re sol sol fa fa mib mib | }
re4 r r2 |
r2 solb4\p r4 |
fa4 r r2 |
r fa4 r |
mib r r2 |
r mib4 r |
re r r2 |
r re4 r |
mib mib mib mib |
red red red red |
red? red red red |
red? red red red |
dod dod dod dod |
dod? dod dod dod |
dod dod dod dod |
red red red red |
red red red red |
red red red red |
\ru#3 { red\fermata r } r2 |
mi8\p\cresc mi mi mi mi mi mi mi |
fad\f fad fad fad fad2:8 |
sol4\ff r sol r |
sol r r mi |
re! r r2 |
do!4-.\ff red-. do-. red-. |
si,-. r r2 |
si,4-.\ff red-. si,-. red-. |
mi r r2 |
mi4-.\ff sol-. mi-. sol-. |
re r r2 |
re4\ff fa! re fa |
do4 r r8. do16 do8.\trill si,32 do |
re4 r r8. re16 re8.\trill do32 re |
mi4 r r8. mi16 mi8.\trill re32 mi |
fa8. fa16 fa8.\trill mi32 fa sol8. sol16 sol8.\trill fa32 sol |
la8. la16 la8.\trill sol32 la si8. si16 si8.\trill la32 si |
do'4 do'_\markup { \dynamic mf \italic stanchi } do' dod' |
re'2 red' |
mi' mi' |
fa' fad' |
sol'1\p~ |
sol' |
sol,~ |
sol,~ |
sol,~ |
sol,~ |
sol,2 r |
do'4_\markup { \dynamic p \italic pizzicato } r r |
r do' do |
R2. |
r4 sol sol, |
do r r |
r do' do |
\twoVoices #'(primo secondo tutti) <<
  { \clef "tenor" R2. |
    <>^\markup\italic Solo
    \ru#3 { \grace la'16 sol'32 fad' sol'8. } |
    sol'2.\trill |
    \ru#3 { \grace la'16 sol'32 fad' sol'8. } |
    sol'2.\trill |
    \clef "bass" do'4 r r | }
  { r4 r sol |
    do'4 r r |
    sol4 r r |
    do' r r |
    sol r r |
    do r r | }
>>
sol4 r r |
do r r |
sol r r |
do do do |
do2 r4 |
