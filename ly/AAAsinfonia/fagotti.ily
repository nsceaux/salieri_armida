\clef "bass" R1*32 |
do2-\sug\ff r |
R1*2 |
do2-\sug\ff r |
R1*2 |
do2-\sug\ff r |
si, r |
la,4 r re, r |
sol, r si, sol, |
la, r re, r |
sol, r r2 |
si,4-!-\sug\ff re-! si,-! re-! |
sib, r4 r2 |
sib,4-\sug\ff dod sib, dod |
la, r r2 |
la,4-\sug\ff dod la, dod |
lab, r r2 |
lab,4-\sug\ff si,! lab, si, |
sol, r r2 |
sol,4-\sug\ff si, sol, si, |
fad, r r2 |
fad4-\sug\ff la fad la |
sol r r2 |
sol1-\sug\ff |
fa2:8 fa:8 |
re1 |
mib8 mib mib mib mib2:8 |
mib1 |
re4 r r2 |
r4 sol8(-\sug\ff sol fa fa) mib?( mib) |
re4 r4 r2 |
r4 sol(-\sug\ff fa mib?) |
re8 r sol4( fa mib) |
re8 r sol4( fa mib) |
re4 r r2 |
R1*20 |
fad2:8\f fad:8 |
sol4-\sug\ff r sol r |
sol-\sug\ff r r mi |
re r r2 |
do!4-.\ff red-. do-. red-. |
si, r4 r2 |
si,4-.\ff red-. si,-. red-. |
mi4 r r2 |
mi4-\sug\ff sol mi sol |
re r r2 |
re4-\sug\ff fa! re fa |
do r r8. do16 do8.\trill si,32 do |
re4 r r8. re16 re8.\trill do32 re |
mi4 r r8. mi16 mi8.\trill re32 mi |
fa8. fa16 fa8.\trill mi32 fa sol8. sol16 sol8.\trill fa32 sol |
la8. la16 la8.\trill sol32 la si8. si16 si8.\trill la32 si |
do'4 r r2 |
R1*9 R1^\fermataMarkup |
\clef "tenor"
R2.*7 |
\twoVoices #'(primo secondo tutti) <<
  { mi'2. |
    fa'4( re'2) |
    mi'2. |
    fa'4( re'2) |
    mi'2 fa'4 |
    sol'2 fa'4 |
    mi'2 fa'4 |
    sol'2 fa'4 |
    mi' mi' mi' |
    mi'2 }
  { do'2. |
    re'4( si2) |
    do'2. |
    re'4( si2) |
    do'2 fa'4 |
    mi'2 re'4 |
    do'2 re'4 |
    mi'2 re'4 |
    do' do' do' |
    do'2 }
>> r4 |
