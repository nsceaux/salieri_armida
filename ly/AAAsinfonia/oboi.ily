\clef "treble" R1*21 R1*11 |
<>\ff <<
  \tag #'(primo tutti) { do'''2 }
  \tag #'(secondo tutti) { mi''2 }
>> r2 |
R1*2 |
<>\ff <<
  \tag #'(primo tutti) { do'''2 }
  \tag #'(secondo tutti) { mi'' }
>> r |
R1*2 |
<>\ff <<
  \tag #'(primo tutti) {
    do'''2 s |
    sol'' s |
    fad''4 s la'' s |
    si'' s si'' si'' |
    do''' s la' s |
    si'
  }
  \tag #'(secondo tutti) {
    mi''2 s |
    re'' s |
    re''4 s re'' s |
    re'' s re'' re'' |
    re'' s re' s |
    re'
  }
  { s2 r |
    s r |
    s4 r s r |
    s r s2 |
    s4 r s r | }
>> r4 r2 |
<>-\sug\ff \ru#4 { lab''8.( fa''16) } |
sol''4 r r2 |
<>-\sug\ff \ru#4 { sol''8.( mi''16) } |
sol''4 r r2 |
<>-\sug\ff \ru#4 { sol''8.( mi''16) } |
fa''4 r r2 |
<>-\sug\ff \ru#4 { fa''8.( re''16) } |
fa''4 r r2 |
<>-\sug\ff \ru#4 { fa''8.( re''16) } |
mib''4 r r2 |
<>-\sug\ff \ru#4 { la''8.( do''16) } |
si'!4 r4 r2 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''2:8 sib'':8 |
    lab''4 s2. |
    lab''1 |
    sol''4 s2. |
    sol''1 |
    fa''4 s2. |
    s4 sib''( lab'' sol'') |
    fa''4 s2. |
    s4 sib''( lab'' sol'') |
    fa''8-! s sib''4( lab'' sol'') |
    fa''8-! s sib''4( lab'' sol'') |
    fa''4 }
  { sib'1 |
    sib'?4 s2. |
    sib'1 |
    sib'?4 s2. |
    sib'1 |
    sib'?4 s2. |
    s4 sib'2 sib'4 |
    sib' s2. |
    s4 sib'2 sib'4 |
    sib'8 s sib'2 sib'4 |
    sib'8 s sib'2 sib'4 |
    sib'?4 }
  { s1 |
    s4 r r2 |
    s1 |
    s4 r r2 |
    s1 |
    s4 r r2 |
    r4 s2.-\sug\ff |
    s4 r r2 |
    r4 s2.-\sug\ff |
    s8 r s2. |
    s8 r s2. |
    s4 }
>> r4 r2 |
R1*21 |
<>-\sug\ff <<
  \tag #'(primo tutti) {
    si''4 s si'' s |
    si'' s s dod'' |
    re''
  }
  \tag #'(secondo tutti) {
    re''4 s re'' s |
    re'' s s sol' |
    fad'
  }
  { s4 r s r | s r r s | s }
>> r4 r2 |
<>\ff \ru#4 { la''8.( fad''16) } |
la''8( fad''?) r4 r2 |
<>\ff \ru#3 { si''8.( fad''16) } si'' la'' sol'' fad'' |
sol''4 r r2 |
<>\ff \ru#4 { si''8.( sol''16) } |
si''8( sol'') r4 r2 |
<>\ff \ru#4 { si''8.( sol''16) } |
do'''1~ |
do'''~ |
do'''~ |
do'''~ |
do'''2 re''' |
mi'''4 r r2 |
R1*9 R1^\fermataMarkup |
%%
<<
  \tag #'(primo tutti) {
    <>^\markup\italic Solo _\markup\italic dolce 
    sol''4 fa''8(-. mi''-. fa''-. sol''-.) |
    la''8. si''32 do''' sol''2 |
    sol''8( sold'' la'') mi''( fa'' sol'') |
    mi''4 re'' r |
    sol'' fa''8(-. mi''-. fa''-. sol'') |
    \once\slurDashed la''8.( si''32 do''' sol''2) |
    sol''4( fa''8 mi'') re''4\trill |
    do'' r4 r8 sol'' |
    \grace sol''8 fa''4. re''8( sol'' fa'') |
    mi''4 r r8 sol'' |
    \grace sol'' fa''4. re''8( sol'' si') |
    do''2 la''4 |
    sol''2 si'4 |
    do''2 la''4 |
    sol''2 si'4 |
    do''4\trill do''\trill do''\trill |
    do''2\trill r4 |
  }
  \tag #'secondo R2.*17
>>
