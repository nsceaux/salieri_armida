\clef "treble" R1*115 |
R2.*7 |
\twoVoices #'(primo secondo tutti) <<
  { mi''2. |
    fa''4 re''2 |
    mi''8( do''') si''( do''') si''( do''') |
    fa''4 re''2 |
    mi'' la''4 |
    sol''2. |
    sol'4( do'') la'' |
    sol''2. |
    mi''4\trill mi''\trill mi''\trill |
    mi''2\trill
  }
  { do''2. |
    re''4 si'2 |
    do''8( mi'') re''( mi'') re''( mi'') |
    re''4 si'2 |
    do'' fa''4 |
    sol''2. |
    sol'4( do'') fa'' |
    sol''2. |
    do''4\trill do''\trill do''\trill |
    do''2\trill
  }
>> r4 |
