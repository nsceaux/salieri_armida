\clef "treble"
<<
  \tag #'primo {
    R1*8 |
    do'2.(\pp do''4) |
    sib'4( lab' sol' fa') |
    mib'2 reb'4.\trill mib'16 reb' |
    do'8 do'4 do' do' do'8 |
    do' do'4 do' do' do'8 |
    do' do'4 do' do' do'8 |
    si2 r4 re'' |
    mib''4.( sol''8 si'4. do''8) |
    si'4 r r re' |
    mib'4.( sol'8) si4.( do'8) |
    si4 r sol2 |
    sol sol |
    sol\fermata
  }
  \tag #'secondo {
    R1*4
    sol2.(\pp sol'4) |
    fa' mib' re' do' |
    si2 lab!4.\trill sib16 lab |
    sol8 sol4 sol sol8 la si |
    do'2.( lab'4) |
    \once\slurDashed sol'4( fa' mib' re') |
    do'4 mib' mib'2 |
    mib'8 mib'4 mib' mi' mi'8 |
    fa' fa'4 fa'8 mib'! mib'4 mib'8 |
    re' re'4 re' re' re'8 |
    re'2 r4 si' |
    do''4.( mib'8 re'4. mib'8) |
    \ru#8 { sol8.( lab16) } |
    sol4 r r2 |
    sol2 r |
    sol\fermata
  }
>> r4 r8 sol16\ff la32 si |
do'4\fermata r8 sol16 la32 si do'4\fermata r8 sol16 la32 si |
do'8 do'32 re' mi' fa' sol'8 sol'16 la'32 si' do''8 do''32 re'' mi'' fa'' sol''8 sol''16 la''32 si'' |
do'''16( sol'' mi'' do'') sol''( mi'' do'' sol') mi''( do'' sol' mi') do''( sol' mi' do') |
la2 r4 r8 la16 si32 dod' |
re'4\fermata r8 re'32 mi' fa' sol' la'4\fermata r8 la'16 si'32 dod'' |
re''8 re''32 mi'' fa'' sol'' la''8 la''16 si''32 dod''' re'''16( la'' fad'' re'') la''( fad'' re'' la') |
fad''( re'' la' fad') re''( la' fad' re') si2\fermata |
r4 r8 si16 dod'32 red' mi'4\fermata r8 mi'32 fad' sol' la' |
si'4\fermata r8 si'16 dod''32 red'' mi''8 mi''32 fad'' sol'' la'' si''8 si''16 dod'''32 red''' |
mi'''16( si'' sol'' mi'') si''( sol'' mi'' si') sol''( mi'' si' sol') mi''( si' sol' mi') |
fa'!1\trill |
<sol mi' do'' mi''>4\ff r8 do'-\sug\p \grace re'16 do'8 si16 do' \grace re'16 do'8 si16 do' |
si la sol la si do' re' mi' fa' mi' re' mi' re' mi' fa' sol' |
la' fa' mi' fa' fa' re' dod' re' re' si la si si sol la si |
<sol mi' do'' mi''>4\ff r8 do'-\sug\p \grace re'16 do'8 si16 do' \grace re'16 do'8 si16 do' |
si la sol la si do' re' mi' fa' mi' re' mi' re' mi' fa' sol' |
la' fa' mi' fa' fa' re' dod' re' re' si la si si sol la si |
<<
  \tag #'primo {
    <sol mi' do''>4\ff r8 do'' mi''16 re'' do'' si' do'' re'' mi'' fa'' |
    <sol'' si'>4 r8 sol'' la''16 sol'' fad'' sol'' fad'' sol'' la'' si'' |
  }
  \tag #'secondo {
    do'16-\sug\ff <mi' do''> q q q q q q q <sol mi'> q q q4:16 |
    <sol re'>2:16 q8. sol''16 fad'' sol'' la'' si'' |
  }
>>
do'''16 si'' la'' si'' do''' si'' la'' si'' do''' si'' la'' sol'' fad'' mi'' re'' do'' |
si'( sol'') \ru#7 { si''( sol'') } |
do''' si'' la'' si'' do''' si'' la'' si'' do''' si'' la'' sol'' fad'' mi'' re'' do'' |
<<
  \tag #'primo {
    si'8( sol'') sol''-.\mf sol''-. sol''-. sol''-. sol''-. sol''-. |
    <>-\sug\ff \ru#4 { lab''8.( fa''16) } |
    sol''8(\mf mi'')-. mi''-.( mi''-. mi''-. mi''-. mi''-. mi''-.) |
    <>\ff \ru#4 { sol''8.( mi''16) } |
    sol''8(\mf mi'') mi''-.( mi''-. mi''-. mi''-. mi''-. mi''-.) |
    <>\ff \ru#4 { sol''8.( mi''16) } |
    fa''8(\mf re'') \once\slurDashed re''-.( re''-.) re''-.( re''-. re''-. re''-.) |
    <>\ff \ru#4 { fa''8.( re''16) } |
    fa''8(\mf re'') re''-.( re''-.) re''-.( re''-. re''-. re''-.) |
    <>\ff \ru#4 { fa''8.( re''16) } |
    mib''8(\mf do'') do''-.( do''-.) do''-.( do''-. do''-. do''-.) |
    <>\ff la''!8.( do''16) \ru#3 { la''8.( do''16) } |
    si'!8( sol'') sol''-. sol''-. sol''(-.\mf sol''-. sol''-. sol''-.) |
  }
  \tag #'secondo {
    si'4 r r2 |
    re''4-!-\sug\ff si'-! re''-! si'-! |
    dod''4-\sug\mf dod''8 dod'' dod''2:8 |
    dod''4-!-\sug\ff sib'-! dod''-! sib'-! |
    dod''4-\sug\mf dod''8 dod'' dod''2:8 |
    dod''4-!-\sug\ff la'-! dod''-! la'-! |
    si'!4-\sug\mf si'8 si' si'2:8 |
    si'!4-!-\sug\ff lab'-! si'-! lab'-! |
    si'4-\sug\mf si'8 si' si'2:8 |
    si'4-!-\sug\ff sol'-! si'-! sol'-! |
    la'!4-\sug\mf la'8 la' la'2:8 |
    la'4-!-\sug\ff fad'-! la'-! fad'-! |
    sol'4 r r2 |
  }
>>
<sib' sib''>2:16\ff q:16 |
<lab'' sib'? re'>4 <fa'' sib' re'> <re'' fa' sib> <sib' re'> |
<<
  \tag #'primo { <sib'? lab''>2:16 q:16 | }
  \tag #'secondo { <sib' fa''>2:16 q:16 | }
>>
<mib' sib' sol''>4 <mib'' sol'> <sib' mib'> <sol' sol> |
<sib' sol''>2:16 q:16 |
<sib' fa''>4:16 <sib' sol''>:16\p <sib' fa''>:16 <sib' sol''>:16 |
<sib' fa''>:16 <sib' sib''>:16\ff <sib' lab''>:16 <sib' sol''>:16 |
<sib' fa''>4:16 <sib' sol''>:16-\sug\p <sib' fa''>:16 <sib' sol''>:16 |
<sib' fa''>:16 <sib' sib''>:16\ff <sib' lab''>:16 <sib' sol''>:16 |
\ru#2 { <sib' fa''>:16 <sib' sib''>:16 <sib' lab''>:16 <sib' sol''>:16 | }
fa''8(\p sib') sib'-. sib'-. sib'-. sib'-. sib'-. sib'-. |
dob''( sib') sib'-. sib'-. solb'( sib') sib'-. sib'-. |
fa'( sib') sib'-. sib'-. sib'-. sib'-. sib'-. sib'-. |
dob''( sib') sib'-. sib'-. fa'( sib') sib'-. sib'-. |
mib'( sib') sib'-. sib'-. sib'-. sib'-. sib'-. sib'-. |
dob''( sib') sib'-. sib'-. mib'( sib') sib'-. sib'-. |
re'( sib') sib'-. sib'-. \ru#4 sib'-. |
dob''( sib') sib'-. sib'-. fa'( sib') sib'-. sib'-. |
solb'( sib') sib'-. sib'-. \ru#4 sib'-. |
lad'( si'!) si'-. si'-. \ru#4 si'-. |
si'8 red''4 fad'' si'' fad''8~ |
fad'' red''4 dod'' si' si'8 |
si'( lad') lad'-. lad'-. \ru#4 lad'-. |
lad'8 dod''4 fad'' sold'' lad''8~ |
lad'' sold''4 fad'' mi''8 red'' dod'' |
si' si'4 si' si' si'8~ |
si' red''4 fad'' sold'' la''!8~ |
la'' fad''4 red'' do''! si'8 |
la'!4\fermata r4 <<
  \tag #'primo {
    la'4\fermata r |
    la'\fermata r4 r2 |
    <si' sol''!>2:16\p\cresc q:16 |
    <re''! la''>2:16\f q:16 |
    si''16\ff la'' sol'' fad'' sol'' fad'' sol'' la''
  }
  \tag #'secondo {
    do'!4\fermata r4 |
    si\fermata r r2 |
    <mi' si'>2:16-\sug\p -\sug\cresc q:16 |
    <re' do''!>:16-\sug\f q:16 |
    <re' si'>4-\sug\ff r
  }
>> si''16 la'' sol'' fad'' sol'' fad'' sol'' la'' |
si''4-! sol''-! mi''-! dod''-! |
<<
  \tag #'primo {
    re''8( la'') la''-.(\mf la''-. la''-. la''-. la''-. la''-.) |
    <>\ff \ru#4 { la''8.( fad''16) } |
    la''8(\mf fad'') fad''(-. fad''-. fad''-. fad''-. fad''-. fad''-.) |
    <>\ff \ru#3 { si''8. fad''16 } si''16 la'' sol'' fad'' |
    sol''8(\mf si'') si''(-. si''-. si''-. si''-. si''-. si''-.) |
    <>\ff \ru#4 { si''8.( sol''16) } |
    si''8(\mf sol'') sol''-. sol''-. \ru#4 sol''-. |
    <>\ff \ru#4 { si''8.( sol''16) } |
    <do''! do'''!>2:16 \ru#8 <do'' do'''>:16 re''':16 |
    mi'''8
  }
  \tag #'secondo {
    re''8( fad'') \once\slurDashed fad''-.(-\sug\mf fad''-.) fad''-.( fad''-. fad''-. fad''-.) |
    red''4-.-\sug\ff do''!-. red''-. do''-. |
    red''4-\sug\mf red''8(-. red''-. red''2:8^\dotFour) |
    red''4-\sug\ff si' red'' si' |
    \once\slurDashed si'8(-\sug\mf sol'') \once\slurDashed sol''-.( sol''-. sol''2:8^\dotFour) |
    sol'4-\sug\ff mi' sol' mi' |
    sol'8(-\sug\mf si') si'-. si'-. si'2:8^\dotFour |
    fa'!4-\sug\ff re' fa' re' |
    mi'4 r r8. do'16 do'8.\trill si32 do' |
    re'4 r r8. re'16 re'8.\trill do'32 re' |
    mi'4 r r8. mi'16 mi'8.\trill re'32 mi' |
    fa'8. fa'16 fa'8.\trill mi'32 fa' sol'8. sol'16 sol'8.\trill fa'32 sol' |
    la'8. la'16 la'8.\trill sol'32 la' si'8. si'16 si'8.\trill la'32 si' |
    do''8
  }
>> mi''4_\markup { \dynamic mf \italic stanchi } mi''( mi''8-. mi''-. mi''-.) |
mi''8( fa'') fa''4.( fad''8)-.( fad''-. fad''-.) |
fad''( sol'') sol''4.( sold''8)-.( sold''-. sold''-.) |
sold''8( la'') la''4.( do''8 \grace re''16 do''8 si'16 do'') |
si'4-! <<
  \tag #'primo {
    re''4(\p mib'' re'') |
    mib''( re'' sol'' fa''8 mib'') |
    re''4 re'( mib' re') |
    mib'( re' sol' fa'8 mib') |
    re'4
  }
  \tag #'secondo {
    si'4(\p do'' si') |
    do''( si' mib'' re''8 do'') |
    si'4 si( do' si) |
    \once\slurDashed do'( si mib' re'8 do') |
    si!4
  }
>> r4 sol2~ |
sol sol |
sol\fermata r |
%% 3/4
<>_\markup\whiteout\italic { dol. a mezza vocce } <<
  \tag #'primo {
    sol'4 fa'8(-. mi'-. fa'-. sol'-.) |
    la'8. si'32 do'' sol'2 |
    sol'8( sold' la') mi'(-. fa'-. sol'-.) |
    mi'4( re') r |
    sol'4 fa'8(-. mi'-. fa'-. sol'-.) |
    la'8.( si'32 do'' sol'2) |
    sol'4 fa'8 mi' re'4\trill |
    \omit TupletBracket do''16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') }
    \ru#2 { sol'16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') } } |
    sol'2. |
    \ru#3 { sol'16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') } } |
    sol'2. |
    \ru#2 { sol'16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') } }
    la'16.-.
  }
  \tag #'secondo {
    mi'4 re'8(-. do'-. re'-. mi'-.) |
    fa'4( mi'2) |
    mi'4( fa'8) dod'-.( re'-. mi'-.) |
    do'4( si) r |
    mi'4 re'8(-. do'-. re'-. mi'-.) |
    fa'4( mi'2) |
    mi'4 re'8 do' si4-\sug^\trill |
    do'2. |
    \omit TupletBracket \ru#3 { sol16. re'32 \tuplet 3/2 { fa'16( mi' re') } } |
    do'2. |
    \omit TupletBracket \ru#3 { sol16. re'32 \tuplet 3/2 { fa'16( mi' re') } } |
    do'4 r do''16.-.
  }
>> fa''32-. \tuplet 3/2 { la''16( sol'' fa'') } |
\ru#2 { do''16.-. mi''32-. \tuplet 3/2 { sol''16( fa'' mi'') } }
sol'16. re''32 \tuplet 3/2 { fa''16( mi'' re'') } |
\ru#2 { do''16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') } }
do''16.-. fa''32-. \tuplet 3/2 { la''16( sol'' fa'') } |
\ru#2 { do''16.-. mi''32-. \tuplet 3/2 { sol''16( fa'' mi'') } }
sol'16. re''32 \tuplet 3/2 { fa''16( mi'' re'') } |
\ru#3 { sol'16.-. do''32-. \tuplet 3/2 { mi''16( re'' do'') } } |
do''2 r4 |
