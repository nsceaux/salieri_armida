\piecePartSpecs
#`((flauti #:score-template "score-deux"
           #:music , #{
\quoteViolinoI "AAAviolini"
s1*115 \break
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s2.*7 \mmRestVisible } \break #})

   (oboi #:score-template "score-deux"
           #:music , #{
\quoteViolinoI "AAAviolini"
s1*8
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*13 s1*11 \mmRestVisible } \break
s1*36 \break \allowPageTurn
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*21 \mmRestVisible } \break
s1*16 \break
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*10 \mmRestVisible } \break
               #})

   (corni #:score-template "score-deux"
          #:tag-global ()
          #:instrument "Corni in C"
          #:music , #{
\quoteViolinoI "AAAviolini"
s1*8
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*13 s1*11 \mmRestVisible } \break
s1*24 \break \allowPageTurn
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*33 \mmRestVisible } \break
s1*3
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*4 \mmRestVisible }
s1*6
<>^\markup\tiny "Vln"
\cue "AAAviolini" { \mmRestInvisible s1*13 s2.*7 \mmRestVisible } \break
                      #})

   (fagotti #:score "score-fagotti")
   (tromboni #:music , #{
\quoteViolinoI "AAAviolini"
s1*8
<>^\markup\tiny "Vln"
\cueTreble "AAAviolini" { \mmRestInvisible s1*13 s1*11 \mmRestVisible } \break
s1*36\break \allowPageTurn
<>^\markup\tiny "Vln"
\cueTreble "AAAviolini" { \mmRestInvisible s1*24 \mmRestVisible } \break
               #})
   (violino1)
   (violino2)
   (viola)
   (bassi #:score "score-bassi"))
