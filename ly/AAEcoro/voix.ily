<<
  \tag #'voix1 {
    \clef "soprano/treble" fa''8 mib'' re'' |
    mib''8. re''16 do''8 |
    re'' mib'' re''16[ mib''] |
    \grace re''8 do''4 sib'8 |
    sib' do'' re'' |
    do''8. re''16 mib''8 |
    re''16[ fa''] mib''8 re'' |
    \grace re''8 do''4 r8 |
    R4.*2 |
    re''8 mib'' re''16[ mib''] |
    \grace re''8 do''4 sib'8 |
    sib' do'' re'' |
    do''8. re''16 mib''8 |
    re''16[ fa''] mib''8 re'' |
    \grace re''8 do''4 r8 |
    do''8 re'' mib''16[ do''] |
    fa''8. mib''16 re''8 |
    do''8 re'' mib''16[ do''] |
    fa''16.[ mib''32] re''8 r |
    sib'8 sol'' fa'' |
    fa''[ mib''16] re'' re''8 |
    \grace fa''16 mib''8 re'' do'' |
    \grace do''8 re''4 r8 |
    do''8 re'' mib''16[ do''] |
    fa''8. mib''16 re''8 |
    do'' re'' mib''16[ do''] |
    \grace do''8 sib'4*1/2
  }
  \tag #'voix2 {
    \clef "soprano/treble" re''8 do'' sib' |
    do''8. sib'16 la'8 |
    sib'8 sib' sib'16[ do''] |
    \grace sib'8 la'4 sib'8 |
    re'8 fa' sib' |
    la'8. sib'16 do''8 |
    sib'16[ re''] do''8 sib' |
    \grace sib'8 la'4 r8 |
    R4.*2 |
    sib'8 sib' sib'16[ do''] |
    \grace sib'8 la'4 sib'8 |
    re'8 fa' sib' |
    la'8. sib'16 do''8 |
    sib'16[ re''] do''8 sib' |
    \grace sib' la'4 r8 |
    la'8 sib' do''16[ la'] |
    re''8. do''16 sib'8 |
    la' sib' do''16[ la'] |
    re''16.[ do''32] sib'8 r |
    sib' mib'' re'' |
    re''[ do''16] sib' sib'8 |
    \grace re''16 do''8 sib' la' |
    \grace la'8 sib'4 r8 |
    la'8 sib' do''16[ la'] |
    re''8. do''16 sib'8 |
    la' sib' do''16[ la'] |
    sib'4*1/2
  }
>>