\clef "bass" sib4_\markup\italic [dol.] r8 |
fa4 r8 |
sib8 sol fa |
mib4 re8 |
sib,4\p r8 |
fa4 r8 |
sib,8 fa sib |
r fa, fa |
sib,4\f r8 |
fa4 r8 |
sib sol fa |
mib4 re8 |
sib,4\p r8 |
fa4 r8 |
sib la sib |
r fa fa, |
r r fa\f |
sib,4 r8 |
r r fa sib, sib r |
R4.*2 |
mib8\f fa fa, |
sib,8 sib sib16\trill la32 sol |
fa8\p r fa |
sib,4\f r8 |
r r fa\p |
\custosNote sib,8
