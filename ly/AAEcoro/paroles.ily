Don -- zel -- le sem -- pli -- ci
non vi la -- gna -- te,
che trop -- po ra -- pi -- da
fug -- ga l’e -- tà
non vi la -- gna -- te,
che trop -- po ra -- pi -- da
fug -- ga l’e -- tà

se fres -- che, e gio -- va -- ni
non v’af -- fret -- ta -- te
il frut -- to a co -- glie -- re
del -- la Bel -- tà.

Don -- zel -- le sem -- pli -- ci
non vi la -- gnate…