\clef "treble"
<<
  \tag #'primo {
    fa''8(_\markup\italic dol. mib'' re'') |
    mib''8.( re''16 do''8) |
    re''( mib'' re''16 mib'') |
    \grace re''8 do''4 sib'8 |
    sib'8(\p do'' re'') |
    do''8.( re''16 mib''8) |
    re''16( fa'' mib''8 re'') |
    \grace re''8 do''4 r8 |
    fa''8(\f mib'' re'') |
    mib''8.( re''16 do''8) |
    re'' mib'' re''16 mib'' |
    \grace re''8 do''4 sib'8 |
    sib'8(\p do'' re'') |
    do''8.( re''16 mib''8) |
    re''16 fa'' \grace fa''16 mib''8[ re''] |
    \grace re''8 do''4 r8 |
    do''8(\p re'' mib''16 do'') |
    <re' sib' fa''>8.\f mib''16 re''8 |
    do''\p re'' mib''16 do'' |
    <re' sib' fa''>16.\f mib''32 re''8 r |
    << { sib'8\p sol'' fa'' } \\ sib' >> |
    << { fa''8 mib''16 re'' re''8 } \\ sib' >> |
    \grace fa''8 mib''\f re'' do'' |
    sib'8 sib r |
    do''8\p( re'' mib''16 do'') |
    <re' sib' fa''>8.\f mib''16 re''8 |
    do''\p re'' mib''16 do'' |
    <re' sib' fa''>2*1/4\f
  }
  \tag #'secondo {
    re''8(_\markup\italic [dol.] do'' sib') |
    do''8. sib'16 la'8 |
    sib'4 sib'16 do'' |
    \grace sib'8 la'4 sib'8 |
    re'( fa' sib') |
    la'8.( sib'16 do''8) |
    sib'16( re'' do''8 sib') |
    \grace sib'8 la'4 r8 |
    re''(-\sug\f do'' sib') |
    do''8.( sib'16 la'8) |
    sib'4 sib'16 do'' |
    \grace sib'8 la'4 sib'8 |
    re'8-\sug\p fa' sib' |
    la'8. sib'16 do''8 |
    sib'16 re'' \grace re''8 do''[ sib'] |
    \grace sib'8 la'4 r8 |
    la'(-\sug\p sib' do''16 la') |
    <re' re''>8.-\sug\f do''16 sib'8 |
    la'8-\sug\p sib' do''16 la' |
    <re' re''>16.-\sug\f do''32 sib'8 r |
    sib'4.-\sug\p |
    sib'8 la' sib' |
    \grace re'' do''-\sug\f sib' la' |
    sib' sib r |
    la'8(-\sug\p sib' do''16 la') |
    <re' re''>8.-\sug\f do''16 sib'8 |
    la'8-\sug\p sib' do''16 la' |
    <re' re''>2*1/4\f
  }
>>