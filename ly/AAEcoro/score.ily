\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 1 \super mo } }
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 2 \super do } }
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s4.*5\break s4.*6\pageBreak
        \grace s8 s4.*6\break s4.*6\pageBreak
        \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
