\clef "alto" sib4_\markup\italic [dol.] r8 |
fa4 r8 |
sib8 sol' fa' |
mib'4 re'8 |
sib4 r8 |
fa'4 r8 |
sib8 fa' sib' |
fa' fa fa' |
sib4-\sug\f r8 |
fa'4 r8 |
sib( sol' fa') |
mib'4 re'8 |
sib4-\sug\p r8 |
fa'4 r8 |
sib la sib |
r fa' fa |
fa'4.~ |
fa'~ |
fa'~ |
fa'8 fa r |
mib'4\p re'8 |
re' do' sib |
sol'\f fa' fa' |
sib8 sib sib16 la32 sol |
fa8-\sug\p fa' fa' |
fa'4.-\sug\f~ |
fa'-\sug\p |
sib2*1/4\f
