\piecePartSpecs
#`((bassi #:score-template "score-voix")

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Ismene
  \livretVerse#12 { Ove corre, infelice! Ove la guida }
  \livretVerse#12 { Il suo cieco furor? l’antico fasto }
  \livretVerse#12 { La sua gloria dov’è? Qual forza ignota }
  \livretVerse#12 { Si l’oppresse in un dì. D’amore il regno }
  \livretVerse#12 { Questa che a voglia sua volse, e rivolse, }
  \livretVerse#12 { Questa di mille amanti }
  \livretVerse#12 { I voti e i pianti a disprezzare avvezza, }
  \livretVerse#12 { Or siegue chi la fugge, e la disprezza. }
}

       #}))
