\clef "bass" sib,1~ |
sib,~ |
sib,2 mib |
do1 |
si,!~ |
si,2 do~ |
do1~ |
do2 si,~ |
si,1~ |
si,~ |
si,2 mi~ |
mi1 |
r4 fad si,2 |
