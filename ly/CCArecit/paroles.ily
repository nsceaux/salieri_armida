O -- ve cor -- re, in -- fe -- li -- ce! o -- ve la gui -- da
il suo cie -- co fu -- ror? L’an -- ti -- co fa -- sto
la sua glo -- ria dov’ è? Qual for -- za i -- gno -- ta
si l’op -- pres -- se in un dì? D’a -- mo -- re il re -- gno
que -- sta che a vo -- glia sua vol -- se, e ri -- vol -- se,
que -- sta di mille a -- man -- ti
i vo -- ti e i pian -- ti a di -- sprez -- za -- re av -- vez -- za,
or se -- gue chi la fug -- ge, e la di -- sprez -- za.
