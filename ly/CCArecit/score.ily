\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Ismene
      shortInstrumentName = \markup\character Is.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \includeNotes "bassi"
      \origLayout { s1*3\break s1*3\break s1*3\break s1*2\break }
    >>
  >>
  \layout { }
  \midi { }
}
