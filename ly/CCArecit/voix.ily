\clef "alto/treble" r4 r8 sib'16 sib' sib'4 fa'8 fa' |
re' re' re' re'16 \ficta mib' fa'8 fa' r8 fa'16 sol' |
lab'4 \ficta sib'8 fa'8 sol'4 r16 sol' sol' la' |
fad'8 fad' r fad'16 sol' la'4 fad'8 re' |
sol'4 r16 sol' si'! re'' re''8 sol' r re'16 mi'! |
fa'4 sol'8 re' mi'!4 r16 sol' sol' la' |
fad'8 fad' re' re'16 re' re'8 mi' fad'! fad' |
\ficta fad'4 sol'8 la' la' red' r4 |
fad'8 fad'16 fad' fad'8 sol' la' la' r do'' |
la' la' r la' fad' fad' r fad' |
la' la' la' si' sol' sol' r mi' |
sol' sol' fad' re' lad' lad' r16 lad' lad' si' |
si'8 fad' r4 r2 |
