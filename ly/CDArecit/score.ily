\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Ub. s1*21 s2
        \set Staff.shortInstrumentName = \markup\character Ri. s2 s1
        \set Staff.shortInstrumentName = \markup\character Ub. s1*6 s2
        \set Staff.shortInstrumentName = \markup\character Ri. s2 s1*15 s2 s1*9 s2
        \set Staff.shortInstrumentName = \markup\character Ub. s2 s1*13 s2.
        \set Staff.shortInstrumentName = \markup\character Ri. s4 s1*11 s2
        \set Staff.shortInstrumentName = \markup\character Ub.
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*3\break s1*4\pageBreak
        s1*4\break s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*3\pageBreak
        % 5
        s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2 s2 \bar "" \break s2 s1*2\break
        s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2\break s2 s1*2\break s1*2\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2\break s1*2\break s1*2\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2\break s1*2 s2 \bar "" \break s2 s1\pageBreak
        % 10
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1
      }
    >>
  >>
  \layout { }
  \midi { }
}
