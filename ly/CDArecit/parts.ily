\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ubaldo
    \livretVerse#12 { Siam quasi in salvo, eccoci al lido. Osserva }
    \livretVerse#12 { L’amico legno, e quella, }
    \livretVerse#12 { Che ne siede al governo, e che a sua voglia }
    \livretVerse#12 { Regola i venti, e il mar, sicura Guida, }
    \livretVerse#12 { Che m’attende al partir. }
    \livretPers Rinaldo
    \livretVerse#12 { (Povera Armida!) }
    \livretPers Ubaldo
    \livretVerse#12 { Ma tu sospiri! Il guardo }
    \livretVerse#12 { Fissi smarrito al suol; confuso, è mesto }
    \livretVerse#12 { Or ti volgi, or t’arresti, e nel momento, }
    \livretVerse#12 { Che affrettar ti dovria, sembri più lento! }
    \livretVerse#12 { Che mediti? che pensi? }
    \livretPers Rinaldo
    \livretVerse#12 { Ah tutto, amico }
    \livretVerse#12 { Svelerotti il mio cor. Fuggir da quella, }
    \livretVerse#12 { Che fù l’anima mia, che patria e regno }
    \livretVerse#12 { Abbandonò per me, che resta in preda }
    \livretVerse#12 { All’ affanno, al rossor, m’empie d’orrore }
    \livretVerse#12 { Mi sembra crudeltà. Trionfo, è vero, }
    \livretVerse#12 { Ma il conflitto è crudel. Tutto m’invita }
    \livretVerse#12 { Se la ragione ascolto, }
    \livretVerse#12 { Sollecito a partir. Se ascolto, il cuore }
    \livretVerse#12 { Non parla che di lei. L’onor mi sprona, }
    \livretVerse#12 { La pietà mi trattien. Ma ch’io ritorni }
    \livretVerse#12 { A consolarla almen nel fiero istante }
    \livretVerse#12 { Della partenza amara, e chi’io le dia }
    \livretVerse#12 { Nel suo mortale affano i pegni estremi }
    \livretVerse#12 { Di tenera amistà, se non di fede, }
    \livretVerse#12 { La ragion non lo vieta, amor lo chiede. }
  }
  \null
  \column {
    \livretPers Ubaldo
    \livretVerse#12 { Signor che dici mai? De’ molli affetti }
    \livretVerse#12 { Tremi al conflitto, e maggior guerra aspetti? }
    \livretVerse#12 { T’arresta il passo un piccol rio, che appena }
    \livretVerse#12 { Serba un fil d’onda in faccia al Sirio ardente? }
    \livretVerse#12 { E lo potrai varcar quando è torrente? }
    \livretVerse#12 { E questo è il tuo trionfo? Ah no, tu gemi }
    \livretVerse#12 { Ancor fralle catene, e per sedurti }
    \livretVerse#12 { Il piacer lusinghiero }
    \livretVerse#12 { Si veste di pietà. }
    \livretPers Rinaldo
    \livretVerse#12 { No, non è vero. }
    \livretVerse#12 { Son libero, son mio. Ma chi’io la lasci, }
    \livretVerse#12 { Ingannata cosi… Povera Armida! }
    \livretVerse#12 { Non la vedrò più mai. Che amaro pianto }
    \livretVerse#12 { Verserà da begli occhi? E ch’io non possa }
    \livretVerse#12 { Raddolcirla, placarla, }
    \livretVerse#12 { Darle l’estremo addio… }
    \livretVerse#12 { Non lo sperar. Troppo trionfo è il mio. }
    \livretPers Ubaldo
    \livretVerse#12 { Vattene dunque, fuggi, }
    \livretVerse#12 { Và, colma la misura à tuoi delitti; }
    \livretVerse#12 { Torna alle tue catene. Io solo al campo }
    \livretVerse#12 { Senza te tornerò. Dirò, che invano }
    \livretVerse#12 { Impiegò per salvarti }
    \livretVerse#12 { I suoi prodigi il Ciel; che più non curi }
    \livretVerse#12 { Nè l’onor, ne la fè; che ti lasciai }
    \livretVerse#12 { Dal tuo vil giogo oppresso, }
    \livretVerse#12 { Della Patria nemico, e di te stesso. }
  }
  \null
} #}))
    