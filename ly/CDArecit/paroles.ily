Siam qua -- si in sal -- vo, ec -- co -- ci al li -- do. Os -- ser -- va
l’a -- mi -- co le -- gno, e quel -- la,
che ne sie -- de al go -- ver -- no, e che a sua vo -- glia
re -- go -- la i ven -- ti, e il mar, si -- cu -- ra gui -- da,
che ne at -- ten -- de al par -- tir.

(Po -- ve -- ra Ar -- mi -- da!)

Ma tu so -- spi -- ri! Il guar -- do
fis -- si smar -- ri -- to al suol; con -- fu -- so, e me -- sto
or ti vol -- gi, or t’ar -- re -- sti, e nel mo -- men -- to,
che af -- fret -- tar ti do -- vri -- a, sem -- bri più len -- to!
che me -- di -- ti? che pen -- si?

Ah tut -- to, a -- mi -- co
sve -- le -- rot -- ti il mio cor. Fug -- gir da quel -- la,
che fu l’a -- ni -- ma mi -- a, che pa -- tria, e re -- gno
ab -- ban -- do -- nò per me, che re -- sta in pre -- da
all’ af -- fan -- no, al ros -- sor, m’em -- pie d’or -- ro -- re
mi sem -- bra cru -- del -- tà. Tri -- on -- fo, è ve -- ro,
ma il con -- flit -- to è cru -- del. Tut -- to m’in -- vi -- ta
se la ra -- gio -- ne as -- col -- to,
sol -- le -- ci -- to a par -- tir. Se as -- col -- to, il cuo -- re,
non par -- la che di le -- i. L’o -- nor mi spro -- na,
la pie -- tà mi trat -- tien. Ma ch’io ri -- tor -- ni
a con -- so -- lar -- la al -- men nel fie -- ro is -- tan -- te
del -- la par -- ten -- za a -- ma -- ra, e chi’io le di -- a
nel suo mor -- ta -- le af -- fan -- no i pe -- gni e -- stre -- mi
di te -- ne -- ra a -- mi -- stà, se non di fe -- de,
la ra -- gion non lo vie -- ta, a -- mor lo chie -- de.

Si -- gnor che di -- ci ma -- i? De’ mol -- li af -- fet -- ti
tre -- mi al con -- flit -- to, e mag -- gior guer -- ra a -- spet -- ti?
T’ar -- re -- sta il pas -- so un pic -- ciol ri -- o, che ap -- pe -- na
ser -- ba un fil d’on -- da in fac -- cia al Si -- rio ar -- den -- te?
e lo po -- trai var -- car quan -- do è tor -- ren -- te?
E que -- sto è il tuo tri -- on -- fo? Ah nò, tu ge -- mi an -- cor fra le ca -- te -- ne, e per se -- dur -- ti
il pia -- cer lu -- sin -- ghie -- ro
si ve -- ste di pie -- tà.

No, non è ve -- ro.
Son li -- be -- ro, son mi -- o. Ma chi’io la la -- sci,
in -- gan -- na -- ta co -- sì… Po -- ve -- ra Ar -- mi -- da!
Non la ve -- drò più ma -- i. Che a -- ma -- ro pian -- to
ver -- se -- rà da’ be -- gli oc -- chi? E ch’io non pos -- sa
rad -- dol -- cir -- la, pla -- car -- la,
dar -- le l’e -- stre -- mo ad -- di -- o…
non lo spe -- rar. Trop -- po tri -- on -- fo è il mi -- o.

Vat -- te -- ne dun -- que, fug -- gi,
va’, col -- ma la mi -- su -- ra a’ tuoi de -- lit -- ti;
tor -- na al -- le tue ca -- te -- ne. Io so -- lo al cam -- po
sen -- za te tor -- ne -- rò. Di -- rò, che in -- va -- no
im -- pie -- gò per sal -- var -- ti
i suoi pro -- di -- gi il Ciel; che più non cu -- ri
né l’o -- nor, né la fé; che ti las -- cia -- i
dal tuo vil gio -- go op -- pres -- so,
del -- la Pa -- tria ne -- mi -- co, e di te stes -- so.
