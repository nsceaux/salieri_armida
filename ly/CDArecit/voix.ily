\ffclef "tenor/G_8"
R1*12 |
<>^\markup\character Ubaldo
re'4 do'8 re' si si si dod'16 re' |
re'8 la r4 r2 |
R1 |
r2 r4 r8 si |
si si16 si la8 si sold sold r mi |
sold sold r sold16 sold sold4 sold8 fad |
\ficta sold8 sold r16 sold sold la si4 si |
si8 si16 si si8 si sold4 r16 si si mi' |
mi'8 si r si16 dod' re'4 si8 dod' |
la4 r
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r do''!8 si'16 do'' |
la'8 la' r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r8 la la re' re' la r16 la la sol |
la8 la16 la sol8 la fad4 r16 re fad mi |
\ficta fad8 fad r fad16 sol la8 la r do'16 mi' |
re'8 re' r16 re' re' re' re'8 la r la16 la |
la4 la8 si do'!8 do' do' do'16 re' |
si8 si r re' si16 si si8 r sol |
do' do' r4
\ffclef "soprano/treble" <>^\markup\character Rinaldo
do''4 do''8 mi'' |
do'' do'' r do''16 do'' do''4 la'8 si' |
sol'4 r r8 la' la' si' |
dod'' dod'' r dod''16 re'' mi''4 re''8 mi'' |
dod''8 dod'' r dod'' la' la' r si' |
\ficta dod'' dod'' r dod'' dod'' dod'' si' la' |
re''4 r16 re'' re'' re'' re''8 la' r la'16 la' |
la'8 la' r la'16 la' fad'4 r |
mib''4 re''8 mib'' do'' do'' r re'' |
re'' la' la' sib' sol'4 r8 re'' |
sib' sib' r re'' re'' sol' r sol'16 sol' |
re''4 re''8 mib'' do''4 do''8 re''16 mib'' |
\ficta mib''8 la' r la' la' la' la' sib' |
do'' do'' r fa' do'' do'' do'' re'' |
sib'4 r16 sib' sib' do'' lab'8 lab' r fa' |
lab' lab' lab' sib' sol' sol' r16 mib'' mib'' mib'' |
mib''8 sib' r sib'16 do'' |
reb''4 reb''8 mib'' do''4 r |
lab'4 sib'8 do'' do'' sol' r sol' |
sol' sol' sol' lab' sib'4 reb''8 do''16 reb'' |
sib'8 sib' sib' sib'16 sib' lab'8 sib' sol' sol' |
r sol' sol' lab' sib' sib' r sib' |
sib' sib' do'' reb'' do'' do'' r16 do'' do'' do'' |
do''8 sol' r sol' sol' sol' sol' lab' |
sib' sib' sib' do'' lab' lab' r fa'16 fa' |
do''4 do''8 re'' si'! si' r16 si' si' do'' |
do''8 sol' r4
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r4 r8 mib' |
do'4 r16 do' sib do' lab8 lab r16 mib lab sol |
lab8 lab do' sib16 do' lab8 lab r16 lab lab sib |
do'4 sib8 lab reb' reb' r4 |
r8 reb' reb' reb' reb' lab r16 lab lab lab |
fa8 fa r16 fa \ficta lab \ficta solb lab8 lab r lab16 sib |
dob'8 dob' r mib' dob' dob' sib dob' |
lab lab r lab lab lab lab sib |
dob'4 dob'8 reb'16 lab sib8 sib r reb'! |
sib sib lab solb si8 si r4 |
r2 r4 r8 si |
sold4 r16 si si mi' mi'8[ si] si si16 si |
sold8 sold r16 sold si la si8 si r si16 si |
si4 si8 dod' re' re' r re' |
re' si si dod' la4
\ffclef "soprano/treble" <>^\markup\character Rinaldo
mi''!8 re''16 mi'' |
dod''8 dod'' r la' dod''16 dod'' dod''8 r re'' |
re'' la' r16 la' la' re'' re''8 la' r la'16 la' |
la'4 sol'8 la' fad'4 r |
mib''8 re''16 mib'' do''8 do'' do'' do''16 do'' re''8 la' |
sib' sib' r16 re'' re'' sol'' sol''8 re'' r re''16 re'' |
re''4 do''8 re'' sib' sib' r16 sol' sol' la' |
si'!8 si' r si'16 do'' re''8 re'' r mib'' |
fa'' fa'' r4 re''8 re''16 re'' si'!8 sol' |
do''8 do'' r4 r2 |
r r4 mib''8 re''16 mib'' |
do''4 r do''8 do''16 do'' do''8 sib' |
sol' sol' r4
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r4 la8 la16 si! |
dod'8 dod' mi' la la4 la16 la la si |
dod'8 dod' r16 dod' dod' re' re'8 la r4 |
re'8 re'16 re' do'8 re' si si r re |
sol4 sol8 la si si r re'16 re' |
si4 do'8 re' sol4 r16 sol la si |
la8 la r mi mi mi4 mi16 fa! |
sol8 sol r16 sol sol sib sol8 sol r la |
fa!4 r16 fa sol la sib8 sib r fa16 sol |
lab4 lab8 \ficta sib sol4 r16 sib sib sib |
sol8 sol r16 mib mib fa sol8 sol r sib |
\ficta sib mib r sib!16 sib mib'4 do'8 sib |
la!8 la r16 la la sib sib8 fa r4 |
