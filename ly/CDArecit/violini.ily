\clef "treble" \tag #'primo <>_\markup\italic { non troppo forte } ^"Tutto legatto"
<sold' mi''>8( red' mi' fad') mi'( sold si red') |
mi'( sold') dod''( si') dod''( si') dod''( si') |
dod''( si' lad' si') lad' si' lad' si' |
mi' red' mi' fad' sold' fad' sold' mi' |
red' fad' dod'' si' dod'' si' dod'' si' |
dod'' si' lad' si' lad' si' lad' si' |
dod' mid' sold' si' mid'' sold'' mid'' si' |
la' dod'' fad'' sold'' la'' fad'' dod'' la' |
fad' dod' la dod' fad' la dod' mid' |
fad' la' dod'' mid'' fad''( mi'' re'' dod'') |
\ficta re'' fad'' la'' sol'' fad'' mi'' re'' dod'' |
re'' fad'' re'' la' fad' re'' la' fad' |
re'4 r <re' si'> r |
r mi''\f mi''8( sold'' si'' sold'') |
mi'' si' sold' si' mi'' si' sold' mi' |
si' sold' mi' si sold2~ |
sold r |
R1*4 |
r8\fermata <<
  \tag #'primo {
    dod''8-.(\p dod''-. dod''-.) do''2~ |
    do'' r8 do''-.(\mf do''-. do''-.) |
    do''2-\sug\rf
  }
  \tag #'secondo {
    mi'8-.(-\sug\p mi'-. mi'-.) mi'2~ |
    mi' r8 mi'-.(-\sug\mf mi'-. mib'-.) |
    re'2\rf
  }
>> r2 |
R1*4 |
<<
  \tag #'primo {
    si'4-! r si'-! r |
    do''-! r r2 |
  }
  \tag #'secondo {
    re'4-! r sol'-! r |
    sol'-! r r2 |
  }
>> \allowPageTurn
R1*15 |
R2 |
R1*26 | \allowPageTurn
<<
  \tag #'primo {
    r2 la''\p~ |
    la''1 |
    sib''~ |
    sib''2 r |
    si'!4-! r r2 |
  }
  \tag #'secondo {
    r2 mib''\p~ |
    mib''1 |
    re''~ |
    re''2 r |
    sol'4-! r r2 |
  }
>>
R1 |
<<
  \tag #'primo { do''4-! }
  \tag #'secondo { sol'-! }
>> r4 r8 do'-!\f re'-! mib'-! |
fa'-! sol'-! la'-! si'!-! do''4-! r |
R1 |
r4 <re' la' fad''> << mi''2 \\ la' >> |
R1 |
<<
  \tag #'primo {
    dod''4 r re'' r |
    r2 si'! |
  }
  \tag #'secondo {
    la'4 r la' r |
    r2 re' |
  }
>>
R1*2 |
<<
  \tag #'primo { la'2 }
  \tag #'secondo { mi' }
>> r2 |
R1 |
<<
  \tag #'primo {
    la'4 r sib' r |
    r2 \ficta sib' |
  }
  \tag #'secondo {
    fa'4 r fa' r |
    r2 sol' |
  }
>>
R1*2 |
r2 r4 <<
  \tag #'primo { <la' fa''>4 }
  \tag #'secondo { <do'' fa'>4 }
>>

