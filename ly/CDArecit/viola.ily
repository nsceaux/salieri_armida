\clef "alto" \once\tieDashed sold1~ |
sold |
fad |
si2 lad |
si red'~ |
red'1 |
sold |
la~ |
la~ |
la~ |
la~ |
la~ |
la4 r sol r |
r dod' si2~ |
si1~ |
si~ |
si2 r |
R1*4 |
r8\fermata la'8-.(\p la'-. la'-.) la'2~ |
la' r8 la'8-.(\mf la'-. la'-.) |
la'2-\sug\rf r |
R1*4 |
sol'4-! r re'-! r |
mi'-! r r2 |
R1*15 |
R2 | \allowPageTurn
R1*26 | \allowPageTurn
r2 do''-\sug\p~ |
do''1 |
sib'~ |
sib'2 r |
re'4-! r r2 |
R1 |
mib'4-! r4 r8 do-!\f re-! mib-! |
fa-! sol-! la-! si!-! do'4-! r |
R1 |
r4 re' dod'2 |
R1 |
mi'4 r re' r |
r2 sol' |
R1*2 |
dod'2 r |
R1 |
re'4 r re' r |
r2 mib' |
R1*2 |
r2 r4 fa' |
