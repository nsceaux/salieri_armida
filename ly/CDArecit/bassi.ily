\clef "bass" <>_\markup\italic { non troppo forte } mi1~ |
mi |
red |
dod |
si, |
si, |
dod |
fad~ |
fad~ |
fad~ |
fad~ |
fad~ |
fad4 r sol r |
r la\f sold2~ |
sold1~ |
sold~ |
sold2 r |
R1*4 |
la8\fermata\noBeam la8-.(\p la-. la-.) la2~ |
la r8 la(-.\mf la-. sol-.) |
fad2-\sug\rf r |
R1*4 |
sol4-! r fa!-! r |
mi r r2 |
fad r |
sol8-! sol( mi re) dod2 |
R1*3 |
fad2 r |
R1*2 | \allowPageTurn
r2 sol |
R1 |
fa!2 mib |
R1*2 |
re2 r |
r mib |
R2 |
r2 lab |
r mi! |
R1*5 |
r2 fa |
R1 |
r4 sol lab2 |
R1*2 |
solb2 fa |
R1*4 |
r2 solb |
r red4 r16*1/2 si,32\f dod red mi fad sold lad |
si8. la!16 sold8. fad16 mi2 |
R1*3 | \allowPageTurn
r2 dod |
r sol |
fad r |
r fad\p~ |
fad1 |
sol~ |
sol2 r |
fa!4-! r r2 |
R1 |
mib4-! r r8 do-.\f re-. mib!-. |
fa-. sol-. la-. si!-. do'4-! r |
R1 |
r4 re dod2 |
R1 |
sol4 r fad r |
r2 sol |
R1*2 |
dod2 r |
R1 |
re4 r re r |
r2 mib |
R1*2 |
r2 r4 fa |
