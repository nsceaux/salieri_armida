\clef "tenor/G_8" r4 r8 si16 si si4 si8 mi' |
mi' si r16 si si si sold8 sold r16 sold si la |
si8 si r16 si re' dod' la4 r8 mi' |
mi' la r la16 la la4 sol8 la |
fad fad r16 fad la sol la8 la do' do'16 do' |
si8 si si la!16 sol dod'8 dod' r re' |
re'8 la r4 r
