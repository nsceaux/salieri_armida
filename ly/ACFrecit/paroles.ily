Quan -- to fra -- gi -- li, e va -- ne, im -- mon -- di spir -- ti,
son l’ar -- mi vo -- stre in -- con -- tro al Ciel! Mi -- ra -- te
quan -- to è lie -- ve il con -- tra -- sto, e quan -- to po -- co
ba -- sta, in -- fe -- li -- ci, a ri -- chia -- mar -- vi al fuo -- co!
