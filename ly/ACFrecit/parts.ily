\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { Quanto fragili, e vane, immondi spirti, }
  \livretVerse#12 { Son l'armi vostre incontro al Ciel! Mirate }
  \livretVerse#12 { Quanto è lieve il contrasto, e quanto poco }
  \livretVerse#12 { Basta, infelici, a richiamarvi al fuoco! }
}

       #}))
