\clef "treble"
<<
  \tag #'primo { <sold'' si'>2 }
  \tag #'secondo { mi''2 }
>> r2 |
R1 |
r2 <<
  \tag #'primo { la''2 }
  \tag #'secondo { mi'' }
>>
R1 |
<<
  \tag #'primo { fad''2 }
  \tag #'secondo { la' }
>> r2 |
<<
  \tag #'primo {
    <re'' si''>4 r <mi'' dod'''>4 r |
    r <mi'' dod'''> <fad'' re'''>
  }
  \tag #'secondo {
    <re' si'> r <la' dod'> r |
    r <la' mi''> <la' fad''>
  }
>>
