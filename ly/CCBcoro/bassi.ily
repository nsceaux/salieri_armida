\clef "bass" mi4\mf mi8 |
mi4 mi8 red4 red8 |
mi8( do lad,) si, si la! |
sol4(\rf mi8) do4 do8 |
si,4 si,8 si,4 si,8 |
si,4 si8 si8.\rf do'16 si8 |
si4 si8 la4 la8 |
la4 la8 sol8.\rf la16 sol8 |
sol4 sol8 fad4 fad8\f |
mi4.\fermata r4 r8 |
la4 la8 si4 si,8 |
mi4. r4 r8 |
la4 la8 si4 si,8 |
mi4 sol8 la4 si8 |
mi4 re do2 |
R1*2 |
si,2 r |
red r |
mi4 r la r |
r2 r4 si |
mi2 r |
