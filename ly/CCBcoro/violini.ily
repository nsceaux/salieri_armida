\clef "treble"
<<
  \tag #'primo {
    mi'8.\mf fad'16 sol'8 |
    sol'8. la'16 si'8 si'8. do''16 si' la' |
    \grace la'8 sol'4. fad'4 si'8 |
    mi''\rf( re'' do'' do'' si' lad') |
    si'4 fad'8 sol'8. si'16 sol'8 |
    fad'4. r4 r8 |
    do''8.\mf re''16 do''8 do''4 do''8 |
    si'4. si' |
    la'8.\rf si'16 la'8 la'4 red''8\f |
    mi''4\fermata si'8 r4 r8 |
    do''8. si'16 la'8 sol'8. la'16 fad'8 |
    sol'8. la'16 si'8 si'8. mi''16 re''8 |
    do''8. si'16 la'8 sol'8. la'16 fad'8 |
    mi'4 mi'8 mi'8. fad'16 red'8 |
    mi'8. si'16 si'8.\trill la'32 si' do''2 |
  }
  \tag #'secondo {
    r4 r8 |
    mi'8.-\sug\mf fad'16 sol'8 fad'8. la'16 sol' fad' |
    \grace fad'8 mi'4. red'4 red'8 |
    mi'4-\sug\rf sol'8 mi'4 mi'8 |
    red'4 red'8 mi'8. sol'16 mi'8 |
    red'4. r4 r8 |
    mi'4-\sug\mf mi'8 fad'4 fad'8 |
    fad'8.\rf sol'16 fad'8 mi'4 mi'8 |
    mi'4 mi'8 red'4 la'8-\sug\f |
    sol'4.\fermata r4 r8 |
    la'8. sol'16 fad'8 mi'8. fad'16 red'8 |
    mi'8. fad'16 sol'8 sold'4. |
    la'8. sol'!16 fad'8 mi'8. fad'16 red'8 |
    mi'4 si8 do'4 si8 |
    sol4 fa' mi'2 |
  }
>>
R1*7 |
