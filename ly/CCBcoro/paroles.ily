\tag #'(voix1 voix2) {
  Ah mi -- se -- ra Re -- gi -- na
  che mai sa -- rà di tè
  che
  for -- se al -- la sua ro -- vi -- na
  for -- se al -- la sua ro -- vi -- na
  vol -- ge in -- fe -- li -- ce il piè,
  vol -- ge in -- fe -- li -- ce il piè.
}
\tag #'(ismene basse) {
  An -- dia -- mo, a -- mi -- che; in sì cru -- del mo -- men -- to
  non s’ab -- ban -- do -- ni al suo de -- stin. Si cer -- chi
  fi -- de e -- gual -- men -- te a dì fe -- li -- ci, e re -- i,
  o di sal -- var -- la, o di pe -- rir con le -- i.
}
