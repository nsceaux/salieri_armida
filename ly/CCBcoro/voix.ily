<<
  \tag #'voix1 {
    \clef "soprano/treble" r4 r8 |
    r4 si'8 si'8. do''16 si' la' |
    \grace la'8 sol'4. fad'4 si'8 |
    mi''[ re''] do'' do''[ si'] lad' |
    si'4. r4 r8 |
    si'4. r4 r8 |
    do''8. re''16 do''8 do''4 do''8 |
    si'4. si' |
    la'8. si'16 la'8 la'4 red''8 |
    mi''4\fermata si'8 r4 r8 |
    do''8. si'16 la'8 sol'[ la'] fad' |
    \grace fad'8 sol'4. r4 r8 |
    do''8. si'16 la'8 sol'([ la']) fad'8 |
    mi'4. r4 r8 |
  }
  \tag #'voix2 {
    \clef "alto/treble" r4 r8 |
    r4 sol'8 fad'8. la'16 sol' fad' |
    \grace fad'8 mi'4. red'4 red'8 |
    mi'4 sol'8 mi'4 mi'8 |
    red'4. r4 r8 |
    \ficta red'4. r4 r8 |
    mi'8. mi'16 mi'8 fad'4 fad'8 |
    fad'8.[ sol'16 fad'8] mi'4. |
    mi'8. mi'16 mi'8 red'4 la'8 |
    sol'!4\fermata sol'8 r4 r8 |
    la'8. sol'16 fad'8 mi'[ fad'] red' |
    \grace red'8 mi'4. r4 r8 |
    la'8. sol'16 fad'8 mi'([ fad']) red'8 |
    mi'4. r4 r8 |
  }
  \tag #'(ismene basse) { \clef "alto/treble" r4 r8 R2.*13 }
>>
<<
  \tag #'(voix1 voix2) R1*8
  \tag #'(ismene basse) {
    \tag #'basse { <>^\markup\character Ismene }
    r2 r8 sol' do'' si' |
    do'' do'' r do'' do'' do'' do'' la' |
    fad'! fad' fad' fad'16 sol' la'8 la' r16 la' do'' si' |
    sol'4 r8 re' sol' sol' sol' la'16 si' |
    si'8 fad'! r16 fad' fad' sol' la'8 la' r si' |
    sol' sol' si' do''16 re'' do''8 do'' r4 |
    do''8 la'16 la' la'8 sol' mi' mi' r4 |
    R1 |
  }
>>