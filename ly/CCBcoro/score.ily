\score {
  <<
    \new GrandStaff \with { \haraKiri \violiniInstr } <<
      \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
      \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
    >>
    \new ChoirStaff \with {
      \haraKiri
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 1 \super o } }
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps { Canto \concat { 2 \super o } }
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with {
        \haraKiriFirst
        instrumentName = \markup\character Ismene
        shortInstrumentName = \markup\character Is.
      } \withLyrics <<
        \global \keepWithTag #'ismene \includeNotes "voix"
      >> \keepWithTag #'ismene \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\center-column { Viola e Basso }
        shortInstrumentName = \markup\center-column { Vla. B. }
      } <<
        { s4. s2.*13 s1 \set Staff.shortInstrumentName = "B." }
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \origLayout {
          s4. s2.*3\break s2.*4\pageBreak
          s2.*4\break s2.*2 s1\pageBreak
          s1*2\break s1*2 s2\bar "" \break
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
