\clef "alto" mi'4\mf mi'8 |
mi'4 mi'8 red'4 red'8 |
mi'8( do' lad) si si' la'! |
sol'4(\rf mi'8) do'4 do'8 |
si4 si8 si4 si8 |
si4 si'8 si'8.\rf do''16 si'8 |
si'4 si'8 la'4 la'8 |
la'4 la'8 sol'8.\rf la'16 sol'8 |
sol'4 sol'8 fad'4 fad'8\f |
mi'4.\fermata r4 r8 |
la'4 la'8 si'4 si8 |
mi'4. r4 r8 |
la'4 la'8 si'4 si8 |
mi'4 sol'8 la'4 si'8 |
mi'4 re' do'2 |
R1*7 |
