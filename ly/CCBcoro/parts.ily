\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Coro
    \livretVerse#10 { Ah misera regina }
    \livretVerse#10 { Che sarà mai di te? }
    \livretVerse#10 { Forse alla sua rovina }
    \livretVerse#10 { Volge infelice il piè. }
  }
  \null
  \column {
    \livretPers Ismene
    \livretVerse#12 { Andiamo, amiche; In si crudel momento }
    \livretVerse#12 { Non si abbandoni al suo destin. Si cerchi }
    \livretVerse#12 { Fide egualmente a dì felici, e rei, }
    \livretVerse#12 { O di salvarla, o di perir con lei. }
  }
  \null
} #}))
