Mi -- se -- ro! chi mi scuo -- te? E qua -- li in que -- sto
bre -- ve son -- no af -- fan -- no -- so
tur -- ba -- no i -- dee fu -- ne -- ste il mio ri -- po -- so!
Oh mor -- te! or -- ri -- bil lar -- va! A -- gli oc -- chi mie -- i
qual po -- ter ti pre -- sen -- ta? e co -- me a -- pre -- si
a te -- mer -- ne l’a -- spet -- to? Un fred -- do ge -- lo
mi spar -- ge in se -- no, i fal -- li a me rin -- fac -- cia,
e il fer -- ro mi -- ci -- dial vi -- bra, e mi -- nac -- cia.
Qual’ in -- so -- li -- to or -- ror! quai nuo -- vi sen -- si
m’ag -- gi -- tan l’al -- ma… e qua -- le
mi fe -- ri -- sce lo sguar -- do
im -- prov -- vi -- so ba -- glior? l’ar -- me lu -- cen -- ti
chi rec -- cò? co -- me? quan -- do? e in es -- sa… Oh Dio!
Quan -- to da me di -- ver -- so!…
Mi ri -- co -- no -- sco ap -- pe -- na. A que -- sto se -- gno
av -- vi -- lir -- mi po -- tei
tra -- sfor -- mar -- mi co - %- sì?
