\clef "alto" la4(-\sug\p sib) r8 sib-! |
\ficta sib4-! sib-! sib2 |
r4 r8 \ficta sib-! sib4-! sib-! |
\ficta \once\tieDashed sib1-\sug\pp~ |
\ficta sib1~ |
sib~ |
sib2 mib'4 r8 mib-! |
mib4-! mib-! mib-! r |
mib'2:16-\sug\pp mib': |
\ficta mib': mib': |
\ficta mib': mib': |
\ficta mib':-\sug\f mib': |
\ficta mib'4 r re'-\sug\p r |
r2 mib'2-\sug\p~ |
mib'1~ |
mib' |
reb'-\sug\rf~ |
reb'4 r r2 |
r4 r8 do16\f re32 mi fa4 lab8.-! do'16-! |
fa'4 r r2 |
R1 |
do'2 r |
r4 r8 \once\slurDashed reb16( mib) fa4-! sib-! |
re! r r2 |
R1 |
r2 r4 r8 mib16( fa) |
sol4 sib8.-! mib'16-! mi!4-! r |
R1*2 |
r4 r8 do16 re32 mi fa4 la!8.-! do'16-! |
fad4 r r2 |
sol4\p r r2 |
fa!4\p r r si'!:32\p |
si'1: |
si'2 r |
do''4-! r r2 |
fad'2 r |
R1 |

