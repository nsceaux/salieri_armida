\clef "treble" la'4(\p sib') r8 sib-! |
\ficta sib4-! sib-! sib2 |
r4 r8 \ficta sib-! sib4-! sib-! |
\ficta sib1\pp~ |
\ficta sib~ |
sib~ |
sib2 mib'4 r8 mib'-! |
\ficta mib'4-! mib'-! mib'-! r |
<<
  \tag #'primo {
    sib'2:16\pp sib': |
    sib': sib': |
    sib': sib': |
    do'':\f reb'': |
    do''4 r si'!\p r |
    r2 do''4 r8 sol'' |
    do'''8(\rf sol'' mib'' do'') sol''( mib'' do'' sol') |
    mib'2 r4 r8 do''' |
    reb'''(\rf sib'' sol'' mi''!) sol''( mi'' reb'' sib') |
    sol'4 r r2 |
  }
  \tag #'secondo {
    sol'2:16-\sug\pp sol': |
    sol': sol': |
    sol': sol': |
    lab':-\sug\f sib': |
    \ficta lab'4 r sol'-\sug\p r |
    r2 \once\tieDashed <sol' do'>2-\sug\p~ |
    q1~ |
    q |
    <sib sol'>1-\sug\rf~ |
    q4 r r2 |
  }
>>
r4 r8 do'16\f re'32 mi' fa'4 lab'8.-! do''16-! |
fa''4 r r2 |
R1 |
<<
  \tag #'primo { la'!2 r | }
  \tag #'secondo { fa'2 r | }
>>
r4 r8 reb'16( mib') fa'4-! sib'-! |
re'!4 r r2 |
R1 |
r2 r4 r8 mib'16( fa') |
sol'4 sib'8.-! mib''16-! mi'!4-! r |
R1*2 |
r4 r8 do'16 re'32 mi' fa'4 la'!8.-! do''16-! |
fad'4 r r2 |
<<
  \tag #'primo {
    sib'4\p r r2 |
    si'!4\p r r re'':32\p |
    re''1: |
    re''2 r |
    do''4-! r r2 |
    re''2 r |
    
  }
  \tag #'secondo {
    re'4-\sug\p r r2 |
    re'4-\sug\p r r lab'':32-\sug\p |
    lab''1: |
    lab''2 r |
    sol''4-! r r2 |
    la'2 r |
  }
>>
R1 |
