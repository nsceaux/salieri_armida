\clef "soprano/treble" r4 r2 |
r2 sib'16 sib' sib'8 r fa'16 fa' |
re'8 re' r4 r2 |
r r8 fa' sib' la' |
\ficta sib'8 sib' r sib'16 sib' sib'8 sib' sib' do'' |
lab'4 lab' fa'8 fa'16 fa' fa'8 sol' |
lab' lab' r16 lab' sib' fa' sol'8 sol' r4 |
R1 |
r4 sib' mib''8 sib' r sib' |
\ficta sib'4 sib' sol'8 sol' r16 sol' sib'! lab' |
\ficta sib'8 sib' r sib'16 do'' reb''4 mib''8 sib' |
do'' do'' r4 r2 |
r8 do'' do'' re'' si'! si' r si'16 do'' |
re''4 si'8 sol' do'' do'' r4 |
R1 |
r4 do'' do'' do'' |
reb''2..( mi''8) |
sib'4 r4 r8 sib' reb'' do'' |
lab'8 lab' r4 r2 |
r4 r8 do'' lab' lab' sol' lab' |
fa' fa' r fa' la'! la' la' sib' |
do''4 mib''8 do'' r4 do''8 reb'' |
sib' sib' r4 r2 |
r4 r8 sib'16 sib' sib'4 fa'8 fa' |
re'4 r r8 sib' re'' fa'' |
fa''8 lab' lab' sib'16 fa' sol'8 sol' r4 |
r2 r4 r8 sol' |
do'' do'' r do''16 do'' do''4 do''8 re'' |
sib' sib' r sol'16 la'! sib'4 do''8 sol' |
la'!4 r r2 |
r4 la'8 la'16 re'' re''8 la' r re''16 la' |
sib'4 r re''8 re'' r4 |
si'!8 si' r sol' si' si' fa''4 |
re''2 re''8 re''16 re'' re''8 re'' |
si'!8 si' r re'' re'' mib'' fa'' mib'' |
do'' do'' r4 r8 do'' re'' mib'' |
re'' re'' r la'16 la' la'4 la'8 la' |
fad'4 r8 la'16 la' re''4 re''8 la' |
%sib'4
