\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretVerse#12 { Misero! chi mi scuote? Equale in questo, }
    \livretVerse#12 { Breve sonno affannoso }
    \livretVerse#12 { Turbano idée funeste il mio riposo! }
    \livretVerse#12 { Oh morte! orribil larva! Agli occhi miei }
    \livretVerse#12 { Qual poter ti presenta? e come appresi }
    \livretVerse#12 { A temerne l'aspetto? Un freddo gelo }
    \livretVerse#12 { Mi sparge in seno, i falli a me rinfaccia, }
    \livretVerse#12 { E il ferro micidial vibra, e minaccia. }
    \livretVerse#12 { Quale insolito orror! quai nuovi sensi }
  }
  \null
  \column {
    \livretVerse#12 { M'agitan l'alma… e quale }
    \livretVerse#12 { Mi ferisce lo sguardo }
    \livretVerse#12 { Improvviso baglior? l'arme lucenti }
    \livretVerse#12 { Chi recò? come? quando? e in essa… Oh Dio! }
    \livretVerse#12 { Quanto da me diverso!… }
    \livretVerse#12 { Mi riconosco appena. A questo segno }
    \livretVerse#12 { Avvilirmi potei }
    \livretVerse#12 { Trasformarmi così? }
  }
  \null
} #}))
