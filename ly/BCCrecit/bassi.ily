\clef "bass" la(\p sib) r8 sib,-! |
sib,?4-! sib,-! sib,2 |
r4 r8 sib,?-! sib,4-! sib,-! |
\once\tieDashed sib,?1\pp~ |
sib,1~ |
sib,~ |
sib,2 mib4 r8 mib-! |
mib?4-! mib-! mib-! r |
mib?2:16\pp mib: |
mib?: mib: |
mib?: mib: |
lab:\f sol: |
lab?4 r fa\p r |
r2 mib\p~ |
\once\tieDashed mib1~ |
mib2 r |
\once\tieDashed mi!1\rf~ |
mi4 r r2 |
r4 r8 do16\f re32 mi fa4 lab8.-! do'16-! |
fa'4 r r2 |
R1 |
mib2 r |
r4 r8 \once\slurDashed reb16( mib) fa4-! sib-! |
re! r r2 |
R1 |
r2 r4 r8 mib16( fa) |
sol4 sib8.-! mib'16 mi!4-! r |
R1*2 |
r4 r8 do16 re32 mi fa4 la!8.-! do'16-! |
fad4 r r2 |
sol4\p r r2 |
fa!4\p r r fa:32\p |
fa1: |
fa2 r |
mib4 r r2 |
fad2 r |
R1 |
%sol2
