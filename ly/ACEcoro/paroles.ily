Qual è la man che scuo -- te
l’An -- ti -- ca Reg -- gia a Plu -- to?
Qual’ è il po -- ter te -- mu -- to,
che noi da -- gli an -- tri or -- ri -- bi -- li
ri -- chia -- ma a’ rai del dì?
Stra -- zin le fie -- re Eu -- me -- ni -- di,
strug -- ga la fiam -- ma ul -- tri -- ce,
l’in -- cau -- to, e l’in -- fe -- li -- ce,
che pro -- vo -- car -- lo ar -- dì.
