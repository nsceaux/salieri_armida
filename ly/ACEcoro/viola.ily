\clef "alto" r4 r8. dod'16 |
dod2 r4 r16*1/2 dod32 red mi fad sold lad \ficta sid |
dod'4. red'8 mi'4. fad'8 |
sold'2\fermata r4 r8 dod' |
sold'16 sold' la'! la' sold' sold' fadd' fadd' sold' sold' fad' fad' mi' mi' red' red' |
mi' mi' fad' fad' mi' mi' red' red' mi' mi' re' re' dod' dod' sid sid |
dod' dod' si! si la la sold sold la la sold sold fad fad mid mid |
fad2:16 fadd:16 |
sold2 r4 r8 sold |
dod'4. dod'8 red'4 mi' |
sid2 dod'4 r8 sold |
dod'4 sold mi dod |
sold2.\fermata r8 sold' |
dod''16 dod'' red'' red'' mi'' mi'' red'' red'' dod'' dod'' si'! si' la' la' sold' sold' |
la' la' si' si' dod'' dod'' si' si' la' la' sold' sold' fad' fad' mid' mid' |
fad' fad' sold' sold' la' la' sold' sold' fad' fad' mi'! mi' red' red' dod' dod' |
sid sid sid sid sid sid dod' dod' red'4:16 mi':16 |
fad':16 sold':16 la':16 fadd':16 |
sold'2 r |
mi':16 mi':16 |
red':16 red':16 |
dod':16 dod':16 |
si:16 si:16 |
la:16 la:16 |
sold:16 mi:16 |
fad:16 sold:16 |
dod8\fermata r16 sold' sold'8. sold'16 fad'8[\fermata r16 fad'] fad'8. fad'16 |
\custosNote mi'16 \stopStaff
