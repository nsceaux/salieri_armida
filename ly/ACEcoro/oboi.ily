\clef "treble" r2 |
\twoVoices #'(primo secondo tutti) <<
  { dod''2\fermata }
  { mi'\fermata }
>> r4 r8 <>^"unis." sold' |
dod''4. red''8 mi''4. fad''8 |
sold''2\fermata r4 r8 dod'' |
sold'' la''! sold'' fadd'' sold'' fad'' mi'' red'' |
mi'' fad'' mi'' red'' mi'' re'' dod'' sid' |
dod'' si'! la' sold' la' sold' fad' mid' |
fad'2 fadd' |
sold' r |
R1*10 |
\twoVoices #'(primo secondo tutti) <<
  { si''1 |
    si'' |
    si''2 la''~ |
    la'' sold''~ |
    sold''4 la'' sold'' fadd'' |
    sold''2 dod'''4. mi''8 |
    fad''2 sold'' |
    dod''8\fermata }
  { sold''1 |
    fad'' |
    mi'' |
    red'' |
    dod'' |
    sid'2 sold'4. mi''8 |
    fad''2 sold'' |
    dod''8\fermata }
>> r16 \twoVoices #'(primo secondo tutti) <<
  { sold''16 sold''8. sold''16 la''8[\fermata r16 la''] la''8. la''16 |
    sold''2 }
  { mi''16 mi''8. mi''16 red''8[\fermata r16 red''] red''8. red''16 |
    mi''2 }
>>
\bar "|."
