\clef "treble" r4 r8. dod''16 |
<dod'' mi'>2\fermata r4 r16*1/2 dod'32 red' mi' fad' sold' lad' \ficta sid' |
dod''4. red''8 mi''4. fad''8 |
sold''2\fermata r4 r8 dod'' |
sold''16 sold'' la''! la'' sold'' sold'' fadd'' fadd'' sold'' sold'' fad'' fad'' mi'' mi'' red'' red'' |
mi'' mi'' fad'' fad'' mi'' mi'' red'' red'' mi'' mi'' re'' re'' dod'' dod'' sid' sid' |
dod'' dod'' si'! si' la' la' sold' sold' la' la' sold' sold' fad' fad' mid' mid' |
fad'2:16 fadd':16 |
sold'2 r4 r8 sold' |
dod''4. dod''8 red''4 mi'' |
sid'2 dod''4 r8 sold' |
dod''4 sold' mi' dod' |
sold2.\fermata r8 sold' |
dod''16 dod'' red'' red'' mi'' mi'' red'' red'' dod'' dod'' si'! si' la' la' sold' sold' |
la' la' si' si' dod'' dod'' si' si' la' la' sold' sold' fad' fad' mid' mid' |
fad' fad' sold' sold' la' la' sold' sold' fad' fad' mi'! mi' red' red' dod' dod' |
sid sid sid sid sid sid dod' dod' red' red' red' red' mi' mi' mi' mi' |
fad' fad' fad' fad' sold'4:16 la':16 fadd':16 |
sold'2 r |
<<
  \tag #'primo {
    mi'16 fad' sold' la'! si'! dod'' red'' mi'' mi' fad' sold' la' si' dod'' red'' mi'' |
    \ru#2 { red' mi' fad' sold' la' si' dod'' red'' } |
    \ru#2 { dod' red' mi' fad' sold' la' si' dod'' } |
    \ru#2 { si dod' red' mi' fad' sold' la' si' } |
    la si dod' red' mi' fad' sold' la' sold'4:16 fadd'16 fadd' fadd' fadd' |
    sold'2:16 dod''4:16 dod''16 dod'' mi' mi' |
  }
  \tag #'secondo {
    <<
      { si'2:16 si':16 |
        si':16 si':16 |
        si':16 la':16 |
        la':16 sold':16 |
        sold'4:16 la':16 sold':16 } \\
      { sold'2:16 sold':16 |
        fad':16 fad':16 |
        mi':16 mi':16 |
        red':16 red':16 |
        dod'4:16 dod':16 dod':16 }
    >> dod'4:16 |
    sid2:16 sold'4:16 sold'16 sold' mi' mi' |
  }
>>
fad'2:16 sold':16 |
dod'8\fermata r16 <<
  \tag #'primo {
    <dod'' sold''>16 q8. q16 <la''! si'!>8[\fermata r16 la''] <si' la''>8. q16 |
    \custosNote <si' sold''>16
  }
  \tag #'secondo {
    mi''16 mi''8. mi''16 red''8[\fermata r16 red''] red''8. red''16 |
    \custosNote mi''16
  }
>> \stopStaff
