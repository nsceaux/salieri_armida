\clef "bass" r2 |
dod2\fermata r4 r16*1/2 dod32 red mi fad sold lad \ficta sid |
dod'4. \clef "tenor" red'8 mi'4. fad'8 |
sold'2 r |
r r4 r8 red' |
mi' fad' mi' red' mi' re' dod' sid |
\clef "bass" dod' si! la sold la sold fad mi |
fad2 fadd |
sold r4 r8 sold |
dod'4. dod'8 red'4 mi' |
sid2 dod'4 r8 sold |
dod'4 sold mi dod |
sold,2\fermata r4 r8 sold |
dod' red' mi' red' dod' si! la sold |
la si dod' si la sold fad mid |
fad sold la sold fad mi! red dod |
sid,4. dod8 red4 mi |
fad sold la fadd |
sold2 r |
mi1 |
red |
dod |
si, |
la, |
sold,2 mi |
fad sold |
dod8\fermata r16 dod' dod'8. dod'16 si8[\fermata r16 si] si8. si16 |
\sug mi2 \bar "|."
