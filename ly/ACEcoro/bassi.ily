\clef "bass" r4 r8. dod'16 |
dod2\fermata \sug r4 r16*1/2 dod32 red mi fad sold lad \ficta sid |
dod'4. \clef "tenor" red'8 mi'4. fad'8 |
sold'2\fermata r4 r8 dod' |
sold' la' sold' fadd' sold' fad' mi' red' |
mi' fad' mi' red' mi' re' dod' sid |
\clef "bass" dod' si! la sold la sold fad mid |
fad2:16 fadd:16 |
sold2 r4 r8 sold |
dod'4. dod'8 red'4 mi' |
sid2 dod'4 r8 sold |
dod'4 sold mi dod |
sold,2\fermata r4 r8 sold |
dod' red' mi' red' dod' si! la sold |
la si dod' si la sold fad mid |
fad sold la sold fad mi! red dod |
sid,4. dod8 red4 mi |
fad sold la fadd |
sold2 r |
mi16 fad sold la si! dod' red' mi' mi fad sold la si dod' red' mi' |
\ru#2 { red mi fad sold la si dod' red' } |
\ru#2 { dod red mi fad sold la si dod' } |
\ru#2 { si, dod red mi fad sold la si } |
la, si, dod red mi fad sold la la, la la la la la la la |
sold2:16 mi:16 |
fad:16 \sug sold:16 |
dod8\fermata r16 dod' dod'8. dod'16 si8[\fermata r16 si] si8. si16 |
\custosNote mi16 \stopStaff
