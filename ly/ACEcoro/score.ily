\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr  } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
      \new GrandStaff \with { \tromboniInstr } <<
        \new Staff <<
          \global \keepWithTag #'primo-secondo \includeNotes "tromboni"
        >>
        \new Staff <<
          \global \keepWithTag #'terzo \includeNotes "tromboni"
        >>
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \with {
        instrumentName = \markup\smallCaps Tenore
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\smallCaps Basso
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s2 s1*2\pageBreak
        s1*3\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        s1*3\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
