<<
  \tag #'voix1 \clef "tenor/G_8"
  \tag #'(voix2 basse) \clef "bass"
>>
r2 |
R1*7 |
r2 r4 r8 sold |
dod'4. dod'8 red'4 mi' |
sid2 dod'4 r8 sold |
dod'4 sold mi dod |
sold,2 sold,4\fermata r8 sold |
dod'[ red'] mi'[ red'] dod'[ si!] la[ sold] |
la[ si] dod' r r4 r8 mid |
fad[ sold] la[ sold] fad[ mi!] red[ dod] |
sid,4. dod8 red4 mi |
fad sold la fadd |
sold2 r |
mi'4 mi'8 mi' mi'4 mi' |
red'4. red'8 red'2 |
dod'4 dod'8 dod' dod'4 dod' |
si!2 si4. si8 |
la4. la8 sold4. fadd8 |
sold2 dod'4. mi8 |
fad4. fad8 sold4. sold8 |
dod2 r |
s16
