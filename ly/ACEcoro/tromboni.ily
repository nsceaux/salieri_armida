\tag #'(primo primo-secondo) \clef "alto"
\tag #'secondo \clef "tenor/alto"
\tag #'terzo \clef "bass"
r2 |
<<
  \twoVoices #'(primo secondo primo-secondo) <<
    { sold'2\fermata }
    { dod'2\fermata }
  >>
  \tag #'terzo { dod2\fermata }
>> r2 |
<<
  \tag #'(primo secondo primo-secondo) {
    dod'4. red'8 mi'4. fad'8 |
    sold'2\fermata r4 r8 dod' |
    sold' la'! sold' fadd' sold' fad' mi' red' |
    mi' fad' mi' red' mi' re'! dod' sid |
    dod' si! la sold \twoVoices #'(primo secondo primo-secondo) <<
      { la'8 sold' fad' mid' | fad'2 fadd' | sold' }
      { la8 sold fad mid | fad2 fadd | sold }
    >>
  }
  \tag #'terzo {
    dod4. red8 mi4. fad8 |
    sold2\fermata r4 r8 dod |
    sold la! sold fadd sold fad mi red |
    mi fad mi red mi re dod sid, |
    dod' si! la sold la sold fad mid |
    fad2 fadd |
    sold
  }
>> r2 |
R1*10 |
<<
  \twoVoices #'(primo secondo primo-secondo) <<
    { si'1~ |
      si'~ |
      si'2 la'~ |
      la' sold' |
      sold'4 la' sold' fadd' |
      sold'2 dod''4. mi'8 |
      fad'2 sold' |
      dod' }
    { sold'1 |
      fad' |
      mi' |
      red' |
      dod' |
      sid2 sold4. mi'8 |
      mi'2 fad' |
      dod' }
  >>
  \tag #'terzo {
    mi1 |
    red |
    dod |
    si, |
    la, |
    sold,2 mi |
    fad2 sold |
    dod
  }
>> r2 |
r \bar "|."
