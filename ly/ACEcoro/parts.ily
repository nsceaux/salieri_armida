\piecePartSpecs
#`((oboi #:score-template "score-voix")
   (tromboni #:score-template "score-tromboni-voix")
   (fagotti
    #:music , #{

\quoteBassi "ACEbassi"
s2 s1*2 s2 s4.
\cue "ACEbassi" { <>^\markup\tiny Bassi \restInvisible s8 s2.. \restVisible }

                #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\column {

\livretVerse#12 { Qual è la man che scuote }
\livretVerse#10 { L’Antica Reggia a Pluto? }
\livretVerse#10 { Qual è il poter temuto, }
\livretVerse#10 { Che noi dagli antri orribili }
\livretVerse#10 { Richiama à rai del dì! }
\livretVerse#12 { Strazin le fiere Eumenidi, }
\livretVerse#10 { Strugga la fiamma ultrice, }
\livretVerse#10 { L'incauto, e l'infelice }
\livretVerse#10 { Che provocarlo ardì. }


} #}))
