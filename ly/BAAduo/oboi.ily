\clef "treble" r8 |
\override Staff.AccidentalSuggestion.avoid-slur = #'outside
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 do'''16 la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''8 }
  { R2 |
    r4 r8 do'''16 la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 \grace la''8 sol''8 fa''16 mi'' |
    fa''8 }
>> r8 r4 |
R2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4. \grace sib''8 la''16 sol''32 la'' | sib''8 }
  { do''4. fa''16 mi''32 fa'' | sol''8 }
>> r8 r4 |
R2*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4. \grace sib''8 la''16 sol''32 la'' | sib''8 }
  { do''4. fa''16 mi''32 fa'' | sol''8 }
>> r8 r4 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4 s8 do'''32 la''16. |
    \grace fad''8 sol''4 s8 do'''32 la''16. |
    sol''4 }
  { do''4 s8 la''32 fa''16. |
    \grace re''8 mi''4 s8 la''32 fa''?16. |
    mi''4 }
  { s4 r8 s\rf | s4 r8 s\rf | }
>> r4 |
R2*9 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { do'''4 sib'' | la'' }
  { la''4 sol'' | fa'' }
>> r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4 sib'' | la'' }
  { la''4 sol'' | fa'' }
  { s2\p | s4\f }
>> r4 |
R2*3 |
\twoVoices #'(primo secondo tutti) <<
  { fa''4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4. }
  { la'4 sib'8 sib' |
    la'4 sib'8 sib' |
    la'4. }
>> r8 |
R2*4 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { fa''4( mi'') |
    re'' mi'' |
    fa'' mi'' |
    re'' mi'' |
    s8 la''[ la''] s |
    s re''[ re''] s |
    s re''[ re'' mi''16 do''] |
    \grace do''8 si'4. }
  { re''4( do'') |
    si' do'' |
    re'' do'' |
    si' do'' |
    s8 do''[ do''] s |
    s8 la'[ la'] s |
    s la'[ la' la'] |
    re'4. }
  { s2-\sug\p |
    s2*2 |
    s2\f |
    r8 s4\p r8 |
    r8 s4 r8 |
    r8 }
>> r8 |
R2*5 | \allowPageTurn
r4 r16 <>\f \twoVoices #'(primo secondo tutti) <<
  { re''8 fa''16 | mi''4\fermata }
  { sol'8 re''16 | do''4\fermata }
>> r4 |
R2*3 | \allowPageTurn
<>\p \twoVoices #'(primo secondo tutti) <<
  { mi''2 | re'' | do''4 }
  { do''2 | si' | do''4 }
>> r4 |
R2*4 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { do''2~ | do''~ | do''~ | do'' | fa''8 }
  { do''2~ | do''~ | do''~ | do'' | la'8 }
>> r8 r4 |
R2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { do'''4( sib'') | la''8 }
  { la''4( sol'') | fa''8 }
>> r8 r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4( sib'') | la'' }
  { la''4( sol'') | fa'' }
  { s2 | s4\f }
>> r4 | \allowPageTurn
R2*3 |
\twoVoices #'(primo secondo tutti) <<
  { fa''4 fad''32 sol''16. sib''32 sol''16. |
    fa''!4 fad''32 sol''16. sib''32 sol''16. |
    fa''4 }
  { la'4 sib' | la' sib' | la' }
>> r4 | \allowPageTurn
R2*29 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { mi''!4. sol''16( fa'') |
    fa''4. la''16( sol'') |
    sol''4. sib''16( la'') |
    la''4~ la''16( sol'' fa'' mi'') |
    \grace mi''8 fa''4~ fa''16( mi'' re'' dod'') |
    re''16-! re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4 sol' |
    \grace sib''8 la''16( sold'') \grace sib''8 la''16( sold'') \grace sib''8 la''16( sol'') fa''( mi'') |
    \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''( dod''?) |
    re''16-! re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4 sol' |
    fa'4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4 }
  { sol'4. sib'16( la') |
    la'4. fa''16( mi'') |
    mi''4. sol''16( fa'') |
    fa''4~ fa''16( mi'' re'' dod'') |
    \ficta \grace dod''8 re''4~ re''16( \ficta do'' sib' la') |
    sib'16-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' |
    \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''( dod''?) |
    \grace mi''8 re''16( dod'') \grace mi''8 re''16( dod'') \grace mi''8 re''16( do'') sib'( la') |
    sib'16-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' |
    fa'8 la' sib'4 |
    la'8 la' sib'4 |
    la' }
  { s2\p | s2*2 | s4\f s16 s8.\p | s2*3 | s2\f |
  }
>> r4 |
