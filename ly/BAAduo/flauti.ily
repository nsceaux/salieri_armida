\clef "treble" r8 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 do'''16 la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''8 }
  { R2 |
    r4 r8 do'''16 la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 \grace la''8 sol''8 fa''16 mi'' |
    fa''8 }
>> r8 r4 |
R2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4. \grace sib''8 la''16 sol''32 la'' | sib''8 }
  { do''4. fa''16 mi''32 fa'' | sol''8 }
>> r8 r4 |
R2*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4. \grace sib''8 la''16 sol''32 la'' | sib''8 }
  { do''4. fa''16 mi''32 fa'' | sol''8 }
>> r8 r4 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4 s8 do'''32 la''16. |
    \grace fad''8 sol''4 s8 do'''32 la''16. |
    sol''4 }
  { do''4 s8 la''32 fa''16. |
    \grace re''8 mi''4 s8 la''32 fa''?16. |
    mi''4 }
  { s4 r8 s\rf | s4 r8 s\rf | }
>> r4 |
R2*9 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { do'''4 sib'' | la'' }
  { la''4 sol'' | fa'' }
>> r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4 sib'' | la'' }
  { la''4 sol'' | fa'' }
  { s2\p | s4\f }
>> r4 |
R2*3 |
\twoVoices #'(primo secondo tutti) <<
  { fa''4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4 fad''32( sol''16.) sib''32( sol''16.) |
    fa''!4. }
  { la'4 re''8 do'' |
    do''4 re''8 do'' |
    do''4. }
>> r8 |
R2*4 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { sol''2~ |
    sol''~ |
    sol''~ |
    sol'' |
    s8 la''[ la''] s |
    s re''[ re''] s |
    s re''[ re'' mi''16 do''] |
    \grace do''8 si'4. }
  { sol'2~ |
    sol'~ |
    sol'~ |
    sol' |
    s8 do''[ do''] s |
    s8 la'[ la'] s |
    s la'[ la' la'] |
    re'4. }
  { s2*4 | r8 s4\p r8 | r8 s4 r8 | r8 }
>> r8 |
R2*5 |
r4 r16 <>\f \twoVoices #'(primo secondo tutti) <<
  { re''8 fa''16 | mi''4\fermata }
  { sol'8 re''16 | do''4\fermata }
>> r4 |
R2*3 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { mi''2 | re'' | do''4 }
  { do''2 | si' | do''4 }
>> r4 |
R2*3 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 do'''16\p la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 }
  { R2 |
    r4 r8 do'''16 la'' |
    fa''4 sol'' |
    la'' \grace do'''8 sib'' la''16 sol'' |
    fa''4 \grace la''8 sol''8 fa''16 mi'' |
    fa''4 }
>> r4 |
R2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { do'''4( sib'') | la''8 }
  { la''4( sol'') | fa''8 }
>> r8 r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { do'''4( sib'') | la'' }
  { la''4( sol'') | fa'' }
  { s2 | s4\f }
>> r4 | \allowPageTurn
R2*3 |
\twoVoices #'(primo secondo tutti) <<
  { fa''4 fad''32 sol''16. sib''32 sol''16. |
    fa''!4 fad''32 sol''16. sib''32 sol''16. |
    fa''4 }
  { la'4 sib' | la' sib' | la' }
>> r4 |
R2*29 |
\twoVoices #'(primo secondo tutti) <<
  { s8 mi''16. sol''32 mi''4 |
    s8 fa''16. la''32 fa''4 |
    s8 sol''16. sib''32 sol''4 |
    la'' }
  { s8 do''16. mi''32 do''4 |
    s8 la'16. do''32 la'4 |
    s8 mi''16. sol''32 mi''4 |
    fa'' }
  { r8 s4.\p | r8 s4. | r8 s4. | }
>> r4 |
R2*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { \grace sib''8 la''16( sold'') \grace sib''8 la''16( sold'') \grace sib''8 la''16( sol'') fa''( mi'') |
    \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''( dod''?) |
    re''16-! re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4 sol' | }
  { \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''(  dod''?) |
    \grace mi''8 re''16( dod'') \grace mi''8 re''16( dod'') \grace mi''8 re''16( do'') sib'( la') |
    sib'16-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' | }
>>
\twoVoices #'(primo secondo tutti) <<
  { fa'4 fad''32 sol''16. sib''32 sol''16. |
    fa''!4 fad''32 sol''16. sib''32 sol''16. |
    fa''!4 }
  { fa'4 re''8 do'' |
    do''4 re''8 do'' |
    do''4 }
>> r4 |
