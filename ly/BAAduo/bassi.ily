\clef "bass" r8 |
\override AccidentalSuggestion.avoid-slur = #'outside
<>_\markup\center-align { \italic non troppo \dynamic f } fa4 r |
fa r |
do' do8 r |
R2*4 |
fa4 r |
fa r |
do'2:16\f do':16\p |
do'4:16 fa8 r |
R2 |
do'2:16\p do':16 |
do'16 do' do' do' fa8 r |
R2 |
do'2\fp |
do\fp |
do,4. r8 |
fa4\p r |
fa r |
do' do8 r |
\ru#4 { fa r do r | }
fa4 r |
fa8 r sib r |
r do' do' do |
fa4 r |
fa8 r sib r |
r do' do' do |
r fa fa, r |
R2 |
r8 sib, sib sib |
do'4 do |
\tieDashed fa2~ |
fa~ |
fa4. \tieSolid r8 |
r8 sib(\p la sol) |
fa4 fa, |
fa fa8\mf fa |
fa r fa, r |
sol2\p~ |
sol~ |
sol~ |
sol4\f do' |
r8 fa[\p fa,] r |
r fa[ fa] r |
r fa[ fa fa] |
sol4 r |
sol8 r sol, r |
do r do, r |
sol r sol, r |
do r do, r |
fa r sol r |
la\mf r si\f r |
do'4\fermata r |
fa\p r |
fa r |
mi8\f r r la |
sol4\p r |
sol, r |
do'16(\f re') do'( re') sib!( do') sib( do') |
la( sib) la( sib) sol( la) sol( la) |
fa4\p r |
fa r |
do' do8 r |
R2*4 | \allowPageTurn
fa4\mf r |
fa8\p r sib r |
r do' do' do |
fa4\mf r |
fa8 r sib r |
r do' do' do |
r fa\f fa,\p fa |
R2 |
r8 sib sib sib |
do' do' do do |
fa4\mf r |
fa\mf r |
fa, r |
sib,\p r |
fa r |
sol8 \ficta mib fa fa, |
sib, sib sib, r |
sib2\f |
sol\p |
fa4 r |
fad r |
sol r |
fad r |
sol r |
la2\fermata |
sib4 r |
re16\p re re re re\f re re re |
\ficta mib mib mib mib mib\p mib mib mib |
fa fa fa fa fa, fa, fa, fa, |
sib,( do re \ficta mib fa sol la sib) |
re\p re re re re\f re re re |
\ficta mib mib mib mib mib\p mib mib mib |
fa fa fa fa fa, fa, fa, fa, |
sib,8. sib16 \grace do' sib8.\f la32 sol |
fa8 fa, fa r |
fa\p r fa, r |
fa r fa, r |
fa r fa, r |
fa fa fa, r |
r fa\f fa fa |
r la la la |
r sib sib, r |
r do\p do, r |
r do do, r |
r do do, r |
fa4\f r |
R2 |
r8 sib\p sib sib |
do'4 do |
fa4.\f r8 |
R2 |
r8 sib sib sib |
do'4 do |
fa r |
fa r |
fa, r |
