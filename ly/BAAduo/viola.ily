\clef "alto" r8 |
\override AccidentalSuggestion.avoid-slur = #'outside
<>_\markup\whiteout\center-align { \italic non troppo \dynamic f }
fa'4 r |
fa' r |
do' do8 r |
R2*4 |
fa'4 r |
fa r |
do''2:16\f |
do'':16-\sug\p |
do''4:16 fa'8 r |
R2 |
do''2:16\p |
do'':16 |
do''4:16 fa'8 r |
R2 |
do''2-\sug\fp |
do'-\sug\fp |
do4. r8 |
fa4-\sug\p r |
fa r |
do' do8 r |
fa'8-. do'-. mi'-. do'-. |
fa'8-. do'-. sol'-. do'-. |
la'-. do'-. mi'-. do'-. |
fa'8-. do'-. sol'-. do'-. |
fa'4 r |
fa8 r sib r |
r do' do' do |
fa4 r |
fa8 r sib r |
r do' do' do |
fa4 r |
R2 |
r8 sib sib sib |
do'4 do |
fa sib8 sib |
la4 sib8 sib |
la4. r8 |
r sib(-\sug\p la sol) |
fa4 fa' |
fa fa8-\sug\mf fa |
fa8 r fa' r |
sol'2\p~ |
sol'~ |
sol' |
sol'4\f do' |
r8 fa'[-\sug\p fa] r |
r8 fa'[ fa'] r |
r fa'[ fa' fa'] |
sol'4. r8 |
re'16-. do'-. si-. la-. sol-. la-. si-. do'-. |
do'-. re'-. mi'-. fa'-. mi'-. do'-. mi'-. do'-. |
re'-. si-. la-. sol-. sol-. si-. do'-. re'-. |
do'-. re'-. mi'-. fa'-. mi'-. do'-. mi'-. do'-. |
fa8 r sol r |
la-\sug\mf r si-\sug\f r |
do'4\fermata r |
fa-\sug\p r |
fa' r |
mi'8-\sug\f r r la |
sol4-\sug\p r |
sol r |
do' r |
do'16(\f re') do'( re') sib( do') sib( do') |
la4\p r |
fa r |
do' do8 r |
R2*4 | \allowPageTurn
fa'4-\sug\mf r |
fa8-\sug\p r sib r |
r do' do' do |
fa4-\sug\mf r |
fa8 r sib r |
r8 do' do' do |
r fa'-\sug\f fa-\sug\p r |
R2 |
r8 sib sib sib |
do' do' do do |
fa-\sug\mf la sib4 |
la8-\sug\mf la sib4 |
la r |
sib-\sug\p r |
fa' r |
sol'8 \ficta mib' fa' fa |
sib sib' sib r |
sib'2\f |
do''\p |
do''4 r |
re'4 r |
re' r |
re' r |
re' r |
la2\fermata |
sib4 r |
r fa'\f |
\ficta mib'16 mib' mib' mib' mib'4:16\p |
fa':16 fa:16 |
sib16( do' re' \ficta mib' fa' sol' la' sib') |
r4 fa'\f |
\ficta mib'2:16 |
fa'4:16\p fa:16 |
sib8. sib16 sib8.\trill\f la32 sol |
fa8 fa' fa r |
fa'\p r fa r |
fa' r fa r |
fa' r fa r |
r fa' fa r |
r fa\f fa fa |
r la la la |
r sib sib r |
r do'\p do r |
r do' do r |
r do' do r |
la'4-\sug\f~ la'16( sol'-\sug\p fa' mi') |
\grace mi'8 fa'4~ fa'16( mi' re' dod') |
re'16-! re'( mi' fa') \grace mi'8 re' do'16 sib |
la4 sol |
la'16(\f sold') la'( sold') la'( sol') fa'( mi') |
fa'( mi') fa'( mi') fa'( mi') re'( \ficta dod') |
re'-! re'( mi' fa') mi' re' do' sib |
la8 do' do4 |
fa8 la sib4 |
la8 la sib4 |
la r |
