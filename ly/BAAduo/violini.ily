\clef "treble"
\override AccidentalSuggestion.avoid-slur = #'outside
<>_\markup\whiteout\center-align { \italic non troppo \dynamic f }
<<
  \tag #'primo {
    do''8 |
    do''4 fa''16( mi'' re'' do'') |
    do''4 re''16( do'' sib' la') |
    la'8.( sib'16) sol'8-. r |
  }
  \tag #'secondo {
    la'8 |
    la'4 re''16( do'' sib' la') |
    la'4 sib'16( la' sol' fa') |
    fa'8.( sol'16) mi'8-. r |
  }
>>
fa'8 do' mi' do' |
fa' do' sol' do' |
la' do' mi' do' |
fa' do' sol' do' |
<<
  \tag #'primo {
    fa'16 la''-. sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    la'-. do'''-. sib''-. la''-. sol''-. fa''-. mi''-. re''-. |
    do''4 r |
    r8 \grace do'''16 sib''\f( la''32 sib'') \grace sib''16 la''8\p sol''16 fa'' |
    mi''8.( fa''32 sol'') fa''16.[ sol''32] \grace la''16 sol''32[\f fad'' sol''16] |
    la''8
  }
  \tag #'secondo {
    fa'16 fa''-. mi''-. re''-. do''-. sib'-. la'-. sol'-. |
    fa'-. la''-. sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    do''4 r |
    r8 sol''16(-\sug\f fad''32 sol'') sol''16(-\sug\p \ficta fa'') sib'( la') |
    sol'8.( la'32 sib') la'16. do''32 do''8-\sug\f |
    fa''8
  }
>> \grace re'''8 do'''32[ si'' do'''16] \grace re'''8 do'''32[ si'' do'''16] \grace re'''8 do'''32[ si'' do'''16] |
do'''4 r |
<<
  \tag #'primo {
    r8 \grace do'''8 sib''!16(\f la''32 sib'') \grace sib''8 la'' sol''16 fa'' |
    mi''8.(\p fa''32 sol'') fa''16.[ sol''32] \grace la''8 sol''32[ \ficta fad'' sol''16] |
    la''8
  }
  \tag #'secondo {
    r8 \grace la'' sol''16(-\sug\f fad''32 sol'') \grace sol''8 \ficta fa'' sib'16( la') |
    sol'8.(-\sug\p la'32 sib') la'16. do''32 do''8 |
    fa''8
  }
>> \grace re'''8 do'''32[ si'' do'''16] \grace re'''8 do'''32[ si'' do'''16] \grace re'''8 do'''32[ si'' do'''16] |
do'''4 <<
  \tag #'primo {
    sold''32\rf( la''16.) do'''32(\rf la''16.) |
    \grace fad''8 sol''4\rf sold''32(\rf la''16.) do'''32( la''16.) |
    sol''4 r8 do''\p |
    do''4 fa''16( mi'' re'' do'') |
    do''4 re''16( do'' sib' la') |
    la'8. sib'16 sol'8 do'''16( la'') |
    fa''4_\markup\italic dolce sol''( |
    la'') \grace do'''8 sib'' la''16 sol'' |
    fa''4 \grace do'''8 sib'' la''16 sol'' |
    fa''4 \grace la''8 sol'' fa''16 mi'' |
    fa''-.\mf la''-. sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    la'8 do'' \grace dod'' re''(\rf fa''16 re'') |
    do''4\p sib' |
    la'16 la''-.\mf sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    la'8 do'' \grace dod'' re''(\rf fa''16 re'') |
    do''4(\p sib') |
    <la' la''>4\f~ la''16( sol''\p fa'' mi'') |
    \grace mi''8 fa''4~ fa''16( mi'' re'' dod'') |
    re''16-. re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4( sol') |
    fa'4 fad'32(_\markup\italic dol. sol'16.) sib'32( sol'16.) |
    fa'4 fad'32(\rf sol'16.) sib'32( sol'16.) |
    fa'!4. r8 |
    r8 fa'4-\sug\p fa'8 |
    fa'8(\rf fa'' mi'' re'') |
    do''8. do''16 \grace mi''8 re''32(\mf do''16.) \grace mi''8 re''32( do''16.) |
    do''8.( re''32 mi'') fa''16-. mi''-. re''-. do''-. |
    si'16(\p sol' re' sol') mi'( sol' do'' sol') |
  }
  \tag #'secondo {
    mi''32\rf( fa''16.) la''32(\rf fa''16.) |
    \grace re''8 mi''4\rf mi''32(-\sug\rf fa''16.) la''32( fa''16.) |
    mi''4 r8 la'-\sug\p |
    la'4 re''16( do'' sib' la') |
    la'4 sib'16( la' sol' fa') |
    fa'8. sol'16 mi'8 r |
    r4 r8 do'''16( la'') |
    fa''4( sol'') |
    fa''( sol'') |
    la''4 \grace do'''8 sib'' la''16 sol'' |
    fa''-.-\sug\mf fa''-. mi''-. re''-. do''-. sib'-. la'-. sol'-. |
    fa'8 la' \grace la' sib'(-\sug\rf re''16 sib') |
    la'4-\sug\p sol' |
    fa'16 fa''-.-\sug\mf mi''-. re''-. do''-. sib'-. la'-. sol'-. |
    fa'8 la' \grace la' sib'-\sug\rf( re''16 sib') |
    la'4(-\sug\p sol') |
    fa'8\f fa''~ fa''16( mi''-\sug\p re'' dod'') |
    \grace dod''8 re''4~ re''16( \ficta do'' sib' la') |
    sib'16-. sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4( mi') |
    fa'4 re'8 do' |
    do'4 re'8 do' |
    do'4. r8 |
    r re'(-\sug\p do' sib) |
    la8(-\sug\rf re'' do'' sib') |
    la'8. la'16 \grace do''8 sib'32(-\sug\mf la'16.) \grace do''8 sib'32( la'16.) |
    la'8.( sib'32 sol') la'16-. sol'-. fa'-. mi'-. |
    re'16(-\sug\p sol' re' sol') mi'( sol' do'' sol') |
  }
>>
si'16( re'' fa'' re'') fa''( mi'' re'' do'') |
si'( fa' re' fa') mi'( sol' do'' sol') |
si'(\f re'' fa'' re'') fa''( mi'' re'' do'') |
<<
  \tag #'primo {
    r8 la'[(-.\p la'-.)] r |
    r8 re''([-. re'']-.) r |
    r re''[ re'' mi''16 do''] |
    \grace do''8 si'4. r8 |
    r4 r16 sol''-.( sol''-. sol''-.) |
    sol''8.( la''32 fad'') sol''8 r |
    r4 r16 sol'' sol'' sol'' |
    sol''8. la''32 fad'' sol''8 r |
    r16 la'8( do''16) r si'8( re''16) |
    r16 do''8(\mf mi''16) r re''8(\f fa''16) |
    mi''4\fermata r |
    la'8 r sold'32(\mf la'16.) sold'32( la'16.) |
    r4 dod''32(\mf re''16.) dod''32( re''16.) |
    fad''16(\f sol'') red''( mi'') si'( do'') la' fad' |
    sol'2\p~ |
    sol' |
    mi'4 r |
    r4 r8 do''\p |
    do''4 fa''16( mi'' re'' do'') |
    do''4 re''16( do'' sib' la') |
    la'8. sib'16 sol'8 r |
  }
  \tag #'secondo {
    r8 do'([-.\p do']-.) r |
    r la'-.([ la']-.) r |
    r8 la'[ la' la'] |
    \grace mi' re'4. r8 |
    fa'16-. mi'-. re'-. do'-. si-. do'-. re'-. mi'-. |
    mi'-. fa'-. sol'-. la'-. sol'-. mi'-. sol'-. mi'-. |
    fa'16-. re'-. do'-. \ficta si-. si-. re'-. mi'-. fa'-. |
    mi'-. fa'-. sol'-. la'-. sol'-. mi'-. sol'-. mi'-. |
    r16 fa'8 fa'16 r fa'8 fa'16 |
    r mi'8\mf do''16 r sol'8\f sol'16 |
    sol'4\fermata r |
    do'8 r si32(-\sug\mf do'16.) si32( do'16.) |
    r4 mi'32(-\sug\mf fa'16.) mi'32( fa'16.) |
    R2 |
    fad'16(\p sol') red'( mi') si( do') red'( mi') |
    mi'( fa'!) dod'( re') la( si) do'( re') |
    do'4 r |
    r4 r8 la'-\sug\p |
    la'4 re''16( do'' sib' la') |
    la'4 sib'16( la' sol' fa') |
    fa'8. sol'16 mi'8 r |
  }
>>
fa'8-. do'-. mi'-. do'-. |
fa' do' sol' do' |
la' do' mi' do' |
fa' do' sol' do' |
<<
  \tag #'primo {
    fa'16\mf la''-. sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    la'8 do'' \grace dod'' re''\rf fa''16( re'') |
    do''4( sib') |
    la'16\mf la''-. sol''-. fa''-. mi''-. re''-. do''-. sib'-. |
    la'8 do'' \grace dod'' re''\rf fa''16( re'') |
    do''4( sib') |
    <la' la''>4\f~ la''16( sol''\p fa'' mi'') |
    \grace mi''8 fa''4~ fa''16( mi'' re'' dod'') |
    re''16-! re''( mi'' fa'') \grace mi''8 re''( do''16 sib') |
    la'4 sol' |
    fa'\mf fad'32 sol'16. sib'32 sol'16. |
    fa'4\mf fad'32( sol'16.) sib'32( sol'16.) |
    fa'4 r |
    fa''8(_\markup\italic dol. \ficta mib''16 re'' re''8 re'') |
    re''8. \ficta mib''16 do''4 |
    sib'8( do'' re'' \ficta mib''16 do'') |
    \grace do''8 sib' sib' r re''16 \ficta mib'' |
    fa''2\f |
    mi''!\p |
    fa''4 r |
    R2 |
    sib'32[(_\markup\italic scherzando re'') re''( fad'')] fad''[( sol'') sol''( la'')] la''[( sib'') sib''( la'')] sol''[( fa'') \ficta mib''( re'')] |
    re''4 r |
    sib'32[( re'') re''( fad'')] fad''[( sol'') sol''( la'')] la''[( sib'') sib''( la'')] sol''[( fa'') \ficta mib''( re'')] |
    do''2\fermata |
    fa'4 r |
  }
  \tag #'secondo {
    fa'16-\sug\mf fa''-. mi''-. re''-. do''-. sib'-. la'-. sol'-. |
    fa'8 la' \grace la' sib'-\sug\rf re''16( sib') |
    la'4( sol') |
    fa'16-\sug\mf fa''-. mi''-. re''-. do''-. sib'-. la'-. sol'-. |
    fa'8 la' \grace la' sib'-\sug\rf re''16( sib') |
    la'4 sol' |
    fa'8-\sug\f fa''~ fa''16( mi''-\sug\p re'' dod'') |
    \grace dod''8 re''4~ re''16( do'' sib' la') |
    sib'-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' |
    fa'-\sug\mf re'8 do' |
    do'4-\sug\mf re'8 do' |
    do'4 r |
    re''8(_\markup\tiny\italic dol. do''16 sib' sib'8 sib') |
    sib'8. do''16 la'4 |
    re'8( la' sib' sol'16 \ficta mib') |
    \ficta \grace mib'8 re' re' r4 |
    re''2\f |
    sib'\p |
    la'4 r8 \ficta mib'16 re' |
    re'16( fad' la' sol') fad'( mib' re' do') |
    sib4 r8 \ficta mib'16 re' |
    re'( fad' la' sol') fad'( mib' re' do') |
    sib4 r |
    fa''2\fermata |
    re''4 r |
  }
>>
sib'4\p <sib'' sib'>4\f |
<<
  \tag #'primo {
    \grace la''8 sol''4.( fa''16\p \ficta mib'') |
    re''4. do''8 |
    \grace do''8 re''4 r |
    
  }
  \tag #'secondo {
    \grace fa''8 \ficta mib''4.( re''16-\sug\p do'') |
    sib'4. la'8 |
    \grace la'8 sib'4 r |
  }
>>
sib'4 <sib' sib''>4\f |
<<
  \tag #'primo {
    \grace la''8 sol''4.( fa''16\p \ficta mib'') |
    re''4. do''8 |
    sib'4
    
  }
  \tag #'secondo {
    \grace fa''8 \ficta mib''4.( re''16-\sug\p do'') |
    sib'4 la' |
    sib'
  }
>> sib'8.\trill\f la'32 sol' |
fa'4 r8 <<
  \tag #'primo {
    do''8\p |
    re''( mi''! fa'' mi''16 re'') |
    \grace re''8 do''8.( sib'16 la'8 do'') |
    re''( mi'' \grace sol'' fa'' mi''16 re'') |
    \grace re''8 do''4 r8 do''16 re'' |
    mib''4\mf~ mib''16( fa'') fa''( sol'') |
    sol''8(\rf fa''4 mib''8) |
    \ficta mib''( re'') r fa''16(\p mi'') |
    mi''4.\p sol''16( fa'') |
    fa''4. la''16( sol'') |
    sol''4. sib''16( la'') |
    <la' la''>4\f~ la''16( sol''\p fa'' mi'') |
    \grace mi''8 fa''4~ fa''16( mi'' re'' dod'') |
    re''16-! re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4 sol' |
    \grace sib''8 la''16(\f sold'') \grace sib''8 la''16( sold'') \grace sib''8 la''16( sol'') fa''( mi'') |
    \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''( \ficta dod'') |
    re''16-! re''( mi'' fa'') \grace mi''8 re'' do''16 sib' |
    la'4 sol' |
    fa'4 fad'32( sol'16.) sib'32( sol'16.) |
    fa'!4 fad'32( sol'16.) sib'32( sol'16.) |
    fa'!4 r |
  }
  \tag #'secondo {
    la'8-\sug\p |
    sib'( do'' re'' do''16 sib') |
    \grace sib'8 la'8.( sol'16 fa'8 la') |
    sib'( do'' \grace mi''8 re'' do''16 sib') |
    \grace sib'8 la'4 r8 la'16 sib' |
    do''4-\sug\mf~ do''16( re'') re''( mib'') |
    \ficta mib''8-\sug\rf( re''4 do''8) |
    do''( sib') r la'16-\sug\p( sol') |
    sol'4.\p sib'16( la') |
    la'4. fa''16( mi'') |
    mi''4. sol''16( fa'') |
    fa''4-\sug\f~ fa''16( mi''-\sug\p re'' dod'') |
    \ficta \grace dod''8 re''4~ re''16( \ficta do'' sib' la') |
    sib'16-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' |
    \grace sol''8 fa''16(\f mi'') \grace sol''8 fa''16( mi'') \grace sol''8 fa''16( mi'') re''( \ficta dod'') |
    \grace mi''8 re''16( dod'') \grace mi''8 re''16( dod'') \grace mi''8 re''16( do'') sib'( la') |
    sib'16-! sib'( do'' re'') \grace do''8 sib' la'16 sol' |
    fa'4 mi' |
    fa' re'8( do') |
    do'4 re'8( do') |
    do'4 r |
  }
>>
