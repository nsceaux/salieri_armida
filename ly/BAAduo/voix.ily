\override AccidentalSuggestion.avoid-slur = #'outside
<<
  \tag #'(voix1 basse) {
    \clef "soprano/treble" r8 |
    R2*19 |
    r4 r8 do'' |
    do''4\melisma fa''16[ mi'']\melismaEnd re''[ do''] |
    do''4\melisma re''16[ do'']\melismaEnd sib'[ la'] |
    la'8.[ sib'16] sol'8 do''16[ la'] |
    fa'4 sol' |
    la'4 \grace do''8 sib'[ la'16 sol'] |
    fa'4( sol') |
    la'4 \grace do''8 sib'[ la'16 sol'] |
    fa'4 r |
    r8 do'' \appoggiatura dod'' re'' fa''16[ re''] |
    do''4 sib' |
    la'4 r |
    r8 do'' \appoggiatura dod'' re'' fa''16[ re''] |
    do''4 sib' |
    la''4~ la''16[ sol''] fa''[ mi''] |
    \grace mi''8 fa''4~ fa''16[\melisma mi'' re'' dod''] |
    re''-![ re''( mi'' fa'')]\melismaEnd \grace mi''8 re''[ do''16 sib'] |
    la'4 \grace do''8 sib'[ la'16 sol'] |
    fa'4 r |
  }
  \tag #'voix2 {
    \clef "soprano/treble" r8 |
    R2*19 |
    r4 r8 la' |
    la'4\melisma re''16[ do'']\melismaEnd sib'[ la'] |
    la'4\melisma sib'16[ la']\melismaEnd sol'[ fa'] |
    fa'8.[ sol'16] mi'8 r |
    r4 r8 do''16[ la'] |
    fa'4 sol' |
    la'4 \grace do''8 sib'8[ la'16 sol'] |
    fa'8 la' \grace la'8 sol'[ fa'16 mi'] |
    fa'4 r |
    r8 la' \appoggiatura la' sib' re''16[ sib'] |
    la'4 sol' |
    fa'4 r |
    r8 la' \appoggiatura la' sib' re''16[ sib'] |
    la'4 sol' |
    fa''~ fa''16[ mi''] re''([ dod'']) |
    \ficta \grace dod''8 re''4~ re''16[ \ficta do'' sib' la']\melisma |
    sib'16[-! sib'( do'' re'')]\melismaEnd \grace do''8 sib'[ la'16 sol'] |
    fa'4 \grace la'8 sol'[ fa'16 mi'] |
    fa'4 r |
  }
>>
<<
  \tag #'voix1 { R2*27 }
  \tag #'(voix2 basse) {
    R2 |
    r4 r8 do''16[ la'] |
    fa'2~ |
    fa'8 fa'' mi'' re'' |
    do'' do'' r4 |
    r r8. do''16 |
    si'4 do'' |
    \grace { re''16[ mi''] } fa''4~ fa''16 mi''[ re'' do''] |
    si'4 do'' |
    r4 r8 do'' |
    si'16 la' la'8. si'16[ do'' dod''] |
    re''4. fa''16[ mi''] |
    re''4. mi''16[ do''] |
    \grace do''8 si'4 r |
    sol'8 sol'16 sol' sol'8 sol' |
    sol'8.[ la'16] sol'8 r |
    re''16[ si'] la' sol' sol'8 sol' |
    sol'8.[ la'16] sol'4 |
    \grace si'8 la' la'16 do'' \grace do''8 si'! si'16[ re''] |
    \appoggiatura re''8 do'' do''16[ mi''] \appoggiatura mi''8 re'' re''16[ fa''] |
    mi''16[\fermata fa'' \grace sol''8 fa''16 mi''32 fa'']\melisma sol''16[ mi'' re'' do'']\melismaEnd |
    si'16 la' la'8 r4 |
    mi''16 re'' re''8 r4 |
    fad''16[ sol''] red''[\melisma mi''] si'[ do'']\melismaEnd la'[ fad'] |
    sol'2\melisma re''\melismaEnd |
    do''4 r |
  }
>>
<<
  \tag #'(voix1 basse) {
    r4 r8 do'' |
    do''4\melisma fa''16[ mi'']\melismaEnd re''[ do''] |
    do''4\melisma re''16[ do'']\melismaEnd sib'[ la'] |
    la'8.[ sib'16] sol'8 r |
    r4 r8 do''16[ la'] |
    fa'4 sol' |
    la'4 \grace do''8 sib'8[ la'16 sol'] |
    fa'8. la'16 \grace la'8 sol'[ fa'16 mi'] |
    fa'4 r |
    r8 do'' \appoggiatura dod'' re'' fa''16[ re''] |
    do''4 sib' |
    la'4 r |
    r8 do'' \appoggiatura dod'' re'' fa''16[ re''] |
    do''4 sib' |
    la''4~ la''16[ sol''] fa''[ mi''] |
    \grace mi''8 fa''4~ fa''16[\melisma mi'' re'' dod''] |
    re''-![ re''( mi'' fa'')]\melismaEnd \grace mi''8 re''[ do''16 sib'] |
    la'4 sol' |
    fa'4 r |
  }
  \tag #'voix2 {
    r4 r8 la' |
    la'4\melisma re''16[ do'']\melismaEnd sib'[ la'] |
    la'4\melisma sib'16[ la']\melismaEnd sol'[ fa'] |
    fa'8.[ sol'16] mi'8 do''16[ la'] |
    fa'4 sol' |
    la'4 \grace do''8 sib'[ la'16 sol'] |
    fa'4( sol') |
    la'4 \grace do''8 sib'[ la'16 sol'] |
    fa'4 r |
    r8 la' \appoggiatura la' sib' re''16[ sib'] |
    la'4 sol' |
    fa'4 r |
    r8 la' \appoggiatura la' sib' re''16[ sib'] |
    la'4 sol' |
    fa''~ fa''16[ mi''] re''([ dod'']) |
    \ficta \grace dod''8 re''4~ re''16[ \ficta do'' sib' la']\melisma |
    sib'16[-! sib'( do'' re'')]\melismaEnd \grace do''8 sib'[ la'16 sol'] |
    fa'4 mi' |
    fa'4 r |
  }
>>
<<
  \tag #'(voix1 basse) {
    R2*2 |
    fa''8 \ficta mib''16 re'' re''8 re'' |
    re''8.[ \ficta mib''16] do''4 |
    sib'8 do'' re'' \ficta mib''16 do'' |
    \grace do''8 sib' sib' r re''16[ \ficta mib''] |
    fa''4 re''8.[ sib'16] |
    sib'4. la'16[ sib'] |
    do''4 r8 \ficta mib''16[ re''] |
    re''[ fad''] la''[ sol''] fad''[ mib''] re''[ do''] |
    sib'16.[ do''32] re''8 r \ficta mib''16[ re''] |
    re''[ fad''] la''[ sol''] fad''[ mib''] re''[ do''] |
    \grace do''8 sib'4 r8 \ficta mib''16[ sol''] |
    fa''8 fa'' fa''\fermata sol''16[ \ficta mib''] |
    \ficta \grace mib''8 re''4 r |
    sib' sib'' |
    \grace la''8 sol''4. fa''16 \ficta mib'' |
    re''4.\melisma do''8\melismaEnd |
    \grace do''8 re''4 r |
    sib'4 sib'' |
    \grace la''8 sol''4. fa''16 \ficta mib'' |
    re''4.\melisma do''8\melismaEnd |
    sib'4 r |
  }
  \tag #'voix2 { R2*23 }
>>
<<
  \tag #'(voix1 basse) {
    r4 r8 do'' |
    re'' mi''! fa'' mi''16[ re''] |
    do''8.[ sib'16] la'8 do'' |
    re'' mi'' fa'' mi''16[ re''] |
    \grace re''8 do''4 r8 do''16[ re''] |
    mib''4~ mib''16[ fa''] fa''[ sol''] |
    sol''8( fa''4) mib''8 |
    mib'' re'' r fa''16[ mi''] |
    mi''4.\melisma sol''16[ fa''] |
    fa''4. la''16[ sol''] |
    sol''4. sib''16[ la''] |
    la''4~ la''16[ sol'' fa'' mi''] |
    \grace mi''8 fa''4~ fa''16[ mi'' re'' dod''] |
    re''[-! re''( mi'' fa'')]\melismaEnd \grace mi''8 re''[ do''16 sib'] |
    la'4 sol' |
    la''16[(\melisma sold'') la''( sold'')] la''([ sol''])\melismaEnd fa''[ mi''] |
    fa''[(\melisma mi'') fa''( mi'')] fa''[( mi'') re''( \ficta dod'')] |
    re''-![ re''( mi'' fa'')]\melismaEnd \grace mi''8 re''[ do''16 sib'] |
    la'4 sol' |
    fa' r |
    R2*2 |
  }
  \tag #'voix2 {
    r4 r8 la' |
    sib' do'' re'' do''16[ sib'] |
    la'8.[ sol'16] fa'8 la' |
    sib' do'' re'' do''16[ sib'] |
    \grace sib'8 la'4 r8 la'16[ sib'] |
    do''4~ do''16[ re''] re''[ mib''] |
    \ficta mib''8( re''4) do''8 |
    do'' sib' r la'16[ sol'] |
    sol'4.\melisma sib'16[ la'] |
    la'4. fa''16[ mi''] |
    mi''4. sol''16[ fa''] |
    fa''4~ fa''16[ mi'' re'' dod''] |
    \ficta \grace dod''8 re''4~ re''16[ \ficta do'' sib' la'] |
    sib'-![ sib'( do'' re'')]\melismaEnd \grace do''8 sib'[ la'16 sol'] |
    fa'4 mi' |
    fa''16[(\melisma mi'') fa''( mi'')] fa''([ mi''])\melismaEnd re''[ \ficta dod''] |
    re''[(\melisma dod'') re''( dod'')] re''[ do'' sib' la'] |
    sib'16[-! sib'( do'' re'')]\melismaEnd \grace do''8 sib'[ la'16 sol'] |
    fa'4 mi' |
    fa' r |
    R2*2 |
  }
>>
