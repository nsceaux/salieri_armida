\clef "treble" \transposition fa
r8 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 sol''16 mi'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4
  }
  { R2 |
    r4 r8 sol''16 mi'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4 \grace mi''8 re'' do''16 sol' |
    mi'4 }
>> r4 |
R2*3 |
r4 r8 \twoVoices #'(primo secondo tutti) <<
  { re''8 | mi''8 sol'' fa'' mi'' | re''4 }
  { sol'8 | do'' mi'' re'' do'' | sol'4 }
>> r4 |
R2 |
r4 r8 \twoVoices #'(primo secondo tutti) <<
  { re''8 |
    mi'' sol'' fa'' mi'' |
    re''4 s8 mi'' |
    re''4 s8 mi'' |
    re''4 }
  { sol'8 |
    do'' mi'' re'' do'' |
    sol'4 s8 do'' |
    sol'4 s8 do'' |
    sol'4 }
  { s8 | s2 | s4 r8 s | s4 r8 s | }
>> r4 |
R2*9 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' | do'' }
  { do''4 sol' | mi' }
>> r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' | do'' }
  { do''4 sol' | mi' }
  { s2\p | s4\f }
>> r4 |
R2*3 |
\twoVoices #'(primo secondo tutti) <<
  { do''2~ | do''~ | do''4. }
  { do'2~ | do'~ | do'4. }
>> r8 |
R2*4 |
\tag #'tutti <>^"a 2." re''2~ |
re''~ |
\once\tieDashed re''~ |
re''4\f sol' |
r8 do''[\p do''] r |
r do''[ do''] r |
r do''[ do'' do''] |
re''4. r8 |
R2*5 |
r4 re''\f~ |
re''\fermata r4 |
R2*3 |
re''2\p~ |
re'' |
sol'4 r |
R2*3 | \allowPageTurn
\twoVoices #'(primo secondo tutti) <<
  { r4 r8 sol''16 mi'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4 }
  { R2 |
    r4 r8 sol''16 mi'' |
    do''4 re'' |
    mi'' \grace sol''8 fa'' mi''16 re'' |
    do''4 \grace mi''8 re'' do''16 sol' |
    mi'4 }
>> r4 |
R2 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' | do'' }
  { do''4 sol' | mi' }
>> r4 |
R2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' | do'' }
  { do''4 sol' | mi' }
  { s2 | s4\f }
>> r4 | \allowPageTurn
R2*3 |
\tag #'tutti <>^"a 2." do''4 do''8 do'' |
do''4 do''8 do'' |
do''4 r |
R2*29 |
\twoVoices #'(primo secondo tutti) <<
  { s8 re''16. re''32 re''4 |
    s8 mi''16. sol''32 mi''4 |
    s8 re''16. re''32 re''4 |
    do'' }
  { s8 sol'16. sol'32 sol'4 |
    s8 do''16. mi''32 do''4 |
    s8 sol'16. sol'32 sol'4 |
    do'4 }
  { r8 s4.\p | r8 s4. | r8 s4. | }
>> r4 |
R2*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4 }
  { do' }
>> r4 |
R2*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 re'' |
    do'' do''8 do'' |
    do''4 do''8 do'' |
    do''4 }
  { do''4 sol' |
    mi' r |
    mi' r |
    mi' }
>> r4 |
