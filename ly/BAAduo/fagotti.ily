\clef "bass" r8 |
fa4 r |
fa r |
do' do8 r |
R2*4 |
fa4 r |
fa r |
do'2\f~ |
do'\p~ |
do'4 fa8 r |
R2 |
do'2~ |
do'~ |
do'4 fa8 r |
R2 |
do'2 |
do |
do,4. r8 |
R2*17 |
fa2~ |
\once\tieDashed fa~ |
fa4. r8 |
R2*4 |
\clef "tenor" \twoVoices #'(primo secondo tutti) <<
  { fa'4 mi' |
    re' mi' |
    fa' mi' |
    re' mi' | }
  { re'4 do' |
    si? do' |
    re' do' |
    si do' | }
  { s2-\sug\p | s2*2 | s2\f | }
>>
\clef "bass" R2*9 |
r4 si\f |
do'\fermata r |
R2*6 |
do'16( re') do'( re') sib( do') sib( do') |
la4 r |
R2*12 |
r8 fa[-\sug\f fa,]-\sug\p r |
R2*3 |
fa4 fa8 fa |
fa4 fa8 fa |
fa,4 r |
R2*29 |
r8 sol'16.\p mi'32 do'4 |
r8 do'16. la32 do'4 |
r8 sib16. sol32 do'4 |
fa4 r |
R2*3 |
fa4-\sug\f r |
R2 |
r8 sib sib sib |
do'4 do |
fa fa8 fa |
fa4 fa8 fa |
fa4 r |
