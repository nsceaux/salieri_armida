\score {
  <<
    \new ChoirStaff <<
      \new GrandStaff \with { \flautiInstr  } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "flauti" >>
      >>
      \new GrandStaff \with { \oboiInstr  } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new GrandStaff \with { \corniFInstr  } <<
        \new Staff <<
          \keepWithTag #'() \global \keepWithTag #'primo \includeNotes "corni"
        >>
        \new Staff <<
          \keepWithTag #'() \global \keepWithTag #'secondo \includeNotes "corni"
        >>
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Arm.
      } \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Rin.
      } \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
    >>
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        %% 1
        s8 s2*5\pageBreak
        s2*7\pageBreak
        s2*4\pageBreak
        s2*3\pageBreak
        %% 5
        s2*3 s4 \bar "" \pageBreak
        s4 s2*3\pageBreak
        s2*4\pageBreak
        s2*5\pageBreak
        s2*6\pageBreak
        %% 10
        s2*4\pageBreak
        \grace s8 s2*4\pageBreak
        s2*5\pageBreak
        s2*4\pageBreak
        s2*3\pageBreak
        %% 15
        s2*4\pageBreak
        s2*4\pageBreak
        s2*5\pageBreak
        s2*4\pageBreak
        s2*6\pageBreak
        %% 20
        s2*4\pageBreak
        s2*5\pageBreak
        s2*4\pageBreak
        s2*6\pageBreak
        s2*4\pageBreak
        %% 25
        s2*4\pageBreak
        \grace s8 s2*4\pageBreak
        s2*5\pageBreak
        s2*4\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
