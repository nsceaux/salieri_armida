Qui’l re -- gno è del __ con -- ten -- to;
la se -- de \tag #'(voix1 basse) { del pia -- cer __ }
del pia -- cer, \tag #'voix2 { del pia -- cer, }
la se -- de del pia -- cer,
la se -- de del pia -- cer, __
la se -- de del pia -- cer.

\tag #'(voix2 basse) {
  Fresch’ om -- bre, e ver -- di spon -- de,
  cui ba -- gna un rio __ d’ar -- gen -- to
  c’in -- vi -- ta -- no
  c’in -- vi -- ta -- no a go -- der:
  par che la ter -- ra e l’on -- de
  spi -- ri -- no un dol -- ce ar -- do -- re,
  sem -- bra che fin d’a -- mor
  che fin d’a -- mor __
  mor -- mo -- ri
  mor -- mo -- ri
  mor -- mo -- ri il ven -- to.
}

Qui’l re -- gno è del __ con -- ten -- to;
la se -- de \tag #'voix2 { del pia -- cer __ }
del pia -- cer, \tag #'(voix1 basse) { del pia -- cer, }
la se -- de del pia -- cer,
la se -- de del pia -- cer, __
la se -- de del pia -- cer.

\tag #'(voix1 basse) {
  Fol -- le chi del -- la vi -- ta
  pas -- sa il bre -- ve mo -- men -- to
  in tor -- bi -- di pen -- sier,
  che val l’e -- tà fio -- ri -- ta,
  che val ric -- chez -- za ed or,
  se cam -- bia un van ti -- mor.
  Tut -- to, tut -- to in tor -- men -- to,
  tut -- to, tut -- to in tor -- men -- to.
}

Pre -- zio -- so è il tem -- po, e lie -- ve,
fac -- cia -- mo -- ne te -- sor.
La vi -- ta è un cam -- min bre -- ve
spar -- gia -- mo -- lo di fior __
spar -- gia -- mo -- lo di fior.
