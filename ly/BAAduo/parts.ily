\piecePartSpecs
#`((flauti #:score-template "score-deux-voix")
   (oboi #:score-template "score-deux-voix")
   (corni #:tag-global () #:instrument "Corni in D"
          #:score-template "score-deux-voix")
   (fagotti #:music , #{
\quoteBassi "BAAduo"
s8 s2*20
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*14 \mmRestVisible s2 \mmRestInvisible s2*2 \mmRestVisible }
s2*3
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*4 \mmRestVisible }
s2*4
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*9 \mmRestVisible \restInvisible s8 \restVisible s8 }
s4 s2
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*6 \mmRestVisible }
s2*2
<>^\markup\tiny "B."
\cue "BAAduo" {
  \mmRestInvisible s2*2 \mmRestVisible s2*4\allowPageTurn
  \mmRestInvisible s2*6 \mmRestVisible
}
s2*2
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*2 \mmRestVisible }
s2*3
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*29 \mmRestVisible }
s2*5
<>^\markup\tiny "B."
\cue "BAAduo" { \mmRestInvisible s2*2 \mmRestVisible }
    #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers A 2
    \livretVerse#12 { Qui'l regno è del contento; }
    \livretVerse#12 { La sede del piacer. }
    \livretPers Rinaldo
    \livretVerse#12 { Fresch’ ombre, e verdi sponde, }
    \livretVerse#12 { Cui bagna un rio d'argento }
    \livretVerse#12 { C'invitano a goder. }
    \livretVerse#12 { Per che la terra e l'onda }
    \livretVerse#12 { Spirino un dolce ardor, }
    \livretVerse#12 { Sembra che fin d'amor }
    \livretVerse#12 { Mormori il vento. }
    \livretPers A 2
    \livretVerse#12 { Qui'l regno è del contento; }
    \livretVerse#12 { La sede del piacer. }
  }
  \null
  \column {
    \livretPers Armida
    \livretVerse#12 { Folle chi della vita }
    \livretVerse#12 { Passa il breve momento }
    \livretVerse#12 { In torbidi pensier. }
    \livretVerse#12 { Che val l'età fiorita, }
    \livretVerse#12 { Che val ricchezza ed or, }
    \livretVerse#12 { Se cambia un van timor. }
    \livretVerse#12 { Tutto in tormento. }
    \livretPers A 2
    \livretVerse#12 { Prezioso è il tempo, e lieve, }
    \livretVerse#12 { Facciamone tesor. }
    \livretVerse#12 { La vita è un camin breve }
    \livretVerse#12 { Sparghiamola di fior. }
  }
  \null
} #}))

