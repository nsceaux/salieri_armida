\clef "bass" fad2\p r |
sol4 r sol( la |
sib fad sol) \sug r |
fa2 r |
mib4 r mib( fa! |
sol re mib) r |
R1*2 |
mib2(\p re)\rf |
R1 |
r2 mib4\p r |
\ficta mib1\fp |
re~ |
re |
mib4 r r2 |
R1 |
r2 fad\fp~ |
fad r |
R1 |
sol2 r |
sol, do~ |
do do\f |
r re4.\p re,8 |
