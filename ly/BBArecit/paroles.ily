E non deg -- gio se -- guir -- la? Ah, sen -- za Ar -- mi -- da
son se -- co -- li gl’is -- tan -- ti. A che mi gio -- va
il ri -- den -- te sog -- gior -- no, e do -- ve or so -- no
tan -- te va -- rie bel -- lez -- ze, on -- de l’a -- dor -- na
la pro -- di -- ga na -- tu -- ra a -- gli oc -- chi mie -- i?
Ah che vi -- ci -- no a lei
tut -- to è lie -- to, e gio -- con -- do,
ri -- de il cie -- lo, ri -- de il mon -- do;
ma co -- pre un fos -- co ve -- lo,
se s’al -- lon -- ta -- na Ar -- mi -- da, e ter -- ra, e cie -- lo,
e di -- ven -- ta per me da lei di -- vi -- so
un de -- ser -- to d’or -- ror l’is -- tes -- so E -- li - %- so.
