\clef "treble"
<<
  \tag #'primo {
    re''2\p r |
    r4 r8 <>_\markup\italic dol. re''( sib''8) la''( sol'' fad'') |
    sol''-! mib''( re'' do'') sib'4 r |
    re''2 r |
    r4 r8 mib''8( sib'') sol''( fa'' re'') |
    mib'' sib'4 lab'8 sol'4 r |
    R1*2 |
    sib'2(\p si')\rf |
    R1 |
    r2 do''4\p r |
    sol''1\fp |
    fa''~ |
    fa'' |
    mib''4 r r2 |
    r mib''4( sib') |
    sol' mib' <re' do'>2\fp~ |
    q1~ |
    q1~ |
    <sib re'>2 r |
    fa''2 mib''~ |
    mib'' fad'\f |
    R1 |
  }
  \tag #'secondo {
    la'2\p r |
    r re''8 re''4 re''8 |
    re''4 la' sol' r |
    lab'2 r |
    r sib'8 sib'4 sib'8 |
    \ficta sib'4 fa' mib' r |
    R1*2 |
    sol'2-\sug\p sol'-\sug\rf |
    R1 |
    r2 sol'4\p r |
    do''1\fp |
    re''2. sib'4~ |
    sib'1~ |
    sib'4 r r2 |
    R1 |
    r2 la\fp~ |
    la1~ |
    la |
    sib2 r |
    <sol' re''>2 sol'~ |
    sol' mib'\f |
    r sib'4.\p la'8 |
  }
>>
