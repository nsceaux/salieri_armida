\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Rinaldo
      shortInstrumentName = \markup\character Rin.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s1*3\pageBreak
        s1*3\break s1*3\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
