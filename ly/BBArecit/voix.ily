\clef "soprano/treble" r4 r8 la'16 la' re''4 re''8 la' |
sib'8 sib' r4 r2 |
r sib'4 sib'8 do'' |
re'' re'' r fa'' re'' re'' re'' mib'' |
mib'' sib' r4 r2 |
r r8 mib'' mib'' mib'' |
mib'' sib' r sib'16 sib' sib'4 lab'8 sib' |
sol' sol' r16 sol' sib' lab' sib'8 sib' r sib'16 sib' |
mib''4 re''8 do'' si'!8 si' si' do''16 re'' |
re''8 sol' r sol' si'! si' si' do'' |
re'' re'' r16 re'' si'! sol' do''8 do'' r4 |
sol''2 fa''8 mib'' re'' do'' |
\grace do''8 sib'2 r4 re''8 fa'' |
fa''2 lab'4. sib'8 |
sol'8 sol' r sib'16 sib' sol'8 sol' r mib''16 sol'' |
mib''8 mib'' r mib'' mib''4 sib' |
sol' mib' re'4.( mib'8) |
re'4 r8 do''8 mib'' re'' do'' sib' |
la' la' r sib' do'' do'' r la' |
sib' sib' r8 re''16 re'' re''4 do''8 re'' |
si'!4 r16 re'' fa'' re'' mib''8 mib'' r do''16 re'' |
mib''8([ do'']) lab'8. sol'16 \grace sol'8 fad'4 r8 mib'' |
do'' do'' r sib' sib'4.( la'8) |
