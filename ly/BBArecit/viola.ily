\clef "alto" fad'2\p r |
r2 sol4( la |
sib8) re'4 re'8 re'4 r |
sib2 r |
r2 mib4( fa! |
sol8) sib4 sib8 sib4 r4 |
R1*2 |
sol'2-\sug\p fa'-\sug\rf |
R1 |
r2 mib'4\p r |
sol'2.\fp la'4 |
sib'1 |
lab' |
sol'4 r r2 |
R1 |
r2 fad-\sug\fp~ |
fad1~ |
fad |
\sug sol2 r |
si!2 \once\tieDashed do'~ |
do' la\f |
r sol'4.\p( fad'8) |
