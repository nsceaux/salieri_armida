\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#12 { E non deggio seguirla? Ah senza Armida }
  \livretVerse#12 { Son secoli gl'istanti. A che mi giova }
  \livretVerse#12 { Il ridente soggiorno, e dove or sono }
  \livretVerse#12 { Tante varie bellezze, onde l'adorna }
  \livretVerse#12 { La prodiga natura agli occhi miei? }
  \livretVerse#12 { Ah che vicino a lei }
  \livretVerse#12 { Tutto è lieto, e giocondo, }
  \livretVerse#12 { Ride il cielo, ride il mondo, }
  \livretVerse#12 { Ma cuopre un fosco velo, }
  \livretVerse#12 { Se s'allontana Armida, e terra, e cielo, }
  \livretVerse#12 { E diventa per me da lei diviso }
  \livretVerse#12 { Un deserto d'orror l'istesso Eliso. }
}

       #}))
