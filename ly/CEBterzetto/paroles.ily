\tag #'(armida basse) {
  Strap -- pa -- mi il cor dal se -- no,
  per -- fi -- do, tra -- di -- tor:
  sia que -- sto il pre -- mio al -- me -- no
  d’un in -- fe -- li -- ce
  d’un in -- fe -- li -- ce a -- mor
  sia que -- sto il pre -- mio al -- me -- no
  d’un in -- fe -- li -- ce a -- mor.
}
\tag #'(rinaldo basse) {
  Sor -- gi, che di -- ci?… Oh Di -- o!
}
\tag #'(ubaldo basse) {
  Ah ti se -- du -- ce il cor
  ti se -- du -- ce il cor.
}
\tag #'(armida rinaldo basse) {
  Che fie -- ro sta -- to è il mi -- o,
  che bar -- ba -- ro do -- lor,
  che bar -- ba -- ro do -- lor.
}
\tag #'(armida basse) {
  Tu ve -- di il mi -- o tor -- men -- to.
}
\tag #'(ubaldo basse) {
  Del Ciel le vo -- ci in -- ten -- di.
}
\tag #'(armida ubaldo basse) {
  E reo d’un tra -- di -- men -- to
}
\tag #'(armida basse) {
  un fi -- do a -- mo -- re of -- fen -- di?
}
\tag #'(ubaldo basse) {
  Ol -- trag -- gi o -- no -- re, e \tag #'ubaldo { fe? }
}
\tag #'(rinaldo basse) {
  Que -- sto è cru -- del ci -- men -- to
  cru -- del ci -- men -- to.
}

Ah nell’ an -- gu -- stia e -- stre -- ma
il cor mi tre -- ma, e il piè
il cor __ mi tre -- ma.
Ah nell’ an -- gu -- stia e -- stre -- ma
il cor mi tre -- ma, e il piè
mi tre -- ma, e il piè
mi tre -- ma, e il piè.

\tag #'(ubaldo basse) {
  Vie -- ni l’o -- nor t’af -- fret -- ta.
}
\tag #'(rinaldo basse) {
  Ri -- man -- ti in pa -- ce, ad -- di -- o,
  nò, più cru -- del del mi -- o
  il tuo de -- stin non è
  nò, più cru -- del del mi -- o
  il tuo de -- stin non è.
}
\tag #'(armida basse) {
  Ah bar -- ba -- ro, as -- pet -- ta,
  ah fer -- ma -- ti, in -- gra -- to.
  Oh Nu -- mi, ven -- det -- ta
  d’un mo -- stro spie -- ta -- to
  che fe -- de non ha.
  Sprez -- za -- ta, tra -- di -- ta
  m’op -- pri -- me il tor -- men -- to,
  mi man -- ca la vi -- ta,
  ge -- la -- re mi sen -- to,
  oh Nu -- mi pie -- tà
  oh Nu -- mi pie -- tà.
}
\tag #'(rinaldo basse) {
  Mi -- se -- ra!
}
\tag #'(ubaldo basse) {
  Ah do -- ve va -- i?
}
\tag #'(rinaldo basse) {
  A ri -- chia -- mar -- la in vi -- ta.
}
\tag #'(ubaldo basse) {
  Fug -- gi
  fug -- gi la fro -- de or -- di -- ta.
}
\tag #'(rinaldo basse) {
  E trop -- pa cru -- del -- tà
  è trop -- pa cru -- del -- tà.
  
  Ar -- mi -- da… Ar -- mi -- da… Ah… più non ha
  né mo -- to, né co -- lor.
  For -- se l’uc -- ci -- de -- rà
  l’ec -- ces -- so del do -- lor,
  l’ec -- ces -- so del do -- lor
  del do -- lor.
  Ah mi si spez -- za il cor __
  fra tan -- to
  fra tan -- to af -- fan -- no.
  Ar -- mi -- da… Ar -- mi -- da…
  Ah mi si spez -- za il cor __
  fra tan -- to
  fra tan -- to af -- fan -- no.
  
  Sen -- ti… non par -- ti -- rò.
  Ve -- di… che t’a -- mo an -- cor,
  che t’a -- mo an -- cor.
  Che par -- lo? oh Di -- o! che fò?
  oh Di -- o! che fò?
  oh sfor -- tu -- na -- to a -- mor!
  do -- ver ti -- ran -- no
  do -- ver ti -- ran -- no!

  Sen -- ti… Ar -- mi -- da… Ah più non ha
  né mo -- to, né co -- lor.
  For -- se l’uc -- ci -- de -- rà
  l’ec -- ces -- so del do -- lor
  del do -- lor.
  Ah mi si spez -- za il cor __
  fra tan -- to af -- fan -- no.
  Che par -- lo? oh Di -- o! che fò?
  Ah mi si spez -- za il cor __
  frà tan -- to af -- fan -- no
  frà tan -- to af -- fan -- no
  frà tan -- to af -- fan -- no
  frà tan -- to af -- fan -- no
  frà tan -- to af -- fan -- no.

  Co -- me re -- star deg -- g’io?…
  Co -- me par -- tir po -- trò?…
}
\tag #'(ubaldo basse) {
  Tu re -- sta, io par -- to.
}
\tag #'(rinaldo basse) {
  Oh Dio!
  sen -- ti -- mi.
}
\tag #'(ubaldo basse) {
  O vie -- ni, o re -- sta.
}
\tag #'(rinaldo basse) {
  Che fie -- ra leg -- ge è \tag #'rinaldo { que -- sta! }
}
\tag #'(ubaldo basse) {
  O -- ra è il ri -- gor pie -- tà
  è il ri -- gor pie -- tà
}
\tag #'(rinaldo basse) {
  Po -- ve -- ra Ar -- mi -- da…
  po -- ve -- ra Ar -- mi -- da ad -- dio ad -- dio
  Il duol m’uc -- ci -- de -- rà.
  Il duol m’uc -- ci -- de -- rà.
}
