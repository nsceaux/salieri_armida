\clef "treble" R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { lab''4. fa''8 re''4 }
  { re''2. }
>> r4 |
R1*11 |
\twoVoices #'(primo secondo tutti) <<
  { s4 sib' sib' sib' |
    s sib' sib' sib' |
    s sib' re'' mib'' | }
  { s4 sol' sol' sol' |
    s sol' sol' sol' |
    s fa' sib' do'' | }
  { r4 s2.\p |
    r4 s2. |
    r4 s2. | }
>>
r4 \tag#'tutti <>^"a 2." sib' re' fa' |
sib' r r2 |
R1*20 |
\twoVoices #'(primo secondo tutti) <<
  { sib'1 |
    do''2 re'' |
    mib'' fa''4 }
  { sol'2 mib'~ |
    mib' sib' |
    sib' sib'4 }
  { s1\p\cresc | s1 | s2\f }
>> r4 |
R1*3 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { lab''1~ |
    lab'' |
    sol'' |
    la'' |
    sib''4 s sib'' sib'' |
    sib''2 }
  { re''1~ |
    re'' |
    mib'' |
    mib'' |
    re''4 s re'' re'' |
    re''2 }
  { s1*4 | s4 r s2 | }
>> r2 |
R1*11 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { fa''1 | mib''4 }
  { re''1 | sol'4 }
  { s1 | s4-\sug\p }
>> r4 r2 |
R1*3 |
\twoVoices #'(primo secondo tutti) <<
  { fa''4 s2. |
    fa''4 s2. |
    mib''4 s2. |
    re''4 s2. |
    sol''1 |
    fa''4 s2. |
    mib''4 s2. |
    re''4 s2. |
    sol''1 |
    fa''4 s2. |
    mib''1~ |
    mib'' |
    mib''2 mi'' |
    fa'' mib''! |
    re''2\fermata }
  { sol'4 s2. |
    lab'4 s2. |
    do''4 s2. |
    si'4 s2. |
    do''1 |
    re''4 s2. |
    do''4 s2. |
    si'4 s2. |
    do''1 |
    re''4 s2. |
    do''1 |
    reb'' |
    do'' |
    do'' |
    fa'2\fermata }
  { s4\p r4 r2 |
    s4 r r2 |
    s4 r r2 |
    s4 r r2 |
    s1\f |
    s4\p r4 r2 |
    s4 r r2 |
    s4 r r2 |
    s1\f |
    s4\p r4 r2 |
    s1\cresc |
    s1\ff |
    s1_\markup\italic calando }
>> r2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mib'4. mib'8 fa'4. fa'8 |
    sol'2 s |
    sib'4 s sib' s |
    sol' mib' }
  { mib'4. mib'8 re'4. re'8 |
    mib'2 s |
    sol'4 s fa' s |
    mib' mib' }
  { s1 | s2 r | s4 r s r | }
>> r2 |
R1*13 | \allowPageTurn
%%
R2*7 |
\twoVoices #'(primo secondo tutti) <<
  { lab''2 | sol''4 s | lab''2 | sol''4 }
  { do''2 | do''4 s | do''2 | do''4 }
  { s2-\sug\fp | s4 r | s2-\sug\f }
>> r4 | \allowPageTurn
R2*27 |
%%
R1*9 |
\tag#'tutti <>^"a 2." mib'2-\sug\f r |
R1*15 |
r4 <>\f \twoVoices #'(primo secondo tutti) <<
  { fa''8. fa''16 fa''4 }
  { la'8. la'16 la'4 }
>> r4\fermata |
R1*2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''2 }
  { mib''1~ | mib''2 }
>> r2 |
R1*2 |
\tag#'tutti <>^"a 2." sib'8\f sib' re'' fa'' sib'' fa'' re'' fa'' |
sib'4 r r2 |
R1*4 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { sol''1~ | sol''2 }
  { mib''1~ | mib''2 }
>> r2 |
R1*3 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { re''2. do''4 | }
  { sib'2. la'4 | }
>>
\tag#'tutti <>^"a 2." sib'8 sib' re'' fa'' sib'' fa'' re'' fa'' |
sib' re'' fa'' re'' sib' fa' re' fa' |
sib'4 r r2 | \allowPageTurn
R1*7 | \allowPageTurn
<>\p-\sug\cresc \twoVoices #'(primo secondo tutti) <<
  { re''1 | do''2. sol''4 | fad''1 | sol'' | fad'' | sol''4 }
  { si'1 | do'' | do'' | si' | do'' | si'4 }
  { s1*3 | s1-\sug\ff }
>> r4 r2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { do''4 re'' mib'' fa'' |
    sol'' fa'' sol'' lab'' |
    mib''2. re''4 |
    mib'' fa'' sol'' lab'' |
    mib''2. re''4 |
    do''4 s2. |
    do''4 s2. |
    sib'4 sib' sib'\fermata s |
    sib' }
  { sol'2. si'4 |
    do''2. do''4 |
    do''2. si'4 |
    do''2. do''4 |
    do''2. si'4 |
    do''4 s2. |
    mib'4 s2. |
    re'4 re' re'\fermata s |
    mib'4 }
  { s1*5 | s4 r r2 | s4 r r2 | s2. r4 | }
>> r4 r2 | \allowPageTurn
R1*8 |
\twoVoices #'(primo secondo tutti) <<
  { solb''2. fa''8 mib'' |
    re''4 s solb'' fa''8 mib'' |
    re''4 sib''8. sib''16 sib''4 }
  { la'1 |
    sib'4 s la'2 |
    sib'4 re''8. re''16 re''4 }
  { s1\f | s4 r s2 | }
>> r4\fermata |
R1*2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { lab''1~ | lab''2 }
  { r4 fa''2 mib''4 | re''2 }
>> r2 |
R1 |
<>\p \twoVoices #'(primo secondo tutti) <<
  { sol''2. fa''4 | mib'' }
  { mib''2. re''4 | mib'' }
>> r4 r2 |
R1*5 | \allowPageTurn
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { lab''1~ | lab''2 }
  { fa''2. mib''4 | re''2 }
>> r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sol''2. fa''4 | mib'' }
  { mib''2. re''4 | mib'' }
>> r4 r2 |
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { lab''8( fa'') lab''( fa'') lab''( fa'') | sol''2 fa'' | mib''4 }
  { re''4 do''2 | sib'4 mib''2 re''4 | mib'' }
>> r4 r2 |
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { lab''8( fa'') lab''( fa'') lab''( fa'') | sol''2. fa''4 | }
  { re''4 do''2 | sib'4 mib''2 re''4 | }
>>
\tag#'tutti <>^"a 2." mib''8 sol'' sib'' sol'' mib'' sib' sol' sib' |
mib'' sol'' sib'' sol'' mib'' sib' sol' sib' |
mib'' sol'' mib'' sib' sol' mib'' sib' sol' |
mib'' sol'' mib'' sib' sol' mib'' sib' sol' |
mib'2 r |
R1*10 |
\twoVoices #'(primo secondo tutti) <<
  { sol''2 re'' | mib''2. do''4 | sol''2 }
  { si'2 si' | do''2. mib'4 | re' sol'' }
  { s1\f | s | s2\f }
>> \tag#'tutti <>^"a 2." sol''4. sol''8 |
la''2 si'' |
do''' lab''! |
fa'' sol'' |
do'' r2 |
R1*6 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { lab''2( sol''4) }
  { re''2( mib''4) }
>> r4 |
R1*7 |
<<
  \tag #'(primo tutti) {
    <>^"Solo" r4 mib''\p mib'' mib'' |
    fa''4 sol''8( lab'' sib''4 lab'') |
    sol'' sol'' sol'' sol'' |
    \grace sol''8 fa''4 fa''2 sol''8 fa'' |
    mib''4 r r2 |
  }
  \tag #'secondo R1*5
>>
R1*5 |
r2
