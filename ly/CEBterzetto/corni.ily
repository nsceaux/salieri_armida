\clef "treble" \transposition mib
\twoVoices #'(primo secondo tutti) <<
  { mi'' s mi'' s | re''2. }
  { do''4 s do'' s | sol'2. }
  { s4\f r s\p r | s2.\f }
>> r4 |
R1*11 |
r4 <>\p \tag#'tutti <>^"a 2." do'' do'' do'' |
r do'' do'' do'' |
r re'' re'' re'' |
r2 r4 \twoVoices #'(primo secondo tutti) <<
  { re''4 | sol'' }
  { re'' | sol' }
>> r4 r2 |
R1*20 |
do''1-\sug\p -\sug\cresc |
\twoVoices #'(primo secondo tutti) <<
  { do''2 sol'' | sol'' sol''4 }
  { do''2 re'' | mi'' re''4 }
  { s1 | s2.-\sug\f }
>> r4 |
R1*3 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { re''1~ |
    re'' |
    mi'' |
    re'' |
    re''4 s re'' re'' |
    re''2 }
  { sol'1~ |
    sol' |
    sol' |
    do'' |
    sol'4 s sol' sol' |
    sol'2 }
  { s1*4 | s4 r s2 | }
>> r2 |
R1*11 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { mi''1 | mi''4 }
  { mi'1 | mi'4 } |
  { s1 | s4-\sug\p }
>> r4 r2 |
R1*3 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { mi''4 }
  { do'' }
>> r4 r2 |
\tag#'tutti <>^"a 2." re''4 r r2 |
mi''4 r r2 |
mi''4 r r2 |
do''1\f |
re''4\p r r2 |
mi''4 r r2 |
mi''4 r r2 |
do''1-\sug\f |
re''4-\sug\p r r2 |
mi''1-\sug\cresc |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sol''1 | do''2 }
  { do''1 | do''2 }
>> r2 |
R1 |
\twoVoices #'(primo secondo tutti) <<
  { re''2\fermata }
  { sol'\fermata }
>> r2 |
\twoVoices #'(primo secondo tutti) <<
  { do''4. do''8 re''4. re''8 |
    mi''2 s |
    mi''2 re'' |
    do''4 do'' }
  { mi'4. mi'8 sol'4. sol'8 |
    do''2 s |
    do''2 sol' |
    mi'4 mi' }
  { s1\f | s2 r | }
>> r2 |
R1*13 | \allowPageTurn
%%
R2*7 |
\tag#'tutti <>^"a 2." re''2-\sug\fp |
do''4 r |
re''2-\sug\f |
do''4 r | \allowPageTurn
R2*27 |
%%
R1*9 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do''2 }
  { mi' }
>> r |
R1*15 |
r4 \tag#'tutti <>^"a 2." re''8.-\sug\f re''16 re''4 r\fermata |
R1*2 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''1~ | mi''2 }
  { do''1~ | do''2 }
>> r2 |
R1*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { re''4 re'' re'' re'' | re'' }
  { sol'4 sol' sol' sol' | sol' }
>> r4 r2 |
R1*4 |
<>\ff \twoVoices #'(primo secondo tutti) <<
  { mi''1~ | mi''2 }
  { do''1~ | do''2 }
>> r2 |
R1*3 | \allowPageTurn
\tag#'tutti <>^"a 2." mi''2-\sug\ff re''4 re'' |
\twoVoices #'(primo secondo tutti) <<
  { re''4 re'' re'' re'' |
    re'' re'' re'' re'' |
    re'' }
  { sol'4 sol' sol' sol' |
    sol' sol' sol' sol' |
    sol' }
>> r4 r2 | \allowPageTurn
R1*7 | \allowPageTurn
<>\p-\sug\cresc \twoVoices #'(primo secondo tutti) <<
  { mi''1 | }
  { mi' | }
>>
\tag#'tutti <>^"a 2." do''1 |
do''2. fa''4 |
mi''1-\sug\ff |
do''2. fa''4 |
mi'' r r2 |
R1*2 |
mi''1 |
mi''4 r r2 |
mi''1 |
mi''4 r r2 |
mi''4 r r2 |
re''4\fermata re'' re'' r |
\twoVoices #'(primo secondo tutti) <<
  { mi'' }
  { do'' }
>> r r2 |
R1*8 |
\tag#'tutti <>^"a 2." do''1-\sug\f |
re''4 r do''2 |
\twoVoices #'(primo secondo tutti) <<
  { re''4 re''8. re''16 re''4 }
  { sol'4 sol'8. sol'16 sol'4 }
>> r4\fermata |
R1*2 |
re''1-\sug\ff |
\twoVoices #'(primo secondo tutti) <<
  { re''2 }
  { sol' }
>> r2 |
R1*8 | \allowPageTurn
\tag#'tutti <>^"a 2." re''1-\sug\ff~ |
re''2 r |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { mi''2. re''4 | do'' }
  { do''2. sol'4 | mi' }
>> r4 r2 |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''2 re'' | do''4 }
  { do''2 sol' | mi'4 }
>> r4 r2 |
R1*2 |
\twoVoices #'(primo secondo tutti) <<
  { mi''2. re''4 |
    do'' mi'' mi'' mi'' |
    do'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' |
    mi''2 }
  { do''2. sol'4 |
    do'' do'' do'' do'' |
    do'' do'' do'' do'' |
    do'' do'' do'' do'' |
    do'' do'' do'' do'' |
    do''2 }
>> r2 |
R1*10 |
\tag#'tutti <>^"a 2." mi''1-\sug\f~ |
mi'' |
mi''2 r |
R1*10 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { re''2 mi''4 }
  { sol'2 sol'4 }
>> r4 |
R1*17 |
r2
