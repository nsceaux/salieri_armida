\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (oboi #:score-template "score-deux-voix")
   (corni #:score-template "score-deux-voix"
          #:tag-global ()
          #:instrument "Corni in E♭")
   (fagotti #:music , #{
\quoteBassi "CEBbassi"
s1*2 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  sol4\p sol lab sib |
  mib r mib r |
  lab r lab, r |
  mib r mib'2 |
  re' do' |
  sib mib' |
  re' do'\fermata |
  sib\fermata r |
  lab4 r fa r |
  sol8(\f fa mib) mib-! mib2:8 |
  do'8 do' lab lab sib sib sib, sib, |
  \restInvisible mib4 \restVisible
}
\mmRestVisible s2. s1*5 s2 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  \restInvisible re2\rf | \restVisible
  mib2 mi |
  fa la\f |
  sib sib,4 r |
  r re(\rf mib fa) |
  mib mib\p mib mib |
  fa fa fa, fa, |
  sib,2 r\fermata |
  r4 re(\rf mib fa) |
  mib mib\p mib mib |
  fa fa fa, fa, |
  sib, sib\f la?4.\trill sib16 la |
  sol4 r sol, r |
  sol r r2 |
  sol4\p sol sol sol |
  sol sol sol sol |
  fa fa fa fa |
  sib, sib, sib, sib, |
  re re re re |
}
\mmRestVisible s1*3 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*3
\mmRestVisible s1*6 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*11
\mmRestVisible s1*2 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*3
\mmRestVisible s1*18 s4 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  \restInvisible mib4-!\p sol( mib) | \restVisible
  r lab-! do'( lab) |
  r sol( fa mib) |
  r sib, do re |
  r mib-! sol( mib) |
  r lab-! do'( lab) |
  r lab-!\f do'( lab) |
  r sol-! mib( sol) |
  lab\p r sib r |
  r mib-! sol( mib) |
  r lab-. do'-. lab-. |
  r lab\f do' lab |
  r sol mib sol |
  r lab-\sug\p sib sib, |
  %%
  mib4 r |
  do8\fp do' do' do' |
  si,\fp si si si |
  sol,\fp sol sol sol |
  do4 r8 do'\ff |
  \grace do'16 si8 la16 sol \grace do'16 si8 la16 sol |
  do'4 r |
}
\mmRestVisible s2*4 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s2*9
\mmRestVisible s2*13 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" { s2*5 s1*9 }
\mmRestVisible s1*5 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*11
\mmRestVisible s1 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*2
\mmRestVisible s1*2 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*2
\mmRestVisible s1*2 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*4
\mmRestVisible s1*2 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*3
\mmRestVisible s1*26 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*8
\mmRestVisible s1*3 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*2
\mmRestVisible s1 s2 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  s4 mib4\p |
  lab4 lab lab lab |
  sib2:8\p sib,: |
  mib4 r r2 |
  mib2 r |
  mib r |
  lab4 r r sib, |
  mib mib' re' re' |
  do' sib lab sol |
}
\mmRestVisible s1 s2 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1.
\mmRestVisible s1 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  \voiceOne mib8\p mib mib' mib' do' do' sib sib | \oneVoice
  la2:8 la: |
  sib:
}
\mmRestVisible s2 s1 \mmRestInvisible
<>^\markup\tiny B. \new CueVoice {
  \voiceOne mib8\p mib mib' mib' do' do' sib sib | \oneVoice
  la2:8 la: |
  sib:
}
\mmRestVisible s2 s1*6 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*9
\new CueVoice { sol2:8 sol: | }
\mmRestVisible s1*7 \mmRestInvisible
<>^\markup\tiny B. \cue "CEBbassi" s1*6
s2..
<>^\markup\tiny B. \cue "CEBbassi" { s8 s1*17 \restInvisible s2 }
   #})

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \force-line-width-ratio#0.3 \column {
    \livretPers Armida
    \livretVerse#12 { Strappami il cuor dal petto, }
    \livretVerse#12 { Perfido, traditor. }
    \livretVerse#12 { Sia questo il premio almeno }
    \livretVerse#12 { D’un infelice amor. }
    \livretPers Rinaldo
    \livretVerse#12 { Sorgi che dici?… Oh Dio. }
    \livretPers Ubaldo
    \livretVerse#12 { Ah ti seduce il cor. }
    \livretPers Armida e Rinaldo
    \livretVerse#12 { Che fiero stato è il mio, }
    \livretVerse#12 { Che barbaro dolor! }
    \livretPers Armida
    \livretVerse#12 { Tu vedi il mio tormento. }
    \livretPers Ubaldo
    \livretVerse#12 { Del Ciel le voci intendi. }
    \livretPers A 2
    \livretVerse#12 { E reo d’un tradimento }
    \livretPers Armida
    \livretVerse#12 { Un fido amore offendi? }
    \livretPers Ubaldo
    \livretVerse#12 { Oltraggi onore, e fè? }
    \livretPers Rinaldo
    \livretVerse#12 { Questo è crudel cimento. }
    \livretPers A 3
    \livretVerse#12 { Ah nell’ angustia estrema }
    \livretVerse#12 { Il cuor mi trema, e il piè. }
  }
  \force-line-width-ratio#0.3 \column {
    \livretPers Ubaldo
    \livretVerse#12 { Vieni l’onor t’affretta. }
    \livretPers Rinaldo
    \livretVerse#12 { Rimanti in pace. Addio, }
    \livretVerse#12 { No, più crudel del mio }
    \livretVerse#12 { Il tuo destin non è. }
    \livretPers Armida
    \livretVerse#12 { Ah barbaro, aspetta }
    \livretVerse#12 { Ah fermati, ingrato. }
    \livretVerse#12 { Oh Numi, vendetta }
    \livretVerse#12 { D’un mostro spietato }
    \livretVerse#12 { Che fede non hà. }
    \livretVerse#12 { Sprezzata, tradita }
    \livretVerse#12 { M’opprime il tormento, }
    \livretVerse#12 { Mi manca la vita, }
    \livretVerse#12 { Gelare mi sento, }
    \livretVerse#12 { Oh Numi pietà. }
    \livretPers Rinaldo
    \livretVerse#12 { Misera! }
    \livretPers Ubaldo
    \livretVerse#12 { Ah dove vai? }
    \livretPers Rinaldo
    \livretVerse#12 { A richiamarla in vita. }
    \livretPers Ubaldo
    \livretVerse#12 { Fuggi la frode ordita. }
    \livretPers Rinaldo
    \livretVerse#12 { E troppa crudeltà. }
    \livretVerse#12 { Armida… Ah… più non hà }
  }
  \force-line-width-ratio#0.3 \column {
    \livretVerse#12 { Ne moto, ne color. }
    \livretVerse#12 { Forse l’ucciderà }
    \livretVerse#12 { L’eccesso del dolor. }
    \livretVerse#12 { Ah mi si spezza il cor }
    \livretVerse#12 { Fra tanto affanno. }
    \livretVerse#12 { Senti… Non partirò. }
    \livretVerse#12 { Vedi… Che t’amo ancor. }
    \livretVerse#12 { Che parlo? Oh Dio! che fò? }
    \livretVerse#12 { Oh sfortunato amor! }
    \livretVerse#12 { Dover tiranno! }
    \livretVerse#12 { Come restar degg’io?… }
    \livretVerse#12 { Come partir dovrò?… }
    \livretPers Ubaldo
    \livretVerse#12 { Tu resta, io parto. }
    \livretPers Rinaldo
    \livretVerse#12 { Oh Dio! }
    \livretVerse#12 { Misera. }
    \livretPers Ubaldo
    \livretVerse#12 { O vieni, o resta. }
    \livretPers Rinaldo
    \livretVerse#12 { Che fiera legge è questa. }
    \livretPers Ubaldo
    \livretVerse#12 { Ora è il rigor pietà }
    \livretPers Rinaldo
    \livretVerse#12 { Povera Armida. Addio }
    \livretVerse#12 { Il duol m’ucciderà. }
  }
} #}))
