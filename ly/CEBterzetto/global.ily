\tag #'all \key mib \major
\tempo "Allegro" \midiTempo#160
\time 2/2 s1*98 \bar "||"
\time 2/4 \tempo "Allegro assai" \midiTempo#80 s2*38 \bar "||"
\time 2/2 s1*2
\tempo "A Tempo" \midiTempo#160 s1*7
\tempo "più allegro" s1*5
\tempo "Adagio" s1*4
\tempo "Allegro" s1*49
\tempo "Adagio" s1
\tempo "Allegro" s1
\tempo "Adagio" s1*4
\tempo "Allegro" s1*36
\tempo "più adagio" s1*5
\tempo "Allegro" s1*36
\time 4/4 s2 \bar "|."
