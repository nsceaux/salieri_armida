<<
  \tag #'(armida basse) {
    \tag #'basse <>^\markup\character Armida
    \clef "soprano/treble" mib''4 sib'8 sib' sol''4 mib'' |
    lab''4.( fa''8) re''4 r |
    mib''4 fa''8 sol'' \grace sol''8 fa''4 mib''8 [ re'' ] |
    \grace re''8 mib''2 r4 r8 mib'' |
    do''4. re''8 \grace fa''8 mib''4 re''8 do'' |
    do''4 sib' r sol''~ |
    sol'' fa''2 mib''4 |
    re''8.[ mib''16] fa''4 r sol''~ |
    sol'' fa''8 fa'' fa''4.\fermata sol''16[ mib''] |
    \grace mib''8 re''2 r4 r8 sib' |
    do''4. do''8 re''4. re''8 |
    mib''8[ fa''] sol''2 sol''4 |
    \grace sib''8 lab''4( sol''8) fa'' mib''4 re'' |
    mib''2 r |
  }
  \tag #'rinaldo { \clef "soprano/treble" R1*14 }
  \tag #'ubaldo { \clef "bass/G_8" R1*14 }
>>
<<
  \tag #'(armida ubaldo) R1*3
  \tag #'(basse rinaldo) {
    \tag #'basse { \ffclef "soprano/treble" <>^\markup\character Rinaldo }
    mib''4 sib' r r8 sol'' |
    mi''4 fa'' r sol''8[ mib''] |
    do''4\( re''\) r2 |
  }
>>
<<
  \tag #'(armida rinaldo) { R1*2 r4 }
  \tag #'(ubaldo basse) {
    \tag #'basse { \ffclef "bass/G_8" <>^\markup\character Ubaldo }
    sib4 do'8 re' mib'4 re' |
    do' re'8 mib' fa'4 mib' |
    re'4
  }
>>
<<
  \tag #'(armida basse) {
    \tag #'basse { \ffclef "soprano/treble" <>^\markup\character Armida }
    fa''4 mib'' re'' |
    re''\melisma do''2\melismaEnd re''8[ sib'] |
    la'8.[ sib'16] do''4 r2 |
    r r4 sib'8[ sib''] |
    \once\tieDashed sib''1~ |
    sib''2 la''!4( sol'') |
    fa''2. mib''4 |
    \grace mib''8 re''2\fermata r4 sib'8[ sib''] |
    sib''1~ |
    sib''2 \ficta la''4( sol'') |
    fa''2. la'!4 |
    sib'2 r |
    R1 |
    r2 r4 sib' |
    mib''2. mib''4 |
    sol''8[ fa''] mib''2 mib''4 |
    \grace mib''8 re''4 re'' <<
      \tag #'basse { s2 s1*2 s2 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { r2 R1*2 r2 }
    >> r4 sib' |
    do''4. do''8 re''4. re''8 |
    mib''2 fa''4. fa''8 |
    sol''4. sol''8 \grace sol''8 fa''4. mib''8 |
    re''4 fa'' <<
      \tag #'basse { s2 s1*7 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida {
        r2 R1*7
      }
    >>
    mib''2 mib''4. mib''8 |
    mib''2. mib''4 |
    solb''2 mib'' |
    r r4 mib'' |
    dob''2. dob''4 |
    sib'2. lab'4 |
    \grace lab'8 solb'1 |
    r2 r4 mib'' |
    mib''1~ |
    mib''2. mib''4 |
    re''2 re'' |
    R1 |
    mib''2 mib''4. mib''8 |
    fa''2 sol'' |
    lab''! fa'' |
    r2 r4 fa'' |
    mib''1 |
    fa'' |
    mib'' |
    re'' |
    sol'' |
    r2 r4 fa'' |
    mib''1 |
    re'' |
    sol'' |
    r2 r4 fa'' |
    mib''1 |
    reb'' |
    do''2 r |
    R1*2 |
  }
  \tag #'(rinaldo basse) {
    <<
      \tag #'basse {
        s2. s1*24
        \ffclef "soprano/treble" <>^\markup\character Rinaldo
      }
      \tag #'rinaldo {
        re''4 do'' sib' |
        sib'2. sib'8[ sol'] |
        fa'8.[ sol'16] la'4 r2 |
        R1 |
        r4 fa''4( sol'' lab''!) |
        \grace lab''8 sol''2 fa''4( mib'') |
        re''2. do''4 |
        \grace do''8 sib'2\fermata r |
        r4 fa''( sol'' \ficta lab'') |
        sol''2 fa''4( mib'') |
        \grace mib''8 re''2. do''4 |
        sib'2 r |
        R1*13 |
      }
    >>
    lab''!2 fa''4. re''8 |
    sib'2. lab'4 |
    \grace lab'8 sol'2 sol'4. mib''8 |
    mib''2. mib''4 re'' sib' r2 |
    R1 |
    \tag #'rinaldo {
      solb'2 solb'4. solb'8 |
      \ficta solb'2. solb'4 |
      mib'2 solb' |
      r2 r4 sib' |
      lab'2. lab'4 |
      solb'2. fa'4 |
      \grace fa'8 mib'1 |
      r2 r4 sib' |
      dob''1\melisma |
      do''!2.\melismaEnd do''4 |
      fa'2 fa' |
      R1 |
      sol'2 do''4. do''8 |
      re''2. mib''4 |
      fa''2 re'' |
      r2 r4 sol' |
      sol'1 |
      lab'2( sib') |
      do''1 |
      si' |
      do'' |
      r2 r4 re'' |
      do''1 |
      si' |
      do'' |
      r2 r4 re'' |
      do''1 |
      sib' |
      lab'!2 r |
      R1*2 |
    }
  }
  \tag #'(ubaldo basse) {
    <<
      \tag #'basse { s2. s1*15 s2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r4 r2 R1*15 r2 }
    >> r4 sib |
    fa'2 re' |
    sib2. lab!4 |
    sol4 sib <<
      \tag #'basse { s2 s1*3 s2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo {
        r4 sol |
        lab4. lab8 fa4. fa8 |
        sol2 re4 r |
        R1 |
        r2
      }
    >> r4 sib |
    mib'4. mib'8 do'4. la8 |
    \tag #'ubaldo {
      sib2 r2 |
      R1*5 |
      mib'2 mib'4. mib'8 |
      \ficta reb'2. reb'4 |
      dob'2 dob' |
      r2 r4 solb |
      lab2. lab4 |
      sib2. sib4 |
      dob'1 |
      r2 r4 solb |
      lab1\melisma |
      la2.\melismaEnd la4 |
      sib2 sib |
      R1 |
      do'2 do'4. do'8 |
      do'2. do'4 |
      si2 si |
      r r4 si |
      do'1 |
      fa |
      sol |
      sol |
      mib |
      r2 r4 fa |
      sol1 |
      sol |
      mib |
      r2 r4 fa |
      sol1 |
      sol |
      lab!2 r |
      R1*2 |
    }
  }
>>
%% 82
<<
  \tag #'(armida basse) {
    <<
      \tag #'basse { s1*17 | s4 \ffclef "soprano/treble" <>^\markup\character Armida }
      \tag #'armida { R1*17 | r4 }
    >> r8 sol'' |
    sol''8. mib''16 mib''8 sol'' |
    sol''4 re''8 re'' |
    fa''4 fa''8 fa'' |
    mib'' do'' r4 |
    R2 |
    r4 r8 do'' |
    lab''4 lab''8 lab'' |
    sol''8[ mib''] do'' do'' |
    lab''4 lab''8 lab'' |
    sol''8[ mib''] do'' mib'' |
    \grace mib'' re''4 do''8 si' |
    do''4 r |
    r4 r8 reb'' |
    do''4 mib'' |
    r4 r8 reb'' |
    mib''4 mib'' |
    r4 r8 mi'' |
    fa''2 |
    mib''!4. mib''8 |
    \grace mib''8 re''4 re'' |
    r4 r8 re'' |
    \grace mib''8 re''4 re'' |
    r4 r8 re'' |
    \grace mib''8 re''4 re'' |
    r4 r8 re'' |
    re''2 |
    mib''4. fa''8 |
    sol''2 |
    lab''4. lab''8 |
    sol''2 |
    si'4. si'8 |
    \grace si'8 do''2 |
    r4 r8 do'' |
    lab''2\fermata |
    fa''4 r |
    R2 |
    r4 r8 sol'' |
    do''4 r4 \tag #'armida { r2 | R1*149 | r2 }
  }
  \tag #'(rinaldo basse) {
    <<
      \tag #'basse { s1*3 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { R1*3 r2 }
    >> r4 sib' |
    do''8[ re''] mib''2 mib''4 |
    sol''8[ fa''] mib''2 re''8[ do''] |
    sib'2.( lab'4) |
    sol'2 r |
    do''2 do''4. do''8 |
    lab''2. sol''8[ fa''] |
    re''4 mib'' r mib'' |
    do''4. do''8 sib'4. lab'8 |
    \grace lab'8 sol'2 r |
    do''2 do''4. do''8 |
    lab''2. sol''8[ fa''] |
    re''4 mib'' r mib'' |
    \grace re''8 do''4. do''8 \grace do''8 sib'4. re'8 |
    mib'4 <<
      \tag #'basse { s4 s2*37 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r4 | R2*37 | r2 }
    >> mi''16 mi'' mi''8 <<
      \tag #'basse { s4 s4 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag rinaldo { r4 | r4 }
    >> r8 do'' do'' re'' mib'' fa'' |
    re'' re'' r4 <<
      \tag #'basse { s2 s1 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 | R1 | r2 }
    >> r4 r8 sol'' |
    \grace sol''8 fa''4. mib''8 \grace mib''8 re''4. dod''8 |
    \grace dod''8 re''2 r4 r8 sol'' |
    \grace sol''8 fa''4. mib''8 \grace mib''8 re''4. do''8 |
    sib'4 r r2 |
    R1 |
    r2 r4 sib' |
    mib''2 sib'4 sib' |
    sol''2 mib''4 r |
    R1 |
    mib''2. re''8 do'' |
    \grace do''8 sib'2 r4 sib' |
    lab'2 sol' |
    fa'2. sol'4 |
    mib'4 r r2 |
    sib'4 sib'8 sib' re''4 fa'' |
    \grace fa''8 mib''2 r4 mib''8[ do''] |
    sib'([ la']) mib''([ do'']) sib'([ la']) mib''([ do'']) |
    \grace do''8 reb''2 r4 fa''8[ reb''] |
    do''[ sib'] fa''[ reb''] \grace mib''8 reb''4 do''8[ sib'] |
    \grace sib'8 la'2 \grace mib''8 reb''4 do''8[ sib'] |
    \grace sib' la'2 r\fermata |
    sib'4 r sib'4. sib'8 |
    sib'2 r4 sib' |
    sol''1~ |
    sol''2 r4 mib''8[ re''] |
    si'[ do''] mib'' re'' mib''[ do''] mib''[ do''] |
    sib'2. la'4 |
    sib'4 r r2 |
    r2 r4 re'' |
    \grace re''8 do''4 do'' r re''8[ sib'] |
    la'4 la' r2\fermata |
    sib'2 sib'4. sib'8 |
    sib'2 r4 sib' |
    sol''1~ |
    sol''2 r4 mib''8[ re''] |
    si'[ do''] mib'' re'' si'[ do''] mib''[ re''] |
    si'[\melisma do'' re'' mib''] fa''[ sol'' la'' sib''!] |
    la''[ sol'' fa'' sol''] fa''[ mib'' re'' do''] |
    sib'2. do''4\melismaEnd |
    sib'2 r |
    R1*2 |
    sib'4 sib' r2 |
    r4 sib' re'' do'' |
    sib' r r2 |
    si'4 si' r2 |
    r4 \ficta si' re'' do'' |
    si' r r2 |
    r4 re'' fa'' mib'' |
    re'' r r si' |
    do'' do'' r do'' |
    \grace sol''8 fad''4 fad'' r mib''!8[ do''] |
    si'4 r r sol'' |
    \grace sol''8 fad''4 fad'' r mib''8[ do''] |
    \grace do''8 si'2 r4 si' |
    do''4 re'' mib'' fa'' |
    sol'' fa'' sol'' lab'' |
    mib''2.\melisma re''4\melismaEnd |
    mib''4 fa'' sol'' lab'' |
    mib''2.\melisma re''4\melismaEnd |
    do''4 r r2 |
    R1 |
    sib'4 sib' r\fermata sib'8[ lab'!] |
    sol'4 sol' r2 |
    mib''2. re''8 do'' |
    \grace do''8 sib'2 r4 sib' |
    lab'2 sol' |
    fa'2. sol'4 |
    mib'2 r |
    mib''4 reb''8 do'' do''[ sib'] lab'[ sol'] |
    lab'4 r r r8 mib'' |
    mib''2. mib''4 |
    solb''2. fa''8[ mib''] |
    \grace mib''8 re''2 solb''4 fa''8[ mib''] |
    re''4 r r2\fermata |
    mib''2 mib''4. mib''8 |
    mib''2. mib''4 |
    lab''1~ |
    lab''2 r4 sol''8[ mib''] |
    si'8[\melisma do''] re''[ mib''] fa''[ sol'']\melismaEnd lab''[ fa''] |
    mib''2.( fa''4) |
    mib''2 r4 sol' |
    lab' sol' r sib' |
    do'' sib' r reb'' |
    do''4 r r2 |
    mib''2 mib''4 r8 mib'' |
    mib''2 r4 mib'' |
    lab''1~ |
    lab''2. sol''8[ mib''] |
    si'[\melisma do'' re'' mib'' fa'' sol''] lab''\melismaEnd fa'' |
    mib''2.( fa''4) |
    mib''4 mib'' mib'' mib'' |
    solb''2. fa''8 mib'' |
    re''8([\melisma fa'']) lab''([ fa'']) lab''([ fa''])\melismaEnd lab''([ fa'']) |
    mib''2.\melisma fa''4\melismaEnd |
    mib''4 mib'' mib'' mib'' |
    solb''2. fa''8 mib'' |
    re''8[\melisma fa''] lab''[ fa''] lab''[ fa'']\melismaEnd lab''[ fa''] |
    mib''2.\melisma fa''4\melismaEnd |
    mib''2 r |
    R1*4 |
    mib''4 re''8 do'' do''4. do''8 |
    \grace do''8 si'2 r |
    fa''4 mib''8 re'' re''4. fa''8 |
    \grace fa''8 mib''2 <<
      \tag #'basse { s2 s1 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 R1 r2 }
    >> r4 sol''4 |
    \grace sol''8 fad''2 r |
    mib''8. re''16 re''4 <<
      \tag #'basse { s2 s1 s2 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { r2 R1 r2 }
    >> r4 re'' |
    sol''2 re'' |
    mib''2. do''4 |
    <<
      \tag #'basse { s1*5 \ffclef "soprano/treble" <>^\markup\character Rinaldo }
      \tag #'rinaldo { si'4 sol' r2 R1*4 }
    >>
    mib''2. re''8 mib'' |
    do''4 do'' r2 |
    fa''2. mib''8 fa'' |
    re''4 re'' r r8 sib' |
    \grace fa''8 mib''2 r4 r8 sib' |
    \grace sol''8 fa''2 r |
    r r4 r8 mib'' |
    do''4 r8 do'' sib'4 r8 re'' |
    \grace re'' mib''2 r4 r8 mib'' |
    \grace re''8 do''4 r8 do'' \grace do'' sib'4 r8 re' |
    mib'2 r |
    R1*13 r2
  }
  \tag #'(ubaldo basse) {
    \tag #'basse { \ffclef "bass/G_8" <>^\markup\character Ubaldo }
    R1*2 |
    mib'4 sib8. sib16 sib4. sib8 |
    sol4 mib <<
      \tag #'basse { s2 s1*13 s2*38 s2. \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r2 R1*13 R2*38 | r2 r4 }
    >> r16 do' do' sol |
    la8 la <<
      \tag #'basse { s2. s2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r4 r2 | r2 }
    >> re'4 sib |
    mib'4 do'8 la fa4. mib!8 |
    re4 re <<
      \tag #'basse { s2 s1*107 s2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r2 R1*107 r2 }
    >> r4 sol |
    do' do' r mib' |
    do' do' <<
      \tag #'basse { s2 s1 s2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r2 R1 r2 }
    >> r4 re' |
    re' re r do' |
    si sol <<
      \tag #'basse { s2 s1*2 \ffclef "bass/G_8" <>^\markup\character Ubaldo }
      \tag #'ubaldo { r2 R1*2 }
    >>
    sol2 sol4. sol8 |
    la2 si |
    do'2 lab!4. lab8 |
    fa2 sol |
    do r |
    \tag #'ubaldo { R1*24 r2 }
  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
<<
  \tag #'armida {  }
  \tag #'rinaldo {  }
  \tag #'ubaldo {  }
>>
