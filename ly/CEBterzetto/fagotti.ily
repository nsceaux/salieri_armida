\clef "bass" mib4\f r mib\p r |
fa2.-\sug\f r4 |
R1*11 |
r4 mib-\sug\p sol mib |
r mib sol mib |
r re sib, la, |
r sib, re fa |
sib do'8. re'16 mib'4 re' |
do' re'8. mib'16 fa'4 mib' |
re'4 r r2 |
R1*18 |
mib2:8\p\cresc  sol: |
lab: fa: |
sol:-\sug\f re4 r |
R1*3 |
sib,8\f sib sib sib sib2:8 |
re: re: |
mib: mib: |
do: do: |
sib,4 re8 fa sib4 sib |
sib2 r |
R1*11 |
si1-\sug\f |
do'4-\sug\p r r2 |
R1*3 |
do'4-\sug\p r r2 |
fa4 r r2 |
sol4 r r2 |
sol,4 r r2 |
mib1-\sug\f |
fa4-\sug\p r r2 |
sol4 r r2 |
sol,4 r r2 |
mib1\f |
fa4\p r r2 |
sol4\cresc sol sol sol |
sol\ff sol sol sol |
lab1_\markup\italic calando ~ |
lab~ |
lab2\fermata r | \allowPageTurn
sol4.-\sug\f sol8 re4. re8 |
mib2 r |
mib4 r re r |
mib r r2 |
R1*13 |
%%
R2*7 |
fa,8-\sug\fp fa fa fa |
mib4 r |
fa,8-\sug\f fa fa fa |
mib4 r |
R2*9 |
fad4( sol |
lab\rf sol) |
fad( sol |
lab\rf sol) |
fad( sol |
lab\rf sol) |
fad( fa |
mib re) |
do do |
fa-\sug\f fa |
sol-\sug\p sol |
sol sol, |
do r |
R2*5 |
%%
R1*9 |
mib4\f mib mib mib |
mib mib fa sib, |
mib\p mib mib mib |
mib mib mib mib |
mib r r2 |
R1*11 |
r4 fa-\sug\f fa, r\fermata |
R1*2 |
mib4-\sug\ff re do sib, |
la,2 r |
R1*2 |
sib,4-\sug\f sib, sib, sib, |
sib, r r2 |
R1*4 |
mib4-.\ff re-. do-. sib,-. |
la,2 r |
R1*3 |
sol8-\sug\ff sol fa mi fa fa, fa, fa, |
sib,4 sib, sib, sib, |
sib, sib, sib, sib, |
sib, r r2 |
R1 | \allowPageTurn
r4 <>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { sib4 re' do' | sib }
  { re fa mib | re }
>> r4 r2 |
R1 |
r4 \twoVoices #'(primo secondo tutti) <<
  { si4 re' do' | si }
  { re fa mib | re }
>> r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { re'4 fa' mib' | re' }
  { si re' do' | si }
>> \tag#'tutti <>^"a 2." sol\p-\sug\cresc sol sol |
lab! r r2 |
lab4 lab lab lab |
sol-\sug\ff r r2 |
lab4 lab lab lab |
sol sol fa! fa |
mib8\f mib \ficta si, si, do do re re |
mib mib re re mib mib fa fa |
sol2:8 sol,: |
do8 do re re mib mib fa fa |
sol2: sol,: |
do4 do do do |
do do do do |
re re re\fermata r |
mib r r2 |
R1*8 |
dob4-\sug\f dob dob dob |
sib, r dob dob |
sib, sib sib, r\fermata |
R1*2 |
fa4-.\ff mib-. re-. do-. |
sib,2 r |
R1*8 |
fa4-.\ff mib-. re-. do-. |
sib,2 r |
R1 |
sib4-\sug\f sib4 sib, sib, |
mib r r2 |
R1 |
r2 lab!:8 |
sib: sib,: |
mib4 r r2 |
R1 |
r2 lab!:8 |
sib: sib,: |
mib4 mib mib mib |
mib mib mib mib |
mib mib mib mib |
mib mib mib mib |
mib2 r |
R1*10 |
sol2:8-\sug\f sol: |
sol: sol: |
sol: sol: |
la: si: |
do': lab!: |
fa: sol: |
do r |
R1*6 |
sib,2(-\sug\f mib4) r |
R1*17 |
r2
