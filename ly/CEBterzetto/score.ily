\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr  } <<
        \global \keepWithTag #'tutti \includeNotes "oboi"
      >>
      \new Staff \with { \corniEbInstr  } <<
        \keepWithTag #'corni \global \keepWithTag #'tutti \includeNotes "corni"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with { \haraKiri } <<
      \new Staff \with {
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Ar.
      } \withLyrics <<
        { \noHaraKiri s1*80 \revertNoHaraKiri }
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Rinaldo
        shortInstrumentName = \markup\character Ri.
      } \withLyrics <<
        { \noHaraKiri s1*80 \revertNoHaraKiri }
        \global \keepWithTag #'rinaldo \includeNotes "voix"
      >> \keepWithTag #'rinaldo \includeLyrics "paroles"
      \new Staff \with {
        instrumentName = \markup\character Ubaldo
        shortInstrumentName = \markup\character Ub.
      } \withLyrics <<
        { \noHaraKiri s1*80 \revertNoHaraKiri }
        \global \keepWithTag #'ubaldo \includeNotes "voix"
      >> \keepWithTag #'ubaldo \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion {
          s1*81\break s1*17\break
          %% 2/4
          s2*38\break
          %% 2/2
          s1*109\break
        }
        \origLayout {
          s1*3\pageBreak
          \grace s8 s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          %% 5
          s1*4\pageBreak
          s1*5\pageBreak
          \grace s8 s1*5\pageBreak
          \grace s8 s1*5\pageBreak
          s1*4\pageBreak
          %% 10
          s1*4\pageBreak
          s1*4\pageBreak
          \grace s8 s1*5\pageBreak
          s1*5\pageBreak
          \grace s8 s1*5\pageBreak
          %% 15
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 20
          s1*5\pageBreak
          s1*5\pageBreak
          s1*2 s2*3\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          %% 25
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          s2*5\pageBreak
          %% 30
          s1*3\pageBreak
          s1*4\pageBreak
          \grace s8 s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 35
          \grace s8 s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*4\pageBreak
          %% 40
          s1*5\pageBreak
          s1*5\pageBreak
          \grace s8 s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 45
          \grace s8 s1*5\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 50
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          %% 55
          s1*5\pageBreak
          s1*6\pageBreak
          s1*6\pageBreak
          s1*7\pageBreak
          s1*8\pageBreak
          %% 60
          s1*2
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
