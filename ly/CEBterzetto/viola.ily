\clef "alto" mib4\f r mib\p r |
fa4.\f sol8 lab2 |
sol4\p sol lab sib |
mib'4 r mib r |
lab r lab r |
mib r r8 sol'( sib'\rf sol') |
\slurDashed r8 sol'( fa'\rf sib') r fa'( sol'\rf mib') | \slurSolid
r re'( fa'\rf re') r sol'( sib'\rf sol') |
r sol'( fa')\rf sib' mib'2\fermata |
re' r |
lab4 r fa r |
sol8(\f fa mib) mib-! mib2:8 |
do'8 do' lab lab sib2:8 |
mib'4 mib\p sol mib |
r mib sol mib |
r re' sib la |
r sib re' fa' |
sib do'8. re'16 mib'4 re' |
do' re'8. mib'16 fa'4 mib' |
re'4 r re2-\sug\rf |
mib mi |
fa la\f |
sib4 sib' sib r |
r lab'!(\rf sol' fa') |
mib' mib'\p mib' mib' |
fa' fa' fa fa |
sib2 r\fermata |
r4 \once\slurDashed lab'!(\rf sol' fa') |
mib' mib'\p mib' mib' |
fa' fa' fa fa |
sib sib\f la4.\trill sib16 la |
sol4 r sol r |
sol r r2 |
sol'4\p sol' sol' sol' |
sol' sol' sol' sol' |
fa' fa' fa' fa' |
sib sib sib sib |
re' re' re' re' |
mib'2:8-\sug\cresc sol: |
lab: fa: |
sol:-\sug\f re: |
mib4-\sug\p r la r |
sib r sib r |
do' r fa r |
sib16\f re' re' re' re'2.:16 |
re'2: re': |
mib': mib'4: sol': |
fa'2: fa': |
fa'8 sib re' fa' sib4 sib |
sib2 r |
r4 sib'\p sib' sib' |
r sib' sib' sib' |
r dob'' dob'' dob'' |
r solb' solb' solb' |
r lab' lab' lab' |
r sib' sib' sib |
r dob' dob' dob' |
r solb' solb' solb' |
r lab'! lab' lab' |
r la'! la' la' |
r sib' sib' sib' |
r si'\f si' si' |
r sol'-\sug\p sol' sol' |
r lab' lab' sol' |
r fa' lab' lab' |
r sol' sol' sol' |
r8 sol' sol' sol' sol'2:8 |
r8 lab' lab' lab' lab'2: |
r8 sol' sol' sol' sol'2: |
r8 sol' sol' sol' sol'2: |
r8 sol'-\sug\f sol' sol' sol'2: |
r8 lab'\p lab' lab' lab'2: |
r8 sol' sol' sol' sol'2: |
r8 sol' sol' sol' sol'2: |
r8 sol'\f sol' sol' sol'2: |
r8 lab'\p lab' lab' lab'2: |
r8 sol'-\sug\cresc sol' sol' sol'2: |
sib'2:-\sug\ff sib': |
lab'4_\markup\italic\tiny calando lab' lab' lab' |
lab' lab' lab' lab' |
lab'2\fermata r | \allowPageTurn
sol'4.\f sol'8 re'4. re'8 |
mib'2 r |
sib'4 r sib r |
r mib'-!-\sug\p sol'( mib') |
r4 lab-! do'( lab) |
r \once\slurDashed sol( fa mib) |
r sib do' re' |
r mib'-! \once\slurDashed sol'( mib') |
\ru#4 { do'8 mib' } |
<>\f \ru#4 { do'-. fa'-. } |
sib8-. sib-. sib-. sol'-. mib'-. sol'-. mib'-. sol'-. |
lab'4\p r sib r |
r mib' sol' mib' |
\ru#4 { do'8-. mib'-. } |
<>\f \ru#4 { do'-. fa'-. } |
sib8-. sib-. sib-. sol'-. mib'-. sol'-. mib'-. sol'-. |
r4 lab'\p sib' sib |
%%
mib' r |
mib8\fp mib' mib' mib' |
re\fp re' re' re' |
si'\fp si si si |
do'4 r8 do'\ff |
\grace do'16 si8 la16 sol \grace do'16 si8 la16 sol |
do'4 r |
fa8-\sug\fp fa' fa' fa' |
mib'2:8-\sug\fp |
fa8-\sug\f fa' fa' fa' |
mib'2:8-\sug\fp |
fa'8 fa' sol' sol' |
sol' r sol' r |
sib' r sib' r |
lab'! r lab' r |
sib' r sib' r |
lab' r lab' r |
sol' r sol' r |
lab' r lab' r |
sol' r sol' r |
fad4( sol |
lab\rf sol) |
fad( sol |
lab\rf sol) |
fad( sol |
lab\rf sol) |
fad( fa |
mib re) |
do do' |
fa'\f fa' |
sol'\p sol' |
sol' sol |
do' do' |
mib' mib' |
\once\tieDashed fa'2\f\fermata~ |
fa'4 r |
sol'4\p r |
sol r |
%%
do'8\fermata r16 do'-\sug\f do'8. do'16 do'4 r |
la4\p r r2 |
sib4 r sib r |
do' r la r |
sib r mib' r |
fa' r fa r |
sib r mib' r |
fa' r fa r |
sib lab! sol fa |
sol8\f sol4 sol sol sol8 |
sol sol4 sol8 fa lab4 fa8 |
mib4-\sug\p mib mib mib |
mib mib mib mib |
mib r r2 |
mib'4 r mib' r |
re' r re' r |
do' r sib r |
lab r sib r |
\ru#4 { mib'8-. sol'-. } |
\ru#4 { re'-. fa'-. } |
\ru#4 { do'-. mib'-. } |
la8 la4 la la la8 |
sib sib4 sib sib sib8 |
sib sib4 sib sib sib8 |
la la4 la8 sib sib4 sib8 |
\ficta la4 fa'\f fa r\fermata |
sib4\p sib' la' la' |
sol' sol' fa' fa' |
mib'\ff re' do' sib |
la2. sib4-\sug\p |
mib' mib' mib' mib' |
fa' fa' fa fa |
sib-\sug\f sib sib sib |
sib r r2 |
r4 sol' sol' r |
r fa' fa r\fermata |
sib\p sib' la' la' |
sol' sol' fa' fa' |
mib'\ff re' do' sib |
la2. sib4\p |
mib' mib' mib' mib' |
mib'-\sug\f mib' mib' mib' |
fa' fa' fa' fa' |
sol'8\ff sol' fa' mi' fa' fa fa fa |
sib4 sib sib sib |
sib sib sib sib |
sib r r2 |
sib4-\sug\p r r2 |
r4 <>\p << { sib4 re' do' | sib } \\ { re4 fa mib | re } >> r r2 |
sol4 r r2 |
r4 << { si re' do' | si } \\ { re fa mib | re } >> r r2 | \allowPageTurn
r4 << { re' fa' mib' | re'8 si re' si } \\ { si4 re' do' | si8-\sug\p -\sug\cresc si si si\! } >>
re'8-. si-. re'-. si-. |
\ru#4 { do'-. lab-. } |
do' lab do' lab do' lab lab lab |
sol-\sug\ff re' si re' re' si re' si |
do' lab do' lab do' lab lab lab |
sol si re' si sol4 fa |
mib8\f mib si si do' do' re' re' |
mib' mib' re' re' mib' mib' fa' fa' |
sol'2:8 sol: |
do'8 do' re' re' mib' mib' fa' fa' |
sol'2: sol: |
do'4 do' do' do' |
do' do' do' do' |
re' re' re'\fermata r |
mib' r r2 |
mib'4-\sug\p r mib' r |
re' r re' r |
do' r sib r |
lab r sib r |
sol8-. sol-. sib-. sol-. sib-. sol-. sib-. sol-. |
sib sol sib sol sib sol sib sib |
do' lab do' lab do' lab do' lab |
dob'4 dob' dob' dob' |
\ficta dob'\f dob' dob' dob' |
sib sib dob' dob' |
sib sib sib r\fermata |
mib'4\p mib' re' re' |
do' sib lab sol |
fa-!\ff mib-! re-! do-! |
re2 r4 mib\p |
lab lab lab lab |
sib2:8 sib: |
mib'4 r r2 |
mib'4 r r2 |
mib'4 r r2 |
lab'4 r r sib |
mib' mib' re' re' |
do' sib lab sol |
fa-.\ff mib-. re-. do-. |
re2. mib4-\sug\p |
lab lab lab lab |
sib\ff sib sib sib |
mib'2:8\p do'8 do' sib sib |
la2: la: |
sib: lab!: |
sib: sib: |
mib': do'8 do' sib sib |
la2: la: |
sib: lab!: |
sib: sib: |
mib'4 mib' mib' mib' |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
mib' mib' mib' mib' |
mib'2 r |
r4 mib'\p r mib' |
r re' r re' |
r si r si |
r do' r do' |
r do' r do' |
do' do' do' do' |
lab! lab lab sol |
fad fad fad sol |
fad re mi fad |
sol2:8 sol: |
sol:-\sug\f sol: |
sol: sol: |
sol: sol: |
la: si: |
do': lab!: |
fa: sol: |
do r |
sol1\p |
lab2 r |
lab1 |
sib2 r |
sol r |
re r |
re'2(\f mib'4) r8 mib'\p |
lab4 r8 lab sib4 r8 sib |
sib2 r4 r8 sol |
lab4 r8 lab sib4 r8 sib |
mib2( sib) |
do'4 fa' fa'2 |
mib' do' |
do' sib |
sol sib |
do'4 fa' fa'2 |
mib' do' |
do' sib |
mib mib |
mib mib |
mib mib |
mib mib |
mib mib |
mib mib |
mib
