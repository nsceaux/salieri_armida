\clef "bass" mib4\f r mib\p r |
fa2.\f fa4 |
sol\p sol lab sib |
mib r mib r |
lab r lab, r |
mib r mib'2 |
re' do' |
sib mib' |
re' do'\fermata |
sib r |
lab4 r fa r |
sol8(\f fa mib) mib-! mib2:8 |
do'8 do' lab lab sib sib sib, sib, |
mib4 mib\p sol mib |
r mib sol mib |
r re sib, la, |
r sib, re fa |
sib do'8. re'16 mib'4 re' |
do' re'8. mib'16 fa'4 mib' |
re'4 r re2\rf |
mib2 mi |
fa la\f |
sib sib,4 r |
r re(\rf mib fa) |
mib mib\p mib mib |
fa fa fa, fa, |
sib,2 r\fermata |
r4 re(\rf mib fa) |
mib mib\p mib mib |
fa fa fa, fa, |
sib, sib\f la?4.\trill sib16 la |
sol4 r sol, r |
sol r r2 |
sol4\p sol sol sol |
sol sol sol sol |
fa fa fa fa |
sib, sib, sib, sib, |
re re re re |
mib8\p\cresc mib mib mib sol2:8 |
lab: fa: |
sol:\f re: |
mib4\p r la, r |
sib,4 r sib r |
do' r fa r |
sib,8\f sib sib sib sib2:8 |
re: re: |
mib: mib: |
do: do: |
sib,4 re8 fa sib4 sib |
sib2 r |
mib'4\p r r2 |
reb'4 r r2 |
dob'4 r r2 |
solb4 r r2 |
lab4 r r2 |
sib4 r r2 |
dob'4 r r2 |
solb4 r r2 |
lab4 r r2 |
la4 r r2 |
sib4 r r2 |
si2\f r |
do'4\p r r2 |
do'4 r r2 |
si4 r r2 |
sol4 r r2 |
do'4 r r2 |
fa4 r r2 |
sol4 r r2 |
sol,4 r r2 |
mib1-\sug\f |
fa4\p r r2 |
sol4 r r2 |
sol,4 r r2 |
mib1\f |
fa4\p r r2 |
sol4\cresc sol sol sol |
sol\ff sol sol sol |
lab!_\markup\italic calando lab lab lab |
lab lab lab lab |
lab2\fermata r2 | \allowPageTurn
sol4.\f sol8 re4. re8 |
mib2 r |
mib4 r re r |
r mib-!\p sol( mib) |
r lab-! do'( lab) |
r sol( fa mib) |
r sib, do re |
r mib-! sol( mib) |
r lab-! do'( lab) |
r lab-!\f do'( lab) |
r sol-! mib( sol) |
lab\p r sib r |
r mib-! sol( mib) |
r lab-. do'-. lab-. |
r lab\f do' lab |
r sol mib sol |
r lab-\sug\p sib sib, |
%%
mib4 r |
do8\fp do' do' do' |
si,\fp si si si |
sol,\fp sol sol sol |
do4 r8 do'\ff |
\grace do'16 si8 la16 sol \grace do'16 si8 la16 sol |
do'4 r |
fa,8\fp fa fa fa |
mib2:8\fp |
fa,8\f fa fa fa |
mib2:\fp |
fa8 fa sol sol |
do' r do' r |
sol r sol r |
lab! r lab r |
sol r sol r |
lab r lab r |
sol r sol r |
lab r lab r |
sol r sol r |
fad4( sol |
lab\rf sol) |
fad( sol |
lab\rf sol) |
fad( sol |
lab sol) |
fad( fa |
mib re) |
do do |
fa\f fa |
sol\p sol |
sol sol, |
do do |
mib mib |
fa2\fermata\f~ |
fa4 r |
sol\p r |
sol, r |
do8\fermata r16 do'-\sug\f do'8. do'16 sib4 r |
la\p r r2 |
sib4 r sib r |
do' r la r |
sib r mib r |
fa r fa, r |
sib, r mib r |
fa r fa, r |
sib lab! sol fa |
mib\f mib mib mib |
mib mib fa sib, |
mib\p mib mib mib |
mib mib mib mib |
mib r r2 |
mib4 r mib r |
re r re r |
do r sib, r |
lab, r sib, r |
mib mib mib mib |
re re re re |
do do do do |
fa fa fa fa |
fa fa fa fa |
fa fa mi mi |
fa fa mi mi |
fa fa\f fa, r4\fermata |
sib4\p sib la la |
sol-. sol-. fa-. fa-. |
mib-.\ff re-. do-. sib,-. |
la,2. sib,4\p |
mib mib mib mib |
fa fa fa fa, |
sib,\f sib, sib, sib, |
sib, r r2 |
r4 mib\f mi r |
r fa fa, r\fermata |
sib4-\sug\p sib la la |
sol sol fa fa |
mib-!\ff re-! do-! sib,-! |
la,2. sib,4\p |
mib mib mib mib |
mib\f mib mib mib |
fa fa fa fa |
sol8\ff sol fa mi fa fa, fa, fa, |
sib,4 sib, sib, sib, |
sib, sib, sib, sib, |
sib, r r2 |
sib,2\p r |
sib, r |
sib, r |
sol, r |
sol, r |
sol, r |
sol, r | \allowPageTurn
sol4\p\cresc sol sol sol |
lab! lab lab lab |
lab lab lab lab |
sol\ff sol sol sol |
lab lab lab lab |
sol sol fa fa |
mib8\f mib \ficta si, si, do do re re |
mib mib re re mib mib fa fa |
sol2:8 sol,: |
do8 do re re mib mib fa fa |
sol2: sol,: |
do4 do do do |
do do do do |
re re re\fermata r |
mib r r2 |
mib4-\sug\p r mib r |
re r re r |
do r sib, r |
lab, r sib, r |
mib mib mib mib |
reb reb reb reb |
do do do do |
dob dob dob dob |
dob\f dob dob dob |
sib, sib, dob dob |
sib, sib sib, r\fermata |
mib'4\p mib' re' re' |
do' sib lab sol |
fa-.\ff mib-. re-. do-. |
sib,2. mib4\p |
lab4 lab lab lab |
sib2:8\p sib,: |
mib4 r r2 |
mib2 r |
mib r |
lab4 r r sib, |
mib mib' re' re' |
do' sib lab sol |
fa-.\ff mib-. re-. do-. |
sib,2. mib4\p |
lab4 lab lab lab |
sib\f sib sib, sib, |
mib8\p mib mib' mib' do' do' sib sib |
la2:8 la: |
sib: lab!: |
sib: sib,: |
mib8 mib mib' mib' do' do' sib sib |
la2:8 la: |
sib: lab!: |
sib: sib,: |
mib4 mib mib mib |
mib mib mib mib |
mib mib mib mib |
mib mib mib mib |
mib2 r |
mib'4-\sug\p r mib' r |
re' r re' r |
si r si r |
do' r do' r |
do' r do' r |
do' do' do' do' |
lab lab lab sol |
fad fad fad sol |
fad re mi fad |
sol2:8 sol: |
sol:\f sol: |
sol: sol: |
sol: sol: |
la: si: |
do': lab!: |
fa: sol: |
do2 r |
sol1\p |
lab2 r |
lab1~ |
lab2 r |
sol2 r |
re r |
sib,2\f mib4 r8 mib\p |
lab4 r8 lab sib4 r8 sib, |
mib2 r4 r8 sol |
lab4 r8 lab sib4 r8 sib, |
mib2 sol |
lab sib, |
do do' |
lab sib |
mib sol |
lab sib, |
do do' |
lab sib |
mib mib |
mib mib |
mib mib |
mib mib |
mib mib |
mib mib |
mib
