\clef "treble"
<<
  \tag #'primo {
    <sib sol' mib''>4\f r sol''\p mib'' |
    lab''4.(\f fa''8) re'' re'' re'' re'' |
    mib''4\p fa''8 sol'' \grace sol'' fa''4 mib''8 re'' |
    mib''8( re'' mib'' fa'') sol''-. mib''-. sib'-. sol'-. |
    do''8( si' do'' re'') \grace fa'' mib''4 re''8( do'') |
    do''4 sib' r8 sol''(\rf sib'' sol'') |
    r sol''( fa''\rf sib'') r fa''( sol''\rf mib'') |
    r re''8( fa''\rf re'') r sol''( sib''\rf) sol'' |
    r sol''( fa''\rf sib'') fa''2\fermata |
    fa'' r4 r8 sib' |
    do''-! lab'( sol' fa') re''-! sib'( la' sib') |
    mib''(\f fa'' sol'') sol''-! sol''2:8 |
    \grace sib''8 lab''4( sol''8 fa'') mib''4 re'' |
    <>\p \ru#8 { mib''8-. sib'-. } |
    mi''-. sib'-. fa''-. sib'-. fa''-. re''-. sol''-. mib''-. |
    do''-. la'-. re''-. sib'-.
  }
  \tag #'secondo {
    <sol mib' sib'>4-\sug\f r sib'-\sug\p sol' |
    <sib' re'>2.-\sug\f sib'4 |
    sib'4-\sug\p sib' do'' fa' |
    sol'8( fa' sol' lab') sib'-. sol'-. sol'-. mib'-. |
    mib'4 r r8 do''( sib' lab') |
    lab'4 sol' sib'2 |
    sib' la' |
    sib' sib' |
    sib' la'\fermata |
    sib' r |
    mib'4 r lab r |
    sib'8(-\sug\f lab' sol') sib'-! sib'2:8 |
    mib''8 mib'' do'' do'' sol'4 fa' |
    sol'8-.-\sug\p sol'-. sib'-. sol'-. sib'-. sol'-. sib'-. sol'-. |
    \ru#4 { sib'-. sol'-. } |
    sib'-. sol'-. sib'-. fa'-. re'-. fa'-. mib'-. do'-. |
    mib'-. do'-. sib-. sib-.
  }
>> re'8 re' fa' fa' |
sib'4 r r2 |
R1 |
<<
  \tag #'primo {
    r4 fa''( mib''\rf re'') |
    re'' do''2 re''8 sib' |
    \once\slurDashed la'8.( sib'16 do''4.) fa''8(\f sol'' mib'') |
    \once\slurDashed re''8.( mib''16 fa''4) r sib'8(\rf sib'') |
    sib''1 |
    sib''2 la''4\p sol'' |
    r fa'' fa'' mib'' |
    \grace mib''8 re''2 r4\fermata sib'8\rf sib'' |
    sib''1~ |
    sib''2 la''4\p sol'' |
    r fa'' fa'' la'-! |
    sib'4 r r2 |
    re''4\f <re'' re'>4. re''8( mi'' fad'') |
    sol''( la'' sib'' la'') sol''( fa''! mib''! re'') |
    mib''4\p mib'' mib'' mib'' |
    sol''8(\rf fa'') mib''4 mib'' mib'' |
    re'' re'' re'' re'' |
    fa''8 fa''[ fa'' fa''] re'' re''[ re'' re''] |
    sib' sib' sib' sib' sib' sib' lab'! lab' |
    sol'4 sib' r sib'\cresc |
    do''4. do''8 re''4. re''8 |
    mib''4.\f mib''8 fa'' fa'' fa'' fa'' |
    r sol''\p sol'' sol'' r sol''( fa'') mib'' |
    r8 re''-. re''( fa'') sib'' fa'' re'' sib' |
    mib'' mib'' mib'' mib'' do'' do'' la' la' |
    sib'16\f <sib' lab''!> q q q2.:16 |
    q2: q: |
    <sib' sol''>2: q: |
    la'': la'': |
    sib''8 sib' re'' fa'' <re' re'' sib''>4 q |
    q2 r |
    r4 mib''\p mib'' mib'' |
    r mib'' mib'' mib'' |
    r mib'' mib'' mib'' |
    r mib'' mib'' mib'' |
    r dob'' dob'' dob'' |
    r sib' sib' lab' |
    r solb' solb' solb' |
    r mib'' mib'' mib'' |
    r mib'' mib'' mib'' |
    r mib'' mib'' mib'' |
    r re'' re'' re'' |
    r fa''\f fa'' fa'' |
    r mib''\p mib'' mib'' |
    r fa'' fa'' sol'' |
    r lab''! fa'' fa'' |
    r fa'' fa'' fa'' |
    r8 mib'' mib'' mib'' mib''2:8 |
    r8 fa'' fa'' fa'' fa''2:8 |
    r8 mib'' mib'' mib'' mib''2: |
    r8 re'' re'' re'' re''2: |
    r8 sol''\f sol'' sol'' sol''2: |
    r8 fa''\p fa'' fa'' fa''2: |
    r8 mib'' mib'' mib'' mib''2: |
    r8 re'' re'' re'' re''2: |
    r8 sol''\f sol'' sol'' sol''2: |
    r8 fa''\p fa'' fa'' fa''2: |
    r8 mib''\cresc mib'' mib'' mib''2: |
    mib''2:\ff mib'': |
    mib''8_\markup\italic calando mib'' mib'' mib'' mi''2: |
    fa'': mib''!: |
    <re'' fa'>2\fermata r2 |
  }
  \tag #'secondo {
    r4 re''( do''\rf sib') |
    sib' sib'2 sib'8 sol' |
    fa'8.( sol'16 \ficta la'4) do''2-\sug\f |
    \once\slurDashed sib'8.( do''16 re''4) r2 |
    r4 fa''4(-\sug\rf sol'' lab''!) |
    \grace lab''8 sol''2 fa''4-\sug\p mib'' |
    r re'' re'' do'' |
    \grace do''8 sib'2 r\fermata |
    r4 fa''\rf( sol'' lab''!) |
    \grace lab''8 sol''2 fa''4-\sug\p mib'' |
    r4 re'' re'' do'' |
    sib' sib\f la4.\trill sib16 la |
    sol4 re'4. \slurDashed re'8( mi' fad') |
    sol'( la' sib' la') sol'( fa'! mib'! re') \slurSolid |
    <>-\sug\p \ru#8 { sol'-. sib'-. } |
    lab' sib' lab' sib' lab' sib' lab' sib' |
    fa' lab' fa' lab' fa' lab' fa' lab' |
    fa' lab' fa' lab' fa' lab' fa' lab' |
    mib'4 sol' mib'2-\sug\cresc |
    mib'4. mib'8 sib'4. sib'8 |
    sib'4.-\sug\f sib'8 sib'4. sib'8 |
    r8 sib'-\sug\p sib' mib'' r mib''( re'') do'' |
    r8 sib' sib'( re'') re'' re'' sib' sol' |
    r sol' sol' sol' mib' mib' do' do' |
    re'16-\sug\f fa' fa' fa' fa'2.:16 |
    fa'2: fa': |
    sol'16 sib' sib' sib' sib'2.:16 |
    mib''2: mib'': |
    re''8 sib' re'' fa'' <re'' fa' sib>4 q |
    q2 r |
    r4 solb'-\sug\p solb' solb' |
    r \ficta solb' solb' solb' |
    r \ficta solb' solb' solb' |
    r sib' sib' sib' |
    r lab' lab' lab' |
    r \ficta solb' solb' fa' |
    r mib' mib' mib' |
    r sib' sib' sib' |
    r dob'' dob'' dob'' |
    r do''! do'' do'' |
    r fa' fa' fa' |
    r <sol' re''>-\sug\f q q |
    r do''-\sug\p do'' do'' |
    r re'' re'' mib'' |
    r fa'' re'' re'' |
    r re'' re'' re'' |
    r8 do'' do'' do'' do''2:8 |
    r8 do'' do'' do'' do''2: |
    r8 do'' do'' do'' do''2: |
    r8 si' si' si' si'2: |
    r8 do''-\sug\f do'' do'' do''2: |
    r8 re''-\sug\p re'' re'' re''2: |
    r8 do'' do'' do'' do''2: |
    r8 si' si' si' si'2: |
    r8 do''-\sug\f do'' do'' do''2: |
    r8 re''-\sug\p re'' re'' re''2: |
    r8 do''-\sug\cresc do'' do'' do''2: |
    reb''2:-\sug\ff reb''2: |
    do'':_\markup\italic\tiny calando do'': |
    do'': do'': |
    <sib' re'>2\fermata r |
  }
>> \allowPageTurn
<<
  \tag #'primo {
    <sol mib'>4.\f q8 <sib fa'>4. q8 |
    <sib sol'>2
  }
  \tag #'secondo {
    sib4.-\sug\f sib8 <re' lab>4. q8 |
    <sol mib'>2
  }
>> r16 mib' fa' sol' lab' sib' do'' re'' |
<mib'' sol'>4 r <sib' re'> r |
<<
  \tag #'primo {
    sol'4 mib' r sib'\p |
    do''8 re'' mib''2 mib''4 |
    sol''8( fa'') mib''2 re''8 do'' |
    sib'2. lab'4 |
    \ru#4 { sol'8-. sib'-. } |
    do''2 do''4. do''8 |
    lab''2.\f sol''8 fa'' |
    re''4-. mib''-. r mib''\p |
    do''4. do''8 sib'4. lab'8 |
    \ru#4 { sol'8-. sib'-. } |
    do''2 do''4. do''8 |
    lab''2.\f sol''8 fa'' |
    re''4-! mib''-! r mib''\p |
    do''4. do''8 \grace do''8 sib'4. re'8 |
    mib'4
  }
  \tag #'secondo {
    r4 sol-!\p sib( sol) |
    r do'-! mib'( do') |
    r4 \once\slurDashed sib'( lab' sol') |
    r re' mib' fa' |
    \ru#4 { mib'8-. sol'-. } |
    \ru#4 { mib'-. lab'-. } |
    <>-\sug\f \ru#4 { fa'-. lab'-. } |
    fa'-. lab'-. sol'-. sib'-. sol'-. sib'-. sol'-. sib'-. |
    lab'4.\p lab'8 sol'4. fa'8 |
    \ru#4 { mib'8-. sol'-. } |
    \ru#4 { mib'-. lab'-. } |
    <>-\sug\f \ru#4 { fa'-. lab'-. } |
    fa'-. lab'-. sol'-. sib'-. sol'-. sib'-. sol'-. sib'-. |
    lab'4.-\sug\p lab'8 sol'4. fa'8 |
    mib'4
  }
>>
%% 2/4
<<
  \tag #'primo {
    r8 sol'' |
    sol''16-!\fp mib''( re'' mib'') re''( mib'') lab''(\rf sol'') |
    sol''-!\fp re''( dod'' re'') dod''( re'') lab''( sol'') |
    fa''16-!-\sug\fp fa''( mi'' fa'') mi''-! \once\slurDashed fa''( mi'' fa'') |
    mib''!4
  }
  \tag #'secondo {
    r4 |
    sol8-\sug\fp sol' sol' sol' |
    sol8-\sug\fp sol' sol' sol' |
    re''\fp re' re' re' |
    sol4
  }
>> r8 do'-\sug\ff |
\grace do'16 si8 la16 sol \grace do'16 si8 la16 sol |
do'4 <<
  \tag #'primo {
    r8 do'' |
    lab''!2:16\fp |
    sol''16\fp mib''( re'' do'') si'( do'') si'( do'') |
    lab''2:16\f |
    sol''16-!\fp mib''( re'' do'') si'( do'') si'( do'') |
    \grace mib''8 re''16-! re''( mib'' re'') do''( re'') si'( re'') |
    do''8 mib'' r mib'' |
    r mib'' r mib'' |
    r mib'' r mib'' |
    r mib'' r mib'' |
    r mib'' r mib'' |
    r mi'' r mi'' |
    r fa'' r fa'' |
    r mib'' r mib'' |
    r re'' r re'' |
    r re'' r re'' |
    r re'' r re'' |
    r re'' r re'' |
    r re'' r re'' |
    r re'' r re'' |
    r re'' r re'' |
    r mib'' r fa'' |
    r sol'' r sol'' |
    r lab''\f r lab'' |
    r mib''\p r mib'' |
    r re'' r re'' |
    r mib'' r mib'' |
    do'' sol'' sol'' sol'' |
    lab''2\f\fermata |
    fa''4 r |
    r8 mib''\p mib'' mib'' |
    r re'' re'' re'' |
  }
  \tag #'secondo {
    r4 |
    do''2:16-\sug\fp |
    do''16-!-\sug\fp sol' sol' sol' sol'4:16 |
    do''2:-\sug\f |
    do''16-\sug\fp sol' sol' sol' sol'4: |
    lab'8 sol'16 fa' mib' fa' re' fa' |
    mib'8 do'' r do'' |
    r reb'' r reb'' |
    r do'' r do'' |
    r \ficta reb'' r reb'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r do'' |
    r do'' r re'' |
    r mib'' r mib'' |
    r fa''-\sug\f r fa'' |
    r do''-\sug\p r do'' |
    r si' r si' |
    r do'' r do'' |
    do'' do'' do'' do'' |
    \once\tieDashed do''2-\sug\f\fermata~ |
    do''4 r |
    r8 do''-\sug\p do'' do'' |
    r si' si' si' |
  }
>>
%% 2/2
<<
  \tag #'primo {
    do''8\fermata r16 mi''\f mi''8. mi''16 mi''4 r |
    fa''4\p r r2 |
    re''4 r re'' r |
    mib''4 r do'' r |
    r8 re'' re'' re'' r sol'' sol'' sol'' |
    r fa'' fa'' mib'' r re'' re'' dod'' |
    r dod'' re'' re'' r sol'' sol'' sol'' |
    r fa'' fa'' mib'' r re'' re'' do'' |
    sib'4 r r2 |
  }
  \tag #'secondo {
    do''8\fermata r16 sol'-\sug\f sol'8. sol'16 sol'4 r |
    do''4-\sug\p r r2 |
    fa'4 r fa' r |
    fa' r fa' r |
    r8 fa' fa' fa' r sib' sib' sib' |
    r re'' re'' do'' r sib' sib' la' |
    r \ficta la' sib' sib' r sib' sib' sib' |
    r8 re'' re'' do'' r sib' sib' \ficta la' |
    sib'4 r r2 |
  }
>> \allowPageTurn
<<
  \tag #'primo {
    <mib' sol>4.\f sol'16 fa' mib'8-! sol'-! sib'-! mib''-! |
    sol''-! mib''-! sib''-! sol''-! lab''-! fa''-! re''-! sib'-! |
    <>\p \ru#8 { sib'-. sol'-. } |
    mib'-. mib'-. sol'-. sib'-. mib''-.\mf sib'-. sol''( fa'') |
    mib''4 r sib' r |
    sib' r sib' r |
    lab' r sol' r |
    fa' r fa' r |
    mib' r r2 | \allowPageTurn
    sib'4 sib'8. sib'16 re''4 fa'' |
    \grace fa''8 mib''2 r4 mib''8( do'') |
    sib'( la') mib''( do'') sib'( la') mib''( do'') |
    \grace mib''8 reb''4 r r fa''8( reb'') |
    do''( sib') fa''( reb'') \grace mib''8 reb''4 do''8( sib') |
    la'8 do''4 fa''8 \grace mib''8 reb''4 do''8 sib' |
    la'4 <la' fa''>8.\f q16 q4 r4\fermata |
  }
  \tag #'secondo {
    sib8-\sug\f sib4 sib sib sib8 |
    sib sib4 sib sib sib8 |
    <>-\sug\p \ru#8 { sol'-. mib'-. } |
    mib'-. sol-. sib-. mib'-. sol'-.-\sug\mf mib'-. sib'( lab') |
    sol'4 r sol' r |
    fa' r fa' r |
    mib' r re' r |
    do' r re' r |
    \ru#4 { sol'8-. sib'-. } |
    \ru#4 { fa'-. sib'-. } |
    \ru#4 { mib'-. la'-. } |
    do'8 do'4 do' do' do'8 |
    reb'8 reb'4 reb' reb' reb'8 |
    \ficta reb' reb'4 reb' reb' reb'8 |
    do' do'4 do'8 reb' reb'4 reb'8 |
    do'4 <la fa'>8.-\sug\f q16 q4 r4\fermata |
  }
>>
<<
  \tag #'primo {
    re''8\p re''4 re'' re'' re''8 |
    re''8 re''4 re'' re'' re''8 |
    <>\ff <<
      { sol''8 sol''4 sol'' sol'' sol''8 | sol''2. } \\
      { mib''8 mib''4 mib'' mib'' mib''8 | mib''2. }
    >> mib''8(\p re'') |
    si'( do'') mib''( re'') mib''( do'') mib''( do'') |
    sib' re'' re'' re'' re'' re'' do'' do'' |
  }
  \tag #'secondo {
    sib'8-\sug\p sib'4 sib' sib' sib'8 |
    sib'8 sib'4 sib' sib' sib'8 |
    sib'-\sug\ff sib'4 sib' sib' sib'8 |
    do''2. fa'4\p |
    sol'4 sol' sol'8 mib' sol' mib' |
    re' sib' sib' sib' sib' sib' la' la' |
  }
>>
sib'8\f sib' re'' fa'' sib'' fa'' re'' fa'' |
sib'4 <<
  \tag #'primo {
    re''4 re'' re'' |
    r do'' do'' r |
    r do'' do'' r\fermata |
    re''8-\sug\p re''4 re'' re'' re''8 |
    re''8 re''4 re'' re'' re''8 |
    <>\ff <<
      { sol''8 sol''4 sol'' sol'' sol''8 | sol''2. } \\
      { mib''8 mib''4 mib'' mib'' mib''8 | mib''2. }
    >> mib''8(\p re'') |
    si'( do'') mib''( re'') si'( do'') mib''( re'') |
    si'16\f si' do'' do'' re'' re'' mib'' mib'' fa'' fa'' sol'' sol'' la'' la'' sib''! sib'' |
    \ficta la'' la'' sol'' sol'' fa'' fa'' sol'' sol'' fa'' fa'' mib'' mib'' re'' re'' do'' do'' |
    sib'\ff re'' re'' re'' re''4:16 re'': do'': |
  }
  \tag #'secondo {
    sib'4 sib' sib' |
    r sib' sib' r |
    r la' la' r\fermata |
    sib'8-\sug\p sib'4 sib' sib' sib'8 |
    sib'8 sib'4 sib' sib' sib'8 |
    sib'-\sug\ff sib'4 sib' sib' sib'8 |
    do''2. fa'4-\sug\p |
    sol'4 sol' sol' sol' |
    sol' r8 sol'16\f sol' la' la' sib' sib' do'' do'' re'' re'' |
    do'' do'' sib' sib' la' la' sib' sib' la' la' sol' sol' fa' fa' mib' mib' |
    re'-\sug\ff sib' sib' sib' sib'4:16 sib': la': |
  }
>>
sib'8 sib' re'' fa'' sib'' fa'' re'' fa'' |
sib' re'' fa'' re'' sib' fa' re' fa' |
sib4-! <<
  \tag #'primo {
    sib'4(\p re'' do'') |
    sib'-! sib'( re'' do'') |
    sib' r r2 |
    r4 sib'( re'' do'') |
    si'-! si'( re'' do'') |
    \ficta si'4 r r2 |
    r4 si'( re'' do'') |
    \ficta si' r r2 |
    si'8\p\cresc si'4 si' si' si'8 |
    do'' do''4 do'' do'' sol''8 |
    \grace sol''8 fad'' fad''4 fad'' fad''8 mib''! do'' |
    si'8-\sug\ff si'4 re'' sol'' sol''8 |
    \grace sol''8 fad'' fad''4 fad'' fad''8 mib''! do'' |
    si' sol' si' sol' re'' si' sol' re' |
    do''4:16\f re'': mib'': fa'': |
    sol'': fa'': sol'': lab'': |
    mib''2.: re''4: |
    mib'': fa'': sol'': lab'': |
    mib''2.: re''4: |
  }
  \tag #'secondo {
    re'4(-\sug\p fa' mib') |
    re'-! re'( fa' mib') |
    re' r r2 |
    r4 re'( fa' mib') |
    re'-! re'( fa' mib') |
    re' r r2 |
    r4 re'( fa' mib') |
    re' r r2 | \allowPageTurn
    <>-\sug\p -\sug\cresc \ru#4 { fa'8-. re'-. } |
    \ru#4 { mib'-. do'-. } |
    mib' do' mib' do' mib' do' fad' fad' |
    sol'-\sug\ff si' sol' si' sol' re' sol' re' |
    mib' do' mib' do' mib' do' fad' fad' |
    sol' re' sol' re' sol' re' do' \ficta si |
    sol'2.:16-\sug\f si'4: |
    do''2: do'': |
    do''2.: si'4: |
    do''2: do'': |
    do''2. si'4: |
  }
>>
do''8 do' mib' sol' do'' sol' mib' do' |
r mib' sol' do'' mib'' do'' sol' mib' |
re'4 <<
  \tag #'primo {
    sib'4 sib'\fermata r |
    r8 mib' sol' sib' mib''-. sib'-. sol''( fa'') |
    mib''4-.\p r sib'-. r |
    sib'4 r sib' r |
    lab' r sol' r |
    fa' r fa' r |
    mib' r r2 | \allowPageTurn
    mib''4 reb''8 do'' \grace do'' sib'4 lab'8 sol' |
    lab'4 r r mib'' |
    mib''2:8 mib'': |
    solb''2.\f fa''8 mib'' |
    re'' re'' re'' re'' \grace lab'' solb''4 fa''8 mib'' |
    re''4 <re'' sib''>8. q16 q4 r\fermata |
  }
  \tag #'secondo {
    fa'4 fa'\fermata r |
    r8 sol sib mib' sol'-. mib'-. \once\slurDashed sib'( lab') |
    sol'4-.-\sug\p r sol'-. r |
    fa' r fa' r |
    mib' r re' r |
    do' r re' r |
    mib'8-. mib'-. sol'-. mib'-. sol'-. mib'-. sol'-. mib'-. |
    sol' mib' sol' mib' sol' mib' mib' mib' |
    \ru#8 { lab' mib' } |
    la'-\sug\f mib' la' mib' la' mib' la' la' |
    sib' sib' sib' sib' la'2:8 |
    sib'4 <fa' re''>8. q16 q4 r\fermata |
  }
>>
<<
  \tag #'primo {
    sol''8\p sol''4 sol'' sol'' sol''8 |
    sol'' sol''4 sol'' sol'' sol''8 |
    lab''\ff lab''4 lab'' lab'' lab''8 |
    lab''2. sol''8\p mib'' |
    si'8( do'' re'' mib'') fa''( sol'' lab'' fa'') |
    mib''2.:8 fa''8 fa'' |
    mib''4 sol'( lab' sol') |
    r sib'( do'' sib') |
    r sib'( do'' reb'') |
    do''( sib' do'' re''!) |
    mib''8 sol''4 sol'' sol'' sol''8 |
    sol'' sol''4 sol'' sol'' sol''8 |
    lab''\ff lab''4 lab'' lab'' lab''8 |
    lab''2. sol''8\p mib'' |
    si'8-. do''-. re''-. mib''-. fa''-. sol''-. lab''-. fa''-. |
    mib''16\ff sol'' sol'' sol'' sol''4:16 sol'': fa'': |
    mib''8\p mib''4 mib'' mib'' mib''8 |
    solb''8 solb''4 solb'' \once\slurDashed solb''8( fa'' mib'') |
    re''16 re'' fa'' fa'' lab'' lab'' fa'' fa'' lab'' lab'' fa'' fa'' lab'' lab'' fa'' fa'' |
    mib'' sol'' sol'' sol'' sol''4:16 fa''2: |
    mib''8 mib''4 mib'' mib'' mib''8 |
    solb''8 solb''4 solb'' solb''8( fa'' mib'') |
    re''16 re'' fa'' fa'' lab'' lab'' fa'' fa'' lab'' lab'' fa'' fa'' lab'' lab'' fa'' fa'' |
    mib'' sol'' sol'' sol'' sol''4:16 sol'': fa'': |
  }
  \tag #'secondo {
    mib''8-\sug\p mib''4 mib'' mib'' mib''8 |
    mib'' mib''4 mib'' mib'' mib''8 |
    fa''-\sug\ff fa''4 fa'' fa'' fa''8 |
    fa''2. sib'4-\sug\p |
    mib'4 mib' do' do'8 lab' |
    sol'2.:8 lab'8 lab' |
    sol'4 sib( do' sib) |
    r sol'( lab' sol') |
    r sol'( lab' sib') |
    lab'( sol' lab' fa') |
    sol'8 mib''4 mib'' mib'' mib''8 |
    mib'' mib''4 mib'' mib'' mib''8 |
    fa''-\sug\ff fa''4 fa'' fa'' fa''8 |
    fa''2. sib'4-\sug\p |
    mib'4 mib' do' do'' |
    sib'16-\sug\ff mib'' mib'' mib'' mib''4:16 mib'': re'': |
    mib''8-\sug\p sol'4 sol' sol' sol'8 |
    solb' do''4 do'' do'' do''8 |
    sib'16 sib' re'' re'' fa'' fa'' re'' re'' do''2:16 |
    sib'16 mib'' mib'' mib'' mib''4: mib'': re'': |
    mib''8 sol'4 sol' sol' sol'8 |
    solb' do''4 do'' do'' do''8 |
    sib'16 sib' re'' re'' fa'' fa'' re'' re'' do''2:16 |
    sib'16 mib'' mib'' mib'' mib''4: mib'': re'': |
  }
>>
mib''8 sol'' sib'' sol'' mib'' sib' sol' sib' |
mib'' sol'' sib'' sol'' mib'' sib' sol' sib' |
mib'' sol'' mib'' sib' sol' mib'' sib' sol' |
mib'' sol'' mib'' sib' sol' mib'' sib' sol' |
mib'2 r |
%%
<<
  \tag #'primo {
    r4 do''-\sug\p r do'' |
    r si' r si' |
    r re'' r re'' |
    r mib'' r mib'' |
    r mib'' r mib'' |
    r8 mib'' mib'' mib'' mib''2:8 |
    r8 fad'' fad'' fad'' fad'' fad'' mib'' mib'' |
    r re'' re'' re'' re''2: |
    re''2.: do''!4: |
    si'8 sol' sol' sol' sol' la'16 si' do'' re'' mi'' fad'' |
    sol''2:16-\sug\f re'': |
    mib''2.: do''4: |
    \ficta si'8 sol' sol' sol' sol'2:8 |
  }
  \tag #'secondo {
    r4 sol'-\sug\p r sol' |
    r sol' r sol' |
    r sol' r sol' |
    r sol' r sol' |
    r sol' r sol' |
    r8 sol' sol' sol' sol'2:8 |
    r8 do'' do'' do'' do''2: |
    r8 do'' do'' do'' do'' do'' si' si' |
    la' la' fad' fad' sol' sol' la' la' |
    re' si si si si do'16 re' \ficta mi' \ficta fad' sol' \ficta la' |
    si'2:16-\sug\f si': |
    do''2.: mib'4: |
    re'8 sol' sol' sol' sol'2:8 |
  }
>>
la'2:8 si': |
do'': lab'!: |
fa': sol': |
do'2 r |
<<
  \tag #'primo {
    mib''1\p |
    mib''2 r |
    fa''1~ |
    fa''2 r |
    mib'' r |
    \grace sol''8 fa''2 r4 sib' |
    <sib' lab''>2\f( <sol'' sib'>4) r8 mib''\p |
    do''4 r8 do'' \grace do'' sib'4 r8 re'' |
    \grace re'' mib''2 r4 r8 mib'' |
    \grace re'' do''4 r8 do'' \grace do''8 sib'4 r8 re' |
  }
  \tag #'secondo {
    sib'1-\sug\p |
    do''2 r |
    do''1 |
    re''2 r |
    sib' r |
    sib' r |
    fa'2(-\sug\f mib'4) r8 sol'-\sug\p |
    lab'4 r8 lab' \grace lab'8 sol'4 r8 fa' |
    \grace fa'8 sol'2 r4 r8 sib' |
    mib'4 r8 lab' \grace lab'8 sol'4 r8 fa' |
  }
>>
mib'4(-. mib'-. mib'-. mib'-.) |
<<
  \tag #'primo {
    fa'4 sol'8( lab' sib'4 lab') |
    sol'4-.( sol'-. sol'-. sol'-.) |
    \grace sol'8 fa'4 fa'2 sol'8 fa' |
  }
  \tag #'secondo {
    mib'2( re') |
    mib'4-.( mib'-. mib'-. mib'-.) |
    mib'2 re' |
  }
>>
mib'4(-. mib'-. mib'-. mib'-.) |
<<
  \tag #'primo {
    fa'4 sol'8( lab' sib'4 lab') |
    sol'4-.( sol'-. sol'-. sol'-.) |
    fa'4 fa'2 sol'8 fa' |
    mib'8 mib''( sol'' mib'') sib''( sol'' mib'' sib') |
    sol'( sib' mib'' sib') mib''( sib' sol' sib') |
    mib' mib'( sol' mib') sib'( sol' mib' sib) |
    sol( sib mib' sib) mib'( sib sol sib) |
    <sol mib'>2 q |
    q q |
    q
  }
  \tag #'secondo {
    mib'2 re' |
    mib'4-.( mib'-. mib'-. mib'-.) |
    mib'2 re' |
    \slurDashed <sib sol'>4-.( q-. q-. q-.) |
    q-.( q-. q-. q) |
    q-.( q-. q-. q) |
    q-.( q-. q-. q) | \slurSolid
    <sib sol'>2 q |
    q q |
    q
  }
>>
