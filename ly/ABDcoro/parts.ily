\piecePartSpecs
#`((flauti #:score-template "score-deux")

   (oboi #:score-template "score-deux")

   (corni #:score-template "score-deux"
          #:tag-global ()
          #:instrument "Corni in F")

   (fagotti #:system-count 4
    #:music , #{

\quoteBassi "AABbassi"
\cue "AABbassi" {
  <>^\markup\tiny Bassi \mmRestInvisible s2.*5\mmRestVisible
  \restInvisible s2 \restVisible
} s4
s2.*20
\cue "AABbassi" {
  <>^\markup\tiny Bassi
  \mmRestInvisible\restInvisible s2. s4 \restVisible\mmRestVisible
}
s2
\cue "AABbassi" {
  \mmRestInvisible s2 s8 <>^\markup\tiny Bassi s8 s2. \mmRestVisible
}

                #})

   (violino1)
   (violino2)
   (viola)
   (bassi)

   (silence #:on-the-fly-markup , #{\markup\tacet #}))
