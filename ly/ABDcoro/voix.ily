<<
  \tag #'voix1 {
    \clef "soprano/treble" R2.*22 |
    do''4 do''8 do'' do'' do'' |
    do''8.([ fa''16]) fa''4. mi''8 |
    \grace mi''8 re''4. do''8 do'' re'' |
    \grace do''8 sib'4 sib' r |
    \grace la'8 sol'4 sol'4. la'16[ sib'] |
    la'8.[ sib'16] do''4 r |
    fa''8[ mi''] re'' do'' sib' la' |
    \grace la'8 sol'2 r4 |
    do''4 do''8 do'' re'' do'' |
    \grace do''8 sib'2 r4 |
    sib'4 sib'8 sib' do'' sib' |
    \grace sib'8 la'2 r4 |
    r r8 re'' mi'' fa'' |
    fa'' do'' r4 r |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R2.*22 |
    la'4 la'8 la' la' la' |
    la'8.([ re''16]) re''4. do''8 |
    \grace do''8 sib'4. la'8 la' sib' |
    \grace la'8 sol'4 sol' r |
    \grace fa'8 mi'4 mi'4. fa'16[ sol'] |
    fa'8.[ sol'16] la'4 r |
    re''8[ do''] sib' la' sol' fa' |
    \grace fa'8 mi'2 r4 |
    la'4 la'8 la' la' la' |
    \grace la'8 sol'2 r4 |
    sol'4 sol'8 sol' la' sol' |
    \grace sol'8 fa'2 r4 |
    r r8 sib' sib' do'' |
    do'' sol' r4 r |
  }
  \tag #'voix3 {
    \clef "alto/treble" R2.*22 |
    fa'4 fa'8 fa' fa' fa' |
    fa'4 fa' r8 fa' |
    fa'4. fa'8 fa' fa' |
    re'4 re' r |
    do'4 do' do' |
    fa'4 fa' r |
    r4 r r8 fa' |
    do'2 r4 |
    re'4 re'8 re' re' re' |
    re'2 r4 |
    do' do'8 do' do' do' |
    do'2 r4 |
    r4 r8 fa' sol' la' |
    la' \sug mi' r4 r |
  }
>>
