\clef "alto" fa4\f fa fa |
fa fa fa |
fa fa fa8 fa' |
re'4 re' r |
do' do do |
fa4 r8 do'' sib' sol' |
la-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la2.:8 |
sib:8 |
do'2:8 do8 do |
fa4 r la'8(\p sol')~ |
sol' fa'4 mi' re'8~ |
re' do'4 do'8 do' la-\sug\f |
sib4 do' do |
fa fa' la'8-\sug\p sol'~ |
sol' fa'4 mi' re'8~ |
re' do'4 do'8 do'4 |
sib-\sug\f do' do |
fa r r |
fa\p fa fa |
fa fa-\sug\rf fa |
fa fa fa8 fa' |
re'4 re' r |
do'2. |
fa'4 fa'8 do' la fa |
R2.*2 |
re'2.-\sug\p~ |
re'4 r r |
do'2. |
do'4. fa16\f sol la8 fa |
fa4 r r |
r r sol' |
