\clef "bass" fa4-\sug\f fa fa |
fa fa fa |
fa fa fa |
sol sol8 fa mi re |
do4 do do |
fa4 fa, r8 sib |
la-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la2.:8 |
sib:8 |
do'8 do' do' do' do do |
fa4 fa, r |
R2. |
r4 r r8 la-\sug\f |
sib4 do' do |
fa fa, r |
R2. |
r4 r r8 la-\f |
sib4 do' do |
fa\p fa, r |
fa\p fa fa |
fa fa-\sug\rf fa |
fa fa fa |
sol4 sol8 fa mi re |
do4 do do |
fa fa8 do la, fa, |
r4 r r8 fa,\f |
do4 do, do |
re2\p fad4 |
sol sol8.\f fa!16 fa8.\trill mi32 fa |
mi2\p mi4 |
fa4. fa16\f sol la8-. fa-. |
sib,4 r r |
r r do |
