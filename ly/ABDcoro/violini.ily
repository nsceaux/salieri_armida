\clef "treble"
<<
  \tag #'primo {
    do''\f do''8-.( do''-. do''-. do''-.) |
    do''8.( fa''16) fa''4.\rf mi''8 |
    \grace mi''8 re''4. do''8-.( do''-. re''-.) |
    \grace do''8 sib'4 sib' r |
    sol'4 sol'4. la'16 sib' |
    la'8.( sib'16) do''8-! do''8( re'' mi'') |
    fa''16( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' mi'' sol'') fa''( sol'' mi'' sol'') fa''( la'' sol'' sib'') |
    la''( sib'' sol'' sib'') la''( sib'' sol'' sib'') la''( sol'' fa'' mi'') |
    fa''( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
    re''8( mi''16 fa'') sol''( la'' sib'' do''') re'''( sib'' la'' sol'') |
    fa''4~ fa''16 mi'' fa'' sol'' la'' sib'' la'' sol'' |
    fa''4 r8 do'''(\p re''' si'') |
    do'''( la'') sib''( sol'') la''( fa'') |
    \grace fa'' mi''4. fa''16( sol'') fa''8. do'''16\f |
    \grace do'''16 sib''8 la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4. \grace re'''16 do'''32\p \ficta si'' do'''16 re'''8 \grace do'''16 si''!32 la'' si''16 |
    do'''8 \grace sib''16 la''32 sol'' la''16 sib''8 \grace la''16 sol''32 fa'' sol''16 la''8 fa'' |
    \grace fa''16 mi''4.( fa''16 sol'') fa''8. do'''16\f |
    re''' sib'' la'' sol'' fa''4. \grace la''16 sol''8 |
    fa''4 fa' r |
    do''4\p do''8 do'' do'' do'' |
    do''8.( fa''16) fa''4.\rf mi''8 |
    \grace mi''8 re''4. do''8( do'' re'') |
    \grace do''8 sib'4 sib' r |
    \grace la'8 sol'4 sol'4. la'16 sib' |
    la'8. sib'16 do''4 r |
    fa''8\trill\f( mi'' re'' do'' sib' la') |
    \grace la' sol'2 r4 |
    do''4\p do''8-! do''( re'' do'') |
    \grace do''8 sib'2 r4 |
    sib'4\p sib'8-! sib'( do'' sib') |
    la'4. fa'16\f sol' la'8 fa' |
    re'4 r r |
    r r mi'' |
  }
  \tag #'secondo {
    la'4-\sug\f \once\slurDashed la'8-.( la'-. la'-. la'-.) |
    la'8.( re''16) <<re'4 re''4.-\sug\rf>> do''8 |
    \grace do''8 sib'4. la'8-.( la'-. sib'-.) |
    \grace la'8 sol'4 sol' r |
    \grace fa'8 mi'4 mi'4. fa'16 sol' |
    \once\slurDashed fa'8.( sol'16) la'8-! la'( sib' do'') |
    \ru#15 { do'' \grace re''16 do''32 si' do''16 } |
    \ru#3 { re''8 \grace mi''16 re''32 dod'' re''16 } |
    do''16 sib' la'8~ la'16 sol' la' sib' do'' re'' do'' sib' |
    la'4 r8 do''-\sug\p( re'' si') |
    do''( la') sib'( sol') la'( fa') |
    \grace fa'8 mi'4. \once\slurDashed fa'16( sol') fa'8. do''16-\sug\f |
    \grace mi''16 re''8 do''16 sib' la'4. do''8 |
    \grace mi''8 fa''4 r8 \grace re''16 do''32\p si' do''16 re''8 \grace do''16 si'32 la' si'16 |
    do''8 \grace sib'16 la'32 sol' la'16 sib'8 \grace la'16 sol'32 fa' sol'16 la'8 fa' |
    \grace fa'16 \once\slurDashed mi'4.( fa'16 sol') fa'8. fa''16\f |
    fa'' re'' do'' sib' la'4. sib'8 |
    la'4 fa' r |
    la'4-\sug\p la'8 la' la' la' |
    la'8.( re''16) << re'4 re''4.-\sug\rf>> do''8 |
    \grace do''8 sib'4. la'8( la' sib') |
    \grace la'8 sol'4 sol' r |
    \grace fa'8 mi'4 mi'4. fa'16 sol' |
    fa'8. sol'16 la'4 r |
    re''8\prall-\sug\f( do'' sib' la' sol' fa') |
    \grace fa' mi'2 r4 |
    la'4-\sug\p la'8-! la'-.( la'-. la'-.) |
    \grace la'8 sol'4 sol'8.\f fa'16 fa'8.\trill mi'32 fa' |
    sol'4-\sug\p sol'8-! sol'( la' sol') |
    fa'4. fa'16-\sug\f sol' la'8 fa' |
    re'4 r r |
    r r sib' |
  }
>>
