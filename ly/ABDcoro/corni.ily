\clef "treble" \transposition fa
R2.*6 |
\twoVoices #'(primo secondo tutti) <<
  { do''8 sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do'' sol' do'' sol' do'' re'' |
    mi'' re'' mi'' re'' mi'' re'' |
    do''2.:8 |
    do''4 }
  { mi'8 sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi' sol' mi' sol' mi' sol' |
    do'' sol' do'' sol' do'' sol' |
    mi'8 do'' do''2:8 |
    do''4 }
>> r4 r |
\twoVoices #'(primo secondo tutti) <<
  { mi''4 mi'' mi''8. re''16 | do''4 }
  { do''4 do'' do''8. sol'16 | mi'4 }
>> r4 r |
R2.*2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 }
  { do''4. sol'8 | mi'4 }
>> r4 r |
R2.*2 |
r4 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { mi''4. re''8 | do''4 }
  { do''4. sol'8 | mi'4 }
>> r4 r |
R2. |
r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { do''2~ |
    do''2. |
    re''4 re''2 | }
  { do'2~ |
    do'2. |
    re''4 re''2 | }
>>
R2. |
r4 \tag #'tutti <>^"a 2." do''8 sol' mi' do' |
R2.*8 |
