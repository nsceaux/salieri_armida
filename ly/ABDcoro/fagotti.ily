\clef "bass" R2.*6 |
la8-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la-. sib-. la-. sib-. la-. mi-. |
fa-. mi-. fa-. mi-. fa-. sol-. |
la2.:8 |
sib:8 |
do'2.:8 |
fa4 fa, fa'(\p |
mi' re' do') |
sib2 la4 |
sib-\sug\f do' do |
fa r fa'(\p |
mi' re' do') |
sib2 la4 |
sib4\f do' do |
fa fa, r |
R2. |
\clef "tenor" r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { fa'4. mi'8 | re'4. do'8 }
  { re'4. do'8 | sib4. la8 }
>> r4 |
r4 \clef "bass" \twoVoices #'(primo secondo tutti) <<
  { sib2 | }
  { sol | }
>>
R2. |
r4 fa8 do la, fa, |
R2.*2 |
re2 fad4 |
sol4 sol8.\f fa!16 \grace sol16 fa8. mi32 fa |
mi2 mi4 |
fa4. fa16\f sol la8 fa |
sib,4 r r |
R2. |
