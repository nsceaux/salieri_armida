\clef "treble" R2.*6 |
\tag #'tutti <>^"a 2." do''2.~ |
do''~ |
do''~ |
do'' |
fa''16( sol'' la'' sib'') do'''( sib'' la'' sol'') fa''( mi'' re'' do'') |
re''8( mi''16 fa'') sol''( la'' sib'' do''') re'''( sib'' la'' sol'') |
fa''4~ fa''16 mi'' fa'' sol'' la'' sib'' la'' sol'' |
fa''4 r r |
R2. |
r4 r r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    sib''8 la''16 sol'' fa''4. sol''8 |
    \grace sol''8 la''4 }
  { do''8 |
    re''8 do''16 sib' la'4. do''8 |
    fa''4 }
>> r4 r |
R2. |
r4 r r8 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { do'''8 |
    re'''16 sib'' la'' sol'' fa''4. \grace la''16 sol''8 |
    fa''4 fa' }
  { fa''8 |
    fa''16 re'' do'' sib' la'4. sib'8 |
    la'4 fa' }
>> r4 |
R2. |
r4 <>\rf \twoVoices #'(primo secondo tutti) <<
  { fa''4. mi''8 | \grace mi''8 re''4. do''8 s4 | s4 sol''2 | }
  { re''4. do''8 | \grace do''8 sib'4. la'8 s4 | s4 sib'2 | }
  { s2 | s2 r4 | r4 s2 | }
>>
R2.*10 |
