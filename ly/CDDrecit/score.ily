\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Ri. s2. s1*11 s2.
        \set Staff.shortInstrumentName = \markup\character Ub. s4 s1*6
        \set Staff.shortInstrumentName = \markup\character Ri. s1*3 s2.
        \set Staff.shortInstrumentName = \markup\character Ub. }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s2. s1*3\break s1*3\pageBreak
        s1*3\break \grace s8 s1*3\pageBreak
        s1*2\break s1*2\break s1*2\pageBreak
        s1*2\break s1*2\break \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
