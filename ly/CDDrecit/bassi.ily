\clef "bass" r8 fa\p mib4\fermata r8 mib |
re4 r r2 |
r red |
R1*3 |
r2 mi |
R1 |
r2 sold |
R1 |
la4 r sol!2\f~ |
sol\p r |
r fad |
R1*2 |
sol4 r fa! r |
R1 |
mi2 r |
do r4 re |
fa!2 r |
R1 |
mi4 r sold r |
r2 la~ |
la re |
r r4 mi |
la2 r |
