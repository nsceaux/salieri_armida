Mi -- se -- ra! è già vi -- ci -- na
qual la ren -- de il do -- lor! Pal -- li -- da, smor -- ta,
af -- fan -- na -- ta, tre -- man -- te… ah ve -- di a -- mi -- co,
co -- me cor -- re in -- fe -- li -- ce; e l’a -- spro ge -- lo,
e le sco -- sce -- se ru -- pi
sem -- bra -- no un va -- no in -- ciam -- po
per le te -- ne -- re pian -- te a pas -- si suoi…
ah non sde -- gnar -- ti. Io par -- ti -- rò se vuo -- i.

Non è più tem -- po, e -- ra pru -- den -- za al -- lo -- ra,
de -- bo -- lez -- za or sa -- ri -- a. Ti ser -- ba il Cie -- lo
a dar di tua vir -- tù le pro -- ve e -- stre -- me,
cer -- ca il fol -- le il pe -- ri -- glio, il vil lo te -- me.

Deh non la -- sciar -- mi a -- mi -- co in que -- sto sta -- to
più bi -- so -- gno ho di te, lo ve -- di, oh Di -- o!
che mo -- men -- to fu -- ne -- sto!

Oh Ciel, l’as -- si -- sti; il tuo tri -- on -- fo è ques -- to.
