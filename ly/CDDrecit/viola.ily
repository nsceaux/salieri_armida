\clef "alto" r8 fa'-!\p mib'4\fermata r8 mib'-! |
re'4 r r2 |
r fad' |
R1*3 |
r2 mi' |
R1 |
r2 sold' |
R1 |
r2 sol!2-\sug\f~ |
sol\p r |
r re' |
R1*13 |
