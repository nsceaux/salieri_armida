\clef "treble" r16 fa'\p \grace sol' fa'32[ mi' fa'16] mib'4\fermata r16 mib' \grace fa'16 mib'32[ re' mib'16] |
re'4 r r2 |
r2 <<
  \tag #'primo { do''2 }
  \tag #'secondo { la' }
>>
R1*3 |
r2 <<
  \tag #'primo { si'2 | }
  \tag #'secondo { \ficta sold' | }
>>
R1 |
r2 <<
  \tag #'primo { mi''2 }
  \tag #'secondo { si' | }
>>
R1 |
r2 sol!2\f~ |
sol2 r |
r2 <<
  \tag #'primo { re''2 }
  \tag #'secondo { la'2 | }
>>
R1*13 |
