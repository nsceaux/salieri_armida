\ffclef "soprano/treble" <>^\markup\character Rinaldo
r4 r2 |
re''16 re'' re''8 r16 la' la' la' fad'8 fad' r fad'16 sol' |
la'4 la'8 si' do''4 r |
do''16 la' la'8 r4 fad'8 fad' r fad'16 sol' |
la'8 la' r do'' si'! si' r4 |
red'' mi''8 fad'' fad'' si' r si'16 si' |
si'4 la'8 si' sold' sold' r16 sold' si' la' |
si'8 si' r si' si' si' si' dod'' |
re'' re'' re'' re''16 re'' mi''8 fad'' mi'' mi'' |
r mi''16 mi'' mi''8 si'16 si' \ficta sold'8 sold' r16 re'' re'' mi'' |
\grace re''8 dod''4 r r2 |
mi''4 mi''8 mi'' dod'' dod'' la' la'16 si' |
dod''4 r8 re'' re'' la'
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r16 la fad la |
la8 re r4 \ficta fad8 fad16 fad fad8 sol |
la la r la16 si do'4 la8 si |
sol sol r16 sol sol la si8 si r si |
si si si re' si4 r16 si si do' |
do'8 sol r sol16 sol do'4 la8 la |
fad fad r16 fad fad sol sol8 re r4 |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
re''8 re''16 re'' re''8 re'' si' si' r16 sol' sol' la' |
si'8 si' r si'16 do'' re''4 re''8 mi'' |
do''4 r16 do'' re'' mi'' mi''8 si' r si'16 do'' |
re''4 si'8 do'' la' la'
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
do'4 |
\grace si8 la4. sol8 fa fa r16 re' re' si |
sold8 sold r la la mi r4 |
R1 |

