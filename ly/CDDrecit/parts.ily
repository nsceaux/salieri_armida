\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Rinaldo
    \livretVerse#12 { Misera! E’ già vicina. }
    \livretVerse#12 { Qual la rende il dolor! Pallida, smorta, }
    \livretVerse#12 { Affannata, tremante… Ah vedi amico, }
    \livretVerse#12 { Come corre infelice; e l’aspro gelo, }
    \livretVerse#12 { E le scoscese rupi }
    \livretVerse#12 { Sembrano un vano inciampo }
    \livretVerse#12 { Per le tenere piante a passi suoi… }
    \livretVerse#12 { Ah non sdegnarti. Io partirò se vuoi. }
  }
  \null
  \column {
    \livretPers Ubaldo
    \livretVerse#12 { Non è più tempo. Era prudenza allora, }
    \livretVerse#12 { Debolezza or saria. Ti serba il Cielo. }
    \livretVerse#12 { A dar di tua virtù le prove estreme, }
    \livretVerse#12 { Cerca il folle il periglio, il vil lo teme. }
    \livretPers Rinaldo
    \livretVerse#12 { Deh non lasciarmi amico. In questo stato }
    \livretVerse#12 { Più bisogno ho’ di te. Lo vedi, oh Dio! }
    \livretVerse#12 { Che momento funesto! }
    \livretPers Ubaldo
    \livretVerse#12 { (Oh Ciel, l’assisti; il tuo trionfo è questo.) }
  }
  \null
} #}))
    