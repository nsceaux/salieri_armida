\ffclef "tenor/G_8" <>^\markup\character Ubaldo
si16 fad fad8 r sold mi mi r4 |
\ffclef "alto/treble" <>^\markup\character Rinaldo
r2 r4 r8 sol' |
do''4 r8 sol' mi' mi' r4 |
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r2 fa4 r8 fa |
la la r la fa fa r fa16 sol |
la8 la r fa sib sib r16 sib re' fa' |
fa'8 \ficta sib r sib sib sib sib do' |
lab8 lab r lab16 lab lab4 sol8 lab |
fa8 fa r16 fa fa sol lab8 lab lab sib16 do' |
lab8 lab lab16 lab32 lab \ficta sib16 fa sol8 sol r16 sib! mib' sib |
sol4 r8 sol sol8 sol sol lab |
sib sib r sib16 do' reb'4 mib'8 sib |
do' do' r4 do'4. do'16 do' |
do'8 sol r sol sol sol sol sol |
mi! mi r sol sib sib do' sol |
lab lab r16 do' do' fa' fa'8 do' r do'16 do' |
lab4 sib8 do' fa4 r16 do' do' do' |
la!8 la r la la la sib do' |
do' fa r fa do' do' do' reb' |
sib4 r reb'8 sib r sib16 sib |
dob'8 dob' r16 dob' dob' mib' dob'8 dob' r dob' |
\ficta dob' dob' dob' dob' lab lab r fa16 solb |
lab4 lab8 sib dob' dob' r dob' |
reb' reb' r solb la! la r4 |
r2 la8 fad re' la16 la |
fad8 fad r fad16 fad fad4 mi8 fad |
re8 re re' re'16 mi' do'8 do' r16 do' do' si |
do'8 do' r do' do' do' re' mi' |
mi' la r la16 si do'4 do'8 re' si4 r |
si8 sol r mi' dod' dod' r la16 si |
dod'4 dod'8 re' re' la r4 |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r4 si'8 si'16 si' sold'4 r |
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
si4 si8 mi' mi' si r si |
do'! do' r do'16 do' do'4 do'8 do' |
si si r4 si si8 si |
si8 fad r fad16 sol la4 la8 si |
sol4 mi'8 si16 si sold8 sold r sold16 la |
si8 si r si16 do' la4 r16 do' do' do' |
do'8 sol r sol16 sol sol4 sol8 sol |
mi8 mi r sol16 sol sol4 sol8 la |
sib sib r16 sib sib do' la8 la r fa |
si!8 si r do' do' sol r4 |
\ffclef "soprano/treble" <>^\markup\character Rinaldo
la'8 la'16 la' re''8 la' sib' sib'
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r8 sib |
sol sol r4 sib sib8 do' |
re' re' r re'16 fa' fa'4 lab8 sib |
sol sol
\ffclef "soprano/treble" <>^\markup\character Rinaldo
r16 sib' sib' sib' sol'8 sol' r mib'' |
mib'' sib'16 sib' sol'8 sol' sib' sib'16 sib' sib'8 do'' |
reb''8 reb''
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r16 sib sib sib sol4 r8 sib |
\ficta sib sib mib' sib do' do' r do' |
lab lab lab lab sol sol r sol16 lab |
sib4 sib8 do' reb' reb' r16 reb' do' reb' |
sib8 sib r sol16 lab sib4 sib8 sib |
lab lab r16 do' do' do' fa'8 do' r do'16 do' |
do'4 sib8 do' la!4 r16 la la la |
fad8 fad r16 la la sib do'8 do' r16 do' re' la |
sib8 sib r re' re' sol re' re'16 mib' |
do'4 do'8 do'16 re' mib'4 r8 mib' |
do' do' do'16 do'32 do' do'16 do' reb'4 reb'8 reb'16 reb' |
sib8 sib r sib16 do' reb'4 sib8 do' |
lab4 r16 mib lab sol lab8 lab r lab16 sib do'4 sib8 do' |
\ficta lab lab r16 lab lab sib solb8 solb r solb16 solb |
\ficta solb4 fa8 solb mib4 r16 mib mib fa |
solb8 solb r solb16 solb do'4 reb'8 mib' |
mib' solb r mib16 fa solb4 solb8 lab |
fa8 fa r16 fa fa solb lab8 lab r lab16 lab |
reb'4 reb'8 mib' dob'4 r16 dob' sib dob' |
lab8 lab r lab16 lab lab4 lab8 sib |
dob'8 dob' r16 dob' dob' sibb fad8 fad r4 |
fad'16 dod' dod' r \sug r dod'16 dod' dod' la8 la r \ficta dod8 |
fad fad r fad16 sold la8 la la la16 si |
dod'4 dod'8 dod'16 red' sid8 sid
\ffclef "soprano/treble" <>^\markup\character Rinaldo
mi''4 |
dod''8 dod'' r dod'' dod'' dod'' dod'' re'' |
si' si' r si'16 dod'' re''4 re''8 dod'' |
la'4 r16 la' la' si' dod''8 dod'' r16 dod'' si' dod'' |
la'8 la' r la' dod'' dod'' dod'' re'' |
mi'' mi'' r mi'' mi'' \ficta dod'' dod'' si' |
lad' lad' r16 fad' fad' sol' sol'8 sol' r sol'16 sol' |
dod''4 re''8 mi'' sol'2 |
sol'4 fad'8 sol' mi' mi' r4 |
dod''8 dod''16 re'' mi''8 re'' \grace dod''8 si'4 si'8 si'16 si' |
do''!8 do'' r do''16 do'' la'8 la' r la'16 la' |
fad'4 r la'8 la' r la'16 si' |
do''!8 do'' r do''16 mi'' do''4 do''8 re'' |
si' si' r sol' si' si' r si' |
re'' re'' r4 re''8 si' si' la'16 si' |
sol'4 r16 re'' re'' re'' si'8 si' r si'16 do'' |
re''4 re''8 mi'' do'' do'' r16 do'' do'' mi'' |
\grace re''8 do''4 do''8 do''16 re'' si'8 si'
\ffclef "tenor/G_8" <>^\markup\character Ubaldo
r8 re' |
re' la r4 r2 |

