\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \withLyrics <<
      { \set Staff.shortInstrumentName = \markup\character Ub. s1
        \set Staff.shortInstrumentName = \markup\character Ri. s1*2
        \set Staff.shortInstrumentName = \markup\character Ub. s1*25 s1. s1*2
        \set Staff.shortInstrumentName = \markup\character Ri. s1
        \set Staff.shortInstrumentName = \markup\character Ub. s1*10
        \set Staff.shortInstrumentName = \markup\character Ri. s2.
        \set Staff.shortInstrumentName = \markup\character Ub. s4 s1*2 s4
        \set Staff.shortInstrumentName = \markup\character Ri. s2. s1 s4
        \set Staff.shortInstrumentName = \markup\character Ub. s2. s1*11 s1. s1*10 s2.
        \set Staff.shortInstrumentName = \markup\character Ri. s4 s1*16 s2.
        \set Staff.shortInstrumentName = \markup\character Ub. s4 s1
      }
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*3\break s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \break
        s2 s1*2\break s1*2\break s1*3\pageBreak
        s1*2 s2 \bar "" \break s2 s1. s2 \bar "" \break s2 s1*2\break
        s1*3\break s1*2 s2 \bar "" \pageBreak
        % 5
        s2 s1*2\break s1*2 s2 \bar "" \break s2 s1*2\break s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1*2 s2 \bar "" \break
        s2 s1 s2. \bar "" \break s4 s1 s2 \bar "" \pageBreak
        s2 s1. s2 \bar "" \break s2 s1*2\break s1*2\break s1*2\pageBreak
        s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2 s2 \bar "" \pageBreak
        % 10
        s2 s1*2 \break s1*3\pageBreak
        s1*3\break s1*2 s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
