\clef "bass" r2 mi4\f r |
mi8.\f re16 re8.\trill do32 re do4 r |
R1 |
do8.\p sib,16 sib,8.\trill do32 sib, la,4 r |
R1 |
mib4\p r re r |
R1*3 |
r2 mib4 r |
R1*2 |
lab8-. lab( sol fa) mi!2 |
R1*2 |
fa4 r r2 |
R1 |
mib2 r |
R1 |
reb2 r |
R1*3 |
r2 fad4 r |
fad8. mi16 mi8. fad32 mi re2 |
R1*3 | \allowPageTurn
fad2 r sol |
R1 |
r2 r4 la |
sold2 r |
R1 |
la2 r |
red r |
R1 |
mi4 r re r |
r2 do4 r |
mi r r2 |
R1 |
r2 fa |
r r4 sol |
fad2 sol |
R1 |
fa2 r |
mib r |
R1*2 |
r2 lab4 r |
r2 mi!4 r |
R1*2 |
fa4 r r2 |
r mib |
re r |
sol2 r4 fa! |
mib r r2 | \allowPageTurn
R1*2 |
lab2 r r |
R1*4 |
reb2 r |
R1*2 |
r2 fad:16\p |
\ficta fad1: |
\ficta fad: |
\ficta fad2:\f fad-\sug\fp |
mi r |
mi r |
dod r |
R1*2 |
mi4 r mi\f r |
R1*2 |
mi2\p re~ |
re r |
R1*2 |
sol2 r |
R1 |
r2 fa! |
r mi4 r |
fad r sol r |
r2 r4 la\f |
