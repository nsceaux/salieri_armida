\clef "treble" r2 mi'4\f r |
mi'8.\f re'16 re'8.\trill do'32 re' do'4 r |
R1 |
do'8.\p sib16 sib8.\trill do'32 sib la4 r |
R1 |
<<
  \tag #'primo { la'4\p r sib' r | }
  \tag #'secondo { fa'4-\sug\p r fa' r | }
>>
\ru#22 { R1 \allowPageTurn }
R1.\allowPageTurn
\ru#30 { R1 \allowPageTurn }
R1. \allowPageTurn
\ru#7 { R1 \allowPageTurn }
r2 <>\p <<
  \tag #'primo {
    dod''2:16 |
    \ficta dod''1: |
    \ficta dod'': |
    \ficta dod''2:\f sid'\fp( |
    dod'') r |
    si' r |
    la' r |
  }
  \tag #'secondo {
    la'2:16 |
    la'1: |
    la': |
    la'2:-\sug\f sold'-\sug\fp~ |
    sold' r |
    \ficta sold' r |
    mi' r |
  }
>> 
R1*2 |
<<
  \tag #'primo { dod''4 r sol'!\f r | }
  \tag #'secondo { lad'4 r dod'-\sug\f r | }
>>
R1*2 |
<>\p <<
  \tag #'primo { lad'2 si' | do''! }
  \tag #'secondo { \ficta fad'1 | \ficta fad'2 }
>> r2 |
R1*2 |
<<
  \tag #'primo { si'2 }
  \tag #'secondo { sol' }
>> r2 |
R1 |
r2 <<
  \tag #'primo {
    si'2 |
    r do''4 r |
    do'' r si' r |
  }
  \tag #'secondo {
    sol'2 |
    r sol'4 r |
    la' r sol' r |
  }
>>
r2 r4 << mi'' \\ la'-\sug\f >> |
