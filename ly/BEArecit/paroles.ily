Fer -- ma -- ti, in -- cau -- to.

Oh Ciel! che mi -- ro?

Ah Ri -- nal -- do, Ri -- nal -- do,
do -- ve fug -- gi, che fa -- i? co -- sì del Cie -- lo,
che suo Cam -- pion t’e -- les -- se
a di -- fen -- der la fe -- de, a strug -- ger gli em -- pi
gli al -- ti di -- se -- gni, e i va -- ti -- ci -- ni a -- dem -- pi?
Co -- sì da te Ge -- ru -- sa -- le -- me as -- pet -- ta
li -- ber -- ta -- de, e ven -- det -- ta? Ah chi ti ren -- de
co -- sì da te di -- ver -- so,
sì vi -- le a -- gli oc -- chi mie -- i? Bel -- tà fal -- la -- ce,
che ti gui -- da a pe -- rir; che ti pre -- pa -- ra
un lac -- cio ad o -- gni pas -- so,
un an -- gue in o -- gni fior. Fol -- le! e non ve -- di,
che quan -- to in le -- i di lu -- sin -- ghier t’ap -- par -- ve
son d’A -- ver -- no, a se -- dur -- ti, in -- gan -- ni, e lar -- ve?
Tor -- na, tor -- na in te stes -- so
scon -- si -- glia -- to che se -- i. Co -- gli il mo -- men -- to,
ché da te fug -- ge in -- ti -- mo -- ri -- ta al lam -- po
di quest’ ar -- me fa -- tal. Fug -- gi, de -- lu -- di
le lu -- sin -- ghe fal -- la -- ci.

(Mi -- se -- ro me!)

Tu non mi guar -- di, e ta -- ci?
tu ar -- ros -- si -- sci nel vol -- to! Ah quel ros -- so -- re
è il co -- lor di vir -- tù. Tor -- na, Ri -- nal -- do,
al -- la glo -- ria, all’ o -- nor. T’a -- spet -- ta il cam -- po,
ti ri -- chia -- ma Gof -- fre -- do,
per sen -- tier di pro -- di -- gi il Ciel ti gui -- da:
l’in -- du -- gio è mor -- te.

E ho da la -- scia -- re Ar -- mi -- da?

In -- gra -- to! ah, nel tuo co -- re
chi bi -- lan -- cia il tuo Di -- o?

Ma le pro -- mes -- se,
la fe -- de, a -- mi -- co, i giu -- ra -- men -- ti mie -- i?

Li rom -- pi al Ciel per con -- ser -- var -- li a le -- i?
Sai pur con quan -- te fro -- di,
con quant’ ar -- ti co -- ste -- i de’ no -- stri Du -- ci
i più pro -- di se -- dus -- se, e in rea ca -- te -- na
li ser -- ba -- va cru -- del a du -- ro fa -- to,
se tu non e -- ri; e tu la pian -- gi? In -- gra -- to!
Tre -- ma per te. Tem -- po ver -- rà che Ar -- mi -- da,
sa -- zia del tuo pia -- cer, l’o -- dio fu -- ne -- sto
coll’ a -- mor can -- ge -- rà. Che te la -- scian -- do
su quest’ i -- so -- la i -- gnu -- da, o in un’ o -- scu -- ra
tor -- men -- to -- sa pri -- gion, ri -- ma -- sto in pre -- da
a’ tuoi fie -- ri ri -- mor -- si,
all’ or -- ror di te stes -- so, a li -- be -- rar -- ti
dal -- lo stra -- zio cru -- del, l’a -- mi -- ca ma -- no,
ch’or sal -- var -- ti po -- tri -- a, ri -- chia -- mi in va -- no.
Mi -- se -- ro! in ta -- le sta -- to
op -- pres -- so, di -- spe -- ra -- to,
sen -- za pie -- tà, sen -- za soc -- cor -- so…

Ah ta -- ci!
non i -- na -- sprir la pia -- ga,
che mi la -- ce -- ra il cor. Se tu ve -- des -- si
qua den -- tro, a -- mi -- co, e qua -- le a -- cer -- ba guer -- ra
vi fan gli op -- po -- sti af -- fet -- ti, e qual mi scuo -- te
di mi -- se -- ria, e d’or -- ror sce -- na fu -- ne -- sta.
Io ti fa -- rei pie -- tà; più non di -- stin -- guo
chi mi par -- la, o -- ve son, tre -- mo, e con -- fon -- do
col pe -- ri -- glio lo scam -- po, a -- mi -- co, oh Di -- o!
gui -- da, sal -- va -- mi tu. Fug -- giam da que -- sto
in -- si -- dio -- so re -- cin -- to,
mi fi -- do a te; più non re -- si -- sto.

(Ho vin -- to.)
