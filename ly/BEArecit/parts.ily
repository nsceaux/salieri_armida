\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ubaldo
    \livretVerse#12 { Fermati, incauto. }
    \livretPers Rinaldo
    \livretVerse#12 { Oh Ciel! che miro? }
    \livretPers Ubaldo
    \livretVerse#12 { Ah Rinaldo Rinaldo, }
    \livretVerse#12 { Dove fuggi, che fai? Così del Cielo, }
    \livretVerse#12 { Che suo campion t'elesse }
    \livretVerse#12 { A difender la Fede, a strugger gli empi }
    \livretVerse#12 { Gli alti disegni, e i vaticini adempi? }
    \livretVerse#12 { Così da te Gerusalemme aspetta }
    \livretVerse#12 { Libertade, e vendetta? Ah chi ti rende }
    \livretVerse#12 { Così da te diverso, }
    \livretVerse#12 { Si vile agli occhi miei? Beltà fallace, }
    \livretVerse#12 { Che ti guida a perir; che ti prepara }
    \livretVerse#12 { Un laccio ad ogni passo, }
    \livretVerse#12 { Un angue in ogni fior. Folle! e non vedi, }
    \livretVerse#12 { Che quanto in lei di lusinghier t'apparve }
    \livretVerse#12 { Son d'Averno, a sedurti, inganni, e larve? }
    \livretVerse#12 { Torna, torna in te stesso }
    \livretVerse#12 { Sconsigliato che sei. Cogli il momento, }
    \livretVerse#12 { Che da te fugge intimorita al campo }
    \livretVerse#12 { Di quest' arme fatal. Fuggi, deludi }
    \livretVerse#12 { Le lusinghe fallaci. }
    \livretPers Rinaldo
    \livretVerse#12 { Misero me! }
    \livretPers Ubaldo
    \livretVerse#12 { Tu non mi guardi, e taci? }
    \livretVerse#12 { Tu arrossisci nel volto! Ah quel rossore }
    \livretVerse#12 { E il color di virtù. Torna, Rinaldo, }
    \livretVerse#12 { Alla gloria, all'onor. T'aspetta il Campo, }
    \livretVerse#12 { Ti richiama Goffredo, }
    \livretVerse#12 { Per sentier di prodigi il Ciel ti guida, }
    \livretVerse#12 { L'indugio è morte. }
    \livretPers Rinaldo
    \livretVerse#12 { E ho da lasciare Armida? }
    \livretPers Ubaldo
    \livretVerse#12 { Ingrato! ah nel tuo core }
    \livretVerse#12 { Chi bilancia il tuo Dio! }
  }
  \null
  \column {
    \livretPers Rinaldo
    \livretVerse#12 { Ma le promesse, }
    \livretVerse#12 { La fede, amico, i giuramenti miei? }
    \livretPers Ubaldo
    \livretVerse#12 { Gli rompi al Ciel per conservarli a lei? }
    \livretVerse#12 { Sai pur con quante frodi, }
    \livretVerse#12 { Con quant'arti sostei de' nostri Duci }
    \livretVerse#12 { E più prodi sedusse, e in rea catena }
    \livretVerse#12 { Gli serbava crudel a duro fato, }
    \livretVerse#12 { Se tu non eri; e tu la piangi? Ingrato! }
    \livretVerse#12 { Tema per te. Tempo verrà che Armida, }
    \livretVerse#12 { Sazia del tuo piacer, l'odio funesto }
    \livretVerse#12 { Coll' amor cangierà. Che te lasciando }
    \livretVerse#12 { Su quest' isola ignuda, o in un oscura }
    \livretVerse#12 { Tormentosa prigion, rimasto in preda }
    \livretVerse#12 { A tuoi fieri rimorsi, }
    \livretVerse#12 { All' orror di te stesso; a liberarti }
    \livretVerse#12 { Dallo strazio crudel, l'amica mano, }
    \livretVerse#12 { Ch'or salvarti potria, richiami in vano. }
    \livretVerse#12 { Misero! in tale stato }
    \livretVerse#12 { Oppresso, disperato, }
    \livretVerse#12 { Senza pietà, senza soccorso… }
    \livretPers Rinaldo
    \livretVerse#12 { Ah taci }
    \livretVerse#12 { Non inasprir la piaga, }
    \livretVerse#12 { Che mi lacera il cor. Se tu vedessi }
    \livretVerse#12 { Qua dentro amico, e quale acerba guerra }
    \livretVerse#12 { Vi fan gli opposti affetti, e qual mi scuote }
    \livretVerse#12 { Di miseria, e d'orror scena funesta. }
    \livretVerse#12 { Io ti farei pietà. Più non distinguo }
    \livretVerse#12 { Chi mi parla, ove son; tremo, e confondo }
    \livretVerse#12 { Col periglio la scampo; amico, oh Dio! }
    \livretVerse#12 { Guida, salvami tu. Fuggiam da questo }
    \livretVerse#12 { Insidioso recinto, }
    \livretVerse#12 { Mi fido a te; più non resisto. }
    \livretPers Ubaldo
    \livretVerse#12 { Ho vinto. }
  }
  \null
} #}))
