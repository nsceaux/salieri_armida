\clef "alto" r2 mi'4-\sug\f r |
mi'8.\f re'16 re'8.\trill do'32 re' do'4 r |
R1 |
do'8.\p sib16 sib8.\trill do'32 sib la4 r |
R1 |
do'4-\sug\p r re' r |
R1*62 |
r2 fad':16-\sug\p |
\ficta fad'1: |
\ficta fad': |
fad'2:-\sug\f fad'(-\sug\fp |
mi') r |
re' r |
dod' r |
R1*2 |
fad'4 r mi' r |
R1*2 |
\ficta dod'2 re' |
la' r |
R1*2 |
re'2 r |
R1 |
r2 re' |
r mi'4 r |
re' r re' r |
r2 r4 dod'-\sug\f |
