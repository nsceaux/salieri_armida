\piecePartSpecs
#`((bassi #:score-template "score-voix" #:indent 0)

   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretPers Ismene
    \livretVerse#12 { Ah difendete, amiche, }
    \livretVerse#12 { Il confidato passo. À noti segni }
    \livretVerse#12 { Imminente è il periglio, e nel più chiuso }
    \livretVerse#12 { Del custodito albergo, onde gelosa }
    \livretVerse#12 { Vieta l'accesso à suoi più fidi, e dove }
    \livretVerse#12 { Sola, e sicura al suo poter si fida, }
    \livretVerse#12 { In mezzo à frutti suoi, minaccia Armida. }
    \livretVerse#12 { Collo spuntar del sole }
    \livretVerse#12 { Più non appar la tenebrosa, e folta }
  }
  \null
  \column {
    \livretVerse#12 { Nebbia, che ad ogni sguardo }
    \livretVerse#12 { Quest’ Isola ascondea; meste, e confuse }
    \livretVerse#12 { Strida ingombrano il lido, e ignoto legno }
    \livretVerse#12 { Vi si scorge in sicuro: Erranti e sparsi }
    \livretVerse#12 { Vidi i Mostri custodi }
    \livretVerse#12 { Fuggir per l'erta, e s'altri accorre, e chiede }
    \livretVerse#12 { La cagion della fuga, un tal terrore }
    \livretVerse#12 { Quella Guardia fatal turba, e confonde }
    \livretVerse#12 { Che torce altrove il corso, e non risponde. }
  }
  \null
}

       #}))
