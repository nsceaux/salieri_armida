\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "bassi"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \break
        s2 s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \pageBreak
        s2  s1*2\break s1*2 s2 \bar "" \break
      }
    >>
  >>
  \layout {
    indent = \noindent
    short-indent = 0
  }
  \midi { }
}
