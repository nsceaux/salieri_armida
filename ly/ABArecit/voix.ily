\clef "alto/treble" <>^\markup\character Ismene
sib'2 sib'8 sib' sib' sib' |
sib' fa' r fa' fa' fa' fa' fa' |
re' re' r16 si' do'' re'' re''8 la' r la'16 si' |
do''4 do''8 sib' sol' sol' r16 sol' sol' la' |
sib'8 sib' sib' sib'16 sib' sib'8 do'' la' la' |
do'' do''16 do'' la'8 la' la' la'16 sol' la'8 la' |
r la' sib' do'' do'' fa' r do' |
fa' fa' la' la'16 sib' do''8 do'' r do'' |
do'' mib' mib' fa' re' re' r fa' |
sib' sib' lab' sib' sol'4 r16 sol' la' sib' |
\ficta sib'8 fa' r4 la'8 la'16 la' la'8 re'' |
re'' la' la'16 la' la'8 \appoggiatura sol'8 fad'4 r16 re' re' mi' |
fad'8 fad' r16 fad' sol' la' la'8 re' r16 la' la' sib' |
do''8 do'' r mib'' do'' do'' do'' sib' |
\grace la'8 sol'4 r sib'8 sib'16 sib' sib'8 do'' |
lab' lab' r fa' lab'4 lab'8 sib' |
sol' sol' r16 sib' sib' sib' sol'8 sol' r mib'16 fa' |
sol'4 lab'8 sib' sib' mib' r16 sib' sib' do'' |
do''8 sol' r sol'16 sol' sol'4 sol'8 lab' |
sib' sib' r16 sib' sib' do'' la'8 la' r do'' |
la' la' la' sol' la' la' r la'16 la' |
la'4 sol'8 la' fad' fad' do'' re''16 mib'' |
re''8 re'' r la'16 la' la'4 la'8 sib' |
do''4 do''8 do''16 re'' sib'8 sib' r sib' sib' sib' do'' re'' |
mib'' do'' r16 do'' do'' sib' sol'8 sol' r4 |
