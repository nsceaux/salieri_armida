Ah di -- fen -- de -- te, a -- mi -- che,
il con -- fi -- da -- to pas -- so. A’ no -- ti se -- gni
im -- mi -- nen -- te è il pe -- ri -- glio, e nel più chiu -- so
del cu -- sto -- di -- to al -- ber -- go, on -- de ge -- lo -- sa
vie -- ta l’ac -- ces -- so a’ suoi più fi -- di, e do -- ve
so -- la, e si -- cu -- ra al suo po -- ter si fi -- da,
in mez -- zo a’ fus -- ti suoi, mi -- nac -- cia Ar -- mi -- da.
Col -- lo spun -- tar del so -- le
più non ap -- par la te -- ne -- bro -- sa, e fol -- ta
neb -- bia, che ad o -- gni sguar -- do
quest’ I -- so -- la a -- scon -- dea; me -- ste, e con -- fu -- se
stri -- da in -- gom -- bra -- no il li -- do, e i -- gno -- to le -- gno
vi si scor -- ge in si -- cu -- ro: Er -- ran -- ti e spar -- si
vi -- di i mos -- tri cu -- sto -- di
fug -- gir per l’er -- ta, e s’al -- tri ac -- cor -- re, e chie -- de
la ca -- gion del -- la fu -- ga, un tal ter -- ro -- re
quel -- la guar -- dia fa -- tal tur -- ba, e con -- fon -- de
che tor -- ce al -- tro -- ve il cor -- so, e non ri -- spon -- de.
