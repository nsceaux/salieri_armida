\clef "bass" sib,2\f r |
R1 |
r2 fad4 r |
r2 sol4 r |
r2 r4 mib |
R1*3 |
r2 sib,4 r |
r2 mib4 r |
r fa fad2 |
R1*3 |
sol2 r |
fa2 r |
mib r |
R1 |
mi2 r |
r fa |
R1 |
r2 re4 r |
fad r r2 |
r sol4 r r2 |
do2 r4 re |
