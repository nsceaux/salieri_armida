\clef "bass" sol8\f r sib r do' r la r |
sib r do' r re' r re r |
sol\p r sol r sol r sol r |
sol r sol r sib r sib r |
do' r do' r do r do' r |
do' r la r fad r fad r |
sol r sol r sib\fp r si r |
do' r do' r re' r re r |
sol r sol, r sib\fp r si r |
do' r do' r dod'-\sug\fp r dod' r |
re' do'! sib do' re' r re r |
sol\f r do' r re' r re r |
sol r re r sol,4 r |
r sib sib,\rf sib |
r fa\f fa, fa\p |
r fa fa, fa |
r sib\f sib, re\f |
mib8 r mib r mib r mi r |
fa2 r |
re1\p |
mib1 |
fa\fp |
sol8 r sol\fp r sol r fa r |
mib r mib\fp r mib r re r |
sol r mib r fa r fa, r |
sib, r sib r sib, r re r |
mib r mib\fp r mib r re r |
sol r mib r fa r fa, r |
sib, r sib r sib, r r4 |
fa2:16\fp fa: |
fa: fa: |
fa:-\sug\fp fa: |
fa: fa,4 r |
solb1\p |
fa4\p fa fa |
fa fa fa |
fa fa fa |
fa fa fa |
fa,2\fermata r4 |
r4 sib-!\p lab-! r |
r sol-! sol,-! r |
r do'-! sib!-! r |
la2:8\p la: |
la:-\sug\fp la: |
la: la: |
la: la: |
la: la: |
la:-\sug\f la: |
sol\fermata\ff r |
R1*7 |
fa2\f sol4\p r |
la r la, r |
fa2\f sol4\p r |
la2 la |
sib mib! |
re do'\f |
sib,8 sib, do do sib, sib, do do |
sib, sib, si, si, si,2:8\p |
do: re8 re re, re, |
sib\f sib do' do' sib sib do' do' |
sib sib si si si2:8\p |
do'2:8 re'8 re' re re |
sol,\f sol, la,\p la, sib, sib, do do |
re2: re,: |
sol8\f sol la la sib sib do' do' |
re'2:8 re': |
re: re: |
sib8 sib do' do' sib sib do' do' |
sib sib do' do' sib sib si si |
do' mib' re' dod' re' do' sib la |
sib4 do' re' re |
sol r fa!2 |
sib r |
