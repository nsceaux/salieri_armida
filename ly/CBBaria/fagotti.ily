\clef "bass" R1*14 |
r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { do'8 sib la4 }
  { la8 sol fa!4 }
>> r4 |
R1 |
r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { fa'8 mib' re'4 }
  { re'8 do' sib4 }
>> r4 |
R1*5 |
r4 sol2-\sug\fp r4 |
r mib2-\sug\fp r4 |
R1 |
r4 fa'-.-\sug\p fa'-. r |
r mib2-\sug\fp r4 |
R1*3 |
r4 fa2 fa4~ |
fa4 r r2 |
r4 fa fa, r |
solb1\p |
%%
fa4\p fa fa |
fa fa fa |
fa fa fa |
fa fa fa |
fa,2 r4\fermata |
%%
R1*5 |
\once\slurDashed re'8(-\sug\rf \ficta mi') fa'4 r2 |
R1 |
dod8( re) \ficta mi4 r2 |
la1-\sug\f |
sol2-\sug\ff\fermata r |
R1*7 |
fa2-\sug\f r |
R1 |
fa2-\sug\f r |
R1*2 |
r2 do'-\sug\f |
sib4 r r2 |
r4 sol2 r4 |
R1 |
sib4-\sug\f r r2 |
r4 sol2 r4 |
R1 |
sol,4-\sug\f r r2 |
R1 |
sol,8-\sug\ff sol, la, la, sib, sib, do do |
re2:8 re: |
re: re,: |
sib8 sib do' do' sib sib do' do' |
sib sib do' do' sib sib si si |
do' mib' re' dod' re' do' sib la |
sib4 do' re' re |
sol r r2 |
R1 |
