\tag #'(armida basse) {
  Ah mi tol -- ga al -- men la vi -- ta
  il cru -- del, che m’ha tra -- di -- ta
  per pie -- tà del mio do -- lor
  per pie -- tà
  per pie -- tà __ del mio do -- lor.
  Se non ba -- sta in quel mo -- men -- to
  for -- se a uc -- ci -- der -- mi il tor -- men -- to,
  per -- chè al -- men l’e -- stre -- ma a -- i -- ta,
  per -- chè al -- men l’e -- stre -- ma a -- i -- ta,
  per -- chè al -- men l’e -- stre -- ma a -- i -- ta
  non la deb -- ba, non la deb -- ba a un tra -- di -- tor,
  non la deb -- ba, non la deb -- ba a un tra -- di -- tor.

  Ah mi tol -- ga al -- men la vi -- ta
  il cru -- del, che m’ha tra -- di -- ta
  per pie -- tà del mio do -- lor.
}
\tag #'(voix1 voix2 voix3) {
  Mi -- se -- ra Re -- gi -- na, che mai sa -- rà di te.
}
\tag #'(armida basse) {
  Cru -- de -- le, oh Dio.
  Ah mi tol -- ga al -- men la vi -- ta,
  ah mi tol -- ga al -- men la vi -- ta,
  il cru -- del, che m’ha tra -- di -- ta,
  ah mi tol -- ga al -- men la vi -- ta
  per pie -- tà del mio do -- lor, __
  per pie -- tà del mio do -- lor,
  per pie -- tà del mio do -- lor,
  del __ mio do -- lor,
  del __ mio do -- lor,
  per pie -- tà.
  Ah mi tol -- ga al -- men la vi -- ta
  per pie -- tà del mio do -- lor,
  per pie -- tà, __
  per pie -- tà del mio do -- lor,
  per pie -- tà del mio do -- lor,
  per pie -- tà del mio do -- lor.
}
