\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   
   (oboi #:score-template "score-deux-voix")
   (fagotti #:music , #{

\quoteBassi "CBBbassi"
<>^\markup\tiny B.
\cue "CBBbassi" { \mmRestInvisible s1*14 \mmRestVisible }
s1
<>^\markup\tiny B.
\cue "CBBbassi" { \mmRestInvisible s1 \mmRestVisible }
s2.
<>^\markup\tiny B.
\cue "CBBbassi" { \restInvisible s4\restVisible \mmRestInvisible s1*5 \mmRestVisible }
s1*12
s2.*5
<>^\markup\tiny B.
\cue "CBBbassi" { \mmRestInvisible s1*3 \mmRestVisible }
\new CueVoice { la2:8\p la: | la:-\sug\fp la: | }
s2
<>^\markup\tiny B.
\new CueVoice {
  la2:8 |
  \mmRestInvisible la:-\sug\fp la: | \mmRestVisible
}
s1*10
s2 <>^\markup\tiny B. \cue "CBBbassi" { \mmRestInvisible s1. \mmRestVisible }
s2 <>^\markup\tiny B. \cue "CBBbassi" {
  s2 
  \mmRestInvisible s1*2 \mmRestVisible
  s2
}

#})

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretPers Armida
  \livretVerse#10 { Ah mi tolga almen la vita }
  \livretVerse#10 { Il crudel, che m’ha tradita }
  \livretVerse#10 { Per pietà del mio dolor. }
  \livretVerse#10 { Se non basta in quel momento }
  \livretVerse#10 { Forse a uccidermi il tormento, }
  \livretVerse#10 { Perchè almen l’estrema aita }
  \livretVerse#10 { Non la debba a un traditor. }
}
       #}))
