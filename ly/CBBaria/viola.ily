\clef "alto" sib8-\sug\f r sol' r sol' r re' r |
re' r do' r re' r re r |
sol-\sug\p r sol' r sol' r sol' r |
sol' r sol' r sib r sib r |
do' r do' r do' r do' r |
do' r la r fad r fad r |
sol r sol r sib-\sug\fp r si r |
do' r do' r re' r re r |
sol r sol r sib\fp r si r |
do' r do' r dod'-\sug\fp r dod' r |
re'( do'!) sib-. do'-. re' r re r |
sol\f r do' r re' r re r |
sol' r re' r sol4 r |
r sib sib'4.-\sug\rf sib'8 |
\grace sib'8 la'4 << { do'8 sib la4 } \\ { la8-\sug\rf sol fa!4 } >> r4 |
r4 fa' \once\slurDashed fa'4.(-\sug\rf la'8) |
\grace do''8 sib'4 << { \once\slurDashed fa'8( mib') re'4 } \\ { re'8\rf do' sib4 } >> fa'\f |
sol'8 r sol' r sol' r sol' r |
fa'2 r |
re'1-\sug\p |
mib' |
fa'-\sug\fp |
sol'8 r sol'-\sug\fp r sol' r fa' r |
mib' r mib'-\sug\fp r mib' r re' r |
sol' r mib' r re' re'4 fa'8 |
fa'8 r sib' r sib r re' r |
mib' r mib'-\sug\fp r mib' r mib' r |
sol' r mib' r re' re'4 mib'8 |
re' r sib r sib r reb' reb' |
\ficta reb'4.\fp do'16 sib la4 sib |
la la r reb'8 reb' |
\ficta reb'4.-\sug\fp do'16 sib la4 sib |
\grace sib8 la4 la fa r |
r2 sib |
%%
la4.-\sug\p fa'8-.( fa'-. fa'-.) |
fa'4. fa'8-.( fa'-. fa'-.) |
fa'4. fa'8-.( fa'-. fa'-.) |
fa'4. fa'8-.( fa'-. fa'-.) |
fa'2 r4 |
%%
r sib'-!-\sug\p sib'-! r |
r si'-! si'-! r |
r do''-! do''-! r |
r dod'' dod'' la'8 la' |
la'4.\fp sold'16 sol' fa'4 \ficta mi' |
\once\slurDashed re'8(-\sug\rf mi') fa'4 r la'8 la' |
\once\slurDashed la'(\fp sold' sol' fa') \ficta mi'4 re' |
dod'8(-\sug\rf re') \ficta mi'4 r la'8\f la' |
la'-. sold'-. sol'-. fa'-. \ficta mi'-. re'-. sol'-. fa'-. |
mi'1-\sug\ff\fermata |
<>\p \ru#4 { fa'8-! la'-! } |
\ru#4 { sol' sib' } |
la' sold' la' sold' la' \ficta mi' dod' la |
re' \ficta mi' fa' sol' la' sib' la' sol' |
\ru#4 { fa' la' } |
\ru#4 { sol' sib' } |
la' sold' la' sold' la' \ficta mi' dod' la |
fa2-\sug\f fa'4-\sug\p \ficta mi' |
re'4 r dod' r |
re'2-\sug\f \ficta mi'4-\sug\p mi' |
la'2( fa' |
fa' la') |
la' do'!\f |
sib8 sib do' do' sib sib do' do' |
sib sib sol' sol' sol'2:8\p |
sol'8 sol' do' do' re' re' re re |
sib-\sug\f sib do' do' sib sib do' do' |
sib sib sol' sol' sol'2:8-\sug\p |
sol'8 sol' do' do' re' re' re re |
sol\f sol la\p la sib sib do' do' |
re'2:8 re: |
sol8-\sug\ff sol la la sib sib do' do' |
re'2:8 re': |
re': re: |
sib8 sib do' do' sib sib do' do' |
sib sib do' do' sib sib sol' sol' |
sol' mib' re' dod' re' do' sib la |
sib4 do' re' re |
sol r do''2 |
sib' r |
