\clef "treble" R1*14 |
r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { do''8 sib' la'4 }
  { la'8 sol' fa'!4 }
>> r4 |
R1 |
r4 <>-\sug\rf \twoVoices #'(primo secondo tutti) <<
  { fa''8 mib'' re''4 }
  { re''8 do'' sib'4 }
>> r4 |
R1*5 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { mib''2 }
  { sib'2 }
>> r4 |
r4 <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { sib' }
>> r4 |
R1 |
r4 <>\p \twoVoices #'(primo secondo tutti) <<
  { re''4-. re''-. }
  { sib'-. sib'-. }
>> r4 |
r <>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { sol''2 }
  { sib' }
>> r4 |
R1*2 |
<>-\sug\fp \twoVoices #'(primo secondo tutti) <<
  { \tieDashed fa''1~ | fa''~ | \tieSolid fa''~ | fa''2 fa'4 }
  { \once\tieDashed fa'1~ | fa'~ | fa'~ | fa'2 fa'4 }
>> r4 |
R1 |
%%
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { fa''4. do''8-.( do''-. do''-.) |
    sib'4. sib'8-.( sib'-. sib'-.) |
    la'4. do''8-.( do''-. do''-.) |
    reb''4. reb''8-.( reb''-. reb''-.) |
    \grace reb''8 do''2 r4 | }
  { la'!4. la'8-.( la'-. la'-.) |
    reb''4. reb''8-.( reb''-. reb''-.) |
    do''4. la'8-.( la'-. la'-.) |
    sib'4. sib'8-.( sib'-. sib'-.) |
    \grace sib'8 la'2 r4 | }
>>
%%
R1*5 | \allowPageTurn
<>\rf \twoVoices #'(primo secondo tutti) <<
  { fa''8( \ficta mi'') re''4 }
  { la'8( sol') fa'4 }
>> r2 |
R1 |
<>\rf \twoVoices #'(primo secondo tutti) <<
  { sol''8( fa'') \ficta mi''4 }
  { sib'8( la') sol'4 }
>> r2 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { la'8-. si'-. dod''-. re''-. mi''-. fa''-. sol''-. la''-. |
    sib''1\fermata | }
  { la'8-. sold'-. sol'-. fa'!-. mi'-. re'-. sol'-. fa'-. |
    mi'1\fermata | }
  { s1 | s\ff | }
>>
<<
  \tag #'(primo tutti) {
    \tag#'tutti <>^\markup\concat { 1 \super o }
    re''2\p r4 fa''8( re'') |
    re''2 r4 re'' |
    re'' r dod'' r |
    re'' r r2 |
    re''2 r4 fa''8( re'') |
    re''2 r4 re'' |
    re'' r dod'' r |
  }
  \tag #'secondo { R1*7 }
>>
<>\f \twoVoices #'(primo secondo tutti) <<
  { la''4( fa'') }
  { re''2 }
>> r2 |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { la''4( fa'') }
  { re''2 }
>> r2 |
R1*2 |
r2 <>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { fad''4. fad''8 | sol''4 }
  { re''4. re''8 | re''4 }
>> r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { fa''!2 }
  { re'' }
>> r4 |
R1 |
<>-\sug\f \twoVoices #'(primo secondo tutti) <<
  { sol''4 }
  { re'' }
>> r4 r2 |
r4 \twoVoices #'(primo secondo tutti) <<
  { fa''2 }
  { re'' }
>> r4 |
R1 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol''4 }
  { sib' }
>> r r2 |
R1 |
<>-\sug\ff \twoVoices #'(primo secondo tutti) <<
  { sib''4 la''8 sol'' fa'' mib'' re'' do'' | sib'4 }
  { sib'4 do'' re''8 do'' sib' la' | sol'4 }
>> r4 r2 |
\twoVoices #'(primo secondo tutti) <<
  { la''1 | }
  { sol''2 fad'' | }
>>
\tag#'tutti <>^"a 2." sol''16 sib''8. la''16 fad''8. sol''16 sib''8. la''16 fad''8. |
sol''16 sib''8. la''16 fad''8. sol''16 lab''8. sol''16 fa''!8. |
mib''4 sol'' fad''4.\trill mi''16 fad'' |
sol''16 re''8. mib''16 do''8. sib'16 sol'8. la'16 fad'8. |
sol'4 r r2 |
R1 |
