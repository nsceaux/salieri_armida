\key sib \major
\tempo "Allegro con agitazzione" \midiTempo#120
\time 4/4 s1*34 \bar "||"
\time 3/4 \tempo "Adagio" \midiTempo#80 s2.*5 \bar "||"
\time 4/4 \tempo "Primo tempo" \midiTempo#120 s1*40 \bar "|."
\endMark "Seg."
