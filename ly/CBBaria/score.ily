\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "oboi" >>
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new ChoirStaff \with {
      \haraKiriFirst
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Cor.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix1 \includeNotes "voix"
      >> \keepWithTag #'voix1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix2 \includeNotes "voix"
      >> \keepWithTag #'voix2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'voix3 \includeNotes "voix"
      >> \keepWithTag #'voix3 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \with {
        instrumentName = \markup\character Armida
        shortInstrumentName = \markup\character Ar.
      } \withLyrics <<
        \global \keepWithTag #'armida \includeNotes "voix"
      >> \keepWithTag #'armida \includeLyrics "paroles"
      \new Staff \with { \bassoInstr } <<
        \global \keepWithTag #'tutti \includeNotes "bassi"
        \modVersion { s1*34\break s2.*5\break }
        \origLayout {
          s1*3\pageBreak
          s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          s1*4\pageBreak
          % 5
          s1*4\pageBreak
          s1*4\pageBreak
          s1*4\pageBreak
          \grace s8 s1*4\pageBreak
          s1*3 s2.\pageBreak
          % 10
          s2.*4 s1\pageBreak
          s1*4\pageBreak
          s1*5\pageBreak
          s1*5\pageBreak
          \grace s8 s1*5\pageBreak
          % 15
          s1*4\pageBreak
          s1*4\pageBreak
          \grace s8 s1*4\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
