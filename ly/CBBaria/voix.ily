<<
  \tag #'voix1 {
    \clef "soprano/treble" R1*34 |
    R2. |
    reb''4. reb''8 reb'' reb'' |
    \grace reb''8 do''4 do'' r8 do'' |
    reb''4. reb''8 reb'' reb'' |
    do''2 r4\fermata |
    R1*40 |
  }
  \tag #'voix2 {
    \clef "soprano/treble" R1*34 |
    R2. |
    sib'4. sib'8 sib' sib' |
    \grace sib'8 la'4 la' r8 la' |
    sib'4. sib'8 sib' sib' |
    la'2 r4\fermata |
    R1*40 |
  }
  \tag #'voix3 {
    \clef "alto/treble" R1*34 |
    R2. |
    fa'4. fa'8 fa' fa' |
    fa'4 fa' r8 fa' |
    fa'4. fa'8 fa' fa' |
    fa'2 r4\fermata |
    R1*40 |
  }
  \tag #'(armida basse) {
    \clef "soprano/treble" R1*2 |
    re''2 r4 re''8[ mib''] |
    mib''[ re''] re''[ sol''] sol''[ fa''] mib''[ re''] |
    \grace re''8 mib''4 mib'' r mib''8 mib'' |
    mib''4( re''8) mib'' mib''4( re''8) do'' |
    sib'8.[ do''16] re''4 sol''4. fa''8 |
    \grace fa''8 mib''4 re''8[ do''] \grace do''8 sib'4. la'8 |
    \grace la'8 sib'2 sol''4. fa''8 |
    mib''2 la''4. sol''8 |
    fad''4( sol''8) mib'' re''[ do''] sib'[ la'] |
    sol'4 r r2 |
    r2 r4 sib'8 sib' |
    sib'4. do''16[ re''] mib''4. re''8 |
    \grace re''8 do''4 do'' r do''8 do'' |
    do''4. re''16[ mib''] fa''4. mib''8 |
    \grace mib''8 re''4 re'' r re''8 re'' |
    \grace re''8 do''4 do''8.[ sol''16] sol''4. sib'8 |
    \grace sib'8 la'[ sol'16 fa'] fa'4 r sol'8 la' |
    sib'[ fa''] mib''[ re''] do''[ sib'] la'[ sib'] |
    \grace si'8 do''4 do'' r si'!8 do'' |
    re''[ lab''] sol''[ fa''] mib''[ re''] do''[ re''] |
    \grace re''8 mib''4 mib''2 re''8 re'' |
    do''[ sol''] sol''2 fa''8 fa'' |
    \grace fa''8 mib''4 re''8 do'' sib'4. do''8 |
    \grace do''8 re''2 r4 re''8 re'' |
    do''[ sol''] sol''2 fa''8 fa'' |
    \grace fa''8 mib''4 re''8 do'' sib'4. do''8 |
    sib'4 r r fa''8 fa'' |
    fa''4. mib''16[ reb''] do''4 reb'' |
    \ficta reb'' do'' r fa''8 fa'' |
    fa''4. mib''16[ reb''] do''4. reb''8 |
    \grace mib''8 \ficta reb''4 do'' r fa''8 sib'' |
    sib''[ lab''] lab''[ solb''] solb''[ fa''] fa''[ mi''] |
    %%
    fa''2 r4 |
    r4 r r8 fa'' |
    fa''4 fa' r |
    r r r8 fa''16[ reb''] |
    \grace reb''8 do''2\fermata fa''8 mib''! |
    %%
    re''!4 re'' r8 re'' re'' do'' |
    si'4 si' r sol''8 fa'' |
    mi''4 mi'' r8 mi'' mi'' re'' |
    dod''4 dod'' r la'8 la' |
    la'4. si'16[ dod''] re''4 mi'' |
    fa''8[ \ficta mi''] re''4 r la'8 la' |
    la'[ si'] dod'' re'' mi''4 fa'' |
    sol''8[ fa''] mi''4 r la'8 la' |
    la'[ si'] dod''[ re''] \ficta mi''[ fa''] sol''[ la''] |
    sib''!4.\melisma sol''8 mi''4\fermata\melismaEnd r4 |
    re''4 r r re''8[ fa''] |
    \grace fa''8 mi''2 r4 mi''8[ sol''] |
    \grace sol''8 fa''2 r4 r8 mi'' |
    \grace mi''8 fa''2 r |
    re'' r4 re''8[ fa''] |
    \grace fa''8 \ficta mi''2 r4 \ficta mi''8[ sol''] |
    fa''2 r4 r8 mi'' |
    la''4( fa'') sib''16[( sol''8.) sib''16( sol''8.)] |
    fa''2 mi'' |
    la''4( fa'') sib''16[( sol''8.) sib''16( sol''8.)] |
    fa''2 mib''! |
    re'' dod''4. dod''8 |
    re''2 fad''4. fad''8 |
    sol''([ sib'']) la''([ fad'']) sol''([ sib'']) la''([ fad'']) |
    sol''[ fa''!] fa''2 fa''8 fa'' |
    \grace fa''8 mib''4 re''8[ do''] sib'4 la' |
    sol'' r sol''8([ sib'']) la''([ fad'']) |
    sol''4( fa''!2) fa''8 fa'' |
    \grace fa''8 mib''4 re''8[ do''] sib'4 la' |
    sol'' fa''8 mib'' re''4 do'' |
    sib'2 la' |
    sib''4 la''8 sol'' fa''[ mib''] re''[ do''] |
    sib'1 |
    la'\trill |
    sol'2 r |
    R1*5 |
  }
>>