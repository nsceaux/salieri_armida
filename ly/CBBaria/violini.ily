\clef "treble" \override Staff.AccidentalSuggestion.avoid-slur = #'outside
<re' re''>8-\sug\f q r re'' r mib'' r fad'' |
r sol'' <<
  \tag #'primo {
    r8 mib'' r re'' r fad' |
    sol'(-\sug\p sib') re''-. re''-. re''-. re''-. re''( mib'') |
    mib''( re'') re''-. sol''-. sol''( fa'') mib''-. re''-. |
    \grace re''8 mib''4 mib''8 mib'' mib''2:8 |
    mib''8-! mib''( re'') mib''-! mib''-! mib''( re'') do'' |
    sib'8.( do''16) re''8-. re''-. sol''8\fp sol''4 fa''8 |
    \grace fa''8 mib''4 re''8 do'' sib' sib'4 la'8 |
    la'( sib') sib'-. re''-. sol''8\fp sol''4 fa''8 |
    fa''( mib'') mib''-. mib''-. << { la''8 la''4 } \\ { la'8\fp la'4 } >> sol''8 |
    fad''8-. fad''( sol'') mib''-. re'' do'' sib' la' |
    sol'8-!\f sol'-! r mib'' r re'' r \ficta fad' |
  }
  \tag #'secondo {
    r8 do'' r sib' r la' |
    sol'4-\sug\p sib'8-. sib'-. sib'-. sib'-. \once\slurDashed sib'( do'') |
    do''( sib') sib'-. sib'-. sol'2 |
    sol'4 sol'8 sol' sol'2:8 |
    sol'8( do'') do''-. do''-. la'2 |
    \once\slurDashed re'8.( do'16) sib8-. sib-. re''8\fp re''4 re''8 |
    sol'8 sol'4 sol' sol' \ficta fad'8 |
    fad'( sol') sol'-. sib'-. << { re''8 re''4 re''8 } \\ { sol'8-\sug\fp sol'4 sol'8 } >> |
    re''8( do'') do''-. do''-. << { mi''8 mi''4 mi''8 } \\ { la'8-\sug\fp la'4 la'8 } >> |
    re''8 re''4 do''8 sib' la' sol' \ficta fad' |
    sol'-\sug\f sol' r sol' r sib' r la' |
  }
>>
r8 sol' r re' sol4 <<
  \tag #'primo {
    sib'8 sib' |
    sib'4. do''16 re'' mib''4.(\rf re''8) |
    \grace re''8 do''4 do''8(\rf sib') la'4 do''8-\sug\p do'' |
    do''4. re''16 mib'' fa''4.(\rf mib''8) |
    \grace mib''8 re''4 fa''8(-\sug\rf mib'') re''4 re''-\sug\f |
    do''8 do''4 do'' do'' do''8 |
    do''2 r4 sol'8 la' |
    sib'1\p |
    do''8( sol'' fa'' mib'') re''( do'' si' do'') |
    re''1\fp |
    \grace re''8 mib''4 mib''4.\rf mib''8-. re''-. do''-. |
    do''8 sol'' sol''4.\rf sol''8( fa'') fa''-. |
    \grace fa''8 mib''4 re''8 do'' sib' sib'4 do''8 |
    do''( re'') re''-. re''-. re''2:8 |
    do''8( sol'') sol''4.-\sug\rf sol''8 fa'' fa'' |
    \grace fa''8 mib''4 re''8 do'' sib' sib'4 do''8 |
    sib'4 r r fa''8 fa'' |
    fa''4.\fp mib''16 reb'' do''4 reb'' |
    \grace reb''8 do''4 do'' r fa''8 fa'' |
    fa''4.\fp mib''16 reb'' do''4 reb'' |
    \grace reb''8 do''4 do'' r <>_\markup\whiteout { con la parte } fa''8( sib'') |
    sib''( lab'') lab''( solb'') solb''( fa'') fa''( mi'') |
    fa''4.-\sug\p do''8-.( do''-. do''-.) |
    reb''4. reb''8-.( reb''-. reb''-.) |
    do''4. do''8-.( do''-. do''-.) |
    reb''4. reb''8-.( reb''-. reb''-.) |
    \grace reb''8 do''2 r4 |
  }
  \tag #'secondo {
    r4 |
    re'4. mib'16 fa' \once\slurDashed sol'4.(-\sug\rf fa'8) |
    fa'4 la'8(-\sug\rf sol') fa'4 la'8-\sug\p la' |
    la'4. sib'16 do'' \once\slurDashed re''4.(-\sug\rf do''8) |
    \grace do''8 sib'4 \once\slurDashed re''8(-\sug\rf do'') sib'4 sib'-\sug\f |
    sib'8 sib'4 sib' sib' sib'8 |
    la'2 r |
    fa'1\p |
    sol' |
    lab'\fp |
    sib'4 sib'2:8-\sug\fp sib'8 sib' |
    sib'4 sib'2:-\sug\fp sib'8 sib' |
    sib'8 sib'4 la'8 sib' sib'4 la'8 |
    la'( sib') sib'-. sib'-. sib'2: |
    sib'4 sib'2:-\sug\fp sib'8 sib' |
    sib'8 sib'4 la'8 sib' sib'4 la'8 |
    sib' r sib r sib r reb'8 reb' |
    \ficta reb'4.\fp do'16 sib la4 sib |
    \grace sib8 la4 la r reb'8 reb' |
    \ficta reb'4.-\sug\fp do'16 sib la4 sib |
    \grace sib8 la4 la fa'4. sib'8 |
    sib'( lab') lab'( \ficta solb') solb'( fa') fa'( mi') |
    fa'4.-\sug\p la'8-.( la'-. la'-.) |
    sib'4. sib'8-.( sib'-. sib'-.) |
    la'4. la'8-.( la'-. la'-.) |
    sib'4. sib'8-.( sib'-. sib'-.) |
    \grace sib'8 la'2 r4 |
  }
>>
<<
  \tag #'primo {
    r4 fa''-!-\sug\p fa''-! r |
    r4 sol''-! sol''-! r |
    r sol''-! sol''-! r |
    r la'' la'' la'8 la' |
    la'4.\fp si'16 dod'' re''4 mi'' |
    \once\slurDashed fa''8(\rf \ficta mi'') re''4 r la'8 la' |
    la'(\fp si' dod'' re'') mi''4 fa'' |
    sol''8(\rf fa'') mi''4 r la'8\f la' |
    la'-. si'-. dod''-. re''-. mi''-. fa''-. sol''-. la''-. |
    sib''!1\fermata\ff |
    r8 re''\p \grace mib''16 re''16( dod''32 re'' fa''8) r re'' \grace mib''16 re''16( dod''32 re'' fa''8) |
    r8 \ficta mi'' \grace fa''16 mi''( red''32 mi'' sol''8) r mi'' \grace fa''16 mi''16( red''32 mi'' sol''8) |
    r8 fa'' \grace sol''16 fa''32( \ficta mi'' fa''16 la''8) r \grace fa''16 mi''32 red'' mi''16 sol''8( mi'') |
    re''4 r r2 |
    r8 re'' \grace mib''16 re''16 dod''32 re'' fa''8 r re'' \grace mib''16 re''16 dod''32 re'' fa''8 |
    r8 \ficta mi'' \grace fa''16 mi'' red''32 mi'' sol''8 r mi'' \grace fa''16 mi''16 red''32 mi'' sol''8 |
    r8 \grace sol''16 fa''32 \ficta mi'' fa''16 la''8 fa'' r \grace fa''16 mi''32 \ficta red'' mi''16 sol''8 mi'' |
    << { la''4 fa'' } \\ la'2\f >> sib''16(\p sol''8.) sib''16( sol''8.) |
    r8 \grace sol''16 fa''32 mi'' fa''16 la''8 fa'' r \grace fa''16 mi''32 red'' mi''16 sol''8( mi'') |
    << { la''4 fa'' } \\ la'2\f >> sib''16(\p sol''8.) sib''16( sol''8.) |
    fa''2( mib''! |
    re'' dod'') |
    re'' <re' la' fad''>4.\f <la' fad''>8 |
    sol''( sib'') la''( fad'') sol''( sib'') la''( fad'') |
    sol''8 fa''! fa''4. fa''8\p fa'' fa'' |
    \grace fa''8 mib''4 re''8 do'' sib'4 la' |
    sol''8(\f sib'') la''( fad'') sol''( sib'') la''( fad'') |
    sol''4 fa''!4. fa''8\p fa'' fa'' |
    \grace fa''8 mib''4 re''8 do'' sib'4 la' |
    sol''4\f fa''8\p mib'' re''4 do'' |
    sib'2:8 la': |
    <sol re' sib' sol''>4\ff la''8 sol'' fa'' mib'' re'' do'' |
    sib'16 sib'' sib'' sib'' sib''4:16 sib''2: |
    la'': la'': |
    sol''16 sib''8. la''16 fad''8. sol''16 sib''8. la''16 fad''8. |
    sol''16 sib''8. la''16 fad''8. sol''16 lab''8. sol''16 fa''!8. |
    mib''4 sol'' fad''4.\trill mi''16 fad'' |
    sol''16 re''8. mib''16 do''8. sib'16 sol'8. la'16 fad'8. |
    sol'4 r <fa' mib''!>2 |
    <re'' fa'>2 r |
  }
  \tag #'secondo {
    r4 re''-!-\sug\p re''-! r |
    r re''-! re''-! r |
    r mi''-! mi''-! r |
    r mi'' mi'' dod'8 dod' |
    dod'4.-\sug\fp re'16 mi' fa'4 sol' |
    la'8(-\sug\rf sol') fa'4 r dod'8 dod' |
    \once\slurDashed dod'(-\sug\fp re' mi' fa') sol'4 la' |
    sib'!8(-\sug\rf la') sol'4 r dod'8-\sug\f dod' |
    dod'-. re'-. mi'-. fa'-. sol'-. la'-. sib'-. la'-. |
    dod''1\ff\fermata |
    <>-\sug\p \ru#4 { fa'8-. la'-. } |
    \ru#4 { sol' sib' } |
    la' sold' la' sold' la' \ficta mi' dod' la |
    re' \ficta mi' fa' sol' la' sib' la' sol' |
    \ru#4 { fa' la' } |
    \ru#4 { sol' sib' } |
    la'8 sold' la' sold' la' \ficta mi' dod' la |
    <la' fa''>4(\f re'') sol'8-.\p sib'-. sol'-. sib'-. |
    la' sold' la' sold' la' \ficta mi' dod' la |
    <la' fa''>4(\f re'') sol'8-.\p sib'-. sol'-. sib'-. |
    re''2( do'' |
    sib' sol') |
    fad' <re' re''>4.-\sug\f q8 |
    q4:16 q: q2: |
    << { \voiceOne re''4 re'' re''-\sug\p re'' \oneVoice } { re'4 re'2 re'4 } >> |
    \grace re''8 do''4 sib'8 la' sol'4 fad' |
    <re' re''>2:16\f q: |
    q4 << q2. { s4 s-\sug\p } >> |
    \grace re''8 do''4 sib'8 la' sol'4 fad' |
    sib'8-\sug\f sib' do''-\sug\p do'' sib'4 la' |
    sol'2:8 \ficta fad': |
    sib8-\sug\ff sib do' do' re' re' mib' mib' |
    re'16 sol'' sol'' sol'' sol''4:16 sol''2: |
    sol'': fad'': |
    sol''16 re'' re'' re'' re''4:16 <re'' re'>2: |
    q1: |
    do'8 mib' re' dod' re' do' sib la |
    sib4 sol'16 mib'8. re'16 sib8. do'16 la8. |
    sib4 r la'2 |
    sib' r |
  }
>>
