\clef "treble"
<<
  \tag #'(primo tutti) {
    R2.*2 |
    <>^\markup\concat { 1 \super o }
    sol''4-\sug\f fad''8( mi'' re'' mi'') |
    \grace re''8 do''2 si'4 |
    mi''4~ mi''16( sold'' la'' sold'') la''( si'' do''' mi'') |
    re''4~ re''16( fad'' sol'' fad'') sol''( la'' si'' re'') |
    re''8 mi''16 fad'' \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
    \grace si'8 la'2 r4 |
    R2.*2 |
    sol''4-\sug\rf fad''8( mi'' re'' mi'') |
    \grace re''8 do''2-\sug\p si'4 |
    mi''2. |
    re'' |
  }
  \tag #'secondo { R2.*14 }
>>
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { si''4( la'' sol'') |
    \grace sol''8 fad''2 }
  { re''4( do'' si') |
    \grace si'8 la'2 }
>> r4 |
R2.*3 |
<>-\sug\p \twoVoices #'(primo secondo tutti) <<
  { la''4. la''8\rf( re''' do'''!) |
    si''2. |
    la''4. la''8 re''' do'''! |
    si''2.\fermata |
    la''2 }
  { fad''2 fad''4 |
    sol''2. |
    fad''2 fad''4 |
    sol''2.\fermata |
    fad''2 }
>> r4 | \allowPageTurn
<<
  \tag #'(primo tutti) {
    R2.*4 |
    sol''4-\sug\rf fad''8( mi'' re'' mi'') |
    \grace re''8 do''2-\sug\p si'4 |
    mi''2. |
    re'' |
  }
  \tag #'secondo { R2.*8 }
>>
\twoVoices #'(primo secondo tutti) <<
  { si''8 la'' sol''4 fad'' |
    sol''4. mi''16( re'') do''( si') la'( sol') |
    fad'4( sol'8) mi''16( re'') do''( si') la'( sol') |
    fad'4 sol' sol'' |
    mi''2 }
  { re''8. do''16 si'4 la' |
    sol' r r |
    re''2 r4 |
    re''2 re''4 |
    do''2 }
>> r4 |
\twoVoices #'(primo secondo tutti) <<
  { re''2\fermata\fp }
  { sol'2\fp\fermata }
>> r4 |
r \twoVoices #'(primo secondo tutti) <<
  { si''4 la'' |
    sol''r16 re''(\f mi'' fad'') sol''( si'' la'' sol'') |
    \grace sol''8 fad''4. sol''16 la'' sol''8 re'' |
    mi''8. fad''32 sol'' si'4. la'8 |
    sol'4 }
  { sol''4 fad'' |
    sol'' si'-\sug\f si' |
    do'' la' si' |
    r sol'4. fad'8 |
    sol'4 }
>> r4 r |
