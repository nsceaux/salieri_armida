\clef "soprano/treble" sol'4 r r |
R2.*7 |
re''4 do''8 si' la' sol' |
\grace sol'8 fad'2 sol'4 |
r4 r r8 mi'' |
\grace re''8 do''2 si'4 |
mi''2. |
re''2 si'8([ re'']) |
re''8.([\melisma mi''32 re'']) do''4\melismaEnd si' |
si'4\melisma la'\melismaEnd r |
la'8[ re''] re'' re'' re''16.*2/3[ mi''32*2/3 fad''16*2/3] mi''16[ re''] |
\grace { \ficta dod''8 } si'4 r r |
sol'8 sol' sol' sol'16.*2/3[ la'32*2/3 si'16*2/3] la'8[ sol'] |
fad'4 r r |
si'4 si'8 si' \grace mi'' re'' dod''16[ si'] |
la'4 r r8 la' |
si'4.( re''8[ dod'']) si' |
\grace si' la'2 r4 |
re''4 re''8 do''! si' la' |
sold'4.\melisma la'16[ si']\melismaEnd la'4 |
do''4 do''8 si' la' sol' |
fad'4.\melisma sol'16[ la']\melismaEnd sol'4 |
r r r8 mi'' |
\grace re''8 do''2 si'4 |
mi''2. |
re''2 si'8 re'' |
re''([\melisma mi''16 do'']) si'4\melismaEnd la' |
sol' r r |
re''8([ do'']) si'4 r |
re''8([ do'']) si' la' sol' fa' |
mi'8.[ \ficta fa'16] mi'4 r8 mi'' |
re''8.\fermata[ mi''32 do''] si'4 r8 sol'' |
\grace fad''16 mi''8([ re''16 do'']) si'4\( la'\) |
sol' r r |
R2.*3 |
