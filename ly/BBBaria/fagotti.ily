\clef "tenor" R2.*2 |
<>\f \twoVoices #'(primo secondo tutti) <<
  { sol'4 fad'8 mi' re' do' |
    \grace re'8 do'2 si4 | }
  { mi'4 re'8 do' si la |
    \grace si8 la2 sol4 | }
>>
sol'4 mi'2 |
sol'4 re'2 |
si4 r si8 do'16 dod' |
re'4 re r |
R2.*2 |
\twoVoices #'(primo secondo tutti) <<
  { sol'4 fad'8 mi' re' mi' |
    \grace re'8 do'2 si4 | }
  { mi'4 re'8 do' si do' |
    \grace si8 la2 sol4 | }
  { s2.-\sug\rf | s-\sug\p }
>>
\clef "bass" R2.*5 |
r4 \once\slurDashed sol(-\sug\rf fad) |
mi r r |
re'2.-\sug\p~ |
re' |
re'~ |
re'\fermata |
re2 r4 |
R2.*4 |
<>-\sug\rf \clef "tenor" \twoVoices #'(primo secondo tutti) <<
  { sol'4 fad'8 mi' re' mi' |
    \grace re'8 do'2 si4 | }
  { mi'4 re'8 do' si do' |
    \grace si8 la2 sol4 | }
  { s2. | s-\sug\p }
>>
R2.*4 |
\clef "bass" re'2 r4 |
re'2 sol4~ |
sol2 r4 |
sol2-\sug\fp\fermata r4 |
r \twoVoices #'(primo secondo tutti) <<
  { si4 la | sol }
  { sol fad | sol }
>> sol4\f sol |
la do'( si) |
do' re' re |
sol r r |
