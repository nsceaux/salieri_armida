\score {
  <<
    \new ChoirStaff <<
      \new GrandStaff \with { \flautiInstr  } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "flauti" >>
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'tutti \includeNotes "fagotti"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'primo \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'secondo \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } << \global \includeNotes "viola" >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Rinaldo
      shortInstrumentName = \markup\character Rin.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s2.*3\pageBreak
        \grace s8 s2.*4\pageBreak
        \grace s8 s2.*3\pageBreak
        s2.*5\pageBreak
        \grace s8 s2.*3\pageBreak
        s2.*3\pageBreak
        s2.*3\pageBreak
        s2.*3\pageBreak
        s2.*3\pageBreak
        s2.*4\pageBreak
        s2.*3\pageBreak
        s2.*3\pageBreak
        \grace s8
      }
    >>
  >>
  \layout { }
  \midi { }
}
