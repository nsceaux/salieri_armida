\clef "treble"
<<
  \tag #'primo {
    re''4_\markup\italic dol. do''8( si' la' sol') |
    \grace sol'8 fad'2 sol'4 |
    sol''\f fad''8( mi'' re'' mi'') |
    \grace re''8 do''2 si'4 |
  }
  \tag #'secondo {
    sol'4 r r |
    re' do' si |
    sol'-\sug\f fad'8( mi' re' mi') |
    \grace re'8 do'2 si4 |
  }
>>
mi''4~ mi''16( sold'' la'' sold'') la''( si'' do''' mi'') |
re''4~ re''16( fad'' sol'' fad'') sol''( la'' si'' re'') |
re''8 mi''16 fad'' \grace la''8 sol'' fad''16 mi'' \grace mi''8 re'' do''16 si' |
\grace si'8 la'2 r4 |
<>\p <<
  \tag #'primo {
    re''4 do''8( si' la' sol') |
    \grace sol'8 fad'2 sol'4 |
    sol''4\rf fad''8( mi'' re'' mi'') |
    \grace re''8 do''2\p( si'4) |
  }
  \tag #'secondo {
    <sol re' si'>4 r r |
    re'4 do' si |
    sol'-\sug\rf fad'8( mi' re' mi') |
    \grace re'8 do'2-\sug\p si4 |
  }
>>
r8 mi''(\rf re'' do'' si' la') |
r re''(\rf do'' si' la' sol') |
<<
  \tag #'primo {
    re''4(\p do'' si') |
    \grace do''8 si'4( la') r |
    re''2. |
    si'8 r si'4(\rf la') |
    sol'2. |
    fad'4 r8 la'(\rf re'' do''!) |
    si'2. |
    la'4 r8 la'(\rf re'' do'') |
    si'2.\fermata |
    la'2 r4 |
  }
  \tag #'secondo {
    si'4(-\sug\p la' sol') |
    sol'( fad') r |
    la'2. |
    re'8 r si'4(-\sug\rf la') |
    sol'4( mi' dod') |
    re'4 r la' |
    sol'2. |
    fad'2 fad'4 |
    sol'2.\fermata |
    fad'2 r4 |
  }
>>
R2. |
<<
  \tag #'primo { mi''4.( re''8 do''!4) | }
  \tag #'secondo { << si'2 \\ sold' >> la'4 | }
>>
R2. |
<<
  \tag #'primo {
    re''4.(\rf do''8 si'4) |
    sol''4\rf fad''8( mi'' re'' mi'') |
    \grace re''8 do''2(\p si'4) |
  }
  \tag #'secondo {
    << la'2 \\ fad'-\sug\rf >> sol'4 |
    sol'-\sug\rf fad'8( mi' re' mi') |
    \grace re'8 do'2-\sug\p si4 |
  }
>>
r8 mi''(\rf re'' do'' si' la') |
r re''(\rf do'' si' la' sol') |
<<
  \tag #'primo {
    re''8. do''16 si'4 la' |
    sol'4. mi''16( re'') do''( si') la'( sol') |
    fad'4 sol'8 mi''16( re'') do''( si') la'( sol') |
    fad'4( sol') <fa' sol> |
    <sol mi'>2 r4 |
    re''2\fermata\fp r4 |
    \grace fad''16 mi''8 re''16 do'' si'4 la' |
    sol' r16 re''(\f mi'' fad'') sol''( si'' la'' sol'') |
    \grace sol''8 fad''4. sol''16 la'' sol''8 re'' |
    mi''8. fad''32 sol'' si'4. la'8 |
    sol'4.
  }
  \tag #'secondo {
    sol'8. la'16 sol'4 fad' |
    sol' r r |
    do'4( si) r |
    do'4( si) re' |
    do'2 r4 |
    sol'2\fp\fermata r4 |
    do''8 si'16 la' sol'4 fad' |
    sol' si'-\sug\f si' |
    do'' la' si' |
    r sol'4. fad'8 |
    sol'4.
  }
>> sol'8( si' la') |
