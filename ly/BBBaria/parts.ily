\piecePartSpecs
#`((flauti #:score-template "score-deux-voix" #:indent 0)
   (fagotti #:score-template "score-voix"
            #:music , #{
\quoteBassi "BBBbassi"
<>_\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s2.*2 \mmRestVisible }
\clef "tenor" s2.*6 \clef "bass"
<>_\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s2.*2 \mmRestVisible }
\clef "tenor" s2.*2
<>_\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s2.*5 \mmRestVisible }
s2.*8 \allowPageTurn
<>_\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s2. \mmRestVisible }
s2.
\cue "BBBbassi" { \mmRestInvisible s2. \mmRestVisible }
s2.*2 \clef "bass"
<>_\markup\tiny "B."
\cue "BBBbassi" { \mmRestInvisible s2.*4 \mmRestVisible }

    #})

   (violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (bassi #:score-template "score-voix" #:indent 0)

   (silence #:on-the-fly-markup , #{

\markup\column {
  \livretVerse#10 { Lungi da te, ben mio, }
  \livretVerse#10 { Si viver non poss' io, }
  \livretVerse#10 { Lungi da te, che sei }
  \livretVerse#10 { Luce degli occhi miei, }
  \livretVerse#10 { Vita di questo cor; }
  \livretVerse#10 { Venga, e in un dolce sonno, }
  \livretVerse#10 { Se te mirar non ponno, }
  \livretVerse#10 { Mi chiuda i lumi amor. }
} #}))

