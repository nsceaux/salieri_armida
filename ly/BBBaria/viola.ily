\clef "alto" sol'4 r r |
re'2. |
mi'4-\sug\f re'8( do' si do') |
\grace si8 la2 sol4 |
sol'4 mi'2 |
sol'4 re'2 |
si4 r si8 do'16 dod' |
re'2 r4 |
sol-\sug\p r r |
re'2. |
mi'4-\sug\rf re'8( do' si do') |
\grace si8 la2-\sug\p sol4 |
do'2. |
si |
si4-\sug\p do' dod' |
re' re' re |
fad'2. |
sol'8 r sol'4(-\sug\rf fad') |
mi'2. |
re':8-\sug\p |
re': |
re'8 re' re' re' re' re' |
re'2.\fermata |
re'2 r4 |
R2. |
mi'2. |
R2. |
re'2.-\sug\rf |
mi'4-\sug\rf re'8( do' si do') |
\grace si8 la2-\sug\p sol4 |
do'2. |
si |
si8 do' re'4 re |
sol r r |
la4( sol) r |
la( sol) si |
do'2 r4 |
re'2\fp\fermata r4 |
do' re' re |
sol re'2\f |
re'2. |
do'4 re' re |
sol4. sol'8( si' la') |
