- so.

Lun -- gi da te, ben mi -- o, ben mi -- o,
se vi -- ver non __ poss’ io, __
lun -- gi da te, che sei
lu -- ce de -- gli oc -- chi miei,
vi -- ta di que -- sto cor,
di que -- sto cor;
ven -- ga, e in un dol -- ce son -- no,
se te mi -- rar non pon -- no,
ben mi -- o,
mi chiu -- da i lu -- mi a -- mor,
ven -- ga,
ven -- ga, e in un dol -- ce son -- no,
mi chiu -- da i lu -- mi a -- mor.
