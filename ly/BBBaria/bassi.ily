\clef "bass" sol4 r r |
si(_\markup\italic dol. la sol) |
R2. |
re4( fad sol) |
do' do do' |
si si, si |
sol r sol, |
re re, r |
sol-\sug\p r r |
si( la sol) |
R2. |
re4( fad sol) |
do'2. |
si |
si,4-\sug\p do dod |
re re re, |
fad fad fad |
sol8 r sol4(\rf fad) |
mi dod la, |
re2.:8\p |
re: |
re: |
re2.\fermata |
re,2 r4 |
R2. |
mi2\rf la4 |
R2. |
re2\rf sol4 |
R2. |
re4\p fad sol |
do'2. |
si |
si8 do' re'4 re |
sol r r |
re4( sol) r |
re( sol) si, |
do2 r4 |
si,2\fermata\fp r4 |
do4 re re, |
sol, sol\f sol |
la do'( si) |
do' re' re |
sol4. sol8( si la) |
