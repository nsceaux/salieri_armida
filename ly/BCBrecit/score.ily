\score {
  \new ChoirStaff <<
    \new Staff \with {
      instrumentName = \markup\character Ubaldo
      shortInstrumentName = \markup\character Ub.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassoInstr } <<
      \global \keepWithTag #'tutti \includeNotes "bassi"
      \origLayout {
        s1*2\break s1*3\break s1*2\break s1*2\break s1*3\pageBreak
        s1*3\break s1*2\break s1*3\break s1*3\break s1*2\pageBreak
        s1*2\break s1*2 s2 \bar "" \break s2 s1*2\break s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
