\clef "tenor/G_8" r8 sol16 sol do'8 do'16 mi' do'8 do' r sol16 sol |
sol4 fa8 sol mi mi r16 mi sol fa |
sol8 sol r sol16 la sib4 do'8 re' |
sib4 r8 sol16 la sib8 sib r sib16 do' |
la8 la r4 r r16 fa la sol |
la8 la r la16 sib do'4 re'8 mib' |
re'8 re' r4 la8 la16 la la8 la |
fad8 fad r fad16 sol la4 la8 sib |
do'8 do' r re' re' la r16 la la sib |
do'8. do'16 do'8 sib sol sol r4 |
r2 sib4 r8 sib |
mib'4 mib'8 re' mib' mib' r sib16 sib |
sib4 lab8 sib sol4 r16 sol sib lab |
\ficta sib8 sib r16 sib sib do' reb'8 reb' r mib' |
do' do' r16 do' do' mib' do'8 do' r do' |
do' do' sib do' lab lab r16 \ficta mib lab sol |
lab8 lab r lab16 sib do'4 do'8 reb' |
sib sib r4 do'16 sol sol8 r do' |
lab4 lab lab8 lab16 lab lab8 sib |
do' do' r \ficta mib' do' do' do' reb' |
reb' lab r4 r2 |
reb'4 lab8 lab fab fab r lab16 solb |
fab4 mib8 fab reb reb r4 |
dod'4 dod'8 re' re' sold r16 si si dod' |
re'8 si r dod' la la r mi |
la8 la r16 la la si dod' dod' dod'8 r16 dod' dod' mi' |
\ficta dod'8 dod' r dod'16 dod' dod'4 si8 dod' |
la8 la r16 la dod' la re'8 re' r re'16 re' |
re'4 do'!8 re' si4 r8 sol |
dod'8 dod' r re' re' la r4 |
r8 si sold si si mi r mi |
si si si mi' mi' si r si16 si |
si4 la8 si sold sold r16 sold sold la |
si8 si r do'! re' re' re' re'16 do' |
la8 la r16 la la si do'8 do' r do'16 do' |
mi'4 re'8 mi' do'4 la8 la |
r4 la8 si dod' dod' r la16 la |
la8 mi r fad sol4 r8 sol |
dod' dod' r mi' mi' sol r16 sol sol la |
fa4 r r2 |
r4 re'8 re'16 mi' dod'8 dod' r re' |
re' la
