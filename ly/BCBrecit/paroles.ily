Co -- sì mol -- le, e ri -- den -- te
è il sen -- tier del -- le col -- pe, e l’al -- ma al -- let -- ta
per a -- ge -- vol pen -- dìo
a i -- nol -- trar -- si, e smar -- rir -- si, e se pur tar -- di
dell’ in -- gan -- no s’av -- ve -- de,
tro -- va, in ri -- trar -- ne il pie -- de
del -- la piag -- gia fio -- ri -- ta,
pe -- no -- sa, in -- es -- tri -- ca -- bi -- le l’es -- ci -- ta.
Qui del gio -- vin Ri -- nal -- do
è la dol -- ce pri -- gion; co -- sì lo tie -- ne
fra le ca -- te -- ne Ar -- mi -- da, ed ei non sen -- te
il pe -- so de’ suoi lac -- ci, e in o -- zio im -- bel -- le
in o -- blio di se stes -- so… Ec -- co -- lo. Oh stel -- le!

Ec -- co -- lo in grem -- bo a’ fio -- ri
che pla -- ci -- do ri -- po -- sa. Ah, scon -- si -- glia -- to!
Che le -- tar -- go fu -- ne -- sto! Or -- ri -- do a -- bis -- so
gli si spa -- lan -- ca al pie -- de, e men -- tre in -- tor -- no
ve -- glia -- no al -- la sua pre -- da
mil -- le mo -- stri d’A -- ver -- no in va -- rie for -- me,
sul -- la spon -- da fa -- tal ri -- po -- sa, e dor -- me!
Si scuo -- ta al -- fi -- ne. Al guar -- do suo ri -- splen -- da

Ques -- to lu -- ci -- do spec -- chio, al di cui lam -- po
non reg -- ge om -- bra d’in -- gan -- no; i mol -- li fre -- gi
del -- la sua schia -- vi -- tù veg -- ga e il suo sta -- to
pen -- ti -- men -- to, ros -- sor, dis -- pet -- to, ed i -- ra
gli sve -- gli in sen.

Sor -- gi, Ri -- nal -- do, e mi -- ra.
