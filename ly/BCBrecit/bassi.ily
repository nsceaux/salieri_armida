\clef "bass" mi!1\repeatTie~ |
mi~ |
mi~ |
mi |
fa~ |
fa |
fad~ |
fad~ |
fad~ |
fad2 sol8. sol16 sol8. sol16 |
fa!2 mib~ |
\once\tieDashed mib1~ |
mib~ |
mib |
lab~ |
lab~ |
lab |
r4 r8 mi! mi2 |
fa1 |
mib1 |
reb4 r8. reb'16 reb'8. lab16 lab8. fab16 |
reb1~ |
reb |
dod2 si,!~ |
si, dod~ |
dod1~ |
dod~ |
dod2 fad~ |
fad2 sol~ |
sol r4 la |
sold1~ |
sold~ |
\once\tieDashed sold~ |
\ficta sold |
la1~ |
la~ |
la2 \once\tieDashed sol!~ |
sol1~ |
sol2 dod |
re4 r8. re16\f re4 r8. re16 |
mi8-. fa-. sol-. la-. sib4 r |
r
