\piecePartSpecs
#`((bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{

\markup\fill-line {
  \column {
    \livretVerse#12 { Così molle, e ridente }
    \livretVerse#12 { E' il sentier delle colpe, e l'alma alletta }
    \livretVerse#12 { Per agevol pendìo }
    \livretVerse#12 { Rivoltarsi, e smarirsi, e se pur tardi }
    \livretVerse#12 { Dell inganno s'avvede, }
    \livretVerse#12 { Trova, in ritrarre il piede }
    \livretVerse#12 { Dalla piaggia fiorita, }
    \livretVerse#12 { Penosa, inestricabile l'escita. }
    \livretVerse#12 { Qui del giovin Rinaldo }
    \livretVerse#12 { E' la dolce prigion; così lo tiene }
    \livretVerse#12 { Fralle catene Armida, ed ei non sente }
    \livretVerse#12 { Il peso de' suoi lacci, e in ozio imbelle }
    \livretVerse#12 { In oblio di se stello… Eccolo. Oh stelle! }
  }
  \null
  \column {
    \livretVerse#12 { Eccolo in grembo à fiori }
    \livretVerse#12 { Che placido riposa. Ah sconsigliato! }
    \livretVerse#12 { Che letargo funesto! Orrido abisso }
    \livretVerse#12 { Gli si spalanca al piede, e mentre intorno }
    \livretVerse#12 { Vegliano alla sua preda }
    \livretVerse#12 { Mille mostri d'Averno in varie forme, }
    \livretVerse#12 { Sulla sponda fatal riposa, e dorme! }
    \livretVerse#12 { Si scuota al fine. Al guardo suo risplenda }
    \livretVerse#12 { Questo lucido specchio, al di cui lampo }
    \livretVerse#12 { Non regge ombra d'inganno; i molli fregi }
    \livretVerse#12 { Della sua schiavitù vegga e il suo stato }
    \livretVerse#12 { Pentimento, rossor, dispetto, ed ira }
    \livretVerse#12 { Gli svegli in sen. Scorgi, Rinaldo, e mira. }
  }
  \null
} #}))
