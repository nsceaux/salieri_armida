\score {
  <<
    \new Staff \with {
      \remove "Page_turn_engraver"
      \tinyStaff
      \haraKiriFirst
    } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse { \set fontSize = #-2 \includeLyrics "paroles" }
    \new GrandStaff \with { instrumentName = $(*instrument-name*) \haraKiriFirst } <<
      \new Staff <<
        \keepWithTag #(*tag-global*) \global
        \keepWithTag #'primo-secondo \includeNotes #(*note-filename*)
        \clef #(*clef*)
        $(or (*score-extra-music*) (make-music 'Music))
      >>
      \new Staff <<
        \keepWithTag #(*tag-global*) \global
        \keepWithTag #'terzo \includeNotes #(*note-filename*)
        \clef "bass"
      >>
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
