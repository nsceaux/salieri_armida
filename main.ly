\version "2.19.80"
\include "common.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Armida"
    date = "1771"
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
%% Livret
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO PRIMO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "ATTO PRIMO"
  %% 1-1
  \pieceToc "Sinfonia"
  \includeScore "AAAsinfonia"
}
\bookpart {
  \paper { systems-per-page = 1 }
  \scene "Scena I" "Scena I"
  \sceneDescription\markup\justify {
    [Parco delizioso, che s’apre in vari Viali ombrosi, in fondo à quali
    si vede in lontananza il magnifico ingresso dell’ incantato Palazzo
    d’Armida.
    
    In mezzo al gran Parco, alle fiorite sponde d’uno spazioso lago sono
    imbandite delle ricche mense e stanno scherzando leggiadre donzelle,
    intrecciando a una lieta danza il seguente.]
  }
  %% 1-2
  \pieceToc\markup\wordwrap {
    Coro: \italic { Sparso di pure brine }
  }
  \includeScore "AABcoro"
}
\bookpart {
  %% 1-3
  \pieceToc "Ballo"
  \includeScore "AACballo"
}
\bookpart {
  \paper { page-count = 3 }
  %% 1-4
  \pieceToc "Gavotta"
  \includeScore "AADgavotta"
}
\bookpart {
  %% 1-5
  \pieceToc\markup\wordwrap {
    Coro: \italic { Donzelle semplici }
  }
  \includeScore "AAEcoro"
}
\bookpart {
  \scene "Scena II" "Scena II"
  \sceneDescription\markup\wordwrap-center {
    [Ismene frettolosa, e affannata, e dette.]
  }
  %% 1-6
  \pieceToc\markup\wordwrap {
    Ismene: \italic { Ah disendete, amiche }
  }
  \includeScore "ABArecit"
}
\bookpart {
  %% 1-7
  \pieceToc\markup\wordwrap {
    Coro: \italic { Ah fra la nera }
  }
  \includeScore "ABBcoro"
}
\bookpart {
  %% 1-8
  \pieceToc\markup\wordwrap {
    Ismene: \italic { In si crudel dubbiezza ah non perdiamo }
  }
  \includeScore "ABCrecit"
}
\bookpart {
  \paper { systems-per-page = 1 }
  %% 1-9
  \pieceToc\markup\wordwrap {
    Coro: \italic { Sparso di pure brine }
  }
  \includeScore "ABDcoro"
}
\bookpart {
  \scene "Scena III" "Scena III"
  \sceneDescription\markup\wordwrap-center { [Ubaldo e dette.] }
  %% 1-10
  \pieceToc\markup\wordwrap {
    Ubaldo, Ismene: \italic { Ecco l’onda infidiosa }
  }
  \includeScore "ACArecit"
}
\bookpart {
  %% 1-11
  \pieceToc\markup\wordwrap {
    Coro: \italic { Vieni al fonte del contento }
  }
  \includeScore "ACBcoro"
  %% 1-12
  \pieceToc\markup\wordwrap {
    Ubaldo, Ismene, Coro: \italic { Che periglioso assalto! }
  }
  \includeScore "ACCrecit"
  %% 1-13
  \pieceToc\markup\wordwrap {
    Ismene: \italic { Mostri i più crudi, e infesti }
  }
  \includeScore "ACDaria"
}
\bookpart {
  %% 1-14
  \pieceToc\markup\wordwrap {
    Coro di Demoni: \italic { Qual è la man che scuote }
  }
  \includeScore "ACEcoro"
}
\bookpart {
  %% 1-15
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Quanto fragili, e vane, immondi spirti }
  }
  \includeScore "ACFrecit"
}
\bookpart {
  %% 1-16
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Tornate al nero abisso }
  }
  \includeScore "ACGaria"
}
\bookpart {
  %% 1-17
  \pieceToc\markup\wordwrap {
    Coro di Demoni: \italic { Qual sibilo orrendo }
  }
  \includeScore "ACHcoro"
}
\bookpart {
  %% 1-18
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Eterno Proveder, tu che guidasti }
  }
  \includeScore "ACIrecit"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-19
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Finta larva, d’Abisso fra l’ombra }
  }
  \includeScore "ACJaria"
  \actEnd "Fine dell’ Atto primo."
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO SECONDO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "ATTO SECONDO"
  \scene "Scena I" "Scena I"
  \sceneDescription\markup\column {
    \justify {
      [Spazioso ameno Giardino in fondo al quale si vedono i tortuosi
      intricati portici del Laberinto, cho le circonda.]
    }
    \wordwrap-center {
      [Armida e Rinaldo assisi sull’erba, e intessendo ghirlande.]
    }
  }
  %% 2-1
  \pieceToc\markup\wordwrap {
    Armida, Rinaldo: \italic { Qui’l regno è del contento }
  }
  \includeScore "BAAduo"
}
\bookpart {
  \paper { page-count = 3 }
  %% 2-2
  \pieceToc\markup\wordwrap {
    Armida, Rinaldo: \italic { Addio. Già m'abbandoni? }
  }
  \includeScore "BABrecit"
}
\bookpart {
  %% 2-3
  \pieceToc\markup\wordwrap {
    Armida: \italic { Tremo, bell' idol mio }
  }
  \includeScore "BACaria"
}

\bookpart {
  \scene "Scena II" "Scena II"
  \sceneDescription\markup\wordwrap-center {
    [Rinaldo solo.]
  }
  %% 2-4
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { E non deggio seguirla? }
  }
  \includeScore "BBArecit"
}
\bookpart {
  %% 2-5
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Lungi da te, ben mio }
  }
  \includeScore "BBBaria"
}
\bookpart {
  %% 2-6
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Forse, chi sà? Verranno }
  }
  \includeScore "BBCrecit"
}
\bookpart {
  %% 2-7
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Vieni a me sull’ali d’oro }
  }
  \includeScore "BBDaria"
}
\bookpart {
  %% 2-8
  \pieceToc "Ballo"
  \includeScore "BBEballo"
}
\bookpart {
  %% 2-9
  \pieceToc "[Sinfonia]"
  \includeScore "BBFballo"
}
\bookpart {
  \scene "Scena III" "Scena III"
  \sceneDescription\markup\column {
    \wordwrap-center { [Ubaldo e detto.] }
    \justify {
      [Al finir della breve dolcissima sinfonia, si sente un confuso
      strepito in lontananza, e si vede entrar Ubaldo di fondo fra
      portici, che circondano il Giardino e spariscono le larve.]
    }
  }
  %% 2-10
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Oh come in un momento }
  }
  \includeScore "BCAaria"
}
\bookpart {
  %% 2-11
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Così molle e ridente }
  }
  \includeScore "BCBrecit"
}
\bookpart {
  %% 2-12
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Misero! chi mi scuote? }
  }
  \includeScore "BCCrecit"
}
\bookpart {
  \scene "Scena IV" "Scena IV"
  \sceneDescription\markup\wordwrap-center { [Armida affannata e detto.] }
  %% 2-13
  \pieceToc\markup\wordwrap {
    Armida, Rinaldo: \italic { Soccorso, o Dei! }
  }
  \includeScore "BDAduo"
}
\bookpart {
  %% 2-14
  \pieceToc\markup\wordwrap {
    Rinaldo, Armida: \italic { Questo arcano funesto }
  }
  \includeScore "BDBrecit"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 2-15
  \pieceToc\markup\wordwrap {
    Armida, Rinaldo: \italic { Dilegua il tuo timore }
  }
  \includeScore "BDCduo"
}
\bookpart {
  %% 2-16
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Ora si ch’io mi perdo }
  }
  \includeScore "BDDrecit"
}
\bookpart {
  \scene "Scena V" "Scena V"
  \sceneDescription\markup\wordwrap-center { [Ubaldo e detto.] }
  %% 2-17
  \pieceToc\markup\wordwrap {
    Ubaldo, Rinaldo: \italic { Fermati, incauto }
  }
  \includeScore "BEArecit"
}
\bookpart {
  %% 2-18
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Vedo l’abisso orrendo }
  }
  \includeScore "BEBaria"
  \actEnd "Fine dell’ Atto secondo."
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% ATTO TERZO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bookpart {
  \act "ATTO TERZO"
  \scene "Scena I" "Scena I"
  \sceneDescription\markup\justify {
    [Spazioso sotterraneo auso d’incanti con Ara e Tripode Le seguaci
    d’Armida cinte di nere bende stanno amministrando con una danza grave
    e solenne intorno all’ara un Sacrifizio agli Dei infernali.
    Armida assisa sul tripode, e tenendo in mano la magica verga vi assiste,
    alzandosi crucciosa al finir del seguente.]
  }
  %% 3-1
  \pieceToc\markup\wordwrap {
    Coro, Armida: \italic { Chi sorde vi rende }
  }
  \includeScore "CAAcoro"
}
\bookpart {
  %% 3-2
  \pieceToc\markup\wordwrap {
    Armida: \italic { Misera! Il Ciel m’opprime }
  }
  \includeScore "CABrecit"
}
\bookpart {
  \paper { page-count = 4 }
  \scene "Scena II" "Scena II"
  \sceneDescription\markup\justify {
    [Ismene affannata, e frettolosa con alcune delle Seguaci,
    e detta.]
  }
  %% 3-3
  \pieceToc\markup\wordwrap {
    Ismene, Armida: \italic { Corri, Regina }
  }
  \includeScore "CBArecit"
}
\bookpart {
  %% 3-4
  \pieceToc\markup\wordwrap {
    Armida, coro: \italic { Ah mi tolga almen la vita }
  }
  \includeScore "CBBaria"
}
\bookpart {
  \scene "Scena III" "Scena III"
  \sceneDescription\markup\justify {
    [Ismene col coro delle Seguaci d’Armida.]
  }
  %% 3-5
  \pieceToc\markup\wordwrap {
    Ismene: \italic { Ove corre, infelice! }
  }
  \includeScore "CCArecit"
}
\bookpart {
  \paper { page-count = 2 }
  %% 3-6
  \pieceToc\markup\wordwrap {
    Coro, Ismene: \italic { Ah misera regina }
  }
  \includeScore "CCBcoro"
}
\bookpart {
  \paper { page-count = 12 }
  %% 3-7
  \pieceToc\markup\wordwrap {
    Ismene, coro: \italic { Schernita, depressa }
  }
  \includeScore "CCCaria"
}

\bookpart {
  \scene "Scena IV" "Scena IV"
  \sceneDescription\markup\column {
    \justify {
      [Spiaggia di mare, a cui si giunge dalle scoscese balze d’un alpestre
      montagna, le di cui falde formano in lontananza un piccol seno, dove
      è la navicella della Fortuna pronta alla vela.]
    }
    \wordwrap-center { [Ubaldo, e Rinaldo.] }
  }
  %% 3-8
  \pieceToc\markup\wordwrap {
    Ubaldo, Rinaldo: \italic { Siam quasi in salvo, eccoci al lido. }
  }
  \includeScore "CDArecit"
}
\bookpart {
  %% 3-9
  \pieceToc\markup\wordwrap {
    Ubaldo: \italic { Torna schiavo infelice }
  }
  \includeScore "CDBaria"
}
\bookpart {
  %% 3-10
  \pieceToc\markup\wordwrap {
    Rinaldo: \italic { Ah non lasciarmi no }
  }
  \includeScore "CDCaria"
}
\bookpart {
  %% 3-11
  \pieceToc\markup\wordwrap {
    Rinaldo, Ubaldo: \italic { Misera! È già vicina }
  }
  \includeScore "CDDrecit"
}

\bookpart {
  \scene "Scena V" "Scena V"
  \sceneDescription\markup\wordwrap-center { [Armida affannata e detti.] }
  %% 3-12
  \pieceToc\markup\wordwrap {
    Armida, Ubaldo, Rinaldo: \italic { Fermati, aspetta }
  }
  \includeScore "CEArecit"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 3-13
  \pieceToc\markup\wordwrap {
    Armida, Rinaldo, Ubaldo: \italic { Strappami il cuor dal seno }
  }
  \includeScore "CEBterzetto"
}

\bookpart {
  \scene "Scena VI" "Scena VI"
  \sceneDescription\markup\wordwrap-center {
    [Armida svenuta, Ismene, e il Coro delle seguaci.]
  }
  %% 3-14
  \pieceToc\markup\wordwrap {
    Ismene: \italic { Ecco la sventurata }
  }
  \includeScore "CFArecit"
  %% 3-15
  \pieceToc\markup\wordwrap {
    Coro delle donne: \italic { Ah barbaro, ah senti }
  }
  \includeScore "CFBcoro"
}
\bookpart {
  %% 3-16
  \pieceToc\markup\wordwrap {
    Ismene, Armida: \italic { Infelice Regina! Ancor respira }
  }
  \includeScore "CFCrecit"
}
\bookpart {
  %% 3-17
  \pieceToc\markup\wordwrap {
    Armida, coro: \italic { Io con voi la nera face }
  }
  \includeScore "CFDaria"
  \actEnd "[Fine dell Dramma.]"
}
