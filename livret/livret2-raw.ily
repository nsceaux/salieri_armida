\livretAct ATTO SECONDO
\livretScene SCENA I
\livretDescAtt\column {
  \justify {
    Spazioso ameno Giardino in fondo al quale si vedono i tortuosi
    intricati portici del Laberinto, cho le circonda.
  }
  \justify {
    Armida e Rinaldo assisi sull’erba, e intessendo ghirlande.
  }
}
\livretPers A 2
\livretRef#'BAAduo
%# Qui’l regno è del contento;
%# La sede del piacer.
\livretPers Rinaldo
%# Fresch’ ombre, e verdi sponde,
%# Cui bagna un rio d’argento
%# C’invitano a goder.
%# Par che la terra e l’onde
%# Spirino un dolce ardore,
%# Sembra che fin d’amor
%# Mormori il vento.
\livretPers A 2
%# Qui’l regno è del contento;
%# La sede del piacer.
\livretPers Armida
%# Folle chi della vita
%# Passa il breve momento
%# In torbidi pensier.
%# Che val l’età fiorita,
%# Che val ricchezza ed or,
%# Se cambia un van timor.
%# Tutto in tormento.
\livretPers A 2
%# Prezioso è il tempo, e lieve,
%# Facciamone tesor.
%# La vita è un cammin breve
%# Spargiamolo di fior.
\livretPersDidas Armida alzandosi di sul prato in atto di partire.
\livretRef#'BABrecit
%# Addio.
\livretPersDidas Rinaldo arrestendola affettuosamente
%# Già m’abbandoni?
\livretPers Armida
%# Ah questa è l’ora
%# Che da te lungi, o caro,
%# Mi richiama ogni dì.
\livretPers Rinaldo
%# Ma qual ti sforza
%# A rapire ogni giorno
%# Tanti dolci momenti al nostro amore
%# Cruda, barbara legge?
\livretPers Armida
%# Il mio timore.
\livretPers Rinaldo
%# Timor? Di che?
\livretPers Armida
%# Del tuo, del mio riposo.
\livretPers Rinaldo
%# E chi potria turbarlo in questa, o cara,
%# Separata dal mondo ignota sponda?
\livretPers Armida
%# Numi! Il guardo del sole, i venti, l’onda…
%# Ah tutto a chi ben ama
%# È cagion di timor. Per nostro asilo
%# Quest’ isola felice io scelsi invano
%# In grembo all’ Oceano. Invan le cinsi
%# Di fosca nebbia il piede, e i fianchi, e il tergo
%# Di dirupate orride balze, aggiunsi
%# Folta guardia di mostri, e d’insidiose
%# Sirene allettatrici! I frutti, i fonti
%# Di tosco aspersi, e quanto miri in lei
%# E gli augelli, e le piante, e l’onda, e il vento
%# Tutto è in nostra difesa, eppur pavento.
%# Ah dal dì ch’io cangiai
%# Perte, bell’idol mio, gli affetti miei;
%# Patria, regno, tesor, tutto perdei;
%# Pensa, che s’io ti perdo,
%# Fuor che il rossor della mia fé tradita,
%# Nulla mi resta.
\livretPers Rinaldo
%# Ah non temer, mia vita.
%# Sai che la mia tu sei,
%# Come io son l’alma tua; che non poss’io
%# Viver da te diviso un sol momento;
%# Sai che ogni mio contento,
%# Ogni mia speme è in te, ch’ altro non bramo,
%# Che col tuo dolce nome il fiato estremo
%# Spirar frà labbri tuoi.
\livretPers Armida
%# Lo so: ma tremo.
\livretRef#'BACaria
%#~ Tremo, bell’ idol mio,
%#~ Ma questo mio timor
%#~ Non è tormento.
%#~ É vita dell’ amor
%#~ E stimolo a goder,
%#~ Per lui tutto il piacer
%#~ Di possederti, oh Dio,
%#~ Tutto risento.
%#~ Langue nel sen l’ardor,
%#~ Langue il desio,
%#~ Quando a temer non s’hà,
%#~ E troppa sicurtà
%#~ Non è contento.
\livretDidasP\justify { Parte. }

\livretScene SCENA II
\livretDescAtt\wordwrap-center { Rinaldo solo. }
\livretPers Rinaldo
\livretRef#'BBArecit
%# E non deggio seguirla? Ah, senza Armida
%# Son secoli gl’istanti. A che mi giova
%# Il ridente soggiorno, e dove or sono
%# Tante varie bellezze, onde l’adorna
%# La prodiga natura agli occhi miei?
%# Ah che vicino a lei
%# Tutto è lieto, e giocondo,
%# Ride il cielo, ride il mondo,
%# Ma copre un fosco velo,
%# Se s’allontana Armida, e terra, e cielo,
%# E diventa per me da lei diviso
%# Un deserto d’orror l’istesso Eliso.
\livretRef#'BBBaria
%#~ Lungi da te, ben mio,
%#~ Se viver non poss’ io,
%#~ Lungi da te, che sei
%#~ Luce degli occhi miei,
%#~ Vita di questo cor;
%#~ Venga, e in un dolce sonno,
%#~ Se te mirar non ponno,
%#~ Mi chiuda i lumi amor.
\livretDidasP\justify {
  Rimettendosi a seder sull’erba.
}
\livretRef#'BBCrecit
%# Forse, chi sa? Verranno
%# Con un leggiadro inganno
%# In sembianza d’Armida i lieti sogni
%# A lusingar mia sorte
%# In questa dolce immagine di morte.
%# Oh inganno fortunato,
%# Che le più care idee finga al pensiero,
%# E da un finto piacer lo chiami al vero!
\livretRef#'BBDaria
%#~ Vieni a me sull’ ali d’oro
%#~ Lusinghier sogno amoroso,
%#~ Ingannando il mio riposo
%#~ In sembianza del mio ben.
%#~ Trovi in te per pochi istanti
%#~ Il mio cor qualche ristoro,
%#~ Finche amor del mio tesoro
%#~ Faccia poi svegliarmi in sen.
\livretDidasP\justify {
  S’addormenta e al suono d’una soave sinfonia escono da vari luoghi
  leggiadri fantasmi a uso di piaceri, ballando intorno a Rinaldo.
}

\livretScene SCENA III
\livretDescAtt\column {
  \wordwrap-center { Ubaldo e detto. }
  \justify {
    Al finir della breve dolcissima sinfonia, si sente un confuso
    strepito in lontananza, e si vede entrar Ubaldo di fondo fra
    portici, che circondano il Giardino e spariscono le larve.
  }
}
\livretPers Ubaldo
\livretRef#'BCAaria
%# Oh come in un momento
%#~ Dell’ incantata mole
%#~ Tutto l’orror spari,
%# Qual nube in faccia al vento,
%#~ Qual fosca nebbia suole
%#~ A’ caldi rai del dì.
\livretRef#'BCBrecit
%# Così molle, è ridente
%# E’ il sentier delle colpe, e l’alma alletta
%# Per agevol pendìo
%# A inoltrarsi, e smarrirsi, e se pur tardi
%# Dell’ inganno s’avvede,
%# Trova, in ritrarne il piede
%# Della piaggia fiorita,
%# Penosa, inestricabile l’escita.
%# Qui del giovin Rinaldo
%# È la dolce prigion; così lo tiene
%# Fra le catene Armida, ed ei non sente
%# Il peso de’ suoi lacci, e in ozio imbelle
%# In oblio di se stesso… Eccolo. Oh stelle!
\livretDidasP\justify { Accorgendosi con sorpresa di Rinaldo che dorme. }
%# Eccolo in grembo a’ fiori
%# Che placido riposa. Ah, sconsigliato!
%# Che letargo funesto! Orrido abisso
%# Gli si spalanca al piede, e mentre intorno
%# Vegliano alla sua preda
%# Mille mostri d’Averno in varie forme,
%# Sulla sponda fatal riposa, e dorme!
%# Si scuota alfine. Al guardo suo risplenda
\livretDidasP\justify {
  Scuopre da un velo il lucido scudo, e l’appende ad un ramo.
}
%# Questo lucido specchio, al di cui lampo
%# Non regge ombra d’inganno; i molli fregi
%# Della sua schiavitù vegga e il suo stato
%# Pentimento, rossor, dispetto, ed ira
%#- Gli svegli in sen.
\livretDidasP\justify {
  La scuote con forza e si ritira in disparte ad osservarlo.
}
%#= Sorgi, Rinaldo, e mira.
\livretPers Rinaldo
\livretRef#'BCCrecit
%# Misero! chi mi scuote? E quali in questo
%# Breve sonno affannoso
%# Turbano idée funeste il mio riposo!
%# Oh morte! orribil larva! Agli occhi miei
%# Qual poter ti presenta? e come appresi
%# A temerne l’aspetto? Un freddo gelo
%# Mi sparge in seno, i falli a me rinfaccia,
%# E il ferro micidial vibra, e minaccia.
%# Quale insolito orror! quai nuovi sensi
%# M’agitan l’alma… e quale
%# Mi ferisce lo sguardo
%#- Improvviso baglior?
\livretDidasP\justify { Alzandosi spaventato. }
%#= l’arme lucenti
%# Chi recò? come? quando? e in essa… Oh Dio!
%# Quanto da me diverso!…
%# Mi riconosco appena. A questo segno
%# Avvilirmi potei
%# Trasformarmi così?

\livretScene SCENA IV
\livretDescAtt\wordwrap-center {
  Armida affannata e detto.
}
\livretPers Armida
\livretRef#'BDAduo
%# Soccorso, o Dei!
%#~ Ahimè! son tradita,
%#~ Mi palpita il core,
%#~ Soccorso, pietà.
\livretPers Rinaldo
%#~ Che dici? ah mia vita,
%#~ Qual nuovo terrore
%#~ Tremare ti fá?
\livretPers Armida
%#~ M’opprime l’affanno.
\livretPers Rinaldo
%#~ Ah palpito anch’io.
\livretPers Armida
%#~ Che dubbio tiranno!
\livretPers Rinaldo
%#~ Ma spiegati.
\livretPers Armida
%#~ Oh Dio!
\livretPers Rinaldo
%#~ Ma parla.
\livretPers Armida
%#~ Non só.
\livretPers A 2
%#~ In tanto periglio
%#~ Tal velo ho sul ciglio,
%#~ Che ben non comprendo
%#~ Che parlo, che fó.
\livretPers Rinaldo
\livretRef#'BDBrecit
%# Questo arcano funesto
%# Spiegami per pietà, del tuo spavento
%# Dimmi almen la cagion;
%# Determina, se puoi
%# I miei sospetti in rivelando i tuoi.
\livretPers Armida
%# Ah siam perduti. Uno straniero ignoto
%# Nell’ isola approdò. Lo spazio immenso
%# Del periglioso mar, che ci divide
%# Dal resto de’ viventi,
%# Varcò sicuro, e i fieri mostri, e il giogo
%# Dirupato del colle, e il dolce incanto
%# Delle ninfe lascive, e fin del chiuso
%# Intricato recinto
%# Il fier custode in un sol giorno ha vinto.
\livretPers Rinaldo
%# E non sapesti ancora
%# Come qui giunse, e donde,
%# A che venne, chi sia, dove s’asconde?
\livretPers Armida
%# Questa è de’miei spaventi
%# La più fiera cagion. Poter maggiore
%# Del mio poter lo guida, e rende vane
%# Tutte le mie ricerche. Ah, già col piede
%# Premo l’ampia ruina
%# Dell’incendio crudel, che tutto intorno
%# Strugge, abbatte, divora;
%# E la fiamma crudel non scopro ancora.
\livretPers Rinaldo
%# E temi?…
\livretPers Armida
%# E che potrei
%# Altro temer, ben mio,
%# Dall’ irata del Ciel vindice mano,
%# Che di perderti, oh Dio?
\livretPers Rinaldo
%# Paventi invano.
%# Ah questi molli fregi, onde ti piacque
%# Avvilirmi così, non han sopita
%# Tutta la mia virtù, mi pende al fianco
%# Non vil l’acciaro, e inerme ancor saprei,
%# Non che ignoto guerriero,
%# Sfidare in tua difesa il mondo intero.
\livretRef#'BDCduo
%#~ Dilegua il tuo timore,
%#~ Serena i mesti rai;
%#~ Sai ch’io t’adoro, e sai,
%#~ Ch’io morirò per te.
\livretPers Armida
%#~ Taci, che accresci al core
%#~ Il suo mortale affanno.
%#~ L’ira del Ciel tiranno
%#~ Tutta si sfoghi in me.
\livretPers Rinaldo
%#~ Mio ben.
\livretPers Armida
%#~ Mio dolce amore,
\livretPers A 2
%#~ Che barbaro momento,
\livretPers Rinaldo
%#~ Io tremo al tuo terrore
\livretPers Armida
%#~ L’alma mancar mi sento
\livretPers A 2
%#~ Ne intendo il mio spavento
%#~ Ne posso dir perché.
\livretPers Rinaldo
%#~ Ma il mio soccorso?
\livretPers Armida
%#~ È vano.
\livretPers Rinaldo
%#~ L’amore…
\livretPers Armida
%#~ È mio periglio.
\livretPers Rinaldo
%#~ Il Cielo…
\livretPers Armida
%#~ Ah de’ suoi fulmini
%#~ Già balenar sul ciglio
%#~ Mi vedo acceso il lampo.
\livretPers Rinaldo
%#~ No mira al nostro scampo
%#~ Qual’arme il Ciel mi diè;
\livretDidasP\justify { Stacca lo scudo e l’imbraccia. }
%#~ Vedi
\livretDidasP\justify { Lo presenta ad Armida. }
\livretPers Armida
%#~ Oh stelle! che luce funesta!
\livretDidasP\justify { Ritirandosi spaventata. }
%#~ Fuggi ascondi…
\livretPers Rinaldo
%#~ Ma senti, t’arresta
\livretDidasP\justify { Confuso in Atto di trattenerla. }
\livretPers A 2
%#~ Ah, che strana vicenda è mai questa,
%#~ No più orribil la morte non è.
\livretDidasP\justify { Fugge spaventata. }
\livretPers Rinaldo
\livretRef#'BDDrecit
%# Ora sì ch’io mi perdo. Ah chi le ispira
%# Questo nuovo terror? Teme il periglio,
%# E aborrisce l’ajuto! Il Ciel pietoso
%# M’arma per sua difesa, e sul suo capo
%# Il Cielo, se credo a lei, fulmina e tuona!
%# Di perdermi paventa, e m’abbandona!
%# Misera! Oh Dio! la rende
%# Forsennata l’affanno. In questo stato
%# Lasciarla in preda al suo crudel deliro
%# Saria…
\livretDidasP\justify { Risoluto in atto di seguire Armida. }

\livretScene SCENA V
\livretDescAtt\wordwrap-center {
  Ubaldo e detto.
}
\livretPers Ubaldo
\livretRef#'BEArecit
%# Fermati, incauto.
\livretPers Rinaldo
%# Oh Ciel! che miro?
\livretPers Ubaldo
%# Ah Rinaldo Rinaldo,
%# Dove fuggi, che fai? Così del Cielo,
%# Che suo campion t’elesse
%# A difender la Fede, a strugger gli empi
%# Gli alti disegni, e i vaticini adempi?
%# Così da te Gerusalemme aspetta
%# Libertade, e vendetta? Ah chi ti rende
%# Così da te diverso,
%# Sì vile agli occhi miei? Beltà fallace,
%# Che ti guida a perir; che ti prepara
%# Un laccio ad ogni passo,
%# Un angue in ogni fior. Folle! e non vedi,
%# Che quanto in lei di lusinghier t’apparve
%# Son d’Averno, a sedurti, inganni, e larve?
%# Torna, torna in te stesso
%# Sconsigliato che sei. Cogli il momento,
%# Ché da te fugge intimorita al lampo
%# Di quest’ arme fatal. Fuggi, deludi
%# Le lusinghe fallaci.
\livretPers Rinaldo
%# Misero me!
\livretPers Ubaldo
%# Tu non mi guardi, e taci?
%# Tu arrossisci nel volto! Ah quel rossore
%# È il color di virtù. Torna, Rinaldo,
%# Alla gloria, all’onor. T’aspetta il Campo,
%# Ti richiama Goffredo,
%# Per sentier di prodigi il Ciel ti guida,
%# L’indugio è morte.
\livretPers Rinaldo
%# E ho da lasciare Armida?
\livretPers Ubaldo
%# Ingrato! ah nel tuo core
%# Chi bilancia il tuo Dio?
\livretPers Rinaldo
%# Ma le promesse,
%# La fede, amico, i giuramenti miei?
\livretPers Ubaldo
%# Li rompi al Ciel per conservarli a lei?
%# Sai pur con quante frodi,
%# Con quant’arti costei de’ nostri Duci
%# I più prodi sedusse, e in rea catena
%# Li serbava crudel a duro fato,
%# Se tu non eri; e tu la piangi? Ingrato!
%# Trema per te. Tempo verrà che Armida,
%# Sazia del tuo piacer, l’odio funesto
%# Coll’ amor cangerà. Che te lasciando
%# Su quest’ isola ignuda, o in un’ oscura
%# Tormentosa prigion, rimasto in preda
%# A tuoi fieri rimorsi,
%# All’ orror di te stesso; a liberarti
%# Dallo strazio crudel, l’amica mano,
%# Ch’or salvarti potria, richiami in vano.
%# Misero! in tale stato
%# Oppresso, disperato,
%# Senza pietà, senza soccorso…
\livretPers Rinaldo
%# Ah taci!
%# Non inasprir la piaga,
%# Che mi lacera il cor. Se tu vedessi
%# Qua dentro amico, e quale acerba guerra
%# Vi fan gli opposti affetti, e qual mi scuote
%# Di miseria, e d’orror scena funesta.
%# Io ti farei pietà; più non distinguo
%# Chi mi parla, ove son; tremo, e confondo
%# Col periglio lo scampo; amico, oh Dio!
%# Guida, salvami tu. Fuggiam da questo
%# Insidioso recinto,
%# Mi fido a te; più non resisto.
\livretPers Ubaldo
%# Ho vinto.
\livretPers Rinaldo
\livretRef#'BEBaria
%#~ Vedo l’abisso orrendo,
%#~ Onde ritrassi il piede
%#~ Sento d’onor, di fede
%#~ Mille rimorsi al cor.
%#~ Tutto mi fa spavento
%#~ Dovunque volgo il ciglio;
%#~ Ma in faccia al mio periglio
%#~ La fiamma ancora io sento
%#~ D’un male estinto ardor.
%#~ D’un nero mar cruccioso
%#~ Tutte le insidie ho scorto.
%#~ Grazia è del Ciel pietoso
%#~ S’io non rimasi assorto,
%#~ Ma pur vicino al porto
%#~ Forse mi perdo ancor.
\livretFinAct Fine dell’ Atto secondo.
\sep
\column-break

