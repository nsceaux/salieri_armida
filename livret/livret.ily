\notesSection "Livret"
\markuplist\abs-fontsize-lines#8 \page-columns-title \act\line { LIVRET } {
\livretAct ATTO PRIMO
\livretScene SCENA I
\livretDescAtt\justify {
  Parco delizioso, che s’apre in vari Viali ombrosi, in fondo à quali
  si vede in lontananza il magnifico ingresso dell’ incantato Palazzo
  d’Armida.

  In mezzo al gran Parco, alle fiorite sponde d’uno spazioso lago sono
  imbandite delle ricche mense e stanno scherzando leggiadre donzelle,
  intrecciando a una lieta danza il seguente.
}
\livretPers Coro
\livretRef#'AABcoro
\livretVerse#12 { Sparso di pure brine }
\livretVerse#10 { All’ aure mattutine }
\livretVerse#10 { Come vermiglio un fior }
\livretVerse#10 { Spunta sul primo albor }
\livretVerse#10 { Poi langue, e muore. }
\livretVerse#12 { Passa per noi così }
\livretVerse#10 { Il fior della beltà, }
\livretVerse#10 { É dura un breve di, }
\livretVerse#10 { Se nella fresca età }
\livretVerse#10 { Nol’ coglie amore }
\livretDidasP Una parte del Coro
\livretRef#'AAEcoro
\livretVerse#12 { Donzelle semplici }
\livretVerse#10 { Non vi lagnate, }
\livretVerse#10 { Che troppo rapida }
\livretVerse#10 { Fugga l’età }
\livretVerse#12 { Se fresche e giovani }
\livretVerse#10 { Non v’affrettate }
\livretVerse#10 { Il frutto a cogliere }
\livretVerse#10 { Della Beltà. }
\livretVerse#12 { Donzelle semplici }
\livretVerse#10 { Non vi lagna… }

\livretScene SCENA II
\livretDescAtt\wordwrap-center {
  Ismene frettolosa, e affannata, e dette.
}
\livretPers Ismene
\livretRef#'ABArecit
\livretVerse#12 { Ah difendete, amiche, }
\livretVerse#12 { Il confidato passo. À noti segni }
\livretVerse#12 { Imminente è il periglio, e nel più chiuso }
\livretVerse#12 { Del custodito albergo, onde gelosa }
\livretVerse#12 { Vieta l’accesso à suoi più fidi, e dove }
\livretVerse#12 { Sola, e sicura al suo poter si fida, }
\livretVerse#12 { In mezzo à frutti suoi, minaccia Armida. }
\livretVerse#12 { Collo spuntar del sole }
\livretVerse#12 { Più non appar la tenebrosa, e folta }
\livretVerse#12 { Nebbia, che ad ogni sguardo }
\livretVerse#12 { Quest’ Isola ascondea; meste, e confuse }
\livretVerse#12 { Strida ingombrano il lido, e ignoto legno }
\livretVerse#12 { Vi si scorge in sicuro: Erranti e sparsi }
\livretVerse#12 { Vidi i Mostri custodi }
\livretVerse#12 { Fuggir per l’erta, e s’altri accorre, e chiede }
\livretVerse#12 { La cagion della fuga, un tal terrore }
\livretVerse#12 { Quella Guardia fatal turba, e confonde }
\livretVerse#12 { Che torce altrove il corso, e non risponde. }
\livretDidasP\justify {
  Alle parole d’Ismene le donzelle s’attruppano interno ad essa impaurite.
}
\livretPers Coro
\livretRef#'ABBcoro
\livretVerse#12 { Ah fra la nera }
\livretVerse#10 { Densa caligine }
\livretVerse#10 { La riva inospita }
\livretVerse#10 { Chi mai scuoprì? }
\livretVerse#12 { Ah fra la schiera }
\livretVerse#10 { Dé mostri orribili }
\livretVerse#10 { Qual Diò, qual Demone }
\livretVerse#10 { La via s’apri? }
\livretPers Ismene
\livretRef#'ABCrecit
\livretVerse#12 { In si crudel dubbiezza ah non perdiamo }
\livretVerse#12 { I momenti in querele. Un finto riso }
\livretVerse#12 { Cuopra il nostro terror. Tutto respiri }
\livretVerse#12 { Letizia, e pace; e parolette accorte }
\livretVerse#12 { E languidi sospiri, e molli sguardi }
\livretVerse#12 { Tutto si metta in opra, e tutto alletti }
\livretVerse#12 { L’incauto vincitore à sorsi infetti. }
\livretVerse#12 { Le difese preparo, e non conosco }
\livretVerse#12 { O il periglio, o il nemico. Ah se lo guida }
\livretVerse#12 { Forza maggior, di fé, di zelo almeno }
\livretVerse#12 { Si compiscan le parti }
\livretVerse#12 { E poi… Che veggio? Ecco il nemico: All’ arti. }
\livretDidasP\justify {
  Guardando con sorpresa dentro la Scena; e assieme colle donzelle
  ripigliando il ballo, e il canto.
}
\livretPers Coro
\livretRef#'ABDcoro
\livretVerse#12 { Sparso di pure brine }
\livretVerse#10 { All’ aure mattutine, }
\livretVerse#10 { Come vermiglio un fior }
\livretVerse#10 { Spunta col primo albor… oh noi meschine? }
\livretDidasP\justify {
  Esce Ubaldo, e le donzelle come impaurite fingono di ritirarsi
  altre verso il fondo altre fragli alberi.
}

\livretScene SCENA III
\livretDescAtt\wordwrap-center {
  Ubaldo e dette.
}
\livretPers Ubaldo
\livretRef#'ACArecit
\livretVerse#12 { Ecco l’onda insidiosa; ove col riso }
\livretVerse#12 { Altri beve la morte. Io la ravviso }
\livretVerse#12 { Alle fiorite sponde, }
\livretVerse#12 { À lauti cibi, alle Sirene immonde. }
\livretVerse#12 { Con quant’ arte dispose i neri inganni }
\livretVerse#12 { L’accorta Maga! }
\livretDidasP\justify {
  In aria ridente, fingendo di richiamar le compagne impaurite.
}
\livretPers Ismene
\livretVerse#12 { Ah dileguate amiche, }
\livretVerse#12 { L’importuno timor. Guerra non reca }
\livretVerse#12 { Alla tranquilla sede }
\livretVerse#12 { Questo prode guerrier. D’ogni diletto }
\livretVerse#12 { Vieni a parte con noi; vieni a deporre }
\livretVerse#12 { Quell’ inutile acciaro }
\livretVerse#12 { Fortunato stranier; dolce ristoro }
\livretVerse#12 { T’offre la mensa, e il rio. De’ fieri mostri }
\livretVerse#12 { La perigliosa guardia }
\livretVerse#12 { Più a temer non ti resta; }
\livretVerse#12 { Altra specie di pugna Amor t’appresta. }
\livretPersDidas Tutto il Coro con Ismene
\livretRef#'ACBcoro
\livretVerse#12 { Vieni al fonte del contento, }
\livretVerse#10 { Fortunato passeggier. }
\livretVerse#10 { E’ perduto ogni momento, }
\livretVerse#10 { Che si perde pel’ piacer. }
\livretPers Ismene
\livretVerse#12 { Dell’ Amor la reggia è questa, }
\livretVerse#10 { La delizia de mortali; }
\livretVerse#10 { Nell’ oblio di tutti i mali }
\livretVerse#10 { Qui si viene a riposar. }
\livretVerse#10 { Qui non v’è cura molesta, }
\livretVerse#10 { Che il piacer di tosco infetti, }
\livretVerse#10 { E il più dolce de diletti }
\livretVerse#10 { Non ti costa, che il bramar. }
\livretPers Tutto il Coro
\livretVerse#12 { Vieni al fonte del contento, }
\livretVerse#10 { Fortunato passeggier. }
\livretVerse#10 { E’ perduto ogni momento, }
\livretVerse#10 { Che si perde pel’ piacer. }
\livretPers Ubaldo
\livretRef#'ACCrecit
\livretVerse#12 { (Che periglioso assalto!) }
\livretPers Ismene
\livretVerse#12 { Al dolce invito. }
\livretVerse#12 { Perchè resisti mai? Non prega invano }
\livretVerse#12 { Donzella in fresca età. Trarrotti io stessa }
\livretVerse#12 { L’elmo lucente, e questa, }
\livretVerse#12 { Con cui distingue Armida }
\livretVerse#12 { I fidi servi suoi, fiorita insegna }
\livretVerse#12 { Al crin ti cingerò. }
\livretDidasP\justify {
  Prendendo une Ghirlanda di fiori, e in atto di levar l’elmo ad Ubaldo.
}
\livretPersDidas Ubaldo rispingendola con severità.
\livretVerse#12 { Scostati, indegna }
\livretVerse#12 { Riserba a miglior uso }
\livretVerse#12 { I finti vezzi, e le lusinghe accorte. }
\livretVerse#12 { Puoi col riso sul labro offrir la morte? }
\livretVerse#12 { So qual tosco s’asconde }
\livretVerse#12 { In que’ cibi, in quell’onde, e le temute }
\livretVerse#12 { Vane frodi d’Armida… }
\livretPersDidas Coro delle Donzelle
\livretDidasP\justify {
  (tutte impaurite scostancdosi da Ubaldo.)
}
\livretVerse#12 { Ah siam perdute. }
\livretPers Ismene
\livretVerse#12 { In mal punto ricusi, }
\livretVerse#12 { Malnato Cavalier, l’offerta pace, }
\livretVerse#12 { E l’offerta amistà; se à molli preghi }
\livretVerse#12 { Altro ch’ onte, e disprezzi offrir non sai }
\livretVerse#12 { Guerra apporti, infelice, e guerra avrai. }
\livretRef#'ACDaria
\livretVerse#10 { Mostri i più crudi, e infesti }
\livretVerse#10 { Della magion di Dite }
\livretVerse#10 { Da’ cupi antri funesti, }
\livretVerse#10 { Al cenno mio, venite. }
\livretDidasP\justify {
  S’oscura la scena, e si cambia in un luogo orribile, presentandosi
  in vari gruppi minaciosi i Demoni a spaventare Ubaldo.
}
\livretPersDidas Coro di Demoni
\livretRef#'ACEcoro
\livretVerse#12 { Qual è la man che scuote }
\livretVerse#10 { L’Antica Reggia a Pluto? }
\livretVerse#10 { Qual è il poter temuto, }
\livretVerse#10 { Che noi dagli antri orribili }
\livretVerse#10 { Richiama à rai del dì! }
\livretVerse#12 { Strazin le fiere Eumenidi, }
\livretVerse#10 { Strugga la fiamma ultrice, }
\livretVerse#10 { L’incauto, e l’infelice }
\livretVerse#10 { Che provocarlo ardì. }
\livretPers Ubaldo
\livretRef#'ACFrecit
\livretVerse#12 { Quanto fragili, e vane, immondi spirti, }
\livretVerse#12 { Son l’armi vostre incontro al Ciel! Mirate }
\livretVerse#12 { Quanto è lieve il contrasto, e quanto poco }
\livretVerse#12 { Basta, infelici, a richiamarvi al fuoco. }
\livretRef#'ACGaria
\livretVerse#12 { Tornate al nero Abisso, }
\livretVerse#10 { Onde l’orror v’alberga, }
\livretVerse#10 { E d’una sola verga }
\livretVerse#10 { All’agitar sparite. }
\livretDidasP\justify {
  Allo scuoter che fa Ubaldo incontro à Demoni la magica
  verga fuggono questi impauriti nella più gran confusione.
}
\livretPersDidas Coro di Demoni
\livretRef#'ACHcoro
\livretVerse#12 { Qual Sibilo orrendo }
\livretVerse#10 { Per l’aer rimbomba? }
\livretVerse#10 { Qual braccio tremendo }
\livretVerse#10 { Ci opprime cosi? }
\livretDidasP\justify {
  Spariti i mostri ritorna nel suo primo ridente flate la scena.
}
\livretPers Ubaldo
\livretRef#'ACIrecit
\livretVerse#12 { Eterno Proveder, tu che guidasti }
\livretVerse#12 { Per si strano camino i passi miei }
\livretVerse#12 { Scorgigli al fin prescritto. Ecco l’albergo, }
\livretVerse#12 { Ove, in grembo al piacere, al giogo indegno }
\livretVerse#12 { Di beltà lusinghiera }
\livretVerse#12 { Il tuo giovane Eroe piega la fronte, }
\livretVerse#12 { Tu che percuoti il monte, e dall’ estrema }
\livretVerse#12 { Falda si scuote, e fumo, e fiamma spira, }
\livretVerse#12 { Tu che nel mezzo all’ ira }
\livretVerse#12 { Il suol rimiri, e il suol vacilla, e trema, }
\livretVerse#12 { Scuoti, scuoti, gran Dio, dal cupo sonno }
\livretVerse#12 { Quell’ alma incauta, al guardo suo disvela }
\livretVerse#12 { L’orror de’ falli suoi, dell’empia Armida }
\livretVerse#12 { Sien le frodi schernite, }
\livretVerse#12 { E trionfi il tuo nome in faccia a Dite. }
\livretRef#'ACJaria
\livretVerse#12 { Finta larva, d’Abisso frall’ ombra, }
\livretVerse#10 { Il piacere gli scherza d’intorno. }
\livretVerse#10 { Ah se il sonno di morte l’ingombra, }
\livretVerse#10 { Se i suoi lumi si chiudono al giorno, }
\livretVerse#10 { Nell’ orrore del carcere indegno }
\livretVerse#10 { Più che a sdegno ti muova a pietà. }
\livretVerse#12 { Sciogli, sgombra, la notte funesta, }
\livretVerse#10 { Dio possente, lo scuoti, lo desta. }
\livretVerse#10 { Chi può trarlo dall’ ombra di morte; }
\livretVerse#10 { Se i tuoi raggi per scorta non hà? }
\livretVerse#12 { Il tuo spirto m’infiamma, m’accende }
\livretVerse#10 { Dio possente lo sento, lo scerno. }
\livretVerse#10 { Ah le frodi, e le forze d’Averno }
\livretVerse#10 { Van contrasto saranno al tuo vanto, }
\livretVerse#10 { E l’incanto di vana beltà. }
\livretDidasP\justify {
  Parte verso il fonde.
}
\livretFinAct Fine dell’ Atto primo.
\sep
\column-break
\livretAct ATTO SECONDO
\livretScene SCENA I
\livretDescAtt\column {
  \justify {
    Spazioso ameno Giardino in fondo al quale si vedono i tortuosi
    intricati portici del Laberinto, cho le circonda.
  }
  \justify {
    Armida e Rinaldo assisi sull’erba, e intessendo ghirlande.
  }
}
\livretPers A 2
\livretRef#'BAAduo
\livretVerse#12 { Qui’l regno è del contento; }
\livretVerse#12 { La sede del piacer. }
\livretPers Rinaldo
\livretVerse#12 { Fresch’ ombre, e verdi sponde, }
\livretVerse#12 { Cui bagna un rio d’argento }
\livretVerse#12 { C’invitano a goder. }
\livretVerse#12 { Par che la terra e l’onde }
\livretVerse#12 { Spirino un dolce ardore, }
\livretVerse#12 { Sembra che fin d’amor }
\livretVerse#12 { Mormori il vento. }
\livretPers A 2
\livretVerse#12 { Qui’l regno è del contento; }
\livretVerse#12 { La sede del piacer. }
\livretPers Armida
\livretVerse#12 { Folle chi della vita }
\livretVerse#12 { Passa il breve momento }
\livretVerse#12 { In torbidi pensier. }
\livretVerse#12 { Che val l’età fiorita, }
\livretVerse#12 { Che val ricchezza ed or, }
\livretVerse#12 { Se cambia un van timor. }
\livretVerse#12 { Tutto in tormento. }
\livretPers A 2
\livretVerse#12 { Prezioso è il tempo, e lieve, }
\livretVerse#12 { Facciamone tesor. }
\livretVerse#12 { La vita è un cammin breve }
\livretVerse#12 { Spargiamolo di fior. }
\livretPersDidas Armida alzandosi di sul prato in atto di partire.
\livretRef#'BABrecit
\livretVerse#12 { Addio. }
\livretPersDidas Rinaldo arrestendola affettuosamente
\livretVerse#12 { Già m’abbandoni? }
\livretPers Armida
\livretVerse#12 { Ah questa è l’ora }
\livretVerse#12 { Che da te lungi, o caro, }
\livretVerse#12 { Mi richiama ogni dì. }
\livretPers Rinaldo
\livretVerse#12 { Ma qual ti sforza }
\livretVerse#12 { A rapire ogni giorno }
\livretVerse#12 { Tanti dolci momenti al nostro amore }
\livretVerse#12 { Cruda, barbara legge? }
\livretPers Armida
\livretVerse#12 { Il mio timore. }
\livretPers Rinaldo
\livretVerse#12 { Timor? Di che? }
\livretPers Armida
\livretVerse#12 { Del tuo, del mio riposo. }
\livretPers Rinaldo
\livretVerse#12 { E chi potria turbarlo in questa, o cara, }
\livretVerse#12 { Separata dal mondo ignota sponda? }
\livretPers Armida
\livretVerse#12 { Numi! Il guardo del sole, i venti, l’onda… }
\livretVerse#12 { Ah tutto a chi ben ama }
\livretVerse#12 { È cagion di timor. Per nostro asilo }
\livretVerse#12 { Quest’ isola felice io scelsi invano }
\livretVerse#12 { In grembo all’ Oceano. Invan le cinsi }
\livretVerse#12 { Di fosca nebbia il piede, e i fianchi, e il tergo }
\livretVerse#12 { Di dirupate orride balze, aggiunsi }
\livretVerse#12 { Folta guardia di mostri, e d’insidiose }
\livretVerse#12 { Sirene allettatrici! I frutti, i fonti }
\livretVerse#12 { Di tosco aspersi, e quanto miri in lei }
\livretVerse#12 { E gli augelli, e le piante, e l’onda, e il vento }
\livretVerse#12 { Tutto è in nostra difesa, eppur pavento. }
\livretVerse#12 { Ah dal dì ch’io cangiai }
\livretVerse#12 { Perte, bell’idol mio, gli affetti miei; }
\livretVerse#12 { Patria, regno, tesor, tutto perdei; }
\livretVerse#12 { Pensa, che s’io ti perdo, }
\livretVerse#12 { Fuor che il rossor della mia fé tradita, }
\livretVerse#12 { Nulla mi resta. }
\livretPers Rinaldo
\livretVerse#12 { Ah non temer, mia vita. }
\livretVerse#12 { Sai che la mia tu sei, }
\livretVerse#12 { Come io son l’alma tua; che non poss’io }
\livretVerse#12 { Viver da te diviso un sol momento; }
\livretVerse#12 { Sai che ogni mio contento, }
\livretVerse#12 { Ogni mia speme è in te, ch’ altro non bramo, }
\livretVerse#12 { Che col tuo dolce nome il fiato estremo }
\livretVerse#12 { Spirar frà labbri tuoi. }
\livretPers Armida
\livretVerse#12 { Lo so: ma tremo. }
\livretRef#'BACaria
\livretVerse#10 { Tremo, bell’ idol mio, }
\livretVerse#10 { Ma questo mio timor }
\livretVerse#10 { Non è tormento. }
\livretVerse#10 { É vita dell’ amor }
\livretVerse#10 { E stimolo a goder, }
\livretVerse#10 { Per lui tutto il piacer }
\livretVerse#10 { Di possederti, oh Dio, }
\livretVerse#10 { Tutto risento. }
\livretVerse#10 { Langue nel sen l’ardor, }
\livretVerse#10 { Langue il desio, }
\livretVerse#10 { Quando a temer non s’hà, }
\livretVerse#10 { E troppa sicurtà }
\livretVerse#10 { Non è contento. }
\livretDidasP\justify { Parte. }

\livretScene SCENA II
\livretDescAtt\wordwrap-center { Rinaldo solo. }
\livretPers Rinaldo
\livretRef#'BBArecit
\livretVerse#12 { E non deggio seguirla? Ah, senza Armida }
\livretVerse#12 { Son secoli gl’istanti. A che mi giova }
\livretVerse#12 { Il ridente soggiorno, e dove or sono }
\livretVerse#12 { Tante varie bellezze, onde l’adorna }
\livretVerse#12 { La prodiga natura agli occhi miei? }
\livretVerse#12 { Ah che vicino a lei }
\livretVerse#12 { Tutto è lieto, e giocondo, }
\livretVerse#12 { Ride il cielo, ride il mondo, }
\livretVerse#12 { Ma copre un fosco velo, }
\livretVerse#12 { Se s’allontana Armida, e terra, e cielo, }
\livretVerse#12 { E diventa per me da lei diviso }
\livretVerse#12 { Un deserto d’orror l’istesso Eliso. }
\livretRef#'BBBaria
\livretVerse#10 { Lungi da te, ben mio, }
\livretVerse#10 { Se viver non poss’ io, }
\livretVerse#10 { Lungi da te, che sei }
\livretVerse#10 { Luce degli occhi miei, }
\livretVerse#10 { Vita di questo cor; }
\livretVerse#10 { Venga, e in un dolce sonno, }
\livretVerse#10 { Se te mirar non ponno, }
\livretVerse#10 { Mi chiuda i lumi amor. }
\livretDidasP\justify {
  Rimettendosi a seder sull’erba.
}
\livretRef#'BBCrecit
\livretVerse#12 { Forse, chi sa? Verranno }
\livretVerse#12 { Con un leggiadro inganno }
\livretVerse#12 { In sembianza d’Armida i lieti sogni }
\livretVerse#12 { A lusingar mia sorte }
\livretVerse#12 { In questa dolce immagine di morte. }
\livretVerse#12 { Oh inganno fortunato, }
\livretVerse#12 { Che le più care idee finga al pensiero, }
\livretVerse#12 { E da un finto piacer lo chiami al vero! }
\livretRef#'BBDaria
\livretVerse#10 { Vieni a me sull’ ali d’oro }
\livretVerse#10 { Lusinghier sogno amoroso, }
\livretVerse#10 { Ingannando il mio riposo }
\livretVerse#10 { In sembianza del mio ben. }
\livretVerse#10 { Trovi in te per pochi istanti }
\livretVerse#10 { Il mio cor qualche ristoro, }
\livretVerse#10 { Finche amor del mio tesoro }
\livretVerse#10 { Faccia poi svegliarmi in sen. }
\livretDidasP\justify {
  S’addormenta e al suono d’una soave sinfonia escono da vari luoghi
  leggiadri fantasmi a uso di piaceri, ballando intorno a Rinaldo.
}

\livretScene SCENA III
\livretDescAtt\column {
  \wordwrap-center { Ubaldo e detto. }
  \justify {
    Al finir della breve dolcissima sinfonia, si sente un confuso
    strepito in lontananza, e si vede entrar Ubaldo di fondo fra
    portici, che circondano il Giardino e spariscono le larve.
  }
}
\livretPers Ubaldo
\livretRef#'BCAaria
\livretVerse#12 { Oh come in un momento }
\livretVerse#10 { Dell’ incantata mole }
\livretVerse#10 { Tutto l’orror spari, }
\livretVerse#12 { Qual nube in faccia al vento, }
\livretVerse#10 { Qual fosca nebbia suole }
\livretVerse#10 { A’ caldi rai del dì. }
\livretRef#'BCBrecit
\livretVerse#12 { Così molle, è ridente }
\livretVerse#12 { E’ il sentier delle colpe, e l’alma alletta }
\livretVerse#12 { Per agevol pendìo }
\livretVerse#12 { A inoltrarsi, e smarrirsi, e se pur tardi }
\livretVerse#12 { Dell’ inganno s’avvede, }
\livretVerse#12 { Trova, in ritrarne il piede }
\livretVerse#12 { Della piaggia fiorita, }
\livretVerse#12 { Penosa, inestricabile l’escita. }
\livretVerse#12 { Qui del giovin Rinaldo }
\livretVerse#12 { È la dolce prigion; così lo tiene }
\livretVerse#12 { Fra le catene Armida, ed ei non sente }
\livretVerse#12 { Il peso de’ suoi lacci, e in ozio imbelle }
\livretVerse#12 { In oblio di se stesso… Eccolo. Oh stelle! }
\livretDidasP\justify { Accorgendosi con sorpresa di Rinaldo che dorme. }
\livretVerse#12 { Eccolo in grembo a’ fiori }
\livretVerse#12 { Che placido riposa. Ah, sconsigliato! }
\livretVerse#12 { Che letargo funesto! Orrido abisso }
\livretVerse#12 { Gli si spalanca al piede, e mentre intorno }
\livretVerse#12 { Vegliano alla sua preda }
\livretVerse#12 { Mille mostri d’Averno in varie forme, }
\livretVerse#12 { Sulla sponda fatal riposa, e dorme! }
\livretVerse#12 { Si scuota alfine. Al guardo suo risplenda }
\livretDidasP\justify {
  Scuopre da un velo il lucido scudo, e l’appende ad un ramo.
}
\livretVerse#12 { Questo lucido specchio, al di cui lampo }
\livretVerse#12 { Non regge ombra d’inganno; i molli fregi }
\livretVerse#12 { Della sua schiavitù vegga e il suo stato }
\livretVerse#12 { Pentimento, rossor, dispetto, ed ira }
\livretVerse#12 { Gli svegli in sen. }
\livretDidasP\justify {
  La scuote con forza e si ritira in disparte ad osservarlo.
}
\livretVerse#12 { \transparent { Gli svegli in sen. } Sorgi, Rinaldo, e mira. }
\livretPers Rinaldo
\livretRef#'BCCrecit
\livretVerse#12 { Misero! chi mi scuote? E quali in questo }
\livretVerse#12 { Breve sonno affannoso }
\livretVerse#12 { Turbano idée funeste il mio riposo! }
\livretVerse#12 { Oh morte! orribil larva! Agli occhi miei }
\livretVerse#12 { Qual poter ti presenta? e come appresi }
\livretVerse#12 { A temerne l’aspetto? Un freddo gelo }
\livretVerse#12 { Mi sparge in seno, i falli a me rinfaccia, }
\livretVerse#12 { E il ferro micidial vibra, e minaccia. }
\livretVerse#12 { Quale insolito orror! quai nuovi sensi }
\livretVerse#12 { M’agitan l’alma… e quale }
\livretVerse#12 { Mi ferisce lo sguardo }
\livretVerse#12 { Improvviso baglior? }
\livretDidasP\justify { Alzandosi spaventato. }
\livretVerse#12 { \transparent { Improvviso baglior? } l’arme lucenti }
\livretVerse#12 { Chi recò? come? quando? e in essa… Oh Dio! }
\livretVerse#12 { Quanto da me diverso!… }
\livretVerse#12 { Mi riconosco appena. A questo segno }
\livretVerse#12 { Avvilirmi potei }
\livretVerse#12 { Trasformarmi così? }

\livretScene SCENA IV
\livretDescAtt\wordwrap-center {
  Armida affannata e detto.
}
\livretPers Armida
\livretRef#'BDAduo
\livretVerse#12 { Soccorso, o Dei! }
\livretVerse#10 { Ahimè! son tradita, }
\livretVerse#10 { Mi palpita il core, }
\livretVerse#10 { Soccorso, pietà. }
\livretPers Rinaldo
\livretVerse#10 { Che dici? ah mia vita, }
\livretVerse#10 { Qual nuovo terrore }
\livretVerse#10 { Tremare ti fá? }
\livretPers Armida
\livretVerse#10 { M’opprime l’affanno. }
\livretPers Rinaldo
\livretVerse#10 { Ah palpito anch’io. }
\livretPers Armida
\livretVerse#10 { Che dubbio tiranno! }
\livretPers Rinaldo
\livretVerse#10 { Ma spiegati. }
\livretPers Armida
\livretVerse#10 { Oh Dio! }
\livretPers Rinaldo
\livretVerse#10 { Ma parla. }
\livretPers Armida
\livretVerse#10 { Non só. }
\livretPers A 2
\livretVerse#10 { In tanto periglio }
\livretVerse#10 { Tal velo ho sul ciglio, }
\livretVerse#10 { Che ben non comprendo }
\livretVerse#10 { Che parlo, che fó. }
\livretPers Rinaldo
\livretRef#'BDBrecit
\livretVerse#12 { Questo arcano funesto }
\livretVerse#12 { Spiegami per pietà, del tuo spavento }
\livretVerse#12 { Dimmi almen la cagion; }
\livretVerse#12 { Determina, se puoi }
\livretVerse#12 { I miei sospetti in rivelando i tuoi. }
\livretPers Armida
\livretVerse#12 { Ah siam perduti. Uno straniero ignoto }
\livretVerse#12 { Nell’ isola approdò. Lo spazio immenso }
\livretVerse#12 { Del periglioso mar, che ci divide }
\livretVerse#12 { Dal resto de’ viventi, }
\livretVerse#12 { Varcò sicuro, e i fieri mostri, e il giogo }
\livretVerse#12 { Dirupato del colle, e il dolce incanto }
\livretVerse#12 { Delle ninfe lascive, e fin del chiuso }
\livretVerse#12 { Intricato recinto }
\livretVerse#12 { Il fier custode in un sol giorno ha vinto. }
\livretPers Rinaldo
\livretVerse#12 { E non sapesti ancora }
\livretVerse#12 { Come qui giunse, e donde, }
\livretVerse#12 { A che venne, chi sia, dove s’asconde? }
\livretPers Armida
\livretVerse#12 { Questa è de’miei spaventi }
\livretVerse#12 { La più fiera cagion. Poter maggiore }
\livretVerse#12 { Del mio poter lo guida, e rende vane }
\livretVerse#12 { Tutte le mie ricerche. Ah, già col piede }
\livretVerse#12 { Premo l’ampia ruina }
\livretVerse#12 { Dell’incendio crudel, che tutto intorno }
\livretVerse#12 { Strugge, abbatte, divora; }
\livretVerse#12 { E la fiamma crudel non scopro ancora. }
\livretPers Rinaldo
\livretVerse#12 { E temi?… }
\livretPers Armida
\livretVerse#12 { E che potrei }
\livretVerse#12 { Altro temer, ben mio, }
\livretVerse#12 { Dall’ irata del Ciel vindice mano, }
\livretVerse#12 { Che di perderti, oh Dio? }
\livretPers Rinaldo
\livretVerse#12 { Paventi invano. }
\livretVerse#12 { Ah questi molli fregi, onde ti piacque }
\livretVerse#12 { Avvilirmi così, non han sopita }
\livretVerse#12 { Tutta la mia virtù, mi pende al fianco }
\livretVerse#12 { Non vil l’acciaro, e inerme ancor saprei, }
\livretVerse#12 { Non che ignoto guerriero, }
\livretVerse#12 { Sfidare in tua difesa il mondo intero. }
\livretRef#'BDCduo
\livretVerse#10 { Dilegua il tuo timore, }
\livretVerse#10 { Serena i mesti rai; }
\livretVerse#10 { Sai ch’io t’adoro, e sai, }
\livretVerse#10 { Ch’io morirò per te. }
\livretPers Armida
\livretVerse#10 { Taci, che accresci al core }
\livretVerse#10 { Il suo mortale affanno. }
\livretVerse#10 { L’ira del Ciel tiranno }
\livretVerse#10 { Tutta si sfoghi in me. }
\livretPers Rinaldo
\livretVerse#10 { Mio ben. }
\livretPers Armida
\livretVerse#10 { Mio dolce amore, }
\livretPers A 2
\livretVerse#10 { Che barbaro momento, }
\livretPers Rinaldo
\livretVerse#10 { Io tremo al tuo terrore }
\livretPers Armida
\livretVerse#10 { L’alma mancar mi sento }
\livretPers A 2
\livretVerse#10 { Ne intendo il mio spavento }
\livretVerse#10 { Ne posso dir perché. }
\livretPers Rinaldo
\livretVerse#10 { Ma il mio soccorso? }
\livretPers Armida
\livretVerse#10 { È vano. }
\livretPers Rinaldo
\livretVerse#10 { L’amore… }
\livretPers Armida
\livretVerse#10 { È mio periglio. }
\livretPers Rinaldo
\livretVerse#10 { Il Cielo… }
\livretPers Armida
\livretVerse#10 { Ah de’ suoi fulmini }
\livretVerse#10 { Già balenar sul ciglio }
\livretVerse#10 { Mi vedo acceso il lampo. }
\livretPers Rinaldo
\livretVerse#10 { No mira al nostro scampo }
\livretVerse#10 { Qual’arme il Ciel mi diè; }
\livretDidasP\justify { Stacca lo scudo e l’imbraccia. }
\livretVerse#10 { Vedi }
\livretDidasP\justify { Lo presenta ad Armida. }
\livretPers Armida
\livretVerse#10 { Oh stelle! che luce funesta! }
\livretDidasP\justify { Ritirandosi spaventata. }
\livretVerse#10 { Fuggi ascondi… }
\livretPers Rinaldo
\livretVerse#10 { Ma senti, t’arresta }
\livretDidasP\justify { Confuso in Atto di trattenerla. }
\livretPers A 2
\livretVerse#10 { Ah, che strana vicenda è mai questa, }
\livretVerse#10 { No più orribil la morte non è. }
\livretDidasP\justify { Fugge spaventata. }
\livretPers Rinaldo
\livretRef#'BDDrecit
\livretVerse#12 { Ora sì ch’io mi perdo. Ah chi le ispira }
\livretVerse#12 { Questo nuovo terror? Teme il periglio, }
\livretVerse#12 { E aborrisce l’ajuto! Il Ciel pietoso }
\livretVerse#12 { M’arma per sua difesa, e sul suo capo }
\livretVerse#12 { Il Cielo, se credo a lei, fulmina e tuona! }
\livretVerse#12 { Di perdermi paventa, e m’abbandona! }
\livretVerse#12 { Misera! Oh Dio! la rende }
\livretVerse#12 { Forsennata l’affanno. In questo stato }
\livretVerse#12 { Lasciarla in preda al suo crudel deliro }
\livretVerse#12 { Saria… }
\livretDidasP\justify { Risoluto in atto di seguire Armida. }

\livretScene SCENA V
\livretDescAtt\wordwrap-center {
  Ubaldo e detto.
}
\livretPers Ubaldo
\livretRef#'BEArecit
\livretVerse#12 { Fermati, incauto. }
\livretPers Rinaldo
\livretVerse#12 { Oh Ciel! che miro? }
\livretPers Ubaldo
\livretVerse#12 { Ah Rinaldo Rinaldo, }
\livretVerse#12 { Dove fuggi, che fai? Così del Cielo, }
\livretVerse#12 { Che suo campion t’elesse }
\livretVerse#12 { A difender la Fede, a strugger gli empi }
\livretVerse#12 { Gli alti disegni, e i vaticini adempi? }
\livretVerse#12 { Così da te Gerusalemme aspetta }
\livretVerse#12 { Libertade, e vendetta? Ah chi ti rende }
\livretVerse#12 { Così da te diverso, }
\livretVerse#12 { Sì vile agli occhi miei? Beltà fallace, }
\livretVerse#12 { Che ti guida a perir; che ti prepara }
\livretVerse#12 { Un laccio ad ogni passo, }
\livretVerse#12 { Un angue in ogni fior. Folle! e non vedi, }
\livretVerse#12 { Che quanto in lei di lusinghier t’apparve }
\livretVerse#12 { Son d’Averno, a sedurti, inganni, e larve? }
\livretVerse#12 { Torna, torna in te stesso }
\livretVerse#12 { Sconsigliato che sei. Cogli il momento, }
\livretVerse#12 { Ché da te fugge intimorita al lampo }
\livretVerse#12 { Di quest’ arme fatal. Fuggi, deludi }
\livretVerse#12 { Le lusinghe fallaci. }
\livretPers Rinaldo
\livretVerse#12 { Misero me! }
\livretPers Ubaldo
\livretVerse#12 { Tu non mi guardi, e taci? }
\livretVerse#12 { Tu arrossisci nel volto! Ah quel rossore }
\livretVerse#12 { È il color di virtù. Torna, Rinaldo, }
\livretVerse#12 { Alla gloria, all’onor. T’aspetta il Campo, }
\livretVerse#12 { Ti richiama Goffredo, }
\livretVerse#12 { Per sentier di prodigi il Ciel ti guida, }
\livretVerse#12 { L’indugio è morte. }
\livretPers Rinaldo
\livretVerse#12 { E ho da lasciare Armida? }
\livretPers Ubaldo
\livretVerse#12 { Ingrato! ah nel tuo core }
\livretVerse#12 { Chi bilancia il tuo Dio? }
\livretPers Rinaldo
\livretVerse#12 { Ma le promesse, }
\livretVerse#12 { La fede, amico, i giuramenti miei? }
\livretPers Ubaldo
\livretVerse#12 { Li rompi al Ciel per conservarli a lei? }
\livretVerse#12 { Sai pur con quante frodi, }
\livretVerse#12 { Con quant’arti costei de’ nostri Duci }
\livretVerse#12 { I più prodi sedusse, e in rea catena }
\livretVerse#12 { Li serbava crudel a duro fato, }
\livretVerse#12 { Se tu non eri; e tu la piangi? Ingrato! }
\livretVerse#12 { Trema per te. Tempo verrà che Armida, }
\livretVerse#12 { Sazia del tuo piacer, l’odio funesto }
\livretVerse#12 { Coll’ amor cangerà. Che te lasciando }
\livretVerse#12 { Su quest’ isola ignuda, o in un’ oscura }
\livretVerse#12 { Tormentosa prigion, rimasto in preda }
\livretVerse#12 { A tuoi fieri rimorsi, }
\livretVerse#12 { All’ orror di te stesso; a liberarti }
\livretVerse#12 { Dallo strazio crudel, l’amica mano, }
\livretVerse#12 { Ch’or salvarti potria, richiami in vano. }
\livretVerse#12 { Misero! in tale stato }
\livretVerse#12 { Oppresso, disperato, }
\livretVerse#12 { Senza pietà, senza soccorso… }
\livretPers Rinaldo
\livretVerse#12 { Ah taci! }
\livretVerse#12 { Non inasprir la piaga, }
\livretVerse#12 { Che mi lacera il cor. Se tu vedessi }
\livretVerse#12 { Qua dentro amico, e quale acerba guerra }
\livretVerse#12 { Vi fan gli opposti affetti, e qual mi scuote }
\livretVerse#12 { Di miseria, e d’orror scena funesta. }
\livretVerse#12 { Io ti farei pietà; più non distinguo }
\livretVerse#12 { Chi mi parla, ove son; tremo, e confondo }
\livretVerse#12 { Col periglio lo scampo; amico, oh Dio! }
\livretVerse#12 { Guida, salvami tu. Fuggiam da questo }
\livretVerse#12 { Insidioso recinto, }
\livretVerse#12 { Mi fido a te; più non resisto. }
\livretPers Ubaldo
\livretVerse#12 { Ho vinto. }
\livretPers Rinaldo
\livretRef#'BEBaria
\livretVerse#10 { Vedo l’abisso orrendo, }
\livretVerse#10 { Onde ritrassi il piede }
\livretVerse#10 { Sento d’onor, di fede }
\livretVerse#10 { Mille rimorsi al cor. }
\livretVerse#10 { Tutto mi fa spavento }
\livretVerse#10 { Dovunque volgo il ciglio; }
\livretVerse#10 { Ma in faccia al mio periglio }
\livretVerse#10 { La fiamma ancora io sento }
\livretVerse#10 { D’un male estinto ardor. }
\livretVerse#10 { D’un nero mar cruccioso }
\livretVerse#10 { Tutte le insidie ho scorto. }
\livretVerse#10 { Grazia è del Ciel pietoso }
\livretVerse#10 { S’io non rimasi assorto, }
\livretVerse#10 { Ma pur vicino al porto }
\livretVerse#10 { Forse mi perdo ancor. }
\livretFinAct Fine dell’ Atto secondo.
\sep
\column-break

\livretAct ATTO TERZO
\livretScene SCENA I
\livretDescAtt\column {
  \justify {
    Spazioso sotterraneo auso d’incanti con Ara e Tripode Le seguaci
    d’Armida cinte di nere bende stanno amministrando con una danza grave
    e solenne intorno all’ara un Sacrifizio agli Dei infernali.
  }
  \justify {
    Armida assisa sul tripode, e tenendo in mano la magica verga vi assiste,
    alzandosi crucciosa al finir del seguente.
  }
}
\livretPers Coro
\livretRef#'CAAcoro
\livretVerse#10 { Chi sorde vi rende }
\livretVerse#10 { Al magico incanto, }
\livretVerse#10 { Potenze tremende }
\livretVerse#10 { De’ regni del pianto? }
\livretPers Armida
\livretVerse#10 { Son questi, son questi }
\livretVerse#10 { I carmi possenti, }
\livretVerse#10 { Per cui di Cocito }
\livretVerse#10 { Sull’ orride sponde }
\livretVerse#10 { Raddoppio il musgito }
\livretVerse#10 { De’ gridi funesti; }
\livretVerse#10 { Né ancor sanguigna, e pallida }
\livretVerse#10 { Luce coperse il dì }
\livretVerse#10 { Ne larva informe, e squallida }
\livretVerse#10 { Al suon temuto uscì. }
\livretVerse#10 { Né ancor si scuote il Tripode }
\livretVerse#10 { Né sull’ offerte vittime }
\livretVerse#10 { La nera fiamma scende! }
\livretPers Coro
\livretVerse#10 { Chi sorde vi rende }
\livretVerse#10 { Al magico incanto, }
\livretVerse#10 { Potenze tremende }
\livretVerse#10 { De’ regni del pianto? }
\livretPers Armida
\livretVerse#10 { Gemer dell’ antro io sento }
\livretVerse#10 { Le cupe ampie caverne; }
\livretVerse#10 { Vedo il baglior de folgori }
\livretVerse#10 { Odo il muggir del vento, }
\livretVerse#10 { Par ch’ Ecate precipiti }
\livretVerse#10 { Dalle region superne. }
\livretVerse#10 { Scuoton la reggia a Pluto, }
\livretVerse#10 { Turbano a stige il corso }
\livretVerse#10 { Queste mie note orrende. }
\livretVerse#10 { Freme dell’ ombre eterne }
\livretVerse#10 { L’abitator temuto, }
\livretVerse#10 { E al mio soccorso intanto }
\livretVerse#10 { Qual nuovo cenno attende? }
\livretPers Coro
\livretVerse#10 { Chi sorde vi rende }
\livretVerse#10 { Al magico incanto, }
\livretVerse#10 { Potenze tremende }
\livretVerse#10 { De’ regni del pianto? }
\livretPers Armida
\livretRef#'CABrecit
\livretVerse#12 { Misera! Il Ciel m’opprime, }
\livretVerse#12 { M’abbandona l’Abisso. }
\livretVerse#12 { E Rinaldo… Ah crudel! forse congiura }
\livretDidasP\justify {
  Getta infuriata a terra la verga, e il Tripode.
}
\livretVerse#12 { Anch’egli a’danni miei… l’arme fatale }
\livretVerse#12 { Come in sua mano? Oh Dio! mi veggo ancora }
\livretVerse#12 { Quella luce funesta sugli occhi balenar; mi sento ancora }
% L’istesso gelo al cuor, quasi vicina
\livretVerse#12 { Un presagio crudel… }

\livretScene SCENA II
\livretDescAtt\justify {
  Ismene affannata, e frettolosa con alcune delle Seguaci, e detta.
}
\livretPers Ismene
\livretRef#'CBArecit
\livretVerse#12 { Corri, Regina, }
\livretPers Armida
\livretVerse#12 { Misera me! che avvenne? }
\livretPers Ismene
\livretVerse#12 { Il tuo Rinaldo… }
\livretVerse#12 { Fuor del chiuso recinto }
\livretPers Una delle seguaci
\livretVerse#12 { Io l’incontrai }
\livretVerse#12 { Coll’ ignoto guerrier… }
\livretPers Un altra delle seguaci
\livretVerse#12 { De tuoi custodi }
\livretVerse#12 { La truppa sbigottita. }
\livretPers Armida
\livretVerse#12 { Ah tacete, v’intendo. Io son tradita. }
\livretVerse#12 { Fugge l’indegno? Oh stelle! e i giuramenti… }
\livretVerse#12 { Le promesse… la fede… in questo stato!… }
\livretVerse#12 { Senza pur dirmi addio!… Numi! e che fanno, }
\livretVerse#12 { A queste di perfidia inique prove, }
\livretVerse#12 { I fulmini impotenti in man di Giove? }
\livretVerse#12 { Vendetta, o Dei, vendetta… A chi la chiedo? }
\livretVerse#12 { Da chi la spero ohimè? No non mi resta }
\livretVerse#12 { Fuor, che ne’ miei sospiri altra speranza. }
\livretVerse#12 { Questo che sol m’avanza }
\livretVerse#12 { Infelice soccorso }
\livretVerse#12 { Ah non si perda almen. Mi vegga il perfido }
\livretVerse#12 { Supplice a’ suoi piedi chieder mercede; }
\livretVerse#12 { Inondarli di pianto, e se non sente }
\livretVerse#12 { Qualche pietà dell’ infelice Armida, }
\livretVerse#12 { M’abbandoni il crudel, ma pria m’uccida. }
\livretRef#'CBBaria
\livretVerse#10 { Ah mi tolga almen la vita }
\livretVerse#10 { Il crudel, che m’ha tradita }
\livretVerse#10 { Per pietà del mio dolor. }
\livretVerse#10 { Se non basta in quel momento }
\livretVerse#10 { Forse a uccidermi il tormento, }
\livretVerse#10 { Perchè almen l’estrema aita }
\livretVerse#10 { Non la debba a un traditor. }
\livretDidasP\justify { Parte con smania. }

\livretScene SCENA III
\livretDescAtt\justify {
  Ismene col coro delle Seguaci d’Armida.
}
\livretPers Ismene
\livretRef#'CCArecit
\livretVerse#12 { Ove corre, infelice! Ove la guida }
\livretVerse#12 { Il suo cieco furor? l’antico fasto }
\livretVerse#12 { La sua gloria dov’è? Qual forza ignota }
\livretVerse#12 { Si l’oppresse in un dì? D’amore il regno }
\livretVerse#12 { Questa che a voglia sua volse, e rivolse, }
\livretVerse#12 { Questa di mille amanti }
\livretVerse#12 { I voti e i pianti a disprezzare avvezza, }
\livretVerse#12 { Or segue chi la fugge, e la disprezza. }
\livretPers Coro
\livretRef#'CCBcoro
\livretVerse#10 { Ah misera regina }
\livretVerse#10 { Che sarà mai di te? }
\livretVerse#10 { Forse alla sua rovina }
\livretVerse#10 { Volge infelice il piè. }
\livretPers Ismene
\livretVerse#12 { Andiamo, amiche; In sì crudel momento }
\livretVerse#12 { Non si abbandoni al suo destin. Si cerchi }
\livretVerse#12 { Fide egualmente a dì felici, e rei, }
\livretVerse#12 { O di salvarla, o di perir con lei. }
\livretRef#'CCCaria
\livretVerse#10 { Schernita, depressa }
\livretVerse#10 { Da un’ alma spergiura }
\livretVerse#10 { La vita non cura, }
\livretVerse#10 { Al duol di se stessa }
\livretVerse#10 { Già cede il governo, }
\livretVerse#10 { Nel Ciel, nell’ Averno }
\livretVerse#10 { Più speme non ha. }
\livretVerse#10 { Da noi solo aspetta }
\livretVerse#10 { In tanto periglio }
\livretVerse#10 { Tradita vendetta, }
\livretVerse#10 { Smarrita, consiglio, }
\livretVerse#10 { Oppressa, pietà. }
\livretDidasP\justify { Parte seguita dal Coro. }
\livretPers Coro
\livretVerse#10 { Ah trovi infelice }
\livretVerse#10 { In sorte sì fiera, }
\livretVerse#10 { Se aita non spera, }
\livretVerse#10 { Almen fedeltà. }

\livretScene SCENA IV
\livretDescAtt\column {
  \justify {
    Spiaggia di mare, a cui si giunge dalle scoscese balze d’un alpestre
    montagna, le di cui falde formano in lontananza un piccol seno, dove
    è la navicella della Fortuna pronta alla vela.
  }
  \wordwrap-center { Ubaldo, e Rinaldo. }
}
\livretPers Ubaldo
\livretRef#'CDArecit
\livretVerse#12 { Siam quasi in salvo, eccoci al lido. Osserva }
\livretVerse#12 { L’amico legno, e quella, }
\livretVerse#12 { Che ne siede al governo, e che a sua voglia }
\livretVerse#12 { Regola i venti, e il mar, sicura Guida, }
\livretVerse#12 { Che ne attende al partir. }
\livretPers Rinaldo
\livretVerse#12 { (Povera Armida!) }
\livretPers Ubaldo
\livretVerse#12 { Ma tu sospiri! Il guardo }
\livretVerse#12 { Fissi smarrito al suol; confuso, e mesto }
\livretVerse#12 { Or ti volgi, or t’arresti, e nel momento, }
\livretVerse#12 { Che affrettar ti dovria, sembri più lento! }
\livretVerse#12 { Che mediti? che pensi? }
\livretPers Rinaldo
\livretVerse#12 { Ah tutto, amico }
\livretVerse#12 { Svelerotti il mio cor. Fuggir da quella, }
\livretVerse#12 { Che fu l’anima mia, che patria e regno }
\livretVerse#12 { Abbandonò per me, che resta in preda }
\livretVerse#12 { All’ affanno, al rossor, m’empie d’orrore }
\livretVerse#12 { Mi sembra crudeltà. Trionfo, è vero, }
\livretVerse#12 { Ma il conflitto è crudel. Tutto m’invita }
\livretVerse#12 { Se la ragione ascolto, }
\livretVerse#12 { Sollecito a partir. Se ascolto, il cuore }
\livretVerse#12 { Non parla che di lei. L’onor mi sprona, }
\livretVerse#12 { La pietà mi trattien. Ma ch’io ritorni }
\livretVerse#12 { A consolarla almen nel fiero istante }
\livretVerse#12 { Della partenza amara, e chi’io le dia }
\livretVerse#12 { Nel suo mortale affanno i pegni estremi }
\livretVerse#12 { Di tenera amistà, se non di fede, }
\livretVerse#12 { La ragion non lo vieta, amor lo chiede. }
\livretPers Ubaldo
\livretVerse#12 { Signor che dici mai? De’ molli affetti }
\livretVerse#12 { Tremi al conflitto, e maggior guerra aspetti? }
\livretVerse#12 { T’arresta il passo un picciol rio, che appena }
\livretVerse#12 { Serba un fil d’onda in faccia al Sirio ardente? }
\livretVerse#12 { E lo potrai varcar quando è torrente? }
\livretVerse#12 { E questo è il tuo trionfo? Ah no, tu gemi }
\livretVerse#12 { Ancor fra le catene, e per sedurti }
\livretVerse#12 { Il piacer lusinghiero }
\livretVerse#12 { Si veste di pietà. }
\livretPers Rinaldo
\livretVerse#12 { No, non è vero. }
\livretVerse#12 { Son libero, son mio. Ma chi’io la lasci, }
\livretVerse#12 { Ingannata così… Povera Armida! }
\livretVerse#12 { Non la vedrò più mai. Che amaro pianto }
\livretVerse#12 { Verserà da’ begli occhi? E ch’io non possa }
\livretVerse#12 { Raddolcirla, placarla, }
\livretVerse#12 { Darle l’estremo addio… }
\livretVerse#12 { Non lo sperar. Troppo trionfo è il mio. }
\livretPers Ubaldo
\livretVerse#12 { Vattene dunque, fuggi, }
\livretVerse#12 { Va’, colma la misura a’ tuoi delitti; }
\livretVerse#12 { Torna alle tue catene. Io solo al campo }
\livretVerse#12 { Senza te tornerò. Dirò, che invano }
\livretVerse#12 { Impiegò per salvarti }
\livretVerse#12 { I suoi prodigi il Ciel; che più non curi }
\livretVerse#12 { Né l’onor, né la fé; che ti lasciai }
\livretVerse#12 { Dal tuo vil giogo oppresso, }
\livretVerse#12 { Della Patria nemico, e di te stesso. }
\livretRef#'CDBaria
\livretVerse#10 { Torna schiavo infelice, }
\livretVerse#10 { Alla prigione antica; }
\livretVerse#10 { D’un’ empia ingannatrice }
\livretVerse#10 { Torna fra’ lacci ancor. }
\livretVerse#10 { Vanne, ma pensa intanto, }
\livretVerse#10 { Che sciorli un dì vorrai, }
\livretVerse#10 { Quando fia vano il pianto }
\livretVerse#10 { Inutile il dolor: }
\livretDidasP\justify { Scostandosi per partire. }
\livretPers Rinaldo
\livretRef#'CDCaria
\livretVerse#10 { Ah non lasciarmi no. }
\livretDidasP\justify { Trattenendolo con smania. }
\livretVerse#10 { (Che barbaro rigor!) }
\livretVerse#10 { Quel che vorrai farò. }
\livretVerse#10 { Perdona a un folle amor }
\livretVerse#10 { L’affanno mio. }
\livretVerse#10 { Vorrei partir… vorrei }
\livretVerse#10 { Darle l’estremo addio… }
\livretVerse#10 { Poveri affetti miei! }
\livretDidasP\justify { Si sente in lontananza la voce d’Armida. }
\livretPers Armida
\livretVerse#10 { Aspetta traditor }
\livretPers Rinaldo
\livretVerse#10 { Eccola, oh Dio! }
\livretDidasP\justify { Col massimo trasporto. }
\livretRef#'CDDrecit
\livretVerse#12 { Misera! È già vicina. }
\livretDidasP\justify { Guardando dentro la scena. }
\livretVerse#12 { Qual la rende il dolor! Pallida, smorta, }
\livretVerse#12 { Affannata, tremante… Ah vedi amico, }
\livretVerse#12 { Come corre infelice; e l’aspro gelo, }
\livretVerse#12 { E le scoscese rupi }
\livretVerse#12 { Sembrano un vano inciampo }
\livretVerse#12 { Per le tenere piante a passi suoi… }
\livretVerse#12 { Ah non sdegnarti. Io partirò se vuoi. }
\livretPers Ubaldo
\livretVerse#12 { Non è più tempo. Era prudenza allora, }
\livretVerse#12 { Debolezza or saria. Ti serba il Cielo. }
\livretVerse#12 { A dar di tua virtù le prove estreme, }
\livretVerse#12 { Cerca il folle il periglio, il vil lo teme. }
\livretPers Rinaldo
\livretVerse#12 { Deh non lasciarmi amico. In questo stato }
\livretVerse#12 { Più bisogno ho di te. Lo vedi, oh Dio! }
\livretVerse#12 { Che momento funesto! }
\livretPers Ubaldo
\livretVerse#12 { (Oh Ciel, l’assisti; il tuo trionfo è questo.) }

\livretScene SCENA V
\livretDescAtt\wordwrap-center { Armida affannata e detti. }
\livretPers Armida
\livretRef#'CEArecit
\livretVerse#12 { Fermati, aspetta. Ahime! respiro appena. }
\livretVerse#12 { Un palpito affannoso }
\livretVerse#12 { Mi serra il cuor, mi toglie }
\livretVerse#12 { E moto, e voce, e m’abbandona in questi }
\livretVerse#12 { Brevi istanti fatali }
\livretVerse#12 { Fin la forza di piangere i miei mali. }
\livretPers Ubaldo
\livretVerse#12 { (Che periglioso assalto!) }
\livretPers Rinaldo
\livretVerse#12 { (Ah sconsigliato! }
\livretVerse#12 { Qual cimento aspettai!) }
\livretPers Armida
\livretVerse#12 { Mirami ingrato, }
\livretVerse#12 { Ah che ti feci mai }
\livretVerse#12 { Per ridurmi così? Come a tal segno }
\livretVerse#12 { Io da te meritai l’odio, e lo sdegno? }
\livretPers Ubaldo
\livretVerse#12 { (Tremo per lui.) }
\livretPers Rinaldo
\livretVerse#12 { (Resisto appena.) Ascolta }
\livretVerse#12 { E almen per poco Armida, }
\livretVerse#12 { Modera il tuo dolor. Pur troppo io sento }
\livretVerse#12 { I rimproveri tuoi, gemo al tuo pianto, }
\livretVerse#12 { Mi dispera il tuo affanno, e al solo aspetto }
\livretVerse#12 { Del tuo presente stato }
\livretVerse#12 { Io mi sento morir. Vani argomenti }
\livretVerse#12 { Sariano alla mia fuga }
\livretVerse#12 { L’arti tue, l’odio antico, il mio periglio; }
\livretVerse#12 { Ma a un Cittadino, a un figlio }
\livretVerse#12 { Parla la Patria, e il Ciel. Tutto condanna }
\livretVerse#12 { Il nostro amore, e mi richiama altrove }
\livretVerse#12 { La giustizia, il dover. Vuoi ch’io tradisca }
\livretVerse#12 { Tante speranze, e tanti voti, e manchi, }
\livretVerse#12 { Guerrier codardo, e cittadin ribelle }
\livretVerse#12 { Alla Patria, all’ onor? }
\livretPers Armida
\livretVerse#12 { Perfido! Oh stelle! }
\livretVerse#12 { Patria, e regno ebbi anch’io. Di mille voti }
\livretVerse#12 { E di dolci speranze l’oggetto }
\livretVerse#12 { Fui già gran tempo, e grand’invidia porsi }
\livretVerse#12 { Dell’ emule regine al fasto altero; }
\livretVerse#12 { Anch’io del vostro impero }
\livretVerse#12 { Distruggere i principi, e col tuo sangue }
\livretVerse#12 { La Patria oppressa vendicar, stimai }
\livretVerse#12 { Un consiglio de’ numi, e ti salvai. }
\livretVerse#12 { Ma son vane memorie. In questo stato }
\livretVerse#12 { Sol le frodi rammenti, e l’odio antico; }
\livretVerse#12 { L’amante mi tradì; parlo al nemico. }
\livretVerse#12 { Tuo prigionier, tua preda, }
\livretVerse#12 { Chiedo sol di seguirti. Ah dove andrei }
\livretVerse#12 { Senza te sventurata, ove non sia }
\livretVerse#12 { Segno agli scherni altrui la sorte mia? }
\livretVerse#12 { Teco verrò. La mia beltà negletta }
\livretVerse#12 { Del tuo trionfo al campo }
\livretVerse#12 { La gloria accrescerà; Fedele ancella }
\livretVerse#12 { Nel fervor della pugna a’ giorni tuoi }
\livretVerse#12 { Io veglierò gelosa, e al ferro ostile }
\livretVerse#12 { Del mio sen farò scudo. Ah non si nieghi }
\livretVerse#12 { L’innocente richiesta al pianto mio. }
\livretPers Ubaldo
\livretVerse#12 { Vacilli forse? }
\livretPers Armida
\livretVerse#12 { Ah non rispondi? }
\livretPers Rinaldo
\livretVerse#12 { Oh Dio! }
\livretVerse#12 { Tu nemica? Tu serva? Io te del campo }
\livretVerse#12 { Al ludibrio esporrei? Deh qui sia il fine }
\livretVerse#12 { Del fallir nostro, Armida, }
\livretVerse#12 { E del nostro rossor. Nel lido ignoto }
\livretVerse#12 { Ne copra la memoria eterno oblio. }
\livretVerse#12 { Rimanti in pace. Addio. Gl’impeti accheta }
\livretVerse#12 { D’un amore infelice; }
\livretVerse#12 { Io parto, a te non lice }
\livretVerse#12 { Meco venir, la gloria tua lo vieta. }
\livretVerse#12 { Addio. Più che non credi }
\livretVerse#12 { Atroce nel lasciarti è la mia pena, }
\livretVerse#12 { Ma seguirmi non dei. }
\livretPers Armida
\livretVerse#12 { Dunque mi svena. }
\livretRef#'CEBterzetto
\livretVerse#10 { Strappami il cuor dal seno, }
\livretVerse#10 { Perfido, traditor. }
\livretVerse#10 { Sia questo il premio almeno }
\livretVerse#10 { D’un infelice amor. }
\livretDidasP\justify { Gettandosi a piè di Rinaldo. }
\livretPersDidas Rinaldo sollevandola con passione.
\livretVerse#10 { Sorgi che dici?… Oh Dio. }
\livretPersDidas Ubaldo a Rinaldo con severità.
\livretVerse#10 { Ah ti seduce il cor. }
\livretPers Armida e Rinaldo
\livretVerse#10 { Che fiero stato è il mio, }
\livretVerse#10 { Che barbaro dolor! }
\livretPers Armida
\livretVerse#10 { Tu vedi il mio tormento. }
\livretPers Ubaldo
\livretVerse#10 { Del Ciel le voci intendi. }
\livretPers A 2
\livretVerse#10 { E reo d’un tradimento }
\livretPers Armida
\livretVerse#10 { Un fido amore offendi? }
\livretPers Ubaldo
\livretVerse#10 { Oltraggi onore, e fe? }
\livretPers Rinaldo
\livretVerse#10 { Questo è crudel cimento. }
\livretPers A 3
\livretVerse#10 { Ah nell’ angustia estrema }
\livretVerse#10 { Il cuor mi trema, e il piè. }
\livretPersDidas Ubaldo a Rinaldo
\livretVerse#10 { Vieni l’onor t’affretta. }
\livretPers Rinaldo
\livretVerse#10 { Rimanti in pace. Addio, }
\livretVerse#10 { No, più crudel del mio }
\livretVerse#10 { Il tuo destin non è. }
\livretDidasP\justify { S’incamina con Ubaldo. }
\livretPersDidas Armida smaniosa
\livretVerse#10 { Ah barbaro, aspetta }
\livretVerse#10 { Ah fermati, ingrato. }
\livretVerse#10 { Oh Numi, vendetta }
\livretVerse#10 { D’un mostro spietato }
\livretVerse#10 { Che fede non ha. }
\livretVerse#10 { Sprezzata, tradita }
\livretVerse#10 { M’opprime il tormento, }
\livretVerse#10 { Mi manca la vita, }
\livretVerse#10 { Gelare mi sento, }
\livretVerse#10 { Oh Numi pietà. }
\livretDidasP\justify { Cade svenuta. }
\livretPersDidas Rinaldo voltandosi con smania
\livretVerse#12 { Misera! }
\livretPers Ubaldo
\livretVerse#12 { Ah dove vai? }
\livretPers Rinaldo
\livretVerse#12 { A richiamarla in vita. }
\livretPers Ubaldo
\livretVerse#12 { Fuggi la frode ordita. }
\livretPers Rinaldo
\livretVerse#12 { E troppa crudeltà. }
\livretDidasP\justify {
  Staccandosi da Ubaldo, e correndo affanato verso Armida.
}
\livretVerse#10 { Armida… Ah… più non ha }
\livretVerse#10 { Né moto, né color. }
\livretVerse#10 { Forse l’ucciderà }
\livretVerse#10 { L’eccesso del dolor. }
\livretVerse#10 { Ah mi si spezza il cor }
\livretVerse#10 { Fra tanto affanno. }
\livretVerse#10 { Senti… Non partirò. }
\livretVerse#10 { Vedi… Che t’amo ancor. }
\livretVerse#10 { Che parlo? Oh Dio! che fò? }
\livretVerse#10 { Oh sfortunato amor! }
\livretVerse#10 { Dover tiranno! }
\livretVerse#10 { Come restar degg’io?… }
\livretVerse#10 { Come partir potrò?… }
\livretPersDidas Ubaldo risoluto incamminandosi verso il lido.
\livretVerse#10 { Tu resta, io parto. }
\livretPersDidas Rinaldo correndo ad arrestarlo.
\livretVerse#10 { Oh Dio! }
\livretVerse#10 { Sentimi. }
\livretPers Ubaldo
\livretVerse#10 { O vieni, o resta. }
\livretPers Rinaldo
\livretVerse#10 { Che fiera legge è questa! }
\livretPers Ubaldo
\livretVerse#10 { Ora è il rigor pietà }
\livretPers Rinaldo
\livretVerse#10 { Povera Armida. Addio }
\livretVerse#10 { Il duol m’ucciderà. }
\livretDidasP\justify { Vanno al lido s’imbarcano, e partono. }

\livretScene SCENA VI
\livretDescAtt\wordwrap-center {
  Armida svenuta, Ismene, e il Coro delle seguaci.
}
\livretPersDidas Ismene voltandosi verso il mare.
\livretRef#'CFArecit
\livretVerse#12 { Ecco la sventurata. In questo stato }
\livretVerse#12 { Quell’ empio traditore }
\livretVerse#12 { Poté lasciarla, e gliel’ sofferse il cuore? }
\livretPersDidas Coro delle donne con Ismene
\livretDidasP\justify {
  Avanzandosi in vari gruppi verso il lido richiamando Rinaldo.
}
\livretRef#'CFBcoro
\livretVerse#10 { Ah barbaro, ah senti, }
\livretVerse#10 { Rivolgi le vele }
\livretVerse#10 { Ah mira, crudele, }
\livretVerse#10 { De’ tuoi tradimenti }
\livretVerse#10 { Il frutto qual è. }
\livretPersDidas Ismene ritornando affanosa accanto a Armida.
\livretRef#'CFCrecit
\livretVerse#12 { Infelice Regina! Ancor respira, }
\livretVerse#12 { Palpita ancora. A richiamarla in vita }
\livretDidasP\justify { Alle sue seguaci che accorrono. }
\livretVerse#12 { Aiutatemi amiche. Al tuo dolore }
\livretVerse#12 { Deh non ceder così. D’un traditore }
\livretVerse#12 { Non accresca il trionfo }
\livretVerse#12 { La tua morte infelice. A vendicarti }
\livretVerse#12 { Serbati in vita almen della vendetta }
\livretVerse#12 { La speme non t’è tolta. }
\livretVerse#12 { Deh consolati, Armida, Armida, ascolta. }
\livretPersDidas Armida rinvenendosi a poco a poco.
\livretVerse#12 { Per chi ritorno in vita? Ah colla morte }
\livretVerse#12 { Finisci almeno il mio crudel martiro, }
\livretVerse#12 { Svenami, traditor. Stelle! Che miro? }
\livretDidasP\justify {
  Alzandosi spaventata nell’ accorgersi della fuga di Rinaldo.
}
\livretVerse#12 { Fugge il crudel? Sulla deserta riva }
\livretVerse#12 { Mi lascia sconsolata e semiviva! }
\livretVerse#12 { E non l’inghiotte il mare? E a incenerirlo }
\livretVerse#12 { Fulmin non piomba? E il duol, che mi divora }
\livretVerse#12 { Mira l’abisso, e non si scuote ancora? }
\livretDidasP\justify {
  Resta per un momento immobile fissando spaventata lo sguardo verso la marina,
  indi s’ineamina furiosa.
}
\livretVerse#12 { Sorgete alfin sorgete }
\livretDidasP\justify { Ritornando furente. }
\livretVerse#12 { Alla temuta voce, o furie infeste, }
\livretVerse#12 { Della notte profonda. }
\livretDidasP\justify {
  Si sentono fulmini, e tuoni e s’ingombra di tenebre la scena.
}
\livretVerse#12 { Struggete, oh Dei, struggete }
\livretVerse#12 { Queste del mio rossor rive funeste. }
\livretDidasP\justify {
  Si sente in lontananza lo strepito della rovina dell’ isola.
}
\livretVerse#12 { I venti, e le tempeste }
\livretVerse#12 { Turbino il Cielo, è l’onda. Ah più non chiedo }
\livretVerse#12 { Difesa a un folle amor. Voglio vendetta }
\livretVerse#12 { Della mia fé tradita. }
\livretVerse#12 { Questa speranza sol mi serba in vita. }
\livretRef#'CFDaria
\livretVerse#10 { Io con voi la nera face }
\livretVerse#10 { A turbargli i rai del giorno, }
\livretVerse#10 { Al crudel sempre d’intorno }
\livretVerse#10 { Nuova Furia agiterò. }
\livretVerse#10 { Io nel sen tutto d’Aletto }
\livretVerse#10 { Verserogli il rio veleno; }
\livretVerse#10 { A quel perfido dal petto }
\livretVerse#10 { L’empio cuore io strapperò. }
\livretVerse#10 { E agli ingrati eterno esempio }
\livretVerse#10 { Nel suo scempio io lascerò. }
\livretDidasP\justify {
  (Monta Armida sopra un carro tirato da’ Dragoni alati, e intorno ad
  essa in vari gruppi sulle nere infuocate nubi le sue seguaci, che
  replicano in Coro.)
}
\livretVerse#10 { E agli ingrati eterno esempio }
\livretVerse#10 { Nel suo scempio io lascerò. }
\livretFinAct Fine dell Dramma.
}
