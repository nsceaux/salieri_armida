\livretAct ATTO TERZO
\livretScene SCENA I
\livretDescAtt\column {
  \justify {
    Spazioso sotterraneo auso d’incanti con Ara e Tripode Le seguaci
    d’Armida cinte di nere bende stanno amministrando con una danza grave
    e solenne intorno all’ara un Sacrifizio agli Dei infernali.
  }
  \justify {
    Armida assisa sul tripode, e tenendo in mano la magica verga vi assiste,
    alzandosi crucciosa al finir del seguente.
  }
}
\livretPers Coro
\livretRef#'CAAcoro
%#~ Chi sorde vi rende
%#~ Al magico incanto,
%#~ Potenze tremende
%#~ De’ regni del pianto?
\livretPers Armida
%#~ Son questi, son questi
%#~ I carmi possenti,
%#~ Per cui di Cocito
%#~ Sull’ orride sponde
%#~ Raddoppio il musgito
%#~ De’ gridi funesti;
%#~ Né ancor sanguigna, e pallida
%#~ Luce coperse il dì
%#~ Ne larva informe, e squallida
%#~ Al suon temuto uscì.
%#~ Né ancor si scuote il Tripode
%#~ Né sull’ offerte vittime
%#~ La nera fiamma scende!
\livretPers Coro
%#~ Chi sorde vi rende
%#~ Al magico incanto,
%#~ Potenze tremende
%#~ De’ regni del pianto?
\livretPers Armida
%#~ Gemer dell’ antro io sento
%#~ Le cupe ampie caverne;
%#~ Vedo il baglior de folgori
%#~ Odo il muggir del vento,
%#~ Par ch’ Ecate precipiti
%#~ Dalle region superne.
%#~ Scuoton la reggia a Pluto,
%#~ Turbano a stige il corso
%#~ Queste mie note orrende.
%#~ Freme dell’ ombre eterne
%#~ L’abitator temuto,
%#~ E al mio soccorso intanto
%#~ Qual nuovo cenno attende?
\livretPers Coro
%#~ Chi sorde vi rende
%#~ Al magico incanto,
%#~ Potenze tremende
%#~ De’ regni del pianto?
\livretPers Armida
\livretRef#'CABrecit
%# Misera! Il Ciel m’opprime,
%# M’abbandona l’Abisso.
%# E Rinaldo… Ah crudel! forse congiura
\livretDidasP\justify {
  Getta infuriata a terra la verga, e il Tripode.
}
%# Anch’egli a’danni miei… l’arme fatale
%# Come in sua mano? Oh Dio! mi veggo ancora
%# Quella luce funesta sugli occhi balenar; mi sento ancora
% L’istesso gelo al cuor, quasi vicina
%# Un presagio crudel…

\livretScene SCENA II
\livretDescAtt\justify {
  Ismene affannata, e frettolosa con alcune delle Seguaci, e detta.
}
\livretPers Ismene
\livretRef#'CBArecit
%# Corri, Regina,
\livretPers Armida
%# Misera me! che avvenne?
\livretPers Ismene
%# Il tuo Rinaldo…
%# Fuor del chiuso recinto
\livretPers Una delle seguaci
%# Io l’incontrai
%# Coll’ ignoto guerrier…
\livretPers Un altra delle seguaci
%# De tuoi custodi
%# La truppa sbigottita.
\livretPers Armida
%# Ah tacete, v’intendo. Io son tradita.
%# Fugge l’indegno? Oh stelle! e i giuramenti…
%# Le promesse… la fede… in questo stato!…
%# Senza pur dirmi addio!… Numi! e che fanno,
%# A queste di perfidia inique prove,
%# I fulmini impotenti in man di Giove?
%# Vendetta, o Dei, vendetta… A chi la chiedo?
%# Da chi la spero ohimè? No non mi resta
%# Fuor, che ne’ miei sospiri altra speranza.
%# Questo che sol m’avanza
%# Infelice soccorso
%# Ah non si perda almen. Mi vegga il perfido
%# Supplice a’ suoi piedi chieder mercede;
%# Inondarli di pianto, e se non sente
%# Qualche pietà dell’ infelice Armida,
%# M’abbandoni il crudel, ma pria m’uccida.
\livretRef#'CBBaria
%#~ Ah mi tolga almen la vita
%#~ Il crudel, che m’ha tradita
%#~ Per pietà del mio dolor.
%#~ Se non basta in quel momento
%#~ Forse a uccidermi il tormento,
%#~ Perchè almen l’estrema aita
%#~ Non la debba a un traditor.
\livretDidasP\justify { Parte con smania. }

\livretScene SCENA III
\livretDescAtt\justify {
  Ismene col coro delle Seguaci d’Armida.
}
\livretPers Ismene
\livretRef#'CCArecit
%# Ove corre, infelice! Ove la guida
%# Il suo cieco furor? l’antico fasto
%# La sua gloria dov’è? Qual forza ignota
%# Si l’oppresse in un dì? D’amore il regno
%# Questa che a voglia sua volse, e rivolse,
%# Questa di mille amanti
%# I voti e i pianti a disprezzare avvezza,
%# Or segue chi la fugge, e la disprezza.
\livretPers Coro
\livretRef#'CCBcoro
%#~ Ah misera regina
%#~ Che sarà mai di te?
%#~ Forse alla sua rovina
%#~ Volge infelice il piè.
\livretPers Ismene
%# Andiamo, amiche; In sì crudel momento
%# Non si abbandoni al suo destin. Si cerchi
%# Fide egualmente a dì felici, e rei,
%# O di salvarla, o di perir con lei.
\livretRef#'CCCaria
%#~ Schernita, depressa
%#~ Da un’ alma spergiura
%#~ La vita non cura,
%#~ Al duol di se stessa
%#~ Già cede il governo,
%#~ Nel Ciel, nell’ Averno
%#~ Più speme non ha.
%#~ Da noi solo aspetta
%#~ In tanto periglio
%#~ Tradita vendetta,
%#~ Smarrita, consiglio,
%#~ Oppressa, pietà.
\livretDidasP\justify { Parte seguita dal Coro. }
\livretPers Coro
%#~ Ah trovi infelice
%#~ In sorte sì fiera,
%#~ Se aita non spera,
%#~ Almen fedeltà.

\livretScene SCENA IV
\livretDescAtt\column {
  \justify {
    Spiaggia di mare, a cui si giunge dalle scoscese balze d’un alpestre
    montagna, le di cui falde formano in lontananza un piccol seno, dove
    è la navicella della Fortuna pronta alla vela.
  }
  \wordwrap-center { Ubaldo, e Rinaldo. }
}
\livretPers Ubaldo
\livretRef#'CDArecit
%# Siam quasi in salvo, eccoci al lido. Osserva
%# L’amico legno, e quella,
%# Che ne siede al governo, e che a sua voglia
%# Regola i venti, e il mar, sicura Guida,
%# Che ne attende al partir.
\livretPers Rinaldo
%# (Povera Armida!)
\livretPers Ubaldo
%# Ma tu sospiri! Il guardo
%# Fissi smarrito al suol; confuso, e mesto
%# Or ti volgi, or t’arresti, e nel momento,
%# Che affrettar ti dovria, sembri più lento!
%# Che mediti? che pensi?
\livretPers Rinaldo
%# Ah tutto, amico
%# Svelerotti il mio cor. Fuggir da quella,
%# Che fu l’anima mia, che patria e regno
%# Abbandonò per me, che resta in preda
%# All’ affanno, al rossor, m’empie d’orrore
%# Mi sembra crudeltà. Trionfo, è vero,
%# Ma il conflitto è crudel. Tutto m’invita
%# Se la ragione ascolto,
%# Sollecito a partir. Se ascolto, il cuore
%# Non parla che di lei. L’onor mi sprona,
%# La pietà mi trattien. Ma ch’io ritorni
%# A consolarla almen nel fiero istante
%# Della partenza amara, e chi’io le dia
%# Nel suo mortale affanno i pegni estremi
%# Di tenera amistà, se non di fede,
%# La ragion non lo vieta, amor lo chiede.
\livretPers Ubaldo
%# Signor che dici mai? De’ molli affetti
%# Tremi al conflitto, e maggior guerra aspetti?
%# T’arresta il passo un picciol rio, che appena
%# Serba un fil d’onda in faccia al Sirio ardente?
%# E lo potrai varcar quando è torrente?
%# E questo è il tuo trionfo? Ah no, tu gemi
%# Ancor fra le catene, e per sedurti
%# Il piacer lusinghiero
%# Si veste di pietà.
\livretPers Rinaldo
%# No, non è vero.
%# Son libero, son mio. Ma chi’io la lasci,
%# Ingannata così… Povera Armida!
%# Non la vedrò più mai. Che amaro pianto
%# Verserà da’ begli occhi? E ch’io non possa
%# Raddolcirla, placarla,
%# Darle l’estremo addio…
%# Non lo sperar. Troppo trionfo è il mio.
\livretPers Ubaldo
%# Vattene dunque, fuggi,
%# Va’, colma la misura a’ tuoi delitti;
%# Torna alle tue catene. Io solo al campo
%# Senza te tornerò. Dirò, che invano
%# Impiegò per salvarti
%# I suoi prodigi il Ciel; che più non curi
%# Né l’onor, né la fé; che ti lasciai
%# Dal tuo vil giogo oppresso,
%# Della Patria nemico, e di te stesso.
\livretRef#'CDBaria
%#~ Torna schiavo infelice,
%#~ Alla prigione antica;
%#~ D’un’ empia ingannatrice
%#~ Torna fra’ lacci ancor.
%#~ Vanne, ma pensa intanto,
%#~ Che sciorli un dì vorrai,
%#~ Quando fia vano il pianto
%#~ Inutile il dolor:
\livretDidasP\justify { Scostandosi per partire. }
\livretPers Rinaldo
\livretRef#'CDCaria
%#~ Ah non lasciarmi no.
\livretDidasP\justify { Trattenendolo con smania. }
%#~ (Che barbaro rigor!)
%#~ Quel che vorrai farò.
%#~ Perdona a un folle amor
%#~ L’affanno mio.
%#~ Vorrei partir… vorrei
%#~ Darle l’estremo addio…
%#~ Poveri affetti miei!
\livretDidasP\justify { Si sente in lontananza la voce d’Armida. }
\livretPers Armida
%#~ Aspetta traditor
\livretPers Rinaldo
%#~ Eccola, oh Dio!
\livretDidasP\justify { Col massimo trasporto. }
\livretRef#'CDDrecit
%# Misera! È già vicina.
\livretDidasP\justify { Guardando dentro la scena. }
%# Qual la rende il dolor! Pallida, smorta,
%# Affannata, tremante… Ah vedi amico,
%# Come corre infelice; e l’aspro gelo,
%# E le scoscese rupi
%# Sembrano un vano inciampo
%# Per le tenere piante a passi suoi…
%# Ah non sdegnarti. Io partirò se vuoi.
\livretPers Ubaldo
%# Non è più tempo. Era prudenza allora,
%# Debolezza or saria. Ti serba il Cielo.
%# A dar di tua virtù le prove estreme,
%# Cerca il folle il periglio, il vil lo teme.
\livretPers Rinaldo
%# Deh non lasciarmi amico. In questo stato
%# Più bisogno ho di te. Lo vedi, oh Dio!
%# Che momento funesto!
\livretPers Ubaldo
%# (Oh Ciel, l’assisti; il tuo trionfo è questo.)

\livretScene SCENA V
\livretDescAtt\wordwrap-center { Armida affannata e detti. }
\livretPers Armida
\livretRef#'CEArecit
%# Fermati, aspetta. Ahime! respiro appena.
%# Un palpito affannoso
%# Mi serra il cuor, mi toglie
%# E moto, e voce, e m’abbandona in questi
%# Brevi istanti fatali
%# Fin la forza di piangere i miei mali.
\livretPers Ubaldo
%# (Che periglioso assalto!)
\livretPers Rinaldo
%# (Ah sconsigliato!
%# Qual cimento aspettai!)
\livretPers Armida
%# Mirami ingrato,
%# Ah che ti feci mai
%# Per ridurmi così? Come a tal segno
%# Io da te meritai l’odio, e lo sdegno?
\livretPers Ubaldo
%# (Tremo per lui.)
\livretPers Rinaldo
%# (Resisto appena.) Ascolta
%# E almen per poco Armida,
%# Modera il tuo dolor. Pur troppo io sento
%# I rimproveri tuoi, gemo al tuo pianto,
%# Mi dispera il tuo affanno, e al solo aspetto
%# Del tuo presente stato
%# Io mi sento morir. Vani argomenti
%# Sariano alla mia fuga
%# L’arti tue, l’odio antico, il mio periglio;
%# Ma a un Cittadino, a un figlio
%# Parla la Patria, e il Ciel. Tutto condanna
%# Il nostro amore, e mi richiama altrove
%# La giustizia, il dover. Vuoi ch’io tradisca
%# Tante speranze, e tanti voti, e manchi,
%# Guerrier codardo, e cittadin ribelle
%# Alla Patria, all’ onor?
\livretPers Armida
%# Perfido! Oh stelle!
%# Patria, e regno ebbi anch’io. Di mille voti
%# E di dolci speranze l’oggetto
%# Fui già gran tempo, e grand’invidia porsi
%# Dell’ emule regine al fasto altero;
%# Anch’io del vostro impero
%# Distruggere i principi, e col tuo sangue
%# La Patria oppressa vendicar, stimai
%# Un consiglio de’ numi, e ti salvai.
%# Ma son vane memorie. In questo stato
%# Sol le frodi rammenti, e l’odio antico;
%# L’amante mi tradì; parlo al nemico.
%# Tuo prigionier, tua preda,
%# Chiedo sol di seguirti. Ah dove andrei
%# Senza te sventurata, ove non sia
%# Segno agli scherni altrui la sorte mia?
%# Teco verrò. La mia beltà negletta
%# Del tuo trionfo al campo
%# La gloria accrescerà; Fedele ancella
%# Nel fervor della pugna a’ giorni tuoi
%# Io veglierò gelosa, e al ferro ostile
%# Del mio sen farò scudo. Ah non si nieghi
%# L’innocente richiesta al pianto mio.
\livretPers Ubaldo
%# Vacilli forse?
\livretPers Armida
%# Ah non rispondi?
\livretPers Rinaldo
%# Oh Dio!
%# Tu nemica? Tu serva? Io te del campo
%# Al ludibrio esporrei? Deh qui sia il fine
%# Del fallir nostro, Armida,
%# E del nostro rossor. Nel lido ignoto
%# Ne copra la memoria eterno oblio.
%# Rimanti in pace. Addio. Gl’impeti accheta
%# D’un amore infelice;
%# Io parto, a te non lice
%# Meco venir, la gloria tua lo vieta.
%# Addio. Più che non credi
%# Atroce nel lasciarti è la mia pena,
%# Ma seguirmi non dei.
\livretPers Armida
%# Dunque mi svena.
\livretRef#'CEBterzetto
%#~ Strappami il cuor dal seno,
%#~ Perfido, traditor.
%#~ Sia questo il premio almeno
%#~ D’un infelice amor.
\livretDidasP\justify { Gettandosi a piè di Rinaldo. }
\livretPersDidas Rinaldo sollevandola con passione.
%#~ Sorgi che dici?… Oh Dio.
\livretPersDidas Ubaldo a Rinaldo con severità.
%#~ Ah ti seduce il cor.
\livretPers Armida e Rinaldo
%#~ Che fiero stato è il mio,
%#~ Che barbaro dolor!
\livretPers Armida
%#~ Tu vedi il mio tormento.
\livretPers Ubaldo
%#~ Del Ciel le voci intendi.
\livretPers A 2
%#~ E reo d’un tradimento
\livretPers Armida
%#~ Un fido amore offendi?
\livretPers Ubaldo
%#~ Oltraggi onore, e fe?
\livretPers Rinaldo
%#~ Questo è crudel cimento.
\livretPers A 3
%#~ Ah nell’ angustia estrema
%#~ Il cuor mi trema, e il piè.
\livretPersDidas Ubaldo a Rinaldo
%#~ Vieni l’onor t’affretta.
\livretPers Rinaldo
%#~ Rimanti in pace. Addio,
%#~ No, più crudel del mio
%#~ Il tuo destin non è.
\livretDidasP\justify { S’incamina con Ubaldo. }
\livretPersDidas Armida smaniosa
%#~ Ah barbaro, aspetta
%#~ Ah fermati, ingrato.
%#~ Oh Numi, vendetta
%#~ D’un mostro spietato
%#~ Che fede non ha.
%#~ Sprezzata, tradita
%#~ M’opprime il tormento,
%#~ Mi manca la vita,
%#~ Gelare mi sento,
%#~ Oh Numi pietà.
\livretDidasP\justify { Cade svenuta. }
\livretPersDidas Rinaldo voltandosi con smania
%# Misera!
\livretPers Ubaldo
%# Ah dove vai?
\livretPers Rinaldo
%# A richiamarla in vita.
\livretPers Ubaldo
%# Fuggi la frode ordita.
\livretPers Rinaldo
%# E troppa crudeltà.
\livretDidasP\justify {
  Staccandosi da Ubaldo, e correndo affanato verso Armida.
}
%#~ Armida… Ah… più non ha
%#~ Né moto, né color.
%#~ Forse l’ucciderà
%#~ L’eccesso del dolor.
%#~ Ah mi si spezza il cor
%#~ Fra tanto affanno.
%#~ Senti… Non partirò.
%#~ Vedi… Che t’amo ancor.
%#~ Che parlo? Oh Dio! che fò?
%#~ Oh sfortunato amor!
%#~ Dover tiranno!
%#~ Come restar degg’io?…
%#~ Come partir potrò?…
\livretPersDidas Ubaldo risoluto incamminandosi verso il lido.
%#~ Tu resta, io parto.
\livretPersDidas Rinaldo correndo ad arrestarlo.
%#~ Oh Dio!
%#~ Sentimi.
\livretPers Ubaldo
%#~ O vieni, o resta.
\livretPers Rinaldo
%#~ Che fiera legge è questa!
\livretPers Ubaldo
%#~ Ora è il rigor pietà
\livretPers Rinaldo
%#~ Povera Armida. Addio
%#~ Il duol m’ucciderà.
\livretDidasP\justify { Vanno al lido s’imbarcano, e partono. }

\livretScene SCENA VI
\livretDescAtt\wordwrap-center {
  Armida svenuta, Ismene, e il Coro delle seguaci.
}
\livretPersDidas Ismene voltandosi verso il mare.
\livretRef#'CFArecit
%# Ecco la sventurata. In questo stato
%# Quell’ empio traditore
%# Poté lasciarla, e gliel’ sofferse il cuore?
\livretPersDidas Coro delle donne con Ismene
\livretDidasP\justify {
  Avanzandosi in vari gruppi verso il lido richiamando Rinaldo.
}
\livretRef#'CFBcoro
%#~ Ah barbaro, ah senti,
%#~ Rivolgi le vele
%#~ Ah mira, crudele,
%#~ De’ tuoi tradimenti
%#~ Il frutto qual è.
\livretPersDidas Ismene ritornando affanosa accanto a Armida.
\livretRef#'CFCrecit
%# Infelice Regina! Ancor respira,
%# Palpita ancora. A richiamarla in vita
\livretDidasP\justify { Alle sue seguaci che accorrono. }
%# Aiutatemi amiche. Al tuo dolore
%# Deh non ceder così. D’un traditore
%# Non accresca il trionfo
%# La tua morte infelice. A vendicarti
%# Serbati in vita almen della vendetta
%# La speme non t’è tolta.
%# Deh consolati, Armida, Armida, ascolta.
\livretPersDidas Armida rinvenendosi a poco a poco.
%# Per chi ritorno in vita? Ah colla morte
%# Finisci almeno il mio crudel martiro,
%# Svenami, traditor. Stelle! Che miro?
\livretDidasP\justify {
  Alzandosi spaventata nell’ accorgersi della fuga di Rinaldo.
}
%# Fugge il crudel? Sulla deserta riva
%# Mi lascia sconsolata e semiviva!
%# E non l’inghiotte il mare? E a incenerirlo
%# Fulmin non piomba? E il duol, che mi divora
%# Mira l’abisso, e non si scuote ancora?
\livretDidasP\justify {
  Resta per un momento immobile fissando spaventata lo sguardo verso la marina,
  indi s’ineamina furiosa.
}
%# Sorgete alfin sorgete
\livretDidasP\justify { Ritornando furente. }
%# Alla temuta voce, o furie infeste,
%# Della notte profonda.
\livretDidasP\justify {
  Si sentono fulmini, e tuoni e s’ingombra di tenebre la scena.
}
%# Struggete, oh Dei, struggete
%# Queste del mio rossor rive funeste.
\livretDidasP\justify {
  Si sente in lontananza lo strepito della rovina dell’ isola.
}
%# I venti, e le tempeste
%# Turbino il Cielo, è l’onda. Ah più non chiedo
%# Difesa a un folle amor. Voglio vendetta
%# Della mia fé tradita.
%# Questa speranza sol mi serba in vita.
\livretRef#'CFDaria
%#~ Io con voi la nera face
%#~ A turbargli i rai del giorno,
%#~ Al crudel sempre d’intorno
%#~ Nuova Furia agiterò.
%#~ Io nel sen tutto d’Aletto
%#~ Verserogli il rio veleno;
%#~ A quel perfido dal petto
%#~ L’empio cuore io strapperò.
%#~ E agli ingrati eterno esempio
%#~ Nel suo scempio io lascerò.
\livretDidasP\justify {
  (Monta Armida sopra un carro tirato da’ Dragoni alati, e intorno ad
  essa in vari gruppi sulle nere infuocate nubi le sue seguaci, che
  replicano in Coro.)
}
%#~ E agli ingrati eterno esempio
%#~ Nel suo scempio io lascerò.
\livretFinAct Fine dell Dramma.
