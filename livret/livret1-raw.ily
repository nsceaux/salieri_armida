\livretAct ATTO PRIMO
\livretScene SCENA I
\livretDescAtt\justify {
  Parco delizioso, che s’apre in vari Viali ombrosi, in fondo à quali
  si vede in lontananza il magnifico ingresso dell’ incantato Palazzo
  d’Armida.

  In mezzo al gran Parco, alle fiorite sponde d’uno spazioso lago sono
  imbandite delle ricche mense e stanno scherzando leggiadre donzelle,
  intrecciando a una lieta danza il seguente.
}
\livretPers Coro
\livretRef#'AABcoro
%# Sparso di pure brine
%#~ All’ aure mattutine
%#~ Come vermiglio un fior
%#~ Spunta sul primo albor
%#~ Poi langue, e muore.
%# Passa per noi così
%#~ Il fior della beltà,
%#~ É dura un breve di,
%#~ Se nella fresca età
%#~ Nol’ coglie amore
\livretDidasP Una parte del Coro
\livretRef#'AAEcoro
%# Donzelle semplici
%#~ Non vi lagnate,
%#~ Che troppo rapida
%#~ Fugga l’età
%# Se fresche e giovani
%#~ Non v’affrettate
%#~ Il frutto a cogliere
%#~ Della Beltà.
%# Donzelle semplici
%#~ Non vi lagna…

\livretScene SCENA II
\livretDescAtt\wordwrap-center {
  Ismene frettolosa, e affannata, e dette.
}
\livretPers Ismene
\livretRef#'ABArecit
%# Ah difendete, amiche,
%# Il confidato passo. À noti segni
%# Imminente è il periglio, e nel più chiuso
%# Del custodito albergo, onde gelosa
%# Vieta l’accesso à suoi più fidi, e dove
%# Sola, e sicura al suo poter si fida,
%# In mezzo à frutti suoi, minaccia Armida.
%# Collo spuntar del sole
%# Più non appar la tenebrosa, e folta
%# Nebbia, che ad ogni sguardo
%# Quest’ Isola ascondea; meste, e confuse
%# Strida ingombrano il lido, e ignoto legno
%# Vi si scorge in sicuro: Erranti e sparsi
%# Vidi i Mostri custodi
%# Fuggir per l’erta, e s’altri accorre, e chiede
%# La cagion della fuga, un tal terrore
%# Quella Guardia fatal turba, e confonde
%# Che torce altrove il corso, e non risponde.
\livretDidasP\justify {
  Alle parole d’Ismene le donzelle s’attruppano interno ad essa impaurite.
}
\livretPers Coro
\livretRef#'ABBcoro
%# Ah fra la nera
%#~ Densa caligine
%#~ La riva inospita
%#~ Chi mai scuoprì?
%# Ah fra la schiera
%#~ Dé mostri orribili
%#~ Qual Diò, qual Demone
%#~ La via s’apri?
\livretPers Ismene
\livretRef#'ABCrecit
%# In si crudel dubbiezza ah non perdiamo
%# I momenti in querele. Un finto riso
%# Cuopra il nostro terror. Tutto respiri
%# Letizia, e pace; e parolette accorte
%# E languidi sospiri, e molli sguardi
%# Tutto si metta in opra, e tutto alletti
%# L’incauto vincitore à sorsi infetti.
%# Le difese preparo, e non conosco
%# O il periglio, o il nemico. Ah se lo guida
%# Forza maggior, di fé, di zelo almeno
%# Si compiscan le parti
%# E poi… Che veggio? Ecco il nemico: All’ arti.
\livretDidasP\justify {
  Guardando con sorpresa dentro la Scena; e assieme colle donzelle
  ripigliando il ballo, e il canto.
}
\livretPers Coro
\livretRef#'ABDcoro
%# Sparso di pure brine
%#~ All’ aure mattutine,
%#~ Come vermiglio un fior
%#~ Spunta col primo albor… oh noi meschine?
\livretDidasP\justify {
  Esce Ubaldo, e le donzelle come impaurite fingono di ritirarsi
  altre verso il fondo altre fragli alberi.
}

\livretScene SCENA III
\livretDescAtt\wordwrap-center {
  Ubaldo e dette.
}
\livretPers Ubaldo
\livretRef#'ACArecit
%# Ecco l’onda insidiosa; ove col riso
%# Altri beve la morte. Io la ravviso
%# Alle fiorite sponde,
%# À lauti cibi, alle Sirene immonde.
%# Con quant’ arte dispose i neri inganni
%# L’accorta Maga!
\livretDidasP\justify {
  In aria ridente, fingendo di richiamar le compagne impaurite.
}
\livretPers Ismene
%# Ah dileguate amiche,
%# L’importuno timor. Guerra non reca
%# Alla tranquilla sede
%# Questo prode guerrier. D’ogni diletto
%# Vieni a parte con noi; vieni a deporre
%# Quell’ inutile acciaro
%# Fortunato stranier; dolce ristoro
%# T’offre la mensa, e il rio. De’ fieri mostri
%# La perigliosa guardia
%# Più a temer non ti resta;
%# Altra specie di pugna Amor t’appresta.
\livretPersDidas Tutto il Coro con Ismene
\livretRef#'ACBcoro
%# Vieni al fonte del contento,
%#~ Fortunato passeggier.
%#~ E’ perduto ogni momento,
%#~ Che si perde pel’ piacer.
\livretPers Ismene
%# Dell’ Amor la reggia è questa,
%#~ La delizia de mortali;
%#~ Nell’ oblio di tutti i mali
%#~ Qui si viene a riposar.
%#~ Qui non v’è cura molesta,
%#~ Che il piacer di tosco infetti,
%#~ E il più dolce de diletti
%#~ Non ti costa, che il bramar.
\livretPers Tutto il Coro
%# Vieni al fonte del contento,
%#~ Fortunato passeggier.
%#~ E’ perduto ogni momento,
%#~ Che si perde pel’ piacer.
\livretPers Ubaldo
\livretRef#'ACCrecit
%# (Che periglioso assalto!)
\livretPers Ismene
%# Al dolce invito.
%# Perchè resisti mai? Non prega invano
%# Donzella in fresca età. Trarrotti io stessa
%# L’elmo lucente, e questa,
%# Con cui distingue Armida
%# I fidi servi suoi, fiorita insegna
%# Al crin ti cingerò.
\livretDidasP\justify {
  Prendendo une Ghirlanda di fiori, e in atto di levar l’elmo ad Ubaldo.
}
\livretPersDidas Ubaldo rispingendola con severità.
%# Scostati, indegna
%# Riserba a miglior uso
%# I finti vezzi, e le lusinghe accorte.
%# Puoi col riso sul labro offrir la morte?
%# So qual tosco s’asconde
%# In que’ cibi, in quell’onde, e le temute
%# Vane frodi d’Armida…
\livretPersDidas Coro delle Donzelle
\livretDidasP\justify {
  (tutte impaurite scostancdosi da Ubaldo.)
}
%# Ah siam perdute.
\livretPers Ismene
%# In mal punto ricusi,
%# Malnato Cavalier, l’offerta pace,
%# E l’offerta amistà; se à molli preghi
%# Altro ch’ onte, e disprezzi offrir non sai
%# Guerra apporti, infelice, e guerra avrai.
\livretRef#'ACDaria
%#~ Mostri i più crudi, e infesti
%#~ Della magion di Dite
%#~ Da’ cupi antri funesti,
%#~ Al cenno mio, venite.
\livretDidasP\justify {
  S’oscura la scena, e si cambia in un luogo orribile, presentandosi
  in vari gruppi minaciosi i Demoni a spaventare Ubaldo.
}
\livretPersDidas Coro di Demoni
\livretRef#'ACEcoro
%# Qual è la man che scuote
%#~ L’Antica Reggia a Pluto?
%#~ Qual è il poter temuto,
%#~ Che noi dagli antri orribili
%#~ Richiama à rai del dì!
%# Strazin le fiere Eumenidi,
%#~ Strugga la fiamma ultrice,
%#~ L’incauto, e l’infelice
%#~ Che provocarlo ardì.
\livretPers Ubaldo
\livretRef#'ACFrecit
%# Quanto fragili, e vane, immondi spirti,
%# Son l’armi vostre incontro al Ciel! Mirate
%# Quanto è lieve il contrasto, e quanto poco
%# Basta, infelici, a richiamarvi al fuoco.
\livretRef#'ACGaria
%# Tornate al nero Abisso,
%#~ Onde l’orror v’alberga,
%#~ E d’una sola verga
%#~ All’agitar sparite.
\livretDidasP\justify {
  Allo scuoter che fa Ubaldo incontro à Demoni la magica
  verga fuggono questi impauriti nella più gran confusione.
}
\livretPersDidas Coro di Demoni
\livretRef#'ACHcoro
%# Qual Sibilo orrendo
%#~ Per l’aer rimbomba?
%#~ Qual braccio tremendo
%#~ Ci opprime cosi?
\livretDidasP\justify {
  Spariti i mostri ritorna nel suo primo ridente flate la scena.
}
\livretPers Ubaldo
\livretRef#'ACIrecit
%# Eterno Proveder, tu che guidasti
%# Per si strano camino i passi miei
%# Scorgigli al fin prescritto. Ecco l’albergo,
%# Ove, in grembo al piacere, al giogo indegno
%# Di beltà lusinghiera
%# Il tuo giovane Eroe piega la fronte,
%# Tu che percuoti il monte, e dall’ estrema
%# Falda si scuote, e fumo, e fiamma spira,
%# Tu che nel mezzo all’ ira
%# Il suol rimiri, e il suol vacilla, e trema,
%# Scuoti, scuoti, gran Dio, dal cupo sonno
%# Quell’ alma incauta, al guardo suo disvela
%# L’orror de’ falli suoi, dell’empia Armida
%# Sien le frodi schernite,
%# E trionfi il tuo nome in faccia a Dite.
\livretRef#'ACJaria
%# Finta larva, d’Abisso frall’ ombra,
%#~ Il piacere gli scherza d’intorno.
%#~ Ah se il sonno di morte l’ingombra,
%#~ Se i suoi lumi si chiudono al giorno,
%#~ Nell’ orrore del carcere indegno
%#~ Più che a sdegno ti muova a pietà.
%# Sciogli, sgombra, la notte funesta,
%#~ Dio possente, lo scuoti, lo desta.
%#~ Chi può trarlo dall’ ombra di morte;
%#~ Se i tuoi raggi per scorta non hà?
%# Il tuo spirto m’infiamma, m’accende
%#~ Dio possente lo sento, lo scerno.
%#~ Ah le frodi, e le forze d’Averno
%#~ Van contrasto saranno al tuo vanto,
%#~ E l’incanto di vana beltà.
\livretDidasP\justify {
  Parte verso il fonde.
}
\livretFinAct Fine dell’ Atto primo.
\sep
\column-break
